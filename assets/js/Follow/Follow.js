import React, { Component } from 'react';
import { UserAPI } from '../API/UserAPI';

export class Follow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            following: this.props.following
        };
        this.api = new UserAPI();

        document.addEventListener('follow.action', (e) => {
            this.setState({
                following: e.detail.state.state
            })
        });

    }

    dispatch(name, state) {
        let event = new CustomEvent(name, {detail: {state}});
        document.dispatchEvent(event);
    }

    follow() {
        this.api.follow(this.props.user).then(() => {
            this.setState({following: true})
        });
        this.dispatch('follow.action', {slug: this.props.user, state: true})
    }

    unfollow() {
        this.api.unfollow(this.props.user).then(() => {
            this.setState({following: false})
        });
        this.dispatch('follow.action', {slug: this.props.user, state: false})
    }

    render() {

        return (
            <span className="profile-follow">
                <span hidden={ this.state.following } onClick={ () => this.follow() }
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Follow">
                    <i className="fa fa-user-plus"/>
                </span>
                <span hidden={ !this.state.following } onClick={ () => this.unfollow() }
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Unfollow">
                    <i className="followed-green fa fa-user-check"/>
                </span>
            </span>
        )
    }
}

