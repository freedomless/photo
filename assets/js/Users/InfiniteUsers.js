import React, {Component} from 'react';
import Loader from 'react-loader-spinner';
import {UserAPI} from '../API/UserAPI';
import InfiniteScroll from 'react-infinite-scroller';

export class InfiniteUsers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            more: this.props.users.has_more,
            users: this.props.users.items,
            page: 2,
        };

        this.api = new UserAPI();

        this.loadMore = this.loadMore.bind(this);
    }

    loadFactory(page) {
        switch (this.props.page) {
            case 'alphabetical':
                return this.api.getAlphabeticalMembers(page);
            case 'founders':
                return this.api.getFounders(page);
            case 'top':
                return this.api.getTopMembers(page);
            case 'following':
                return this.api.getFollowing(page);
            case 'followers':
                return this.api.getFollowers(page);
            case 'by-country':
                return this.api.getByCountry(page, this.props.country);
        }
    }

    loadMore() {

        this.loadFactory(this.state.page).then((res) => {

            let loaded = res.data.data.items;
            let users = this.state.users;

            this.setState({
                loading: false,
                page: this.state.page + 1,
                users: [...users, ...loaded],
                more: res.data.data.has_more
            })
        });
    }

    render() {

        let members = [];

        this.state.users.map((user, key) => {
            members.push(
                <div className="col-lg-3 col-md-4 col-sm-6" key={key}>

                    <div className="shadow profile-thumb rounded-corner">

                        <a href={'/profile/' + user.slug}>

                            <div className="profile-cover">
                                <img src={user.profile.cover || '/images/cover.jpg'} alt=""/>

                                <div className="profile-avatar circle shadow">
                                    <img src={user.profile.picture || '/images/avatar.jpg'} alt=""/>
                                </div>
                            </div>

                            <div className="profile-username">
                                <h5 className="align-bottom">
                                    { user.profile.first_name } { user.profile.last_name }
                                </h5>
                            </div>
                        </a>

                    </div>

                </div>
            );
        });

        if (members.length === 0 && this.state.more === false) {
            members.push(
                <h3 className="text-center w-100 mt-5"
                    hidden={this.state.users.length !== 0 && this.state.more === true}
                    key={Math.random()}>No Users Available
                </h3>
            )
        }

        return (
            <div className="container photos">
                <InfiniteScroll
                    loadMore={ this.loadMore }
                    hasMore={ this.state.more }
                    loader={
                        <div className="w-100 text-center position-relative mb-5 mt-5" key={ 0 }>
                            <Loader type="Oval"
                                    color="#00BFFF"
                                    height={100}
                                    width={100}
                                    className="m-auto"
                            />
                        </div>
                    }>
                    <div className="members row" key={1}>
                        {members}
                    </div>
                </InfiniteScroll>
            </div>
        )
    }
}
