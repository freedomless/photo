$(document).on('click', '.notifications-list', function (e) {
    e.stopPropagation();
});

$(document).on('contextmenu drag  dragstart', '.photo-content', () => {
    $('#copyright-notice').modal('show');
    window.event.returnValue = false;
    return false
});

$(function () {
    $('[data-toggle="popover"]').popover({html: true});
    $('[data-toggle="tooltip"]').tooltip({boundary: 'window', trigger: 'hover'})
});

console.log('%c  ', 'background-image: url(\'https://media.giphy.com/media/11sBLVxNs7v6WA/giphy.gif\'); background-repeat: no-repeat; background-size: 148px 88px; font-size: 128px;');

$(document).ready(function () {

    $('#vote-form').submit(function (e) {

        e.preventDefault();

        let submitButton = $(this).find('button');
        submitButton.attr('disabled', 'disabled');

        let submitUrl = $(this).data('api-url');
        let formSerialize = $(this).serialize();
        $.ajax({
            type: "POST",
            url: submitUrl,
            data: formSerialize,
            success: function (msg) {
                submitButton.removeAttr('disabled');

                if (msg.code !== 0) {
                    $('#js-vote-error .msg').html(msg.message.html);
                    $('#js-vote-error').slideDown().delay(3500).slideUp();
                } else if (msg.code === 0) {
                    $('.poll-choices').html(msg.data.html);
                }
            }
        });
    });

    $('.js-hide-poll').on('click', function (e) {
        e.preventDefault();

        let url = $(this).data('api-url');

        $('.poll-container').remove();
        $.ajax({
            url: url, success: function (result) {
            }
        });
    })
});

$(document).ready(function () {
    $('.js-hide-notification').on('click', function (e) {
        e.preventDefault();

        let url = $(this).data('api-url');

        $('.notification-container').remove();
        $.ajax({
            url: url, success: function (result) {
            }
        });
    })
});

$(document).ready(function(){
    $('.danger').popover({
        html : true,
        trigger: 'click focus',
        content: function() {
            return $('#popover_content_wrapper').html();
        }
    });
});

$('.js-tooltip-item').tooltip();