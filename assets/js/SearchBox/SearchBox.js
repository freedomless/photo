import React, {Component} from 'react';
import {SearchAPI} from '../API/SearchAPI';
import {DebounceInput} from 'react-debounce-input';

export class SearchBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            posts: [],
            query: '',
            hidden: true,
            loading: false
        };

        this.api = new SearchAPI();
        this.handleQuery = this.handleQuery.bind(this);
    }

    componentDidMount() {
        window.addEventListener('click', (e) => {
            if (!document.querySelector('.results').contains(e.target)) {
                this.setState({hidden: true, query: ''});
            }
        });
    }

    handleQuery(e) {
        let query = e.target.value;

        this.setState({query, hidden: false});

        if (this.state.loading === false && query.length > 0) {
            this.setState({loading: true});
            this.api.search(query).then(res => {
                this.setState({loading: false, users: res.data.data.users || [], posts: res.data.data.posts || []});
            });
        }
    }

    render() {
        return (
            <div className="ml-auto search-box">
                <div className="search-box">
                    <i className="fas fa-search"></i>
                    <DebounceInput
                        minLength={2}
                        placeholder="Search"
                        debounceTimeout={300}
                        onChange={event => this.handleQuery(event)}/>
                </div>
                <div className="results shadow p-3" hidden={this.state.hidden}>
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <h5 className="text-muted">Photos</h5>
                            <hr/>
                            <ul className="list-unstyled" hidden={this.state.posts.length === 0}>
                                {this.state.posts.map(item => {
                                    return (
                                        <li className="media mb-3" key={Math.random()}>
                                            <a href={'/photo/portfolio/' + item.id + '/' + item.user.id}
                                               className="result mb-2">
                                                <img src={item.image.thumbnail} alt="" height="100px" width="100px"
                                                     className="rounded" style={{objectFit: 'cover'}}/>
                                            </a>
                                            <div className="media-body ml-2">
                                                <h5 className="mt-0 mb-1">{item.title}</h5>
                                                <span className="text-muted">{item.description ? item.description.substr(0,100) : null} ...</span>
                                            </div>
                                        </li>
                                    );
                                })}
                            </ul>
                            <div className="mt-md-5 pt-md-5"
                                 hidden={this.state.posts.length > 0}>
                                No results found
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <h5 className="text-muted">Users</h5>
                            <hr/>
                            <ul className="list-unstyled" hidden={this.state.users.length === 0}>
                                {this.state.users.map(user => {
                                    return (
                                        <li className="media mb-3" key={Math.random()}>
                                            <a href={'/profile/' + user.slug}>
                                                <img src={user.profile.picture || '/images/avatar.jpg'} alt=""
                                                     width="50px"
                                                     height="50px"
                                                     className="circle shadow-sm"/>
                                            </a>
                                            <div className="media-body ml-2">
                                                <a href={'/profile/' + user.slug}>
                                                    <h5 className="mt-0 mb-1 pt-2">{user.profile.first_name} {user.profile.last_name}</h5>
                                                    @{user.username}
                                                </a>
                                            </div>
                                        </li>
                                    );
                                })}
                            </ul>
                            <div className="mt-md-5 pt-md-5"
                                 hidden={this.state.users.length > 0}>
                                No results found
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

