import React, { Component } from 'react';
import CircleLoader from 'react-loader-spinner';

export class Loader extends Component {
    render() {
        return (
            <div className="spinner">
                <div className="bounce1"/>
                <div className="bounce2"/>
                <div className="bounce3"/>
            </div>
        )
    }
}

