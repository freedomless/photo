import React, { Component } from 'react';

export class Image extends Component {

    constructor(props) {
        super(props);

        this.ref = null;
    }

    render() {
        return (<img ref={ (ref) => this.ref = ref }
                     onClick={ this.props.onClick }
                     onLoad={this.props.onLoad}
                     style={this.props.style || null}
                     src={ this.props.src }
                     alt={ this.props.title }
                     width={ this.props.width || 'auto' }
                     height={ this.props.height || 'auto' }
                     className={ this.props.classes || '' }/>)
    }
}

