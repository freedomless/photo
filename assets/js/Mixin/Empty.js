import React, { Component } from 'react';

export class Empty extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div hidden={ this.props.hidden } className="text-center mt-5">
                <h4 className="text-muted">{ this.props.children }</h4>
            </div>
        )
    }
}
