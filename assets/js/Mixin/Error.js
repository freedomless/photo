import React, { Component } from 'react';

export class Error extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className={ 'text-danger ' + this.props.classes } hidden={ this.props.hidden }>
                { this.props.children }
            </div>
        )
    }
}

