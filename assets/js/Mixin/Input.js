import React, { Component } from 'react';

export class Input extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="form-group">
                { this.props.children }
                { this.props.label ? <label htmlFor={ this.props.id || '' }>{ this.props.label }</label> : null }
                <input type={ this.props.type || 'text' }
                       id={ this.props.id || '' }
                       onChange={ e => this.props.onChange(e.target.value) }
                       value={ this.props.value || '' }
                       className={ 'form-control ' + this.props.classes }
                       placeholder={ this.props.placeholder || '' }/>
            </div>
        )
    }
}

