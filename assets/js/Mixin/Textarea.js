import React, { Component } from 'react';

export class Textarea extends Component {
    render() {
        return (
            <div className="form-group">
                { this.props.children }
                { this.props.label ? <label htmlFor={ this.props.id || '' }>{ this.props.label }</label> : null }
                <textarea id={ this.props.id || '' }
                          onChange={this.props.onChange}
                          value={this.props.value || ''}
                          rows={ this.props.rows || 3 }
                          className={ 'form-control ' + this.props.classes }
                          placeholder={ this.props.placeholder || '' }/>
            </div>
        )
    }
}

