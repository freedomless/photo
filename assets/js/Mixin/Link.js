import React, { Component } from 'react';
import { Icon } from './Icon';

export class Link extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <a href={ this.props.url || '#' } onClick={ this.props.onClick }
               data-toggle={ this.props.tooltip ? 'tooltip' : '' }
               title={ this.props.tooltip ? this.props.tooltip : '' }
               data-placement={this.props.placement || 'top'}
               className={ this.props.classes || '' }
               target={ this.props.target || '_self' }>
                { this.props.children }
                { this.props.icon ? <Icon classes={ this.props.icon }/> : null }
            </a>
        )
    }
}


