import React, { Component } from 'react';
import { Icon } from './Icon';

export class Button extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <button
                data-original-title={ this.props.tooltip ? this.props.tooltip : '' }
                data-toggle={ this.props.tooltip ? 'tooltip' : '' }
                title={ this.props.tooltip ? this.props.tooltip : '' }
                data-placement={ this.props.placement || 'top' }
                onClick={ this.props.onClick }
                type={ this.props.type || 'button' }
                disabled={ this.props.disabled || false }
                className={ this.props.classes }>
                { this.props.children }
                { this.props.icon ? <Icon classes={ this.props.icon }/> : null }
            </button>
        )
    }
}
