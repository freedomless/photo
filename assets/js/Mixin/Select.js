import React, { Component } from 'react';

export class Select extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-group">

                <label>{this.props.label}</label>

                <select className="form-control"
                        onChange={ e => this.props.update(e.target.value) }>
                    { this.props.items.map(item => {
                        return <option key={ item.id } value={ item.id }>{ item[this.props.name || 'name'] }</option>
                    }) }
                </select>

            </div>
        )
    }
}


