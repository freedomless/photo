import React , { Component } from 'react';

export class Icon extends Component
{
    constructor(props) {
        super(props);

    }

    render(){
        return (<i className={this.props.classes} />)
    }
}

export default Icon;
