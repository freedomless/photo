import React , { Component } from 'react';

export class Modal extends Component
{
    constructor(props) {
        super(props);

    }

    render(){
        return(
            <div className="modal fade mt-5 position-absolute" data-backdrop="false" id={this.props.id} tabIndex="1"
                 role="dialog" aria-hidden="true">

                <div className="modal-dialog" role="document">

                    <div className="modal-content">

                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{this.props.title || 'Confirm'}</h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            {this.props.children || 'Are you sure you want to do this action?'}
                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary"
                                    data-dismiss="modal">No
                            </button>
                            <button className="btn btn-info" onClick={this.props.action}>Yes</button>
                        </div>

                    </div>

                </div>

            </div>
        )
    }
}

