import React from 'react';
import * as serviceWorker from './serviceWorker';
import ReactOnRails from 'react-on-rails';
import { SearchBox } from './SearchBox/SearchBox';
import { Messages } from './Messages/Messages';
import { Follow } from './Follow/Follow';
import { Favorite } from './Favorite/Favorite';
import { InfiniteUsers } from './Users/InfiniteUsers';
import { Toasts } from './Toasts/Toasts';
import { Notifications } from './Notifications/Notifications';
import { Cart } from './Cart/Cart';
import { CartButton } from './Cart/CartButton';
import { InfiniteSeries } from './Series/InfiniteSeries';
import { InfinitePost } from './Post/InfinitePost';
import { Options } from './Post/Post/Options';
import { PostSinglePage } from './Post/Single/PostSinglePage';

import './main.js';
import '../css/app.scss';

ReactOnRails.register({Options});
ReactOnRails.register({PostSinglePage});
ReactOnRails.register({SearchBox});
ReactOnRails.register({Messages});
ReactOnRails.register({Follow});
ReactOnRails.register({Favorite});
ReactOnRails.register({InfinitePost});
ReactOnRails.register({InfiniteUsers});
ReactOnRails.register({InfiniteSeries});
ReactOnRails.register({Cart});
ReactOnRails.register({Toasts});
ReactOnRails.register({Notifications});
ReactOnRails.register({CartButton});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
