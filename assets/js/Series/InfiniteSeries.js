import React, { Component } from 'react';
import { SeriesAPI } from '../API/SeriesAPI';
import InfiniteScroll from 'react-infinite-scroller';
import { Filters } from './Filters';
import { Loader } from '../Mixin/Loader';
import { Options } from '../Post/Post/Options';

export class InfiniteSeries extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            series: this.props.series,
            more: true,
            page: this.props.series ? 2 : 1,
            filters: []
        };

        this.api = new SeriesAPI();
        this.loadMore = this.loadMore.bind(this);
        this.updateFilters = this.updateFilters.bind(this)
    }

    loadMore() {

        let promise;

        switch (this.props.page) {
            case 'home':
            case 'gallery':
                promise = this.api.getSeries(this.state.page, this.state.filters);
                break;
            case 'published':
                promise = this.api.getUserSeries(this.props.account, this.state.page);
                break;
            case 'portfolio':
                promise = this.api.getUserPortfolioSeries(this.props.account, this.state.page);
                break;
            case 'store':
                promise = this.api.getProducts(this.state.page, this.state.filters);
                break;
        }

        promise.then(res => {
            this.setState({
                loading: false,
                page: this.state.page + 1,
                series: [...this.state.series, ...(res.data.data.items || [])],
                more: res.data.data.has_more
            })
        })
    }

    updateFilters(filters) {

        filters = $.extend({}, filters);

        for (let prop in filters) {
            if (filters[prop] === '' || filters[prop].length === 0) {
                delete filters[prop]
            }
        }

        this.setState({
            page: 1,
            more: true,
            series: [],
            hidden: true,
            loading: true,
            filters
        });
    }

    render() {

        let items = this.state.series.map(series => {

            let images = series.children.map((post, key) => {

                if (key > 4) {
                    return null;
                }

                let more = key + 1 === series.children.length || (series.children.length > 5 && key > 3) ?
                    null : null;

                return <div key={ key }><img src={ post.image.thumbnail } alt=""/>{ more }</div>
            });

            return (

                <div className="series text-center col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5" key={ series.slug }>
                    <a href={ `/series/${ series.id }/view` }>
                        <div className={ 'grid-' + (series.children.length > 5 ? 5 : series.children.length) }>
                            { images }
                        </div>
                    </a>

                    { series.title ? <div className="mt-1"><b>{ series.title }</b></div> :
                        <div className="mb-2"></div> }

                    <div className="text-center mb-2 owner d-flex justify-content-center">

                        { series.user ?
                            <a href={ '/profile/' + series.user.slug }
                               className="flex-grow-1 ml-5 mt-2"
                            >
                                &copy; { series.user.profile.first_name } { series.user.profile.last_name }
                            </a>
                            :
                            'Curators Team' }


                        <div className="mb-2">
                            { this.props.page === 'store' ? <Options post={ series }
                                                                     sizes={ this.props.sizes }
                                                                     materials={ this.props.materials }/> : null }

                        </div>

                    </div>
                </div>
            );
        });

        let filter = this.props.filter == true ? <Filters
            key={ 0 }
            sorting={ this.props.sorting }
            categories={ this.props.categories }
            filters={ this.props.filters }
            update={ this.updateFilters }/> : null;

        return (
            <div className="container photo-series photos">
                { filter }
                <InfiniteScroll
                    key={ 1 }
                    page={ 0 }
                    loadMore={ this.loadMore }
                    hasMore={ this.state.more }
                    loader={
                        <div className="w-100 text-center position-relative mb-5 mt-5" key={ 0 }>
                            <Loader key={ 1 }
                            />
                        </div>
                    }>
                    <div className="row align-items-center" key={ 1 }>
                        { items }
                    </div>
                    <div className="text-center pt-5"
                         key={ 2 }
                         hidden={ this.state.series.length > 0 || this.state.loading === true }>
                        <h3 className="text-muted pt-5 mb-5">No series available</h3>
                    </div>
                </InfiniteScroll>
            </div>
        )
    }
}

