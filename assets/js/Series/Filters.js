import React, { Component } from 'react';
import { DebounceInput } from 'react-debounce-input';

const filters = {
    categories: '',
    greyscale: '',
    nude: '',
    keyword: ''
};

export class Filters extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: {...filters, ...this.props.filters}
        };

        this.clear = this.clear.bind(this);
        this.updateNude = this.updateNude.bind(this)
        this.updateKeyword = this.updateKeyword.bind(this)
        this.updateGreyscale = this.updateGreyscale.bind(this)
        this.updateCategories = this.updateCategories.bind(this)
    }

    clear() {

        if (this.state.filters.greyscale === '' &&
            this.state.filters.keyword === '' &&
            this.state.filters.nude === '' &&
            this.state.filters.most_sold === '' &&
            this.state.filters.most_liked === '' &&
            this.state.filters.newest === '' &&
            this.state.filters.categories.length === 0) {
            return false;
        }

        this.setState({filters}, () => {
            this.props.update()
        })
    }

    updateCategories(checked, id) {

        let categories = this.state.filters.categories === '' ? [] : this.state.filters.categories.split(',').map((v) => parseInt(v));

        if (!checked) {
            categories.splice(categories.indexOf(id), 1)
        } else {
            categories.push(id);
        }

        let filters = this.state.filters;

        categories = categories.filter((v) => typeof v === 'number').join(',');
        this.setState({
            filters: {
                ...filters,
                categories: categories
            }
        }, () => this.props.update(this.state.filters));
    }

    updateNude(value) {
        let filters = this.state.filters;

        this.setState({
            filters: {
                ...filters,
                nude: value
            }
        }, () => this.props.update(this.state.filters));

    }

    updateGreyscale(value) {
        let filters = this.state.filters;
        this.setState({
            filters: {
                ...filters,
                greyscale: value
            }
        }, () => this.props.update(this.state.filters));
    }

    updateKeyword(value) {
        this.setState({filters: {...this.state.filters, keyword: value}}, () => this.props.update(this.state.filters));
    }

    updateFilter(value) {

        let filters = this.state.filters;

        filters.most_liked = ''
        filters.most_sold = ''
        filters.cheapest = ''
        filters.newest = ''

        console.log({...filters, ...value})
        this.setState({filters: {...filters, ...value}}, () => this.props.update(this.state.filters));
    }

    render() {
        return (
            <div className="filters store-filters">
                <ul className="nav justify-content-center custom-active categories">
                    { this.props.categories.map((category, key) => {
                        return (
                            <li key={ 'category-' + key }>
                                <input type="checkbox"
                                       checked={ this.state.filters.categories !== '' &&
                                       this.state.filters.categories.split(',').indexOf(category.id + '') > -1 }
                                       onChange={ (e) => this.updateCategories(e.target.checked, category.id) }
                                       id={ 'category-' + key }/>
                                <label htmlFor={ 'category-' + key }>
                                    { category.name }
                                </label>
                            </li>)
                    }) }
                </ul>
                <hr/>
                <form>
                    <div className="form-row justify-content-center">
                        <div className="form-group col-md-2">
                            <label htmlFor="">Filter by color</label>
                            <select className="form-control" onChange={ (e) => this.updateGreyscale(e.target.value) }
                                    value={ this.state.filters.greyscale }>
                                <option value="">All</option>
                                <option value="1">Black&White</option>
                                <option value="0">Color</option>
                            </select>
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="">Filter by nude</label>
                            <select className="form-control" onChange={ (e) => this.updateNude(e.target.value) }
                                    value={ this.state.filters.nude }>
                                <option value="">All</option>
                                <option value="1">Nude</option>
                                <option value="0">Not nude</option>
                            </select>
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="">Filter by keyword</label>
                            <DebounceInput
                                className="form-control"
                                minLength={ 2 }
                                value={ this.state.filters.keyword }
                                placeholder="Title, Author or Tag"
                                debounceTimeout={ 300 }
                                onChange={ (e) => this.updateKeyword(e.target.value) }/>
                        </div>
                        <div className="form-group col-md-2">
                            <div className="row">
                                { this.props.sorting ? (
                                    <div className="col-md-3 dropdown text-center">
                                        <label htmlFor="">Sort:</label>
                                        <button type="button"
                                                className="form-control btn btn-dark btn-blue dropdown-toggle"
                                                id="sort"
                                                data-toggle="dropdown"
                                                aria-haspopup="true"
                                                aria-expanded="false">
                                            <i className="fas fa-sort"/>
                                        </button>
                                        <div className="dropdown-menu text-left" aria-labelledby="sort">
                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({most_sold: 1}) }>Most Sold
                                            </button>
                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({most_sold: 0}) }>Least Sold
                                            </button>
                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({most_liked: 1}) }>Most liked
                                            </button>
                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({most_liked: 0}) }>Least liked
                                            </button>

                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({newest: 1}) }>Newest
                                            </button>
                                            <button type="button" className="dropdown-item"
                                                    onClick={ () => this.updateFilter({newest: 0}) }>Oldest
                                            </button>
                                        </div>
                                    </div>
                                ) : null }
                                <div className={ this.props.sorting ? 'col-md-9' : 'col-md-12' }>
                                    <label htmlFor="">Reset:</label>
                                    <button className="form-control btn btn-dark btn-blue"
                                            onClick={ this.clear }>Clear
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <hr/>
            </div>
        )
    }
}
