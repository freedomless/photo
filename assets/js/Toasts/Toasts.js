import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export class Toasts extends Component {

    constructor(props) {

        super(props);

        this.state = {count: 0};

        this.addNotification = this.addNotification.bind(this);
    }

    componentDidMount() {
        document.addEventListener('notification', (e) => {
            this.addNotification(e.detail)
        })
    }

    addNotification(data) {

        if (this.state.count > 0) {
            return;
        }

        const that = this;
        let message = typeof (data.message) === 'object';

        if (message) {
            message = '';

            for (let prop in data.message) {
                message += data.message[prop] + '\n';
            }
        } else {
            message = data.message;
        }

        this.setState({count: 1}, () => {
            toast[data.type](message, {
                position: 'bottom-center',
                autoClose: data.type === 'error' ? false : 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                onClose: () => {
                    that.setState({count: 0});
                }
            });
        });

    }

    render() {
        return (<ToastContainer position="bottom-center"
                                autoClose={ 5000 }
                                hideProgressBar={ false }
                                newestOnTop
                                closeOnClick
                                rtl={ false }
                                pauseOnVisibilityChange
                                draggable
                                pauseOnHover
            />
        )
    }
}

