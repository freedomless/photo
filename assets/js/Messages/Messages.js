import React, { Component } from 'react';
import { MessageAPI } from '../API/MessageAPI';
import { DebounceInput } from 'react-debounce-input';
import Loader from 'react-loader-spinner';

export class Messages extends Component {

    constructor(props) {
        super(props);

        this.state = {
            message: {
                text: '',
                subject: '',
                user: null,
                loading: false
            },
            selected: null,
            errors: {
                user: false,
                subject: false,
                text: false
            },
            empty: false,
            adding: false,
            more: true,
            loading: false,
            query: '',
            suggestions: [],
            messages: [],
            visible: false,
            step: 1,
            page: 1,
            replying: false
        };

        this.api = new MessageAPI();
        this.selectUser = this.selectUser.bind(this);
        this.typeMessage = this.typeMessage.bind(this);
        this.searchForUser = this.searchForUser.bind(this);
        this.addMessage = this.addMessage.bind(this);
        this.getMessages = this.getMessages.bind(this);
        this.reply = this.reply.bind(this);
        this.deleteConversation = this.deleteConversation.bind(this);
    }

    componentDidMount() {

        this.getMessages();

        if (this.props.receiver !== null) {

            let message = this.state.message;
            message.user = this.props.receiver.id;

            this.setState({
                message,
                adding: true,
                query: this.props.receiver.profile.first_name + ' ' + this.props.receiver.profile.last_name
            })
        }

        if (this.props.conversation) {
            this.selectMessage({id: this.props.conversation})
        }

        document.addEventListener('click', () => {
            this.setState({
                visible: false,
                suggestions: []
            })
        });

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        $(this.replyEditor).summernote({
            height: 300, popover: {
                image: [],
                link: [],
                air: []
            }
        });
        $(this.messageEditor).summernote({
            height: 300, popover: {
                image: [],
                link: [],
                air: []
            }
        });

    }

    searchForUser(e) {

        this.setState({query: e.target.value, message: {...this.state.message, user: null}});

        this.api.searchForUser(e.target.value).then(res => {
            let users = res.data.users;
            this.setState({suggestions: users, visible: true, empty: false}, () => {
                if (this.state.suggestions.length === 0) {
                    this.setState({visible: true, empty: true})
                }
            })
        })

    }

    selectUser(user) {
        this.setState({
            message: {...this.state.message, user: user.id},
            visible: false,
            query: user.profile.first_name + ' ' + user.profile.last_name
        })
    }

    typeMessage(e) {

        this.setState({message: e.target.value})
    }

    addMessage() {

        if (!this.state.message.user) {
            this.setState({errors: {...this.state.errors, user: true}});
            return;
        }

        if (!this.state.message.subject) {
            this.setState({errors: {...this.state.errors, subject: true}});
            return;
        }

        let text = $(this.messageEditor).summernote('code');

        if (!text) {
            this.setState({errors: {...this.state.errors, text: true}});
            return;
        }

        this.setState({message: {...this.state.message, loading: true}});

        this.api.addMessage(this.state.message.user, this.state.message.subject, text).then((res) => {

            $(this.messageEditor).summernote('code', '');

            this.setState({
                query: '',
                message: {
                    loading: false,
                    subject: '',
                    user: null
                },
                adding: false,
                messages: [res.data.data, ...this.state.messages]
            }, () => {
                this.selectMessage({id: res.data.data.id})
            })

        })
    }

    selectMessage(message) {

        this.api.getMessage(message.id).then(res => {
            this.setState({selected: res.data.data}, () => {

                let $replies = $('.replies-container');
                $replies.scrollTop($replies[0].scrollHeight);

            });
        });
    }

    getMessages() {

        this.setState({loading: true});

        this.api.getMessages(this.state.page).then(res => {

            let messages = res.data;

            if (messages.data && messages.data.length > 0 && !this.props.mobile) {
                this.selectMessage(messages.data[0])
            }

            this.setState({
                messages: [...this.state.messages, ...messages.data],
                loading: false,
                more: messages.more,
                page: this.state.page++
            })
        })
    }

    reply(receiver) {

        if (!$(this.replyEditor).summernote('code')) {
            return false;
        }

        this.setState({replying: true});

        this.api.addReply({
            text: $(this.replyEditor).summernote('code'),
            receiver
        }, this.state.selected.id).then(res => {

            $(this.replyEditor).summernote('code', '');

            let selected = this.state.selected

            let children =  this.state.selected.children instanceof Array ? this.state.selected.children :  []

            selected.children = [...children, res.data.data]

            this.setState({
                selected,
                replying: false
            }, () => {
                let $replies = $('.replies-container');
                $replies.scrollTop($replies[0].scrollHeight);
            })
        })

    }

    getOppositeUser(message) {
        return this.props.user === message.sender.id ? message.receiver : message.sender;
    }

    deleteConversation() {

        this.api.deleteConversation(this.state.selectDelete).then((res) => {
            if (res.data.code === 0) {

                let messages = this.state.messages.filter(message => {
                    return message.id !== this.state.selectDelete;
                });

                let selected = this.state.selected;

                if (selected && selected.id === this.state.selectDelete) {
                    selected = null;
                }

                this.setState({messages, selected});
            }
        });
        return false;
    }

    selectDelete(id, e) {
        e.stopPropagation();
        e.preventDefault();
        this.setState({selectDelete: id})
    }

    render() {

        let message = '';

        let selected = this.state.selected;
        if (selected) {
            message = <div className="messages">

                <div className="row">

                    <div className="col-md-12">

                        <div className="right-content">

                            <h3>

                                <a href={ '/profile/' + this.getOppositeUser(selected).slug }>
                                    { this.getOppositeUser(selected).profile.first_name } { this.getOppositeUser(selected).profile.last_name }
                                </a>

                                <span className="float-right d-lg-none d-md-block">
                                    <a href="#" onClick={ (e) => {
                                        e.preventDefault();
                                        this.setState({selected: false})
                                    } } title="Close chat">
                                        <i className="fa fa-times"></i>
                                    </a>
                                </span>

                            </h3>

                            <div><strong>Subject: </strong> { selected.subject }</div>
                            <small className="text-muted">{ selected.created_at }</small>

                            <hr/>

                            {/*Replies*/ }
                            <div className="replies-container">

                                <ul className="list-unstyled replies">

                                    <li>
                                        <div dangerouslySetInnerHTML={ {__html: selected.text} }
                                             key={ 'message_reply_' + selected.id }
                                             className={ selected.sender.id === this.props.user ? 'right shadow-sm' : 'left shadow-sm' }/>
                                        <div
                                            className={ selected.sender.id === this.props.user ? 'right text-right transparent-bg' : 'left transparent-bg' }>{ selected.created_at }</div>
                                    </li>

                                    { selected.children instanceof Array ? selected.children.map(reply => {

                                        const right = reply.sender.id === this.props.user;

                                        return <li>
                                            <div key={ 'message_reply_' + reply.id }
                                                 className={ 'shadow-sm ' + (right ? 'right' : 'left') }
                                                 dangerouslySetInnerHTML={ {__html: reply.text} }/>
                                            <div
                                                className={ right ? 'right text-right transparent-bg' : 'left text-left transparent-bg' }>{ reply.created_at }</div>
                                        </li>

                                    }) : null }


                                </ul>

                            </div>

                            <hr/>

                            {/*Editor*/ }
                            <div>

                                <textarea id="reply-editor" ref={ (ref) => this.replyEditor = ref } cols="30"
                                          rows="10"/>

                                <div className="mt-4 mb-4">

                                    <button className="btn btn-blue"
                                            onClick={ () => this.reply(this.getOppositeUser(selected).id) }>
                                        { this.state.replying ? (
                                            <Loader type={ 'ThreeDots' } height={ 20 } width={ 40 }
                                                    color="#fff"/>) : 'Add Reply' }
                                    </button>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        }

        return (
            <div className="container basic-layout">

                {/*Header*/ }
                <div className="row">

                    <div className="col-12">

                        <div className="row">

                            <div className="col-lg-6 col-md-7">

                                <h1 className="float-left">

                                    <span className="align-middle">Messenger</span>

                                </h1>

                            </div>

                            <div className="col-lg-6 col-md-5 text-md-right">

                                <div className="row h-100">

                                    <div className="col-sm-12 my-auto">

                                        <button className="btn btn-info"
                                                onClick={ () => this.setState({adding: true}) }>
                                            New Message
                                        </button>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr/>

                    </div>

                </div>

                {/*New Message*/ }
                <div className="row new-message" hidden={ !this.state.adding }>

                    <div className="col-12">

                        <h3 className="text-muted">Compose New Message

                            <small className="float-right">

                                <button className="btn btn-dark circle"
                                        onClick={ () => this.setState({adding: false}) }>
                                    <i className="fas fa-times"></i>

                                </button>

                            </small>

                        </h3>

                        <div className="default-form-layout">

                            {/*Receiver*/ }
                            <div className="form-group" style={ {position: 'relative'} }>
                                <label htmlFor="subject">Receiver</label>
                                <DebounceInput
                                    value={ this.state.query }
                                    className="form-control"
                                    minLength={ 3 }
                                    placeholder="Search for users"
                                    debounceTimeout={ 400 }
                                    onChange={ this.searchForUser }
                                />

                                <span className="text-danger ml-3"
                                      hidden={ !this.state.errors.user || this.state.message.user }>This field is required *</span>

                                <div className="search-results"
                                     hidden={ !this.state.visible }>

                                    <ul className="list-unstyled text-left ">

                                        <li className="mt-2" hidden={ !this.state.empty }>No users found</li>

                                        { this.state.suggestions.map((user, key) => {
                                            return (
                                                <li className="media mb-3 mt-3 pointer" key={ key }
                                                    onClick={ () => this.selectUser(user) }>

                                                    <img className="mr-3 circle"
                                                         src={ user.profile.picture || '/images/avatar.jpg' }
                                                         alt="Profile Picture" width={ '40px' } height={ '40px' }/>
                                                    <div className="media-body">
                                                    <span
                                                        className="mt-0 mb-1">{ user.profile.first_name } { user.profile.last_name }</span>
                                                    </div>
                                                </li>)
                                        }) }

                                    </ul>
                                </div>

                            </div>

                            {/*Subject*/ }
                            <div className="form-group">

                                <label htmlFor="subject">Subject</label>
                                <input type="text"
                                       className="form-control"
                                       id="subject" value={ this.state.message.subject }
                                       name="subject"
                                       placeholder="Conversation Subject"
                                       onChange={ (e) => this.setState(this.setState({
                                           message: {
                                               ...this.state.message,
                                               subject: e.target.value
                                           }
                                       })) }/>

                                <span className="ml-3 text-danger"
                                      hidden={ !this.state.errors.subject || this.state.message.subject }>This field is required *</span>

                            </div>

                            {/*Message*/ }
                            <div className="form-group">

                                {/*ADD EDITOR*/ }
                                <textarea id="message-editor" ref={ (ref) => this.messageEditor = ref } cols="30"
                                          rows="10"></textarea>


                                <span className="text-danger ml-3"
                                      hidden={ !this.state.errors.text || this.state.message.text }>This field is required *</span>
                            </div>

                            <div className="text-center mt-2 mb-2">
                                <button className="btn btn-blue"
                                        onClick={ this.addMessage }>
                                    { this.state.message.loading ? (
                                        <Loader type={ 'ThreeDots' } height={ 20 } width={ 20 }
                                                color="#fff"/>) : 'Send' }
                                </button>
                            </div>

                        </div>

                    </div>
                </div>

                {/*Loading Animation*/ }
                <li className="list-unstyled text-center" hidden={ !this.state.loading }>
                    <Loader type="Oval"
                            color="#00BFFF"
                            height={ 50 }
                            width={ 50 }
                            className="m-auto"/>
                </li>

                <div className="row" hidden={ this.state.adding }>

                    {/*Conversations list*/ }
                    <div className={ 'col-md-4 d-md-block ' + (this.state.selected ? 'd-none' : '') }>

                        <ul className="conversations ml-0 pl-0">

                            { this.state.messages.map((message, key) => {
                                return (
                                    <li className={ 'media pointer mb-4 ' +
                                    (selected && selected.id === message.id ? 'selected-conversation' : '')
                                    }
                                        onClick={ () => this.selectMessage(message) } key={ key }>

                                        <img className="avatar-image circle mr-3"
                                             src={ this.getOppositeUser(message).profile.picture || '/images/avatar.jpg' }
                                             alt="Profile Picture"/>

                                        <div className="media-body">

                                            <h5 className="mt-0 mb-1">
                                                { this.getOppositeUser(message).profile.first_name } { this.getOppositeUser(message).profile.last_name }

                                                <button
                                                    className="float-right btn rounded-circle text-danger"
                                                    data-toggle="modal" data-target="#delete-conversation"
                                                    onClick={ (e) => this.selectDelete(message.id, e) }>

                                                    <i className="far fa-trash-alt"></i>

                                                </button>
                                            </h5>

                                            { message.subject }
                                            <div>
                                                <span className="text-muted time">{ message.created_at }</span>
                                            </div>
                                        </div>

                                    </li>
                                );
                            }) }

                            <li className="text-muted list-unstyled"
                                hidden={ this.state.more || this.state.messages.length > 0 || this.state.loading }>
                                No messages
                            </li>

                        </ul>

                        {/*Confirm Delete*/ }
                        <div className="modal fade show" id="delete-conversation" tabIndex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        Do you really want to delete this conversation?
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No
                                        </button>
                                        <button type="button" data-dismiss="modal" onClick={ this.deleteConversation }
                                                className="btn btn-primary">Yes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className={ 'col-md-8 d-md-block' + (!this.state.selected ? 'd-none' : '') }>

                        { message }

                    </div>

                </div>

            </div>
        )
    }
}

