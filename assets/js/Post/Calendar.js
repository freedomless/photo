import React, { Component } from 'react'
import { Datepicker, START_DATE, END_DATE } from '@datepicker-react/styled'

export class Calendar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            startDate: null,
            endDate: null,
            focusedInput: 'startDate',
            visible: false
        };

    }

    handleDatesChange(data) {

        this.setState(data, () => {

            if (this.state.startDate && this.state.endDate) {
                this.setState({visible: false});
                this.props.updateDate(this.state);
            } else if (this.state.startDate === null && this.state.endDate === null) {
                this.setState({startDate: null, endDate: null, visible: false, focusedInput: 'startDate'});
                this.props.updateDate({startDate: null, endDate: null});
            } else {
                this.setState({...data, focusedInput: END_DATE});
            }
        })
    }

    render() {

        return (
            <div>

                <button
                    className={ 'btn btn-' + (this.state.visible ? 'info' : 'primary') }
                    onClick={ () => this.setState({visible: !this.state.visible}) }>Calendar
                </button>

                <div hidden={ !this.state.visible }>
                    <Datepicker
                        onClose={ () => this.setState({visible: false}) }
                        onDatesChange={ (data) => this.handleDatesChange(data) }
                        maxBookingDate={ new Date() }
                        startDate={ this.state.startDate } // Date or null
                        endDate={ this.state.endDate } // Date or null
                        focusedInput={ this.state.focusedInput } // START_DATE, END_DATE or null
                    />
                </div>

            </div>
        )

    }
}

