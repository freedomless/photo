import React, { Component } from 'react';
import { DateCreated } from './DateCreated';
import { Image } from '../../Mixin/Image';
import { Publisher } from './Publisher';
import { Nude } from './Nude';
import { Link } from '../../Mixin/Link';
import CountdownTimer from 'react-component-countdown-timer';
import { Options } from './Options';

export class Post extends Component {
    constructor(props) {
        super(props);
    }

    getUrl() {

        if (this.props.settings.category || this.props.settings.album || this.props.settings.user) {
            return `/photo/${ this.props.settings.page }/${ this.props.post.id }/${ this.props.settings.category || this.props.settings.album || this.props.settings.user }`;
        } else if (this.props.post.series) {
            return `/photo/${ this.props.settings.page }/${ this.props.post.id }/${ this.props.post.series }`;
        } else {
            return `/photo/${ this.props.settings.page }/${ this.props.post.id }`;
        }

    }

    render() {

        let post = this.props.post;
        let seconds = null;

        if (this.props.post.deletion_date || (this.props.post.product && this.props.post.product.commission_changing_on)) {

            let time = this.props.post.deletion_date || this.props.post.product.commission_changing_on;

            let date = new Date(time);
            seconds = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());

            let now = new Date();

            let now_utc = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

            seconds = Math.round((seconds - now_utc) / 1000)

        }

        return (

            <div className="photo col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center mt-5 mb-5 store-product">


                <div className="thumb-container store-product-image" onClick={ this.props.open }>

                    <Link url={ this.getUrl() } onClick={ (e) => {
                        e.preventDefault();
                    } }>

                        <Image src={ post.image.thumbnail } title={ post.title }/>

                        <Nude hidden={ this.props.settings.nude || post.is_nude !== true }/>

                    </Link>

                </div>

                <div className="d-flex justify-content-center store-product-publisher">

                    <div className="flex-grow-1 ml-5">
                        <Publisher
                            hidden={ this.props.settings.page === 'portfolio' || this.props.settings.page === 'published' }
                            user={ post.user }/>

                    </div>
                    <div className="mr-5">
                        { this.props.page === 'store' || this.props.page === 'user-store' ? <Options post={ post }
                                                                 sizes={ this.props.sizes }
                                                                 materials={ this.props.materials }/> : null }

                    </div>
                </div>


                <DateCreated hidden={ this.props.settings.page !== 'awarded' }
                             date={ post.awards ? post.awards[0].urlized_date : null }/>


                { this.props.settings.page === 'store' ? seconds > 86400 ? <p className="pb-1 badge badge-pill badge-primary store-product-timout">Will be available
                                                                                       for { Math.round(seconds / 60 / 60 / 24) } days {this.props.post.product && this.props.post.product.commission_changing_on ? 'on this price' : ''}</p> : seconds ?
                    <p className="pb-1 badge badge-pill badge-primary">Last chance {this.props.post.product && this.props.post.product.commission_changing_on ? 'on this price' : ''}</p> : null  : null}

                { this.props.settings.page === 'store' && this.props.post.deletion_date ?
                    <div className="d-none"><CountdownTimer onEnd={ this.props.remove } count={ seconds }/>
                    </div> : null }


            </div>
        )
    }
}

