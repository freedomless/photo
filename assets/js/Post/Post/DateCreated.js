import React, { Component } from 'react';

export class DateCreated extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (<div hidden={this.props.hidden} className="photo-date">{ this.props.date }</div>);
    }
}

