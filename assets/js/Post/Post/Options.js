import React, { Component } from 'react';
import { StoreAPI } from '../../API/StoreAPI';
import { Button, PopoverBody, UncontrolledPopover } from 'reactstrap';
import { Loader } from '../../Mixin/Loader';

export class Options extends Component {

    constructor(props) {
        super(props);

        this.state = {
            material: 1,
            frame: '1',
            MATWidth: '20',
            size: 1,
            total: '0.00',
            initialTotal: '0.00',
            quantity: this.props.quantity > 0 ? this.props.quantity : 1,
            isOpen: false,
            updating: false,
            discount: false,
            added: false
        };

        this.api = new StoreAPI();

    }

    hide(event) {
        
        let specifiedElement = document.getElementsByClassName('popover');

        if (!specifiedElement.length) {
            return
        }

        let isClickInside = specifiedElement[0].contains(event.target);

        if (!isClickInside) {
            this.setState({isOpen: false})
        }
    }

    componentDidMount() {

    }

    formatMoney(amount, decimalCount = 2, decimal = '.', thousands = ',') {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? '-' : '';

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : '');
        } catch (e) {
        }
    };

    addToCart(callback) {

        this.setState({isOpen: false})

        this.api.addToCard({
            product: this.props.post.for_sale,
            material: this.state.material,
            size: this.state.size,
            quantity: this.state.quantity
        }).then(() => {

            iziToast.show({
                theme: '',
                color: 'green',
                icon: 'far fa-image',
                message: 'Product added to cart!',
                position: 'bottomCenter', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });

            let event = new CustomEvent('add-to-cart');
            document.dispatchEvent(event);

            if (callback instanceof Function) {
                callback();
            }
        });

    }

    buyNow() {
        this.setState({isOpen: false})

        this.addToCart(() => {
            window.location.href = '/cart'
        });
    }

    updateProductQuantity(quantity) {

        quantity = Math.round(quantity);
        quantity = quantity > 0 ? quantity : 1;

        this.setState({quantity, total: quantity * this.state.initialTotal});
    }

    updateProductMaterial(material) {

        this.setState({material}, () => {
            this.calculatePrice()
        });
    }

    updateProductSize(size) {

        this.setState({size}, () => {
            this.calculatePrice()
            this.calculateFrameAndPrint($('#imageForView'), this.state.frame);
        })
    }



    calculatePrice() {

        this.setState({updating: true});

        this.api.calculatePrice(this.props.post.for_sale, this.state.quantity, this.state.size, this.state.material).then(res => {

            this.setState({
                total: res.data.data.total,
                initialTotal: parseFloat(res.data.data.total.replace(',', '')) / this.state.quantity,
                updating: false,
                discount: res.data.data.discount
            });

        })
    }

    chooseMAT( color) {
        let image = $('#imageForView');
        image.css({ 'background-color': color });
    }

    calculateFrameAndPrint(image, frame) {
        let imgWidth = image.width();
        let imgHeight = image.height();
        let aspectRatio = imgWidth / imgHeight;
        let frameWidth = 20;
        let MATWidth = this.state.MATWidth;
        let printWidth = 0;
        let pixels = 0;

        // printWidth = 600 - ((this.state.size - 1) * 200);

        if ( aspectRatio >= 1) {
            printWidth = 600 - ((this.state.size - 1) * 200);
            printWidth *= aspectRatio;
        }
 
        if (frame === '1') {
            printWidth = 600 - ((this.state.size - 1) * 200);
            let width = frameWidth * 2 + MATWidth * 2 + parseInt(printWidth);
            console.log( 'frameWidth: ' + frameWidth );
            console.log( 'MATWidth: ' + MATWidth );
            console.log( 'printWidth: ' + printWidth );

            pixels = 400 / width;
            image.css({ 
                'border': (pixels * frameWidth) + 'px solid black',
                'padding': (pixels * MATWidth) + 'px'
            });
            console.log( (pixels) + 'px solid black' );
            console.log( (pixels) + 'px solid black' );
        }
    }

    chooseMATSize(MATWidth) {
        let image = $('#imageForView');
        this.setState({MATWidth}, () => {
            MATWidth,
            this.calculateFrameAndPrint(image, this.state.frame);
        });
    }

    chooseFrame(frame) {
        this.setState({frame}, () => {
            frame
        });

        let image = $('#imageForView');
        let withFrame = $('#withFrame');

        if ( frame === '1' ) {
            // with Frame
            withFrame.css({ 'display': 'block' });
            this.calculateFrameAndPrint(image, frame);
        }
        if ( frame === '2' ) {
            // with aluminium base
            image.css({ 
                'border': 'none', 
                'padding': '0px'
            });
            withFrame.css({ 'display': 'none' });
        }
        if ( frame === '3' ) {
            // with canvas
            withFrame.css({ 'display': 'block' });
            this.calculateFrameAndPrint(image, frame);
        }
    }

    render() {
        const materials = this.props.materials.map((value, index) => {
            return (<option value={ value.id } key={ index }>{ value.name }</option>)
        });
        const sizes = this.props.sizes.map((value, index) => {

            let height = Math.round(this.props.post.opposite * value.width);

            height = height > 0 ? height : value.height

            return (<option value={ value.id }
                            key={ index }>{ value.width } x { height }</option>)
        });
        const material = this.props.materials.filter((val) => {
            return val.id === parseInt(this.state.material)
        })[0];

        return (
            <div className="position-relative store-product-cart" id={ 'wrapper-' + this.props.post.id }>
                <Button
                    id={ 'popover-' + this.props.post.id } type="button"
                    className={'btn-printstore'}
                >
                    <i className="fa fa-shopping-cart"/>
                </Button>
                <UncontrolledPopover placement="bottom"
                                     isOpen={ this.state.isOpen }
                                     delay={ 200 }
                                     target={ 'popover-' + this.props.post.id }
                                     toggle={ () => {
                                         this.setState({isOpen: !this.state.isOpen}, () => {
                                             if (this.state.isOpen) {
                                                 document.addEventListener('click', this.hide.bind(this));
                                             } else {
                                                 document.removeEventListener('click', this.hide.bind(this));
                                             }

                                         })
                                         this.calculatePrice()
                                         this.calculateFrameAndPrint($('#imageForView'), this.state.frame);
                                     }
                                     }>
                    <PopoverBody>
                        <div className="pppp">
                            <div className="row">
                                <div className="col-8 text-center">
                                    <img id="imageForView" src={ this.props.post.image.thumbnail } 
                                        style={{
                                            width: '400px', 
                                            border: 'solid black 15px',
                                            marginTop: '25px',
                                            padding: '30px',
                                            background: 'white'
                                        }} 
                                    />
                                </div>

                                <div className="col-4">
                                    <div className="form-group">
                                        <label htmlFor="">Quantity</label>
                                        <input className="form-control"
                                            min={ 1 }
                                            type="number"
                                            defaultValue={ this.state.quantity }
                                            onChange={ (e) => this.updateProductQuantity(e.target.value) }
                                            placeholder="Quantity"/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="">Print Size</label>
                                        <select className="form-control" defaultValue={ this.state.size }
                                                onChange={ e => this.updateProductSize(e.target.value) }>
                                            { sizes }
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="">Base</label>
                                        <select className="form-control" defaultValue={ this.state.material }
                                                onChange={ e => this.chooseFrame(e.target.value) }
                                        >
                                            <option value="1">With Frame</option>
                                            <option value="2">Aluminium Base</option>
                                            <option value="3">Canvas</option>
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="">Media</label>
                                        <select className="form-control" defaultValue={ this.state.material }
                                                onChange={ e => this.updateProductMaterial(e.target.value) }>
                                            { materials }
                                        </select>
                                    </div>

                                    <div id="withFrame">
                                        <div className="form-group">
                                            <label htmlFor="">MAT</label>
                                            <select className="form-control" defaultValue={ this.state.material }
                                                    onChange={ e => this.chooseMAT(e.target.value) }
                                            >
                                                <option value="white">White</option>
                                                <option value="green">Green</option>
                                            </select>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="">MAT Size</label>
                                            <select className="form-control" defaultValue={ this.state.material }
                                                    onChange={ e => this.chooseMATSize(e.target.value) }
                                            >
                                                <option value="20">20 mm</option>
                                                <option value="25">25 mm</option>
                                            </select>
                                        </div>
                                    </div>

                                    <span className="text-muted">
                                        <i className="fas fa-info-circle"></i> {material ? material.description : null}
                                        <div><a href="/product-details" target="_blank" className="text-info">Product Details</a></div>
                                    </span>

                                    <h5>
                                        { this.state.discount > 0 ? (
                                            <div><small className="text-muted">Discount { this.state.discount } &euro;</small>
                                            </div>) : null }
                                        <strong>Price <span className="text-muted">{ this.state.total } &euro;</span></strong>
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div className="mt-5 d-flex justify-content-center align-content-around btn-group">
                                <button className="btn btn-blue" onClick={ this.addToCart.bind(this) }>Add to cart
                                </button>

                                <button className="btn btn-success" onClick={ this.buyNow.bind(this) }>Buy Now
                                </button>
                            </div>
                        </div>

                        <div className="updating rounded" hidden={ !this.state.updating }>

                            <Loader/>

                        </div>
                    </PopoverBody>
                </UncontrolledPopover>
            </div>
        )
    }
}
