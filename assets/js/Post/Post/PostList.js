import React, { Component } from 'react';
import { Post } from './Post';
import { PostSinglePage } from '../Single/PostSinglePage';

export class PostList extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (

            <div className="row align-items-center photos store-products">

                { this.props.data.map((data, index) =>

                    <Post key={ index }
                          sizes={ this.props.sizes }
                          materials={ this.props.materials }
                          page={this.props.page}
                          open={ () => this.props.open(index) }
                          remove={ () => this.props.remove(index) }
                          settings={ this.props.settings } post={ data }/>) }

            </div>
        )
    }
}

