import React, {Component} from 'react';

export class PublishedStamp extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div hidden={this.props.hidden}
                     className={'text-muted small published-stamp '
                     + (this.props.hidden ? '' : 'd-none d-lg-block')}>PUBLISHED</div>
        )
    }
}

