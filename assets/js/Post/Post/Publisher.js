import React, { Component } from 'react';

export class Publisher extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let user = this.props.user;

        return (
            <div hidden={ this.props.hidden } className="owner">
                {user ? (
                    <a href={ '/profile/' + user.slug } className="text-primary">
                        © { user.profile.first_name } { user.profile.last_name }
                    </a>
                ) : null}
            </div>
        )
    }
}
