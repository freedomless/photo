import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { PostAPI } from '../API/PostAPI';
import { Filters } from './Filters';
import { PostList } from './Post/PostList';
import { Empty } from '../Mixin/Empty';
import { PostSinglePage } from './Single/PostSinglePage';
import { Loader } from '../Mixin/Loader';
import { Calendar } from './Calendar';
import moment from 'moment';

export class InfinitePost extends Component {

    constructor(props) {

        super(props);

        this.state = {
            posts: this.props.posts ? this.props.posts : [],
            page: this.props.posts ? 2 : 1,
            more: this.props.hidden !== undefined ? this.props.hidden : true,
            post: null,
            data: this.props.posts ? this.props.posts : [],
            filters: this.props.filters || {},
            hidden: this.props.hidden !== undefined ? this.props.hidden : true,
            visible: false,
            favorite: null,
            loading: false,
            date: null,
            url: window.location.href
        };

        this.api = new PostAPI();

        document.addEventListener('follow.action', (e) => {
            this.updateFollow(e.detail.state.slug, e.detail.state.state)
        });

    }

    loadFactory(page, filters) {

        switch (this.props.page) {
            case 'favorites':
                return this.api.getFavorites(page);
            case 'popular':
                return this.api.getPopular(page);
            case 'latest':
                return this.api.getLatest(page, filters);
            case 'awarded':
                return this.api.getAwarded(page, this.state.date);
            case 'following':
                return this.api.getFollowing(page);
            case 'published':
                return this.api.getPublished(page, this.props.user);
            case 'portfolio':
                return this.api.getPortfolio(page, this.props.user);
            case 'category':
                return this.api.getByCategory(page, this.props.category);
            case 'tag':
                return this.api.getByTag(page, this.props.tag);
            case 'award':
                return this.api.getAwarded(page);
            case 'store':
                return this.api.getForSale(page, filters);
            case 'user-store':
                return this.api.getForSale(page, {'user': this.props.user});
            default:
                return false
        }

    }

    loadMore() {

        if (this.state.loading || !this.state.more) {
            return;
        }

        this.setState({loading: true});

        const promise = this.loadFactory(this.state.page, this.state.filters);

        if (promise) {
            promise.then((res) => {

                let postResults = res;

                let loaded = res.data.posts;

                let posts = this.state.posts;

                this.setState({
                    page: this.state.page + 1,
                    more: postResults.data.more,

                    loading: false,
                    posts: [...posts, ...loaded]
                }, () => {
                    this.setState({
                        hidden: this.state.posts.length !== 0
                    })
                });

            });
        } else {
            this.setState({
                more: false,
                loading: false
            });
        }

    }

    addPosts(posts, more) {

        let current = this.state.posts;

        this.setState({
            page: this.state.page + 1,
            posts: [...current, ...posts],
            more: more
        });
    }

    openSingle(id) {

        let post = this.state.posts[id];

        this.setState({post: post});

        document.querySelector('html').classList.add('no-scroll');
    }

    closeSinglePostPage() {

        this.setState({post: null});

        window.history.pushState({}, '', `${ this.state.url }`);

        document.querySelector('html').classList.remove('no-scroll');

    }

    updateFilters(filters) {

        filters = $.extend({}, filters);

        for (let prop in filters) {
            if (filters[prop] === '' || filters[prop].length === 0) {
                delete filters[prop]
            }
        }

        let url = this.state.url.split('?')[0];

        window.history.pushState({}, '', `${ url }?${ $.param(filters || {}) }`);
        
        
        console.log(url)
        
        this.setState({
            page: 1,
            more: true,
            posts: [],
            hidden: true,
            filters,
            url: `${ url }?${ $.param(filters || {}) }`
        });
    }

    updateFavorite(slug, favorite) {
        let posts = this.state.posts.map(post => {
            if (post.slug === slug) {
                post.favorites = favorite ? [[]] : [];
            }

            return post;
        });

        this.setState({posts: posts});
    }

    updateDate(data) {

        let filter = {
            start_date: moment(data.startDate).format('YYYY-MM-DD'),
            end_date: moment(data.endDate).format('YYYY-MM-DD')
        };

        if (data.startDate === null || data.endDate === null) {
            filter = {}
        }

        this.setState({
            page: 1,
            more: true,
            posts: [],
            hidden: true,
            date: filter
        });
    }

    updateFollow(slug, follow) {
        let posts = this.state.posts.map(post => {
            if (post.user.id === slug) {
                post.user.followers = follow ? [[]] : [];
            }
            return post;
        });

        this.setState({posts: posts});
    }

    remove(index) {

        let posts = this.state.posts;
        posts.splice(index, 1);

        this.setState({posts})

    }

    render() {

        return (
            <div>
                {/*Filters*/ }
                { this.props.filter === true ? <Filters key={ 0 }
                                                        sorting={this.props.sorting}
                                                        categories={ this.props.categories }
                                                        filters={ this.props.filters }
                                                        update={ this.updateFilters.bind(this) }/> : null }

                { this.props.page === 'awarded' ? <Calendar updateDate={ data => this.updateDate(data) }/> : null }

                {/*Posts infinite scroll*/ }
                <InfiniteScroll
                    loadMore={ this.loadMore.bind(this) }
                    hasMore={ this.state.more }
                    threshold={ 1000 }
                    loader={ <Loader key={ 1 }/> }>

                    {/*Posts list*/ }
                    <PostList key={ 2 } settings={ this.props }
                              remove={this.remove.bind(this)}
                              page={this.props.page}
                              sizes={ this.props.sizes }
                              materials={ this.props.materials }
                              open={ this.openSingle.bind(this) }
                              data={ this.state.posts }/>

                    {/* If empty show message */ }
                    <Empty key={ 3 } hidden={ this.state.hidden }>No Photos.</Empty>

                </InfiniteScroll>

                {/* If post selected add single post page */ }
                { this.state.post ? <PostSinglePage key={ 4 }
                                                    post={ this.state.post }
                                                    posts={ this.state.posts }
                                                    settings={ this.props }
                                                    more={this.state.more   }
                                                    sizes={ this.props.sizes }
                                                    materials={ this.props.materials }
                                                    addPosts={ this.addPosts.bind(this) }
                                                    filters={ this.state.filters }
                                                    page={ this.state.page }
                                                    close={ this.closeSinglePostPage.bind(this) }/> : null }

            </div>
        )
    }
}

