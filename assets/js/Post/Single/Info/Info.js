import React, { Component } from 'react';
import { PostAPI } from '../../../API/PostAPI';

export class Info extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: null,
            loading: false
        };

        this.details = [];
        this.api = new PostAPI();
    }

    componentDidMount() {
        this.loadInfo();
        document.addEventListener('reload-info', this.loadInfo.bind(this))
    }

    componentWillUnmount() {
        document.removeEventListener('reload-info', this.loadInfo.bind(this))
    }

    loadInfo() {

        this.setState({visible: !this.state.visible});

        if (this.state.data === false) {
            return;
        }

        this.setState({loading: true});

        this.api.getInfo(this.props.post.id).then(res => {

            this.setState({visible: true, loading: false, data: res.data}, () => {

                let allowed = [
                    'camera', 'lens', 'focal_length', 'shutter_speed', 'iso', 'aperture',
                    'exposure', 'flash', 'filter', 'tripod', 'date'
                ];

                this.details = [];

                for (let prop in res.data) {

                    if (allowed.indexOf(prop) == -1 || !res.data[prop]) {
                        continue;
                    }

                    let title = prop.replace('_', ' ');
                    title = title.charAt(0).toUpperCase() + title.slice(1);
                    let value = res.data[prop];

                    this.details.push({title, value})

                }

            });
        });
    }

    render() {

        return this.state.data ?
            <div className="content photo-info shadow-sm">
                <div className="post-info p-3">
                    <h3>Info:</h3>
                    <hr/>

                    <div>
                        <h5 className="text-muted mb-3">
                            { this.state.data.title }
                        </h5>
                        <p className="mb-3">
                            { this.state.data.description }
                        </p>

                        { this.state.data.title ? <hr/> : null }

                        <h5 className="mt-5 mb-5 text-muted">Stats</h5>
                        <div>
                            <strong>Views:</strong>
                            <span className="text-muted float-right">
                                { this.state.data.views }
                            </span>
                            <hr/>
                        </div>
                        <div hidden={ !(this.state.data.tags instanceof Array) || this.state.data.tags.length === 0 }
                             className={ this.state.data.tags.length > 0 ? 'd-flex' : '' }>
                            <strong>Tags:</strong>
                            <div
                                className="text-muted d-flex flex-wrap">{ this.state.data.tags instanceof Array ? this.state.data.tags.map((tag, key) => {
                                return <a className="ml-3" href={ '/tag/' + tag.name } key={ key }>{ tag.name }</a>
                            }) : null }</div>
                        </div>
                        <hr hidden={ !(this.state.data.tags instanceof Array) || this.state.data.tags.length === 0 }/>
                        <div>
                            <strong>Category:</strong>
                            <span className="text-muted float-right">{ this.state.data.category.name }</span>
                        </div>
                        <hr/>

                        <h5 className="mt-5 mb-5 text-muted" hidden={ this.details.length === 0 }>Details</h5>
                        <div className="row">
                            { this.details.map((detail) => {

                                return (
                                    <div className="col-12 mb-3" key={ Math.random() }>
                                        <strong>{ detail.title }</strong>
                                        <span className="text-muted float-right">{ detail.value }</span>
                                    </div>
                                )
                            }) }
                        </div>
                        <h5 className="mt-5 mb-5 text-muted"
                            hidden={ !(this.state.data.favorites instanceof Array) || this.state.data.favorites.length === 0 }>
                            Favorites
                            ({ this.state.data.favorites.length > 0 ? this.state.data.favorites.length : '' })
                        </h5>
                        <div className="favorite mb-5 pb-5"
                             hidden={ !(this.state.data.favorites instanceof Array) || this.state.data.favorites.length === 0 }>
                            { this.state.data.favorites instanceof Array ? this.state.data.favorites.map((favorite, key) => {
                                return (
                                    <a href={ '/profile/' + favorite.user.slug } key={ key }
                                       className="d-inline-block ml-1 mr-1">
                                        <img src={ favorite.user.profile.picture || '/images/avatar.jpg' }
                                             alt="Profile Picture"
                                             className="circle shadow favs-pp mb-2"
                                             title={ favorite.user.profile.first_name + ' ' + favorite.user.profile.last_name }
                                        />
                                    </a>
                                )
                            }) : null }
                        </div>
                    </div>

                </div>
            </div>
            : null;

    }
}

