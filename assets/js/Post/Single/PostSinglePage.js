import React, { Component } from 'react';
import { Bar } from './Bar/Bar';
import { Button } from '../../Mixin/Button';
import { Image } from '../../Mixin/Image';
import { PostAPI } from '../../API/PostAPI';
import { InfiniteComments } from './Comment/InfiniteComments';
import { Info } from './Info/Info';
import { FavoriteAPI } from '../../API/FavoriteAPI';
import { UserAPI } from '../../API/UserAPI';
import { Options } from './Options/Options';

export class PostSinglePage extends Component {

    constructor(props) {
        super(props);

        let follow = this.props.post ? this.props.post.follow : false;
        let favorite = this.props.post ? this.props.post.favorite : false;

        this.state = {
            posts: [],
            post: null,
            page: this.props.page ? this.props.page : 1,
            next: false,
            prev: false,
            info: false,
            more: this.props.more !== undefined ? this.props.more : true,
            follow: follow,
            favorite: favorite,
            loading: false,
            comments: false,
            disabled: false,
            options: false
        };

        this.api = new PostAPI();
        this.favoriteApi = new FavoriteAPI();
        this.followApi = new UserAPI();

        this.onBackButton = this.onBackButton.bind(this);
    }

    verify(err) {

        if (err.response.status === 401 || err.response.status === 403) {
            iziToast.show({
                color: 'red',
                message: 'You have to verify!',
                position: 'bottomLeft', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });
        }

    }

    componentDidMount() {

        let large = $(this.large.ref).fadeOut(0);

        //If photo loaded not from InfinitePost load more ids

        //If post comes form InfinitePost component load it.
        if (this.props.posts || this.props.post) {
            this.setState({
                posts: this.props.posts || [],
                post: this.props.post
            }, () => {

                if (this.props.settings.full) {
                    this.loadMore()
                }

                this.updateHistory(true, this.props.settings.single);

                if (this.props.posts) {
                    this.api.addView(this.state.post.id);
                }

                this.cacheSiblings();

                large.fadeIn(() => {
                    this.allowNextPrev();

                    large.on('load', () => {
                        large.delay(200).fadeIn();
                        //Check if next prev image to show buttons
                        this.allowNextPrev();
                    });

                });

            });

        }

        if (window.history && window.history.pushState) {
            window.addEventListener('popstate', this.onBackButton)
        }

        //Make nav make dark
        document.querySelector('.navbar').classList.add('photo-preview');

        $(document).on('keydown', (e) => {

            switch (e.which) {
                case 37: // left
                    if (this.state.prev) {
                        this.getPost(0);
                    }
                    break;

                case 39: // right
                    if (this.state.next) {
                        this.getPost(1);
                    }
                    break;

                case 27:
                    this.props.close();
                    break;

                default:
                    return; // exit this handler for other keys
            }
            e.preventDefault(); //
        })

    }

    componentWillUnmount() {
        //Remove event for history
        window.removeEventListener('popstate', this.onBackButton);

        //Remove black navbar class
        document.querySelector('.navbar').classList.remove('photo-preview');

        if (this.props.full) {
            this.api.getFavoriteAndFollow()
        }
    }

    updateFavorite(slug, favorite) {

        let posts = this.state.posts.map(post => {

            if (post.id === slug) {
                post.favorite = !!favorite;
            }

            return post;
        });

        this.setState({posts: posts});

    }

    updateFollow(slug, follow) {

        let posts = this.state.posts.map(post => {
            if (post.user.id === slug) {
                post.follow = !!follow;
            }
            return post;
        });

        this.setState({posts: posts});
    }

    getCurrentIndex() {

        let index = -1;

        this.state.posts.map((val, i) => {

            if (val.id === this.state.post.id) {
                index = i;
            }
        });

        return index;
    }

    loadMore() {

        if (this.state.more) {

            this.api
                .loadMoreIds(
                    this.props.settings,
                    this.state.page,
                    this.props.filters,
                    this.state.post.id)
                .then(res => {

                    const data = res.data.data;
                    const loaded = data.items;
                    let ids = [];

                    for (let i = 0; i < loaded.length; i++) {
                        ids.push(loaded[i].id)
                    }

                    this.setState({
                        page: this.state.page + 1,
                        more: data.has_more
                    });

                    this.api.getFavoriteAndFollow(ids).then((res) => {

                        let result = res.data.data;
                        let favorites = result.favorite;
                        let follows = result.follow;

                        for (let i = 0; i < loaded.length; i++) {
                            loaded[i].favorite = favorites.indexOf(loaded[i].id) > -1;
                            loaded[i].follow = follows.indexOf(loaded[i].id) > -1;
                        }

                        let selected = this.state.post;

                        this.setState({
                            follow: follows.indexOf(selected.id) > -1,
                            favorite: favorites.indexOf(selected.id) > -1,
                            posts: [...this.state.posts, ...loaded]
                        }, () => {
                            this.allowNextPrev();
                        })

                        if (!this.props.settings.full) {
                            this.props.addPosts(loaded, data.has_more)
                        }

                    });

                });
        }
    }

    getPost(dir, history = true) {

        //If post is loading return false
        if (this.state.loading === true) {
            return false;
        }

        //Set state to loading and disable next and prev button
        this.setState({loading: true, next: false, prev: false});

        //Get index of photo
        let index = this.getCurrentIndex();

        //If direction is 0 get prev else next
        index = dir === 0 ? index - 1 : index + 1;

        //If loaded post is close to end of loaded posts load more
        if (index >= this.state.posts.length - 10 && index <= this.state.posts.length) {
            this.loadMore();
        }

        let post = this.state.posts[index];

        this.api.addView(post.id);

        this.updatePost(post, history);

    };

    updatePost(post, history) {

        if (!this.large) {
            return;
        }

        let large = $(this.large.ref);

        //Fade out image and then update state
        large.fadeOut(() => {

            this.setState({
                post: post,
                loading: false,
                next: false,
                prev: false,
                follow: post.follow,
                favorite: post.favorite

            }, () => {

                //Reload comments/info if open
                this.reloadSide();

                if (history) {
                    //Change history
                    this.updateHistory();
                }

                //Cache new post
                this.cacheSiblings();

            });

        });

    }

    cacheSiblings() {

        //Get index of photo
        let index = this.getCurrentIndex();

        //Get prev post
        let prev = index > -1 ? this.state.posts[index - 1] : null;
        //Get next post
        let next = index > -1 ? this.state.posts[index + 1] : null;

        if (prev) {
            this.prevImage.ref.src = this.props.settings.mobile ? prev.image.small : prev.image.large;
        }

        if (next) {
            this.nextImage.ref.src = this.props.settings.mobile ? next.image.small : next.image.large;
        }

    }

    updateHistory(change = true, replace = false) {

        if (!change) {
            return;
        }

        let state;

        if (this.props.settings.category || this.props.settings.album || this.props.settings.user) {
            state = [{slug: this.state.post.id}, 'Post', `/photo/${ this.props.settings.page }/${ this.state.post.id }/${ this.props.settings.category || this.props.settings.album || this.props.settings.user }`];
        } else if (this.state.post.type > 1) {
            state = [{slug: this.state.post.id}, 'Post', `/photo/${ this.props.settings.page }/${ this.state.post.id }/${ this.state.post.series }`];
        } else {
            state = [{slug: this.state.post.id}, 'Post', `/photo/${ this.props.settings.page }/${ this.state.post.id }`];
        }

        if (!replace) {
            history.pushState(...state);
        } else {
            history.replaceState(...state)
        }

    }

    onBackButton(data) {
        if (data === undefined || data.state === undefined || data.state === null || !data.state.slug) {
            if (this.props.settings.single) {
                window.removeEventListener('popstate', this.onBackButton)
                location.reload();
            } else {
                this.props.close();
            }

            return false;
        } else {
            this.getPost(0, false)
        }

        return false;
    }

    reloadSide() {

        if (this.state.options) {
            this.setState({options: false})
        }

        if (this.state.comments) {
            let event = new CustomEvent('reload-comments', {detail: {post: this.state.post.id}});
            document.dispatchEvent(event);
        }

        if (this.state.info) {
            let event = new CustomEvent('reload-info', {detail: {post: this.state.post.id}});
            document.dispatchEvent(event);
        }

    }

    allowNextPrev() {

        let index = this.getCurrentIndex();

        if (index === -1) {
            this.setState({prev: false, next: false});

            return false;
        }

        let prev = index !== 0;

        let next = index !== this.state.posts.length - 1;

        this.setState({prev, next})
    }

    addToFavorites() {

        this.setState({favorite: true, favoriteLoading: true});

        this.favoriteApi.like(this.state.post.id).then(() => {
            this.setState({favoriteLoading: false});
        }, err => {
            this.setState({favorite: false, favoriteLoading: false});
            this.verify(err)
        });

        this.updateFavorite(this.state.post.id, true);
    }

    removeFromFavorites() {
        this.setState({favorite: false});

        this.favoriteApi.unlike(this.state.post.id).then(() => {
        }, err => {
            this.setState({favorite: true});
            this.verify(err)
        });

        this.updateFavorite(this.state.post.id, false);
    }

    follow() {
        this.followApi.follow(this.state.post.user.id).then(() => {
            this.setState({follow: true})
        }, err => {
            this.setState({follow: false})
            this.verify(err)
        });
        this.updateFollow(this.state.post.user.id, true);
        this.dispatch('follow.action', {slug: this.state.post.user.id, state: true})
    }

    unfollow() {
        this.followApi.unfollow(this.state.post.user.id).then(() => {
            this.setState({follow: false})
        }, err => {
            this.setState({follow: true})
            this.verify(err)
        });
        this.updateFollow(this.state.post.user.id, false);

        this.dispatch('follow.action', {slug: this.state.post.user.id, state: false})

    }

    dispatch(name, state) {
        let event = new CustomEvent(name, {detail: {state}});
        document.dispatchEvent(event);
    }

    render() {

        $('[data-toggle="tooltip"]').tooltip({boundary: 'window', trigger: 'hover'});

        return (
            <div
                className={ 'photo-page ' + (this.state.options || this.state.comments || this.state.info ? 'active' : '') }>

                { !this.props.settings.full ?
                    <div className="close" hidden={ this.props.full }>

                        <Button icon="fas fa-times"
                                onClick={ () => {
                                    this.props.close();
                                } }/>

                    </div>
                    : null }

                <div className="left photo-image left-image">

                    <div className="photo-content">

                        { this.state.prev ?
                            <div className="box-prev pointer" onClick={ () => this.getPost(0) }>


                                <Button icon="fas fa-chevron-left"
                                        classes="previous-photo"/>

                            </div>
                            : null }

                        <Image
                            ref={ (ref) => {
                                this.prevImage = ref
                            } }
                            classes="d-none"/>

                        <Image
                            ref={ (ref) => {
                                this.large = ref
                            } }
                            classes="photo-page-image active"
                            src={ this.state.post ? this.props.settings.mobile ? this.state.post.image.small : this.state.post.image.large : '' }
                            title={ this.state.post ? this.state.post.title : '' }/>


                        <Image
                            ref={ (ref) => {
                                this.fade = ref
                            } }
                            classes="photo-page-image not-active"
                        />

                        <Image
                            ref={ (ref) => {
                                this.nextImage = ref
                            } }
                            classes="d-none"/>

                        { this.state.next ?

                            <div className="box-next pointer" onClick={ () => this.getPost(1) }>

                                <Button icon="fas fa-chevron-right"
                                        classes="next-photo"/>

                            </div>

                            : null }

                    </div>

                </div>

                <div className="right photo-meta right-image">

                    { this.state.comments ?
                        <InfiniteComments settings={ this.props.settings } post={ this.state.post }/> : null }

                    { this.state.info ? <Info post={ this.state.post }/> : null }

                    { this.state.options ?
                        <Options product={ this.state.post.for_sale }
                                 opposite={this.state.post.opposite}
                                 sizes={ this.props.sizes }
                                 materials={ this.props.materials }/> : null }

                </div>

                {/* Bottom Bar */ }
                <Bar post={ this.state.post }
                     info={ this.state.info }
                     following={ this.state.post && this.state.follow }
                     favorite={ this.state.post && this.state.favorite }
                     disabled={ this.state.disabled }
                     comments={ this.state.comments }
                     settings={ this.props.settings }
                     updateFollow={ this.updateFollow.bind(this) }
                     updateFavorite={ this.updateFavorite.bind(this) }
                     options={ this.state.options }
                     addToFavorites={ this.addToFavorites.bind(this) }
                     removeFromFavorites={ this.removeFromFavorites.bind(this) }
                     follow={ this.follow.bind(this) }
                     unfollow={ this.unfollow.bind(this) }

                     toggleInfo={ () => {
                         this.setState({info: !this.state.info, options: false, comments: false})
                     } }
                     toggleComments={ () => {
                         this.setState({comments: !this.state.comments, options: false, info: false})
                     } }
                     toggleOptions={ () => {
                         this.setState({options: !this.state.options, comments: false, info: false})
                     } }
                />

            </div>
        )
    }
}

