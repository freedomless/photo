import React, { Component } from 'react';
import { Comment } from './Comment';
import { Empty } from '../../../Mixin/Empty';
import { Modal } from '../../../Mixin/Modal';
import { CommentAPI } from '../../../API/CommentAPI';

export class CommentList extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        let comments = this.props.comments.map((comment, key) => {
            return <Comment key={ 'comment-' + comment.id }
                            remove={ this.props.settings.curator }
                            auth={ this.props.settings.auth }
                            selected={ key === 0 && this.props.selected }
                            comment={ comment }/>
        });

        return (
            <div>
                <ul className="list-unstyled mt-5">
                    { comments }
                </ul>
                { comments.length === 0 ?
                    <Empty></Empty>
                    : null }
            </div>
        )
    }
}
