import React, { Component } from 'react';
import { Loader } from '../../../Mixin/Loader';
import { CommentList } from './CommentList';
import { CommentAPI } from '../../../API/CommentAPI';
import { CommentBox } from './CommentBox';
import { Modal } from '../../../Mixin/Modal';

export class InfiniteComments extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            visible: false,
            more: true,
            page: 1,
            loading: false
        };

        this.api = new CommentAPI();
    }

    componentDidMount() {

        this.loadMore();

        document.addEventListener('reload-comments', this.reload.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener('reload-comments', this.reload.bind(this));
    }

    reload() {
        this.setState({data: [], page: 1, more: true}, () => {
            this.loadMore();
        })
    }

    remove() {
        let modal = $('#remove-comment');
        let id = modal.data('id');


        this.api.remove(id).then(res => {
            let comments =this.state.data.filter((comment) => {
                return comment.id !== id;
            });

            this.setState({data:[...comments]})
        });

        modal.modal('hide')
    }

    loadMore() {

        this.setState({visible: true, loading: true});

        if (this.state.loading) {
            return false;
        }

        let comment = null;

        this.api.getComments(this.props.post.id, this.state.page, comment).then(res => {

            this.setState({
                selected: !!comment,
                loading: false,
                data: [...this.state.data, ...res.data.comments],
                more: res.data.more,
                visible: true,
                page: this.state.page + 1
            })
        })
    }

    addComment(text) {

        this.api.createComment(text, this.props.post.id).then(res => {
            this.setState({
                data: [res.data, ...this.state.data]
            })
        });
    }

    render() {
        return (
            <div className="photo-comments content shadow-sm">
                <h3 className="mt-3 mb-5 ml-3 text-muted">Comments</h3>
                <CommentBox add={ this.addComment.bind(this) }
                            settings={ this.props.settings }/>
                <CommentList settings={ this.props.settings }
                             comments={ this.state.data }/>
                <div className="text-center mb-5" hidden={ !this.state.more }>
                    <button className="btn"
                            onClick={ () => this.loadMore() } disabled={ this.state.loading }>
                        { this.state.loading ? '' : 'Show more' }
                    </button>
                </div>

                <Modal id="remove-comment" action={ () => {
                    this.remove();
                } }>Are you sure you want to delete this
                    comment?</Modal>
            </div>
        )
    }
}
