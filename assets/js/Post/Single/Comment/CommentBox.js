import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import { Textarea } from '../../../Mixin/Textarea';
import { Error } from '../../../Mixin/Error';
import { Button } from '../../../Mixin/Button';
import { Link } from '../../../Mixin/Link';

export class CommentBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            comment: '',
            loading: false,
            error: false
        }

    }

    add() {

        if (!this.state.comment) {
            this.setState({error: 'This field is required!'});
            return false;
        }

        this.setState({error: false, loading: true});

        this.props.add(this.state.comment);

        this.setState({comment: '', error: false, loading: false});
    }

    render() {
        return (
            <div>
                <form hidden={ !this.props.settings.auth }>
                    <Textarea
                        classes="photo-comment-box"
                        value={ this.state.comment }
                        onChange={ (event) => this.setState({comment: event.target.value}) }
                        placeholder="Comment..."
                    />
                    <Error hidden={ !this.state.error } classes="text-center mt-3 mb-3">
                        Comment cannot be blank!
                    </Error>
                    <div className="text-center">
                        <Button classes="btn btn-dark btn-blue" onClick={ this.add.bind(this) }
                                disabled={ this.state.loading }>
                            { this.state.loading ? (
                                <Loader type={ 'ThreeDots' } height="20px" width="40px" color="#fff"/>) : 'Add' }
                        </Button>
                    </div>
                </form>
                <div className="text-center" hidden={ this.props.settings.auth }>
                    <Link url="/login" classes="btn btn-dark btn-blue">
                        Login to comment
                    </Link>
                </div>
            </div>
        )
    }
}
