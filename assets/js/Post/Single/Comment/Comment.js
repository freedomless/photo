import React, { Component } from 'react';
import { Replies } from '../Replies/Replies';
import { ReplyAPI } from '../../../API/ReplyAPI';
import { Button } from '../../../Mixin/Button';
import { Modal } from '../../../Mixin/Modal';

export class Comment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            comment: this.props.comment,
            replies: this.props.comment.replies || [],
            reply: '',
            error: false,
            visible: false
        };

        this.replyApi = new ReplyAPI();

        this.createReply = this.createReply.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.toggleVisibility = this.toggleVisibility.bind(this);
    }

    remove(id) {
        let modal = $('#remove-comment');
        modal.data('id', id);
        modal.modal('show');
    }

    toggleVisibility() {
        this.setState({visible: !this.state.visible});
    }

    createReply() {

        if (!this.state.reply) {
            return false;
        }

        this.setState({loading: true});

        this.replyApi.create(this.props.comment.id, this.state.reply).then(res => {

            let replies = this.state.replies instanceof Array ? this.state.replies : [];

            this.setState({
                replies: [...replies, res.data.data],
                reply: '',
                error: false,
                loading: false,
                visible: false
            });
        })
    }

    removeReply() {

        let modal = $('#remove-reply-' + this.state.comment.id);

        let id = modal.data('id');

        this.replyApi.remove(id).then(res => {

            let replies = this.state.replies.filter((reply) => {
                return reply.id !== id;
            });

            this.setState({replies: [...replies]})
        });

        modal.modal('hide')
    }

    changeInput(text) {
        this.setState({reply: text});
    }

    render() {

        return (
            <li className={ this.props.selected ? 'bg-grey' : '' } key={ 'comment-' + this.state.comment.id }>
                <div className="media">
                    <img src={ this.state.comment.user.profile.picture || '/images/avatar.jpg' } alt=""
                         className="circle comment-avatar"/>
                    <div className="media-body ml-2">
                        <p className="mt-0 mb-0">
                            <a href={ '/profile/' + this.state.comment.user.slug }>
                                { this.state.comment.user.profile.first_name } { this.state.comment.user.profile.last_name }
                            </a>
                            { this.props.remove ?
                                <Button icon="fas fa-times"
                                        onClick={ () => this.remove(this.state.comment.id) }
                                        classes="btn  float-right"/>
                                : null }
                            <small className="float-right text-muted">{ this.props.comment.created_at }</small>

                        </p>
                        { this.state.comment.text }
                    </div>
                </div>
                <div className="d-block">
                    <Replies auth={ this.props.auth }
                             remove={ this.props.remove }
                             reply={ this.state.reply }
                             comment={this.state.comment.id}
                             visible={ this.state.visible }
                             toggleVisible={ this.toggleVisibility }
                             changeInput={ this.changeInput }
                             createReply={ this.createReply }
                             replies={ this.state.replies }/>
                    <Modal id={ 'remove-reply-' + this.state.comment.id } action={ this.removeReply.bind(this) }>Are you
                                                                                                                 sure
                                                                                                                 you
                                                                                                                 want to
                                                                                                                 delete
                                                                                                                 this
                                                                                                                 reply?</Modal>
                </div>
                <hr/>
            </li>
        )
    }
}

