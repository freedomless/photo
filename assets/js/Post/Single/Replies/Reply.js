import React, { Component } from 'react';
import TimeAgo from 'javascript-time-ago/modules/JavascriptTimeAgo';
import en from 'javascript-time-ago/locale/en';
import { Button } from '../../../Mixin/Button';

class Reply extends Component {
    constructor(props) {
        super(props);
    }


    remove(id) {
        let modal = $('#remove-reply-' + this.props.comment);
        modal.data('id', id);
        modal.modal('show');
    }

    render() {

        return (
            <li className="media mt-3 mb-3">
                <img src={ this.props.reply.user.profile.picture || '/images/avatar.jpg' } alt=""
                     className="circle comment-avatar"/>
                <div className="media-body ml-2">
                    <p className="mt-0 mb-0">
                        <a href={ '/profile/' + this.props.reply.user.slug }>
                            { this.props.reply.user.profile.first_name } { this.props.reply.user.profile.last_name }
                        </a>
                        { this.props.remove ?
                            <Button icon="fas fa-times"
                                    onClick={ () => this.remove(this.props.reply.id) }
                                    classes="btn  float-right"/>
                            : null }
                        <small className="float-right text-muted">{ this.props.reply.created_at }</small>
                    </p>
                    { this.props.reply.text }
                </div>

            </li>
        )
    }
}

export default Reply;
