import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import Reply from './Reply';
import ReplyBox from './ReplyBox';
import { ReplyAPI } from '../../../API/ReplyAPI';

export class Replies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            error: false,
            visible: false
        };

        this.api = new ReplyAPI();

    }

    render() {

        let replies = this.props.replies instanceof Array ? this.props.replies : [];

        replies = replies.map((reply, index) => {
            return <Reply comment={ this.props.comment }
                          remove={ this.props.remove } reply={ reply } key={ index }/>
        });

        return (
            <div className="replies">
                <div className="ml-4 pl-3" hidden={ replies.length === 0 }>
                    <span className="text-danger" hidden={ !this.state.error }>Reply field is required</span>
                    <ul className="list-unstyled">
                        { replies }
                        <li hidden={ !this.state.more } className="text-center">
                            <button className="custom-button gradient-dark-light-blue shadow"
                                    disabled={ this.state.loading }>
                                { this.state.loading ? (<Loader type={ 'ThreeDots' } height="20px" width="40px"
                                                                color="#fff"/>) : 'more' }
                            </button>
                        </li>
                    </ul>

                </div>
                <button className="btn btn-link ml-5 text-muted" onClick={ this.props.toggleVisible }>Reply</button>
                <div hidden={ !this.props.visible } className="ml-4 pl-3">
                    <ReplyBox auth={ this.props.auth }
                              change={ this.props.changeInput }
                              reply={ this.props.reply }
                              create={ this.props.createReply }/>
                </div>
            </div>
        )
    }
}

