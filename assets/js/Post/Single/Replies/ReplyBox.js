import React , { Component } from 'react';
import Loader from 'react-loader-spinner';

class ReplyBox extends Component
{
    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div>
                <div hidden={!this.props.auth}>
                    <div className="custom-input">
                    <textarea rows="2" value={ this.props.reply }
                              onChange={ (event) => this.props.change(event.target.value) }
                              className='photo-comment-box'
                              placeholder="Reply..."/>
                    </div>
                    <div className="text-danger ml-3 text-sm" hidden={ !this.props.error }>
                        { this.props.error }
                    </div>
                    <div className="text-center">
                        <button className="btn btn-dark btn-blue"
                                value={ this.props.comment }
                                disabled={ this.props.loading }
                                onClick={ this.props.create }>
                            { this.props.loading ? (<Loader type={ 'ThreeDots' } height="20px" width="40px"
                                                            color="#fff"/>) : 'Add' }
                        </button>
                    </div>
                </div>
                <div className="text-center" hidden={this.props.auth}>
                    <a href="/login" className="btn btn-dark btn-blue">Login to comment</a>
                </div>
            </div>
        )
    }
}

export default ReplyBox;
