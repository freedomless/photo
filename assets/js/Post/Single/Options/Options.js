import React, { Component } from 'react';
import { StoreAPI } from '../../../API/StoreAPI';
import { Loader } from '../../../Mixin/Loader';
import { PopoverBody } from 'reactstrap';

export class Options extends Component {

    constructor(props) {
        super(props);

        this.state = {
            material: 1,
            size: 1,
            total: 0,
            initialTotal: 0,
            quantity: this.props.quantity > 0 ? this.props.quantity : 1,
            updating: false,
            discount: false
        };

        this.api = new StoreAPI();
    }

    componentDidMount() {
        this.calculatePrice()
    }

    formatMoney(amount, decimalCount = 2, decimal = '.', thousands = ',') {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? '-' : '';

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : '');
        } catch (e) {
        }
    };

    addToCart(callback) {

        this.api.addToCard({
            product: this.props.product,
            material: this.state.material,
            size: this.state.size,
            quantity: this.state.quantity
        }).then(() => {

            iziToast.show({
                theme: '',
                color: 'green',
                icon: 'far fa-image',
                message: 'Product added to cart!',
                position: 'bottomCenter', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });

            let event = new CustomEvent('add-to-cart');
            document.dispatchEvent(event);

            if (callback instanceof Function) {
                callback();
            }
        });

    }

    buyNow() {
        this.addToCart(() => {
            window.location.href = '/cart'
        });
    }

    updateProductQuantity(quantity) {

        quantity = Math.round(quantity);
        quantity = quantity > 0 ? quantity : 1;

        this.setState({quantity, total: quantity * this.state.initialTotal});
    }

    updateProductMaterial(material) {

        this.setState({material}, () => {
            this.calculatePrice()
        });
    }

    updateProductSize(size) {

        this.setState({size}, () => {
            this.calculatePrice()
        });
    }

    calculatePrice() {

        this.setState({updating: true})

        this.api.calculatePrice(this.props.product, this.state.quantity, this.state.size, this.state.material).then(res => {

            this.setState({
                total: res.data.data.total,
                initialTotal: res.data.data.total / this.state.quantity,
                updating: false,
                discount: res.data.data.discount
            });

        })
    }

    render() {

        const materials = this.props.materials.map((value, index) => {
            return (<option value={ value.id } key={ index }>{ value.name }</option>)
        });
        const sizes = this.props.sizes.map((value, index) => {

            return (<option value={ value.id }
                            key={ index }>{ value.width } x { Math.round(this.props.opposite * value.width) }</option>)
        });

        const material = this.props.materials.filter((val) => {
            return val.id === parseInt(this.state.material)
        })[0];

        return (
            <div className="product-options content">

                <h3 className="text-muted">Product options</h3>

                <hr/>

                <div className="form-group">

                    <label htmlFor="">Quantity</label>

                    <input className="form-control"
                           min={ 1 }
                           type="number"
                           defaultValue={ this.state.quantity }
                           onChange={ (e) => this.updateProductQuantity(e.target.value) }
                           placeholder="Quantity"/>

                </div>

                <div className="form-group">

                    <label htmlFor="">Size</label>

                    <select className="form-control" defaultValue={ this.state.size }
                            onChange={ e => this.updateProductSize(e.target.value) }>
                        { sizes }
                    </select>

                </div>

                <div className="form-group">

                    <label htmlFor="">Media</label>

                    <select className="form-control" defaultValue={ this.state.material }
                            onChange={ e => this.updateProductMaterial(e.target.value) }>
                        { materials }
                    </select>

                </div>

                <span className="text-muted">
                    <i className="fas fa-info-circle"></i> {material ? material.description : null}
                    <div><a href="/product-details" target="_blank" className="text-info">Product Details</a></div>
                </span>

                <h5>
                    { this.state.discount > 0 ? (<div><small className="text-muted">Discount {this.state.discount} &euro;</small></div>) : null }

                    <strong>Price <span
                        className="text-muted">{ this.formatMoney(this.state.total) } &euro;</span></strong>
                </h5>

                <div className="mt-5 d-flex justify-content-center align-content-around btn-group">

                    <button className="btn btn-blue" onClick={ this.addToCart.bind(this) }>Add to cart</button>

                    <button className="btn btn-success" onClick={ this.buyNow.bind(this) }>Buy Now</button>

                </div>

                <div className="updating rounded" hidden={ !this.state.updating }>

                    <Loader/>

                </div>

            </div>
        )
    }
}
