import React, { Component } from 'react';
import { Link } from '../../../Mixin/Link';
import { Image } from '../../../Mixin/Image';

export class Author extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        let user = this.props.user;

        return (
            <Link url={ '/profile/' + user.slug } classes="d-flex user">
                <Image
                    src={ user && user.profile.picture ? user.profile.picture : '/images/avatar.jpg' }
                    title={ this.props ? this.props.title : '' }
                    width="30px"
                    height="30px"
                    classes="rounded-circle"/>
                <span className="d-none d-md-block">{ user ? user.profile.first_name : '' } { user ? user.profile.last_name : '' }</span>
            </Link>
        )
    }
}
