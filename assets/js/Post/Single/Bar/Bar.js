import React, {Component} from 'react';
import {Author} from './Author';
import {PublishedStamp} from '../../Post/PublishedStamp';
import {Link} from '../../../Mixin/Link';
import {Button} from '../../../Mixin/Button';

export class Bar extends Component {

    constructor(props) {
        super(props);

    }

    share(e, network) {

        e.preventDefault();

        const fbQuery = {
            app_id: '2360993147521648',
            display: 'popup',
            href: location.href
        };

        const links = {
            facebook: 'https://www.facebook.com/dialog/share?' + $.param(fbQuery),
            twitter: 'https://twitter.com/intent/tweet?url=' + location.href,
            pinterest: 'http://pinterest.com/pin/create/button/?url=' + location.href
                + '&media=' + this.props.post.image.small
                + '&description=' + (this.props.post.title ?? 'Photography') + ' by '
                + this.props.post.user.profile.first_name + ' '
                + this.props.post.user.profile.last_name
        };

        const networks = {
            facebook: {width: 600, height: 300},
            twitter: {width: 600, height: 254},
            pinterest: {width: 800, height: 454},
        };

        let popup = function (network) {
            let options = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,';
            window.open(links[network], '', options + 'height=' + networks[network].height + ',width=' + networks[network].width);
        };
        popup(network);

    }

    render() {

        return (
            <div className="options">


                {this.props.post ?

                    <div className="d-flex justify-content-between p-0">


                        <div className="d-flex flex-grow-1">

                            { this.props.post.user ?
                                <Author user={ this.props.post ? this.props.post.user : null }/> : null }

                        </div>

                        <div className="d-flex flex-grow-1">

                            <PublishedStamp hidden={this.props.post.status < 3}/>

                        </div>

                        <div className="d-flex">

                            { this.props.post.for_sale ?
                                <Button icon={ 'fas fa-shopping-cart ' + (this.props.options ? 'liked' : '') }
                                        onClick={ this.props.toggleOptions }
                                        classes={this.props.settings.curator === true ? 'd-none d-sm-block btn-cart ' : 'btn-cart '}
                                        tooltip="Buy"/> : null }

                            <span className="dropdown">

                                <button className="btn btn-link share-btn-main share-btn-bar dropdown-toggle js-tooltip-item"
                                        type="button" id="js-drop-up-social" title="Share It!"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i className="fa fa-share-alt"></i>
                                </button>

                                <div className="dropdown-menu share-btn-container share-btn-container-bar"
                                     aria-labelledby="js-drop-up-social">

                                    <Button onClick={(e) => this.share(e, 'facebook')}
                                            icon="fab fa-facebook-f btn-share-facebook"
                                            tooltip="Share on Facebook"/>

                                    <Button onClick={(e) => this.share(e, 'pinterest')}
                                            icon="fab fa-pinterest btn-share-pinterest"
                                            tooltip="Share on Pinterest"/>

                                    <Button onClick={(e) => this.share(e, 'twitter')}
                                            icon="fab fa-twitter btn-share-twitter"
                                            tooltip="Share on Twitter"/>

                                </div>

                            </span>

                            {/*Link to curators page*/}
                            {this.props.settings.curator === true ?
                                <Link url={'/curator/photo/' + this.props.post.id} icon="fa fa-tools"
                                      tooltip="Curators view"/> : null}

                            {/*If photo is in series add link*/}
                            {this.props.post.series ?
                                <Link url={'/series/' + this.props.post.series + '/view'} icon="fas fa-image"
                                      classes={'d-none d-sm-block'}
                                      tooltip="Series"/> : null}

                            {/*If post is not own add follow/unfollow button*/ }
                            { this.props.post.user && this.props.settings.auth && this.props.settings.auth !== this.props.post.user.id  ?
                                <Button
                                    icon={'fa ' + (this.props.following === true ? 'fa-user-check liked' : 'fa-user-plus ')}
                                    onClick={() => this.props.following ? this.props.unfollow() : this.props.follow()}
                                    disabled={this.props.disabled}
                                    classes={'d-none d-sm-block'}
                                    tooltip={this.props.following === true ? 'Unfollow' : 'Follow'}/> : null}

                            {/*If post is not own add like/unlike button*/ }
                            {  this.props.post.user && this.props.settings.auth && this.props.settings.auth !== this.props.post.user.id
                            && this.props.settings.curator !== true
                                ?
                                <Button icon={ 'far fa-heart ' + (this.props.favorite === true ? 'fas liked' : '') }
                                        disabled={ this.props.favoriteLoading || this.props.disabled }
                                        classes={this.props.settings.curator === true ? 'd-none d-sm-block' : ''}
                                        onClick={ () => this.props.favorite ? this.props.removeFromFavorites() : this.props.addToFavorites() }
                                        tooltip={ this.props.favorite === true ? 'Unfavorite' : 'Favorite' }/>
                                : null }

                            <Button icon={'fas fa-comment-alt ' + (this.props.comments ? 'liked' : '')}
                                    onClick={this.props.toggleComments}
                                    tooltip="Comments"/>

                            <Button icon={'fas fa-info ' + (this.props.info ? 'liked' : '')}
                                    onClick={this.props.toggleInfo}
                                    tooltip="Info"/>

                        </div>

                    </div>
                    : null}
            </div>
        )
    }
}
