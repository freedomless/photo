import React, { Component } from 'react';
import { FavoriteAPI } from '../API/FavoriteAPI';

export class Favorite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favorite: this.props.favorite,
        };

        this.like = this.like.bind(this);
        this.unlike = this.unlike.bind(this);
        this.api = new FavoriteAPI();
    }

    like() {
        this.setState({favorite:true});
        this.api.like(this.props.post)

    }

    unlike() {
        this.setState({favorite:false});
        this.api.unlike(this.props.post)
    }

    render() {

        return (
            <div className="d-inline-block">

                <button hidden={!this.state.favorite}
                        onClick={ () => this.unlike() }
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Remove from Favorites"
                        className="add-favorite active">

                    <i className="fas fa-heart" />

                </button>

                <button hidden={this.state.favorite}
                        onClick={ () => this.like() }
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Add to Favorites!"
                        className="add-favorite">

                    <i className="far fa-heart" />

                </button>

            </div>
        )

    }
}
