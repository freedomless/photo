import React , { Component } from 'react';

class NewReply extends Component
{
    render(){
        return(
            <li className="media mb-5">
                <a href={ this.props.notification.payload.sender.path}>
                    <img src={ this.props.notification.payload.sender.picture || '/images/avatar.jpg' } className="mr-3 circle shadow" width="50px" height="50px"/>
                </a>
                <div className="media-body ml-3">


                    <div className="row">
                        <div className="col-12">
                            <button className="float-right delete-notification" onClick={ () => {
                                this.props.destroy(this.props.notification.id)
                            } }>
                                <i className="fas fa-times text-muted"></i>
                            </button>

                            <small className="float-right text-muted">{ this.props.ago }</small>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-3" hidden={this.props.notification.payload.post.photo_url === undefined}>

                            <a href={ this.props.notification.payload.post.url }>

                                <img src={ this.props.notification.payload.post.photo_url } alt="Logo" width="100%"/>

                            </a>

                        </div>
                        <div  className={this.props.notification.payload.post.photo_url === undefined ? 'col-12' : 'col-9'}>

                            <p className="mt-0 mb-1">
                                <a href={ this.props.notification.payload.sender.path} className="text-muted">
                                    { this.props.notification.payload.sender.first_name } { this.props.notification.payload.sender.last_name }
                                </a>
                            </p>
                            <a href={this.props.notification.payload.post.url}>
                                <span className="notification-label-reply">Replied on your comment</span>
                            </a>

                        </div>
                    </div>

                </div>
            </li>
        )
    }
}

export default NewReply;
