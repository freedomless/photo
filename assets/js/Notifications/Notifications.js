import React, { Component } from 'react';
import NewPost from './NewPost';
import NewComment from './NewComment';
import Welcome from './Welcome';
import PostLiked from './PostLiked';
import { NotificationAPI } from '../API/NotificationAPI';
import PostPublished from './PostPublished';
import NewMessage from './NewMessage';
import Followed from './Followed';
import NewReply from './NewReply';
import { CurateLimitLeft } from './CurateLimitLeft';
import CurateLimitReached from './CurateLimitReached';
import { CurateLimitActive } from './CurateLimitActive';
import PhotoOfTheDay from './PhotoOfTheDay';
import { SeriesPublished } from './SeriesPublished';
import { Loader } from '../Mixin/Loader';
import { SelectionSeries } from './SelectionSeries';
import FundsReceived from './FundsReceived';
import PayoutRequested from './PayoutRequested';
import PayoutCompleted from './PayoutCompleted';

export class Notifications extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            more: true,
            loading: false,
            notifications: [],
            page: 1,
            count: this.props.count
        };

        this.api = new NotificationAPI();
        this.load = this.load.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    componentDidMount() {

        $(document).on('show.bs.dropdown', '.dropdown', () => {
            this.load()
        });

        $(document).on('hide.bs.dropdown', '.dropdown', () => {
            this.setState({
                visible: false,
                more: true,
                loading: false,
                notifications: [],
                page: 1
            })
        })
    }

    parseNotification(notification) {

        switch (notification.type.name) {

            case 'selection_series_published':
                return <SelectionSeries destroy={ this.destroy } notification={ notification }
                                        ago={ notification.created_at } key={ Math.random() }/>;

            case 'curate_limit_active':
                return <CurateLimitActive destroy={ this.destroy } notification={ notification }
                                          ago={ notification.created_at } key={ Math.random() }/>;
            case 'curate_limit_left':
                return <CurateLimitLeft destroy={ this.destroy } notification={ notification }
                                        ago={ notification.created_at } key={ Math.random() }/>;
            case 'curate_limit_reached':
                return <CurateLimitReached destroy={ this.destroy } notification={ notification }
                                           ago={ notification.created_at } key={ Math.random() }/>;
            case 'new_post':
                return <NewPost destroy={ this.destroy } notification={ notification }
                                ago={ notification.created_at } key={ Math.random() }/>;
            case 'new_comment':
                return <NewComment destroy={ this.destroy } notification={ notification }
                                   ago={ notification.created_at }
                                   key={ Math.random() }/>;
            case 'new_reply':
                return <NewReply destroy={ this.destroy } notification={ notification }
                                 ago={ notification.created_at }
                                 key={ Math.random() }/>;
            case 'post_liked':
                return <PostLiked destroy={ this.destroy } notification={ notification }
                                  ago={ notification.created_at }
                                  key={ Math.random() }/>;
            case 'photo_of_the_day':
                return <PhotoOfTheDay destroy={ this.destroy } notification={ notification }
                                      ago={ notification.created_at }
                                      key={ Math.random() }/>;
            case 'post_published':
                return <PostPublished destroy={ this.destroy } notification={ notification }
                                      ago={ notification.created_at }
                                      key={ Math.random() }/>;
            case 'series_published':
                return <SeriesPublished destroy={ this.destroy } notification={ notification }
                                        ago={ notification.created_at }
                                        key={ Math.random() }/>;

            case 'new_message':
                return <NewMessage destroy={ this.destroy } notification={ notification }
                                   ago={ notification.created_at }
                                   key={ Math.random() }/>;
            case 'followed':
                return <Followed destroy={ this.destroy } notification={ notification }
                                 ago={ notification.created_at } key={ Math.random() }/>;
            case 'welcome':
                return <Welcome destroy={ this.destroy } notification={ notification }
                                ago={ notification.created_at } key={ Math.random() }/>

            case 'funds-received':
                return <FundsReceived destroy={ this.destroy } notification={ notification }
                                      ago={ notification.created_at } key={ Math.random() }/>
            case 'payout-request':
                return <PayoutRequested destroy={ this.destroy } notification={ notification }
                                      ago={ notification.created_at } key={ Math.random() }/>
            case 'payout-completed':
                return <PayoutCompleted destroy={ this.destroy } notification={ notification }
                                      ago={ notification.created_at } key={ Math.random() }/>
            default:
                return ''
        }

    }

    load() {

        this.setState({loading: true, visible: true, count: 0});

        this.api.getNotifications(this.state.page).then(res => {
            this.setState({
                page: this.state.page + 1,
                loading: false,
                notifications: [...this.state.notifications, ...res.data.notifications],
                more: res.data.more
            })
        })
    }

    destroy(id) {

        let notifications = this.state.notifications.filter(notification => {
            return notification.id !== id;
        });
        this.setState({notifications});

        this.api.destroy(id).then();
        return false;
    };

    render() {

        let notifications = this.state.notifications.map(notification => this.parseNotification(notification));

        return (
            <li className="nav-item dropdown">
                <a href="#" className="nav-link dropdown-toggle" id="notifications" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="fas fa-bell text-white nav-notifications-icon d-none d-xl-inline-block mr-2"></i>
                    <span className="d-xl-none text-link">NOTIFICATIONS</span>
                    <span className="badge badge-danger circle position-relative"
                          hidden={ this.state.count === 0 }>{ this.state.count }</span>
                </a>
                <div className="dropdown-menu shadow border-0 dropdown-menu-right notifications-list"
                     aria-labelledby="notifications">
                    <h6 className="text-center p-2">
                        Notifications:
                    </h6>
                    <hr/>
                    <div className="notifications">
                        <ul className="list-unstyled">
                            { notifications }
                            <li hidden={ !this.state.more || this.state.loading } className="text-center">
                                <button className="btn btn-blue load-more"
                                        disabled={ this.state.loading } onClick={ this.load }>
                                    { this.state.loading ? (
                                        <Loader type={ 'ThreeDots' } height={ 20 } width={ 40 }
                                                color="#fff"/>) : 'More' }
                                </button>
                            </li>
                            <li className="pt-5 mt-5 text-center text-muted"
                                hidden={ this.state.more || this.state.notifications.length !== 0 }>
                                No notifications
                            </li>
                        </ul>
                        <div hidden={ !this.state.loading } className="text-center">
                            { this.state.loading ? (
                                <Loader/>) : 'more' }
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}
