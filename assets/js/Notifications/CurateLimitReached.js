import React , { Component } from 'react';

export class CurateLimitReached extends Component
{
    constructor(props) {
        super(props);

    }

    render(){
        return(
            <li className="media mb-5">
                <img src="/images/logo.png" alt="Logo" width="50px"/>
                <div className="media-body ml-3">

                    <div className="row">
                        <div className="col-12">
                            <button className="float-right delete-notification" onClick={() =>{this.props.destroy(this.props.notification.id)}}>
                                <i className="fas fa-times text-muted"></i>
                            </button>
                            <small className="float-right text-muted">{ this.props.ago }</small>
                        </div>
                    </div>

                    <p className="mt-0 mb-1">
                        Photoimaginart Team
                    </p>
                    You have reached the maximum allowed photos that you can send to curators team!
                    You will be able to send photos again after {this.props.notification.payload.left} {this.props.notification.payload.left > 1 ? 'days' : 'day'}.
                </div>
            </li>
        )
    }
}

export default CurateLimitReached;
