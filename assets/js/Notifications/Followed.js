import React, { Component } from 'react';

class Followed extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <li className="media mb-5">
                <a href={ this.props.notification.payload.sender.path }>
                    <img src={ this.props.notification.payload.sender.picture || '/images/avatar.jpg' }
                         className="mr-3 circle shadow" width="50px" height="50px"/>
                </a>
                <div className="media-body ml-3">

                    <div className="row">
                        <div className="col-12">
                            <button className="float-right delete-notification" onClick={() =>{this.props.destroy(this.props.notification.id)}}>
                                <i className="fas fa-times text-muted"></i>
                            </button>
                            <small className="float-right text-muted">{ this.props.ago }</small>
                        </div>
                    </div>

                    <p className="mt-0 mb-1">
                        <a href={  this.props.notification.payload.sender.path } className="text-muted">
                            { this.props.notification.payload.sender.first_name } { this.props.notification.payload.sender.last_name }
                        </a>
                    </p>
                    Followed you
                </div>
            </li>
        )
    }
}

export default Followed;
