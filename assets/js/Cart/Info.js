import React, { Component } from 'react';
import Address from './Address';

class Info extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div hidden={ this.props.stage !== 2 }>
                <div className="text-right">
                    <button className="btn btn-blue" onClick={ () => this.props.changeStage(1) }>
                        Edit Cart
                    </button>
                </div>
                <Address touched={ this.props.touched }
                         info={ this.props.shipping }
                         change={ this.props.changeInfo('shipping') }
                         countries={ this.props.countries }
                         type="shipping"
                         errors={ this.props.errors.shipping }
                         title="Shipping Address"
                />
                <hr/>
                <div className="d-flex justify-content-center align-content-center">

                    <div className="form-check m-auto">
                        <input type="checkbox"
                               defaultChecked={ this.props.same }
                               value={ this.props.same }
                               onChange={ this.props.toggleSame }
                               id="same_billing"
                               className="form-check-input"/>
                        <label className="form-check-label" htmlFor="same_billing">Billing same as shipping?</label>
                    </div>

                </div>
                <hr/>
                <Address touched={ this.props.touched }
                         info={ this.props.billing }
                         countries={ this.props.countries }
                         change={ this.props.changeInfo('billing') }
                         type="billing"
                         title="Billing Address"
                         errors={ this.props.errors.billing }
                         hidden={ this.props.same }/>

                <div className="text-center mt-3 mb-3">
                    <button className="btn btn-blue btn-lg btn-block"
                            onClick={ () => this.props.changeStage(3, this.props.validate) }>
                        Go to Payment
                    </button>
                    {/*<h1>Demo Mode - Purchase is not possible</h1>*/}
                </div>
            </div>
        )
    }
}

export default Info;
