import React , { Component } from 'react';
import Loader from 'react-loader-spinner';

class Processing extends Component
{
    constructor(props) {
        super(props);

    }

    render(){
        return(
            <div className="payment-loader" hidden={!this.props.hidden}>
                <div className="wrapper">
                    <Loader type="Oval"
                            color="#00BFFF"
                            height={100}
                            width={100}
                            className="m-auto"
                    />
                    <div className="text-center payment-loader-text">
                        <h5 className="text-center ">
                            {this.props.text}
                        </h5>
                    </div>
                </div>
            </div>
        )
    }
}

export default Processing;
