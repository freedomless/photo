import React, {Component} from 'react';

class Address extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let countries = [];

        for (let prop in this.props.countries) {
            countries.push(<option value={prop} key={prop}>{this.props.countries[prop]}</option>)
        }

        return (
            <div className="mt-3" hidden={this.props.hidden}>
                <h4 className="">{this.props.title}</h4>
                <hr/>
                <div className="form-group mb-3">
                    <label htmlFor={'full-name-' + this.props.type}>Full Name</label>
                    <input className="form-control" type="text" id={'full-name-' + this.props.type} name="name"
                           required={true} value={this.props.info.name} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.name && !this.props.errors.name}>
                        Full name is required*
                    </div>
                </div>

                <div className="form-group mb-3">
                    <label htmlFor={'email-' + this.props.type}>Email</label>
                    <input className="form-control" type="email" id={'email-' + this.props.type} name="email"
                           required={true} value={this.props.info.email} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.email && !this.props.errors.email}>
                        Email is empty or invalid*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'address1-' + this.props.type}>Address line 1</label>
                    <input className="form-control" type="text" id={'address1-' + this.props.type} name="address1"
                           required={true} value={this.props.info.address1} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.address1 && !this.props.errors.address1}>
                        Address is required*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'address2-' + this.props.type}>Address line 2 (optional)</label>
                    <input className="form-control" type="text" id={'address2-' + this.props.type} required={false}
                           name="address2" value={this.props.info.address2} onChange={this.props.change}/>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'zip-' + this.props.type}>Postal Code</label>
                    <input className="form-control" type="text" id={'zip-' + this.props.type} name="zip"
                           required={true} value={this.props.info.zip} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.zip && !this.props.errors.zip}>
                        Postal code is required*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'city-' + this.props.type}>City</label>
                    <input className="form-control" type="text" id={'city-' + this.props.type} name="city"
                           required={true} value={this.props.info.city} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.city && !this.props.errors.city}>
                        City is required*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'county-' + this.props.type}>County</label>
                    <input className="form-control" type="text" id={'county-' + this.props.type} name="county"
                           required={true} value={this.props.info.county} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.county && !this.props.errors.county}>
                        County is required*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'country-' + this.props.type}>Country</label>
                    <div className="text-info small"><strong>Currently we ship to:</strong> Austria, Belgium, Bulgaria,
                        Switzerland, Cyprus, Czechia, Germany, Denmark, Estonia, Spain, Finland, France, United Kingdom,
                        Greece, Croatia, Hungary, Ireland, Iceland, Italy, Lithuania, Luxembourg, Latvia, Netherlands,
                        Norway, Poland, Portugal, Romania, Serbia, Sweden, Slovenia, Slovakia.<br/>
                            <strong>This list will be expanded in near future.</strong>
                        <hr/>
                    </div>
                    <select className="form-control" name="country" id={'country-' + this.props.type}
                            value={this.props.info.country} required={true} onChange={this.props.change}>
                        <option value="" disabled>Select Country</option>
                        {countries}
                    </select>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.country && !this.props.errors.country}>
                        Country is required*
                    </div>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor={'phone-' + this.props.type}>Phone Number</label>
                    <input className="form-control" type="text" id={'phone-' + this.props.type} name="phone"
                           required={true} value={this.props.info.phone} onChange={this.props.change}/>
                    <div className="text-danger"
                         hidden={!this.props.touched || this.props.info.phone && !this.props.errors.phone}>
                        Phone is empty or invalid*
                    </div>
                </div>
            </div>
        )
    }
}

export default Address;
