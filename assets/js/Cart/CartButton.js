import React, { Component } from 'react';

export class CartButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: this.props.count
        }
    }


    componentDidMount() {
        document.addEventListener('add-to-cart',() => {
           this.setState({count: this.state.count + 1})
        });

        document.addEventListener('remove-from-cart',() => {
            this.setState({count: this.state.count - 1})
        });
    }

    render() {
        return (
            <div>
           
                <a className="nav-link text-white" href={ this.props.href }>
                  
                    <i className="fas fa-shopping-cart"/> 
                    
                    <span className="badge badge-light"> { this.state.count }</span>
                    
                    <span className="d-lg-none">Cart</span>
               
                </a>
                
            </div>
        )
    }
}

