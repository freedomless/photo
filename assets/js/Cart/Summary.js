import React, {Component} from 'react';

class Summary extends Component {
    constructor(props) {
        super(props);
    }

    formatMoney(amount, decimalCount = 2, decimal = '.', thousands = ',') {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? '-' : '';

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : '');
        } catch (e) {
            console.log(e)
        }
    };

    getTotal() {

        let total = 0;

        if (this.props.items instanceof Array) {
            this.props.items.map(product => total += parseFloat(product.total.replace(',', '')));
        }

        return total;
    }

    getCountry(id) {
        if (id) {
            return this.props.countries[id].name
        }

        return '';
    }

    render() {

        let total = this.getTotal();

        return (
            <div hidden={this.props.stage !== 3}>

                <h2 className="text-center mb-5 position-relative">
                    <small className="float-left">
                        <button className="btn btn-blue" onClick={() => this.props.changeStage(2)}>Edit Address
                        </button>
                    </small>
                    Summary
                    <small className="float-right">
                        <button className="btn btn-blue" onClick={() => this.props.changeStage(1)}>Edit Cart</button>
                    </small>
                </h2>
                <hr/>

                <div className="cart-table shadow-sm p-3">
                    <h5 className="">Order</h5>
                    <hr/>
                    <div>
                        <div className="row">
                            <div className="col-6">
                                <div><strong>Art Prints</strong></div>
                                <div><strong>Shipping</strong></div>
                                <div><strong>Tax</strong></div>
                                <hr/>
                                <div><strong>Total</strong></div>
                            </div>
                            <div className="col-6">
                                <div className="row">
                                    <div className="col-12">
                                        {/*TODO: FIX MEEEEE!!!*/}
                                        <div>{this.formatMoney(total * 0.85)} &euro;</div>
                                        <div><strong>Free</strong></div>
                                        <div>{this.formatMoney(total - (total * 0.85))} &euro;</div>
                                        <hr/>
                                        <div>{this.formatMoney((total))} &euro;</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row pl-5 ml-1 mt-3">
                            <div className="col-6">

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-6">
                        <h5 className="">Shipping address</h5>
                        <ul className="list-group-flush pl-0 rounded">
                            <li className="list-group-item d-flex">
                                <strong>Full Name</strong>
                                <div>{this.props.shipping.name}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Email</strong>
                                <div>{this.props.shipping.email}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Address 1</strong>
                                <div>{this.props.shipping.address1}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Address 2</strong>
                                <div>{this.props.shipping.address2}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Postcode</strong>
                                <div>{this.props.shipping.zip}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>City</strong>
                                <div>{this.props.shipping.city}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>County</strong>
                                <div>{this.props.shipping.county}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Country</strong>
                                <div>{this.getCountry(this.props.shipping.country)}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Phone</strong>
                                <div>{this.props.shipping.phone}</div>
                            </li>
                        </ul>
                    </div>
                    <div className="col-6">
                        <h5 className="">Billing address</h5>
                        <ul className="list-group-flush pl-0  rounded">
                            <li className="list-group-item d-flex">
                                <strong>Full Name</strong>
                                <div>{this.props.same ? this.props.shipping.name : this.props.billing.name}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Email</strong>
                                <div>{this.props.same ? this.props.shipping.email : this.props.billing.email}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Address 1</strong>
                                <div>{this.props.same ? this.props.shipping.address1 : this.props.billing.address1}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Address 2</strong>
                                <div>{this.props.same ? this.props.shipping.address2 : this.props.billing.address2}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Postcode</strong>
                                <div>{this.props.same ? this.props.shipping.zip : this.props.billing.zip}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>City</strong>
                                <div>{this.props.same ? this.props.shipping.city : this.props.billing.city}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>County</strong>
                                <div>{this.props.same ? this.props.shipping.country : this.props.billing.country}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Country</strong>
                                <div>{this.props.same ? this.getCountry(this.props.shipping.country) : this.getCountry(this.props.billing.country)}</div>
                            </li>
                            <li className="list-group-item d-flex">
                                <strong>Phone</strong>
                                <div>{this.props.same ? this.props.shipping.phone : this.props.billing.phone}</div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="text-center mt-3 mb-3 pt-5">
                    <button className="btn btn-blue btn-lg"
                            onClick={this.props.order}>
                        Payment <i className="fab fa-paypal"/>
                    </button>
                </div>

            </div>
        )
    }
}

export default Summary;
