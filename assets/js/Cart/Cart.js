import React, { Component } from 'react';
import ProductsList from './ProductsList';
import Info from './Info';
import Summary from './Summary';
import { StoreAPI } from '../API/StoreAPI';
import Processing from './Processing';

export class Cart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            stage: 1,
            products: [],
            billing: {
                email: '',
                name: '',
                address1: '',
                address2: '',
                zip: '',
                city: '',
                county: '',
                country: '',
                phone: ''
            },
            shipping: {
                email: '',
                name: '',
                address1: '',
                address2: '',
                zip: '',
                city: '',
                county: '',
                country: '',
                phone: ''
            },
            errors: {
                billing: {
                    email: false,
                    name: false,
                    address1: false,
                    address2: false,
                    zip: false,
                    city: false,
                    county: false,
                    country: false,
                    phone: false
                },
                shipping: {
                    email: false,
                    name: false,
                    address1: false,
                    address2: false,
                    zip: false,
                    city: false,
                    county: false,
                    country: false,
                    phone: false
                }
            },
            billing_shipping_same: true,
            visible: false,
            payment_loading: false,
            shipping_loading: false,
            shipping_cost: 0,
            items: this.props.items,
            touched: false,
            updating: []
        };

        this.api = new StoreAPI();
        this.event = new CustomEvent('cart-update');

        this.makeOrder = this.makeOrder.bind(this);

        this.changeInfo = this.changeInfo.bind(this);
        this.changeStage = this.changeStage.bind(this);
        this.validateInfo = this.validateInfo.bind(this);
        this.remove = this.remove.bind(this);
        this.update = this.update.bind(this);
        this.toggleBillingShippingSame = this.toggleBillingShippingSame.bind(this);
    }

    componentDidMount() {
        this.autocompleteBillingInfo();
    }

    autocompleteBillingInfo() {

        let info = {
            email: '',
            name: '',
            address1: '',
            address2: '',
            zip: '',
            city: '',
            county: '',
            country: '',
            phone: ''
        };

        if (this.props.user && this.props.user.address) {

            info = {
                email: this.props.user.email || '',
                name: this.props.user.profile.first_name + ' ' + this.props.user.profile.last_name,
                address1: this.props.user.address.address1 || '',
                address2: this.props.user.address.address2 || '',
                zip: this.props.user.address.zip || '',
                city: this.props.user.address.city || '',
                country: this.props.user.address.country || '',
                county: this.props.user.address.county || '',
                phone: this.props.user.address.phone || ''
            };

            this.setState({
                billing: info,
                shipping: info
            })
        }
    }

    changeStage(stage, callback) {

        if (callback && !callback()) {
            return false
        }

        this.setState({stage});
    }

    toggleBillingShippingSame() {

        this.setState({billing_shipping_same: !this.state.billing_shipping_same})
    }

    remove(id) {

        let items = this.state.items;

        for (let i = 0; i < items.length; i++) {
            if (items[i].id === id) {
                items.splice(i, 1);
                break
            }
        }

        this.setState({items: items})

        this.api.deleteCard(id);

        let event = new CustomEvent('remove-from-cart');
        document.dispatchEvent(event);
    }

    update(item) {

        this.setState({updating: [...this.state.updating, item.id]});

        this.api.updateCard(item.id, {
            product: item.product.id,
            material: item.material,
            size: item.size,
            quantity: parseInt(item.quantity)
        }).then(res => {

            let item = res.data.data;
            let items = this.state.items;

            for (let i = 0; i < items.length; i++) {
                if (items[i].id === item.id) {
                    items[i] = item;
                    break
                }
            }

            this.state.updating.splice(this.state.updating.indexOf(item.id), 1);

            this.setState({items: items})
        })
    }

    dispatchNotification(message) {
        let event = new CustomEvent('notification', {detail: {message, type: 'error'}});
        document.dispatchEvent(event)
    }

    validateInfo() {

        let valid = true;
        let shipping = this.state.shipping;
        let billing = this.state.billing;
        let errors = this.state.errors;
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const phoneRegex = /^([0-9+\s]+)$/;

        for (let prop in shipping) {

            let current = true;

            if (prop === 'address2') {
                continue;
            }

            if (prop === 'email' && !emailRegex.test(shipping[prop].toLowerCase())) {
                valid = false;
                current = false;
            }

            if (prop === 'phone' && !phoneRegex.test(shipping[prop])) {
                valid = false;
                current = false;
            }

            if (!shipping[prop]) {
                valid = false;
                current = false;
            }

            errors.shipping[prop] = !current;
        }

        if (this.state.billing_shipping_same === false) {
            for (let prop in billing) {
                if (prop === 'address2') {
                    continue;
                }

                if (prop === 'email' && emailRegex.test(billing[prop].toLowerCase())) {
                    valid = false;
                }

                if (prop === 'phone' && phoneRegex.test(billing[prop])) {
                    valid = false;
                }

                if (!billing[prop]) {
                    valid = false;
                }

                errors.billing[prop] = !valid;
            }
        }

        if (!valid) {
            this.setState({touched: true, errors});
        }

        return valid;
    }

    makeOrder() {

        if (!this.validateInfo()) {
            return false;
        }

        let shipping = this.state.shipping;

        let billing = null;

        if (this.state.billing_shipping_same) {
            billing = this.state.shipping
        } else {
            billing = this.state.billing;
        }

        this.setState({payment_loading: true});

        this.api.makeOrder(billing, shipping)
            .then(res => {

                    if (res.data.code === 0) {
                        location.href = res.data.data
                    }

                    if (res.data.code !== 0) {
                        this.setState({payment_loading: false});
                        for (let i = 0; i < res.data.extra.length; i++) {
                            this.dispatchNotification(res.data.extra[i]);
                        }
                    }

                },
                err => {
                    this.setState({payment_loading: false});
                    alert('Ops... Something went wrong.Please try again later.')
                })
    }

    changeInfo(fields) {
        return (event) => {

            let state = this.state;
            state[fields][event.target.name] = event.target.value;
            state.errors[fields][event.target.name] = false;
            this.setState(state);
        }
    }

    render() {
        return (
            <div className="cart-wrapper">
                <ProductsList stage={ this.state.stage }
                              items={ this.state.items }
                              configuration={ this.props.configuration }
                              materials={ this.props.materials }
                              sizes={ this.props.sizes }
                              changeStage={ this.changeStage }
                              update={ this.update }
                              updating={ this.state.updating }
                              remove={ this.remove }
                />
                <Info stage={ this.state.stage }
                      changeStage={ this.changeStage }
                      validate={ this.validateInfo }
                      changeInfo={ this.changeInfo }
                      touched={ this.state.touched }
                      billing={ this.state.billing }
                      countries={ this.props.countries }
                      shipping={ this.state.shipping }
                      errors={ this.state.errors }
                      toggleSame={ this.toggleBillingShippingSame }
                      same={ this.state.billing_shipping_same }/>
                <Summary stage={ this.state.stage }
                         changeStage={ this.changeStage }
                         order={ this.makeOrder }
                         configuration={ this.props.configuration }
                         items={ this.state.items }
                         shipping_cost={ this.state.shipping_cost }
                         billing={ this.state.billing }
                         shipping={ this.state.shipping }
                         countries={ this.props.countries }
                         same={ this.state.billing_shipping_same }/>
                <Processing hidden={ this.state.payment_loading } text="You soon will be redirected to payment page"/>
            </div>
        )
    }
}

