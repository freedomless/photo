import React, { Component } from 'react';
import { DebounceInput } from 'react-debounce-input';
import { Loader } from '../Mixin/Loader';

class Product extends Component {

    constructor(props) {
        super(props);
    }

    formatMoney(amount, decimalCount = 2, decimal = '.', thousands = ',') {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? '-' : '';

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : '');
        } catch (e) {

        }
    };

    render() {


        let size = this.props.sizes.filter((a) => a.id === this.props.item.size)[0];

        size = size ? size.width : 0;

        let description = [];

        let images = this.props.item.product.children instanceof Array ? this.props.item.product.children.map((post, key) => {

            if (key > 4) {
                return null;
            }

            description.push(<li key={key} className="list-group-item bg-transparent  p-0">
                                   <span className="item-size">
                                       { key + 1 }. Size: { size } x { Math.round(size * post.post.opposite) } - Price { this.formatMoney(post.total) } &euro;
                                  </span>
                            </li>)

            let more = key + 1 === this.props.item.product.children.length || (this.props.item.product.children.length > 5 && key > 3) ? null : null;

            return <div key={ key }><img src={ post.post.image.thumbnail } alt=""/>{ more }</div>
        }) : [];

        if (images.length === 0) {
            images = <img src={ this.props.item.product.post.image.thumbnail } alt=""/>;
        }

        const material = this.props.materials.filter((val) => {
            return val.id === parseInt(this.props.item.material)
        })[0];

        return (
            <div className="item p-3 mb-2 mt-2 rounded d-flex justify-content-start">

                <div className="updating" hidden={ this.props.updating.indexOf(this.props.item.id) === -1 }>

                    <Loader/>

                </div>

                <div className="w-50 d-inline-block">

                    <div className="form-group">

                        <label>Quantity</label>

                        <DebounceInput
                            minLength={ 1 }
                            className="form-control"
                            type="number"
                            debounceTimeout={ 300 }
                            value={ this.props.item.quantity }
                            onChange={ (event) => {
                                let item = this.props.item;
                                item.quantity = event.target.value;
                                this.props.update(item)
                            } }/>
                    </div>

                    <div className="form-group">

                        <label>Size</label>

                        <select value={ this.props.item.size }
                                className="form-control"
                                onChange={ (event) => {
                                    let item = this.props.item;
                                    item.size = event.target.value;
                                    this.props.update(item)
                                } }>
                            { this.props.sizes.map((size, key) => {
                                return (<option key={ key }
                                                value={ size.id }>{ size.width + ' x ' + (this.props.item.product.post.opposite > 0 ?
                                    Math.round(this.props.item.product.post.opposite * size.width)
                                    : size.height) }</option>)
                            }) }
                        </select>

                    </div>

                    <div className="form-group">

                        <label>Media</label>

                        <select value={ this.props.item.material }
                                className="form-control"
                                onChange={ (event) => {
                                    let item = this.props.item;
                                    item.material = event.target.value;
                                    this.props.update(item)
                                } }>

                            { this.props.materials.map((material, key) => {
                                return (<option key={ key } value={ material.id }>{ material.name }</option>)
                            }) }

                        </select>

                    </div>

                    <span className="text-muted">
                        <i className="fas fa-info-circle"></i> {material ? material.description : null}
                        <div><a href="/product-details" target="_blank" className="text-info">Product Details</a></div>
                    </span>

                    <h5 className="ml-3">
                        { this.props.item.discount > 0 ?
                            <div><small>Discount: { this.formatMoney(this.props.item.discount) } &euro;</small></div> : null }
                        Price <small>{ this.props.item.total }  &euro;</small>
                    </h5>

                </div>

                <div className="w-50 d-inline-block text-center">
                    <div className="series">
                        <div
                            className={ 'grid-' + (this.props.item.product.children.length > 5 ? 5 : this.props.item.product.children.length) }>
                            { images }
                        </div>
                    </div>
                    <div className="d-flex justify-content-center mt-3">
                        <ul className="list-group list-group-flush w-50">
                            { description }
                        </ul>
                    </div>
                    <button className="remove-item-button" onClick={ () => this.props.remove(this.props.item.id) }>
                        <i className="fas fa-times"/>
                    </button>
                </div>

            </div>
        )
    }
}

export default Product;
