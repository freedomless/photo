import React, { Component } from 'react';
import Product from './Product';

class ProductsList extends Component {
    constructor(props) {
        super(props);
    }

    formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
            console.log(e)
        }
    };

    render() {
        let total = 0;
        let discount = 0;

        let products = this.props.items instanceof Array ? this.props.items.map((item, key) => {

            item.material = item.material.id ? item.material.id : item.material;
            item.size = item.size.id ? item.size.id : item.size;
            total += parseFloat(item.total.replace(',', ''));
            
            if(item.discount){
                discount =parseFloat(item.discount.replace(',', ''))
            }

            return <Product key={ key }
                            updating={ this.props.updating }
                            materials={ this.props.materials }
                            configuration={ this.props.configuration }
                            sizes={ this.props.sizes }
                            item={ item }
                            update={ this.props.update }
                            remove={ this.props.remove }/>
        }) : null;

        total = this.formatMoney(total)

        return (
            <div hidden={ this.props.stage !== 1 }>

                <div hidden={ this.props.items.length === 0 }>
                    <hr/>

                    <h1 className="text-center  ml-3 mt-3 mb-5">Cart</h1>

                    <hr/>
                </div>


                <div className="container center" hidden={ this.props.items.length > 0 }>
                    <div className="jumbotron w-50">
                        <h1 className="text-center">
                            Cart is empty
                            <br/>
                            <a className="btn btn-blue mt-2" href="/store">Go to store</a>
                        </h1>
                    </div>
                </div>


                <div className="items" hidden={ this.props.items.length === 0 }>
                    { products }


                    <h3 className="ml-3 mb-5 pb-5 mt-4">
                        {discount > 0 ? <div><small>Total Discount: {this.formatMoney(discount)} &euro;</small></div> : null}
                        Total Price { total } &euro;
                    </h3>

                </div>

                <div className="text-center mb-5" hidden={ this.props.stage !== 1 || this.props.items.length === 0 }>

                    <button className="btn btn-blue btn-lg btn-block"
                            onClick={ () => this.props.changeStage(2, () => this.props.items.length > 0) }>
                        Checkout
                    </button>

                </div>

            </div>
        )
    }
}

export default ProductsList;
