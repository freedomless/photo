import axios from 'axios';

export class PostAPI {

    constructor() {

        this.transformResponse = (data) => {

            let list = JSON.parse(data);

            if (list.data === undefined) {
                list = {data: {items: [], total: 0, has_more: false}}
            }

            return {posts: list.data.items, total: list.data.total, more: list.data.has_more};
        };

    }

    show(id) {
        return axios.get(`/api/photo/${ id }`);
    }

    loadMoreIds(settings, page, filters, series) {

        // let series = this.props.series ? this.props.series.slug : null;

        if (settings.page === 'album') {
            return axios.get(`/api/photos/${ settings.page }/${ settings.album }/${ page }`)
        }

        if (settings.page === 'category') {
            return axios.get(`/api/photos/${ settings.page }/${ settings.category }/${ page }`)
        }

        if (settings.page === 'published' || settings.page === 'portfolio') {
            return axios.get(`/api/photos/${ settings.page }/${ page }/${ settings.user }`)
        }

        if (settings.page === 'series') {
            return axios.get(`/api/series/${ series }`)
        }

        return axios.get(`/api/photos/${ settings.page }/${ page }${ filters ? '?' + $.param(filters || {}) : '' }`)

    }

    getInfo(post) {
        return axios.get(`/api/photos/${ post }/info`, {
            transformResponse: res => {
                let data = JSON.parse(res);

                return data.data
            }
        })
    }

    getFavoriteAndFollow(posts) {
        return axios.post(`/api/posts/follow-favorite`, posts)
    }

    getBySeries(series, page) {
        return axios.get(`/api/series/photos/${ series }/${ page }`, {transformResponse: this.transformResponse});
    }

    getByAlbum(album, page) {
        return axios.get(`/api/album/${ album }/${ page }`, {transformResponse: this.transformResponse});
    }

    getPopular(page) {
        return axios.get(`/api/photos/popular/${ page }`, {transformResponse: this.transformResponse})
    }

    getLatest(page, filters) {

        return axios.get(`/api/photos/latest/${ page }?${ $.param(filters || {}) }`, {transformResponse: this.transformResponse})
    }

    getAwarded(page, filters) {
        return axios.get(`/api/photos/awarded/${ page }?${ $.param(filters || {}) }`, {transformResponse: this.transformResponse})
    }

    getFollowing(page) {
        return axios.get(`/api/photos/following/${ page }`, {transformResponse: this.transformResponse})
    }

    getPublished(page, user) {
        if (user) {
            return axios.get(`/api/photos/published/${ page }/${ user }`, {transformResponse: this.transformResponse})
        }
        return axios.get(`/api/photos/published/${ page }`, {transformResponse: this.transformResponse})
    }

    getPortfolio(page, user) {
        if (user) {
            return axios.get(`/api/photos/portfolio/${ page }/${ user }`, {transformResponse: this.transformResponse})
        }
        return axios.get(`/api/photos/portfolio/${ page }`, {transformResponse: this.transformResponse})
    };

    getByCategory(page, category) {
        return axios.get(`/api/photos/category/${ category }/${ page }`, {transformResponse: this.transformResponse});
    }

    getByTag(page, tag) {
        return axios.get(`/api/photos/tag/${ tag }/${ page }`, {transformResponse: this.transformResponse});
    }

    getFavorites(page) {
        return axios.get(`/api/photos/favorites/${ page }`, {transformResponse: this.transformResponse});
    }

    addView(id) {
        return axios.get(`/api/photo/${ id }/add-view`)
    }

    getForSale(page,filters) {
        return axios.get(`/api/store/single/${ page }?${ $.param(filters || {}) }`, {transformResponse: this.transformResponse})
    }
}
