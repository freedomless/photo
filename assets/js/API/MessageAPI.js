import axios from 'axios';

export class MessageAPI {

    searchForUser(firstName) {
        return axios.get(`/api/search/users/${ firstName }`, {
            transformResponse: (res) => {
                let data = JSON.parse(res).data;
                return {users: data.users};
            }
        });
    }

    getMessages(page) {
        return axios.get(`/api/messages/${ page }`, {
            transformResponse: (res) => {
                let response = JSON.parse(res);

                return {data: response.data.items, more: response.data.has_more}
            }
        })
    }

    addMessage(receiver, subject, text) {
        return axios.post(`/api/messages`, {receiver, subject, text})
    }

    addReply(data,id){
        return axios.post(`/api/messages/${id}`,data)
    }

    getMessage(id,page = 1) {
        return axios.get(`/api/message/${id}/${page}`)
    }

    deleteConversation(id){
        return axios.delete(`/api/messages/${id}`);
    }
}
