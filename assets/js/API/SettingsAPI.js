import axios from 'axios';

export class SettingsAPI {

    update(data) {
        return axios.put('/api/user', data)
    }

    updateImage(type,file){
        return axios.post(`/api/${type}/upload`,file);
    }
}
