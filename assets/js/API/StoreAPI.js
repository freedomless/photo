import axios from 'axios';

export class StoreAPI {

    makeOrder(billing, shipping) {
        return axios.post('/api/order', {billing_address: billing, shipping_address: shipping});
    }

    addToCard(product) {
        return axios.post('/api/cart', product, {withCredentials: true})
    }

    updateCard(id, product) {
        return axios.put(`/api/cart/${ id }`, product, {withCredentials: true})

    }

    deleteCard(id) {
        return axios.delete(`/api/cart/${ id }`, {withCredentials: true})
    }

    calculatePrice(product, quantity, size, material) {
        return axios.get(`/api/price/${ product }/${ quantity }/${ size }/${ material }`)
    }


}
