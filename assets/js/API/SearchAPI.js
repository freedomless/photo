import axios from 'axios';

export class SearchAPI {

    search(query){
        return axios.get(`/api/search/${query}` );
    }

}
