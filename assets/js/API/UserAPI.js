import axios from 'axios';

export class UserAPI {

    follow(user) {
        return axios.post(`/api/follow/${user}`);
    }

    unfollow(user) {
        return axios.delete(`/api/follow/${user}`);
    }

    getTopMembers(page) {
        return axios.get(`/api/members/top/${page}`);
    }

    getAlphabeticalMembers(page) {
        return axios.get(`/api/members/${page}`);
    }

    getFounders(page) {
        return axios.get(`/api/members/founders/${page}`);
    }

    getFollowing(page) {
        return axios.get(`/api/members/following/${page}`);
    }

    getFollowers(page) {
        return axios.get(`/api/members/followers/${page}`);
    }

    getByCountry(page,iso) {
        return axios.get(`/api/members/by-country/${iso}/${page}` );
    }
}
