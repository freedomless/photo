import axios from 'axios';

export class CommentAPI {

    remove(id){
        return axios.delete(`/api/comments/${id}`)
    }

    getComments(post, page, comment) {

        if (comment) {
            return axios.get(`/api/comments/${ post }/${ page }?comment=${comment}`, {
                transformResponse: (data) => {

                    let list = JSON.parse(data);

                    return {comments: list.data.items, total: list.data.total, more: list.data.has_more};
                }
            })

        }
        return axios.get(`/api/comments/${ post }/${ page }`, {
            transformResponse: (data) => {

                let list = JSON.parse(data);

                return {comments: list.data.items, total: list.data.total, more: list.data.has_more};
            }
        })

    }

    createComment(text, post) {
        return axios.post('/api/comments', {text, post}, {
            transformResponse: (data) => {
                let comment = JSON.parse(data)
                return comment.data
            }
        })
    }
}

