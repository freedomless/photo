import axios from 'axios';


export class FavoriteAPI {

    like(post){
        return axios.post(`/api/favorites/${post}`)
    }

    unlike(post){
        return axios.delete(`/api/favorites/${post}`)
    }
}
