import axios from 'axios';

export class NotificationAPI {

    getNotifications(page = 1) {
        return axios.get(`/api/notifications/${ page }`, {
            transformResponse: (res) => {
                let data = JSON.parse(res);
                return {more: data.data.has_more, notifications: data.data.items}
            }
        })
    }

    destroy(notification) {
        return axios.delete(`/api/notifications/${ notification }`);
    }
}
