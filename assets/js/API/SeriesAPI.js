import axios from 'axios';

export class SeriesAPI {

    getSeries(page,filters){
        return axios.get(`/api/photos/series/${page}?${ $.param(filters || {}) }`);
    }

    getUserSeries(slug,page){
        return axios.get(`/api/photos/published/${page}/${slug}/1`)
    }

    getUserPortfolioSeries(slug,page){
        return axios.get(`/api/photos/portfolio/${page}/${slug}/1`)
    }

    getProducts(page,filters){
        return axios.get(`/api/store/series/${page}?${ $.param(filters || {}) }`)
    }
}
