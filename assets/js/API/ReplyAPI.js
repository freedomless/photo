import axios from 'axios';

export class ReplyAPI {

    remove(id){
        return axios.delete(`/api/replies/${id}`);
    }

    getReplies(page, comment) {
        return axios.get(`/api/replies/${ comment }/${ page }`)
    }

    create(comment, text) {
        return axios.post(`/api/replies`, {comment, text});
    }
}

