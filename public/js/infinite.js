function infinite(container, body = document) {

    $(document).ready(function () {
        let loader = $('.spinner').hide();
        let page = 1;
        let parent = $(body);
        let loading = false;

        // Each time the user scrolls
        parent.scroll(function () {

            let that = $(this);

            let scrollTop = that.scrollTop();
            let max = parent.is(document) ? that.height() : that[0].scrollHeight;
            let def = parent.is(document) ? window.innerHeight : that.height();
            let offset = 300;

            if (scrollTop >= max - def - offset) {

                if (loading) {
                    return;
                }

                loader.show();

                page++;

                let href = location.href;

                if (href.indexOf('?') !== -1) {
                    href += '&page=' + page + '&ajax=true';
                } else {
                    href += '?page=' + page + '&ajax=true';
                }

                loading = true;
                $.ajax({
                    url: href,
                    dataType: 'html',
                    success: function (html) {

                        loader.hide();

                        if (html.trim() === '') {
                            parent.unbind('scroll');
                        }
                        loading = false;

                        $(container).append(html);
                    }
                });
            }
        });
    });
}
