$.total = 0;

$.fn.dropzone = function (options) {

    let that = this;

    $(document).on('series-child-removed', function () {
        if ($.total > 0) {
            --$.total;
        }
    })

    return this.each(function () {

        //User for add and remove class when drag-over and drag-leave
        let counter = 0;

        let element = $(this);

        let label = element.find('label');
        let input = element.find('input');

        if (input.data('label')) {
            label.html(`<span>${ input.data('label') }</span>`);
        }

        let preview = $('<div class="preview"><img src="" alt="" width="200px" height="200px" style="object-fit: cover"></div>');
        let errors = $('<div class="errors"><ul class="list-unstyled"></ul></div>');
        let remove = $('<button class="remove" type="button"><i class="fas fa-times"></i></button>');
        let loader = $('<strong>Uploading Image</strong><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>')
        let backdrop = $('<div class="modal-backdrop d-flex justify-content-center align-items-center flex-column"></div>')
        let doc = $(document);
        let win = window;
        //Placeholder from dimensions validator
        let previewImage = null;

        let settings = $.extend({
            maxFileSize: 30,
            maxFiles: 1,
            minWidth: 1000,
            minHeight: 1000,
            form: false,
            loader: true,
            backdrop: true,
            mimeTypes: ['image/jpeg'],
            errors: {
                fileType: 'Invalid file type. Allowed types { mimeTypes } .',
                fileSize: 'File is larger than { maxFileSize } MB!',
                minSize: 'Image must be at least { minWidth }px of width or height!'
            }
        }, options);

        //Add preview list placeholder
        label.append(preview);
        preview = preview.find('img');
        //Add errors list placeholder
        element.append(errors);
        errors = errors.find('ul');

        //Add allowed mime type from settings
        input.prop('accept', settings.mimeTypes);
        //Dragover add highlight on drop zone
        label.on('dragover', function (e) {
            $(this).addClass('drag-over')
        });
        //Dragleave remove highlight from drop zone
        label.on('dragleave', function (e) {
            $(this).removeClass('drag-over')
        });
        //Dragover add highlight on drop zone
        doc.on('dragenter', function (e) {

            if (counter > 0) {
                return;
            }

            counter++;
            that.find('label').addClass('drag-start');

            e.preventDefault();

        });
        //Dragleave remove highlight from drop zone
        doc.on('dragleave', function (e) {
            e.preventDefault();

            counter--;

            if (counter === -1) {
                that.find('label').removeClass('drag-start');
            }

        });
        //Support fo IE
        win.addEventListener('dragover', function (e) {
            e.preventDefault();
        }, false);
        //On drop in dropzone process file else prevent browser from load image and exit web page
        win.addEventListener('drop', function (e) {
            e.preventDefault();

            let that = $(e.target);

            label.removeClass('drag-over')
            label.removeClass('drag-start')

            if (that.is(label) || that.is(label.find('span'))) {

                let file = e.dataTransfer.files[0];
                let list = e.dataTransfer.files
                

                validation(file).then((result) => {

                    if (result) {
                        

                        input[0].files = list;

                        console.log(input[0].files)

                        addPreview(file);
                    }

                });

            }

        }, false);
        //File input on change process image
        input.on('change', function (e) {

            let file = e.target.files[0];

            validation(file).then((result) => {

                if (result) {
                    addPreview(file);
                }

            });

        });
        //If remove button is clicked handle it
        doc.on('click', '.preview .remove', function (e) {

            let $btn = $(this);
            let $preview = $btn.parent();

            $preview.find('img').prop('src', '');
            $preview.removeClass('active');
            $preview.parent().removeClass('active');
            $btn.remove();
            input[0].value = null;

            $.total--;

            return false;
        });

        //Validate all
        function validation(file) {

            let dfd = jQuery.Deferred();

            if (!file instanceof File) {
                dfd.resolve(false);
            }

            if (!typeValidator(file)) {
                dfd.resolve(false);
            }

            if (!sizeValidator(file)) {
                dfd.resolve(false);
            }

            minDimensionsValidator(file).then((status) => {
                dfd.resolve(status);
            });

            return dfd;
        }

        //Valida file size
        function sizeValidator(file) {

            if (file.size > (settings.maxFileSize * 1000 * 1000) && errors.find('.error-file-size').length === 0) {

                errors.append('<li class="text-danger error-file-size">' + errorParser(settings.errors.fileSize) + '</li>');

                return false;
            }

            errors.find('.error-file-size').remove();
            return true;
        }

        //Validate mime type
        function typeValidator(file) {

            if (settings.mimeTypes.indexOf(file.type) === -1 && errors.find('.error-file-type').length === 0) {

                errors.append('<li class="text-danger error-file-type">' + errorParser(settings.errors.fileType) + '</li>');

                return false;
            }

            errors.find('.error-file-type').remove();
            return true;
        }

        //Validate min dimension requirements
        function minDimensionsValidator(file) {

            let dfd = jQuery.Deferred();

            let width = true, height = true;

            let reader = new FileReader();

            reader.onload = function (e) {

                let img = new Image;

                img.onload = function () {

                    height = img.height >= settings.minHeight;
                    width = img.width >= settings.minWidth;

                    if (!height && !width && errors.find('.error-min-width').length === 0) {

                        errors.append('<li class="text-danger error-min-size">' + errorParser(settings.errors.minSize) + '</li>');
                        width = false;

                    } else {

                        errors.find('.error-min-size').remove();
                    }

                    dfd.resolve(width || height);

                    previewImage = reader.result;

                    label.addClass('active')
                };

                img.src = reader.result;

            };

            reader.readAsDataURL(file);

            return dfd;

        }

        //Parse variables in errors
        function errorParser(error) {

            let toBeReplaced = error.matchAll(/{( .+? )}/g);

            toBeReplaced = Array.from(toBeReplaced);

            for (let i = 0; i < toBeReplaced.length; i++) {
                let value = settings[toBeReplaced[i][1].trim()];

                let regex = new RegExp('{\\s*' + toBeReplaced[i][1] + '\\s*}')

                error = error.replace(regex, value);

            }

            return error;

        }

        //Add image preview
        function addPreview() {

            preview.attr('src', previewImage);
            preview.parent().addClass('active');

            preview.parent().append(remove);

            that.find('label').removeClass('drag-start drag-over');

            $.total++;
        }

        //If form is passed show loader and then submit it
        if (settings.form) {

            settings.form.on('submit', function (e) {

                if ($.min !== undefined && $.total < parseInt($.min)) {

                    $('.errors-min').removeClass('d-none')

                    return false;
                } else {
                    $('.errors-min').addClass('d-none')
                }

                if (input[0].files.length === 0) {

                    e.preventDefault();

                    errors.append('<div class="text-danger error-file-required">Image is required!</div>');

                    return false;
                }

                errors.find('error-file-required').remove();

                $('body').append(backdrop);

                if (settings.loader) {

                    preview.css({width: '200px', borderRadius: '100%', marginBottom: '20px'});
                    backdrop.append(preview);
                    backdrop.append(loader);

                }

            })

        }

    })

};
