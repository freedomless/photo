$(function () {

    var cover = $('.cover');
    var label = $('.cover label');
    var image = $('#cover-image');
    var cropImg = document.getElementById('cover-image-cropper');
    var save = $('#save');
    var cancel = $('#cancel');
    var options = $('.options');
    var loader = $('.dots-loader');

    $('#cover').on('change', function (e) {

        let fileReader = new FileReader();
        fileReader.addEventListener('load', function (e) {
            image.hide();
            label.hide();
            options.show();
            cropImg.setAttribute('src', fileReader.result);

            const cropper = new Cropper(cropImg, {
                viewMode: 3,
                dragMode: 'move',
                minCropBoxWidth: 1200,
                minCropBoxHeight: 350,
                restore: false,
                guides: true,
                center: true,
                highlight: true,
                cropBoxMovable: false,
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false
            });

            cancel.on('click', function () {
                image.show();
                label.show();
                loader.hide();
                options.hide();
                cropper.destroy();
            });

            save.on('click', function () {
                var canvas;
                loader.show();

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: 1200,
                        height: 350
                    });
                    canvas.toBlob(function (blob) {
                        var formData = new FormData();
                        options.hide();
                        formData.append('cover', blob, 'cover.jpg');
                        $.ajax('/api/cover', {
                            method: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,

                            success: function (res) {
                                if (res.status === 'error') {

                                    let event = new CustomEvent('notification', {
                                        detail: {
                                            type: 'error',
                                            message: {...res.extra}
                                        }
                                    });
                                    document.dispatchEvent(event);

                                    return;
                                }
                                window.location.reload();
                            },

                            error: function () {
                            },

                            complete: function () {
                                loader.hide();
                            }
                        });
                    }, 'image/png');
                }
            });
        });

        fileReader.readAsDataURL(this.files[0])

    });

    $('#avatar').on('change', function () {

        var formData = new FormData();
        options.hide();
        formData.append('avatar', this.files[0]);
        $.ajax('/api/avatar', {
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,

            success: function (res) {

                if (res.status === 'error') {

                    let event = new CustomEvent('notification', {
                        detail: {
                            type: 'error',
                            message: {...res.extra}
                        }
                    });
                    document.dispatchEvent(event);

                    return;
                }

                window.location.reload();
            },

            error: function () {
            },

            complete: function () {

            }
        });
    })

});
