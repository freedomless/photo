<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'PayPal' => array($vendorDir . '/paypal/rest-api-sdk-php/lib'),
    'Nacmartin\\PhpExecJs' => array($vendorDir . '/nacmartin/phpexecjs/src'),
    'JsonpCallbackValidator' => array($vendorDir . '/willdurand/jsonp-callback-validator/src'),
);
