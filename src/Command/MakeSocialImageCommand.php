<?php

namespace App\Command;

use App\Entity\Photo\Post;
use App\Enum\FileStorageFolderEnum;
use App\Helper\SocialImageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MakeSocialImageCommand extends Command
{
    protected static $defaultName = 'app:make:social:images';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var SocialImageCreator
     */
    private SocialImageCreator $socialImageCreator;
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * MakeSocialImageCommand constructor.
     * @param EntityManagerInterface $em
     * @param SocialImageCreator $socialImageCreator
     * @param ContainerInterface $container
     * @param string|null $name
     */
    public function __construct(
        EntityManagerInterface $em,
        SocialImageCreator $socialImageCreator,
        ContainerInterface $container,
        string $name = null
    ) {
        parent::__construct($name);
        $this->em = $em;
        $this->socialImageCreator = $socialImageCreator;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->addArgument('replace', InputArgument::OPTIONAL, 'Replace existing social images', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $replace = $input->getArgument('replace') == 'true';

        $published = $this->em->getRepository(Post::class)
            ->createQueryBuilder('p')
            ->where('p.status > 2')
            ->getQuery()
            ->getResult();

        $total = count($published);

        if ($io->confirm("Are you sure that you want to generate {$total} social resizes?", false)) {
            $io->progressStart($total);

            $watermark = 'public/' . $this->container->getParameter('app.social_resize_watermark_path');
            foreach ($published as $post) {
                $this->socialImageCreator->create($post, $replace, $watermark, false);

                $io->progressAdvance();
            }

            $this->em->flush();

            $io->progressFinish();

            $io->success("Done! {$total} images created!");
        }

        return 0;
    }
}
