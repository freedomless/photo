<?php

namespace App\Command;

use App\Entity\Photo\Award;
use App\Entity\Photo\Post;
use App\Event\Notification\PhotoAwardedEvent;
use App\Event\Notification\PhotoPublishedEvent;
use App\Event\Notification\SeriesPublishedEvent;
use App\Helper\DispatcherTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendFutureNotificationCommand
 * @package App\Command
 */
class SendFutureNotificationCommand extends Command
{
    use DispatcherTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'app:send:notification';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * SendFutureNotificationCommand constructor.
     * @param EntityManagerInterface $em
     * @param null $name
     */
    public function __construct(EntityManagerInterface $em,
                                $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $posts = $this->em->getRepository(Post::class)
            ->createQueryBuilder('p')
            ->where('p.hasNotification is null OR p.hasNotification = 0')
            ->andWhere('p.showDate < :now')
            ->andWhere('p.status > 2')
            ->andWhere('p.type = 1')
            ->andWhere('p.parent is null')
            ->setParameter('now', new DateTime())
            ->getQuery()
            ->getResult();

        foreach ($posts as $post) {

            $this->dispatch(new PhotoPublishedEvent(null, $post->getUser(), compact('post')));
            $post->setHasNotification(true);
            sleep(1);
        }

        $this->em->flush();

        $series = $this->em->getRepository(Post::class)
            ->createQueryBuilder('s')
            ->where('s.hasNotification is null OR s.hasNotification = 0')
            ->andWhere('s.showDate < :now')
            ->andWhere('s.status > 2')
            ->andWhere('s.type > 1 AND s.type < 4')
            ->setParameter('now', new DateTime())
            ->getQuery()
            ->getResult();

        foreach ($series as $s) {

            $this->dispatch(new SeriesPublishedEvent(null, $s->getUser(), ['series' => $s]));

            $s->setHasNotification(true);

            sleep(1);
        }

        $awards = $this->em->getRepository(Award::class)
            ->createQueryBuilder('a')
            ->where('a.hasNotification = false or a.hasNotification IS NULL')
            ->andWhere('a.createdAt = :today')
            ->setParameter('today', (new DateTime())->format('Y-m-d'))
            ->getQuery()
            ->getResult();

        /**
         * @var Award $award
         */
        foreach ($awards as $award) {

            $post = $award->getPost();

            $this->dispatch(new PhotoAwardedEvent(null, $post->getUser(), ['award' => $award, 'post' => $post]));

            $award->setHasNotification(true);

        }

        $this->em->flush();

        return 0;

    }
}
