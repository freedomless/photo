<?php

namespace App\Command;

use App\Entity\Notification\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RemoveSeenNotificationsCommand extends Command
{
    protected static $defaultName = 'app:notifications:remove';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->em->getRepository(Notification::class)
            ->createQueryBuilder('n')
            ->delete()
            ->where('n.isSeen = true or n.isEmailSent = true')
            ->andWhere('n.createdAt < :time_ago')
            ->setParameter('time_ago', (new \DateTime())->modify('-1 week'))
            ->getQuery()->execute();

        $io->success('Notifications have been removed!');

        return 0;
    }
}
