<?php

namespace App\Command;

use App\Entity\Photo\ImageCurateAllowance;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\Event\Notification\CurateLimitActiveEvent;
use App\EventSubscriber\ToCurateWorkflowGuardSubscriber;
use App\Helper\DispatcherTrait;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RefreshCurateAllowanceCommand
 * @package App\Command
 */
class RefreshCurateAllowanceCommand extends Command
{
    use DispatcherTrait;

    /**
     * @var string
     */
    protected static $defaultName = 'app:curate-allowance:refresh';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * RefreshCurateAllowanceCommand constructor.
     * @param EntityManagerInterface $em
     * @param ContainerInterface $container
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $em,
                                ContainerInterface $container,
                                string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $users = $this->em->getRepository(User::class)->findBy(['curateAllowanceLimitReached' => true]);

        $workflow = $this->container->get('state_machine.post_publishing');

        $allowances = $this->em->getRepository(ImageCurateAllowance::class)->findAll();


        /**
         * @var User $user
         */
        foreach ($users as $user) {

            $role = ToCurateWorkflowGuardSubscriber::getHighestRole($user->getRoles());

            $allowance = array_values(array_filter($allowances, function ($value) use ($role) { return $value->getRole() === $role; }));

            /**
             * @var Post $post
             */
            $post = $user->getPosts()->first();

            $status = $post->getStatus();

            $post->setStatus(0);

            if ($workflow->can($post, 'to_curate') > 0) {

                $user->setCurateAllowanceLimitReached(false);

                $this->dispatch(new CurateLimitActiveEvent(null, $user, ['left' => $allowance[0]->getQuantity()]));
            }

            $io->writeln($user->getId());

            $post->setStatus($status);

        }

        $this->em->flush();

        $io->success('Rest');

        return 0;
    }
}
