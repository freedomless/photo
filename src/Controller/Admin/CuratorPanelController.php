<?php

namespace App\Controller\Admin;

use App\Dto\Request\UserFilterDto;
use App\Entity\BaseEntity;
use App\Entity\Photo\CurateComment;
use App\Entity\Photo\Post;
use App\Entity\Photo\PostBlock;
use App\Entity\Photo\Vote;
use App\EntityManager\AwardManager;
use App\EntityManager\BaseManager;
use App\EntityManager\MessageManager;
use App\EntityManager\PostManager;
use App\Enum\FileStorageFolderEnum;
use App\FileManager\S3FileManager;
use App\Helper\CacheTrait;
use App\Helper\DispatcherTrait;
use App\Helper\SocialImageCreator;
use App\Processor\ImageProcessor;
use App\Repository\CategoryRepository;
use App\Repository\ConfigurationRepository;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Workflow\Workflow;

/**
 * Class CuratorPanelController
 * @package App\Controller\Admin
 *
 * @Security("is_granted('ROLE_CURATOR')")
 */
class CuratorPanelController extends BaseController
{
    use DispatcherTrait, CacheTrait;

    /**
     * @var PostManager
     */
    private PostManager $manager;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var BaseManager
     */
    private BaseManager $voteManager;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * CuratorPanelController constructor.
     * @param PostManager $manager
     * @param BaseManager $voteManager
     * @param SessionInterface $session
     * @param LoggerInterface $logger
     * @param CategoryRepository $categoryRepository
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(PostManager $manager,
                                BaseManager $voteManager,
                                SessionInterface $session,
                                LoggerInterface $logger,
                                CategoryRepository $categoryRepository,
                                ConfigurationRepository $configurationRepository)
    {
        $this->manager = $manager;
        $this->manager->repository->setPaginationType('web');
        $this->configurationRepository = $configurationRepository;
        $this->categoryRepository = $categoryRepository;

        $this->session = $session;
        $this->voteManager = $voteManager;
        $this->logger = $logger;
    }

    /**
     * @Route("/curator/pending/{page?1}/{limit?20}", name="admin_photo_for_curate")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return Response
     * @throws ExceptionInterface
     */
    public function pending(Request $request, int $page = 1, int $limit = 20): Response
    {
        /**
         * @var UserFilterDto $filter
         */
        $filter = $this->denormalize($request->query->get('filters', []), UserFilterDto::class);

        $posts = $this->manager->repository->adminGetForCurate($page, $limit, $filter);

        return $this->render('admin/curator/pending.html.twig', compact('posts'));
    }

    /**
     * @Route("/curator/photo/{id}", name="admin_photo_show")
     *
     * @param Request $request
     * @param Post $post
     *
     * @param AwardManager $awardManager
     * @param BaseManager $rejectReasonManager
     * @param BaseManager $postBlockManager
     * @return Response
     * @throws InvalidArgumentException
     */
    public function show(Request $request,
                         Post $post,
                         AwardManager $awardManager,
                         BaseManager $rejectReasonManager,
                         BaseManager $postBlockManager): Response
    {
        if ($post->getParent()) {
            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getParent()->getId()]);
        }

        $this->manager->addView($post);

        $hasBeenPhotoOfTheDay = $awardManager->hasBeenPhotoOfTheDay($post);

        $categories = $this->categoryRepository->getCategories();

        preg_match('/(pending|published|rejected)/', $request->server->get('HTTP_REFERER'), $matches);

        if (count($matches)) {
            $this->session->set('back', $request->server->get('HTTP_REFERER', $this->generateUrl('admin_photo_for_curate')));
        }

        $user = $this->getUser();

        $votes = $this->voteManager->repository->findBy(['post' => $post]);

        $voted = $this->voteManager->repository->findOneBy(['post' => $post, 'voter' => $user], [], false);

        $blocked = $postBlockManager->repository->getActive($post);

        $alreadyBlockedByCurator = $postBlockManager->repository->findOneBy(['blocker' => $this->getUser(), 'post' => $post], [], false);

        $reasons = $rejectReasonManager->repository->findAll();

        return $this->render('admin/curator/show.html.twig', compact('post', 'reasons', 'blocked', 'categories', 'votes', 'voted', 'hasBeenPhotoOfTheDay', 'alreadyBlockedByCurator'));
    }

    /**
     * @Route("/curator/photo/{id}/next", name="admin_photo_show_next")
     * @param Request $request
     * @param Post $post
     *
     * @return RedirectResponse
     */
    public function next(Post $post)
    {
        /** @var Post[] $nextPost */
        $nextPost = $this->manager->repository->adminGetNextForCurate($post);

        if (!$nextPost) {
            $this->addToast('There is no next photo.', null, 'red');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        return $this->redirectToRoute('admin_photo_show', ['id' => $nextPost[0]->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/prev", name="admin_photo_show_prev")
     * @param Post $post
     *
     * @return RedirectResponse
     */
    public function previous(Post $post)
    {
        /** @var Post[] $nextPost */
        $nextPost = $this->manager->repository->adminGetPreviousForCurate($post);

        if (!$nextPost) {
            $this->addToast('There is no previous photo.', null, 'red');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        return $this->redirectToRoute('admin_photo_show', ['id' => $nextPost[0]->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/update", name="admin_photo_update")
     *
     * @param Request $request
     * @param Post $post
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function update(Request $request, Post $post): RedirectResponse
    {

        $post->setIsNude($request->request->getBoolean('is_nude', $post->getIsNude()));
        $post->setIsGreyscale($request->request->getBoolean('is_greyscale', $post->getIsNude()));

        $category = $this->categoryRepository->find($request->request->getInt('category', 0));

        if ($category) {
            $post->setCategory($category);
        }

        $this->manager->flush($post);

        $id = $post->getParent() ? $post->getParent()->getId() : $post->getId();

        return $this->redirectToRoute('admin_photo_show', compact('id'));
    }

    /**
     * @Route("/curator/published/{page?1}/{limit?20}", name="admin_photo_published")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return Response
     * @throws Exception
     */
    public function published(Request $request, int $page = 1, int $limit = 20): Response
    {

        /**
         * @var UserFilterDto $filter
         */
        $filter = $this->denormalize($request->query->get('filters', []), UserFilterDto::class);

        $posts = $this->manager->repository->adminGetPublished($page, $limit, $filter);

        return $this->render('admin/curator/published.html.twig', compact('posts'));
    }

    /**
     * @Route("/curator/rejected/{page?1}/{limit?20}", name="admin_photo_rejected")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return Response
     * @throws Exception
     */
    public function rejected(Request $request, int $page = 1, int $limit = 20): Response
    {
        /**
         * @var UserFilterDto $filter
         */
        $filter = $this->denormalize($request->query->get('filters', []), UserFilterDto::class);

        $posts = $this->manager->repository->adminGetRejected($page, $limit, $filter);

        return $this->render('admin/curator/rejected.html.twig', compact('posts'));
    }

    /**
     * @Route("/curator/photo/{id}/curate/{action}", name="admin_photo_curate", requirements={"action": "0|1"})
     *
     * @param Post $post
     * @param bool $action
     * @param Request $request
     * @param MessageManager $messageManager
     * @param BaseManager $rejectReasonManager
     * @param ImageProcessor $processor
     * @param S3FileManager $fileManager
     * @param ContainerInterface $container
     *
     * @param BaseManager $postBlockManager
     * @param SocialImageCreator $socialImageCreator
     * @return RedirectResponse
     *
     * @throws InvalidArgumentException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function action(Post $post, bool $action,
                           Request $request,
                           MessageManager $messageManager,
                           BaseManager $rejectReasonManager,
                           ContainerInterface $container,
                           SocialImageCreator $socialImageCreator,
                           BaseManager $postBlockManager): RedirectResponse
    {
        $blocked = $postBlockManager->repository->getActive($post);

        if ($blocked && $blocked->getBlocker()->getId() !== $this->getUser()->getId()) {

            $this->addToast('This post was blocked by another curator!');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        /**
         * @var Workflow $workflow
         */
        $workflow = $container->get('state_machine.post_publishing');

        //Check if post can be published
        if ($action === true && $workflow->can($post, 'to_published')) {

            $workflow->apply($post, 'to_published');

            //Create social resize
            $this->dispatcher
                ->addListener(KernelEvents::TERMINATE, function () use ($socialImageCreator, $post) {
                    $socialImageCreator->create($post);
                });
        }

        //Check if post can be rejected and if shown decline
        if ($post->getShowDate() === null || $post->getShowDate() > new DateTime()) {

            if ($action === false && $workflow->can($post, 'to_rejected')) {

                if ($post->getStatus() === 3) {
                    $this->manager->move($post, true);
                }

                $workflow->apply($post, 'to_rejected');

                $reasons = [];

                foreach ($request->request->get('reasons') ?? [] as $reason) {
                    $rejectReason = $rejectReasonManager->repository->find($reason);
                    $post->addRejectReason($rejectReason);
                    $reasons[] = $rejectReason->getReason();
                }

                if (!empty($reasons)) {
                    $messageManager->sendRejectReasons($post, $this->getUser(), $reasons);
                }
            }

        }

        //Set show date and publisher
        if ($post->getStatus() === 3) {

            if ($post->getType() > 2) {
                $post->setShowDate(new DateTime());

            } else {
                $span = $this->configurationRepository->getConfig()->getPublishTimespan();

                $lastPublishedDate = $this->manager->repository->getLastPublished()->getShowDate();

                if ($lastPublishedDate < new DateTime()) {
                    $lastPublishedDate = new DateTime();
                }

                $post->setShowDate($lastPublishedDate->modify("+ {$span}minutes"));
            }

            if ($post->getType() > 1) {
                $this->cache->invalidateTags(['series']);
                $this->cache->invalidateTags(['portfolio_' . $post->getUser()->getId() . '_series_published']);
            }

        } else {
            $post->setShowDate(null);
        }

        $post->setPublisher($this->getUser());
        $post->setCuratedAt(new DateTime());

        $this->manager->flush($post);

        //Remove user votes after post is published/rejected
        $entityManager = $container->get('doctrine.orm.entity_manager');

        foreach ($post->getCurates() as $curate) {
            $entityManager->remove($curate);
        }

        $entityManager->flush();

        return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/vote/{vote}", name="admin_photo_vote", methods={"POST"})
     *
     * @param Post $post
     * @param bool $vote
     *
     * @return RedirectResponse
     */
    public function vote(Post $post, bool $vote): RedirectResponse
    {
        $user = $this->getUser();

        $voting = new Vote();
        $voting->setPost($post);
        $voting->setVoter($user);
        $voting->setVote($vote);

        $this->manager->em->persist($voting);
        $this->manager->em->flush();

        return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/published/move/{up}/{page}", name="admin_photo_published_move", methods={"POST"})
     *
     * @param Request $request
     * @param Post $post
     * @param bool $up
     *
     * @param int $page
     * @return RedirectResponse
     *
     * @throws InvalidArgumentException
     */
    public function move(Request $request, Post $post, bool $up, int $page): RedirectResponse
    {
        $places = $request->request->getInt('places', 1);

        $moved = $this->manager->move($post, $up, $places);

        if ($moved) {
            $this->addToast('Photo was moved by ' . $moved . ' places!', null, 'green', null);
        } else {
            $this->addToast('Cannot move this photo!', null, 'red', null);
        }

        return $this->redirectToRoute('admin_photo_published', compact('page'));
    }

    /**
     * @Route("/curator/photo/{id}/comment", name="admin_curator_comment", methods={"POST"})
     *
     * @param Request $request
     * @param Post $post
     *
     * @param BaseManager $curatorCommentManager
     * @return RedirectResponse
     */
    public function comment(Request $request, Post $post, BaseManager $curatorCommentManager)
    {
        $text = $request->request->get('comment');

        if (!empty($text)) {
            $comment = new CurateComment();
            $comment->setPost($post);
            $comment->setUser($this->getUser());
            $comment->setText($text);

            $curatorCommentManager->create($comment);
        }

        if ($post->getParent()) {
            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getParent()->getId()]);
        }

        return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/block", name="admin_curator_block_post")
     * @param Post $post
     * @param BaseManager $postBlockManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function blockPost(Post $post, BaseManager $postBlockManager)
    {

        $blocked = $postBlockManager->repository->getActive($post);

        if ($blocked) {

            $this->addToast('This photo is already blocked!');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        $blockedByCurator = $postBlockManager->repository->findOneBy(['blocker' => $this->getUser(), 'post' => $post], [], false);

        if ($blockedByCurator) {

            $this->addToast('This photo was already blocked by you!');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        $delay = $this->configurationRepository->getConfig()->getCuratorPostBlockPeriod() ?? 1;

        $block = new PostBlock();
        $block->setPost($post);
        $block->setBlocker($this->getUser());
        $block->setEndDate((new DateTime())->modify("+ $delay days"));

        $postBlockManager->em->persist($block);
        $postBlockManager->em->flush();

        $this->addToast('This photo is blocked!');

        return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
    }

    /**
     * @Route("/curator/photo/{id}/unblock", name="admin_curator_unblock_post")
     * @param Post $post
     * @param BaseManager $postBlockManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function unblockPost(Post $post, BaseManager $postBlockManager)
    {

        $blockedByCurator = $postBlockManager->repository->findOneBy(['blocker' => $this->getUser(), 'post' => $post], [], false);

        if (!$blockedByCurator) {

            $this->addToast('This photo was not blocked by you!');

            return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
        }

        $blockedByCurator->setEndDate(new DateTime());

        $postBlockManager->em->flush();

        $this->addToast('This photo is unblocked!');

        return $this->redirectToRoute('admin_photo_show', ['id' => $post->getId()]);
    }
}
