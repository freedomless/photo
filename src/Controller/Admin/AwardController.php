<?php

namespace App\Controller\Admin;

use App\Entity\Photo\Award;
use App\Entity\Photo\Post;
use App\EntityManager\AwardManager;
use App\Event\Notification\PhotoAwardedEvent;
use App\Helper\DispatcherTrait;
use App\Repository\AwardRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminAwardController
 * @package App\Controller\Admin
 * @Security("is_granted('ROLE_CURATOR')")`
 */
class AwardController extends BaseController
{
    use DispatcherTrait;

    /**
     * @Route("/curator/photo-of-the-day", name="admin_manage_photo_of_the_day")
     *
     * @param AwardRepository $repository
     * @return Response
     * @throws Exception
     */
    public function index(AwardRepository $repository): Response
    {
        /** @var Award[] $photos */
        $photos = $repository->findUpcomingPhotos();

        return $this->render('admin/curator/curate_potd.html.twig',
            ['photos' => $photos]
        );
    }

    /**
     * @Route("/curator/photo-of-the-day/add/{id}", name="admin_create_photo_of_the_day")
     *
     * @param Post $photo
     * @param AwardManager $manager
     * @return RedirectResponse
     * @throws NonUniqueResultException
     */
    public function create(Post $photo, AwardManager $manager): RedirectResponse
    {
        try {
            $status = $manager->create($photo);
            if ($status) {
                $this->addToast('Photo added successfully!.', null, 'green');
            } else {
                $this->addToast('Error! Photos not switched!', null, 'red');
            }
        } catch (UniqueConstraintViolationException $exception) {
            $this->addToast('Error! Photos is already added!', null, 'red');
        }

        return $this->redirectToRoute('admin_photo_show', ['id' => $photo->getId()]);
    }

    /**
     * @Route("/curator/photo-of-the-day/switch", name="admin_switch_photo_of_the_day")
     * @param Request $request
     * @param AwardManager $manager
     * @return RedirectResponse
     */
    public function switch(Request $request, AwardManager $manager): RedirectResponse
    {
        if ($posts = $manager->switch($request->get('ids'))) {

            $this->addToast('Photos switched successfully!', null, 'green');
        } else {
            $this->addToast('Error! Photos not switched!', null, 'red');
        }

        return $this->redirectToRoute('admin_manage_photo_of_the_day');
    }

    /**
     * @Route("/curator/photo-of-the-day/delete/{id}", name="admin_delete_photo_of_the_day")
     *
     * @param Award $photo
     * @param AwardManager $manager
     * @return RedirectResponse
     * @throws Exception
     */
    public function delete(Award $photo, AwardManager $manager): RedirectResponse
    {
        if ($manager->delete($photo)) {
            $this->addToast('Photo deleted from the queue successfully!', null, 'green');
        } else {
            $this->addToast('Error! Photo not deleted!', null, 'red');
        }

        return $this->redirectToRoute('admin_manage_photo_of_the_day');
    }
}
