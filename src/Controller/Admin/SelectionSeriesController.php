<?php

namespace App\Controller\Admin;

use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use App\EntityManager\SelectionSeriesManager;
use App\Form\Type\SelectionSeriesType;
use App\Helper\SocialImageCreator;
use App\Repository\SelectionSeriesRepository;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Class SelectionSeriesController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_CURATOR")
 */
class SelectionSeriesController extends BaseController
{

    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * SelectionSeriesController constructor.
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache) {
        $this->cache = $cache;
    }

    /**
     * @Route("/curator/selection", name="admin_series_selection_list", methods={"GET"})
     *
     * @param SelectionSeriesRepository $selectionSeriesRepository
     *
     * @return Response
     */
    public function list(SelectionSeriesRepository $selectionSeriesRepository)
    {
        $series = $selectionSeriesRepository->findBy([], ['visibility' => 'desc', 'id' => 'desc']);

        return $this->render("admin/selection/list.html.twig", [
            'series' => $series
        ]);
    }

    /**
     * @Route("/curator/selection/new/{id}", name="admin_series_selection_new", methods={"GET", "POST"})
     *
     * @param Post $series
     * @param SelectionSeriesRepository $repository
     * @param Request $request
     * @param SelectionSeriesManager $selectionSeriesManager
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     * @throws NonUniqueResultException
     */
    public function add(
        Post $series,
        SelectionSeriesRepository $repository,
        Request $request,
        SelectionSeriesManager $selectionSeriesManager,
        SocialImageCreator $creator
    )
    {
        if (null !== $repository->findOneBySeries($series)) {
            $this->addToast(sprintf('This series id:%d already exists in front page series', $series->getId()));

            return $this->redirectToRoute('admin_series');
        }

        $form = $this->createForm(SelectionSeriesType::class, null, ['validation_groups' => ['create']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SelectionSeries $selectionSeries */
            $selectionSeries = $form->getData();

            /** @var UploadedFile $cover */
            $cover = $form['imageCover']->getData() ?? $creator->collage($series);

            $selectionSeriesManager->create($selectionSeries, $series, $cover);

            $this->cache->delete('selection_series');

            $this->addToast(
                sprintf('Series with title %s added successfully!', $selectionSeries->getTitle()));

            return $this->redirectToRoute('admin_series_selection_list');
        }

        return $this->render("admin/selection/new.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/curator/selection/{id}/edit", name="admin_series_selection_edit", methods={"GET", "POST"})
     *
     * @param SelectionSeries $selectionSeries
     * @param Request $request
     * @param SelectionSeriesManager $selectionSeriesManager
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function edit(SelectionSeries $selectionSeries,
                         Request $request,
                         SelectionSeriesManager $selectionSeriesManager)
    {

        $form = $this->createForm(SelectionSeriesType::class, $selectionSeries,['validation_groups' => ['update']]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SelectionSeries $selectionSeries */
            $selectionSeries = $form->getData();

            /** @var UploadedFile $cover */
            $cover = $form['imageCover']->getData();

            $selectionSeriesManager->update($selectionSeries, $cover);

            $this->addToast(
                sprintf('Series with id: #%d, successfully Updated!', $selectionSeries->getId()));

            $this->cache->delete('selection_series');

            return $this->redirectToRoute('admin_series_selection_list');
        }

        return $this->render("admin/selection/edit.html.twig", [
            'form' => $form->createView(),
            'series' => $selectionSeries
        ]);
    }

    /**
     * @Route("/curator/selection/{id}/delete", name="admin_series_selection_delete", methods={"POST"})
     *
     * @param SelectionSeries $selectionSeries
     * @param SelectionSeriesManager $selectionSeriesManager
     *
     * @return RedirectResponse
     *
     * @throws InvalidArgumentException
     */
    public function delete(SelectionSeries $selectionSeries, SelectionSeriesManager $selectionSeriesManager)
    {

        $this->cache->delete('selection_series');

        $selectionSeriesManager->delete($selectionSeries);

        $this->addToast(
            sprintf('Series with id #%d was removed from selection', $selectionSeries->getId()));

        return $this->redirectToRoute('admin_series_selection_list');
    }

    /**
     * @Route("/curator/selection/{id}/show", name="admin_series_selection_show", methods={"POST"})
     * @param SelectionSeries $selectionSeries
     * @param SelectionSeriesManager $selectionSeriesManager
     * @return RedirectResponse
     */
    public function show(SelectionSeries $selectionSeries, SelectionSeriesManager $selectionSeriesManager)
    {
        $selectionSeriesManager->showOnFrontPage($selectionSeries);

        $this->cache->delete('selection_series');


        $this->addToast(
            sprintf('Series with id #%d was show on front page!', $selectionSeries->getId()));

        return $this->redirectToRoute('admin_series_selection_list');
    }

    /**
     * @Route("/curator/selection/{id}/hide", name="admin_series_selection_hide", methods={"POST"})
     *
     * @param SelectionSeries $selectionSeries
     * @param SelectionSeriesManager $selectionSeriesManager
     *
     * @return RedirectResponse
     *
     * @throws InvalidArgumentException
     */
    public function hide(SelectionSeries $selectionSeries, SelectionSeriesManager $selectionSeriesManager)
    {
        $this->cache->delete('selection_series');

        $selectionSeriesManager->hideFromFrontPage($selectionSeries);

        $this->addToast(
            sprintf('Series with id #%d was removed from front page!', $selectionSeries->getId()));

        return $this->redirectToRoute('admin_series_selection_list');
    }
}
