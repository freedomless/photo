<?php

namespace App\Controller\Admin;

use App\Entity\Poll\Poll;
use App\Form\Type\PollType;
use App\EntityManager\PollManager;
use App\Repository\Poll\PollRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class PollController extends BaseController
{
    /**
     * @Route("/admin/poll", name="poll_admin_list")
     *
     * @param PollRepository $pollRepository
     * @return Response
     */
    public function list(PollRepository $pollRepository)
    {
        /** @var Poll[] $polls */
        $polls = $pollRepository->findAllSorted();

        return $this->render('admin/poll/list.html.twig', [
            'polls' => $polls
        ]);
    }

    /**
     * @Route("/admin/poll/new", name="poll_admin_new")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(PollType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Poll $poll */
            $poll = $form->getData();

            $entityManager->persist($poll);
            $entityManager->flush();

            $this->addToast( sprintf('Poll %s created!', $poll->getTitle()));

            return $this->redirectToRoute('poll_admin_list');
        }

        return $this->render('admin/poll/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/poll/{id}/choice/manage", name="poll_admin_manage_choices")
     *
     * @param Poll $poll
     * @param Request $request
     * @param PollManager $pollManager
     * @return Response
     */
    public function manageChoices(Poll $poll, Request $request, PollManager $pollManager)
    {
        if (
            $request->isMethod('POST') &&
            $this->isCsrfTokenValid('submit_choices', $request->request->get('token'))
        ) {
            $choices = $request->request->get('choice');

            if (null !== $choices) {
                $pollManager->persistChoices($choices, $poll);

                $this->addToast(
                    sprintf('Choices modified successfully for %s poll!', $poll->getTitle()));

                return $this->redirectToRoute('poll_admin_list');
            }
        }

        return $this->render('admin/poll/choice_manage.html.twig', [
            'poll' => $poll
        ]);
    }

    /**
     * @Route("/admin/poll/{id}/edit", name="poll_admin_edit")
     *
     * @param Poll $poll
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function edit(Poll $poll, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Poll $poll */
            $poll = $form->getData();

            $entityManager->persist($poll);

            $entityManager->flush();

            $this->addToast( sprintf('Poll %s edited!', $poll->getTitle()));

            return $this->redirectToRoute('poll_admin_list');
        }

        return $this->render('admin/poll/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/poll/{id}/delete", name="poll_admin_delete")
     *
     * @param Poll $poll
     * @param EntityManagerInterface $entityManager
     * @param PollManager $pollManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function delete(Poll $poll, EntityManagerInterface $entityManager, PollManager $pollManager)
    {
        $entityManager->remove($poll);

        if ($poll->isActive()) {
            $pollManager->clearCache();
        }

        $entityManager->flush();

        $this->addToast( sprintf('Poll %s deleted!', $poll->getTitle()));

        return $this->redirectToRoute('poll_admin_list');
    }

    /**
     * @Route("/admin/poll/{id}/activate", name="poll_admin_activate")
     *
     * @param Poll $poll
     * @param EntityManagerInterface $entityManager
     * @param PollRepository $pollRepository
     * @param PollManager $pollManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function activate(Poll $poll, EntityManagerInterface $entityManager, PollRepository $pollRepository, PollManager $pollManager)
    {
        if ($poll->getStatus() === false && $poll->getChoices()->count() > 1) {
            $poll->setStatus(true);

            $pollRepository->deactivateAllPolls();

            $pollManager->clearCache();

            $entityManager->flush();

            $this->addToast( sprintf('Poll %s activated!', $poll->getTitle()));

            return $this->redirectToRoute('poll_admin_list');
        }

        $this->addToast( sprintf('Poll %s is already activated or does not have at least 2 choices!', $poll->getTitle()));

        return $this->redirectToRoute('poll_admin_list');
    }

    /**
     * @Route("/admin/poll/{id}/deactivate", name="poll_admin_deactivate")
     *
     * @param Poll $poll
     * @param PollRepository $pollRepository
     * @param PollManager $pollManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function deactivate(Poll $poll, PollRepository $pollRepository, PollManager $pollManager)
    {
        $pollRepository->deactivateAllPolls();

        $pollManager->clearCache();

        $this->addToast( sprintf('Poll %s created!', $poll->getTitle()));

        return $this->redirectToRoute('poll_admin_list');
    }
}
