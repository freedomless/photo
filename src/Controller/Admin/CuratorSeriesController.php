<?php

namespace App\Controller\Admin;

use App\Entity\Photo\Post;
use App\EntityManager\PostManager;
use App\Form\Type\PostType;
use App\Helper\CacheTrait;
use App\Helper\SocialImageCreator;
use App\Repository\SeriesSettingsRepository;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CuratorSeriesController
 * @package App\Controller\Admin
 *
 * @Security("is_granted('ROLE_CURATOR')")
 */
class CuratorSeriesController extends BaseController
{
    use CacheTrait;

    /**
     * @var PostManager
     */
    private PostManager $manager;

    /**
     * CuratorSeriesController constructor.
     * @param PostManager $manager
     */
    public function __construct(PostManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/curator/manage/series/{page?1}/{limit?20}", name="admin_series")
     *
     * @param int $page
     * @param int $limit
     *
     * @return Response
     */
    public function index(int $page = 1, int $limit = 20): Response
    {

        $posts = $this->manager->repository->adminGetSeries($page, $limit);

        return $this->render('admin/curator/series/index.html.twig', compact('posts'));
    }

    /**
     * @Route("/curator/series", name="admin_series_create")
     *
     *
     * @param Request $request
     * @param SeriesSettingsRepository $seriesSettingsRepository
     *
     * @param SocialImageCreator $socialImageCreator
     * @return Response
     *
     * @throws InvalidArgumentException
     * @throws \App\Exception\ApiException
     */
    public function create(Request $request, SeriesSettingsRepository $seriesSettingsRepository,SocialImageCreator $socialImageCreator): Response
    {

        if ($request->query->get('ajax')) {

            $page = $request->request->count() === 0 ? $request->query->get('page', 1) : 0;
            $limit = $request->request->count() === 0 ? 20 : 0;

            $this->manager->repository->setPaginationType('web');
            $posts = $this->manager->repository->getPostsForSeriesFromExisting(null, null, $page, $limit);

            return $this->render('photo/children.html.twig', compact('posts'));
        }

        $settings = $seriesSettingsRepository->getSettings();

        $form = $this->generateForm(
            PostType::class,
            new Post(),
            $request,
            'admin_series',
            function ($form) use ($socialImageCreator) {

                /**
                 * @var Post $model
                 */
                $model = $form->getData();

                //Add type of the post
                $model->setType(4);

                /**
                 * @var Post $post
                 */
                $post = $this->manager->create($model, false, []);

                $image = $socialImageCreator->collage($post);

                $this->addToast('Series was created successfully!', $image);

                $this->cache->invalidateTags(['series']);

            }, ['type' => 5, 'settings' => $settings]);

        return $this->redirectOrRender($form, 'photo/series-from-published.html.twig', compact('settings'));
    }

    /**
     * @Route("/curator/series/{id}/edit", name="admin_series_update")
     *
     * @param Request $request
     * @param SeriesSettingsRepository $seriesSettingsRepository
     * @param Post $post
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function update(Request $request, SeriesSettingsRepository $seriesSettingsRepository, Post $post): Response
    {

        if ($request->query->get('ajax')) {

            $page = $request->request->count() === 0 ? $request->query->get('page', 1) : 0;
            $limit = $request->request->count() === 0 ? 20 : 0;

            $this->manager->repository->setPaginationType('web');
            $posts = $this->manager->repository->getPostsForSeriesFromExisting($post, null, $page, $limit);

            return $this->render('photo/children.html.twig', compact('posts'));
        }

        $settings = $seriesSettingsRepository->getSettings();

        $form = $this->generateForm(
            PostType::class,
            $post,
            $request,
            'admin_series',
            function ($form) use ($post, $seriesSettingsRepository) {

                $model = $form->getData();

                $this->manager->update($model, $post, false, []);

                $image = $post->getCover()->getImage()->getThumbnail();

                $this->addToast('Your series info was updated', $image);

            },
            ['method'   => 'PUT',
             'type'     => 5,
             'settings' => $settings]);

        return $this->redirectOrRender($form, 'photo/series-from-published.html.twig', compact('post', 'settings'));

    }

    /**
     * @Route("/curator/series/{id}/delete", name="admin_series_delete")
     * @param Post $post
     * @return RedirectResponse
     */
    public function delete(Post $post)
    {
        $this->manager->delete($post, false);

        return $this->redirectToRoute('admin_series');
    }

}
