<?php

namespace App\Controller\Admin;

use App\Controller\Web\BaseController as WebBaseController;

/**
 * Class BaseController
 * @package App\Controller\Web
 */
abstract class BaseController extends WebBaseController
{

}
