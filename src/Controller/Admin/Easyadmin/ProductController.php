<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\Store\Product;
use App\EntityManager\MessageManager;
use App\Enum\ProductStatusEnum;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller\Admin\Easyadmin
 */
class ProductController extends EasyAdminController
{

    /**
     * @var MessageManager
     */
    private MessageManager $messageManager;

    /**
     * ProductController constructor.
     * @param MessageManager $messageManager
     */
    public function __construct(MessageManager $messageManager)
    {

        $this->messageManager = $messageManager;
    }

    /**
     * @Route("/admin/product/approve/{id}", name="admin_approve_product")
     * @param Product $product
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return RedirectResponse
     */
    public function approveProduct(
        Product $product,
        EntityManagerInterface $entityManager,
        Request $request
    )
    {
        echo 'sdfsdfsf';
        if ($product && $product->getStatus() != ProductStatusEnum::PUBLISHED) {
            $now = new DateTime();

            $product->setStatus(ProductStatusEnum::PUBLISHED);
            $product->setApprovedOn($product->getApprovedOn() ?? $now);

            if ($product->getChildren()) {
                foreach ($product->getChildren() as $prod) {
                    if ($prod->getStatus() != ProductStatusEnum::PUBLISHED) {
                        $prod->setStatus(ProductStatusEnum::PUBLISHED);
                        $prod->setApprovedOn($prod->getApprovedOn()  ?? $now);
                    }
                }
            }

            $entityManager->flush();
        }

        if ($referer = $request->headers->get('referer', null)) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute('easyadmin');
    }

    /**
     * @return mixed|Response
     */
    protected function showAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_SHOW);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        $fields = $this->entity['show']['fields'];
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $this->dispatch(
            EasyAdminEvents::POST_SHOW,
            [
                'deleteForm' => $deleteForm,
                'fields'     => $fields,
                'entity'     => $entity,
            ]
        );

        $parameters = [
            'entity'      => $entity,
            'fields'      => $fields,
            'delete_form' => $deleteForm->createView(),
        ];

        return $this->executeDynamicMethod(
            'render<EntityName>Template',
            ['show', 'admin/easyadmin/product_show.html.twig', $parameters]
        );
    }

    /**
     * @param Product $entity
     */
    protected function updateEntity($entity)
    {
        $originalData = $this->em->getUnitOfWork()->getOriginalEntityData($entity);

        if ($entity->getStatus() === 2) {

            $this->messageManager->sendProductRejected($entity);

            $entity->getPost()->getImage()->setPrint(null);
            $entity->getPost()->getImage()->setPrintThumbnail(null);

            parent::removeEntity($entity);

            return $this->redirectToRoute('easyadmin');
        }

        if (array_key_exists('status', $originalData) && $originalData['status'] !== $entity->getStatus() && $entity->getStatus() === 3) {
            $entity->setApprovedOn($entity->getApprovedOn()  ?? new DateTime());
        }

        foreach ($entity->getChildren() as $child) {
            $child->setStatus($entity->getStatus());

            if (array_key_exists('status', $originalData) && $originalData['status'] !== $entity->getStatus() && $entity->getStatus() === 3) {
                $child->setApprovedOn($entity->getApprovedOn()  ?? new DateTime());
            }
        }

        parent::updateEntity($entity);
    }

}
