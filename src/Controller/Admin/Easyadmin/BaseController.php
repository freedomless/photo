<?php

namespace App\Controller\Admin\Easyadmin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class BaseController extends EasyAdminController
{
    /**
     * @param string $message
     * @param string|null $image
     * @param string|null $color
     * @param string|null $icon
     * @param string|null $theme
     */
    protected function addToast(string $message, ?string $image = null, ?string $color = null, ?string $icon = null, ?string $theme = null): void
    {
        $data = compact('message', 'color', 'image', 'icon', 'theme');

        $this->addFlash('toast', json_encode(array_filter($data)));
    }

}
