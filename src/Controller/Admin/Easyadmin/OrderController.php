<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\Store\Order;
use App\Entity\Store\OrderItem;
use App\Entity\User\Balance;
use App\Entity\User\User;
use App\EntityManager\OrderManager;
use App\Event\Notification\FundsReceivedEvent;
use App\Event\Notification\TrackIdAddedEvent;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class OrderController
 * @package App\Controller\Admin\Easyadmin
 */
class OrderController extends BaseController
{
    /**
     * @var OrderManager
     */
    private OrderManager $orderManager;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * OrderController constructor.
     * @param OrderManager $orderManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(OrderManager $orderManager, EventDispatcherInterface $eventDispatcher)
    {
        $this->orderManager = $orderManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Order $entity
     */
    protected function updateEntity($entity)
    {
        $unitOfWork = $this->em->getUnitOfWork();

        $unitOfWork->computeChangeSets();

        $changeSet = $unitOfWork->getEntityChangeSet($entity);

        if (isset($changeSet['dispatched']) && $changeSet['dispatched'][1] === true && isset($changeSet['trackId']) && $changeSet['trackId'][1] !== null) {
            $this->eventDispatcher->dispatch(new TrackIdAddedEvent(null, $entity->getCustomer(), ['order' => $entity]));
        }

        parent::updateEntity($entity);
    }

    /**
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateBalanceAction()
    {

        $id = $this->request->query->get('id');

        /**
         * @var $order Order
         */
        $order = $this->orderManager->repository->find($id);

        if ($order === null) {
            $this->addFlash('notification-error', 'Order not found');

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }

        if ($order->getTransaction()->getStatus() < 1) {
            $this->addFlash('notification-error', 'Order is no paid');

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }

        /**
         * @var $item OrderItem
         */
        foreach ($order->getItems() as $item) {
            if ($item->getCommissionAddedToBalance() === true) {
                continue;
            }

            $product = $item->getProduct();
            $commission = $item->getCommission();

            /**
             * @var $user User
             */
            $user = $product->getPost()->getUser();
            $balance = $user->getBalance();

            if ($balance === null) {
                $balance = new Balance();
                $balance->setUser($user);
                $this->em->persist($balance);
            }

            $balance->setTotal($balance->getTotal() + ($commission * $item->getQuantity()));
            $item->setCommissionAddedToBalance(true);

            $this->eventDispatcher->dispatch(new FundsReceivedEvent(null, $user, ['item' => $item]));

            $this->addFlash('notification-success', $commission . ' EUR added to balance of ' . $user->getUsername());
        }

        $this->em->flush();

        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }
}
