<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\Configuration;
use App\Entity\Photo\Post;
use App\Helper\CacheTrait;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Psr\Cache\InvalidArgumentException;

class ConfigurationController extends EasyAdminController
{
    use CacheTrait;

    /**
     * @param Configuration $entity
     * @throws InvalidArgumentException
     */
    protected function persistEntity($entity)
    {
        $this->cache->delete('configuration');

        parent::persistEntity($entity);
    }

    /**
     * @param object $entity
     * @throws InvalidArgumentException
     */
    protected function updateEntity($entity)
    {
        $this->cache->delete('configuration');


        $originalObject = $this->em->getUnitOfWork()->getOriginalEntityData($entity);


        if ((int)$entity->getPublishTimespan() !== $originalObject['publishTimespan']) {

            $posts = $this->em->getRepository(Post::class)
                ->createQueryBuilder('p')
                ->where('p.status = 3')
                ->andWhere('p.showDate >= :now')
                ->andWhere('p.parent is null')
                ->setParameter('now', new \DateTime())
                ->orderBy('p.showDate', 'ASC')
                ->getQuery()
                ->getResult();

            $initial = null;

            $span = $entity->getPublishTimespan();

            /**
             * @var Post $post
             */
            foreach ($posts as $i => $post) {

                if ($i === 0) {
                    $initial = $post->getShowDate();
                    continue;
                }

                $showData = (clone $initial)->modify('+' . ($span * $i) . 'minutes');

                foreach ($post->getChildren() as $child){
                    $child->setShowDate($showData);
                }

                $post->setShowDate($showData);

            }

        }

        parent::updateEntity($entity);
    }
}
