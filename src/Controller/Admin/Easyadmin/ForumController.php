<?php

namespace App\Controller\Admin\Easyadmin;

use App\FileManager\S3FileManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ForumController extends EasyAdminController
{
    /**
     * @var S3FileManager
     */
    private S3FileManager $fileManager;

    public function __construct(S3FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    protected function persistEntity($entity)
    {
       $this->uploadImage($entity);

       parent::persistEntity($entity);
    }

    protected function updateEntity($entity)
    {
        $this->uploadImage($entity);

        parent::updateEntity($entity);

    }

    private function uploadImage($entity)
    {
        if($entity->getImage() instanceof UploadedFile){
           $url = $this->fileManager->add($entity->getImage(),'forum/');
           $entity->setImage($url);
        }
    }
}
