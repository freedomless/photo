<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use App\Event\Notification\PhotoAwardedEvent;
use App\Event\Notification\PhotoPublishedEvent;
use App\Event\Notification\SeriesPublishedEvent;
use App\Event\Notification\SeriesSelectedEvent;
use App\Helper\DispatcherTrait;
use App\Repository\AwardRepository;
use App\Repository\PostRepository;
use App\Repository\SelectionSeriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class EmailTesterController extends AbstractController
{
    use DispatcherTrait;

    private $receiverEmail;

    /**
     * @var PostRepository
     */
    private PostRepository $postRepository;

    /**
     * EmailTesterController constructor.
     * @param PostRepository $postRepository
     * @param ContainerInterface $container
     */
    public function __construct(
        PostRepository $postRepository,
        ContainerInterface $container
    ) {
        $this->postRepository = $postRepository;
        $this->container = $container;

        $this->receiverEmail = $container->getParameter('app.system_email');
    }

    /**
     * @Route("/admin/email-tester", name="admin_email_tester")
     */
    public function index()
    {
        return $this->render("admin/easyadmin/email_tester.html.twig");
    }

    /**
     * @Route("/admin/email-tester/photo", name="admin_email_tester_photo")
     */
    public function testPhoto()
    {
        /** @var Post $post */
        $post = $this->postRepository->findOneBy(['id' => 25437]);

        //$receiver = $this->setReceiver($post->getUser());

        $this->dispatch(new PhotoPublishedEvent(null, $this->getUser(), compact('post')));

        return $this->redirectToRoute('admin_email_tester');
    }

    /**
     * @Route("/admin/email-tester/series", name="admin_email_tester_series")
     */
    public function testSeries()
    {
        /** @var Post $post */
        $series = $this->postRepository->findOneBy(['id' => 25177]);

        $this->dispatch(new SeriesPublishedEvent(null, $this->getUser(), ['series' => $series]));

        return $this->redirectToRoute('admin_email_tester');
    }

    /**
     * @Route("/admin/email-tester/selection-series", name="admin_email_tester_selection_series")
     * @param SelectionSeriesRepository $seriesRepository
     * @return RedirectResponse
     */
    public function testSelectionSeries(SelectionSeriesRepository $seriesRepository)
    {
        /** @var SelectionSeries $post */
        $selectionSeries = $seriesRepository->findOneBy(['id' => 18]);

        /** @var Post[] $posts */
        $posts = $selectionSeries->getPost()->getChildren();

        foreach ($posts as $post) {
            $this->dispatch(new SeriesSelectedEvent(null, $this->getUser(), [
                'post'      => $post,
                'selection' => $selectionSeries
            ]));

            break;
        }

        return $this->redirectToRoute('admin_email_tester');
    }

    /**
     * @Route("/admin/email-tester/potd", name="admin_email_tester_potd")
     * @param AwardRepository $awardRepository
     * @return RedirectResponse
     */
    public function testPhotoOfTheDay(AwardRepository $awardRepository)
    {
        /** @var Post $post */
        $award = $awardRepository->findOneBy(['id' => 512]);

        $post = $award->getPost();

        $this->dispatch(new PhotoAwardedEvent(null, $this->getUser(), ['award' => $award, 'post' => $post]));

        return $this->redirectToRoute('admin_email_tester');
    }
}
