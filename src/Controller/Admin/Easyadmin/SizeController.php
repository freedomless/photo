<?php

namespace App\Controller\Admin\Easyadmin;

use App\Helper\CacheTrait;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Psr\Cache\InvalidArgumentException;

/**
 * Class SizeController
 * @package App\Controller\Admin\Easyadmin
 */
class SizeController extends EasyAdminController
{
    use CacheTrait;

    /**
     * @param object $entity
     * @throws InvalidArgumentException
     */
    protected function persistEntity($entity)
    {
        $this->cache->delete('sizes');

        parent::persistEntity($entity);
    }

    /**
     * @param object $entity
     * @throws InvalidArgumentException
     */
    protected function updateEntity($entity)
    {
        $this->cache->delete('sizes');

        parent::updateEntity($entity);
    }
}
