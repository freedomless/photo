<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\User\Balance;
use App\Entity\User\User;
use App\EntityManager\AlbumManager;
use App\EntityManager\BaseManager;
use App\EntityManager\MessageManager;
use App\EntityManager\NotificationManager;
use App\EntityManager\PostManager;
use App\Enum\FileStorageFolderEnum;
use App\Enum\ImageSizeEnum;
use App\FileManager\S3FileManager;
use App\Processor\ImageProcessor;
use App\Repository\ConfigurationRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use ImagickException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package App\Controller\Admin\Easyadmin
 */
class UserController extends BaseController
{
    /**
     * @var BaseManager
     */
    private BaseManager $settingsManager;

    /**
     * @var BaseManager
     */
    private BaseManager $cartManager;

    /**
     * @var S3FileManager
     */
    private S3FileManager $fileManager;

    /**
     * @var PostManager
     */
    private PostManager $postManager;

    /**
     * @var BaseManager
     */
    private BaseManager $balanceManager;

    /**
     * @var ImageProcessor
     */
    private ImageProcessor $imageProcessor;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    /**
     * @var BaseManager|PostManager
     */
    private $albumManager;

    /**
     * @var NotificationManager
     */
    private NotificationManager $notificationManager;

    /**
     * @var BaseManager
     */
    private BaseManager $commentManager;

    /**
     * @var BaseManager
     */
    private BaseManager $replyManager;

    /**
     * @var BaseManager
     */
    private BaseManager $topicManager;

    /**
     * @var BaseManager
     */
    private BaseManager $opinionManager;

    /**
     * @var MessageManager
     */
    private MessageManager $messageManager;

    /**
     * @var BaseManager
     */
    private BaseManager $curateManager;

    /**
     * @var BaseManager
     */
    private BaseManager $voteManager;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * UserController constructor.
     * @param BaseManager $settingsManager
     * @param BaseManager $cartManager
     * @param BaseManager $balanceManager
     * @param BaseManager $commentManager
     * @param BaseManager $replyManager
     * @param BaseManager $topicManager
     * @param BaseManager $opinionManager
     * @param BaseManager $curateManager
     * @param BaseManager $voteManager
     * @param MessageManager $messageManager
     * @param PostManager $postManager
     * @param NotificationManager $notificationManager
     * @param AlbumManager $albumManager
     * @param ConfigurationRepository $configurationRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param ImageProcessor $imageProcessor
     * @param S3FileManager $fileManager
     */
    public function __construct(BaseManager $settingsManager,
                                BaseManager $cartManager,
                                BaseManager $balanceManager,
                                BaseManager $commentManager,
                                BaseManager $replyManager,
                                BaseManager $topicManager,
                                BaseManager $opinionManager,
                                BaseManager $curateManager,
                                BaseManager $voteManager,
                                MessageManager $messageManager,
                                PostManager $postManager,
                                NotificationManager $notificationManager,
                                AlbumManager $albumManager,
                                ConfigurationRepository $configurationRepository,
                                UserPasswordEncoderInterface $encoder,
                                ImageProcessor $imageProcessor,
                                S3FileManager $fileManager)
    {
        $this->settingsManager = $settingsManager;
        $this->cartManager = $cartManager;
        $this->fileManager = $fileManager;
        $this->postManager = $postManager;
        $this->balanceManager = $balanceManager;
        $this->imageProcessor = $imageProcessor;
        $this->encoder = $encoder;
        $this->albumManager = $albumManager;
        $this->notificationManager = $notificationManager;
        $this->commentManager = $commentManager;
        $this->replyManager = $replyManager;
        $this->topicManager = $topicManager;
        $this->opinionManager = $opinionManager;
        $this->messageManager = $messageManager;
        $this->curateManager = $curateManager;
        $this->voteManager = $voteManager;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @param object $entity
     * @throws ImagickException
     */
    protected function persistEntity($entity)
    {
        $this->upload($entity);
        $this->encodePassword($entity);

        parent::persistEntity($entity);
    }

    /**
     * @param object $entity
     * @throws ImagickException
     */
    protected function updateEntity($entity)
    {
        $this->upload($entity);
        $this->encodePassword($entity);

        parent::updateEntity($entity);

    }

    /**
     * @param User $entity
     */
    protected function removeEntity($entity)
    {
        /**
         * @var Balance $balance
         */
        $balance = $this->balanceManager->repository->findOneBy(['user' => $entity], [], false);

        if (($balance && ($balance->getBlocked() > 0 || $balance->getTotal() > 0)) || $entity->getDeletionDate() === null) {

            if ($entity->getDeletionDate() === null) {

                $deletionDate = (new \DateTime())->modify("+ {$this->configurationRepository->getConfig()->getPostDeletionInterval()} days");

                foreach ($entity->getPosts() as $post) {

                    $post->setDeletionDate($deletionDate);

                    foreach ($post->getChildren() as $child) {
                        $child->setDeletionDate($deletionDate);
                        $this->postManager->delete($child);
                    }

                    $this->postManager->delete($post);
                }

                $entity->setDeletionDate($deletionDate);
                $entity->setIsDeleted(true);
            }

        } else {

            if ($entity->getDeletionDate() < new \DateTime()) {

                if ($balance) {
                    $this->balanceManager->delete($balance, false);
                }

                foreach ($entity->getPosts() as $post) {

                    $post->setUser(null);

                    foreach ($post->getChildren() as $child) {
                        $child->setUser(null);
                    }

                }

                $entity->setIsDeleted(true);
            }

        }

        $settings = $this->settingsManager->repository->findOneBy(['user' => $entity], [], false);

        if ($settings) {
            $this->settingsManager->delete($settings, false);
        }

        $cart = $this->cartManager->repository->findOneBy(['user' => $entity], [], false);
        if ($cart) {
            $this->cartManager->delete($cart, false);
        }

        $albums = $this->albumManager->repository->findBy(['user' => $entity]);

        foreach ($albums as $album) {
            $this->albumManager->delete($album, false);
        }

        $replies = $this->replyManager->repository->findBy(['user' => $entity]);

        foreach ($replies as $reply) {
            $this->replyManager->delete($reply, false);
        }

        $comments = $this->commentManager->repository->findBy(['user' => $entity]);

        foreach ($comments as $comment) {
            $this->commentManager->delete($comment, false);
        }

        $votes = $this->voteManager->repository->findBy(['voter' => $entity]);

        foreach ($votes as $vote) {
            $this->voteManager->delete($vote, false);
        }

        $notifications = $this->notificationManager->repository->findBy(['sender' => $entity]);

        foreach ($notifications as $notification) {
            $this->notificationManager->delete($notification, false);
        }

        $notifications = $this->notificationManager->repository->findBy(['receiver' => $entity]);

        foreach ($notifications as $notification) {
            $this->notificationManager->delete($notification, false);
        }

        $topics = $this->topicManager->repository->findBy(['owner' => $entity]);

        foreach ($topics as $topic) {
            $this->topicManager->delete($topic, false);
        }

        $opinions = $this->opinionManager->repository->findBy(['user' => $entity]);

        foreach ($opinions as $opinion) {
            $this->opinionManager->delete($opinion, false);
        }

        $messages = $this->messageManager->repository->findBy(['sender' => $entity]);

        foreach ($messages as $message) {
            $this->messageManager->delete($message, false);
        }

        $messages = $this->messageManager->repository->findBy(['receiver' => $entity]);

        foreach ($messages as $message) {
            $this->messageManager->delete($message, false);
        }

        $curates = $this->curateManager->repository->findBy(['users' => $entity]);

        foreach ($curates as $curate) {
            $this->curateManager->delete($curate, false);
        }

    }

    /**
     * Upload cover and picture
     *
     * @param User $entity
     *
     * @throws ImagickException
     */
    protected function upload($entity)
    {

        if ($entity->getProfile()->getPictureFile() instanceof UploadedFile) {

            $picture = $this->imageProcessor->process(ImageSizeEnum::AVATAR, $entity->getProfile()->getPictureFile());

            $uri = $this->fileManager->add($picture, FileStorageFolderEnum::AVATAR);

            $entity->getProfile()->setPicture($uri);

        }

        if ($entity->getProfile()->getCoverFile() instanceof UploadedFile) {

            $this->imageProcessor->setBestfit(false);

            $this->imageProcessor->setImage($entity->getProfile()->getCoverFile());

            $x = $this->imageProcessor->getWidth() > ImageSizeEnum::COVER[0] ? ($this->imageProcessor->getWidth() - ImageSizeEnum::COVER[0]) / 2 : 0;
            $y = $this->imageProcessor->getHeight() > ImageSizeEnum::COVER[1] ? ($this->imageProcessor->getHeight() - ImageSizeEnum::COVER[1]) / 2 : 0;

            $width = ImageSizeEnum::COVER[0] > $this->imageProcessor->getWidth() ? $this->imageProcessor->getWidth() : ImageSizeEnum::COVER[0];
            $height = ImageSizeEnum::COVER[1] > $this->imageProcessor->getHeight() ? $this->imageProcessor->getHeight() : ImageSizeEnum::COVER[1];

            $this->imageProcessor->getImagick()->cropImage($width, $height, $x, $y);

            $uri = $this->fileManager->add($this->imageProcessor->getBlob(), FileStorageFolderEnum::COVER);

            $entity->getProfile()->setCover($uri);

        }
    }

    /**
     * @param User $entity
     */
    protected function encodePassword($entity)
    {
        if ($entity->getPlainPassword()) {
            $entity->setPassword($this->encoder->encodePassword($entity, $entity->getPlainPassword()));
        }
    }

}
