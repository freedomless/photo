<?php

namespace App\Controller\Admin\Easyadmin;

use App\Entity\User\Payout;
use App\EntityManager\BaseManager;
use App\Event\Notification\PayoutCompletedEvent;
use App\PaymentGateway\PayPal\PayPalPayout;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PayoutController
 * @package App\Controller\Admin\Easyadmin
 */
class PayoutController extends BaseController
{

    /**
     * @var PayPalPayout
     */
    private PayPalPayout $payout;

    /**
     * @var BaseManager
     */
    private BaseManager $balanceManager;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * PayoutController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param PayPalPayout $payout
     * @param BaseManager $balanceManager
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, PayPalPayout $payout, BaseManager $balanceManager)
    {
        $this->payout = $payout;
        $this->balanceManager = $balanceManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function acceptAction()
    {

        $id = $this->request->query->get('id');

        $payout = $this->em->find(Payout::class, $id);

        $balance = $this->balanceManager->repository->findOneBy(['user' => $payout->getUser()]);

        if ($balance->getPayout() === null) {

            $this->addToast('No payout email');

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));

        }

        $success = $this->payout->payout($balance->getPayout(), $payout->getSum());

        if (is_string($success)) {

            $this->addToast($success);

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }

        $newBlockedBalance = $balance->getBlocked() - $payout->getSum();

        if ($newBlockedBalance < 0) {

            $this->addToast('Payout amount larger than available blocked balance');

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));
        }

        $this->addToast('Payout success');
        $payout->setStatus(Payout::PAID);
        $balance->setBlocked($newBlockedBalance);

        $this->em->flush();

        $this->eventDispatcher->dispatch(new PayoutCompletedEvent(null, $payout->getUser(), ['balance' => $payout->getSum(), 'status' => 'accepted']));

        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));

    }

    /**
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function rejectAction()
    {
        $id = $this->request->query->get('id');

        $payout = $this->em->find(Payout::class, $id);

        $balance = $payout->getUser()->getBalance();

        $newBalance = $balance->getBlocked() - $payout->getSum();

        if ($newBalance < 0) {

            $this->addToast('Blocked balance lower than requested sum');

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'list',
                'entity' => $this->request->query->get('entity'),
            ));

        }

        $balance->setTotal($balance->getTotal() + $payout->getSum());

        $balance->setBlocked($balance->getBlocked() - $payout->getSum());

        $payout->setStatus(Payout::REJECTED);

        $this->em->flush();

        $this->addToast('Payout rejected');

        $this->eventDispatcher->dispatch(new PayoutCompletedEvent(null, $payout->getUser(), ['balance' => $payout->getSum(), 'status' => 'rejected']));


        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));

    }
}
