<?php

namespace App\Controller\Web;

use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\EntityManager\PostManager;
use App\Event\Point\PhotoCuratePointEvent;
use App\Helper\DispatcherTrait;
use App\Repository\BaseRepository;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Throwable;

/**
 * Class CurateController
 * @package App\Controller
 * @IsGranted("IS_AUTHENTICATED_REMEMBERED")
 */
class VoteController extends AbstractController
{
    use DispatcherTrait;

    /**
     * @var PostManager
     */
    private PostManager $postManager;

    /**
     * @var BaseRepository
     */
    private BaseRepository $userCurateSettingsRepository;

    /**
     * @var CacheInterface
     */
    private CacheInterface  $cache;

    /**
     * VoteController constructor.
     * @param PostManager $postManager
     * @param CacheInterface $cache
     * @param BaseRepository $userCurateSettingsRepository
     */
    public function __construct(PostManager $postManager,
                                CacheInterface $cache,
                                BaseRepository $userCurateSettingsRepository)
    {
        $this->postManager = $postManager;
        $this->userCurateSettingsRepository = $userCurateSettingsRepository;
        $this->cache = $cache;
    }

    /**
     * @Route("/vote", name="web_vote")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function index(): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $settings = $this->userCurateSettingsRepository->findAll()[0];

        $key = 'vote_' . $user->getId();

        $post = $this->cache->get($key, function () use ($user) {

            $post = $this->postManager->repository->getForMembersCurate($user);

            if ($post !== null) {

                $this->postManager->addView($post);

                return $post->getId();
            }

        });

        /**
         * @var Post $post
         */
        $post = $post ? $this->postManager->repository->find($post) : null;

        if ($post && $post->getStatus() > 2) {
            $this->cache->delete($key);

            return $this->redirectToRoute('web_vote');
        }

        $favorite = $post ? $this->postManager->repository->isFavorite($user->getId(), $post->getId()) : false;

        if($post === null){
            $this->cache->delete($key);
        }

        return $this->render('vote/index.html.twig', compact('post', 'favorite', 'settings'));
    }

    /**
     * @Route("/vote/{vote}", name="web_vote_action")
     *
     * @Security("is_granted('ROLE_USER') and is_granted('VERIFIED')")
     *
     * @param int $vote
     *
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function vote(int $vote): RedirectResponse
    {
        try {

            /**
             * @var User $user
             */
            $user = $this->getUser();

            $settings = $this->userCurateSettingsRepository->findAll()[0];

            if ($settings->getType() === 1 && ($vote > 1 || $vote < 0)) {
                throw new HttpException(403);
            }

            if ($settings->getType() === 2 && ($vote > $settings->getMax() || $vote < $settings->getMin())) {
                throw new HttpException(403);
            }

            $key = 'vote_' . $user->getId();

            $id = $this->cache->get($key, function () { return null; });

            if ($id === null) {
                return $this->redirectToRoute('web_vote');
            }

            $this->postManager->vote($user, $id, $vote);

            $this->cache->delete($key);

            $this->dispatch(new PhotoCuratePointEvent($user));

        } catch (Throwable $e) {

        }

        return $this->redirectToRoute('web_vote');
    }
}
