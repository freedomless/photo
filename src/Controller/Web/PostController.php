<?php

namespace App\Controller\Web;

use App\Builder\Seo\PostSeoBuilder;
use App\Builder\Seo\SeriesSeoBuilder;
use App\Entity\Photo\Award;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\EntityManager\PostManager;
use App\Event\Point\PostCreatedPointEvent;
use App\Form\Type\PostType;
use App\Helper\CacheTrait;
use App\Helper\DispatcherTrait;
use App\Repository\SeriesSettingsRepository;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Workflow\Exception\NotEnabledTransitionException;

/**
 * Class PostController
 * @package App\Controller
 */
class PostController extends BaseController
{

    use CacheTrait, DispatcherTrait;

    private const UPLOAD_TYPES = [
        'single'                => [
            'view'     => 'photo/upload.html.twig',
            'type'     => 1,
            'redirect' => 'single'
        ],
        'series'                => [
            'view'     => 'photo/upload-series.html.twig',
            'type'     => 2,
            'redirect' => 'series'
        ],
        'series-from-published' => [
            'view'     => 'photo/series-from-published.html.twig',
            'type'     => 3,
            'redirect' => 'series'
        ]
    ];

    /**
     * @var PostManager
     */
    private PostManager $manager;

    /**
     * PostController constructor.
     * @param PostManager $manager
     */
    public function __construct(PostManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/manage/photos/{type?single}", name="web_photos_manage", requirements={"type": "single|series" })
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param string $type
     * @param int $page
     * @param int $limit
     *
     * @return Response
     */
    public function index(Request $request, string $type, int $page = 1, int $limit = 20): Response
    {

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $this->manager->repository->setPaginationType('web');

        if ($type === 'single') {
            $posts = $this->manager->repository->getByUser($user, $request->query->getInt('page', $page), $limit);
        } else {
            $posts = $this->manager->repository->getByUser($user, $request->query->getInt('page', $page), $limit, true);
        }

        //For infinite scroll in manage single/series
        if ($request->isXmlHttpRequest()) {

            return $this->render('post/posts.html.twig', compact('posts'));
        }

        return $this->render('post/index.html.twig', compact('posts'));
    }

    /**
     * @Route("/upload/{type?single}", name="web_photo_create", requirements={"type": "single|series|series-from-published" })
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param SeriesSettingsRepository $seriesSettingsRepository
     * @param string $type
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function create(Request $request, SeriesSettingsRepository $seriesSettingsRepository, string $type): Response
    {
        //For series image processing
        set_time_limit(120);

        //User for series of published
        if ($request->query->get('ajax')) {

            $page = $request->request->count() === 0 ? $request->query->get('page', 1) : 0;
            $limit = $request->request->count() === 0 ? 20 : 0;

            $this->manager->repository->setPaginationType('web');
            $posts = $this->manager->repository->getPostsForSeriesFromExisting(null, $this->getUser(), $page, $limit);

            return $this->render('photo/children.html.twig', compact('posts'));
        }

        $uploadType = static::UPLOAD_TYPES[$type];

        $settings = $seriesSettingsRepository->getSettings();

        $form = $this->generateForm(PostType::class, new Post(), $request, 'web_photos_manage', function ($form) use ($uploadType) {

            /**
             * @var Post $model
             */
            $model = $form->getData();

            //Add type of the post
            $model->setType($uploadType['type']);

            $user = $this->getUser();
            //Add owner of the post
            $model->setUser($user);

            /**
             * @var Post $post
             */
            $post = $this->manager->create($model, false, []);

            //Used for toast
            $image = $uploadType['type'] > 1 ? $post->getCover()->getImage()->getThumbnail() : $post->getImage()->getThumbnail();

            $this->addToast('Your ' . ($uploadType['type'] > 1 ? 'series' : 'photo') . ' was uploaded successfully!', $image);

            $this->cache->invalidateTags(['portfolio_' . $user->getId()]);
            $this->cache->invalidateTags(['portfolio_' . $user->getId() . '_series']);

            $this->dispatch(new PostCreatedPointEvent($user));

        }, ['type' => $uploadType['type'], 'settings' => $settings], ['type' => $uploadType['redirect']]);

        return $this->redirectOrRender($form, $uploadType['view'], compact('settings'));
    }

    /**
     * @Route("/photo/{id}/edit", name="web_photo_update")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param SeriesSettingsRepository $seriesSettingsRepository
     * @param Post $post
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function update(Request $request, SeriesSettingsRepository $seriesSettingsRepository, Post $post): Response
    {
        //For series image processing
        set_time_limit(90);

        //User for series of published
        if ($request->query->get('ajax')) {

            $page = $request->request->count() === 0 ? $request->query->get('page', 1) : 0;
            $limit = $request->request->count() === 0 ? 20 : 0;

            $this->manager->repository->setPaginationType('web');
            $posts = $this->manager->repository->getPostsForSeriesFromExisting($post, $this->getUser(), $page, $limit);

            return $this->render('photo/children.html.twig', compact('posts'));
        }

        $settings = $seriesSettingsRepository->getSettings();

        $form = $this->generateForm(
            PostType::class,
            $post,
            $request,
            'web_photos_manage',
            function ($form) use ($post, $seriesSettingsRepository) {

                $model = $form->getData();

                $this->manager->update($model, $post, false, []);

                $image = $post->getType() > 1 ? $post->getCover()->getImage()->getThumbnail() : $post->getImage()->getThumbnail();

                $this->addToast('Your ' . ($post->getType() > 1 ? 'series' : 'photo') . ' info was updated', $image);

                $this->cache->invalidateTags(['portfolio_' . $post->getUser()->getId()]);

            },
            ['method'   => 'PUT',
             'type'     => $post->getType(),
             'settings' => $settings], ['type' => $post->getType() === 1 ? 'single' : 'series']);

        $type = array_filter(static::UPLOAD_TYPES, function ($value) use ($post) { return $value['type'] === $post->getType(); });
        $type = $type[array_keys($type)[0]];

        return $this->redirectOrRender($form, $type['view'], compact('post', 'settings'));

    }

    /**
     * @Route("/photo/{id}/series/cover", name="web_photo_set_cover")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function setCover(Post $post): RedirectResponse
    {

        $parent = $post->getParent();
        $parent->setCover($post);

        foreach ($parent->getChildren() as $child) {
            $child->setPosition(1);
        }

        $post->setPosition(0);

        $this->manager->em->flush();

        return $this->redirectToRoute('web_photo_update', ['id' => $parent->getId()]);
    }

    /**
     * @Route("/photo/{id}/curate", name="web_photo_for_curate")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function sendForCurate(Post $post): RedirectResponse
    {

        if ($post->getParent() !== null) {

            $this->addToast('Your cannot sent for curate single post from series!', null, '#8b0000');

            return $this->redirectToRoute('web_photos_manage');
        }

        $workflow = $this->container->get('state_machine.post_publishing');

        if ($post->getStatus() === 0) {

            try {

                $workflow->apply($post, 'to_curate');

                $post->setSentForCurateDate(new DateTime());

                $this->manager->flush($post);

            } catch (NotEnabledTransitionException $e) {
            }

        }

        $type = count($post->getChildren()) > 0 ? 'series' : 'single';

        return $this->redirectToRoute('web_photos_manage', compact('type'));
    }

    /**
     * @Route("/photo/{id}/delete", name="web_photo_delete")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     *
     * @param SeriesSettingsRepository $seriesSettingsRepository
     * @return RedirectResponse
     *
     * @throws InvalidArgumentException
     */
    public function destroy(Post $post, SeriesSettingsRepository $seriesSettingsRepository): RedirectResponse
    {

        $settings = $seriesSettingsRepository->getSettings();

        $transition = $post->getParent() ?? $post;

        $series = count($post->getChildren()) > 0 ? ['type' => 'series'] : [];

        //If series children are more than min allow
        $count = $post->getParent() ? count($post->getParent()->getChildren()) > $settings['min'] : true;

        //Check if post is not for curate or is not in series for curate
        if (($transition->getStatus() === 0 || $transition->getStatus() === 2) && $count) {

            $this->manager->delete($post);

            $this->addToast('Your photo was deleted!', null, 'green');

            $this->cache->invalidateTags(['portfolio_' . $post->getUser()->getId()]);

        } else {
            $this->addToast('Your cannot delete this photo!', null, 'red');
        }

        return $this->redirectToRoute('web_photos_manage', $series);
    }

    /**
     * @Route("/photo/{page}/{id}/{payload?}", name="web_photo_show")
     *
     * @Security("is_granted('ALLOW_ENTER')")
     *
     * @param Request $request
     * @param PostSeoBuilder $builder
     * @param string $page
     * @param int|string $id
     * @param string|null $payload
     *
     * @return Response
     * @throws ExceptionInterface
     */
    public function show(Request $request, PostSeoBuilder $builder, string $page, string $id, ?string $payload = null): Response
    {
        /**
         * @var $post Post
         */
        $post = $this->manager->repository->findOneByIdOrSlug($id);

        if ($post === null || $post->getUser() === null) {
            throw new HttpException(404);
        }

        if (($page === 'awarded') && count($post->getAwards()) === 0) {
            throw new HttpException(404);
        }

        if (($page === 'popular' || $page === 'latest') && $post->getStatus() < 3) {
            throw new HttpException(404);
        }

        $user = $this->getUser();
        if ($user instanceof UserInterface) {
            $this->manager->addView($post);
        }

        $request->getSession()->remove('back');

        $seoParams = [];
        if ($page === 'awarded') {
            $awards = $post->getAwards();
            if (isset($awards[0]) && $awards[0] instanceof Award) {
                $seoParams['date'] = $awards[0]->getCreatedAt();
            }
        }

        $seo = $builder->build($post, $page, $seoParams);

        $post = $this->normalize($post, ['groups' => 'post']);

        if ($user) {

            $post['favorite'] = count($this->manager->repository->getFavoritePostsByUser($user, [$post['id']])) > 0;

            $post['follow'] = count($this->manager->repository->getFollowedByPosts($user, [$post['id']])) > 0;

        }

        return $this->render('post/show.html.twig', compact('post', 'page', 'payload', 'seo'));
    }

    /**
     * @Route("/series/{id}", name="web_series_show_old")
     * @Route("/series/{id}/view", name="web_series_show")
     *
     * @param Request $request
     * @param RouterInterface $router
     * @param SessionInterface $session
     * @param SeriesSeoBuilder $seriesSeoBuilder
     * @param int $id
     *
     * @return Response
     *
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     */
    public function series(Request $request, RouterInterface $router, SessionInterface $session, SeriesSeoBuilder $seriesSeoBuilder, int $id)
    {
        $url = parse_url($request->server->get('HTTP_REFERER', '/'));
        $route = $router->match($url['path'] ?? '/');

        if ($route['_route'] !== 'web_series_show_old' && $route['_route'] !== 'web_series_show') {
            $session->set('series-back', $route['_route']);
            $session->set('series-back-arguments', $route);
        }

        /** @var Post $post */
        $post = $this->manager->repository->createQueryBuilder('s')
            ->where('s.type > 1')
            ->andWhere('s.isDeleted = false')
            ->andWhere('s.id = :id or s.oldId = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($post === null) {
            throw new HttpException(404);
        }

        $seo = $seriesSeoBuilder->build($post, 'series');

        $posts = $this->normalize($post->getChildren(), ['groups' => ['post']]);

        $post = $this->normalize($post,['groups' => ['post','store']]);

        return $this->render('post/series.html.twig', compact('post', 'posts', 'seo'));
    }
}
