<?php

namespace App\Controller\Web;

use App\Dto\Response\SeoDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/product-details", name="web_product_details")
     *
     * @return Response
     */
    public function media(): Response
    {
        return $this->render('pages/product-details.html.twig');
    }

    /**
     * @Route("/privacy-policy", name="web_privacy_policy")
     *
     * @return Response
     */
    public function privacy(): Response
    {
        return $this->render('pages/privacy-policy.html.twig');
    }

    /**
     * @Route("/terms-of-service", name="web_tos")
     *
     * @return Response
     */
    public function tos(): Response
    {
        return $this->render('pages/terms-of-service.html.twig');
    }

    /**
     * @Route("/faq", name="web_faq")
     *
     * @return Response
     */
    public function faq(): Response
    {
        return $this->render('pages/faq.html.twig');
    }

    /**
     * @Route("/user-agreement", name="web_user_agreement")
     *
     * @return Response
     */
    public function userAgreement(): Response
    {
        return $this->render('pages/user-agreement.html.twig');
    }
}
