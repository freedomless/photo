<?php

namespace App\Controller\Web;

use App\Dto\Request\PostFilterDto;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PhotoController
 * @package App\Controller
 */
class GalleryController extends BaseController
{

    /**
     * @var PostRepository
     */
    private PostRepository $repository;

    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;

    /**
     * GalleryController constructor.
     * @param PostRepository $repository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(PostRepository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/photos/awarded", name="web_gallery_awarded", defaults={"page": "awarded"})
     *
     *
     * @return Response
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function awarded()
    {
        $posts = $this->repository->getAwarded(1, 20);

        $posts = $this->normalize($posts,['groups' => ['post','api','award']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        $categories = $this->categoryRepository->getCategories();

        return $this->render('gallery/index.html.twig', compact('posts', 'categories'));
    }

    /**
     * @Route("/photos/series", name="web_gallery_series")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function series(Request $request)
    {

        /**
         * @var PostFilterDto $filters
         */
        $filters = count(array_filter($request->query->all())) > 0 ?  $this->denormalize($request->query->all(), PostFilterDto::class) : null;

        $series = $this->repository->getLatest(1, 20, $filters, true);

        $categories = $this->categoryRepository->getCategories();

        return $this->render('gallery/series.html.twig', compact('series', 'categories'));
    }

    /**
     * @Route("/photos/{page?latest}", name="web_gallery")
     *
     * @param Request $request
     * @param string $page
     *
     * @return Response
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function index(Request $request, string $page): Response
    {

        $posts = null;

        if ($page === 'latest') {

            /**
             * @var PostFilterDto $filters
             */
            $filters = count(array_filter($request->query->all())) > 0 ?  $this->denormalize($request->query->all(), PostFilterDto::class) : null;

            $posts = $this->repository->getLatest(1, 20, $filters, false);

        }

        $categories = $this->categoryRepository->getCategories();

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());


        return $this->render('gallery/index.html.twig', compact('posts', 'categories'));
    }

}
