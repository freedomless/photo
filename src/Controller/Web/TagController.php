<?php

namespace App\Controller\Web;

use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TagController
 * @package App\Controller\Web
 */
class TagController extends BaseController
{
    /**
     * @Route("/tag/{tag}", name="web_photo_by_tag", methods={"GET"})
     *
     * @param PostRepository $postRepository
     * @param string $tag
     *
     * @return Response
     * @throws \Exception
     */
    public function show(PostRepository $postRepository, string $tag): Response
    {
        $posts = $postRepository->getByTag($tag, 1, 20);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $postRepository->getFavoritesAndFollows($posts);

        return $this->render('photo/tag.html.twig', compact('tag', 'posts'));
    }
}
