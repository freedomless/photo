<?php

namespace App\Controller\Web;

use App\Builder\Seo\UserSeoBuilder;
use App\Builder\Seo\UserStoreSeoBuilder;
use App\Dto\Request\StoreFilterDto;
use App\Entity\User\Balance;
use App\Entity\User\Settings;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\OrderManager;
use App\EntityManager\PostManager;
use App\EntityManager\UserManager;
use App\Event\Notification\PayoutRequestedEvent;
use App\Exception\ApiException;
use App\Form\Model\ChangePasswordModel;
use App\Form\Type\ChangePasswordType;
use App\Form\Type\SettingsType;
use App\Form\Type\UserType;
use App\Helper\CacheTrait;
use App\Helper\DispatcherTrait;
use App\Repository\ProductRepository;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class UserController
 * @package App\Controller
 */
class AccountController extends BaseController
{

    use CacheTrait, DispatcherTrait;

    /**
     * @var UserManager
     */
    private UserManager $manager;

    /**
     * @var PostManager
     */
    private PostManager $postManager;

    /**
     * @var BaseManager
     */
    private BaseManager $settingsManager;

    /**
     * AccountController constructor.
     * @param UserManager $manager
     * @param PostManager $postManager
     * @param BaseManager $settingsManager
     */
    public function __construct(UserManager $manager, PostManager $postManager, BaseManager $settingsManager)
    {
        $this->manager = $manager;
        $this->postManager = $postManager;
        $this->settingsManager = $settingsManager;
    }

    /**
     * @Route("/profile/edit", name="web_profile_edit")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return Response
     * @throws ApiException
     */
    public function update(Request $request): Response
    {
        $user = $this->getUser();

        if ($user->getIsDeleted() == true) {
            throw new HttpException(404);
        }

        $form = $this->generateForm(UserType::class, $user, $request, 'web_profile_edit', function ($form) use ($user) {

            $this->manager->update($form->getData(), $user, false);
            $this->addToast('Profile edited!');
        });

        return $this->redirectOrRender($form, 'user/settings/index.html.twig', compact('user'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     *
     * @Route("/profile/{slug}/{type?single}", name="web_profile", requirements={"type": "single|series"})
     *
     * @param User $user
     *
     * @param string $type
     * @param UserSeoBuilder $userSeoBuilder
     * @return Response
     *
     * @throws ExceptionInterface
     */
    public function index(User $user, UserSeoBuilder $userSeoBuilder, string $type = 'single'): Response
    {
        $single = $type === 'single';
        if ($single) {
            $posts = $this->postManager->repository->getPublished($user, 1, 20);
            $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);
            $posts = $this->postManager->repository->getFavoritesAndFollows($posts, $this->getUser());
        } else {
            $posts = $this->postManager->repository->getPortfolio($user, 1, 20, !$single, true);
        }

        $seo = $userSeoBuilder->build($user, 'member');

        return $this->render('user/profile/published.html.twig', compact('user', 'posts', 'seo'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/portfolio/{type?single}", name="web_profile_portfolio", requirements={"type": "single|series"})
     *
     * @param User $user
     *
     * @param string $type
     * @return Response
     */
    public function portfolio(User $user, string $type = 'single'): Response
    {
        $single = $type === 'single';

        $posts = $this->postManager->repository->getPortfolio($user, 1, 20, !$single, false);

        if ($single) {
            $posts = $this->postManager->repository->getFavoritesAndFollows($posts, $this->getUser());
        }

        return $this->render('user/profile/portfolio.html.twig', compact('user', 'posts'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/series", name="web_profile_series")
     * @param User $user
     * @return Response
     */
    public function series(User $user)
    {
        $series = $this->postManager->repository->getPortfolio($user, 1, 20, true, true);

        return $this->render('user/profile/series.html.twig', compact('user', 'series'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/followers", name="web_profile_followers")
     * @param User $user
     * @return Response
     */
    public function followers(User $user): Response
    {
        return $this->render('user/profile/followers.html.twig', compact('user'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/favorites" , name="web_profile_favorites")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param User $user
     * @return Response
     * @throws ExceptionInterface
     */
    public function favorites(User $user): Response
    {
        $posts = $this->postManager->repository->getFavorites($user, 1, 20);
        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->postManager->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->render('user/profile/favorites.html.twig', compact('user', 'posts'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/prints-for-sale/{type?single}", name="web_profile_products", requirements={"type": "single|series"})
     *
     * @param User $user
     *
     * @param ProductRepository $productRepository
     * @param string $type
     * @param UserStoreSeoBuilder $seoBuilder
     * @return Response
     * @throws ExceptionInterface
     */
    public function products(User $user, ProductRepository $productRepository, UserStoreSeoBuilder $seoBuilder, string $type = 'single'): Response
    {
        $single = $type === 'single';

        $filter = new StoreFilterDto();
        $filter->setUser($user->getId());
        $posts = $productRepository->getAllFiltered($filter, 1, 20, !$single);

        $groups = ['index', 'post', 'tags', 'api', 'store', 'product'];

        if (!$single) {
            $groups[] = 'series';
        }

        $seo = $seoBuilder->build($user, '', ['is_single' => $single]);

        $posts = $this->normalize($posts, compact('groups'));

        return $this->render('user/profile/products.html.twig', compact('user', 'posts', 'seo'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/profile/{slug}/followed", name="web_profile_followed")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param User $user
     *
     * @return Response
     *
     * @throws ExceptionInterface
     */
    public function followed(User $user): Response
    {
        $posts = $this->postManager->repository->getFromFollowingUsers($user, 1, 20);
        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->postManager->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->render('user/profile/following.html.twig', compact('user', 'posts'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/orders/{page?1}/{limit?20}", name="web_orders", name="web_orders")
     * @Security("is_granted('ROLE_USER') and is_granted('STORE_ENABLED')")
     *
     * @param OrderManager $orderManager
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function orders(OrderManager $orderManager, int $page, int $limit): Response
    {

        $user = $this->getUser();

        $orders = $orderManager->repository->getSuccessfulByUser($user, $page, $limit);

        return $this->render('user/profile/orders.html.twig', compact('user', 'orders'));
    }

    /**
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     * @Route("/payout-request", name="web_profile_payout")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param BaseManager $balanceManager
     * @return Response
     * @throws ApiException
     */
    public function payoutRequest(BaseManager $balanceManager): Response
    {

        $user = $this->getUser();
        $balance = $balanceManager->repository->findOneBy(compact('user'), [], false);

        if (!$balance) {

            $balance = new Balance();
            $balance->setUser($user);

            $balanceManager->create($balance);

        }

        $email = $balance->getPayout();

        if (empty($email)) {
            $this->addToast('Please go to settings and add paypal email.');

            return $this->redirectToRoute('web_profile', ['slug' => $user->getSlug()]);
        }


        if ($balance->getTotal() === 0.0) {
            $this->addToast('Nothing to withdraw');

            return $this->redirectToRoute('web_profile', ['slug' => $user->getSlug()]);
        }

        $this->manager->createPayoutRequest($user);

        $this->addToast('Payout request created.');

        $this->dispatch(new PayoutRequestedEvent(null, $this->getUser(), ['balance' => $balance->getBlocked()]));

        return $this->redirectToRoute('web_profile', ['slug' => $user->getSlug()]);
    }

    /**
     *
     * @Route("/settings", name="web_settings")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     * @throws ApiException
     * @throws InvalidArgumentException
     */
    public function settings(Request $request)
    {
        /**
         * @var Settings $settings
         */
        $settings = $this->settingsManager->repository->findOneBy(['user' => $this->getUser()]);

        if ($settings->getUser()->getIsDeleted()) {
            throw new HttpException(404);
        }

        $form = $this->generateForm(SettingsType::class, $settings, $request, 'web_settings', function ($form) use ($settings) {
            $this->settingsManager->update($form->getData(), $settings);
            $this->cache->delete('settings_' . $this->getUser()->getId());
        });

        return $this->redirectOrRender($form, 'user/settings/general.html.twig');
    }

    /**
     * @Route("/change-password",name="web_change_password")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return RedirectResponse|Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        if ($user->getIsDeleted()) {
            throw new HttpException(404);
        }

        $form = $this->generateForm(ChangePasswordType::class, new ChangePasswordModel(), $request, 'web_profile_edit', function ($form) use ($user, $encoder) {

            /**
             * @var ChangePasswordModel $model
             */
            $model = $form->getData();

            if ($user->getPassword() && !$encoder->isPasswordValid($user, $model->getOldPassword())) {

                $form->get('oldPassword')->addError(new FormError('Wrong password'));

                return;
            }

            $this->manager->changePassword($user, $model->getPassword());

            $this->addToast('You password was changed!');

        }, compact('user'));

        return $this->redirectOrRender($form, 'user/settings/password.html.twig');
    }

}
