<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FollowController
 * @package App\Controller
 */
class FollowController extends AbstractController
{

    /**
     * @param int|null $id
     * @return Response
     *
     */
    public function index(?int $id = null): Response
    {

    }
}
