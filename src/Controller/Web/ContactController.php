<?php

namespace App\Controller\Web;

use App\Entity\User\User;
use App\Form\Model\ContactModel;
use App\Form\Type\ContactType;
use App\Mailer\Mailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends BaseController
{
    /**
     * @Route("/contacts", name="web_contacts")
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Request $request
     *
     * @param Mailer $emailService
     * @param string $siteEmail
     * @return Response
     */
    public function index(Request $request, Mailer $emailService, string $siteEmail): Response
    {
        $form = $this->generateForm(ContactType::class, new ContactModel(), $request, 'web_contacts', function ($form) use ($request, $siteEmail, $emailService) {
            // TODO: Activate and test re-captcha
            $formData = $form->getData()->toArray();

            /** @var User $currentUser */
            $currentUser = $this->getUser();
            $formData['app_user_email'] = $currentUser->getEmail();
            $formData['app_user_ip'] = $request->getClientIp();

            $replyTo = $formData['email'] ? $formData['email'] : $siteEmail;

            $emailService->send(
                'Contact us',
                $siteEmail,
                'contact-us',
                $formData,
                $replyTo
            );

            $this->addToast('Your message was sent. Please allow up to 24 hours for reply.');

        });

        return $this->redirectOrRender($form, 'contact/index.html.twig');
    }
}
