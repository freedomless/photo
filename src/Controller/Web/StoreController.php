<?php

namespace App\Controller\Web;

use App\Dto\Request\StoreFilterDto;
use App\Entity\Store\Invoice;
use App\Repository\CategoryRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class StoreController
 * @package App\Controller\Web
 */
class StoreController extends BaseController
{

    /**
     * @Route("/store/{type?single}/{page?1}/{limit?20}", name="web_store",requirements={"type":"single|series"},defaults={"type":"single"})
     *
     * @param Request $request
     * @param PostRepository $postRepository
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     * @param string $type
     * @param int $page
     * @param int $limit
     *
     * @return Response
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function index(Request $request,
                          PostRepository $postRepository,
                          ProductRepository $productRepository,
                          CategoryRepository $categoryRepository,
                          string $type,
                          int $page = 1,
                          int $limit = 20)
    {

        /**
         * @var StoreFilterDto $filter
         */
        $filter = $this->denormalize($request->query->all(), StoreFilterDto::class);


        $series = $type === 'series';

        $posts = $productRepository->getAllFiltered($filter, $page, $limit, $series);


        $groups = ['index', 'post', 'tags','api','store','product'];

        if ($series) {
            $groups[] = 'series';
        }

        $categories = $categoryRepository->getCategories();

        $posts = $this->normalize($posts, compact('groups'));


        $posts = $postRepository->getFavoritesAndFollows($posts, $this->getUser());


        return $this->render('store/index.html.twig', compact('posts', 'categories'));
    }


    /**
     * @Route("/invoice/{token}", name="web_invoice")
     *
     * @param Invoice $invoice
     * @param ConfigurationRepository $configurationRepository
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function invoice(Invoice $invoice, ConfigurationRepository $configurationRepository)
    {
        $configuration = $configurationRepository->getConfig();

        return $this->render('store/success.html.twig', compact('invoice', 'configuration'));
    }

    /**
     * @return Response
     */
    public function fail()
    {

        return $this->render('store/fail.html.twig');
    }
}
