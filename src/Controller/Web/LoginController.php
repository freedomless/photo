<?php

namespace App\Controller\Web;

use App\Authentication\SocialAuthentication;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="web_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('web_profile', ['slug' => $this->getUser()->getSlug()]);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login/index.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/login/{network}", name="web_social_login")
     * @Route("/login/check/{network}", name="web_social_login_auth")
     * @param SocialAuthentication $authentication
     * @param string $network
     * @return RedirectResponse
     */
    public function social(SocialAuthentication $authentication, string $network): RedirectResponse
    {
        return $authentication->connect($network);
    }

    /**
     * @Route("/logout", name="web_logout")
     */
    public function logout(): void
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
