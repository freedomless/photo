<?php

namespace App\Controller\Web;

use App\Exception\ApiException;
use App\Helper\PaginatorTrait;
use App\Helper\SerializationTrait;
use App\Helper\TranslationTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController
 * @package App\Controller\Web
 */
abstract class BaseController extends AbstractController
{

    use TranslationTrait,
        SerializationTrait,
        PaginatorTrait;

    /**
     * @param string $type
     * @param $model
     * @param Request $request
     * @param string $redirect
     * @param callable $action
     * @param array $extra
     * @param array $parameters
     * @return FormView|RedirectResponse
     */
    protected function generateForm(string $type,
                                    $model,
                                    Request $request,
                                    string $redirect,
                                    callable $action,
                                    array $extra = [],
                                    array $parameters = [])
    {
        $form = $this->createForm($type, $model, $extra);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $response = $action($form);

            if ($response) {
                return $response;
            }

            return $this->redirectToRoute($redirect, $parameters);

        }

        return $form->createView();
    }

    /**
     * @param $form
     * @param string|null $view
     * @param array $data
     * @return Response
     */
    protected function redirectOrRender($form, ?string $view = null, array $data = []): Response
    {
        if ($form instanceof Response) {
            return $form;
        }

        return $this->render($view, array_merge(compact('form'), $data));
    }

    /**
     * @param string $message
     * @param string|null $image
     * @param string|null $color
     * @param string|null $icon
     * @param string|null $theme
     */
    protected function addToast(string $message, ?string $image = null, ?string $color = null, ?string $icon = null, ?string $theme = null): void
    {
        $data = compact('message', 'color', 'image', 'icon', 'theme');

        $this->addFlash('toast', json_encode(array_filter($data)));
    }
}
