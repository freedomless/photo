<?php

namespace App\Controller\Web;

use App\Builder\Seo\IndexSeoBuilder;
use App\Builder\Seo\SeriesSeoBuilder;
use App\Entity\Photo\Award;
use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use App\Helper\DispatcherTrait;
use App\Helper\PaginatorTrait;
use App\Helper\SerializationTrait;
use App\Helper\VideoPresentationHelper;
use App\Repository\AwardRepository;
use App\Repository\PostRepository;
use App\Repository\SelectionSeriesRepository;
use DateTime;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class DefaultController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    use PaginatorTrait, SerializationTrait, DispatcherTrait;

    /**
     * @var AwardRepository
     */
    private AwardRepository $awardRepository;

    /**
     * @var PostRepository
     */
    private PostRepository $repository;

    /**
     * @var SelectionSeriesRepository
     */
    private SelectionSeriesRepository $selectionSeriesRepository;

    /**
     * @var mixed
     */
    private $photoOfTheDay;

    /**
     * @var mixed
     */
    private $selectionSeries;

    /**
     * HomeController constructor.
     * @param PostRepository $repository
     * @param SelectionSeriesRepository $selectionSeriesRepository
     * @param AwardRepository $awardRepository
     * @throws InvalidArgumentException
     */
    public function __construct(
        PostRepository $repository,
        SelectionSeriesRepository $selectionSeriesRepository,
        AwardRepository $awardRepository
    ) {
        $this->awardRepository = $awardRepository;
        $this->repository = $repository;
        $this->selectionSeriesRepository = $selectionSeriesRepository;

        /** @var Award $photoOfTheDay */
        $this->photoOfTheDay = $this->awardRepository->findCurrentPhotoOfTheDay();
        $this->selectionSeries = $this->selectionSeriesRepository->getVisible();
    }

    /**
     * @Route("/latest", name="web_latest", defaults={"page":"latest"})
     *
     * @return Response
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function latest(): Response
    {
        $posts = $this->repository->getLatest(1, $this->container->getParameter('app.default_limit'));

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        $photo_of_the_day = $this->photoOfTheDay;
        $selection_series = $this->selectionSeries;

        return $this->render('home/index.html.twig', compact('posts', 'photo_of_the_day', 'selection_series'));
    }

    /**
     * @Route("/", name="web_popular", defaults={"page":"popular"})
     *
     * @param VideoPresentationHelper $videoHelper
     * @return Response
     * @throws InvalidArgumentException
     */
    public function popular(VideoPresentationHelper $videoHelper): Response
    {
        $user = $this->getUser();

        $posts = $this->repository->getPopular(1, 20);

        $posts = $this->repository->getFavoritesAndFollows($posts, $user);

        $photo_of_the_day = $this->photoOfTheDay;
        $selection_series = $this->selectionSeries;

        $params = compact('posts', 'photo_of_the_day', 'selection_series');
        if ($video = $videoHelper->getRandomVideo()) {
            $params = compact('posts', 'photo_of_the_day', 'selection_series', 'video');
        }

        return $this->render('home/index.html.twig', $params);
    }

    /**
     * @Route("/photo-of-the-day/{id}/{date?}", name="web_photo_of_the_day")
     *
     *
     *
     * @param int $id
     * @param AwardRepository $repository
     * @param IndexSeoBuilder $indexSeoBuilder
     * @return Response
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function photoOfTheDayHistory(
        int $id,
        AwardRepository $repository,
        IndexSeoBuilder $indexSeoBuilder
    ): Response {
        $photo_of_the_day = $repository->findOneByIdOrCurrent($id);

        if (null === $photo_of_the_day || $photo_of_the_day->getCreatedAt() > new DateTime()) {
            /** @var Award $photo_of_the_day */
            $photo_of_the_day = $repository->findCurrentPhotoOfTheDay();
        }

        $user = $this->getUser();

        $posts = $this->repository->getPopular(1, 20);

        $posts = $this->repository->getFavoritesAndFollows($posts, $user);

        /** @var Post $seoPhoto */
        $seoPhoto = $photo_of_the_day->getPost();

        $seo = $indexSeoBuilder->build($seoPhoto, 'index_page', ['date' => ($photo_of_the_day->getCreatedAt())]);

        $photo_of_the_day = $this->normalize($photo_of_the_day, ['groups' => 'post']);

        return $this->render('home/index.html.twig', compact('posts', 'photo_of_the_day', 'seo'));
    }

    /**
     * @Route("/selection-series/{id}/{slug?}", name="series_selection_history", methods={"GET"})
     * @ParamConverter("SelectionSeries", options={"mapping": {"id": "id"}})
     * @param SelectionSeries $selection_series
     * @param SeriesSeoBuilder $seriesSeoBuilder
     * @return Response
     * @throws InvalidArgumentException
     */
    public function selectionSeriesHistory(SelectionSeries $selection_series, SeriesSeoBuilder $seriesSeoBuilder)
    {
        $seo = $seriesSeoBuilder->build($selection_series->getPost(), 'series');

        $seo->setImage($selection_series->getCover());
        $seo->setDescription(sprintf('"%s"', $selection_series->getPost()->getTitle()));

        $user = $this->getUser();

        $posts = $this->repository->getPopular(1, 20);

        $posts = $this->repository->getFavoritesAndFollows($posts, $user);

        $photo_of_the_day = $this->photoOfTheDay;

        return $this->render("home/index.html.twig", compact('seo', 'posts', 'selection_series'));
    }

    /**
     * @Route("/series", name="web_series")
     *
     * @return Response
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function series(): Response
    {
        $series = $this->repository->getLatest(1, 20, null, true);

        $photo_of_the_day = $this->photoOfTheDay;
        $selection_series = $this->selectionSeries;

        return $this->render('home/series.html.twig', compact('series', 'photo_of_the_day', 'selection_series'));
    }
}
