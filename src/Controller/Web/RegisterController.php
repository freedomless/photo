<?php

namespace App\Controller\Web;

use App\Authentication\BaseAuthentication;
use App\Entity\User\User;
use App\EntityManager\UserManager;
use App\Event\Notification\UserConfirmedEvent;
use App\Event\Notification\UserRegisteredEvent;
use App\Form\Type\RegistrationType;
use App\Helper\DispatcherTrait;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use ReCaptcha\ReCaptcha;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RegisterController
 * @package App\Controller
 */
class RegisterController extends BaseController
{

    use DispatcherTrait;

    /**
     * @var UserManager
     */
    private UserManager $manager;

    /**
     * @var BaseAuthentication
     */
    private BaseAuthentication $authentication;

    /**
     * RegisterController constructor.
     * @param UserManager $manager
     * @param BaseAuthentication $authentication
     */
    public function __construct(UserManager $manager, BaseAuthentication $authentication)
    {
        $this->manager = $manager;
        $this->authentication = $authentication;
    }

    /**
     * @Route("/register", name="web_register")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function register(Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('web_profile', ['slug' => $this->getUser()->getSlug()]);
        }

        $form = $this->generateForm(RegistrationType::class, new User(), $request, 'web_register', function ( $form) use ($request) {

            $recaptcha = new ReCaptcha(getenv('GOOGLE_RECAPTCHA_SECRET'));
            $resp = $recaptcha->setExpectedHostname(getenv('SITE_URL'))
                ->verify($form['recaptcha']->getData(), $request->getClientIp());

            if (!$resp->isSuccess()) {

                $form->addError(new FormError('Invalid recaptcha'));

                return $form->createView();
            }

            /**
             * @var User $user
             */
            $user = $this->manager->create($form->getData(), false, []);

            $verification = $this->manager->createVerification($user);

            $this->dispatch(new UserRegisteredEvent(null, $user, compact('verification')));

            $this->authentication->authenticate($user);

        });

        return $this->redirectOrRender($form, 'security/register/index.html.twig');
    }

    /**
     * @Route("/confirmation/{token}", name="web_confirmation")
     *
     * @param string $token
     *
     * @return RedirectResponse|NotFoundHttpException
     *
     * @throws NonUniqueResultException
     */
    public function confirm(string $token)
    {
        $user = $this->manager->verificationTokenExists($token);

        if ($user === null) {
            throw $this->createNotFoundException('Verification Token Not Found!');
        }

        $this->manager->verify($user);

        $this->dispatch(new UserConfirmedEvent(null, $user));

        $this->addToast('Email verified!');

        return $this->redirectToRoute('web_profile', ['slug' => $user->getSlug()]);
    }

    /**
     * @Route("/resend-confirmation", name="web_resend_confirmation")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @return RedirectResponse
     * @throws Exception
     */
    public function resendConfirmation(): RedirectResponse
    {
        $user = $this->getUser();

        if ($user->getIsVerified()) {
            return $this->redirectToRoute('web_profile');
        }

        $verification = $this->manager->createVerification($user);

        $this->dispatch(new UserRegisteredEvent(null, $user, compact('verification')));

        $this->addToast('Confirmation email re-send. Please check Your email Inbox.');

        return $this->redirectToRoute('web_profile', ['slug' => $user->getSlug()]);

    }
}
