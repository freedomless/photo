<?php

namespace App\Controller\Web;

use App\Entity\Photo\Album;
use App\Entity\User\User;
use App\EntityManager\AlbumManager;
use App\EntityManager\UserManager;
use App\Form\Type\AlbumType;
use App\Repository\AlbumRepository;
use phpDocumentor\Reflection\DocBlock\Description;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class AlbumController
 * @package App\Controller\Web
 */
class AlbumController extends BaseController
{
    /**
     * @var AlbumManager
     */
    private AlbumManager $manager;

    /**
     * AlbumController constructor.
     * @param AlbumManager $manager
     */
    public function __construct(AlbumManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/profile/{slug}/albums/{page?1}", name="web_albums")
     *
     * @Entity("user", expr="repository.findOneBy({slug: slug, isDeleted:false})")
     *
     * @param Request $request
     * @param User $user
     * @param AlbumRepository $repository
     * @param int $page
     *
     * @return Response
     */
    public function index(Request $request, User $user, AlbumRepository $repository, int $page = 1): Response
    {
        $repository->setPaginationType('web');

        $page = $request->query->get('page', $page);

        $albums = $repository->getByUser($user, $page, 10);

        if ($request->query->get('ajax')) {
            return $this->render('album/album.html.twig', compact('albums'));
        }

        return $this->render('album/index.html.twig', compact('albums', 'user'));

    }

    /**
     * @Route("/album/{id}", name="web_album_show")
     *
     * @param Album $album
     * @return Response
     * @throws ExceptionInterface
     */
    public function show(Album $album): Response
    {

        $posts = $this->manager->repository->getOrderedByCover($album);

        $posts = $this->normalize($posts, ['groups' => 'post']);

        return $this->render('album/show.html.twig', compact('album', 'posts'));
    }

    /**
     * @Route("/album", name="web_album_create")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $user = $this->getUser();

        if ($request->query->get('ajax')) {

            $posts = $this->manager->repository->getPosts($user, null, $request->query->get('page'));

            return $this->render('album/_partials/photos.html.twig', compact('posts'));
        }

        $album = new Album();
        $form = $this->generateForm(
            AlbumType::class,
            $album,
            $request,
            'web_albums',
            function ($form) {

                $album = $form->getData();

                $album->setUser($this->getUser());
                $album->setCover($album->getPosts()->first());

                $this->manager->create($album);

                $this->addToast('Album created!');

            },
            ['user' => $user],
            ['slug' => $user->getSlug()]
        );

        return $this->redirectOrRender($form, 'album/form.html.twig', compact('album'));
    }

    /**
     * @Route("/album/{id}/update", name="web_album_update")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @param Album $album
     * @return RedirectResponse|Response
     */
    public function update(Request $request, Album $album)
    {
        $user = $this->getUser();

        if ($album->getUser()->getId() !== $user->getId()) {
            throw $this->createNotFoundException();
        }

        if ($request->query->get('ajax')) {

            $posts = $this->manager->repository->getPosts($user, $album, $request->query->get('page'));

            return $this->render('album/_partials/photos.html.twig', compact('posts'));
        }

        $form = $this->generateForm(
            AlbumType::class,
            $album,
            $request,
            'web_albums',
            function ($form) {

                $this->manager->flush($form->getData());

                $this->addToast('Album Updated!');

            },
            ['user' => $user, 'method' => 'PUT'],
            ['slug' => $user->getSlug()]
        );

        return $this->redirectOrRender($form, 'album/form.html.twig',compact('album'));
    }

    /**
     * @Route("/album/{id}/delete", name="web_album_delete")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Album $album
     *
     * @return RedirectResponse
     */
    public function destroy(Album $album): RedirectResponse
    {
        $user = $this->getUser();

        if ($album->getUser()->getId() !== $user->getId()) {
            throw $this->createNotFoundException();
        }

        $this->manager->delete($album, false);

        return $this->redirectToRoute('web_albums', ['slug' => $user->getSlug()]);
    }
}
