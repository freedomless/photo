<?php

namespace App\Controller\Web;

use App\Entity\Forum\Opinion;
use App\Entity\Forum\Topic;
use App\Exception\ApiException;
use App\Form\Type\OpinionType;
use App\EntityManager\BaseManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class OpinionController
 * @package App\Controller\Web
 */
class OpinionController extends BaseController
{

    /**
     * @var BaseManager
     */
    private BaseManager $sectionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $opinionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $topicManager;

    public function __construct(BaseManager $sectionManager,
                                BaseManager $opinionManager,
                                BaseManager $topicManager)
    {

        $this->sectionManager = $sectionManager;
        $this->opinionManager = $opinionManager;
        $this->topicManager = $topicManager;
    }

    /**
     * @Route("/forum/topic/{slug}/{page?1}", requirements={"page": "\d+"}, name="web_forum_section_opinion", priority=3)
     *
     * @param Request $request
     * @param string $slug
     * @param int $page
     * @param int $limit
     *
     * @return Response
     * @throws ApiException
     */
    public function index(Request $request, string $slug, int $page = 1, int $limit = 10): Response
    {
        /**
         * @var Topic $topic
         */
        $topic = $this->topicManager->repository->findOneBy(compact('slug'));

        if ($topic === null) {
            throw new HttpException(404);
        }

        $opinions = $this->opinionManager->repository->getOpinions($topic, $page, $limit);

        $form = $this->createForm(OpinionType::class, new Opinion());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();

            if ($user instanceof UserInterface) {
                /**
                 * @var Opinion $model
                 */
                $model = $form->getData();
                $model->setTopic($topic);
                $model->setUser($user);

                $this->opinionManager->create($model);

                $page = $this->opinionManager->repository->getLastPage($topic->getId(), $limit);

                return $this->redirectToRoute('web_forum_section_opinion', compact('slug', 'page'));
            }

            $form->addError(new FormError('No cheating!!!'));
        }

        $form = $form->createView();

        return $this->render('forum/opinions.html.twig', compact('topic', 'opinions', 'form'));
    }

    /**
     * @Route("/forum/opinion/{id}/update", name="web_forum_opinion_update", priority=1)
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, int $id)
    {
        $user = $this->getUser();

        if ($this->isGranted('ROLE_SUPER_ADMIN', $user)) {
            $opinion = $this->opinionManager->repository->findOneBy(['id' => $id]);
        } else {
            $opinion = $this->opinionManager->repository->findOneBy(['id' => $id, 'user' => $user->getId()]);
        }

        $form = $this->generateForm(OpinionType::class, $opinion, $request, 'web_forum_section_opinion', function ($form) use ($opinion) {

            $user = $this->getUser();

            if ($user instanceof UserInterface) {
                /**
                 * @var $model Opinion
                 */
                $model = $form->getData();

                $this->opinionManager->update($model, $opinion);

                $this->addToast('Opinion is updated!');

                return;
            }


            $form->addError(new FormError('No cheating!!!'));

        }, [], ['slug' => $opinion->getTopic()->getSlug()]);

        return $this->redirectOrRender($form, 'forum/opinion.html.twig');
    }

    /**
     * @Route("/forum/opinion/{id}/delete", name="web_forum_opinion_destroy", priority=2)
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $user = $this->getUser();

        if ($this->isGranted('ROLE_SUPER_ADMIN', $user)) {
            $opinion = $this->opinionManager->repository->findOneBy(['id' => $id]);
        } else {
            $opinion = $this->opinionManager->repository->findOneBy(['id' => $id, 'user' => $user->getId()]);
        }

        if ($opinion === null || ($opinion !== null && $opinion->getId() === $opinion->getTopic()->getOpinions()->first()->getId())) {
            throw new HttpException(404);
        }

        $slug = $opinion->getTopic()->getSlug();
        $this->opinionManager->delete($opinion->getId(), false);

        return $this->redirectToRoute('web_forum_section_opinion', compact('slug'));

    }

}
