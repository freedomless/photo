<?php

namespace App\Controller\Web;

use App\EntityManager\BaseManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SectionController
 * @package App\Controller\Web
 */
class SectionController extends AbstractController
{

    /**
     * @var BaseManager
     */
    private BaseManager $sectionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $opinionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $topicManager;

    /**
     * SectionController constructor.
     * @param BaseManager $sectionManager
     * @param BaseManager $opinionManager
     * @param BaseManager $topicManager
     */
    public function __construct(BaseManager $sectionManager,
                                BaseManager $opinionManager,
                                BaseManager $topicManager)
    {
        $this->sectionManager = $sectionManager;
        $this->opinionManager = $opinionManager;
        $this->topicManager = $topicManager;
    }

    /**
     * @Route("/forum/{page?1}", name="web_forum")
     *
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function index(int $page = 1, int $limit = 20): Response
    {

        $sections = $this->sectionManager->repository->getSections($page, $limit);

        $opinions = [];

        foreach ($sections as $section) {
            $opinions[$section->getId()] = $this->opinionManager->repository->getLastForSection($section);
        }

        return $this->render('forum/index.html.twig', compact('sections', 'opinions'));
    }

    /**
     * @Route("/forum/{slug}/section/{page?1}", name="web_forum_section")
     *
     * @param string $slug
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function show(string $slug, int $page = 1, int $limit = 5): Response
    {
        $section = $this->sectionManager->repository->findOneBy(compact('slug'));

        if ($section === null) {
            throw new HttpException(404);
        }

        $topics = $this->topicManager->repository->getTopics($section, $page, $limit);

        return $this->render('forum/show.html.twig', compact('section', 'topics'));
    }

}
