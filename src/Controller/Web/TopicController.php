<?php

namespace App\Controller\Web;

use App\Entity\Forum\Opinion;
use App\Entity\Forum\Topic;
use App\EntityManager\BaseManager;
use App\Form\Model\TopicModel;
use App\Form\Type\TopicType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TopicController
 * @package App\Controller\Web
 */
class TopicController extends BaseController
{

    /**
     * @var BaseManager
     */
    private BaseManager $sectionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $opinionManager;

    /**
     * @var BaseManager
     */
    private BaseManager $topicManager;

    public function __construct(BaseManager $sectionManager,
                                BaseManager $opinionManager,
                                BaseManager $topicManager)
    {

        $this->sectionManager = $sectionManager;
        $this->opinionManager = $opinionManager;
        $this->topicManager = $topicManager;
    }

    /**
     * @Route("/topic/{slug}/create", name="web_forum_topic_create")
     *
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Request $request
     * @param string $slug
     *
     * @return Response
     */
    public function create(Request $request, string $slug)
    {
        $topic = new Topic();
        $topic->addOpinion(new Opinion());

        $form = $this->generateForm(TopicType::class, $topic, $request, 'web_forum_section', function ($form) use ($slug) {

            /**
             * @var Topic $topic
             */
            $topic = $form->getData();

            $topic->setOwner($this->getUser());
            $topic->setSection($this->sectionManager->repository->findOneBy(compact('slug')));
            $topic->getOpinions()->first()->setTopic($topic);
            $topic->getOpinions()->first()->setUser($this->getUser());

            $this->topicManager->create($topic);

        }, [], compact('slug'));

        return $this->redirectOrRender($form, 'forum/topic.html.twig');
    }

    /**
     * @Route("/forum/topic/{slug}/update", name="web_forum_topic_update")
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     *
     * @param Request $request
     * @param string $slug
     *
     * @return Response
     */
    public function update(Request $request, string $slug)
    {
        if (!$request->getSession()->has('topic-back-button')) {
            $request->getSession()->set('topic-back-button', $request->headers->get('referer'));
        }


        /**
         * @var Topic $topic
         */
        $topic = $this->topicManager->repository->findOneBy(compact('slug'));

        if ($topic === null) {
            throw new HttpException(404);
        }

        $model = new TopicModel();
        $model->setTitle($topic->getTitle());
        $model->setSection($topic->getSection());
        $model->setOpinions(new ArrayCollection([$topic->getOpinions()->first()]));

        $form = $this->generateForm(TopicType::class, $model, $request, 'web_forum_section', function ($form) use ($topic) {

            $model = $form->getData();

            $topic->setTitle($model->getTitle());
            $topic->setSection($model->getSection());

            $this->topicManager->update($topic, $topic);

            $this->opinionManager->update($topic->getOpinions()->first(),$model->getOpinions()->first());


            $this->addToast('Topic edited!');
        }, ['update' => true,'method' => 'PUT', 'data_class' => TopicModel::class], ['slug' => $topic->getSection()->getSlug()]);

        return $this->redirectOrRender($form, 'forum/topic.html.twig');

    }

    /**
     * @Route("/topic/{slug}/delete", name="web_forum_topic_delete")
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     *
     * @param string $slug
     *
     * @return RedirectResponse
     */
    public function destroy(string $slug): RedirectResponse
    {
        $owner = $this->getUser();



        if ($this->isGranted('ROLE_SUPER_ADMIN', $owner)) {
            /**
             * @var Topic $topic
             */
            $topic = $this->topicManager->repository->findOneBy(compact('slug'), [], false);
        } else {
            /**
             * @var Topic $topic
             */
            $topic = $this->topicManager->repository->findOneBy(compact('slug', 'owner'), [], false);

        }


        if ($topic === null) {
            throw $this->createNotFoundException();
        }

        $slug = $topic->getSection()->getSlug();

        $this->topicManager->delete($topic, false);

        return $this->redirectToRoute('web_forum_section', compact('slug'));
    }

}
