<?php

namespace App\Controller\Web;

use App\Entity\Photo\Post;
use App\Entity\Store\Product;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\PostManager;
use App\EntityManager\ProductManager;
use App\Enum\ProductStatusEnum;
use App\Form\Type\ProductType;
use App\Repository\ConfigurationRepository;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ProductController
 * @package App\Controller\Web
 * @Security("is_granted('STORE_ENABLED')")
 */
class ProductController extends BaseController
{

    /**
     * @var ProductManager
     */
    private ProductManager $manager;

    /**
     * ProductController constructor.
     * @param ProductManager $manager
     */
    public function __construct(ProductManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/manage/products/{page?1}", name="web_manage_products")
     *
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function index(Request $request, int $page = 1, int $limit = 20)
    {

        $page = $request->query->get('page',$page);

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $this->manager->repository->setPaginationType('web');

        $posts = $this->manager->repository->getProductPosts($user, $page, $limit);

        //For infinite scroll in manage single/series
        if ($request->isXmlHttpRequest()) {

            return $this->render('product/posts.html.twig', compact('posts'));
        }

        return $this->render('product/index.html.twig', compact('posts'));
    }

    /**
     * @Route("/sold/{page?1}", name="web_manage_sold")
     *
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param BaseManager $orderItemManager
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function sold(BaseManager $orderItemManager, int $page = 1, int $limit = 20)
    {

        $items = $orderItemManager->repository->getByUser($this->getUser(), $page, $limit);


        return $this->render('product/sold.html.twig', compact('items'));
    }

    /**
     * @Route("/for-sale/{id}/{type?single}", name="web_photo_for_sale", requirements={"type": "single|series" }, priority=10)
     *
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Request $request
     * @param RouterInterface $router
     * @param SessionInterface $session
     * @param ConfigurationRepository $configurationRepository
     * @param Post $post
     * @param string $type
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function addToStore(Request $request,
                               RouterInterface $router,
                               SessionInterface $session,
                               ConfigurationRepository $configurationRepository,
                               Post $post,
                               string $type): Response
    {

        $route = $router->match(parse_url($request->server->get('HTTP_REFERER'))['path']);

        if ($route['_route'] !== 'web_photo_for_sale') {
            $session->set('product-back', $route['_route']);
            $session->set('product-back-arguments', $route);
        }

        $workflow = $this->container->get('state_machine.post_publishing');

        if (!$workflow->can($post, 'to_for_sale')) {

            $this->addToast('This photo is not eligible for store yet!', $post->getImage()->getThumbnail());

            return $this->redirectToRoute('web_photos_manage');
        }

        $configuration = $configurationRepository->getConfig();

        $product = $post->getProduct() ?? new Product($post);

        if ($post->getType() > 1) {

            foreach ($post->getChildren() as $child) {

                $product->addChild($child->getProduct() ?? new Product($child));

            }

        }

        $form = $this->generateForm(ProductType::class, $product, $request, 'web_photo_for_sale', function ($form) use ($type, $product) {

            [$completed, $autoApprove] = $this->manager->addToStore($form->getData());

            if ($completed) {

                if ($autoApprove) {
                    $this->addToast('Your ' . ($type === 'single' ? 'photo' : 'series') . ' is in store!');

                } else {

                    if ($product->getStatus() === ProductStatusEnum::PUBLISHED) {
                        $this->addToast('Your product was updated!');
                    } else {
                        $this->addToast('Your ' . ($type === 'single' ? 'photo' : 'series') . ' was add for approval!');
                    }
                }

            } else {
                $this->addToast('Your have to add print image and price in order to your product to be send for approval!');
            }

        }, ['type'          => $type === 'single' ? 1 : 2,
            'configuration' => $configuration], ['type' => $type, 'id' => $post->getId()]);

        return $this->redirectOrRender($form, 'photo/upload-print.html.twig', compact('configuration', 'product'));
    }

    /**
     * @Route("/product/{id}/delete", name="web_product_delete")
     *
     * @Security("is_granted('ROLE_MEMBER')")
     *
     * @param Post $post
     * @param ConfigurationRepository $repository
     * @param PostManager $manager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function delete(Post $post, ConfigurationRepository $repository, PostManager $manager)
    {

        if ($post->getUser()->getId() !== $this->getUser()->getId()) {
            throw  $this->createNotFoundException();
        }

        $deletionInterval = $repository->getConfig()->getPostDeletionInterval() ?? 1;

        $deletionDate = (new \DateTime())->modify("+ {$deletionInterval} days");

        $post->setDeletionDate($deletionDate);

        foreach ($post->getChildren() as $child) {
            $child->setDeletionDate($deletionDate);

            $manager->flush($child);
        }

        $manager->flush($post);

        return $this->redirectToRoute('web_manage_products');
    }

}
