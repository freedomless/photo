<?php

namespace App\Controller\Web;

use App\Entity\EntityInterface;
use App\Entity\Store\Cart;
use App\Entity\Store\CartItem;
use App\EntityManager\BaseManager;
use App\EntityManager\InvoiceManager;
use App\Event\Notification\System\NewOrderEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use App\Helper\PriceCalculator;
use App\Repository\ConfigurationRepository;
use App\Repository\MaterialRepository;
use App\Repository\ProductRepository;
use App\Repository\SizeRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class CartController
 * @package App\Controller\Web
 */
class CartController extends BaseController
{

    /**
     * @var BaseManager
     */
    private BaseManager $cartManager;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var PriceCalculator
     */
    private PriceCalculator $calculator;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * CartController constructor.
     * @param SessionInterface $session
     * @param PriceCalculator $calculator
     * @param ConfigurationRepository $configurationRepository
     * @param BaseManager $cartManager
     */
    public function __construct(SessionInterface $session,
                                PriceCalculator $calculator,
                                ConfigurationRepository $configurationRepository,
                                BaseManager $cartManager)
    {
        $this->cartManager = $cartManager;
        $this->session = $session;
        $this->calculator = $calculator;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @Route("/cart", name="web_cart_index")
     * @param SizeRepository $sizeRepository
     * @param BaseManager $cartItemManager
     * @param MaterialRepository $materialRepository
     * @return Response
     * @throws ApiException
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function index(SizeRepository $sizeRepository,
                          BaseManager $cartItemManager,
                          MaterialRepository $materialRepository)
    {
        $materials = $materialRepository->getMaterials();
        $sizes = $sizeRepository->getSizes();

        // todo: FIX ME shMitko
        $countries = [
            "AT" => "Austria",
            "BE" => "Belgium",
            "BG" => "Bulgaria",
            "CH" => "Switzerland",
            "CY" => "Cyprus",
            "CZ" => "Czechia",
            "DE" => "Germany",
            "DK" => "Denmark",
            "EE" => "Estonia",
            "ES" => "Spain",
            "FI" => "Finland",
            "FR" => "France",
            "GB" => "United Kingdom",
            "GR" => "Greece",
            "HR" => "Croatia",
            "HU" => "Hungary",
            "IE" => "Ireland",
            "IS" => "Iceland",
            "IT" => "Italy",
            "LT" => "Lithuania",
            "LU" => "Luxembourg",
            "LV" => "Latvia",
            "NL" => "Netherlands",
            "NO" => "Norway",
            "PL" => "Poland",
            "PT" => "Portugal",
            "RO" => "Romania",
            "RS" => "Serbia",
            "SE" => "Sweden",
            "SI" => "Slovenia",
            "SK" => "Slovakia"
        ];

        $configuration = $this->normalize($this->configurationRepository->getConfig());

        $user = $this->getUser();
        $session = $this->session->getId();

        if ($user !== null) {
            $cart = $this->cartManager->repository->findOneBy(compact('user'), [], false);
        } else {
            $cart = $this->cartManager->repository->findOneBy(compact('session'), [], false);
        }

        if ($cart === null) {
            $entity = new Cart();
            $user === null ? $entity->setSession($session) : $entity->setUser($user);
            $cart = $this->cartManager->create($entity);
        }

        /**
         * @var CartItem $item
         */
        foreach ($cart->getItems() as $item) {

            $deletionDate = $item->getProduct()->getPost()->getDeletionDate();

            if ($deletionDate !== null && $deletionDate < new \DateTime()) {
                $cartItemManager->delete($item, false);
            }

            $price = $this->calculator->calculatePrice($item->getProduct(), $item->getSize(), $item->getMaterial(), $item->getQuantity());
            $item->setTotal($price['total']);
            $item->setDiscount($price['discount']);

            foreach ($item->getProduct()->getChildren() as $child) {
                $price = $this->calculator->calculatePrice($child, $item->getSize(), $item->getMaterial(), $item->getQuantity());
                $child->setTotal($price['total']);
            }

        }

        $cart = $this->normalize($cart, ['groups' => ['cart', 'post', 'index']]);

        $items = $cart['items'];


        return $this->render('store/cart.html.twig', compact('materials', 'user', 'sizes', 'countries', 'items', 'configuration'));

    }
}
