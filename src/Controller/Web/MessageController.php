<?php

namespace App\Controller\Web;

use App\Helper\SerializationTrait;
use App\EntityManager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class MessageController
 * @package App\Controller
 */
class MessageController extends AbstractController
{
    use SerializationTrait;

    /**
     * @var UserManager
     */
    private UserManager $userManager;

    /**
     * MessageController constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager) {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/messages", name="web_messages")
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ExceptionInterface
     */
    public function index(Request $request): Response
    {
        $user = $this->getUser();

        $receiver = null;

        $query = (int)$request->get('user');

        if ($query) {
            $receiver = $this->userManager->repository->find($query);
            $receiver = $this->normalize($receiver, ['groups' => 'basic_user']);
        }

        return $this->render('message/index.html.twig', compact('user', 'receiver'));
    }
}
