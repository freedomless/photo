<?php

namespace App\Controller\Web;

use App\Authentication\BaseAuthentication;
use App\Entity\User\ForgottenPassword;
use App\EntityManager\UserManager;
use App\Event\Notification\ForgottenPasswordEvent;
use App\Form\Model\ForgottenPasswordModel;
use App\Form\Type\ForgottenPasswordConfirmType;
use App\Form\Type\ForgottenPasswordType;
use App\Helper\DispatcherTrait;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ForgottenPasswordController
 * @package App\Controller
 */
class ForgottenPasswordController extends BaseController
{
    use DispatcherTrait;

    /**
     * @var UserManager
     */
    private UserManager $userManager;

    /**
     * @var BaseAuthentication
     */
    private BaseAuthentication $authentication;

    /**
     * ForgottenPasswordController constructor.
     * @param UserManager $userManager
     * @param BaseAuthentication $authentication
     */
    public function __construct(UserManager $userManager, BaseAuthentication $authentication)
    {
        $this->userManager = $userManager;
        $this->authentication = $authentication;
    }

    /**
     * @Route("/forgotten-password", name="web_forgotten_password")
     *
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function create(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('web_profile', ['slug' => $this->getUser()->getSlug()]);
        }

        $form = $this->generateForm(ForgottenPasswordType::class, new ForgottenPasswordModel(), $request, 'web_forgotten_password', function ($form) {

            $email = $form->getData()->getEmail();

            $user = $this->userManager->repository->findOneBy(['email' => $email, 'isDeleted' => false], [], false);

            if (!$user) {
                $this->addToast('You will receive email soon. You can use the link within 15 minutes.');

                return $this->redirectToRoute('web_forgotten_password');
            }

            /**
             * @var ForgottenPassword $token
             */
            $token = $this->userManager->createForgottenPasswordToken($email);

            $this->dispatch(new ForgottenPasswordEvent(null, $user, ['token' => $token->getToken()]));

            $this->addToast('You will receive email soon. You can use the link within 15 minutes.');

        });

        return $this->redirectOrRender($form, 'security/forgotten_password/index.html.twig');
    }

    /**
     * @Route("/forgotten-password/{email}/{token}", name="web_forgotten_password_reset")
     *
     * @param Request $request
     * @param string $email
     * @param string $token
     * @return RedirectResponse|Response
     */
    public function reset(Request $request, string $email, string $token)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('web_profile', ['slug' => $this->getUser()->getSlug()]);
        }

        $form = $this->generateForm(ForgottenPasswordConfirmType::class, new ForgottenPasswordModel(), $request, 'web_forgotten_password_reset', function ($form) use ($email, $token) {

            $user = $this->userManager->verifyForgottenPasswordToken($email, $token);

            if ($user) {
                $this->userManager->makeForgottenTokenAsUsed($token);
                $this->userManager->resetPassword($user, $form->getData()->getPassword());
            } else {
                $this->addToast('This link was used or 15 minutes timespan has elapsed! Please generate new link.');

                return $this->redirectToRoute('web_forgotten_password');
            }

            $this->authentication->authenticate($user);

            $this->addToast('Your password has been reset! ');

            return $this->redirectToRoute('web_profile', ['slug' => $this->getUser()->getSlug()]);

        }, [], compact('email', 'token'));

        return $this->redirectOrRender($form, 'security/forgotten_password/restore.html.twig');
    }
}
