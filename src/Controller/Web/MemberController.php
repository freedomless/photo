<?php

namespace App\Controller\Web;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class MemberController
 * @package App\Controller
 */
class MemberController extends BaseController
{

    /**
     * @Route("/members/{page?alphabetical}/{iso?}", name="web_members")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param UserRepository $repository
     * @param string $page
     * @return Response
     * @throws ExceptionInterface
     */
    public function index(UserRepository $repository, string $page): Response
    {

        $user = $this->getUser();

        switch ($page) {
            case 'alphabetical':
                $users = $repository->getUsersAlphabetically(1, 20);
                break;
            case 'founders':
                $users = $repository->getFounders(1, 20);
                break;
            case 'following':
                $users = $repository->getFollowingUsers($user, 1, 20);
                break;
            case 'followers':
                $users = $repository->getFollowers($user, 1, 20);
                break;
        }

        $users = $this->normalize($users, ['groups' => ['api', 'basic_user']]);

        return $this->render('member/index.html.twig', compact('users'));
    }

    /**
     * @Route("/", name="web_members_by_country")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param string|null $country
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function byCountry(?string $country = null, int $page = 1, int $limit = 20): Response
    {

        return $this->render('');
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function becomeMember(Request $request): RedirectResponse
    {

    }
}
