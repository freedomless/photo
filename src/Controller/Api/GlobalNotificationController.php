<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GlobalNotificationController
 * @package App\Controller\Api
 */
class GlobalNotificationController extends BaseController
{
    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * GlobalNotificationController constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {

        $this->session = $session;
    }

    /**
     * @Route("/api/notification/{notificationName}/hide", name="api_global_notification_hide")
     *
     * @param string $notificationName
     * @return JsonResponse
     */
    public function hideGlobalNotificationForSession(string $notificationName)
    {
        $this->session->set(
            hash('md5', $notificationName), true
        );

        return $this->success();
    }
}
