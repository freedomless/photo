<?php

namespace App\Controller\Api;

use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package App\Controller\Api
 */
class SearchController extends BaseController
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var PostRepository
     */
    private PostRepository $postRepository;

    /**
     * SearchController constructor.
     * @param UserRepository $userRepository
     * @param PostRepository $postRepository
     */
    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/api/search/{query}", name="api_search", methods={"GET"})
     *
     * @param string $query
     * @return JsonResponse
     */
    public function index(string $query): JsonResponse
    {
        $user = $this->getUser();

        $posts = $this->postRepository->search($query);
        $users = $this->userRepository->search($query, $user);

        return $this->success(compact('users', 'posts'), ['groups' => ['basic_user', 'post']]);
    }

    /**
     * @Route("/api/search/users/{query}", name="api_search_users", methods={"GET"})
     *
     * @param string $query
     *
     * @return JsonResponse
     */
    public function users(string $query): JsonResponse
    {
        $user = $this->getUser();

        $users = $this->userRepository->search($query, $user);

        return $this->success(compact('users'), ['groups' => ['basic_user']]);
    }
}
