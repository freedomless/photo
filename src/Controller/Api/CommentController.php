<?php

namespace App\Controller\Api;

use App\Entity\Photo\Comment;
use App\Entity\User\User;
use App\Event\Notification\PhotoCommentedEvent;
use App\Exception\ApiException;
use App\EntityManager\BaseManager;
use App\EntityManager\PostManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CommentController
 * @package App\Controller
 */
class CommentController extends BaseController
{
    /**
     * @var BaseManager
     */
    private BaseManager $manager;

    /**
     * CommentController constructor.
     * @param BaseManager $commentManager
     */
    public function __construct(BaseManager $commentManager)
    {
        $this->manager = $commentManager;
    }

    /**
     * @Route("/api/comments/{id}/{page?1}", name="api_comments_list", methods={"GET"})
     *
     * @param Request $request
     * @param string $slug
     * @param int $page
     *
     * @return JsonResponse
     */
    public function index(Request $request, int $id, int $page = 1): JsonResponse
    {

        $comments = $this->manager->repository->getByPost($id, $request->query->get('comment', null), $page, 10);

        return $this->success($comments, ['groups' => 'comment']);
    }

    /**
     * @Route("/api/comments", methods={"POST"})
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @param PostManager $postManager
     * @return JsonResponse
     *
     * @throws ApiException
     */
    public function create(Request $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /**
         * @var Comment $comment
         */
        $comment = $this->deserialize($request->getContent(), Comment::class);

        $comment->setUser($user);

        $comment = $this->manager->create($comment, true, ['create']);

        $post = $comment->getPost();

        if ($user->getId() !== $post->getUser()->getId()) {

            $this->dispatch(new PhotoCommentedEvent($user, $comment->getPost()->getUser(), compact('comment', 'post')));

        }

        return $this->success($comment, ['groups' => 'comment']);
    }

    /**
     * @Route("/api/comments/{id}", methods={"DELETE"})
     *
     * @Security("is_granted('ROLE_CURATOR')")
     *
     * @param Comment $comment
     *
     * @return JsonResponse
     */
    public function destroy(Comment $comment)
    {
        $this->manager->delete($comment, false);

        return $this->success();
    }
}
