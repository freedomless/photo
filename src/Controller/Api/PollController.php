<?php

namespace App\Controller\Api;

use App\Entity\Poll\Poll;
use App\Entity\Poll\PollChoice;
use App\Entity\Poll\PollVote;
use App\Form\Type\PollVoteType;
use App\EntityManager\PollManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PollApiController
 * @package App\Controller\Api
 *
 * @IsGranted("IS_AUTHENTICATED_REMEMBERED")
 */
class PollController extends BaseController
{
    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }

    /**
     * @Route("/api/poll/{id}/hide", name="api_poll_hide_for_session")
     * @param Poll $poll
     * @param PollManager $pollManager
     * @return JsonResponse
     */
    public function hidePollForSession(Poll $poll, PollManager $pollManager)
    {
        $pollManager->setPoolAsVoted($poll->getId());

        return $this->success();
    }

    /**
     * @Route("/api/poll/vote", name="api_poll_vote")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param PollManager $pollManager
     * @return RedirectResponse|Response
     */
    public function vote(Request $request, EntityManagerInterface $entityManager, PollManager $pollManager)
    {
        $form = $this->createForm(PollVoteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PollChoice $choice */
            $choice = $form->getData()['choices'];
            $user = $this->getUser();

            if ($choice) {
                $poll = $choice->getPoll();

                if ($poll->hasUser($user)) {
                    return $this->error(['html' => 'You have already voted for this poll!']);
                }

                $poll->addUser($user);

                $vote = new PollVote();
                $vote->setUser($user);
                $vote->setChoice($choice);

                $entityManager->persist($poll);
                $entityManager->persist($vote);
                $entityManager->flush();

                // success
                $resultsHtml = $pollManager->getPollResultsAsHtml($poll);

                $pollManager->setPoolAsVoted($poll->getId());

                return $this->success(['html' => $resultsHtml]);
            }
        }

        return $this->error(['html' => 'You should select an option to vote.']);
    }
}
