<?php

namespace App\Controller\Api;

use App\Dto\Request\PostFilterDto;
use App\Dto\Request\StoreFilterDto;
use App\Entity\Store\Material;
use App\Entity\Store\Product;
use App\Entity\Store\Size;
use App\Form\Type\StoreFilterType;
use App\Helper\PriceCalculator;
use App\Repository\ProductRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class StoreController extends BaseController
{
    /**
     * @Route("/api/price/{product}/{quantity}/{size}/{material}", name="api_product_price")
     *
     * @Entity("product", expr="repository.findOneBy({id: product, status: 3})")
     * @Entity("size", expr="repository.findOneBy({id: size, isEnabled: true})")
     * @Entity("material", expr="repository.findOneBy({id: material, isEnabled: true})")
     *
     * @param Product $product
     * @param int $quantity
     * @param Size $size
     * @param Material $material
     *
     * @param PriceCalculator $productService
     * @return JsonResponse
     */
    public function calculateProductPrice(Product $product,
                                          int $quantity,
                                          Size $size,
                                          Material $material,
                                          PriceCalculator $productService)
    {
        $total = $productService->calculatePrice($product, $size, $material, $quantity);

        return $this->success($total);
    }

    /**
     * @Route("/api/store/{type?single}/{page}")
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param string $type
     * @param int $page
     * @return Response
     * @throws ExceptionInterface
     */
    public function single(Request $request, ProductRepository $productRepository, string $type, int $page = 1)
    {

        /**
         * @var StoreFilterDto $filter
         */
        $filter = $this->denormalize($request->query->all(), StoreFilterDto::class);


        $series = $type === 'series';

        $posts = $productRepository->getAllFiltered($filter, $page, 20, $series);

        $groups = ['index', 'post', 'tags', 'api', 'store'];

        if ($series) {
            $groups[] = 'series';
        }

        $posts = $this->normalize($posts, compact('groups'));

        return $this->success($posts);
    }
}
