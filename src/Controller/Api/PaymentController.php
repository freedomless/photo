<?php

namespace App\Controller\Api;

use App\Entity\Store\Order;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\InvoiceManager;
use App\EntityManager\OrderManager;
use App\Event\Notification\System\NewOrderEvent;
use App\Exception\ApiException;
use App\PaymentGateway\PaymentGatewayFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PaymentController
 * @package App\Controller\Api
 * @Security("is_granted('STORE_ENABLED')")
 */
class PaymentController extends BaseController
{

    /**
     * @var PaymentGatewayFactory
     */
    private $payment;

    /**
     * @var BaseManager
     */
    private $cartManager;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * PaymentController constructor.
     * @param BaseManager $cartManager
     * @param SessionInterface $session
     * @param OrderManager $orderManager
     * @param PaymentGatewayFactory $payment
     */
    public function __construct(
        BaseManager $cartManager,
        SessionInterface $session,
        OrderManager $orderManager,
        PaymentGatewayFactory $payment
    )
    {
        $this->payment = $payment;
        $this->cartManager = $cartManager;
        $this->session = $session;
        $this->orderManager = $orderManager;
    }

    /**
     * @Route("/api/order", name="api_order_create")
     *
     * @param Request $request
     * @param string $gateway
     * @return JsonResponse
     * @throws ApiException
     */
    public function order(Request $request, string $gateway = 'paypal'): JsonResponse
    {
        $payment = $this->payment->build($gateway);

        /**
         * @var Order $order
         */
        $order = $this->deserialize($request->getContent(), Order::class);

        $cart = $this->getCart();

        $this->orderManager->copyCartItems($cart, $order);

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $redirect = $payment->create($order, $user);

        return $this->success($redirect);
    }

    /**
     * @Route("/{gateway}/answer", name="api_order_answer")
     *
     * @param SessionInterface $session
     * @param InvoiceManager $invoice
     * @param string $gateway
     * @return Response
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function answer(SessionInterface $session, InvoiceManager $invoice, string $gateway = 'paypal'): Response
    {
        $payment = $this->payment->build($gateway);
        $success = $payment->complete();

        if ($success) {

            $this->addFlash('toast', json_encode(['message' => 'Your order has been accepted']));

            if ($this->getUser() instanceof UserInterface) {

                $this->cartManager->delete($this->cartManager->repository->findOneBy(['user' => $this->getUser()], [],false), false);

            } else {

                $this->cartManager->delete($this->cartManager->repository->findOneBy(['session' => $session->getId()],[], false), false);

            }

            $this->dispatch(new NewOrderEvent(null,  null, ['invoice' => $invoice->repository->findOneBy(['token' => $success], [], false)]));

            return $this->redirectToRoute('web_invoice', ['token' => $success]);
        }

        $this->addFlash('toast', json_encode(['message' => 'Opps something when wrong']));

        return $this->redirectToRoute('');
    }

    /**
     * @Route("/{gateway}/cancel", name="api_order_cancel")
     *
     * @param string $gateway
     * @return Response
     * @throws ApiException
     */
    public function cancel(string $gateway = 'paypal'): Response
    {
        $payment = $this->payment->build($gateway);
        $payment->cancel();

        return $this->redirectToRoute('web_cart_index');
    }

    /**
     * @return mixed|null
     * @throws ApiException
     * @throws ExceptionInterface
     */
    private function getCart()
    {
        $user = $this->getUser();

        if ($user) {
            return $this->cartManager->repository->findOneBy(compact('user'));
        }
        $session = $this->session->getId();

        return $this->cartManager->repository->findOneBy(compact('session'));
    }
}
