<?php

namespace App\Controller\Api;

use App\Dto\Request\PostFilterDto;
use App\Dto\Request\StoreFilterDto;
use App\Dto\Response\ListDto;
use App\Entity\Photo\Album;
use App\Entity\Photo\Category;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\PostManager;
use App\EntityManager\UserManager;
use App\Repository\FollowRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping\Driver\DatabaseDriver;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PostController
 * @package App\Controller\Api
 */
class PostController extends BaseController
{
    /**
     * @var PostRepository
     */
    private PostRepository $repository;

    /**
     * PostController constructor.
     * @param PostRepository $repository
     */
    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/api/posts/follow-favorite",name="api_photos_follow_favorite")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowFavorite(Request $request)
    {
        $favorite = [];
        $follow = [];

        $posts = json_decode($request->getContent() ?: '{}', true);

        if ($this->getUser()) {
            $favorite = array_column($this->repository->getFavoritePostsByUser($this->getUser(), $posts), 'id', null);
            $favorite = array_map('intval', $favorite);
            $follow = array_column($this->repository->getFollowedByPosts($this->getUser(), $posts), 'id', null);
            $follow = array_map('intval', $follow);
        }

        return $this->success(compact('favorite', 'follow'));
    }

    /**
     * @Route("/api/photos/{id}", name="api_photos_show")
     *
     * @param PostManager $manager
     * @param BaseManager $favoriteManager
     * @param FollowRepository $followRepository
     * @param RouterInterface $router
     * @param int $id
     * @return JsonResponse
     */
    public function show(PostManager $manager,
                         BaseManager $favoriteManager,
                         FollowRepository $followRepository,
                         RouterInterface $router,
                         int $id): JsonResponse
    {
        $this->denyAccessUnlessGranted('ALLOW_ENTER');

        $post = $this->repository->findOneBy(['id' => $id, 'isDeleted' => false]);

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $manager->addView($post);

        $favorite = $follow = false;

        if ($user) {
            $favorite = $favoriteManager->repository->findOneBy(compact('post', 'user')) !== null;
            $follow = $followRepository->findOneBy(['follower' => $post->getUser(), 'followed' => $user], [], false) !== null;
        }

        $curate = null;

        if ($this->isGranted('ROLE_CURATOR')) {
            $curate = $router->generate('admin_photo_show', ['id' => $post->getId()]);
        }

        return $this->success(compact('post', 'favorite', 'curate', 'follow'), ['groups' => 'post']);
    }

    /**
     * @Route("/api/photo/{id}/add-view", name="api_add_view")
     * @param PostManager $manager
     * @param int $id
     * @return JsonResponse
     */
    public function addView(PostManager $manager, int $id)
    {
        $manager->addView($this->repository->find($id));

        return $this->success();
    }

    /**
     * @Route("/api/photos/published/{page?1}/{user?}/{series?0}", name="api_photos_published")
     *
     * @param UserManager $userManager
     * @param int $page
     * @param int|null $user
     *
     * @param int $series
     * @return JsonResponse
     *
     * @throws ExceptionInterface
     */
    public function published(UserManager $userManager, int $page = 1, ?int $user = null, bool $series = false): JsonResponse
    {

        if ($series === false) {

            $posts = $this->repository->getPublished($user, $page, 20);

            $user = $userManager->repository->find($user);

            $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);
            $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());
        } else {
            $posts = $this->repository->getPortfolio($user, $page, 20, $series, true);
            $posts = $this->normalize($posts, ['groups' => ['post', 'api', 'series']]);
        }

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/portfolio/{page?1}/{user}/{series?0}", name="api_photos_portfolio")
     *
     * @param UserManager $userManager
     * @param int $page
     * @param int $limit
     * @param int|null $user
     * @param bool $series
     * @return JsonResponse
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     * @throws MappingException
     */
    public function portfolio(UserManager $userManager, int $page = 1, int $limit = 20, int $user = null, bool $series = false): JsonResponse
    {

        $posts = $this->repository->getPortfolio($user, $page, $limit, $series, false);

        $groups = ['groups' => ['post', 'api']];

        if ($series) {
            $groups['groups'][] = 'series';
        }

        $posts = $this->normalize($posts, ['groups' => $groups]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);

    }

    /**
     * @Route("/api/photos/latest/{page?1}/{limit?20}", name="api_photos_latest")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function latest(Request $request, int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var PostFilterDto $filters
         */
        $filters = count(array_filter($request->query->all())) > 0 ? $this->denormalize($request->query->all(), PostFilterDto::class) : null;

        $posts = $this->repository->getLatest($page, $limit, $filters);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/popular/{page?1}/{limit?20}", name="api_photos_popular")
     *
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function popular(int $page = 1, int $limit = 20): JsonResponse
    {
        $posts = $this->repository->getPopular($page, $limit);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/following/{page?1}/{limit?20}", name="api_photos_following")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function following(int $page = 1, int $limit = 20): JsonResponse
    {
        $posts = $this->repository->getFromFollowingUsers($this->getUser(), $page, $limit);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts, ['groups' => ['post', 'api']]);
    }

    /**
     * @Route("/api/photos/category/{id}/{page?1}/{limit?20}", name="api_photos_by_category")
     *
     * @param Category $category
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     *
     * @throws ExceptionInterface
     */
    public function category(Category $category, int $page = 1, int $limit = 20): JsonResponse
    {
        $posts = $this->repository->getByCategory($category, $page, $limit);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/favorites/{page?1}/{limit?20}", name="api_photos_favorites")
     *
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function favorite(int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $posts = $this->repository->getFavorites($user, $page, $limit);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);

    }

    /**
     * @Route("/api/photos/tag/{tag}/{page?1}/{limit?20}", name="api_photos_by_tag")
     *
     * @param string $tag
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     *
     * @throws ExceptionInterface
     */
    public function tag(string $tag, int $page = 1, int $limit = 20): JsonResponse
    {
        $posts = $this->repository->getByTag($tag, $page, $limit);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/awarded/{page?1}/{limit?20}")
     *
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function awarded(Request $request, int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var PostFilterDto $filters
         */
        $filters = count(array_filter($request->query->all())) > 0 ? $this->denormalize($request->query->all(), PostFilterDto::class) : null;

        $posts = $this->repository->getAwarded($page, $limit, $filters);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);

    }

    /**
     * @Route("/api/photos/album/{id}/{page?1}/{limit?20}", name="api_photos_by_album")
     *
     * @param Album $album
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     */
    public function album(Album $album, int $page = 1, int $limit = 20): JsonResponse
    {
        return $this->success();

    }

    /**
     * @Route("/api/photos/series/{page?1}/{limit?20}", name="api_photos_series_all")
     *
     * @param Request $request
     * @param int $page
     *
     * @return JsonResponse
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function series(Request $request, int $page = 1)
    {
        /**
         * @var PostFilterDto $filters
         */
        $filters = count(array_filter($request->query->all())) > 0 ? $this->denormalize($request->query->all(), PostFilterDto::class) : null;

        $series = $this->repository->getLatest($page, 20, $filters, true);

        return $this->success($series, ['groups' => ['post', 'series']]);
    }

    /**
     * @Route("/api/series/{id}", name="api_series_photos")
     * @param int $id
     * @return JsonResponse
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     */
    public function seriesPhotos(int $id)
    {
        $post = $this->repository->createQueryBuilder('p')
            ->where('p.id = :id or p.oldId = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $posts = $post->getParent()->getChildren();

        $posts = new ListDto($posts, count($posts), 1, false);

        $posts = $this->normalize($posts, ['groups' => ['post', 'api']]);

        $posts = $this->repository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @Route("/api/photos/{id}/info",name="api_photos_info")
     *
     * @param int $id
     * @return JsonResponse
     */
    public function info(int $id): JsonResponse
    {
        $post = $this->repository->find($id);

        return $this->success($post, ['groups' => ['post', 'tags', 'favorites']]);
    }

    /**
     * @Route("/api/photos/store/{page}")
     *
     * @param Request $request
     * @param PostRepository $postRepository
     * @param ProductRepository $productRepository
     * @param string $type
     * @param int $page
     * @param int $limit
     * @return Response
     * @throws ExceptionInterface
     */
    public function store(Request $request,
                          PostRepository $postRepository,
                          ProductRepository $productRepository,
                          string $type = 'single', int $page = 1, int $limit = 20)
    {

        /**
         * @var StoreFilterDto $filter
         */
        $filter = $this->denormalize($request->query->all(), StoreFilterDto::class);

        $posts = $productRepository->getAllFiltered($filter, $page, $limit, false);

        $groups = ['index', 'post', 'tags', 'api', 'store', 'product'];

        $posts = $this->normalize($posts, compact('groups'));

        $posts = $postRepository->getFavoritesAndFollows($posts, $this->getUser());

        return $this->success($posts);
    }

    /**
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return string
     */
    private function group(Request $request, int &$page, int &$limit): string
    {
        $group = $request->query->get('slider') ? 'post_id' : 'basic_post';

        if ($group === 'id') {
            $page = 0;
            $limit = 0;
        }

        return $group;
    }
}
