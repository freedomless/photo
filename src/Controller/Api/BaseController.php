<?php

namespace App\Controller\Api;

use App\Dto\Response\ListDto;
use App\Helper\DispatcherTrait;
use App\Helper\SerializationTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Json;

/**
 * Class BaseController
 * @package App\Controller\Api
 */
abstract class BaseController extends AbstractController
{
    use SerializationTrait,
        DispatcherTrait;

    /**
     * @param $data
     * @param array $context
     * @param array $headers
     * @return JsonResponse
     */
    protected function success($data = null, array $context = [], array $headers = []): JsonResponse
    {
        $response = ['code' => 0];

        if ($data) {
            $response['data'] = $data;
        }

        if (isset($context['groups']) && is_string($context['groups'])) {
            $context['groups'] = [$context['groups']];
        }

        if ($data instanceof ListDto) {
            $context['groups'][] = 'api';
        }

        return new JsonResponse($this->serialize($response, $context), 200, $headers, true);
    }

    /**
     * @param string|null $massage
     * @return JsonResponse
     */
    protected function error($massage = null): JsonResponse
    {
        $response = [
            'message' => $massage,
            'code' => -1
        ];

        return new JsonResponse($response);
    }

}
