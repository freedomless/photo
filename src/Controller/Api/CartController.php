<?php

namespace App\Controller\Api;

use App\Entity\Store\Cart;
use App\Entity\Store\CartItem;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\Exception\ApiException;
use App\Helper\PriceCalculator;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class CartController
 * @package App\Controller\Api
 */
class CartController extends BaseController
{

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var BaseManager
     */
    private BaseManager $cartItemManager;

    /**
     * @var BaseManager
     */
    private BaseManager $cartManager;

    /**
     * @var PriceCalculator
     */
    private PriceCalculator $calculator;

    /**
     * CartController constructor.
     * @param SessionInterface $session
     * @param PriceCalculator $calculator
     * @param BaseManager $cartManager
     * @param BaseManager $cartItemManager
     */
    public function __construct(SessionInterface $session,
                                PriceCalculator $calculator,
                                BaseManager $cartManager,
                                BaseManager $cartItemManager)
    {
        $this->session = $session;
        $this->cartItemManager = $cartItemManager;
        $this->cartManager = $cartManager;
        $this->calculator = $calculator;


        $session->set('cart','test');
    }

    /**
     * @Route("/api/cart", name="api_cart_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ApiException
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function create(Request $request): JsonResponse
    {

        /**
         * @var $cartItem CartItem
         */
        $cartItem = $this->deserialize($request->getContent(), CartItem::class, ['groups' => 'create']);

        $deletionDate = $cartItem->getProduct()->getPost()->getDeletionDate();

        if ($deletionDate !== null && $deletionDate < new \DateTime()) {
            return $this->error('Product is unavalable');
        }

        /**
         * @var User $user
         */
        $user = $this->getUser();
        $session = $this->session->getId();

        if ($user !== null) {

            $cart = $this->cartManager->repository->findOneBy(compact('user'), [], false);
        } else {
            $cart = $this->cartManager->repository->findOneBy(compact('session'), [], false);
        }

        if ($cart === null) {
            $entity = new Cart();
            $user === null ? $entity->setSession($session) : $entity->setUser($user);
            $cart = $this->cartManager->create($entity);
        }

        $cartItem->setCart($cart);

        $this->cartItemManager->create($cartItem);

        return $this->success();

    }

    /**
     * @Route("/api/cart/{id}", name="api_cart_update", methods={"PUT"})
     *
     * @param Request $request
     *
     * @param int $id
     *
     * @return JsonResponse
     *
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $user = $this->getUser();
        $session = $this->session->getId();

        /**
         * @var Cart $cart
         */
        $cart = $user !== null ? $this->cartManager->repository->findOneBy(compact('user')) :
            $this->cartManager->repository->findOneBy(compact('session'));

        /**
         * @var CartItem $item
         */
        $item = $this->cartItemManager->repository->findOneBy(compact('id', 'cart'));

        if ($item === null) {
            return $this->error('Item not found');
        }

        $entity = $this->cartItemManager->update($request->getContent(), $item, true, ['cart']);

        $price = $this->calculator->calculatePrice($item->getProduct(), $item->getSize(), $item->getMaterial(), $item->getQuantity());

        $item->setTotal($price['total']);
        $item->setDiscount($price['discount']);

        return $this->success($entity, ['groups' => ['cart', 'post', 'index']]);

    }

    /**
     * @Route("/api/cart/{id}", name="api_cart_delete", methods={"DELETE"})
     *
     * @param int $id
     * @return JsonResponse
     * @throws ApiException
     */
    public function destroy(int $id): JsonResponse
    {

        $user = $this->getUser();
        $session = $this->session->getId();

        /**
         * @var CartItem $item
         */
        $item = $this->cartItemManager->repository->findOneBy(compact('id'));

        if ($item && ($item->getCart()->getUser()->getId() === $user->getId() || $item->getCart()->getSession() === $session)) {
            $this->cartItemManager->delete($item, false);
        }

        return $this->success();
    }

}
