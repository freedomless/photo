<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-02-24
 * Time: 12:42
 */

namespace App\Controller\Api;

use App\EntityManager\UserManager;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MemberController
 * @package App\Controller\Api
 *
 * @Security("is_granted('ROLE_USER')")
 */
class MemberController extends BaseController
{

    /**
     * @var UserRepository
     */
    private UserRepository $repository;

    /**
     * MemberController constructor.
     * @param UserManager $manager
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/members/top/{page?1}",name="api_members")
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function top(int $page = 1, int $limit = 20): JsonResponse
    {
        $users = $this->repository->getTopRankUsers($page, $limit);

        return $this->success($users, ['groups' => 'basic_user']);
    }

    /**
     * @Route("/api/members/founders/{page?1}", name="api_founders")
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function founders(int $page = 1, int $limit = 20): JsonResponse
    {
        $founders = $this->repository->getFounders($page, $limit);

        return $this->success($founders, ['groups' => 'basic_user']);
    }

    /**
     * @Route("/api/members/following/{page?1}", name="api_members_following")
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function following(int $page = 1, int $limit = 20): JsonResponse
    {
        $user = $this->getUser();
        $users = $this->repository->getFollowingUsers($user, $page, $limit);

        return $this->success($users, ['groups' => 'basic_user']);
    }

    /**
     * @Route("/api/members/followers/{page?1}", name="api_members_followed")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function followers(int $page = 1, int $limit = 20): JsonResponse
    {
        $user = $this->getUser();
        $users = $this->repository->getFollowers($user, $page, $limit);

        return $this->success($users, ['groups' => 'basic_user']);
    }

    /**
     * @Route("/api/members/{page?1}", name="api_members_alphabetical")
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function alphabetical(int $page = 1, int $limit = 20): JsonResponse
    {

        $users = $this->repository->getUsersAlphabetically($page, $limit);

        return $this->success($users, ['groups' => 'basic_user']);
    }

}
