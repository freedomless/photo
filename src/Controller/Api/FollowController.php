<?php

namespace App\Controller\Api;

use App\Entity\User\Follow;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\Event\Notification\UserFollowedEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FollowController
 * @package App\Controller\Api
 *
 * @Security("is_granted('ROLE_USER')")
 */
class FollowController extends BaseController
{
    use DispatcherTrait;

    /**
     * @var BaseManager
     */
    private BaseManager $followManager;

    /**
     * FollowController constructor.
     * @param BaseManager $followManager
     */
    public function __construct(BaseManager $followManager)
    {
        $this->followManager = $followManager;
    }

    /**
     * @Route("/api/follow/{id}", methods={"POST","GET"})
     * @Security("is_granted('VERIFIED')")
     * @param User $followed
     * @return JsonResponse
     * @throws ApiException
     */
    public function create(User $followed)
    {
        $follower = $this->getUser();

        $follow = new Follow();
        $follow->setFollowed($followed);
        $follow->setFollower($follower);

        try {
            $this->followManager->create($follow, false);

            $this->dispatch(new UserFollowedEvent($follower, $followed));

            return $this->success();

        } catch (UniqueConstraintViolationException $e) {
            return $this->error('Already followed!');
        }

    }

    /**
     * @Route("/api/follow/{id}", methods={"DELETE"})
     * @Security("is_granted('VERIFIED')")
     * @param User $followed
     * @return JsonResponse
     */
    public function delete(User $followed)
    {
        $follower = $this->getUser();

        $follow = $this->followManager->repository->findOneBy(compact('follower', 'followed'));

        if ($follow) {
            $this->followManager->delete($follow, false);
        }

        return $this->success();
    }
}
