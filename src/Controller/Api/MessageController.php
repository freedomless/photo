<?php

namespace App\Controller\Api;

use App\Entity\Messenger\Message;
use App\Entity\User\User;
use App\EntityManager\MessageManager;
use App\Event\Notification\NewMessageEvent;
use App\Exception\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MessageController
 *
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class MessageController extends BaseController
{

    /**
     * @var MessageManager
     */
    private MessageManager $manager;

    /**
     * MessageController constructor.
     * @param MessageManager $manager
     */
    public function __construct(MessageManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/api/messages/{page?1}/{limit?20}", name="api_messages_index", methods={"GET"})
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function index(int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $messages = $this->manager->repository->getMainMessages($user, $page, $limit);

        return $this->success($messages, ['groups' => 'message']);
    }

    /**
     * @Route("/api/message/{id}/{page?1}", name="api_messages_show", methods={"GET"})
     *
     * @param int $id
     * @param int $page
     * @param int $limit
     *
     * @return JsonResponse
     */
    public function show(int $id, int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $messages = $this->manager->repository->getMessage($user, $id, $page, $limit);

        return $this->success($messages, ['groups' => 'message']);
    }

    /**
     * @Route("/api/messages/{id?}", name="api_messages_create", methods={"POST"})
     *
     * @param Request $request
     * @param int|null $id
     *
     * @return JsonResponse
     *
     * @throws ApiException
     */
    public function create(Request $request, ?int $id = null): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /**
         * @var Message $entity
         */
        $entity = $this->deserialize($request->getContent(), Message::class);

        $entity->setSender($user);

        if ($user->getId() === $entity->getReceiver()) {
            return $this->error('Wrong user');
        }

        // If user or receiver is deleted or blocked
        if (
            $user->getIsDeleted() ||
            $user->getIsBlocked() ||
            $entity->getReceiver()->getIsDeleted() ||
            $entity->getReceiver()->getIsBlocked()
        ) {
            return $this->error('You have no permissions to use the message system');
        }

        $bannedWords = [
            '100asa',
        ];

        // If bad word is in the title or the text
        foreach ($bannedWords as $bannedWord) {
            if (
                preg_match('/' . strtolower($bannedWord) . '/i', strtolower($entity->getSubject()))
                || preg_match('/' . strtolower($bannedWord) . '/i', strtolower($entity->getText()))
            ) {
                return $this->error('You have no permissions to use the message system');
            }
        }

        if ($id !== null) {
            /**
             * @var $parent Message
             */
            $parent = $this->manager->repository->getMessage($user, $id, 0, 0);

            if ($parent === null) {
                throw new ApiException(-1, ['message' => 'Message does not exits!']);
            }

            $parent->setIsSeen(false);

            $entity->setParent($parent);
        }

        if ($entity->getReceiver() === null || $entity->getReceiver() === $user->getId()) {
            throw new ApiException(4000);
        }

        $message = $this->manager->create($entity);

        if (
            $entity->getParent() === null
            || ($entity->getParent()->getIsDeletedForReceiver() == null && $entity->getParent()->getIsDeletedForSender(
                ) == null)
        ) {
            $this->dispatch(new NewMessageEvent($user, $entity->getReceiver(), ['conversation' => $message->getId()]));
        }

        return $this->success($message, ['groups' => 'message']);
    }

    /**
     * @Route("/api/messages/{id}", name="api_messages_delete", methods={"DELETE"})
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /** @var Message $message */
        $message = $this->manager->repository->findOneBy(['sender' => $user->getId(), 'id' => $id], null, false);
        if ($message) {
            $this->manager->deleteForSender($message);

            return $this->success();
        }

        $message = $this->manager->repository->findOneBy(['receiver' => $user->getId(), 'id' => $id], null, false);
        if ($message) {
            $this->manager->deleteForReceiver($message);

            return $this->success(['message' => 'Deleted']);
        }

        return $this->error(['message' => 'Not found']);
    }
}
