<?php

namespace App\Controller\Api;

use App\Entity\User\User;
use App\EntityManager\UserManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AccountController
 * @package App\Controller\Api
 */
class AccountController extends BaseController
{
    /**
     * @var UserManager
     */
    private UserManager $manager;

    /**
     * AccountController constructor.
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/api/avatar", name="api_update_avatar")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function avatar(Request $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /**
         * @var UploadedFile $avatar
         */
        $avatar = $request->files->get('avatar');

        if ($avatar->getMimeType() !== 'image/jpeg') {
            return $this->error('Invalid file type!');
        }

        $this->manager->updateAvatar($user, $avatar);

        return $this->success();
    }

    /**
     * @Route("/api/cover", name="api_update_cover")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function cover(Request $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /**
         * @var UploadedFile $cover
         */
        $cover = $request->files->get('cover');

        if ($cover->getMimeType() !== 'image/png') {
            return $this->error('Invalid file type!');
        }

        $this->manager->updateCover($user, $cover);

        return $this->success();
    }

    /**
     * @param int|null $id
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function followers(?int $id = null, int $page = 1, int $limit = 20): JsonResponse
    {

    }

    /**
     * @Route("/api/accept-cookies", name="api_accept_cookies")
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function acceptCookies(): JsonResponse
    {
        $user = $this->getUser();
        $response = $this->success();

        if ($user instanceof UserInterface) {

            $user->setAcceptPolicies(new \DateTime());
            $response->headers->setCookie(Cookie::create('cookies', true, time() + 3000000000));

            $this->manager->flush($user);

            return $response;
        }

        $response->headers->setCookie(Cookie::create('cookies', true, time() + 3000000000));

        return $response;

    }

}
