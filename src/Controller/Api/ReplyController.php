<?php

namespace App\Controller\Api;

use App\Entity\Photo\Reply;
use App\Event\Notification\CommentRepliedEvent;
use App\Exception\ApiException;
use App\EntityManager\BaseManager;
use App\Helper\DispatcherTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReplyController
 * @package App\Controller\Api
 *
 */
class ReplyController extends BaseController
{
    use DispatcherTrait;

    /**
     * @var BaseManager
     */
    private BaseManager $manager;

    /**
     * ReplyController constructor.
     * @param BaseManager $replyManager
     */
    public function __construct(BaseManager $replyManager)
    {
        $this->manager = $replyManager;
    }

    /**
     * @Route("/api/replies", name="api_replies_create", methods={"POST"})
     *
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ApiException
     */
    public function create(Request $request): JsonResponse
    {
        $reply = $this->deserialize($request->getContent(), Reply::class);

        $reply->setUser($this->getUser());

        /**
         * @var Reply $reply
         */
        $reply = $this->manager->create($reply);

        $comment = $reply->getComment();

        if ($comment->getUser()->getId() !== $reply->getUser()->getId()) {
            $this->dispatch(new CommentRepliedEvent($reply->getUser(), $comment->getUser(), ['post' => $comment->getPost(), 'comment' => $comment]));
        }

        return $this->success($reply, ['groups' => 'comment']);
    }

    /**
     * @Route("/api/replies/{id}", name="api_replies_delete", methods={"DELETE"})
     *
     * @Security("is_granted('ROLE_CURATOR')")
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {

        $this->manager->delete($id, false);

        return $this->success();
    }
}
