<?php

namespace App\Controller\Api;

use App\Entity\Photo\Favorite;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\PostManager;
use App\Event\Notification\PhotoLikedEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FavoriteController
 * @package App\Controller
 *
 * @Security("is_granted('ROLE_USER') and is_granted('VERIFIED')",statusCode=401,message="Unauthorized")
 */
class FavoriteController extends BaseController
{
    use DispatcherTrait;

    /**
     * @var PostManager
     */
    private PostManager $postManager;

    /**
     * @var BaseManager
     */
    private BaseManager $favoriteManager;

    /**
     * FavoriteController constructor.
     * @param PostManager $postManager
     * @param BaseManager $favoriteManager
     */
    public function __construct(PostManager $postManager, BaseManager $favoriteManager)
    {
        $this->postManager = $postManager;
        $this->favoriteManager = $favoriteManager;
    }

    /**
     * @Route("/api/favorites/{id}", name="api_favorties_create", methods={"POST","GET"})
     *
     * @param int $id
     *
     * @return JsonResponse
     *
     * @throws ApiException
     */
    public function create(int $id): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        /**
         * @var Post|null $post
         */
        $post = $this->postManager->repository->getNotUserPost($user, $id);

        if ($post === null) {
            throw  $this->createNotFoundException();
        }

        $favorite = new Favorite();
        $favorite->setPost($post);
        $favorite->setUser($user);

        try {

            $this->favoriteManager->create($favorite);

            $this->dispatch(new PhotoLikedEvent($user, $post->getUser(), compact('post')));

            return $this->success();

        } catch (UniqueConstraintViolationException $e) {
            return $this->error('Photo already liked');
        }

    }

    /**
     * @Route("/api/favorites/{id}", name="api_favroties_delete", methods={"DELETE"})
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {

        $like = $this->favoriteManager->repository->findOneBy(['post' => $id, 'user' => $this->getUser()]);

        if ($like) {
            $this->favoriteManager->delete($like, false);
        }

        return $this->success();
    }
}
