<?php

namespace App\Controller\Api;

use App\Entity\User\User;
use App\EntityManager\NotificationManager;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NotificationController
 * @package App\Controller\Api
 *
 * @Security("is_granted('ROLE_USER')")
 */
class NotificationController extends BaseController
{

    /**
     * @var NotificationManager
     */
    private NotificationManager $manager;

    /**
     * NotificationController constructor.
     * @param NotificationManager $manager
     */
    public function __construct(NotificationManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/api/notifications/{page?1}", name="api_notifications_index", methods={"GET"})
     *
     * @param int $page
     * @param int $limit
     * @return JsonResponse
     */
    public function index(int $page = 1, int $limit = 20): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $notifications = $this->manager->repository->getWebNotifications($user, $page, $limit);

        $this->manager->markAsSeen($notifications->getItems());

        return $this->success($notifications, ['groups' => 'notifications']);
    }

    /**
     * @Route("/api/notifiations/total", name="api_notifications_total")
     *
     * @return mixed
     */
    public function getCount()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        return $this->success($this->manager->repository->getUnseenCount($user));

    }

    /**
     * @Route("/api/notifiations/makr-as-seen", name="api_notifications_seen", methods={"POST"})
     * @return JsonResponse
     */
    public function markAllAsSeen(): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $this->manager->markAllAsSeen($user);

        return $this->success();
    }

    /**
     * @Route("/api/notifications/{id}", name="api_notifications_delete", methods={"DELETE"})
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $receiver = $this->getUser()->getId();
        $notification = $this->manager->repository->findOneBy(array_merge(compact('id', 'receiver'), ['isDeleted' => false]));

        if ($notification === null) {
            return $this->success();
        }

        $this->manager->delete($notification->getId(), false);

        return $this->success();
    }

}
