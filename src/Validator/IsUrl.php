<?php

namespace App\Validator;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraint;

/**
 * Class IsUrl
 * @package App\Validator\Constraits
 * @Annotation
 * @ORM\Entity
 * @ORM\Table(name="is_url")
 */
class IsUrl extends Constraint
{
    public $message = 'This value "{{ string }}" have to be valid url';

    public function validatedBy()
    {
        return \get_class($this) . 'Validator';
    }
}
