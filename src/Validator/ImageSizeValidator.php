<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ImageSizeValidator
 * @package App\Validator
 */
class ImageSizeValidator extends ConstraintValidator
{
    /**
     * @param UploadedFile $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\ImageSize */

        if (null === $value || '' === $value) {
            return;
        }

        $info = getimagesize($value->getPathname());

        [$width, $height] = $info;

        $actual = $width > $height ? $height : $width;

        if ($actual >= $constraint->min) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $constraint->min)
            ->addViolation();
    }
}
