<?php

namespace App\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EntityNormalizer extends ObjectNormalizer
{
    /**
     * Entity manager
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;

    protected static bool $toBeDenormalized = false;

    /**
     * Entity normalizer
     * @param EntityManagerInterface $em
     * @param ClassMetadataFactoryInterface|null $classMetadataFactory
     * @param NameConverterInterface|null $nameConverter
     * @param PropertyAccessorInterface|null $propertyAccessor
     * @param PropertyTypeExtractorInterface|null $propertyTypeExtractor
     */
    public function __construct(
        EntityManagerInterface $em,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null
    )
    {
        parent::__construct($classMetadataFactory, $nameConverter, $propertyAccessor, $propertyTypeExtractor);
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type,string $format = null)
    {

        return strpos($type, 'App\\Entity\\') === 0 &&
            (is_numeric($data) || is_string($data) || (is_array($data) && isset($data['id']))) &&
            static::$toBeDenormalized === false;
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, $class,string $format = null, array $context = [])
    {

        $data = is_array($data) ? array_filter($data) : $data;

        $entity = $this->em->find($class, is_array($data) ? $data['id'] : $data);

        if (is_array($data)) {
            static::$toBeDenormalized = true;
            $entity = $this->serializer->deserialize(json_encode($data), $class, 'json', ['object_to_populate' => $entity]);
            static::$toBeDenormalized = false;
        }

        return $entity;
    }

}
