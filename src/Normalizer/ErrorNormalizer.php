<?php

namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ErrorNormalizer
 * @package App\Normalizer
 */
class ErrorNormalizer extends ObjectNormalizer
{

    /**
     * @param $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data,string $format = null): bool
    {

        return $data instanceof ConstraintViolationList && ($format === 'json' || $format === null);
    }

    /**
     * @param ConstraintViolationList $object
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|mixed|string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object,string $format = null, array $context = [])
    {
        $errors = [];

        /**
         * @var ConstraintViolation $error
         */
        foreach ($object as $error) {
            $errors[$this->toUnderScore($error->getPropertyPath())] = $error->getMessage();
        }

        return $errors;
    }

    /**
     * @param $input
     * @return string
     */
    private function toUnderScore($input): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match === strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }
}
