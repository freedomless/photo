<?php

namespace App\Event\Point;

/**
 * Class CommentPointEvent
 * @package App\Event\Point
 */
class CommentPointEvent extends PointEvent
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'comment';
    }
}
