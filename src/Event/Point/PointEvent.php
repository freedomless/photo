<?php

namespace App\Event\Point;

use App\Entity\User\User;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class PointEvent
 * @package App\Event\Point
 */
abstract class PointEvent extends Event
{

    /**
     * @var User
     */
    protected User   $receiver;

    /**
     * PointEvent constructor.
     * @param User $receiver
     */
    public function __construct(User $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return string
     */
    public abstract function getType(): string;

    /**
     * @return User
     */
    public function getReceiver(): User
    {
        return $this->receiver;
    }
}
