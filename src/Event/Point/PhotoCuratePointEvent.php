<?php

namespace App\Event\Point;

/**
 * Class PhotoCuratePointEvent
 * @package App\Event\Point
 */
class PhotoCuratePointEvent extends PointEvent
{

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'curate';
    }
}
