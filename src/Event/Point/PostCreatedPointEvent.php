<?php

namespace App\Event\Point;

/**
 * Class PostCreatedPointEvent
 * @package App\Event\Point
 */
class PostCreatedPointEvent extends PointEvent
{

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'post';
    }
}
