<?php

namespace App\Event\Notification;

/**
 * Class CommentRepliedEvent
 * @package App\Event\Notification
 */
class CommentRepliedEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        $post = $this->payload['post'];
        $comment = $this->payload['comment'];

        return [
            'sender' => $this->generateBasicUser($this->sender),
            'post'   => [
                'url'       => $this->router->generate('web_photo_show',
                    [
                        'id'      => $post->getId(),
                        'comment' => $comment->getId(),
                        'page'    => 'portfolio',
                        'payload' => $this->receiver->getId()
                    ]),
                'photo_url' => $post->getImage()->getThumbnail()
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'new_reply';
    }

}
