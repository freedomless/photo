<?php

namespace App\Event\Notification;

class PayoutRequestedEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        return [
            'balance' => $this->payload['balance'],
            'name'    => $this->receiver->getProfile()->getFullName()
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
            return 'payout-request';
    }

}
