<?php

namespace App\Event\Notification\System;

use App\Entity\Store\Invoice;
use App\Event\Notification\NotificationEvent;

class NewOrderEvent extends NotificationEvent
{

    public function __construct($sender = null, $receiver = null, array $payload = []) {
        parent::__construct($sender, $receiver, $payload);
        $this->email = $this->payload['invoice']->getOrder()->getBillingAddress()->getEmail();

    }

    public function getPayload(): array
    {
        /**
         * @var Invoice $invoice
         */
        $invoice = $this->payload['invoice'];

        $details = $invoice->getOrder()->getBillingAddress();

        return [
            'name' => $details->getName()
        ];

    }


    public function getType(): string
    {
        return 'invoice';
    }
}
