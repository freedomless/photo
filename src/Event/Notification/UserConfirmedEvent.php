<?php

namespace App\Event\Notification;

use App\Entity\User\User;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class UserConfirmedEvent extends NotificationEvent
 * @package App\Event\Notification
 */
class UserConfirmedEvent extends NotificationEvent
{
    /**
     * @return array
     */
    public function getPayload(): array
    {
        return [
            'receiver' => $this->generateBasicUser($this->receiver),
            'social'   => isset($this->payload['social'])
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'welcome';
    }
}
