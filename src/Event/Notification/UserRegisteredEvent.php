<?php

namespace App\Event\Notification;


/**
 * Class UserRegisteredEvent extends NotificationEvent
 * @package App\Event\Notification
 */
class UserRegisteredEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {

        return [
            'receiver' => $this->generateBasicUser($this->receiver),
            'token'    => $this->payload['verification']->getToken()
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'confirmation';
    }
}
