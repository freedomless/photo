<?php

namespace App\Event\Notification;

/**
 * Class InvoiceCreatedEvent
 * @package App\Event\Notification
 */
class InvoiceCreatedEvent extends NotificationEvent
{

    /**
     * @return array|string[]
     */
    public function getPayload(): array
    {
        return [
            'post' => ''
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'invoice';
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->payload['email'];
    }

}
