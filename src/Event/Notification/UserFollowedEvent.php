<?php

namespace App\Event\Notification;

/**
 * Class UserFollowedEvent
 * @package App\Event\Notification
 */
class UserFollowedEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return ['sender' => $this->generateBasicUser($this->sender)];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'followed';
    }
}
