<?php

namespace App\Event\Notification;

/**
 * Class CurateLimitActiveEvent
 * @package App\Event\Notification
 */
class CurateLimitActiveEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return [
            'receiver' => $this->generateBasicUser($this->receiver),
            'left'     => $this->payload['left'],
        ];

    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'curate_limit_active';
    }

}
