<?php

namespace App\Event\Notification;

use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SeriesSelectedEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        /** @var Post $photo */
        $photo = $this->payload['post'];

        /** @var SelectionSeries $selectionSeries */
        $selectionSeries = $this->payload['selection'];

        return [
            'receiver'   => $this->generateBasicUser($this->receiver),
            'senderName' => 'PiART Curators',
            'post'       => [
                'photo_url' => $photo->getImage()->getThumbnail(),
                'url'       => $this->router->generate(
                    'series_selection_history',
                    [
                        'id' => $selectionSeries->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'series'    => $selectionSeries->getTitle() ?? ""
            ],
        ];
    }

    public function getType(): string
    {
        return 'selection_series_published';
    }
}
