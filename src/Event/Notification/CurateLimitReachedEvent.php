<?php

namespace App\Event\Notification;

/**
 * Class CurateLimitReachedEvent
 * @package App\Event\Notification
 */
class CurateLimitReachedEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return [
            'receiver' => $this->generateBasicUser($this->receiver),
            'left'     => $this->payload['left'],
        ];

    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'curate_limit_reached';
    }

}
