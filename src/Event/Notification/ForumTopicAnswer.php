<?php

namespace App\Event\Notification;

class ForumTopicAnswer extends NotificationEvent
{
    public function getPayload(): array
    {

    }

    public function getType(): string
    {
        return 'forum-topic-answer';
    }
}
