<?php

namespace App\Event\Notification;

use App\Event\Point\CommentPointEvent;

class PhotoCommentedEvent extends NotificationEvent
{

    public function __construct($sender = null, $receiver = null, array $payload = []) {
        parent::__construct($sender, $receiver, $payload);
        $this->setNext(new CommentPointEvent($receiver));
    }

    public function getPayload(): array
    {
        $post = $this->payload['post'];
        $comment = $this->payload['comment'];

        return [
            'sender' => $this->generateBasicUser($this->sender),
            'post'   => [
                'url'       => $this->router->generate('web_photo_show',
                    [
                        'comment' => $comment->getId(),
                        'id'      => $post->getId(),
                        'page'    => 'portfolio',
                        'payload' => $this->receiver->getId()]),
                'photo_url' => $post->getImage()->getThumbnail(),

            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'new_comment';
    }

}
