<?php

namespace App\Event\Notification;

use App\Entity\Store\OrderItem;

class FundsReceivedEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        /**
         * @var OrderItem $item
         */
        $item = $this->payload['item'];

        return [
            'name' => $this->receiver->getProfile()->getFullName()
        ];
    }

    public function getType(): string
    {
        return 'funds-received';
    }
}
