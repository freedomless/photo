<?php

namespace App\Event\Notification;

use App\Entity\Photo\Award;
use App\Entity\Photo\Post;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class PhotoAwardedEvent
 * @package App\Event\Notification
 */
class PhotoAwardedEvent extends NotificationEvent
{
    /**
     * @return array
     */
    public function getPayload(): array
    {
        /** @var Award $award */
        $award = $this->payload['award'];

        /** @var Post $post */
        $post = $this->payload['post'];

        return [
            'receiver'   => $this->generateBasicUser($this->receiver),
            'senderName' => 'PiART Curators',
            'post'       => [
                'photo_url' => $post->getImage()->getThumbnail(),
                'publish_date' => $award->getUrlizedDate(),
                'url'       => $this->router->generate(
                    'web_photo_of_the_day',
                    [
                        'id'   => $award->getId(),
                        'date' => $award->getUrlizedDate(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'photo_of_the_day';
    }
}
