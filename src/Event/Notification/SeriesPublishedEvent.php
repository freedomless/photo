<?php

namespace App\Event\Notification;

use App\Entity\Photo\Post;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class SeriesPublishedEvent
 * @package App\Event\Notification
 */
class SeriesPublishedEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        /** @var Post $series */
        $series = $this->payload['series'];

        return [
            'receiver'   => $this->generateBasicUser($this->receiver),
            'senderName' => 'PiART Curators',
            'post'       => [
                'photo_url' => $series->getCover()->getImage()->getThumbnail(),
                'url'       => $this->router->generate(
                    'web_series_show',
                    [
                        'id' => $series->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'series_published';
    }
}
