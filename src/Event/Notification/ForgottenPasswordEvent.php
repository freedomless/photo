<?php

namespace App\Event\Notification;

class ForgottenPasswordEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        return [
            'receiver' => $this->generateBasicUser($this->receiver),
            'token' => $this->payload['token'],
            'receiver_email' => $this->receiver->getEmail()
        ];
    }

    public function getType(): string
    {
        return  'forgotten_password';
    }

}
