<?php

namespace App\Event\Notification;

class NewMessageEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        return [
            'sender'  => $this->generateBasicUser($this->sender),
            'message' => ['url' => $this->router->generate('web_messages') . '?conversation=' . $this->payload['conversation']],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'new_message';
    }
}
