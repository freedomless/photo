<?php

namespace App\Event\Notification;

/**
 * Class PhotoLikedEvent
 * @package App\Event\Notification
 */
class PhotoLikedEvent extends NotificationEvent
{

    /**
     * @return array
     */
    public function getPayload(): array
    {
        $post = $this->payload['post'];

        return [
            'sender' => $this->generateBasicUser($this->sender),
            'post'   => [
                'url'       => $this->router->generate('web_photo_show',
                    [
                        'id'      => $post->getId(),
                        'page'    => 'portfolio',
                        'payload' => $this->sender->getId()]),
                'photo_url' => $post->getImage()->getThumbnail(),
            ],
        ];

    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'post_liked';
    }

}
