<?php

namespace App\Event\Notification;

use App\Entity\User\User;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class NotificationEvent
 * @package App\Event\Notification
 */
abstract class NotificationEvent extends Event
{

    /**
     * @var UserInterface|null
     */
    protected ?UserInterface $sender;

    /**
     * @var UserInterface|null
     */
    protected ?UserInterface $receiver;

    /**
     * @var string|null
     */
    protected ?string $email;

    /**
     * @var array
     */
    protected array $payload;

    /**
     * @var RouterInterface
     */
    protected ?RouterInterface $router;

    /**
     * @var mixed|null
     */
    protected $next;

    /**
     * @var bool|null
     */
    protected ?bool $create;

    /**
     * NotificationEvent constructor.
     * @param null $sender
     * @param null $receiver
     * @param array $payload
     */
    public function __construct($sender = null, $receiver = null, array $payload = [])
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->payload = $payload;
        $this->create = true;
        $this->email = null;
        $this->next = null;
    }

    /**
     * @return null
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return null
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        if ($this->email) {
            return $this->email;
        }

        if ($this->getReceiver()) {
            return $this->getReceiver()->getEmail();
        }

        return null;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed|null
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param mixed|null $next
     */
    public function setNext($next): void
    {
        $this->next = $next;
    }

    /**
     * @return bool|null
     */
    public function getCreate(): ?bool
    {
        return $this->create;
    }

    /**
     * @param bool|null $create
     */
    public function setCreate(?bool $create): void
    {
        $this->create = $create;
    }

    /**
     * @param User $user
     * @return array
     */
    protected function generateBasicUser(User $user)
    {
        return [
            'id'         => $user->getId(),
            'username'   => $user->getUsername(),
            'slug'       => $user->getSlug(),
            'first_name' => $user->getProfile()->getFirstName(),
            'last_name'  => $user->getProfile()->getLastName(),
            'picture'    => $user->getProfile()->getPicture(),
        ];
    }

    /**
     * @return string
     */
    abstract public function getType(): string;
}
