<?php

namespace App\Event\Notification;

class PayoutCompletedEvent extends NotificationEvent
{

    public function getPayload(): array
    {
        return [
            'balance' => $this->payload['balance'],
            'name'    => $this->receiver->getProfile()->getFullName(),
            'status'  => $this->payload['status']
        ];
    }

    public function getType(): string
    {
        return 'payout-completed';
    }
}
