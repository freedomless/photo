<?php

namespace App\Event\Notification;

use App\Entity\Photo\Post;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class PhotoPublishedEvent
 * @package App\Event\Notification
 */
class PhotoPublishedEvent extends NotificationEvent
{
    /**
     * @return array
     */
    public function getPayload(): array
    {
        /** @var Post $post */
        $post = $this->payload['post'];

        return [
            'receiver'   => $this->generateBasicUser($this->receiver),
            'senderName' => 'PiART Curators',
            'post'       => [
                'photo_url' => $post->getImage()->getThumbnail(),
                'url'       => $this->router->generate(
                    'web_photo_show',
                    [
                        'id' => $post->getId(),
                        'page' => 'latest'
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'post_published';
    }
}
