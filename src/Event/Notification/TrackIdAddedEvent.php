<?php

namespace App\Event\Notification;

use App\Entity\Store\Order;

class TrackIdAddedEvent extends NotificationEvent
{
    public function __construct($sender = null, $receiver = null, array $payload = []) {
        parent::__construct($sender, $receiver, $payload);
        /**
         * @var Order $order
         */
        $order = $this->payload['order'];

        $details = $order->getBillingAddress();

        $this->email = $details->getEmail();

    }


    public function getPayload(): array
    {
        /**
         * @var Order $order
         */
        $order = $this->payload['order'];

        $details = $order->getBillingAddress();

        $this->email = $details->getEmail();

        return [
            'name' => $details->getName()
        ];
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'track_id';
    }

}
