<?php

namespace App\Processor;

use App\Enum\ImageSizeEnum;
use Imagick;
use ImagickDraw;
use ImagickException;

/**
 * Class ImageProcessor
 * @package App\ImageProcessor
 */
class ImageProcessor
{

    /**
     * @var int|null
     */
    private ?int $width;

    /**
     * @var int|null
     */
    private ?int $height;

    /**
     * @var bool
     */
    private bool $isGreyscale;

    /**
     * @var Imagick
     */
    private Imagick $imagick;

    /**
     * @var bool
     */
    private bool $bestfit = true;

    /**
     * ImageProcessor constructor.
     */
    public function __construct()
    {
        $this->width = null;
        $this->height = null;
        $this->isGreyscale = false;
        $this->imagick = new Imagick();
    }

    /**
     * @param array $size
     * @param string $uri
     * @param string|null $text
     * @param int $quality
     *
     * @return string
     */
    public function process(array $size, string $uri, ?string $text = null, int $quality = 97): string
    {
        $this->setImage($uri);

        $this->imagick->setImageProperty('colorspace:auto-grayscale', 'false');
        $this->imagick->setImageCompressionQuality($quality);

        $this->resize($size);

        if ($text) {
            $this->text($text);
        }

        $blob = $this->getBlob();

        $this->reset();

        return $blob;

    }

    /**
     * @param array $size
     */
    public function resize(array $size): void
    {
        [$width, $height] = $size;

        if ($this->imagick->getImageWidth() > $size[0] || $this->imagick->getImageHeight() > $size[1]) {
            $this->imagick->resizeImage(min($width, $this->imagick->getImageWidth()), min($height, $this->imagick->getImageHeight()), Imagick::FILTER_LANCZOS, 1, $this->bestfit);
        }

        $this->imagick->stripImage();

    }

    /**
     * @param int $x
     * @param int $y
     * @throws ImagickException
     */
    public function crop(int $x = 10, int $y = 10)
    {
        $this->imagick->cropThumbnailImage($x, $y);
    }

    /**
     * @param string $watermarkPath
     * @param int $x
     * @param int $y
     */
    public function watermark(string $watermarkPath, int $x = 10, int $y = 10)
    {
        $watermark = new Imagick();
        $watermark->readImageFile(fopen($watermarkPath, 'rb'));

        $this->imagick->compositeImage($watermark, Imagick::COMPOSITE_OVER, $x, $y);
    }

    /**
     * Passing zero as either of the arguments will preserve dimension while scaling.
     *
     * @param int $with
     * @param int $height
     * @throws ImagickException
     */
    public function scale(int $with, int $height)
    {
        $this->imagick->scaleImage($with, $height);
    }

    /**
     * @param $name
     * @param string $font
     */
    public function text($name, string $font = 'fonts/ubuntu.ttf'): void
    {
        $text = '© ' . $name;
        $draw = new ImagickDraw();
        // font as absolute or relative path
        $draw->setFont($font);
        // size
        $draw->setFontSize(14);
        // shadow color
        $draw->setFillColor('#000000');
        $draw->setGravity(Imagick::GRAVITY_SOUTHWEST);
        $this->imagick->annotateImage($draw, 10, 12, 0, $text);
        // text color
        $draw->setFillColor('#9b9b9b');

        $this->imagick->annotateImage($draw, 11, 11, 0, $text);
    }

    /**
     * @param string $uri
     * @throws ImagickException
     */
    public function setImage(string $uri): void
    {

        $this->imagick->readImageBlob(file_get_contents($uri));

        $dimensions = $this->imagick->getImageGeometry();

        if (!empty($dimensions) && count($dimensions) > 1) {
            $this->width = $dimensions['width'];
            $this->height = $dimensions['height'];
        }

        if ($this->isGreyscale === null) {

            $identity = $this->imagick->identifyImage();

            $this->isGreyscale = (isset($identity['type']) && strtolower($identity['type']) === ImageSizeEnum::GRAYSCALE);
        }

    }

    /**
     * @return string
     */
    public function getBlob(): string
    {

        return $this->imagick->getImageBlob();
    }

    /**
     * @return string
     */
    public function getOrientation(): ?string
    {
        return $this->width > $this->height ? 'landscape' : 'portrait';
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return bool
     */
    public function isGreyscale(): ?bool
    {
        return $this->isGreyscale ?? false;
    }

    /**
     * Reset image instance
     */
    public function reset(): void
    {

        if ($this->imagick) {
            $this->imagick->clear();
        }

    }

    /**
     * @return bool
     */
    public function isBestfit(): bool
    {
        return $this->bestfit;
    }

    /**
     * @param bool $bestfit
     */
    public function setBestfit(bool $bestfit): void
    {
        $this->bestfit = $bestfit;
    }

    /**
     * @return Imagick
     */
    public function getImagick(): Imagick
    {
        return $this->imagick;
    }

}
