<?php

namespace App\EnvProcessor;

use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

class UrlencodeEnvProcessor  implements EnvVarProcessorInterface
{

    public function getEnv(string $prefix, string $name, \Closure $getEnv)
    {
        $env = $getEnv($name);

        return urlencode($env);
    }

    public static function getProvidedTypes()
    {
       return [ 'urlencode' => 'string'];
    }
}
