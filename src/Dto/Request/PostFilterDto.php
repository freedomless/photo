<?php

namespace App\Dto\Request;

use DateTimeInterface;

/**
 * Class PostFilterDto
 * @package App\Dto\Request
 */
class PostFilterDto
{
    /**
     * @var string|null
     */
    private ?string $categories;

    /**
     * @var string|null
     */
    private ?string $greyscale;

    /**
     * @var string|null
     */
    private ?string $nude;

    /**
     * @var string|null
     */
    private ?string $keyword;

    /**
     * @var string|null
     */
    private ?string $series;

    /**
     * @var string|null
     */
    private ?string $orientation;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $startDate;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $endDate;

    private $user;

    /**
     * PostFilterDto constructor.
     */
    public function __construct() {
        $this->categories = null;
        $this->orientation = null;
        $this->greyscale = null;
        $this->nude = null;
        $this->keyword = null;
        $this->series = null;
        $this->startDate = null;
        $this->endDate = null;
    }

    /**
     * @return string|null
     */
    public function getCategories(): ?string
    {
        return $this->categories;
    }

    /**
     * @param string|null $categories
     * @return PostFilterDto
     */
    public function setCategories(?string $categories): PostFilterDto
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGreyscale(): ?string
    {
        return $this->greyscale;
    }

    /**
     * @param string|null $greyscale
     * @return PostFilterDto
     */
    public function setGreyscale(?string $greyscale): PostFilterDto
    {
        $this->greyscale = $greyscale;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNude(): ?string
    {
        return $this->nude;
    }

    /**
     * @param string|null $nude
     * @return PostFilterDto
     */
    public function setNude(?string $nude): PostFilterDto
    {
        $this->nude = $nude;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string|null $keyword
     * @return PostFilterDto
     */
    public function setKeyword(?string $keyword): PostFilterDto
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeries(): ?string
    {
        return $this->series;
    }

    /**
     * @param string|null $series
     * @return PostFilterDto
     */
    public function setSeries(?string $series): PostFilterDto
    {
        $this->series = $series;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @param DateTimeInterface|null $startDate
     * @return PostFilterDto
     */
    public function setStartDate(?DateTimeInterface $startDate): PostFilterDto
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param DateTimeInterface|null $endDate
     * @return PostFilterDto
     */
    public function setEndDate(?DateTimeInterface $endDate): PostFilterDto
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return PostFilterDto
     */
    public function setUser($user): PostFilterDto
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrientation(): ?string
    {
        return $this->orientation;
    }

    /**
     * @param string|null $orientation
     */
    public function setOrientation(?string $orientation): void
    {
        $this->orientation = $orientation;
    }

}
