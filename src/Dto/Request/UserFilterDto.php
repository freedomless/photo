<?php

namespace App\Dto\Request;

class UserFilterDto
{
    /**
     * @var string|null
     */
    private ?string $username;

    /**
     * @var string|null
     */
    private ?string $name;

    /**
     * @var string|null
     */
    private ?string $firstName;

    /**
     * @var string|null
     */
    private ?string $lastName;

    public function __construct()
    {
        $this->username = null;
        $this->name = null;
        $this->firstName = null;
        $this->lastName = null;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return UserFilterDto
     */
    public function setUsername(?string $username): UserFilterDto
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return UserFilterDto
     */
    public function setName(?string $name): UserFilterDto
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return UserFilterDto
     */
    public function setFirstName(?string $firstName): UserFilterDto
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return UserFilterDto
     */
    public function setLastName(?string $lastName): UserFilterDto
    {
        $this->lastName = $lastName;

        return $this;
    }

}
