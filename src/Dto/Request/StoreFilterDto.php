<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-03-01
 * Time: 12:00
 */

namespace App\Dto\Request;

/**
 * Class StoreFilterDto
 * @package App\Dto
 */
class StoreFilterDto extends PostFilterDto
{

    /**
     * @var string|null
     */
    private $mostSold;

    /**
     * @var string|null
     */
    private $mostLiked;

    /**
     * @var string|null
     */
    private $cheapest;

    /**
     * @var string|null
     */
    private $newest;

    /**
     * @return string|null
     */
    public function getMostSold(): ?string
    {
        return $this->mostSold;
    }

    /**
     * @param string|null $mostSold
     */
    public function setMostSold(?string $mostSold): void
    {
        $this->mostSold = $mostSold;
    }

    /**
     * @return string|null
     */
    public function getMostLiked(): ?string
    {
        return $this->mostLiked;
    }

    /**
     * @param string|null $mostLiked
     */
    public function setMostLiked(?string $mostLiked): void
    {
        $this->mostLiked = $mostLiked;
    }

    /**
     * @return string|null
     */
    public function getCheapest(): ?string
    {
        return $this->cheapest;
    }

    /**
     * @param string|null $cheapest
     */
    public function setCheapest(?string $cheapest): void
    {
        $this->cheapest = $cheapest;
    }

    /**
     * @return string|null
     */
    public function getNewest(): ?string
    {
        return $this->newest;
    }

    /**
     * @param string|null $newest
     */
    public function setNewest(?string $newest): void
    {
        $this->newest = $newest;
    }

}
