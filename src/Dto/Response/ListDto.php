<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-02-23
 * Time: 21:39
 */

namespace App\Dto\Response;

use Symfony\Component\Serializer\Annotation\Groups;

class ListDto
{
    /**
     * @var mixed
     * @Groups({"api"})
     */
    private $items;

    /**
     * @var int
     * @Groups({"api"})
     */
    private int $total;

    /**
     * @var int
     * @Groups({"api"})
     */
    private int $pages;

    /**
     * @var boolean
     * @Groups({"api"})
     */
    private bool $hasMore;

    public function __construct( $items = [], int $total = 0, int $pages = 0, bool $hasMore = false) {
        $this->items = $items;
        $this->total = $total;
        $this->pages = $pages;
        $this->hasMore = $hasMore;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     */
    public function setPages(int $pages): void
    {
        $this->pages = $pages;
    }

    /**
     * @return bool
     */
    public function isHasMore(): bool
    {
        return $this->hasMore;
    }

    /**
     * @param bool $hasMore
     */
    public function setHasMore(bool $hasMore): void
    {
        $this->hasMore = $hasMore;
    }

}
