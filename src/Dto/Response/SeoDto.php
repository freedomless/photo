<?php

namespace App\Dto\Response;

/**
 * Class SeoDto
 * @package App\Dto
 */
class SeoDto
{
    /**
     * @var string|null
     */
    private ?string $title;

    /**
     * @var string|null
     */
    private ?string $description;

    /**
     * @var string|null
     */
    private ?string $image;

    /**
     * @var string|null
     */
    private ?string $imageWidth = null;

    /**
     * @var string|null
     */
    private ?string $imageHeight = null;

    /**
     * @var string|null
     */
    private ?string $creator;

    /**
     * @var string|null
     */
    private ?string $createdAt;

    /**
     * @var string|null
     */
    private ?string $tags;

    /**
     * @var string|null
     */
    private ?string $url;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return SeoDto
     */
    public function setTitle(?string $title): SeoDto
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return SeoDto
     */
    public function setDescription(?string $description): SeoDto
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     * @return SeoDto
     */
    public function setImage(?string $image): SeoDto
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreator(): ?string
    {
        return $this->creator;
    }

    /**
     * @param string|null $creator
     * @return SeoDto
     */
    public function setCreator(?string $creator): SeoDto
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string|null $createdAt
     * @return SeoDto
     */
    public function setCreatedAt(?string $createdAt): SeoDto
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTags(): ?string
    {
        return $this->tags;
    }

    /**
     * @param string|null $tags
     * @return SeoDto
     */
    public function setTags(?string $tags): SeoDto
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return SeoDto
     */
    public function setUrl(?string $url): SeoDto
    {
        $this->url = $url;

        return $this;
    }

    public function getImageHeight(): ?string
    {
        return $this->imageHeight;
    }

    public function setImageHeight(?string $imageHeight)
    {
        $this->imageHeight = $imageHeight;

        return $this;
    }

    public function getImageWidth(): ?string
    {
        return $this->imageWidth;
    }

    public function setImageWidth(?string $imageWidth)
    {
        $this->imageWidth = $imageWidth;

        return $this;
    }
}
