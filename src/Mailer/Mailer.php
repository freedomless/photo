<?php

namespace App\Mailer;

use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Throwable;
use Twig\Environment;

/**
 * Class EmailService
 * @package App\Service
 */
class Mailer
{
    /**
     * @var Swift_Mailer
     */
    private MailerInterface $mailer;

    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * @var array|false|string
     */
    private $siteEmail;

    /**
     * @var array|false|string
     */
    private $siteName;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var KernelInterface
     */
    private KernelInterface $kernel;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;

    /**
     * @var
     */
    private $emailExceptionList;

    /**
     * EmailService constructor.
     * @param Swift_Mailer $mailer
     * @param Environment $twig
     * @param KernelInterface $kernel
     * @param TokenStorageInterface $storage
     * @param LoggerInterface $logger
     * @param string $siteEmail
     * @param string $siteName
     * @param $emailExceptionList
     */
    public function __construct(MailerInterface $mailer,
                                Environment $twig,
                                KernelInterface $kernel,
                                TokenStorageInterface $storage,
                                LoggerInterface $logger,
                                string $siteEmail,
                                string $siteName,
                                $emailExceptionList
    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;

        $this->siteEmail = $siteEmail;
        $this->siteName = $siteName;
        $this->logger = $logger;
        $this->kernel = $kernel;
        $this->storage = $storage;
        $this->emailExceptionList = $emailExceptionList;
    }

    /**
     * @param string $subject
     * @param string $receiver
     * @param string $template
     * @param array $payload
     * @param string|null $replyTo
     */
    public function send(
        string $subject,
        string $receiver,
        string $template,
        array $payload = [],
        ?string $replyTo = null
    ): void
    {
        try {

            $message = (new TemplatedEmail);

            if (null !== $replyTo) {
                $message
                    ->replyTo($replyTo);
            }

            $senderName = $payload['senderName'] ?? $this->siteName . ' Team';

            $message
                ->from(new Address($this->siteEmail, $senderName))
                ->subject($subject)
                ->to($receiver)
                ->context(compact('payload'))
                ->htmlTemplate('emails/' . $template . '.html.twig');

            $this->mailer->send($message);
        } catch (Throwable $e) {
            $this->logger->critical($e->getMessage());
        }
    }

}
