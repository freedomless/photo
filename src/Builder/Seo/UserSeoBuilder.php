<?php

namespace App\Builder\Seo;

use App\Dto\Response\SeoDto;
use App\Entity\EntityInterface;
use App\Entity\User\User;

class UserSeoBuilder extends BaseSeoBuilder
{
    /**
     * @param EntityInterface|User $user
     * @param string $key
     * @param array|null $params
     * @return SeoDto
     */
    public function build(EntityInterface $user, string $key, ?array $params = null): SeoDto
    {
        $image = $user->getProfile()->getCover();

        $seoDto = new SeoDto();
        $seoDto
            ->setTitle($user->getProfile()->getFullName())
            ->setDescription($this->translator->trans("{$key}.description"))
            ->setImage($image)
            ->setCreator($user->getUsername())
            ->setCreatedAt(null)
            ->setTags('')
            ->setUrl(getenv('SITE_URL'));

        if ($image) {
            $imageDetails = getimagesize($image) ?? [];

            $seoDto
                ->setImageWidth($imageDetails[0] ?? '')
                ->setImageHeight($imageDetails[1] ?? '');
        }

        return $seoDto;
    }
}
