<?php

namespace App\Builder\Seo;

use App\Dto\Response\SeoDto;
use App\Entity\EntityInterface;
use App\Entity\User\User;

class UserStoreSeoBuilder extends BaseSeoBuilder
{

    /**
     * @param EntityInterface|User $user
     * @param string $key
     * @param array|null $params
     * @return SeoDto
     */
    public function build(EntityInterface $user, string $key, ?array $params = null): SeoDto
    {
        $image = $user->getProfile()->getCover();

        $fullName = $user->getProfile()->getFullName() ?: $user->getUsername();

        $seoDto = new SeoDto();
        $seoDto
            ->setTitle('Prints for Sale by '.$fullName)
            ->setDescription('Best Quality Art Prints')
            ->setImage($image)
            ->setCreator($user->getUsername())
            ->setCreatedAt(null)
            ->setTags('')
            ->setUrl(getenv('SITE_URL'));

        if ($image) {
            $imageDetails = getimagesize($image) ?? [];

            $seoDto
                ->setImageWidth($imageDetails[0] ?? '')
                ->setImageHeight($imageDetails[1] ?? '');
        }

        return $seoDto;
    }
}