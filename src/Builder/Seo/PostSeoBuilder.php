<?php

namespace App\Builder\Seo;

use App\Dto\Response\SeoDto;
use App\Entity\EntityInterface;
use App\Entity\Photo\Post;
use DateTime;

class PostSeoBuilder extends BaseSeoBuilder
{
    /**
     * @param EntityInterface|Post $post
     * @param string $key
     * @param array|null $params
     *
     * @return SeoDto
     * @throws \Exception
     */
    public function build(EntityInterface $post, string $key, ?array $params = null): SeoDto
    {
        $title = $this->translator->trans("{$key}.title");

        if (isset($params['date'])) {
            $title = sprintf(
                $this->translator->trans("{$key}.title"),
                (new DateTime($params['date']))->format('d M Y')
            );
        }

        if ($post->getStatus() > 2) {
            $description = $this->getPhotoMeta($post);
        } else {
            $title = $this->getPhotoMeta($post);
        }

        $image = $post->getImage()->getLarge();

        if ($post->getStatus() > 2 && $post->getImage()->getSocial()) {
            $image = $post->getImage()->getSocial();
        }

        $seoDto = new SeoDto();
        $seoDto
            ->setTitle($title)
            ->setDescription($description ?? null)
            ->setImage($image)
            ->setCreator($post->getUser()->getUsername())
            ->setCreatedAt($post->getCreatedAt()->format('Y-m-d H:i:s'))
            ->setUrl(getenv('SITE_URL'))
            ->setTags($this->getTags($post->getTags()));

        $imageDetails = getimagesize($image) ?? [];
        $seoDto
            ->setImageWidth($imageDetails[0] ?? '')
            ->setImageHeight($imageDetails[1] ?? '');

        return $seoDto;
    }

    private function getPhotoMeta(Post $photo): string
    {
        if ($photo->getTitle()) {
            return sprintf('"%s" by %s', $photo->getTitle(), $photo->getUser()->getProfile()->getFullName());
        }

        return sprintf('Photography by %s', $photo->getUser()->getProfile()->getFullName());
    }
}
