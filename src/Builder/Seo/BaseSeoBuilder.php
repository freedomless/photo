<?php

namespace App\Builder\Seo;

use App\Dto\Response\SeoDto;
use App\Entity\EntityInterface;
use App\Entity\Photo\Tag;
use Doctrine\Common\Collections\Collection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BaseSeoBuilder
 * @package App\Builder\Seo
 */
abstract class BaseSeoBuilder
{
    /**
     * @var TranslatorInterface
     */
    protected TranslatorInterface $translator;

    /**
     * PostSeoBuilder constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param EntityInterface $entity
     * @param string $context
     * @param array|null $params
     * @return SeoDto
     */
    abstract public function build(EntityInterface $entity, string $context, ?array $params = null): SeoDto;

    /**
     * @param Collection $tags
     * @return mixed
     */
    protected function getTags(Collection $tags)
    {
        $tags = array_filter($tags->toArray(), function ($tag) {
            /** @var $tag Tag */
            return !empty($tag->getName());
        });

        $contact = '';

        foreach ($tags as $tag) {
            $contact .= $tag->getName() . ' ';
        }

        return str_replace(' ', ',', $contact);
    }
}
