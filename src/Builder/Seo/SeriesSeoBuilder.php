<?php

namespace App\Builder\Seo;

use App\Dto\Response\SeoDto;
use App\Entity\EntityInterface;
use App\Entity\Photo\Post;

class SeriesSeoBuilder extends BaseSeoBuilder
{
    /**
     * @param EntityInterface|Post $series
     * @param string $key
     * @param array|null $params
     * @return SeoDto
     */
    public function build(EntityInterface $series, string $key, ?array $params = null): SeoDto
    {
        $title = $this->translator->trans("{$key}.title");
        if ($series->getStatus() > 2) {
            $description = $this->getSeriesMeta($series);
        } else {
            $title = $this->getSeriesMeta($series);
        }

        $user = $series->getUser() ? $series->getUser()->getUsername() : 'Curators Team';

        $image = $series->getImage()->getSocial() ?: $series->getCover()->getImage()->getLarge();

        $seoDto = new SeoDto();
        $seoDto
            ->setTitle($title)
            ->setDescription($description ?? null)
            ->setImage($image)
            ->setCreator($user)
            ->setCreatedAt($series->getCreatedAt()->format('d/m/Y H:i:s'))
            ->setUrl(getenv('SITE_URL'))
            ->setTags($this->getTags($series->getTags()));

        $imageDetails = getimagesize($image) ?? [];
        $seoDto
            ->setImageWidth($imageDetails[0] ?? '')
            ->setImageHeight($imageDetails[1] ?? '');

        return $seoDto;
    }

    private function getSeriesMeta(Post $series): string
    {
        if ($series->getTitle()) {

            $user = $series->getUser() ? $series->getUser()->getProfile()->getFullName() : 'Curators Team';

            return sprintf('"%s" by %s', $series->getTitle(), $user);
        }

        return sprintf('Photography by %s', $series->getUser() ? $series->getUser()->getProfile()->getFullName() : '');
    }
}
