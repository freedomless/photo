<?php

namespace App\Enum;

abstract class FileStorageFolderEnum
{
    public const IMAGES_PRINT     = 'prints/originals/';
    public const IMAGES_PRINT_THUMBNAIL    = 'prints/thumbnails/';
    public const IMAGES_ORIGINAL     = 'originals/';
    public const IMAGES_THUMBS_SMALL = 'thumbs/small/';
    public const IMAGES_THUMBS_MID   = 'thumbs/mid/';
    public const IMAGES_THUMBS_LARGE = 'thumbs/large/';
    public const IMAGES_SOCIAL = 'socials/';

    public const AVATAR = 'users/pictures/';
    public const COVER = 'users/covers/';
}
