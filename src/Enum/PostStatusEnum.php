<?php

namespace App\Enum;

abstract class PostStatusEnum
{
    public const NEW        = 0;
    public const FOR_CURATE = 1;
    public const REJECTED   = 2;
    public const PUBLISHED  = 3;
    public const FOR_SALE   = 4;

}
