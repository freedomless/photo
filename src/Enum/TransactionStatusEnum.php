<?php

namespace App\Enum;

abstract class TransactionStatusEnum
{
    public const PENDING  = 0;
    public const SUCCESS  = 1;
    public const FAILED   = -1;
    public const CANCELED = -2;
}
