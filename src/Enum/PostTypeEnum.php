<?php

namespace App\Enum;

abstract class PostTypeEnum
{
    public const SINGLE                = 1;
    public const SERIES                = 2;
    public const SERIES_FROM_PUBLISHED = 3;
    public const SERIES_FROM_CURATOR   = 4;

}
