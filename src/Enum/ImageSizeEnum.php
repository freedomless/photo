<?php

namespace App\Enum;

abstract class ImageSizeEnum
{
    public const IMAGE_THUMB_LARGE = [2048, 1200];
    public const IMAGE_THUMB_MID   = [1024, 1024];
    public const IMAGE_THUMB_SMALL = [500, 500];
    public const AVATAR            = [200, 200];
    public const COVER             = [1200, 350];
    public const GRAYSCALE         = 'grayscale';

}
