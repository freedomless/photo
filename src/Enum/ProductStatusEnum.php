<?php

namespace App\Enum;

abstract class ProductStatusEnum
{
    public const NEW = 0;
    public const COMPLETED = 1;
    public const REJECTED = 2;
    public const PUBLISHED = 3;
}
