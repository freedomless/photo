<?php

namespace App\Exception;

abstract class ExceptionCodes
{
    /**
     * General service  errors
     * Error codes starts at 1XXX
     */
    public const GENERAL = 11000;
    public const VALIDATION_ERROR = 11001;
    public const ENTITY_NOT_FOUND = 11002;
    public const UNPROCESSABLE_ENTITY = 11003;
    public const NON_UNIQUE_VALUE = 11004;
    public const HTTP_EXCEPTION = 11005;
    public const HTTP_NOT_FOUND_EXCEPTION = 11006;
    public const HTTP_SERVER_EXCEPTION = 11007;


    public const CODES = [];

}

