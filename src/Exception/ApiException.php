<?php

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Class ApiException
 * @package App\Exception
 */
class ApiException extends Exception
{
    /**
     * @var array
     */
    private array $errors;

    /**
     * ApiException constructor.
     * @param int $code
     * @param array $errors
     * @param Throwable|null $previous
     */
    public function __construct(int $code = 11000, array $errors = [], Throwable $previous = null)
    {

        $this->errors = $errors;

        parent::__construct(ExceptionCodes::CODES[$code] ?? 'Error', $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

}
