<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-03-23
 * Time: 16:43
 */

namespace App\PaymentGateway;

use App\Exception\ApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GatewayFactory
 * @package App\PaymentGateway
 */
class PaymentGatewayFactory
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * GatewayFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $type
     * @return PayPal\PayPalPayment|object
     * @throws ApiException
     */
    public function build(string $type = 'paypal')
    {
        switch ($type) {
            case 'paypal':
                return $this->container->get('payment.paypal');
            default:
                throw new ApiException(5404);
        }
    }
}
