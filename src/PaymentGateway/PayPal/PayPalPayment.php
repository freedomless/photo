<?php /** @noinspection CallableParameterUseCaseInTypeContextInspection */

namespace App\PaymentGateway\PayPal;

use App\Entity\Configuration;
use App\Entity\Store\Invoice;
use App\Entity\Store\Order;
use App\Entity\Store\OrderItem;
use App\Entity\Store\Transaction as TransactionEntity;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\EntityManager\InvoiceManager;
use App\Enum\TransactionStatusEnum;
use App\Event\Notification\InvoiceCreatedEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use App\Helper\PriceCalculator;
use App\Helper\SerializationTrait;
use App\Helper\ValidationTrait;
use App\EntityManager\OrderManager;
use App\PaymentGateway\PayPal\Builder\AmountBuilder;
use App\PaymentGateway\PayPal\Builder\DetailsBuilder;
use App\PaymentGateway\PayPal\Builder\ItemBuilder;
use App\PaymentGateway\PayPal\Builder\PayerBuilder;
use App\PaymentGateway\PayPal\Builder\TransactionBuilder;
use App\PaymentGateway\PayPal\Builder\UrlBuilder;
use App\Repository\ConfigurationRepository;
use Exception;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PayPalPayment
 * @package App\PaymentGateway
 */
class PayPalPayment
{
    use SerializationTrait,
        ValidationTrait,
        DispatcherTrait;

    private const GATEWAY = 'PayPal';

    /**
     * @var OrderManager
     */
    private OrderManager $orderManager;

    /**
     * @var BaseManager
     */
    private BaseManager $transactionManager;

    /**
     * @var InvoiceManager
     */
    private InvoiceManager $invoiceManager;

    /**
     * @var Configuration
     */
    private ?Configuration $config;

    /**
     * @var PriceCalculator
     */
    private PriceCalculator $calculator;

    /**
     * PayPalPayment constructor.
     * @param PriceCalculator $priceCalculator
     * @param OrderManager $orderManager
     * @param InvoiceManager $invoiceManager
     * @param ConfigurationRepository $configManager
     * @param BaseManager $transactionManager
     * @throws InvalidArgumentException
     */
    public function __construct(
        PriceCalculator $priceCalculator,
        OrderManager $orderManager,
        InvoiceManager $invoiceManager,
        ConfigurationRepository $configManager,
        BaseManager $transactionManager
    )
    {
        $this->config = $configManager->getConfig();
        $this->orderManager = $orderManager;
        $this->transactionManager = $transactionManager;
        $this->invoiceManager = $invoiceManager;
        $this->calculator = $priceCalculator;
    }

    /**
     * @param Order $order
     * @param User|null $user
     * @return bool|RedirectResponse
     * @throws ApiException
     */
    public function create(Order $order, ?User $user)
    {
        $this->validate($order, ['order']);

        $payment = $this->generatePayment($order, $user);

        try {

            $payment->create(PayPalConfig::getContext());

            $order = $this->orderManager->create($order);

            $transaction = new TransactionEntity();
            $transaction->setOrder($order);
            $transaction->setGateway(static::GATEWAY);
            $transaction->setCurrencyCode($this->config->getCurrency());
            $transaction->setPaymentGross($payment->getTransactions()[0]->getAmount()->getTotal());
            $transaction->setTrnId($payment->getTransactions()[0]->getInvoiceNumber());
            $transaction->setExternalTrnId($payment->getToken());
            $transaction->setStatus(TransactionStatusEnum::PENDING);

            $this->transactionManager->create($transaction);
            //Create payment
            //If paypal payment api return successful response
            //return redirect url to paypal
            return $payment->getApprovalLink();
        } catch (Exception $ex) {
            //Throw something went wrong
            throw new ApiException(4003);
        }
    }

    /**
     * @return  mixed
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function complete()
    {
        // Determine if the user approved the payment or not
        if (isset($_GET['paymentId'])) {

            //Get transaction id
            $paymentId = $_GET['paymentId'];

            //Get Execution
            $payment = $this->getPayment($paymentId);

            $execution = new PaymentExecution();

            $execution->setPayerId($_GET['PayerID']);

            $transaction = $this->transactionManager->repository->findOneBy(['trnId' => $payment->getTransactions()[0]->getInvoiceNumber()]);

            if ($transaction === null) {
                throw new HttpException(404);
            }

            if ($transaction->getStatus() === TransactionStatusEnum::SUCCESS) {
                return $transaction->getOrder()->getInvoice()->getToken();
            }

            try {
                // Execute the payment
                $result = $payment->execute($execution, PayPalConfig::getContext());

                $payment = $this->getPayment($result->getId());

                if ($payment->getState() === 'approved') {

                    $transaction->setStatus(TransactionStatusEnum::SUCCESS);

                    $invoice = new Invoice();
                    $invoice->setOrder($transaction->getOrder());

                    $this->invoiceManager->create($invoice);

                    $this->dispatch(new InvoiceCreatedEvent(null, null, $this->normalize($transaction->getOrder()->getBillingAddress())));

                } else {
                    $transaction->setStatus(TransactionStatusEnum::FAILED);
                }

                $this->transactionManager->flush($transaction);

            } catch (Exception $ex) {
                throw new ApiException(4004);
            }

            return $transaction->getOrder()->getInvoice()->getToken();
        }

        return false;
    }

    /**
     * @throws ApiException
     */
    public function cancel(): void
    {
        if (isset($_GET['token'])) {

            $transaction = $this->transactionManager->repository->findOneBy(['externalTrnId' => $_GET['token']]);

            $transaction->setStatus(TransactionStatusEnum::CANCELED);

            $this->transactionManager->flush($transaction);
        }
    }

    /**
     * @param Order $order
     * @param User|null $user
     * @return Payment
     * @throws ApiException
     * @throws Exception
     */
    private function generatePayment(Order $order, ?User $user): Payment
    {
        //Control var
        $subtotal = 0;

        //Items list
        $itemList = new ItemList();

        if ($order->getItems()->count() === 0) {
            throw new ApiException(5000);
        }

        /**
         * @var $item OrderItem
         * Fill ItemList with items ordered
         */
        foreach ($order->getItems() as $item) {

            $product = $item->getProduct();

            //If product not found in db throw error
            if (!$product) {
                throw new ApiException(5001);
            }

            //Get material price per sq2
            $material = $item->getMaterial();
            //Get size
            $size = $item->getSize();

            //If material or dimension does not exists in db throw error
            if ($material->getId() === null || $size->getId() === null) {
                throw new ApiException(5002);
            }

            $calculatedPrice = $this->calculator->calculatePrice($product, $size, $material);
            $price = (float)str_replace(',', '.', $calculatedPrice['total']);

            // item price to be stored to db
            $item->setPrice($price);

            $item->setCommission($product->getCommission());
            $item->setDiscount((float)str_replace(',', '.', $calculatedPrice['discount']));

            //Add Item to the array
            $itemList->addItem(ItemBuilder::build($product->getPost()->getTitle() ?? 'Art Print - '. $product->getPost()->getId(), $this->config->getCurrency(), $item->getQuantity(), $item->getProduct()->getId(), $price));

            //Calculate price * quantity (quantity included later not in single unit price)
            $price *= $item->getQuantity();

            //Add to subtotal
            $subtotal += $price;
        }

        //Create details about payment shipping, subtotal and taxes
        $details = DetailsBuilder::build(0, $subtotal * $this->config->getTax(), $subtotal);

        //Get total amount with taxes and shipping
        $total = $subtotal + $subtotal * $this->config->getTax();

        //Create amount and add currency
        $amount = AmountBuilder::build($total, $this->config->getCurrency(), $details);

        //Create transaction
        $transaction = TransactionBuilder::build($amount, $itemList, 'Image Printing Payment');

        //Create payer and add payment method
        $payer = PayerBuilder::build();

        //Generate urls for redirect in case of success or cancel
        $urls = UrlBuilder::build();

        //Create Payment
        $payment = $this->createPayment($payer, $urls, $transaction);

        //Add details to order so it can be stored to the db
        $order->setShippingCost(0);

        //Set tax
        $order->setTax($subtotal * $this->config->getTax());

        //Set total price
        $order->setTotal($total);

        //If user add it to order
        $order->setCustomer($user);

        return $payment;
    }

    /**
     * @param $payment
     * @return Payment
     */
    private function getPayment($payment): Payment
    {
        return Payment::get($payment, PayPalConfig::getContext());
    }

    /**
     * @param Payer $payer
     * @param RedirectUrls $urls
     * @param Transaction $transaction
     * @return Payment
     */
    private function createPayment(Payer $payer, RedirectUrls $urls, Transaction $transaction): Payment
    {
        $payment = new Payment();

        return $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($urls)
            ->setTransactions([$transaction]);
    }

}
