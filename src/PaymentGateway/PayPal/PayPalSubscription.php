<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-03-08
 * Time: 00:40
 */

namespace App\PaymentGateway\PayPal;

use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

class PayPalSubscription
{
    public function subscribe()
    {
    }

    public function createPayer()
    {
        $payer = new Payer();

        return $payer->setPaymentMethod('paypal');
    }

    public function createPlan(string $id)
    {
        $plan = new Plan();

        return $plan->setId($id)->setType('FIXED');
    }

    public function createShippingAddress()
    {
        $shipping  = new ShippingAddress();
    }
}
