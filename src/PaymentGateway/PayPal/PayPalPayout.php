<?php

namespace App\PaymentGateway\PayPal;

use PayPal\Api\Currency;
use PayPal\Api\Payout;
use PayPal\Api\PayoutItem;
use PayPal\Api\PayoutSenderBatchHeader;

/**
 * Class PayPalPayout
 * @package App\PaymentGateway\PayPal
 */
class PayPalPayout
{
    /**
     * @var PayPalConfig
     */
    private $config;

    /**
     * PayPalPayout constructor.
     * @param PayPalConfig $config
     */
    public function __construct(PayPalConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $email
     * @param float $amount
     * @return bool
     */
    public function payout(string $email, float $amount)
    {
        $payout = new Payout();

        $senderBatchHeader = new PayoutSenderBatchHeader();
        $senderBatchHeader->setEmailSubject('You have payout from Photoimaginart');
        $senderBatchHeader->setSenderBatchId(uniqid('', true));

        $senderItem = new PayoutItem();
        $senderItem->setNote('Payout from Photoimaginart');
        $senderItem->setReceiver($email);
        $senderItem->setRecipientType('Email');

        $currency = new Currency(json_encode(['value' => $amount,'currency' => 'EUR']));
        $senderItem->setAmount($currency);

        $payout->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem);


        try {
            $payout->create(null, $this->config->getContext());
        } catch (\Exception $ex) {
            if ($ex->getCode() === 422) {
                return 'Insufficient funds';
            }

            return 'General error';
        }


        return true;
    }
}
