<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\Amount;
use PayPal\Api\ItemList;
use PayPal\Api\Transaction;

class TransactionBuilder
{
    /**
     * @param Amount $amount
     * @param ItemList $itemList
     * @param string $description
     * @return Transaction
     * @throws \Exception
     */
    public static function build(Amount $amount, ItemList $itemList, string $description): Transaction
    {
        $transaction = new Transaction();

        return $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($description)
            ->setInvoiceNumber(bin2hex(random_bytes(32)));
    }

}
