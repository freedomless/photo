<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\Payer;

class PayerBuilder
{
    /**
     * @return Payer
     */
    public static function build(): Payer
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        return $payer;
    }

}
