<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\Amount;
use PayPal\Api\Details;

class AmountBuilder
{
    /**
     * @param float $total
     * @param string $currency
     * @param Details $details
     * @return Amount
     */
    public static function build(float $total, string $currency, Details $details): Amount
    {
        $amount = new Amount();

        return $amount->setCurrency($currency)
            ->setTotal($total)
            ->setDetails($details);
    }

}
