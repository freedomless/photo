<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\RedirectUrls;

class UrlBuilder
{
    /**
     * @return RedirectUrls
     */
    public static function build(): RedirectUrls
    {
        $baseUrl = getenv('SITE_HOST');

        return (new RedirectUrls())->setReturnUrl("$baseUrl/paypal/answer")
            ->setCancelUrl("$baseUrl/paypal/cancel");
    }
}
