<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\Details;

class DetailsBuilder
{
    /**
     * @param float $shipping
     * @param float $tax
     * @param float $subtotal
     * @return Details
     */
    public static function build(float $shipping, float $tax, float $subtotal): Details
    {
        $details = new Details();

        return $details->setShipping($shipping)
            ->setTax($tax)
            ->setSubtotal($subtotal);
    }

}
