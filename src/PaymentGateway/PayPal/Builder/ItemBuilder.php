<?php

namespace App\PaymentGateway\PayPal\Builder;

use PayPal\Api\Item;

class ItemBuilder
{
    /**
     * @param string $name
     * @param string $currency
     * @param int $quantity
     * @param int $itemId
     * @param float $price
     * @return Item
     */
    public static function build(string $name, string $currency, int $quantity, int $itemId, float $price): Item
    {
        $item = new Item();

        return $item->setName($name)
            ->setCurrency($currency)
            ->setQuantity($quantity)
            ->setSku($itemId)// Similar to `item_number` in Classic API
            ->setPrice($price);
    }

}
