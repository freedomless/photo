<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-03-11
 * Time: 22:15
 */

namespace App\PaymentGateway\PayPal;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 * Class PayPalConfig
 * @package App\PaymentGateway\PayPal
 */
class PayPalConfig
{

    /**
     * @return ApiContext
     */
    public static function getContext(): ApiContext
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                getenv('PAYPAL_CLIENT_ID'),
                getenv('PAYPAL_CLIENT_SECRET')
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => getenv('PAYPAL_MODE'),
            )
        );

        return $apiContext;
    }
}
