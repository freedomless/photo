<?php

namespace App\PaymentGateway;

interface PaymentInterface
{
    public function create();
    public function complete();
    public function cancel();
}
