<?php

namespace App\Entity\User;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_forgotten_password")
 */
class ForgottenPassword extends BaseEntity implements EntityInterface
{
    /**
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @ORM\Column(type="boolean",options={"default":false})
     */
    private $isUsed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="forgottenPasswords")
     */
    private $user;

    /**
     * @ORM\Column(type="integer",options={"default": 900})
     */
    private $expiresIn;

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ForgottenPassword
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsUsed(): ?bool
    {
        return $this->isUsed;
    }

    /**
     * @param bool $isUsed
     * @return ForgottenPassword
     */
    public function setIsUsed(bool $isUsed): self
    {
        $this->isUsed = $isUsed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpiresIn(): ?int
    {
        return $this->expiresIn;
    }

    /**
     * @param int $expiresIn
     * @return ForgottenPassword
     */
    public function setExpiresIn(int $expiresIn): self
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return ForgottenPassword
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
