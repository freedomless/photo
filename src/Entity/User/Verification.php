<?php

namespace App\Entity\User;

use App\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_verifications")
 */
class Verification extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="verifications")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean",options={"default":false})
     */
    private $used;

    /**
     * Verification constructor.
     * @param User|null $user
     * @throws \Exception
     */
    public function __construct(?User $user = null)
    {
        $this->user = $user;
        $this->used = false;
        $this->token = bin2hex(random_bytes(32));
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Verification
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Verification
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @param mixed $used
     */
    public function setUsed($used): void
    {
        $this->used = $used;
    }
}
