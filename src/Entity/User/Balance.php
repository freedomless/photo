<?php

namespace App\Entity\User;

use App\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_balances")
 */
class Balance extends BaseEntity
{

    /**
     * @ORM\Column(type="float",scale=2,precision=10,options={"default":0})
     */
    private $total;

    /**
     * @ORM\Column(type="float",scale=2,precision=10,nullable=true,options={"default":0})
     */
    private $blocked;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User",inversedBy="balance")
     */
    private $user;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $payout;

    public function __construct(?User $user = null)
    {
        $this->user = $user;
        $this->total = 0;
        $this->blocked = 0;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * @param mixed $payout
     */
    public function setPayout($payout): void
    {
        $this->payout = $payout;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked): void
    {
        $this->blocked = $blocked;
    }
}
