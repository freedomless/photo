<?php

namespace App\Entity\User;

use App\Entity\BaseEntity;
use App\Validator\IsUrl;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_profiles")
 */
class Profile extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=false)
     *
     * @Assert\NotBlank(normalizer="trim")
     *
     * @Groups({"post","comment","message","basic_user"})
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=false)
     *
     * @Assert\NotBlank(normalizer="trim")
     *
     * @Groups({"post","comment","message","basic_user"})
     */
    private $lastName;

    /**
     * @var mixed
     *
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"comment","message","basic_user","favorites"})
     */
    private $picture;

    /**
     * @var UploadedFile|null
     */
    private $pictureFile;

    /**
     * @var mixed
     *
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"basic_user"})
     */
    private $cover;

    /**
     * @var UploadedFile|null
     */
    private $coverFile;

    /**
     * @var Address|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Address",cascade={"persist","remove"}, inversedBy="profile")
     */
    private $address;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean",nullable=true,options={"default":true})
     */
    private $sameBilling;

    /**
     * @var Address|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Address", cascade={"persist", "remove"})
     */
    private $billing;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     * @IsUrl()
     */
    private $facebook;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     * @IsUrl()
     */
    private $twitter;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     * @IsUrl()
     */
    private $instagram;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     * @IsUrl()
     */
    private $website;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $language;

    /**
     * @var string|null
     * @ORM\Column(type="string",length=2, nullable=true)
     */
    private $nationality;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\User",inversedBy="profile")
     */
    private $user;

    /**
     * Profile constructor.
     */
    public function __construct() {

        $this->address = new Address();
        $this->billing = new Address();

    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed|null $picture
     * @return $this
     */
    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string|null $cover
     * @return $this
     */
    public function setCover( $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSameBilling(): ?bool
    {
        return $this->sameBilling;
    }

    /**
     * @param bool|null $sameBilling
     * @return $this
     */
    public function setSameBilling(?bool $sameBilling): self
    {
        $this->sameBilling = $sameBilling;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    /**
     * @param string|null $facebook
     * @return $this
     */
    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    /**
     * @param string|null $twitter
     * @return $this
     */
    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    /**
     * @param string|null $instagram
     * @return $this
     */
    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @param string|null $website
     * @return $this
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     * @return $this
     */
    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address ?? new Address();
    }

    /**
     * @param Address|null $address
     * @return $this
     */
    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getBilling(): ?Address
    {
        return $this->billing ?? new Address();
    }

    /**
     * @param Address|null $billing
     * @return $this
     */
    public function setBilling(?Address $billing): self
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->getLastName();
    }

    /**
     * @return UploadedFile|null
     */
    public function getPictureFile(): ?UploadedFile
    {
        return $this->pictureFile;
    }

    /**
     * @param UploadedFile|null $pictureFile
     */
    public function setPictureFile(?UploadedFile $pictureFile): void
    {
        $this->pictureFile = $pictureFile;
    }

    /**
     * @return UploadedFile|null
     */
    public function getCoverFile(): ?UploadedFile
    {
        return $this->coverFile;
    }

    /**
     * @param UploadedFile|null $coverFile
     */
    public function setCoverFile(?UploadedFile $coverFile): void
    {
        $this->coverFile = $coverFile;
    }



}
