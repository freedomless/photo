<?php

namespace App\Entity\User;

use App\Entity\BaseEntity;
use App\Entity\Notification\Notification;
use App\Entity\Photo\Album;
use App\Entity\Photo\CurateComment;
use App\Entity\Photo\Favorite;
use App\Entity\Photo\Post;
use App\Entity\Poll\Poll;
use App\Entity\Poll\PollVote;
use App\Entity\Store\Order;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Rollerworks\Component\PasswordStrength\Validator\Constraints\PasswordRequirements;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="t_users")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity(fields={"email"}, message="It looks like your already have an account!")
 * @UniqueEntity(fields={"username"}, message="It looks like this username is taken!")
 */
class User extends BaseEntity implements UserInterface
{

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true))
     *
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Length(min="3")
     *
     * @Groups({"basic_user"})
     */
    private $username;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"username"})
     *
     * @ORM\Column(length=128, unique=true,nullable=true)
     *
     * @Groups({"post","comment","message","basic_user","favorites"})
     */
    private $slug;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $roles = ['ROLE_MEMBER'];

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     *
     * @PasswordRequirements(minLength="8",requireLetters=true,requireNumbers=true,requireCaseDiff=true)
     */
    private $password;

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $facebookId;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $googleId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $isVerified;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $isBlocked;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletionDate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true,options={"default":false})
     */
    private $curateAllowanceLimitReached;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $curateLimitStartDate;

    /**
     * @var Verification[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Verification",mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $verifications;

    /**
     * @var Profile
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Profile",mappedBy="user",cascade={"persist","remove"} ,fetch="EAGER")
     *
     * @Groups({"post","comment","message","basic_user","favorites"})
     *
     * @Assert\Valid()
     */
    private $profile;

    /**
     * @var ForgottenPassword[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\ForgottenPassword",mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $forgottenPasswords;

    /**
     * @var Follow[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Follow",mappedBy="follower", cascade={"remove"})
     */
    private $following;

    /**
     * @var Follow[]|null
     * @ORM\OneToMany(targetEntity="App\Entity\User\Follow",mappedBy="followed", cascade={"remove"})
     */
    private $followers;

    /**
     * @var Point[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Point",mappedBy="user", cascade={"remove"})
     */
    private $points;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $point;

    /**
     * @var Favorite[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Favorite",mappedBy="user", cascade={"remove"})
     */
    private $favorites;

    /**
     * @var Order[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Store\Order",mappedBy="customer",fetch="EXTRA_LAZY")
     */
    private $orders;

    /**
     * @var Post[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Post",mappedBy="user")
     */
    private $posts;

    /**
     * @var Notification[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Notification\Notification",mappedBy="receiver", cascade={"remove"})
     */
    private $notifications;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $acceptPolicies;

    /**
     * @var CurateComment[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\CurateComment", mappedBy="user", cascade={"remove"})
     */
    private $curateComments;

    /**
     * @var Album[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Album",mappedBy="user", cascade={"remove"})
     */
    private $albums;

    /**
     * @var PollVote[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Poll\PollVote", mappedBy="user", orphanRemoval=true)
     */
    private $votes;

    /**
     * @var Poll[]|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Poll\Poll", mappedBy="voters")
     * @ORM\JoinTable(name="t_poll_user")
     */
    private $polls;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\Settings", mappedBy="user", cascade={"persist", "remove" })
     */
    private $settings;

    /**
     * @var Balance|null
     * @ORM\OneToOne(targetEntity="App\Entity\User\Balance",mappedBy="user",cascade={"persist"})
     */
    private $balance;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->isVerified = false;
        $this->isBlocked = false;

        $this->setProfile(new Profile());
        $this->setSettings(new Settings());

        $this->verifications = new ArrayCollection();
        $this->forgottenPasswords = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->points = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->curateComments = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->polls = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->email;
    }

    /**
     * @return int|null
     *
     * @Groups({"basic_user","post","message"})
     */
    public function getId(): ?int
    {
        return parent::getId(); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     * @return $this
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    /**
     * @param string|null $facebookId
     * @return $this
     */
    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    /**
     * @param string|null $googleId
     * @return $this
     */
    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     * @return $this
     */
    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    /**
     * @param bool $isBlocked
     * @return $this
     */
    public function setIsBlocked(bool $isBlocked): self
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCurateAllowanceLimitReached(): ?bool
    {
        return $this->curateAllowanceLimitReached;
    }

    /**
     * @param bool|null $curateAllowanceLimitReached
     * @return $this
     */
    public function setCurateAllowanceLimitReached(?bool $curateAllowanceLimitReached): self
    {
        $this->curateAllowanceLimitReached = $curateAllowanceLimitReached;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCurateLimitStartDate(): ?\DateTimeInterface
    {
        return $this->curateLimitStartDate;
    }

    /**
     * @param DateTimeInterface|null $curateLimitStartDate
     * @return $this
     */
    public function setCurateLimitStartDate(?\DateTimeInterface $curateLimitStartDate): self
    {
        $this->curateLimitStartDate = $curateLimitStartDate;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getAcceptPolicies(): ?\DateTimeInterface
    {
        return $this->acceptPolicies;
    }

    /**
     * @param DateTimeInterface|null $acceptPolicies
     * @return $this
     */
    public function setAcceptPolicies(?\DateTimeInterface $acceptPolicies): self
    {
        $this->acceptPolicies = $acceptPolicies;

        return $this;
    }

    /**
     * @return Collection|Verification[]
     */
    public function getVerifications(): Collection
    {
        return $this->verifications;
    }

    /**
     * @param Verification $verification
     * @return $this
     */
    public function addVerification(Verification $verification): self
    {
        if (!$this->verifications->contains($verification)) {
            $this->verifications[] = $verification;
            $verification->setUser($this);
        }

        return $this;
    }

    /**
     * @param Verification $verification
     * @return $this
     */
    public function removeVerification(Verification $verification): self
    {
        if ($this->verifications->contains($verification)) {
            $this->verifications->removeElement($verification);
            // set the owning side to null (unless already changed)
            if ($verification->getUser() === $this) {
                $verification->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Profile|null
     */
    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile|null $profile
     * @return $this
     */
    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $profile ? null : $this;
        if ($profile->getUser() !== $newUser) {
            $profile->setUser($newUser);
        }

        return $this;
    }

    /**
     * @param Settings|null $settings
     * @return $this
     */
    public function setSettings(?Settings $settings): self
    {
        $this->settings = $settings;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $settings ? null : $this;
        if ($settings->getUser() !== $newUser) {
            $settings->setUser($newUser);
        }

        return $this;
    }

    /**
     * @param Settings|null $settings
     * @return $this
     */
    public function getSettings()
    {

        return $this->settings;
    }

    /**
     * @return Collection|ForgottenPassword[]
     */
    public function getForgottenPasswords(): Collection
    {
        return $this->forgottenPasswords;
    }

    /**
     * @param ForgottenPassword $forgottenPassword
     * @return $this
     */
    public function addForgottenPassword(ForgottenPassword $forgottenPassword): self
    {
        if (!$this->forgottenPasswords->contains($forgottenPassword)) {
            $this->forgottenPasswords[] = $forgottenPassword;
            $forgottenPassword->setUser($this);
        }

        return $this;
    }

    /**
     * @param ForgottenPassword $forgottenPassword
     * @return $this
     */
    public function removeForgottenPassword(ForgottenPassword $forgottenPassword): self
    {
        if ($this->forgottenPasswords->contains($forgottenPassword)) {
            $this->forgottenPasswords->removeElement($forgottenPassword);
            // set the owning side to null (unless already changed)
            if ($forgottenPassword->getUser() === $this) {
                $forgottenPassword->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Follow[]
     */
    public function getFollowing(): Collection
    {
        return $this->following;
    }

    /**
     * @param Follow $following
     * @return $this
     */
    public function addFollowing(Follow $following): self
    {
        if (!$this->following->contains($following)) {
            $this->following[] = $following;
            $following->setFollower($this);
        }

        return $this;
    }

    /**
     * @param Follow $following
     * @return $this
     */
    public function removeFollowing(Follow $following): self
    {
        if ($this->following->contains($following)) {
            $this->following->removeElement($following);
            // set the owning side to null (unless already changed)
            if ($following->getFollower() === $this) {
                $following->setFollower(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Follow[]
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    /**
     * @param Follow $follower
     * @return $this
     */
    public function addFollower(Follow $follower): self
    {
        if (!$this->followers->contains($follower)) {
            $this->followers[] = $follower;
            $follower->setFollowed($this);
        }

        return $this;
    }

    /**
     * @param Follow $follower
     * @return $this
     */
    public function removeFollower(Follow $follower): self
    {
        if ($this->followers->contains($follower)) {
            $this->followers->removeElement($follower);
            // set the owning side to null (unless already changed)
            if ($follower->getFollowed() === $this) {
                $follower->setFollowed(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    /**
     * @param Point $point
     * @return $this
     */
    public function addPoint(Point $point): self
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setUser($this);
        }

        return $this;
    }

    /**
     * @param Point $point
     * @return $this
     */
    public function removePoint(Point $point): self
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            // set the owning side to null (unless already changed)
            if ($point->getUser() === $this) {
                $point->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Favorite[]
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    /**
     * @param Favorite $favorite
     * @return $this
     */
    public function addFavorite(Favorite $favorite): self
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites[] = $favorite;
            $favorite->setUser($this);
        }

        return $this;
    }

    /**
     * @param Favorite $favorite
     * @return $this
     */
    public function removeFavorite(Favorite $favorite): self
    {
        if ($this->favorites->contains($favorite)) {
            $this->favorites->removeElement($favorite);
            // set the owning side to null (unless already changed)
            if ($favorite->getUser() === $this) {
                $favorite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCustomer($this);
        }

        return $this;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getCustomer() === $this) {
                $order->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUser($this);
        }

        return $this;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUser() === $this) {
                $post->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    /**
     * @param Notification $notification
     * @return $this
     */
    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setReceiver($this);
        }

        return $this;
    }

    /**
     * @param Notification $notification
     * @return $this
     */
    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getReceiver() === $this) {
                $notification->setReceiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CurateComment[]
     */
    public function getCurateComments(): Collection
    {
        return $this->curateComments;
    }

    /**
     * @param CurateComment $curateComment
     * @return $this
     */
    public function addCurateComment(CurateComment $curateComment): self
    {
        if (!$this->curateComments->contains($curateComment)) {
            $this->curateComments[] = $curateComment;
            $curateComment->setUser($this);
        }

        return $this;
    }

    /**
     * @param CurateComment $curateComment
     * @return $this
     */
    public function removeCurateComment(CurateComment $curateComment): self
    {
        if ($this->curateComments->contains($curateComment)) {
            $this->curateComments->removeElement($curateComment);
            // set the owning side to null (unless already changed)
            if ($curateComment->getUser() === $this) {
                $curateComment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    /**
     * @param Album $album
     * @return $this
     */
    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setUser($this);
        }

        return $this;
    }

    /**
     * @param Album $album
     * @return $this
     */
    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getUser() === $this) {
                $album->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PollVote[]
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    /**
     * @param PollVote $vote
     * @return $this
     */
    public function addVote(PollVote $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes[] = $vote;
            $vote->setUser($this);
        }

        return $this;
    }

    /**
     * @param PollVote $vote
     * @return $this
     */
    public function removeVote(PollVote $vote): self
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
            // set the owning side to null (unless already changed)
            if ($vote->getUser() === $this) {
                $vote->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Poll[]
     */
    public function getPolls(): Collection
    {
        return $this->polls;
    }

    /**
     * @param Poll $poll
     * @return $this
     */
    public function addPoll(Poll $poll): self
    {
        if (!$this->polls->contains($poll)) {
            $this->polls[] = $poll;
            $poll->addVoter($this);
        }

        return $this;
    }

    /**
     * @param Poll $poll
     * @return $this
     */
    public function removePoll(Poll $poll): self
    {
        if ($this->polls->contains($poll)) {
            $this->polls->removeElement($poll);
            $poll->removeVoter($this);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPoint(): ?int
    {
        return $this->point;
    }

    /**
     * @param int|null $point
     */
    public function setPoint(?int $point): void
    {
        $this->point = $point;
    }

    /**
     * @return Balance|null
     */
    public function getBalance(): ?Balance
    {
        return $this->balance;
    }

    /**
     * @param Balance|null $balance
     */
    public function setBalance(?Balance $balance): void
    {
        $this->balance = $balance;
    }


    /**
     * @return DateTimeInterface|null
     */
    public function getDeletionDate(): ?DateTimeInterface
    {
        return $this->deletionDate;
    }

    /**
     * @param DateTimeInterface|null $deletionDate
     */
    public function setDeletionDate(?DateTimeInterface $deletionDate): void
    {
        $this->deletionDate = $deletionDate;
    }

}
