<?php

namespace App\Entity\User;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FollowRepository")
 * @ORM\Table(name="t_followers",uniqueConstraints={@UniqueConstraint(name="follower_followed",columns={"follower_id","followed_id"})})
 */
class Follow implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="following")
     */
    private $follower;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="followers")
     */
    private $followed;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFollower()
    {
        return $this->follower;
    }

    /**
     * @param mixed $follower
     */
    public function setFollower($follower): void
    {
        $this->follower = $follower;
    }

    /**
     * @return mixed
     */
    public function getFollowed()
    {
        return $this->followed;
    }

    /**
     * @param mixed $followed
     */
    public function setFollowed($followed): void
    {
        $this->followed = $followed;
    }

    /**
     */
    public function followed()
    {
        return true;
    }
}
