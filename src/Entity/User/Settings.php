<?php

namespace App\Entity\User;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingsRepository")
 * @ORM\Table(name="t_settings")
 */
class Settings implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     *
     * @Groups({"settings"})
     */
    private $isPortfolioVisible;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     *
     * @Groups({"settings"})
     */
    private $isEmailNotificationAllowed;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     *
     * @Groups({"settings"})
     */
    private $isWebNotificationAllowed;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     *
     * @Groups({"settings"})
     */
    private $isPushNotificationAllowed;

    /**
     * Send nudes ???
     * @ORM\Column(type="boolean",options={"default": false},nullable=true)
     *
     * @Groups({"settings"})
     */
    private $showNudes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User", inversedBy="settings")
     */
    private $user;

    public function __construct(?User $user = null)
    {
        $this->user = $user;
        $this->isPortfolioVisible = true;
        $this->isEmailNotificationAllowed = true;
        $this->isWebNotificationAllowed = true;
        $this->isPushNotificationAllowed = true;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsPortfolioVisible()
    {
        return $this->isPortfolioVisible;
    }

    /**
     * @param mixed $isPortfolioVisible
     */
    public function setIsPortfolioVisible($isPortfolioVisible): void
    {
        $this->isPortfolioVisible = $isPortfolioVisible;
    }


    /**
     * @return mixed
     */
    public function getIsEmailNotificationAllowed()
    {
        return $this->isEmailNotificationAllowed;
    }

    /**
     * @param mixed $isEmailNotificationAllowed
     */
    public function setIsEmailNotificationAllowed($isEmailNotificationAllowed): void
    {
        $this->isEmailNotificationAllowed = $isEmailNotificationAllowed;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIsWebNotificationAllowed()
    {
        return $this->isWebNotificationAllowed;
    }

    /**
     * @param mixed $isWebNotificationAllowed
     */
    public function setIsWebNotificationAllowed($isWebNotificationAllowed): void
    {
        $this->isWebNotificationAllowed = $isWebNotificationAllowed;
    }

    /**
     * @return mixed
     */
    public function getIsPushNotificationAllowed()
    {
        return $this->isPushNotificationAllowed;
    }

    /**
     * @param mixed $isPushNotificationAllowed
     */
    public function setIsPushNotificationAllowed($isPushNotificationAllowed): void
    {
        $this->isPushNotificationAllowed = $isPushNotificationAllowed;
    }

    /**
     * @return mixed
     */
    public function getShowNudes()
    {
        return $this->showNudes;
    }

    /**
     * @param mixed $showNudes
     */
    public function setShowNudes($showNudes): void
    {
        $this->showNudes = $showNudes;
    }
}
