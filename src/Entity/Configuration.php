<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 * @ORM\Table(name="t_configurations")
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $country;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $zip;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $county;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $vatNumber;

    /**
     * @var string|null
     * @ORM\Column(type="float",scale=2,precision=10)
     */
    private $tax;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var float|null
     * @ORM\Column(type="float",scale=2,precision=10)
     */
    private $shipping;

    /**
     * @var float|null
     * @ORM\Column(type="float",scale=2,precision=10)
     */
    private $minCommission;

    /**
     * @var float|null
     * @ORM\Column(type="float",scale=2,precision=10)
     */
    private $maxCommission;

    /**
     * @var int|null
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $priceUpdateInterval;

    /**
     * @var bool
     * @ORM\Column(type="boolean",options={"default":false},nullable=true)
     */
    private $allowShopping;

    /**
     * @ORM\Column(type="boolean",options={"default":false},nullable=true)
     */
    private $allowEnter;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $popularHoursAgo;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $publishTimespan;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowPrintChange;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minPrintSize;

    /**
     * @var float|null
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productCommission;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $curatorPostBlockPeriod;

    /**
     * @var int|null
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $postDeletionInterval;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceWillChangeInterval;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     */
    public function setZip(?string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     * @param string|null $county
     */
    public function setCounty(?string $county): void
    {
        $this->county = $county;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    /**
     * @param string|null $vatNumber
     */
    public function setVatNumber(?string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return string|null
     */
    public function getTax(): ?string
    {
        return $this->tax;
    }

    /**
     * @param string|null $tax
     */
    public function setTax(?string $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return float|null
     */
    public function getShipping(): ?float
    {
        return $this->shipping;
    }

    /**
     * @param float|null $shipping
     */
    public function setShipping(?float $shipping): void
    {
        $this->shipping = $shipping;
    }

    /**
     * @return float|null
     */
    public function getMaxCommission(): ?float
    {
        return $this->maxCommission;
    }

    /**
     * @param float|null $maxCommission
     */
    public function setMaxCommission(?float $maxCommission): void
    {
        $this->maxCommission = $maxCommission;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function getAllowShopping(): bool
    {
        return $this->allowShopping;
    }

    /**
     * @param bool $allowShopping
     */
    public function setAllowShopping(bool $allowShopping): void
    {
        $this->allowShopping = $allowShopping;
    }

    /**
     * @return mixed
     */
    public function getAllowEnter()
    {
        return $this->allowEnter;
    }

    /**
     * @param mixed $allowEnter
     */
    public function setAllowEnter($allowEnter): void
    {
        $this->allowEnter = $allowEnter;
    }

    /**
     * @return mixed
     */
    public function getPopularHoursAgo()
    {
        return $this->popularHoursAgo;
    }

    /**
     * @param $popularHoursAgo
     */
    public function setPopularHoursAgo($popularHoursAgo): void
    {
        $this->popularHoursAgo = $popularHoursAgo;
    }

    /**
     * @return mixed
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param mixed $contactEmail
     */
    public function setContactEmail($contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return mixed
     */
    public function getPublishTimespan()
    {
        return $this->publishTimespan;
    }

    /**
     * @param mixed $publishTimespan
     */
    public function setPublishTimespan($publishTimespan): void
    {
        $this->publishTimespan = $publishTimespan;
    }

    /**
     * @return float|null
     */
    public function getMinCommission(): ?float
    {
        return $this->minCommission;
    }

    /**
     * @param float|null $minCommission
     */
    public function setMinCommission(?float $minCommission): void
    {
        $this->minCommission = $minCommission;
    }

    /**
     * @return int
     */
    public function getPriceUpdateInterval(): ?int
    {
        return $this->priceUpdateInterval;
    }

    /**
     * @param int $priceUpdateInterval
     */
    public function setPriceUpdateInterval(?int $priceUpdateInterval): void
    {
        $this->priceUpdateInterval = $priceUpdateInterval;
    }

    /**
     * @return bool|null
     */
    public function getAllowPrintChange(): ?bool
    {
        return $this->allowPrintChange;
    }

    /**
     * @param bool|null $allowPrintChange
     */
    public function setAllowPrintChange(?bool $allowPrintChange): void
    {
        $this->allowPrintChange = $allowPrintChange;
    }

    /**
     * @return int|null
     */
    public function getMinPrintSize(): ?int
    {
        return $this->minPrintSize;
    }

    /**
     * @param int|null $minPrintSize
     */
    public function setMinPrintSize(?int $minPrintSize): void
    {
        $this->minPrintSize = $minPrintSize;
    }

    /**
     * @return int|null
     */
    public function getPriceWillChangeInterval(): ?int
    {
        return $this->priceWillChangeInterval;
    }

    /**
     * @param int|null $priceWillChangeInterval
     */
    public function setPriceWillChangeInterval(?int $priceWillChangeInterval): void
    {
        $this->priceWillChangeInterval = $priceWillChangeInterval;
    }

    /**
     * @return int|null
     */
    public function getCuratorPostBlockPeriod(): ?int
    {
        return $this->curatorPostBlockPeriod;
    }

    /**
     * @param int|null $curatorPostBlockPeriod
     */
    public function setCuratorPostBlockPeriod(?int $curatorPostBlockPeriod): void
    {
        $this->curatorPostBlockPeriod = $curatorPostBlockPeriod;
    }

    /**
     * @return int|null
     */
    public function getPostDeletionInterval(): ?int
    {
        return $this->postDeletionInterval;
    }

    /**
     * @param int|null $postDeletionInterval
     */
    public function setPostDeletionInterval(?int $postDeletionInterval): void
    {
        $this->postDeletionInterval = $postDeletionInterval;
    }

    /**
     * @return float|null
     */
    public function getProductCommission(): ?float
    {
        return $this->productCommission;
    }

    /**
     * @param float|null $productCommission
     */
    public function setProductCommission(?float $productCommission): void
    {
        $this->productCommission = $productCommission;
    }

}
