<?php

namespace App\Entity\Photo;

use App\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_images")
 */
class Image extends BaseEntity
{

    /**
     * @var UploadedFile|null
     *
     * @Assert\Image(mimeTypes={"image/jpeg"},
     *      mimeTypesMessage="Only JPEG format is allowed!",
     *      maxSize="31M",
     *      minPixels="500000")
     */
    private $file;

    /**
     * @var mixed
     * @ORM\Column(type="string",nullable=true)
     */
    private $print;

    /**
     * @var mixed
     * @ORM\Column(type="string",nullable=true)
     */
    private $printThumbnail;

    /**
     * @var mixed
     * @ORM\Column(type="string",nullable=true)
     */
    private $social;

    /**
     * @var mixed
     *
     * @ORM\Column(type="string",nullable=true)
     *
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"post","carT"})
     */
    private $thumbnail;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"post"})
     */
    private $large;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"post"})
     */
    private $small;

    /**
     * @var int|null
     * @ORM\Column(type="integer",nullable=true)
     */
    private $width;

    /**
     * @var int|null
     * @ORM\Column(type="integer",nullable=true)
     */
    private $height;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $camera;

    /**
     * @var string|null
     * @ORM\Column(type="integer",nullable=true)
     */
    private $size;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $lens;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $focalLength;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $shutterSpeed;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $ISO;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $aperture;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $exposure;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $flash;

    /**
     * @var string|null
     * @ORM\Column(type="string",nullable=true)
     */
    private $filter;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $tripod;

    /**
     * @var Post
     * @ORM\OneToOne(targetEntity="App\Entity\Photo\Post",mappedBy="image")
     */
    private $post;

    /**
     * @var Orientation
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Orientation")
     */
    private $orientation;

    /**
     * @return string|null
     */
    public function getPrint(): ?string
    {
        return $this->print;
    }

    /**
     * @param string|null $print
     * @return $this
     */
    public function setPrint($print): self
    {
        $this->print = $print;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string|null $thumbnail
     * @return $this
     */
    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLarge(): ?string
    {
        return $this->large;
    }

    /**
     * @param string|null $large
     * @return $this
     */
    public function setLarge(?string $large): self
    {
        $this->large = $large;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmall(): ?string
    {
        return $this->small;
    }

    /**
     * @param string|null $small
     * @return $this
     */
    public function setSmall(?string $small): self
    {
        $this->small = $small;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     * @return $this
     */
    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     * @return $this
     */
    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCamera(): ?string
    {
        return $this->camera;
    }

    /**
     * @param string|null $camera
     * @return $this
     */
    public function setCamera(?string $camera): self
    {
        $this->camera = $camera;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int|null $size
     * @return $this
     */
    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLens(): ?string
    {
        return $this->lens;
    }

    /**
     * @param string|null $lens
     * @return $this
     */
    public function setLens(?string $lens): self
    {
        $this->lens = $lens;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFocalLength(): ?string
    {
        return $this->focalLength;
    }

    /**
     * @param string|null $focalLength
     * @return $this
     */
    public function setFocalLength(?string $focalLength): self
    {
        $this->focalLength = $focalLength;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShutterSpeed(): ?string
    {
        return $this->shutterSpeed;
    }

    /**
     * @param string|null $shutterSpeed
     * @return $this
     */
    public function setShutterSpeed(?string $shutterSpeed): self
    {
        $this->shutterSpeed = $shutterSpeed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getISO(): ?string
    {
        return $this->ISO;
    }

    /**
     * @param string|null $ISO
     * @return $this
     */
    public function setISO(?string $ISO): self
    {
        $this->ISO = $ISO;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAperture(): ?string
    {
        return $this->aperture;
    }

    /**
     * @param string|null $aperture
     * @return $this
     */
    public function setAperture(?string $aperture): self
    {
        $this->aperture = $aperture;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExposure(): ?string
    {
        return $this->exposure;
    }

    /**
     * @param string|null $exposure
     * @return $this
     */
    public function setExposure(?string $exposure): self
    {
        $this->exposure = $exposure;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFlash(): ?string
    {
        return $this->flash;
    }

    /**
     * @param string|null $flash
     * @return $this
     */
    public function setFlash(?string $flash): self
    {
        $this->flash = $flash;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilter(): ?string
    {
        return $this->filter;
    }

    /**
     * @param string|null $filter
     * @return $this
     */
    public function setFilter(?string $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTripod(): ?string
    {
        return $this->tripod;
    }

    /**
     * @param string|null $tripod
     * @return $this
     */
    public function setTripod(?string $tripod): self
    {
        $this->tripod = $tripod;

        return $this;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post|null $post
     * @return $this
     */
    public function setPost(?Post $post): self
    {
        $this->post = $post;

        // set (or unset) the owning side of the relation if necessary
        $newImage = null === $post ? null : $this;
        if ($post->getImage() !== $newImage) {
            $post->setImage($newImage);
        }

        return $this;
    }

    /**
     * @return Orientation|null
     */
    public function getOrientation(): ?Orientation
    {
        return $this->orientation;
    }

    /**
     * @param Orientation|null $orientation
     * @return $this
     */
    public function setOrientation(?Orientation $orientation): self
    {
        $this->orientation = $orientation;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getPrintThumbnail()
    {
        return $this->printThumbnail;
    }

    /**
     * @param mixed $printThumbnail
     */
    public function setPrintThumbnail($printThumbnail): void
    {
        $this->printThumbnail = $printThumbnail;
    }

    /**
     * @return mixed
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param mixed $social
     */
    public function setSocial($social): void
    {
        $this->social = $social;
    }

}
