<?php

namespace App\Entity\Photo;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(name="t_comments")
 */
class Comment extends BaseEntity implements EntityInterface
{
    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"comment","create"})
     *
     * @Assert\NotBlank(groups={"create"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"comment","create"})
     *
     * @Assert\NotBlank(groups={"create"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Post", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"comment","create"})
     *
     * @Assert\NotBlank(groups={"create"})
     */
    private $post;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Reply",mappedBy="comment",cascade={"remove"})
     *
     * @Groups({"comment"})
     */
    private $replies;

    /**
     * @Groups({"comment"})
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Comment
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Comment
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post|null $post
     * @return Comment
     */
    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * @param mixed $replies
     */
    public function setReplies($replies): void
    {
        $this->replies = $replies;
    }

    /**
     * @Groups({"comment"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }
}
