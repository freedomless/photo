<?php

namespace App\Entity\Photo;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_replies")
 */
class Reply extends BaseEntity implements EntityInterface
{
    /**
     * @ORM\Column(type="text")
     * @Groups({"comment","create","update"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"comment","create","update"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Comment",inversedBy="replies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comment;

    /**
     * @return int|null
     * @Groups({"comment"})
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @Groups({"comment"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }
}
