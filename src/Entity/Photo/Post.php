<?php

namespace App\Entity\Photo;

use App\Entity\BaseEntity;
use App\Entity\PostUpdateLog;
use App\Entity\Store\Product;
use App\Entity\User\User;
use App\Enum\PostTypeEnum;
use App\Enum\ProductStatusEnum;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ORM\Table(name="t_posts")
 * @ORM\HasLifecycleCallbacks()
 */
class Post extends BaseEntity
{

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $oldId;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string",nullable=true)
     *
     * @Groups({"post"})
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title", "id"})
     *
     * @ORM\Column(length=128, unique=true,nullable=true)
     *
     * @Groups({"post","comment"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text",nullable=true)
     *
     * @Groups({"post"})
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean",options={"default": false})
     *
     * @Groups({"post"})
     */
    private $isNude;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean",nullable=true,options={"default": false})
     *
     * @Groups({"post"})
     */
    private $isGreyscale;

    /**
     * @var int|null
     *
     * @ORM\Column(type="smallint")
     *
     * @Groups({"post"})
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(type="integer",options={"default": 0})
     *
     * @Groups({"post"})
     */
    private $views;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @Groups({"post"})
     */
    private $sentForCurateDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="posts")
     *
     * @Groups({"post"})
     */
    private $user;

    /**
     * @var Image
     *
     * @Assert\Valid()
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Photo\Image",inversedBy="post",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"post","cart"})
     */
    private $image;

    /**
     * @var Category|null
     *
     * @Assert\NotNull(message="Category is required", groups={"create","update"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Category",inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"post"})
     */
    private $category;

    /**
     * @var Curate[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Curate",mappedBy="posts",cascade={"remove"})
     */
    private $curates;

    /**
     * @var Vote[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Vote",mappedBy="post",cascade={"remove"})
     */
    private $votes;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"post"})
     */
    private $curatedAt;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"post"})
     */
    private $showDate;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     */
    private $publisher;

    /**
     * @var CurateComment[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\CurateComment", mappedBy="post",cascade={"remove"})
     */
    private $curateComments;

    /**
     * @var PostUpdateLog[]|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PostUpdateLog", mappedBy="post",cascade={"remove"})
     */
    private $postUpdateLogs;

    /**
     * @var Award[]|null
     *
     * @ORM\OneToMany(targetEntity="Award",mappedBy="post", cascade={"remove"})
     *
     * @Groups({"award"})
     */
    private $awards;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $hasNotification;

    /**
     * @var Product|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Store\Product", mappedBy="post", cascade={"remove", "persist"})
     *
     * @Groups({"index"})
     */
    private $product;

    //Series additional

    /**
     * @var self|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Post", inversedBy="children", fetch="EAGER")
     *
     * @Groups({"parent"})
     */
    private $parent;

    /**
     * @var self|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Post", mappedBy="parent", cascade={"persist"})
     *
     * @Groups({"series"})
     *
     * @ORM\OrderBy({"position":"ASC"})
     */
    private $children;

    /**
     * @var self|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Post")
     */
    private $cover;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @Groups({"post"})
     */
    private $type;

    /**
     * @var Tag[]|null
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo\Tag",inversedBy="posts")
     * @ORM\JoinTable(name="t_post_tag")
     * @Groups({"tags"})
     */
    private $tags;

    /**
     * @var Favorite[]|null
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Favorite",mappedBy="post", cascade={"remove"})
     *
     * @ORM\OrderBy({"id": "DESC"})
     *
     * @Groups({"tags"})
     */
    private $favorites;

    /**
     * @var int|null
     *
     * @ORM\Column(type="smallint",nullable=true)
     */
    private $position;

    /**
     * @var SelectionSeries|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Photo\SelectionSeries", mappedBy="post", fetch="EXTRA_LAZY", cascade={"remove"})
     */
    private $selectionSeries;

    /**
     * @var Comment[]|null
     * @ORM\OneToMany(targetEntity="App\Entity\Photo\Comment", mappedBy="post", cascade={"remove"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo\Album",mappedBy="posts")
     */
    private $albums;

    /**
     * @ORM\OneToMany(targetEntity=PostBlock::class, mappedBy="post", fetch="EXTRA_LAZY")
     */
    private $blocks;

    /**
     * @ORM\ManyToMany(targetEntity=RejectReason::class, mappedBy="post", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="t_post_reject_reasons")
     */
    private $rejectReasons;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"post"})
     */
    private $deletionDate;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->isNude = false;
        $this->isGreyscale = false;
        $this->hasNotification = false;
        $this->status = 0;
        $this->views = 0;
        $this->type = PostTypeEnum::SINGLE;
        $this->image = new Image();

        $this->curates = new ArrayCollection();

        $this->curateComments = new ArrayCollection();

        $this->postUpdateLogs = new ArrayCollection();

        $this->product = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->awards = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->blocks = new ArrayCollection();
        $this->rejectReasons = new ArrayCollection();

    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ucfirst($this->image->getThumbnail());
    }

    /**
     *
     * a* @return int|null
     * @Groups({"comment","post"})
     */
    public function getId(): ?int
    {
        return parent::getId(); // TODO: Change the autogenerated stub
    }

    /**
     * @return int|null
     */
    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    /**
     * @param int|null $oldId
     */
    public function setOldId(?int $oldId): void
    {
        $this->oldId = $oldId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Post
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * @param Image|null $image
     * @return Post
     */
    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurates()
    {
        return $this->curates;
    }

    /**
     * @param mixed $curates
     */
    public function setCurates($curates): void
    {
        $this->curates = $curates;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views): void
    {
        $this->views = $views;
    }

    /**
     * @return mixed
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param mixed $votes
     */
    public function setVotes($votes): void
    {
        $this->votes = $votes;
    }

    /**
     * @return mixed
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param mixed $publisher
     */
    public function setPublisher($publisher): void
    {
        $this->publisher = $publisher;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getIsNude(): ?bool
    {
        return $this->isNude;
    }

    /**
     * @param mixed $isNude
     */
    public function setIsNude(?bool $isNude): void
    {
        $this->isNude = $isNude;
    }

    /**
     * @return bool
     */
    public function isGreyscale(): bool
    {
        return $this->isGreyscale;
    }

    /**
     * @param bool $isGreyscale
     */
    public function setIsGreyscale(bool $isGreyscale): void
    {
        $this->isGreyscale = $isGreyscale;
    }

    /**
     * @return mixed
     */
    public function getShowDate()
    {
        return $this->showDate;
    }

    /**
     * @param mixed $showDate
     */
    public function setShowDate($showDate): void
    {
        $this->showDate = $showDate;
    }

    /**
     * @return Collection|CurateComment[]
     */
    public function getCurateComments(): Collection
    {
        return $this->curateComments;
    }

    /**
     * @param CurateComment $curateComment
     * @return Post
     */
    public function addCurateComment(CurateComment $curateComment): self
    {
        if (!$this->curateComments->contains($curateComment)) {
            $this->curateComments[] = $curateComment;
            $curateComment->setPost($this);
        }

        return $this;
    }

    /**
     * @param CurateComment $curateComment
     * @return Post
     */
    public function removeCurateComment(CurateComment $curateComment): self
    {
        if ($this->curateComments->contains($curateComment)) {
            $this->curateComments->removeElement($curateComment);
            // set the owning side to null (unless already changed)
            if ($curateComment->getPost() === $this) {
                $curateComment->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSentForCurateDate()
    {
        return $this->sentForCurateDate;
    }

    /**
     * @param mixed $sentForCurateDate
     */
    public function setSentForCurateDate($sentForCurateDate): void
    {
        $this->sentForCurateDate = $sentForCurateDate;
    }

    /**
     * @return Collection|PostUpdateLog[]
     */
    public function getPostUpdateLogs(): Collection
    {
        return $this->postUpdateLogs;
    }

    /**
     * @param PostUpdateLog $postUpdateLog
     * @return Post
     */
    public function addPostUpdateLog(PostUpdateLog $postUpdateLog): self
    {
        if (!$this->postUpdateLogs->contains($postUpdateLog)) {
            $this->postUpdateLogs[] = $postUpdateLog;
            $postUpdateLog->setPost($this);
        }

        return $this;
    }

    /**
     * @param PostUpdateLog $postUpdateLog
     * @return Post
     */
    public function removePostUpdateLog(PostUpdateLog $postUpdateLog): self
    {
        if ($this->postUpdateLogs->contains($postUpdateLog)) {
            $this->postUpdateLogs->removeElement($postUpdateLog);
            // set the owning side to null (unless already changed)
            if ($postUpdateLog->getPost() === $this) {
                $postUpdateLog->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * @param mixed $awards
     */
    public function setAwards($awards): void
    {
        $this->awards = $awards;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCuratedAt(): ?DateTimeInterface
    {
        return $this->curatedAt;
    }

    /**
     * @param DateTimeInterface|null $curatedAt
     * @return $this
     */
    public function setCuratedAt(?DateTimeInterface $curatedAt): self
    {
        $this->curatedAt = $curatedAt;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHasNotification(): ?bool
    {
        return $this->hasNotification;
    }

    /**
     * @param bool|null $hasNotification
     */
    public function setHasNotification(?bool $hasNotification): void
    {
        $this->hasNotification = $hasNotification;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product ? ($this->product->last() ?: null) : null;
    }

    /**
     * @param Product|null $product
     */
    public function setProduct(?Collection $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return $this|null
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * @param Post|null $parent
     * @return $this
     */
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     */
    public function setChildren(Collection $children): void
    {
        $this->children = $children;
    }

    /**
     * @param Post $child
     * @return $this
     */
    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @param Post $child
     * @return $this
     */
    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Post|null
     */
    public function getCover(): ?Post
    {
        return $this->cover;
    }

    /**
     * @param Post|null $cover
     */
    public function setCover(?Post $cover): void
    {
        $this->cover = $cover;
    }

    /**
     * @param Curate $curate
     * @return $this
     */
    public function addCurate(Curate $curate): self
    {
        if (!$this->curates->contains($curate)) {
            $this->curates[] = $curate;
            $curate->setPosts($this);
        }

        return $this;
    }

    /**
     * @param Curate $curate
     * @return $this
     */
    public function removeCurate(Curate $curate): self
    {
        if ($this->curates->contains($curate)) {
            $this->curates->removeElement($curate);
            // set the owning side to null (unless already changed)
            if ($curate->getPosts() === $this) {
                $curate->setPosts(null);
            }
        }

        return $this;
    }

    /**
     * @param Vote $vote
     * @return $this
     */
    public function addVote(Vote $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes[] = $vote;
            $vote->setPost($this);
        }

        return $this;
    }

    /**
     * @param Vote $vote
     * @return $this
     */
    public function removeVote(Vote $vote): self
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
            // set the owning side to null (unless already changed)
            if ($vote->getPost() === $this) {
                $vote->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @param Award $award
     * @return $this
     */
    public function addAward(Award $award): self
    {
        if (!$this->awards->contains($award)) {
            $this->awards[] = $award;
            $award->setPost($this);
        }

        return $this;
    }

    /**
     * @param Award $award
     * @return $this
     */
    public function removeAward(Award $award): self
    {
        if ($this->awards->contains($award)) {
            $this->awards->removeElement($award);
            // set the owning side to null (unless already changed)
            if ($award->getPost() === $this) {
                $award->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setPost($this);
        }

        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getPost() === $this) {
                $product->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @param Collection $tags
     */
    public function setTags(Collection $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isRejectable()
    {
        return $this->status < 2 || $this->showDate === null || $this->showDate > new \DateTime();
    }

    /**
     * @Groups({"post"})
     * @return bool
     */
    public function isForSale()
    {
        return $this->getProduct() && $this->getProduct()->getStatus() === ProductStatusEnum::PUBLISHED ? $this->getProduct()->getId() : false;
    }

    /**
     * @Groups({"post"})
     */
    public function isSeries()
    {
        return $this->parent !== null && $this->parent->getStatus() > 2 ? $this->parent->getId() : false;
    }

    /**
     * @return Favorite[]|null
     */
    public function getFavorites(): ?Collection
    {
        return $this->favorites;
    }

    /**
     * @param Collection|null $favorites
     */
    public function setFavorites(?Collection $favorites): void
    {
        $this->favorites = $favorites;
    }

    /**
     * @return int|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return SelectionSeries|null
     */
    public function getSelectionSeries(): ?SelectionSeries
    {
        return $this->selectionSeries;
    }

    /**
     * @param SelectionSeries|null $selectionSeries
     */
    public function setSelectionSeries(?SelectionSeries $selectionSeries): void
    {
        $this->selectionSeries = $selectionSeries;
    }

    /**
     * @return Collection|PostBlock[]
     */
    public function getBlocks(): Collection
    {
        return $this->blocks;
    }

    /**
     * @param PostBlock $block
     * @return $this
     */
    public function addBlock(PostBlock $block): self
    {
        if (!$this->blocks->contains($block)) {
            $this->blocks[] = $block;
            $block->setPost($this);
        }

        return $this;
    }

    /**
     * @param PostBlock $block
     * @return $this
     */
    public function removeBlock(PostBlock $block): self
    {
        if ($this->blocks->contains($block)) {
            $this->blocks->removeElement($block);
            // set the owning side to null (unless already changed)
            if ($block->getPost() === $this) {
                $block->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RejectReason[]
     */
    public function getRejectReasons(): Collection
    {
        return $this->rejectReasons;
    }

    /**
     * @param RejectReason $rejectReason
     * @return $this
     */
    public function addRejectReason(RejectReason $rejectReason): self
    {
        if (!$this->rejectReasons->contains($rejectReason)) {
            $this->rejectReasons[] = $rejectReason;
            $rejectReason->addPost($this);
        }

        return $this;
    }

    /**
     * @param RejectReason $rejectReason
     * @return $this
     */
    public function removeRejectReason(RejectReason $rejectReason): self
    {
        if ($this->rejectReasons->contains($rejectReason)) {
            $this->rejectReasons->removeElement($rejectReason);
            $rejectReason->removePost($this);
        }

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeletionDate(): ?DateTimeInterface
    {
        return $this->deletionDate;
    }

    /**
     * @param DateTimeInterface|null $deletionDate
     */
    public function setDeletionDate(?DateTimeInterface $deletionDate): void
    {
        $this->deletionDate = $deletionDate;
    }

    /**
     * @return float|int
     *
     * @Groups({"post","store","cart"})
     * @SerializedName("opposite")
     */
    public function getOpposite()
    {
        if($this->getType() > 1){
            return 0;
        }

        $image = $this->getImage();

        $width = $image->getOrientation()->getId() === 1 ? $image->getHeight() : $image->getWidth();

        $height = $image->getOrientation()->getId() === 2 ? $image->getHeight() : $image->getWidth();

        return$height / $width;
    }
}
