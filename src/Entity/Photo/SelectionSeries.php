<?php

namespace App\Entity\Photo;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SelectionSeriesRepository")
 * @ORM\Table(name="t_selection_series")
 */
class SelectionSeries
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"post"})
     */
    private $id;

    /**
     * @var Post|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Photo\Post",inversedBy="selectionSeries")
     * @Groups({"post"})
     */
    private $post;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="3")
     * @Groups({"post"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"post"})
     */
    private $cover;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visibility;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $hasNotification;

    /**
     * SelectionSeries constructor.
     */
    public function __construct()
    {
        $this->hasNotification = false;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post|null $post
     * @return SelectionSeries
     */
    public function setPost(?Post $post): SelectionSeries
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCover(): ?string
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     * @return $this
     */
    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisibility(): ?bool
    {
        return $this->visibility;
    }

    /**
     * @param bool $visibility
     * @return $this
     */
    public function setVisibility(bool $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHasNotification(): ?bool
    {
        return $this->hasNotification;
    }

    /**
     * @param bool $hasNotification
     * @return $this
     */
    public function setHasNotification(bool $hasNotification): self
    {
        $this->hasNotification = $hasNotification;

        return $this;
    }
}
