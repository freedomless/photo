<?php

namespace App\Entity\Photo;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AwardRepository")
 * @ORM\Table(name="t_awards", uniqueConstraints={@UniqueConstraint(name="unique_post_type", columns={"post_id", "type_id"})})
 */
class Award extends BaseEntity implements EntityInterface
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Photo\Post",inversedBy="awards")
     *
     * @Groups({"post"})
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity="AwardType",inversedBy="awards")
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasNotification;

    /**
     * @return int|null
     * @Groups({"award","post"})
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @return mixed|Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @Groups({"award","post"})
     *
     * @param bool $format
     * @return mixed
     */
    public function getCreatedAt(bool $format = true)
    {
        return $format ? $this->createdAt->format('Y-m-d H:i:s') : $this->createdAt;
    }

    /**
     * @Groups({"award","post"})
     *
     * @return string
     */
    public function getUrlizedDate(): string
    {
        return strtolower($this->createdAt->format('d-M-Y'));
    }

    /**
     * @return bool
     */
    public function isHasNotification(): bool
    {
        return $this->hasNotification;
    }

    /**
     * @param bool $hasNotification
     */
    public function setHasNotification(bool $hasNotification): void
    {
        $this->hasNotification = $hasNotification;
    }
}
