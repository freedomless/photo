<?php

namespace App\Entity\Poll;

use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_poll_vote")
 * @ORM\HasLifecycleCallbacks()
 */
class PollVote
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PollChoice", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $choice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return PollChoice|null
     */
    public function getChoice(): ?PollChoice
    {
        return $this->choice;
    }

    /**
     * @param PollChoice|null $choice
     * @return $this
     */
    public function setChoice(?PollChoice $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function prePersist(): void
    {
        $this->createdAt = $this->createdAt ?? new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate()
     * @throws \Exception
     */
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
