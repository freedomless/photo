<?php

namespace App\Entity\Poll;

use App\Entity\User\User;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Poll\PollRepository")
 * @ORM\Table(name="t_poll")
 * @ORM\HasLifecycleCallbacks()
 */
class Poll
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="You should set a Poll Title")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\Url(message="Should be a valid hyperlink")
     */
    private $link;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activatedAt = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deactivatedAt = null;

    /**
     * @ORM\OneToMany(targetEntity="PollChoice", mappedBy="poll", orphanRemoval=true)
     * @ORM\OrderBy({"orderIndex" = "ASC"})
     */
    private $choices;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User\User", inversedBy="polls")
     * @ORM\JoinTable(name="t_poll_user")
     */
    private $voters;

    /**
     * Poll constructor.
     */
    public function __construct()
    {
        $this->choices = new ArrayCollection();
        $this->voters = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     * @return $this
     */
    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return true === $this->status;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getActivatedAt(): ?DateTimeInterface
    {
        return $this->activatedAt;
    }

    /**
     * @param DateTimeInterface|null $activatedAt
     * @return $this
     */
    public function setActivatedAt(?DateTimeInterface $activatedAt): self
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeactivatedAt(): ?DateTimeInterface
    {
        return $this->deactivatedAt;
    }

    /**
     * @param DateTimeInterface|null $deactivatedAt
     * @return $this
     */
    public function setDeactivatedAt(?DateTimeInterface $deactivatedAt): self
    {
        $this->deactivatedAt = $deactivatedAt;

        return $this;
    }

    /**
     * @return Collection|PollChoice[]
     */
    public function getChoices(): Collection
    {
        return $this->choices;
    }

    /**
     * @param PollChoice $choice
     * @return $this
     */
    public function addChoice(PollChoice $choice): self
    {
        if (!$this->choices->contains($choice)) {
            $this->choices[] = $choice;
            $choice->setPoll($this);
        }

        return $this;
    }

    /**
     * @param PollChoice $choice
     * @return $this
     */
    public function removeChoice(PollChoice $choice): self
    {
        if ($this->choices->contains($choice)) {
            $this->choices->removeElement($choice);
            // set the owning side to null (unless already changed)
            if ($choice->getPoll() === $this) {
                $choice->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->voters;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasUser(User $user): bool
    {
        return $this->voters->exists(function ($key, $element) use ($user) {
            return $user->getEmail() === $element->getEmail();
        });
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addUser(User $user): self
    {
        if (!$this->voters->contains($user)) {
            $this->voters[] = $user;
        }

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user): self
    {
        if ($this->voters->contains($user)) {
            $this->voters->removeElement($user);
        }

        return $this;
    }


    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function prePersist(): void
    {
        $this->createdAt = $this->createdAt ?? new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate()
     * @throws \Exception
     */
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
