<?php

namespace App\Entity\Poll;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Poll\PollChoiceRepository")
 * @ORM\Table(name="t_poll_choice")
 * @ORM\HasLifecycleCallbacks()
 */
class PollChoice
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Poll\Poll", inversedBy="choices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $poll;

    /**
     * @ORM\OneToMany(targetEntity="PollVote", mappedBy="choice", orphanRemoval=true)
     */
    private $votes;

    /**
     * @ORM\Column(type="smallint")
     */
    private $orderIndex;

    /**
     * PollChoice constructor.
     */
    public function __construct()
    {
        $this->votes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Poll|null
     */
    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    /**
     * @param Poll|null $poll
     * @return $this
     */
    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    /**
     * @return Collection|PollVote[]
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    /**
     * @param PollVote $vote
     * @return $this
     */
    public function addVote(PollVote $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes[] = $vote;
            $vote->setChoice($this);
        }

        return $this;
    }

    /**
     * @param PollVote $vote
     * @return $this
     */
    public function removeVote(PollVote $vote): self
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
            // set the owning side to null (unless already changed)
            if ($vote->getChoice() === $this) {
                $vote->setChoice(null);
            }
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOrderIndex(): ?int
    {
        return $this->orderIndex;
    }

    /**
     * @param int $orderIndex
     * @return $this
     */
    public function setOrderIndex(int $orderIndex): self
    {
        $this->orderIndex = $orderIndex;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function prePersist(): void
    {
        $this->createdAt = $this->createdAt ?? new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @ORM\PreUpdate()
     * @throws \Exception
     */
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
