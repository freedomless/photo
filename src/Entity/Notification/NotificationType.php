<?php

namespace App\Entity\Notification;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_notification_types")
 */
class NotificationType implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"notifications"})
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $subject;

    /**
     * @ORM\Column(type="string")
     */
    private $method;

    /**
     * @ORM\Column(type="string")
     */
    private $template;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     */
    private $isEmail;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     */
    private $isWeb;

    /**
     * @ORM\Column(type="boolean",options={"default": true})
     */
    private $isPush;

    /**
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $isForce;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getIsEmail()
    {
        return $this->isEmail;
    }

    /**
     * @param mixed $isEmail
     */
    public function setIsEmail($isEmail): void
    {
        $this->isEmail = $isEmail;
    }

    /**
     * @return mixed
     */
    public function getIsWeb()
    {
        return $this->isWeb;
    }

    /**
     * @param mixed $isWeb
     */
    public function setIsWeb($isWeb): void
    {
        $this->isWeb = $isWeb;
    }

    /**
     * @return mixed
     */
    public function getIsPush()
    {
        return $this->isPush;
    }

    /**
     * @param mixed $isPush
     */
    public function setIsPush($isPush): void
    {
        $this->isPush = $isPush;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method): void
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getIsForce()
    {
        return $this->isForce;
    }

    /**
     * @param mixed $isForce
     */
    public function setIsForce($isForce): void
    {
        $this->isForce = $isForce;
    }
}
