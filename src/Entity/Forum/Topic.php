<?php

namespace App\Entity\Forum;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TopicRepository")
 * @ORM\Table(name="t_topics")
 */
class Topic extends BaseEntity
{

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title", "id"})
     * @ORM\Column(length=128, unique=true,nullable=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Forum\Section",inversedBy="topics")
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Forum\Opinion",mappedBy="topic",cascade={"remove","persist"})
     * @Assert\Count(min="1")
     * @ORM\OrderBy({"id":"ASC" })
     */
    private $opinions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     */
    private $owner;

    /**
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $isDisabled;

    /**
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $isApproved;

    /**
     * Topic constructor.
     */
    public function __construct()
    {
        $this->opinions = new ArrayCollection();
        $this->isDisabled = false;
        $this->isApproved = false;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     */
    public function setSection($section): void
    {
        $this->section = $section;
    }

    /**
     * @return mixed
     */
    public function getOpinions()
    {
        return $this->opinions;
    }

    /**
     * @param mixed $opinions
     */
    public function setOpinions($opinions): void
    {
        $this->opinions = $opinions;
    }

    /**
     * @param Opinion $opinion
     */
    public function addOpinion(Opinion $opinion)
    {
        $this->opinions->add($opinion);
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param mixed $isDisabled
     */
    public function setIsDisabled($isDisabled): void
    {
        $this->isDisabled = $isDisabled;
    }

    /**
     * @return mixed
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param mixed $isApproved
     */
    public function setIsApproved($isApproved): void
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }
}
