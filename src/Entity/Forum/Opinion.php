<?php

namespace App\Entity\Forum;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OpinionRepository")
 * @ORM\Table(name="t_opinions")
 */
class Opinion extends BaseEntity
{

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Forum\Topic",inversedBy="opinions",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $topic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(nullable=false,unique=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param mixed $topic
     */
    public function setTopic($topic): void
    {
        $this->topic = $topic;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $text = preg_replace('/^(\r\n\r\n)?(<p>&nbsp;<\/p>(\r\n\r\n)?)+/i', '', $text);
        $text = preg_replace('/(\r\n\r\n)?(<p>&nbsp;<\/p>(\r\n\r\n)?)+$/i', '', $text);

        $this->text = $text;
    }

}
