<?php

namespace App\Entity\Store;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscountRepository")
 * @ORM\Table(name="t_discounts")
 */
class Discount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\GreaterThan(1)
     * @Assert\LessThan(100)
     */
    private $percentage;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\GreaterThan(1)
     * @Assert\LessThanOrEqual(5)
     */
    private $total;

    /**
     * @ORM\Column(type="boolean")
     */
    private $series;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEnabled;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getSeries(): ?bool
    {
        return $this->series;
    }

    public function setSeries(bool $series): self
    {
        $this->series = $series;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool|null $isEnabled
     */
    public function setIsEnabled(?bool $isEnabled): void
    {
        $this->isEnabled = $isEnabled;
    }
}
