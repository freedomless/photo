<?php

namespace App\Entity\Store;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_transactions")
 */
class Transaction extends BaseEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $trnId;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $externalTrnId;

    /**
     * @ORM\Column(type="float")
     */
    private $paymentGross;

    /**
     * @ORM\Column(type="string")
     */
    private $currencyCode;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Store\Order",inversedBy="transaction")
     */
    private $order;

    /**
     * @ORM\Column(type="smallint",options={"default":0})
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $gateway;

    /**
     * @return mixed
     */
    public function getTrnId()
    {
        return $this->trnId;
    }

    /**
     * @param mixed $trnId
     */
    public function setTrnId($trnId): void
    {
        $this->trnId = $trnId;
    }

    /**
     * @return mixed
     */
    public function getPaymentGross()
    {
        return $this->paymentGross;
    }

    /**
     * @param mixed $paymentGross
     */
    public function setPaymentGross($paymentGross): void
    {
        $this->paymentGross = $paymentGross;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     */
    public function setCurrencyCode($currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @param mixed $gateway
     */
    public function setGateway($gateway): void
    {
        $this->gateway = $gateway;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getExternalTrnId()
    {
        return $this->externalTrnId;
    }

    /**
     * @param mixed $externalTrnId
     */
    public function setExternalTrnId($externalTrnId): void
    {
        $this->externalTrnId = $externalTrnId;
    }
}
