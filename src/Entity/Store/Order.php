<?php

namespace App\Entity\Store;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use App\Entity\User\Address;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="t_orders")
 */
class Order extends BaseEntity
{

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User",inversedBy="orders")
     */
    private $customer;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private $dispatched;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $trackId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Address", cascade={"persist","remove"})
     *
     * @Assert\Valid(groups={"order"})
     * @Assert\NotBlank(groups={"order"})
     */
    private $shippingAddress;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Address", cascade={"persist","remove"})
     *
     * @Assert\Valid(groups={"order"})
     * @Assert\NotBlank(groups={"order"})
     */
    private $billingAddress;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Store\Transaction",mappedBy="order")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Store\OrderItem",mappedBy="order")
     *
     * @Assert\Valid(groups={"order"})
     * @Assert\NotBlank(groups={"order"})
     * @Assert\Count(min="1",minMessage="Cart is empty",groups={"order"})
     */
    private $items;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Store\Invoice",mappedBy="order")
     */
    private $invoice;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    private $shippingCost;

    /**
     * @var float
     * @ORM\Column(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0.01")
     */
    private $total;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $tax;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->token = bin2hex(random_bytes(6));
        $this->items = new ArrayCollection();
        $this->dispatched = false;
        $this->shippingCost = 0;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param mixed $shippingAddress
     */
    public function setShippingAddress(Address $shippingAddress): void
    {
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param mixed $billingAddress
     */
    public function setBillingAddress(Address $billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function getDispatched()
    {
        return $this->dispatched;
    }

    /**
     * @param mixed $dispatched
     */
    public function setDispatched($dispatched): void
    {
        $this->dispatched = $dispatched;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param OrderItem $item
     */
    public function addItem(OrderItem $item)
    {
        $this->items->add($item);
    }

    /**
     * @param mixed $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * @param mixed $trackId
     */
    public function setTrackId($trackId): void
    {
        $this->trackId = $trackId;
    }

    /**
     * @return mixed
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param mixed $invoice
     */
    public function setInvoice($invoice): void
    {
        $this->invoice = $invoice;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return float|null
     */
    public function getShippingCost(): ?float
    {
        return $this->shippingCost;
    }

    /**
     * @param float|null $shippingCost
     */
    public function setShippingCost(?float $shippingCost): void
    {
        $this->shippingCost = $shippingCost;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTax(): float
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     */
    public function setTax(float $tax): void
    {
        $this->tax = $tax;
    }

}
