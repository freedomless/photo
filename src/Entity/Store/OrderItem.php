<?php

namespace App\Entity\Store;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 * @ORM\Table(name="t_order_items")
 */
class OrderItem implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Order",inversedBy="items")
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Material")
     */
    private $material;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Size")
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Product",inversedBy="orderItems")
     */
    private $product;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    private $commission;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    private $discount;

    /**
     * @ORM\Column(type="boolean",options={"default":false})
     */
    private $commissionAddedToBalance;

    /**
     * @ORM\Column(type="boolean",options={"default":false})
     */
    private $commissionPaid;

    public function __construct()
    {
        $this->commissionAddedToBalance = false;
        $this->commissionPaid = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param mixed $material
     */
    public function setMaterial($material): void
    {
        $this->material = $material;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCommissionAddedToBalance()
    {
        return $this->commissionAddedToBalance;
    }

    /**
     * @param mixed $commissionAddedToBalance
     */
    public function setCommissionAddedToBalance($commissionAddedToBalance): void
    {
        $this->commissionAddedToBalance = $commissionAddedToBalance;
    }

    /**
     * @return mixed
     */
    public function getCommissionPaid()
    {
        return $this->commissionPaid;
    }

    /**
     * @param mixed $commissionPaid
     */
    public function setCommissionPaid($commissionPaid): void
    {
        $this->commissionPaid = $commissionPaid;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return float|null
     */
    public function getCommission(): ?float
    {
        return $this->commission;
    }

    /**
     * @param float|null $commission
     */
    public function setCommission(?float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     */
    public function setDiscount(?float $discount): void
    {
        $this->discount = $discount;
    }
}
