<?php

namespace App\Entity\Store;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_invoices")
 */
class Invoice extends BaseEntity implements EntityInterface
{

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Store\Order",inversedBy="invoice")
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $invoiceId;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * Invoice constructor.
     * @throws \Exception
     */
    public function __construct()
    {

        $this->token = bin2hex(random_bytes(36));
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param mixed $invoiceId
     */
    public function setInvoiceId($invoiceId): void
    {
        $this->invoiceId = $invoiceId;
    }
}
