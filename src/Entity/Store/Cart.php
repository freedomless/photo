<?php

namespace App\Entity\Store;

use App\Entity\EntityInterface;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_carts")
 */
class Cart implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Store\CartItem",mappedBy="cart", cascade={"remove"})
     * @Groups({"cart"})
     */
    private $items;

    /**
     * @var string|null
     *
     * @Column(type="string",nullable=true)
     */
    private $session;

    /**
     * Cart constructor.
     * @param User|null $user
     */
    public function __construct(?User $user = null)
    {
        $this->user = $user;
        $this->items = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @param CartItem $product
     */
    public function addItem(CartItem $product): void
    {
        $this->items->add($product);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    /**
     * @return string|null
     */
    public function getSession(): ?string
    {
        return $this->session;
    }

    /**
     * @param string|null $session
     */
    public function setSession(?string $session): void
    {
        $this->session = $session;
    }
}
