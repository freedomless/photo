<?php

namespace App\Entity\Store;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartItemRepository")
 * @ORM\Table(name="t_cart_items")
 */
class CartItem implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @Groups({"cart"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Cart",inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Product")
     * @Assert\NotNull(message="Invalid product")
     * @Groups({"session_cart", "cart","create"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Material")
     * @Assert\NotNull(message="Invalid material")
     * @Groups({"session_cart", "cart","create"})
     */
    private $material;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store\Size")
     * @Assert\NotNull(message="Invalid size")
     * @Groups({"session_cart", "cart","create"})
     */
    private $size;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Length(min="1")
     * @Groups({"session_cart", "cart","create"})
     */
    private $quantity;

    /**
     * @var string|null
     */
    private $total;

    /**
     * @var mixed
     */
    private $discount;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param mixed $material
     */
    public function setMaterial($material): void
    {
        $this->material = $material;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart(Cart $cart): void
    {
        $this->cart = $cart;
    }

    /**
     * @param string $total
     */
    public function setTotal(string $total)
    {
        $this->total = $total;
    }

    /**
     * @Groups({"cart"})
     * @return string|int
     */
    public function getTotal()
    {
        return $this->total;

    }

    /**
     * @Groups({"cart"})
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount): void
    {
        $this->discount = $discount;
    }
}
