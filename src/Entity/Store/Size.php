<?php

namespace App\Entity\Store;

use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SizeRepository")
 * @ORM\Table(name="t_sizes")
 */
class Size implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     *
     * @Groups({"index","session_cart", "cart"})
     */
    private ?int $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"index"})
     */
    private ?string $alias;

    /**
     * @var int|null
     *
     * @ORM\Column(type="smallint",name="`order`")
     *
     * @Groups({"index"})
     */
    private ?int $order;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"index"})
     */
    private ?int $width;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"index"})
     */
    private ?int $height;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean",options={"default": false})
     */
    private ?bool $isEnabled;

    /**
     * Size constructor.
     */
    public function __construct()
    {

        $this->id = null;
        $this->alias = null;
        $this->order = null;
        $this->width = null;
        $this->height = null;
        $this->isEnabled = null;

    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getWidth() . ' x ' . $this->getHeight();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     * @return Size
     */
    public function setAlias(?string $alias): Size
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOrder(): ?int
    {
        return $this->order;
    }

    /**
     * @param int|null $order
     * @return Size
     */
    public function setOrder(?int $order): Size
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     * @return Size
     */
    public function setWidth(?int $width): Size
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool|null $isEnabled
     * @return Size
     */
    public function setIsEnabled(?bool $isEnabled): Size
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     */
    public function setHeight(?int $height): void
    {
        $this->height = $height;
    }

}
