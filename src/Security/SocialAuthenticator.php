<?php
/**
 * Created by PhpStorm.
 * User: dtanev
 * DateCreated: 2019-01-16
 * Time: 22:07
 */

namespace App\Security;

use App\Entity\User\User;
use App\EntityManager\UserManager;
use Exception;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator as BaseSocialAuthenticator;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class SocialAuthenticator
 * @package App\Security
 */
class SocialAuthenticator extends BaseSocialAuthenticator
{

    /**
     * @var ClientRegistry
     */
    private ClientRegistry $clientRegistry;

    /**
     * @var UserManager
     */
    private UserManager $manager;

    /**
     * @var SessionInterface
     */
    private FlashBagInterface $session;

    /**
     * @var RouterInterface
     */
    private RouterInterface $router;


    /**
     * SocialAuthenticator constructor.
     * @param ClientRegistry $clientRegistry
     * @param UserManager $manager
     * @param RouterInterface $router
     * @param FlashBagInterface $session
     */
    public function __construct(
        ClientRegistry $clientRegistry,
        UserManager $manager,
        RouterInterface $router,
        FlashBagInterface $session
    )
    {
        $this->clientRegistry = $clientRegistry;
        $this->manager = $manager;
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return RedirectResponse
     */
    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse(
            '/login', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'web_social_login_auth';
    }

    /**
     * @param Request $request
     * @return AccessToken|mixed
     */
    public function getCredentials(Request $request)
    {
        $network = $request->attributes->get('network');

        try {
            return ['token' => $this->fetchAccessToken($this->getClient($network)), 'network' => $network];
        } catch (Exception $e) {
            throw new AuthenticationException('Opps Something went wrong');
        }
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return User|object|UserInterface|null
     * @throws Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        $socialUser = $this->getClient($credentials['network'])->fetchUserFromToken($credentials['token']);

        return $this->manager->createSocialUser($socialUser, $credentials['network']);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return RedirectResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token,string $providerKey)
    {
        return new RedirectResponse($this->router->generate('web_profile',['slug' => $token->getUser()->getSlug()]));
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->session->add('error', 'Something went wrong please try again in a minute!');

        return new RedirectResponse('/login');
    }

    /**
     * @param string $network
     * @return OAuth2ClientInterface
     */
    private function getClient(string $network): OAuth2ClientInterface
    {
        return $this->clientRegistry->getClient($network);
    }

}
