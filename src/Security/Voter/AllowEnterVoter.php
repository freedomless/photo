<?php

namespace App\Security\Voter;

use App\Entity\Configuration;
use App\Repository\ConfigurationRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AllowEnterVoter
 * @package App\Security\Voter
 */
class AllowEnterVoter extends Voter
{
    /**
     * @var Configuration
     */
    private $config;

    /**
     * AllowEnterVoter constructor.
     * @param ConfigurationRepository $config
     * @throws InvalidArgumentException
     */
    public function __construct(ConfigurationRepository $config)
    {
        $this->config = $config->getConfig();
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports(string $attribute, $subject)
    {
        return $attribute === 'ALLOW_ENTER';
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        $allowEnter = $this->config->getAllowEnter();

        switch ($attribute) {
            case 'ALLOW_ENTER':

                if ($allowEnter === true) {
                    return true;
                }

                return $user instanceof UserInterface;

            default:
                return false;
        }

    }
}
