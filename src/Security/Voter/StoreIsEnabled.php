<?php

namespace App\Security\Voter;

use App\Entity\Configuration;
use App\Repository\ConfigurationRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class AllowEnterVoter
 * @package App\Security\Voter
 */
class StoreIsEnabled extends Voter
{
    /**
     * @var ConfigurationRepository
     */
    private $config;

    /**
     * AllowEnterVoter constructor.
     * @param ConfigurationRepository $config
     */
    public function __construct(ConfigurationRepository $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject):bool
    {
        return $attribute === 'STORE_ENABLED';
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token):bool
    {

        /**
         * @var $config Configuration
         */
        $config = $this->config->getConfig();

        $storeIsEnabled = $config->getAllowShopping();
        if ($attribute === 'STORE_ENABLED') {
            return $storeIsEnabled === true;
        }

        return false;
    }
}
