<?php

namespace App\DataFixtures;

use App\Entity\Photo\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class CategoriesFixture
 * @package App\DataFixtures
 */
class CategoriesFixture extends Fixture implements FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categories = [
            'Abstract',
            'Action',
            'Animals',
            'Architecture',
            'Conceptual',
            'Creative edit',
            'Documentary',
            'Everyday',
            'Fine Art Nude',
            'Humour',
            'Landscape',
            'Macro',
            'Mood',
            'Night',
            'Performance',
            'Portrait',
            'Still life',
            'Street',
            'Underwater',
            'Wildlife'
        ];

        foreach ($categories as $category) {
            $entity = new Category();
            $entity->setPath(strtolower(str_replace(' ', '-', $category)));
            $entity->setName($category);
            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
       return ['init'];
    }
}
