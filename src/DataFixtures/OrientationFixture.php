<?php

namespace App\DataFixtures;

use App\Entity\Photo\Orientation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class OrientationFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $orientations = ['landscape','portrait'];

        foreach ($orientations as $orientation) {
            $entity = new Orientation();
            $entity->setName($orientation);

            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['init'];
    }
}
