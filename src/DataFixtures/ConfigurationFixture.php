<?php

namespace App\DataFixtures;

use App\Entity\Configuration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class ConfigurationFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $config = new Configuration();


        $config->setCurrency('&euro;');
        $config->setName('Photoimaginart');
        $config->setCountry('BG');
        $config->setCity('Kazanlak');
        $config->setCounty('Stara Zagora');
        $config->setAddress('Ulitza');
        $config->setPhone('0885100100');
        $config->setShipping(10);
        $config->setTax(0.2);
        $config->setVatNumber(123123123);
        $config->setZip(6100);
        $config->setMaxCommission(100);
        $config->setPopularHoursAgo(72);
        $config->setAllowShopping(false);
        $config->setAllowEnter(false);
        $config->setMinCommission(1);

        $manager->persist($config);

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['init'];
    }
}
