<?php

namespace App\Helper;

use Psr\Container\ContainerInterface;
use Symfony\Component\Finder\Finder;

class VideoPresentationHelper
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    private array $videos = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getRandomVideo()
    {
        $this->searchVideos();

        if (count($this->videos) > 0) {
            return $this->videos[rand(0, count($this->videos) - 1)];
        }

        return false;
    }

    private function searchVideos()
    {
        $finder = new Finder();

        $videosPath = $this->container
                ->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'videos';

        $finder->files()->name(['*.mp4'])->in($videosPath);

        if ($finder->hasResults()) {
            $this->videos = array_values(iterator_to_array($finder));
        }
    }
}