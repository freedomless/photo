<?php

namespace App\Helper;

use App\Entity\Photo\Image;
use Psr\Cache\InvalidArgumentException;

abstract class ProductHelper
{
    /**
     * @param Image $image
     * @param int $min
     * @return bool
     */
    public static function needPrint(Image $image, int $min)
    {
        $actual = $image->getWidth() > $image->getHeight() ? $image->getHeight() : $image->getWidth();

        return $actual < $min;

    }
}
