<?php

namespace App\Helper;

use App\Dto\Response\ListDto;
use App\Entity\Photo\Post;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Trait PaginatorTrait
 * @package App\Helper
 */
trait PaginatorTrait
{

    /**
     * @var PaginatorInterface
     */
    protected PaginatorInterface $paginator;

    /**
     * @var string
     */
    protected string $type = 'api';

    /**
     * @required
     * @param PaginatorInterface $paginator
     */
    public function setPaginator(PaginatorInterface $paginator): void
    {
        $this->paginator = $paginator;
    }

    /**
     * @param QueryBuilder $qb
     * @param int $page
     * @param int $limit
     * @return PaginationInterface|ListDto
     */
    public function paginate($qb, int $page = 1, int $limit = 20)
    {

        if ($this->type === 'web') {
            return $this->paginator->paginate($qb, $page, $limit, ['wrap-queries' => true]);
        }

        if ($page && $limit) {
            $qb->setFirstResult(($page - 1) * $limit);
            $qb->setMaxResults($limit);
        }

        $paginator = new Paginator($qb);

        $entities = $paginator->getQuery()->getResult();

        $list = new ListDto();
        $list->setItems($entities);

        if ($limit) {
            $list->setPages((int)round($paginator->count() / $limit));
        }

        $list->setTotal($paginator->count());
        $list->setHasMore(($page > 0 ? $page : 1) * $limit < $paginator->count());

        return $list;
    }

    public function toList($entities, $total, $page, $limit)
    {
        $list = new ListDto();
        $list->setItems($entities);

        if ($limit) {
            $list->setPages((int)round($total / $limit));
        }

        $list->setTotal($total);
        $list->setHasMore(($page > 0 ? $page : 1) * $limit < $total);

        return $list;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setPaginationType(string $type): void
    {
        $this->type = $type;
    }
}
