<?php

namespace App\Helper;

use App\Entity\Photo\Image;
use App\Entity\Photo\Post;
use App\EntityManager\PostManager;
use App\Enum\FileStorageFolderEnum;
use App\FileManager\S3FileManager;
use App\Processor\ImageProcessor;
use Imagick;
use ImagickException;
use ImagickPixel;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SocialImageCreator
{
    /**
     * @var ImageProcessor
     */
    private ImageProcessor $processor;

    /**
     * @var S3FileManager
     */
    private S3FileManager $fileManager;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var PostManager
     */
    private PostManager $manager;

    public function __construct(
        ImageProcessor $processor,
        S3FileManager $fileManager,
        ContainerInterface $container,
        PostManager $manager
    )
    {
        $this->processor = $processor;
        $this->fileManager = $fileManager;
        $this->container = $container;
        $this->manager = $manager;
    }

    /**
     * @param Post $post
     * @param string|null $watermarkPath
     * @param bool $replace
     * @param bool $flush
     * @throws ImagickException
     */
    public function create(Post $post, bool $replace = true, string $watermarkPath = null, bool $flush = true)
    {

        if ($post->getType() > 1) {
            $this->collage($post);
            return;
        }

        /** @var Image $imageObj */
        $imageObj = $post->getImage();

        if (false === $replace && !is_null($imageObj->getSocial())) {
            return;
        }

        $image = $imageObj->getLarge();

        $this->processor->setImage($image);

        $socialResizeQuality = $this->container->getParameter('app.social_resize_quality');
        $this->processor->getImagick()->setImageCompressionQuality($socialResizeQuality);

        $socialResizeWidth = $this->container->getParameter('app.social_resize_width');
        if ($this->processor->getWidth() < $socialResizeWidth) {
            $this->processor->scale($socialResizeWidth, 0);
        } else {
            $this->processor->resize([$socialResizeWidth, ($socialResizeWidth * 5)]);
        }

        $xRatio = $this->container->getParameter('app.social_resize_x_ratio');
        $yRatio = $this->container->getParameter('app.social_resize_y_ratio');

        $width = $this->processor->getImagick()->getImageWidth();
        $x = $width;
        $y = (int)($width * ($yRatio / $xRatio));

        $this->processor->crop($x, $y);

        $watermark = $watermarkPath ?? $this->container->getParameter('app.social_resize_watermark_path');
        $xOffset = $this->container->getParameter('app.social_resize_x_offset');
        $yOffset = $this->container->getParameter('app.social_resize_y_offset');

        $this->processor->watermark($watermark, $xOffset, $y - $yOffset);

        $post->getImage()->setSocial($this->fileManager->add($this->processor->getBlob(), FileStorageFolderEnum::IMAGES_SOCIAL));

        if ($flush) {
            $this->manager->em->flush();
        }

        $this->processor->reset();
    }

    public function collage(Post $post, int $border = 4, string $color = '#17161a')
    {
        $children = count($post->getChildren());
        $nextRow = $children / 2;

        $nextRow = (int)floor($nextRow) === $nextRow ? $nextRow - 1 : floor($nextRow);
        $secondRow = (int)floor($nextRow) === $nextRow ? 1200 / ($nextRow + 1) : 1200 / $nextRow;

        $image = new \Imagick();
        $image->newImage(1200, 632, new ImagickPixel('red'), 'jpg');

        $counter = 0;
        $xStart = 1200 / ($nextRow + 1);

        foreach ($post->getChildren() as $i => $child) {

            $img = new \Imagick();
            $img->readImageBlob(file_get_contents($child->getImage()->getSmall()));

            if ($counter > $nextRow) {
                $counter = 0;
                $xStart = $secondRow;
            }

            [$width, $height] = [$xStart - $border, 332 - 2 * $border];

            if ($counter + 1 > $nextRow || $i === $children - 1) {
                $width -= $border;
            }

            $img->cropThumbnailImage($width, $height);

            $x = $xStart * $counter;

            $y = $i > $nextRow ? 300 : 0;

            $img->borderImage($color, $border, $border);
            $image->compositeImage($img, Imagick::COMPOSITE_OVER, $x, $y);

            $counter++;

        }

        $url = $this->fileManager->add($image->getImageBlob(), FileStorageFolderEnum::IMAGES_SOCIAL);

        $post->getImage()->setSocial($url);

        $this->manager->em->flush();


       return $url;

    }

}
