<?php

namespace App\Helper;

use App\Dto\Response\ListDto;
use App\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Trait SerializationTrait
 * @package Paynetics\Helper
 */
trait SerializationTrait
{
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;

    /**
     * @var NormalizerInterface
     */
    protected NormalizerInterface $normalizer;

    /**
     * @var DenormalizerInterface
     */
    protected DenormalizerInterface $denormalizer;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected array $context = [
        DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s'
    ];

    /**
     * @required
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @required
     * @param NormalizerInterface $normalizer
     */
    public function setNormalizer(NormalizerInterface $normalizer): void
    {

        $this->normalizer = $normalizer;
    }

    /**
     * @required
     * @param DenormalizerInterface $denormalizer
     */
    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    /**
     * @required
     * @param ContainerInterface $container
     */
    public function setServiceContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    /**
     * @param $data
     * @param array $context
     * @param string $format
     * @return string
     */
    public function serialize($data, array $context = [], string $format = 'json'): string
    {

        $this->context[AbstractObjectNormalizer::SKIP_NULL_VALUES] = false;
        $this->context[AbstractObjectNormalizer::GROUPS] = $this->groups($data);
        $this->context[AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER] = static function () { return null; };

        $this->prepareAttributes($context, $data);

        return $this->serializer->serialize($data, $format, array_merge_recursive($this->context, $context));
    }

    /**
     * @param string $data
     * @param string $type
     * @param array $context
     * @param EntityInterface|null $existing
     * @param string $format
     * @return object
     */
    public function deserialize(string $data,
                                string $type,
                                array $context = [],
                                EntityInterface $existing = null,
                                string $format = 'json')
    {

        if ($existing) {
            $this->context[AbstractObjectNormalizer::OBJECT_TO_POPULATE] = $existing;
            $this->context[AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE] = true;
        }

        return $this->serializer->deserialize($data ?: '{}', $type, $format, array_merge_recursive($this->context, $context));
    }

    /**
     * @param $data
     * @param array $context
     * @return array|bool|float|int|string
     * @throws ExceptionInterface
     */
    public function normalize($data, array $context = [])
    {
        $this->context[AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER] = static function () { return null; };

        return $this->normalizer->normalize($data, null, array_merge_recursive($this->context, $context));
    }

    /**
     * @param array $data
     * @param string $type
     * @param array $context
     * @return array|object
     * @throws ExceptionInterface
     */
    public function denormalize(array $data, string $type, array $context = [])
    {
        return $this->denormalizer->denormalize($data, $type, null, $context);
    }

    /**
     * @param $data
     * @return string[]
     */
    protected function groups($data): ?array
    {
        $groups = null;

        if (isset($_GET[AbstractObjectNormalizer::GROUPS])) {
            foreach (explode(',', $_GET[AbstractObjectNormalizer::GROUPS]) as $item) {
                $groups[] = $item;
            }
        }

        if (is_array($data) && isset($data['data']) && $data['data'] instanceof ListDto) {
            $groups[] = 'list';
        }

        return $groups;
    }

    /**
     * @param $context
     * @param $data
     */
    private function prepareAttributes(&$context, $data): void
    {
        if (!array_key_exists(AbstractObjectNormalizer::ATTRIBUTES, $context) ||
            count($context[AbstractObjectNormalizer::ATTRIBUTES]) === 0) {
            unset($context[AbstractObjectNormalizer::ATTRIBUTES]);
        } else {

            unset($this->context['groups'], $context['groups']);

            if ($data['data'] instanceof ListDto) {
                $context[AbstractObjectNormalizer::ATTRIBUTES] = ['items' => $context[AbstractObjectNormalizer::ATTRIBUTES]];
            }
        }

    }

}
