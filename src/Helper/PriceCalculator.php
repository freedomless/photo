<?php

namespace App\Helper;

use App\Entity\Store\Product;
use App\EntityManager\BaseManager;
use App\EntityManager\ProductManager;
use App\Repository\ConfigurationRepository;
use App\Repository\MaterialRepository;
use App\Repository\SizeRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PriceCalculator
 * @package App\Helper
 */
class PriceCalculator
{
    use SerializationTrait;

    /**
     * @var SizeRepository
     */
    private SizeRepository $sizeRepository;

    /**
     * @var MaterialRepository
     */
    private MaterialRepository $materialRepository;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * @var ProductManager
     */
    private ProductManager $productManager;

    /**
     * @var BaseManager
     */
    private BaseManager $discountManager;

    /**
     * PriceCalculator constructor.
     * @param ConfigurationRepository $configurationRepository
     * @param ProductManager $productManager
     * @param SizeRepository $sizeRepository
     * @param BaseManager $discountManager
     * @param MaterialRepository $materialRepository
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        ProductManager $productManager,
        SizeRepository $sizeRepository,
        BaseManager $discountManager,
        MaterialRepository $materialRepository
    ) {
        $this->sizeRepository = $sizeRepository;
        $this->materialRepository = $materialRepository;
        $this->configurationRepository = $configurationRepository;
        $this->productManager = $productManager;
        $this->discountManager = $discountManager;
    }

    /**
     * @param Product $product
     * @param null $size
     * @param null $material
     * @param int $quantity
     * @return mixed
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     */
    public function calculatePrice(Product $product, $size = null, $material = null, int $quantity = 1)
    {
        $total = 0;

        $this->checkForNewPrice($product);

        if (count($product->getChildren()) > 0) {
            foreach ($product->getChildren() as $child) {
                $this->checkForNewPrice($child);

                $total += $this->calculate($child, $size, $material, $quantity);
            }
        } else {
            $total = $this->calculate($product, $size, $material, $quantity);
        }

        $discount = $this->checkForDiscount($product);

        if ($discount) {
            $total -= $total * ($discount / 100);
        }

        return [
            'total'         => number_format($total, 2),
            'discount'      => number_format($total * ($discount / 100), 2)
        ];
    }

    /**
     * @param Product $product
     * @param null $size
     * @param null $material
     * @param int $quantity
     * @return float|int
     * @throws InvalidArgumentException
     * @throws ExceptionInterface
     */
    private function calculate(Product $product, $size = null, $material = null, int $quantity = 1)
    {
        $size = is_int($size) ? $this->sizeRepository->find($size) : $size;
        $material = is_int($material) ? $this->materialRepository->find($material) : $material;

        $size = $size ? $this->normalizer->normalize(
            $size,
            'json',
            ['groups' => 'index']
        ) : $this->sizeRepository->getSizes()[0];
        $material = $material ? $this->normalizer->normalize(
            $material,
            'json',
            ['groups' => 'index']
        ) : $this->materialRepository->getMaterials()[0];

        $image = $product->getPost()->getImage();

        $width = $image->getOrientation()->getId() === 1 ? $image->getHeight() : $image->getWidth();

        $height = $image->getOrientation()->getId() === 2 ? $image->getHeight() : $image->getWidth();

        $area = $size['width'] * $height / $width * $size['width'];

        $price = $this->configurationRepository->getConfig()->getProductCommission()
            + $product->getPrice()
            + $product->getCommission();

        return (round($area * $material['price']) + $price) * $quantity;
    }

    /**
     * @param Product $product
     * @throws \Exception
     */
    private function checkForNewPrice(Product $product)
    {
        if ($product->getCommissionChangingOn() !== null && $product->getCommissionChangingOn() < new \DateTime()) {
            $product->setCommission($product->getNewCommission());
            $product->setNewCommission(null);
            $product->setCommissionChangingOn(null);

            $this->productManager->flush($product);
        }
    }

    /**
     * @param Product $product
     * @return bool
     */
    private function checkForDiscount(Product $product)
    {
        $children = count($product->getChildren());
        if ($children > 0) {
            $discount = $this->discountManager->repository->findForSeries($children);
        } else {
            $discount = $this->discountManager->repository->findOneBy(
                ['series' => false, 'isEnabled' => true],
                [],
                false
            );
        }

        return $discount ? $discount->getPercentage() : false;
    }
}
