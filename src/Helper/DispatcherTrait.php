<?php

namespace App\Helper;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Trait DispatcherTrait
 * @package Paynetics\Helper
 */
trait DispatcherTrait
{
    /**
     * @var EventDispatcherInterface
     */
    protected EventDispatcherInterface $dispatcher;

    /**
     * @required
     * @param mixed $dispatcher
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher): void
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Event $event
     */
    public function dispatch(Event $event): void
    {
        $this->dispatcher->dispatch($event);
    }

}
