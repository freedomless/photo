<?php

namespace App\Helper;

use App\Entity\User\User;

/**
 * Trait AuthorizationTrait
 * @package App\Helper
 */
trait AuthorizationTrait
{

    /**
     * @param $user
     * @param $entity
     * @return bool
     */
    protected function authorize(User $user, $entity): bool
    {
        if (!method_exists('getUser', $entity)) {

            return false;

        }

        return $user->getId() === $entity->getUser()->getId();
    }
}
