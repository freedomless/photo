<?php

namespace App\Helper;

use Symfony\Contracts\Cache\TagAwareCacheInterface;

/**
 * Trait CacheTrait
 * @package App\Helper
 */
trait CacheTrait
{
    /**
     * @var TagAwareCacheInterface
     */
    protected TagAwareCacheInterface $cache;

    /**
     * @required
     * @param TagAwareCacheInterface $cache
     */
    public function setCache(TagAwareCacheInterface $cache)
    {
        $this->cache = $cache;
    }
}
