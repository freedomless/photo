<?php

namespace App\Helper;

use Symfony\Contracts\Translation\TranslatorInterface;

trait TranslationTrait
{

    /**
     * @var TranslatorInterface
     */
    protected TranslatorInterface $translator;

    /**
     * @required
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator): void
    {
        $this->translator = $translator;
    }
}
