<?php

namespace App\Helper;

use App\Dto\DtoInterface;
use App\Entity\EntityInterface;
use App\Exception\ApiException;
use App\Exception\ExceptionCodes;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Trait ValidationTrait
 * @package App\Helper
 */
trait ValidationTrait
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @required
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * @param EntityInterface $entity
     * @param string|GroupSequence|(string|GroupSequence)[]|null $groups
     * @throws ApiException
     */
    protected function validate($entity, $groups = null): void
    {
        $errors = $this->validator->validate($entity, null, $groups);

        if (count($errors) > 0) {
            throw new ApiException(ExceptionCodes::VALIDATION_ERROR, $this->normalize($errors));
        }
    }

}
