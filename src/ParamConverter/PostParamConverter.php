<?php

namespace App\ParamConverter;

use App\Entity\Photo\Post;
use App\EntityManager\PostManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class PostParamConverter
 * @package App\Converter
 */
class PostParamConverter implements ParamConverterInterface
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;

    /**
     * @var PostManager
     */
    private PostManager $manager;

    /**
     * PostParamConverter constructor.
     * @param TokenStorageInterface $storage
     * @param PostManager $manager
     */
    public function __construct(TokenStorageInterface $storage, PostManager $manager)
    {
        $this->storage = $storage;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {

        if (strpos($request->get('_route'), 'admin') === false) {
            $token = $this->storage->getToken();

            if (!$token) {
                throw new AuthenticationException();
            }

            $user = $token->getUser();

            if (!$user instanceof UserInterface) {
                throw new AuthenticationException();
            }

            $id = $request->get('id',0);


            $post = $this->manager->repository->findOneBy(compact('id', 'user'));

        } else {
            $post = $this->manager->repository->find($request->get('id', 0));
        }

        if ($post === null) {
            throw new NotFoundHttpException();
        }

        $request->attributes->set($configuration->getName(), $post);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Post::class;
    }
}
