<?php

namespace App\OldEntity;

use App\Entity\BaseEntity;
use App\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="t_series")
 */
class Series extends BaseEntity implements EntityInterface
{

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true,options={"default":false})
     */
    private $completed;

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     */
    public function setCompleted(bool $completed): void
    {
        $this->completed = $completed;
    }

}
