<?php

namespace App\Form\Model;

use App\Entity\Forum\Section;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class TopicModel
{
    /**
     * @var string
     * @Assert\NotBlank(normalizer="trim")
     */
    private $title;

    /**
     * @var Section
     */
    private $section;

    /**
     * @var ArrayCollection
     */
    private $opinions;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Section
     */
    public function getSection(): Section
    {
        return $this->section;
    }

    /**
     * @param Section $section
     */
    public function setSection(Section $section): void
    {
        $this->section = $section;
    }

    /**
     * @return ArrayCollection
     */
    public function getOpinions(): ArrayCollection
    {
        return $this->opinions;
    }

    /**
     * @param ArrayCollection $opinions
     */
    public function setOpinions(ArrayCollection $opinions): void
    {
        $this->opinions = $opinions;
    }


}
