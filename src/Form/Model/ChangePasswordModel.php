<?php

namespace App\Form\Model;

use Rollerworks\Component\PasswordStrength\Validator\Constraints\PasswordRequirements;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordModel
{
    /**
     * @var string
     */
    private ?string $oldPassword;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Password cannot be blank")
     *
     * @PasswordRequirements(minLength="8",requireLetters=true,requireNumbers=true,requireCaseDiff=true)
     */
    private ?string $password;

    public function __construct() {
        $this->oldPassword = null;
        $this->password = null;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }
}
