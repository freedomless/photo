<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class LoginModel
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Email()
     */
    private ?string $email;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(normalizer="trim")
     */
    private ?string $password;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

}
