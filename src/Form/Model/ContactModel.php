<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ContactModel
{
    /**
     * @var string
     *
     * @Assert\NotBlank(normalizer="trim", message="Please type your Name")
     */
    private ?string $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Spcify a valid Email address.")
     * @Assert\Email(message="This is not a valid Email address.")
     */
    private ?string $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Message should not be blank.")
     * @Assert\Length(min="3", minMessage="You should type at least 3 characters")
     */
    private ?string $message;

    public function __construct() {
        $this->name = null;
        $this->email = null;
        $this->message = null;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ContactModel
     */
    public function setName(?string $name): ContactModel
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ContactModel
     */
    public function setEmail(?string $email): ContactModel
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ContactModel
     */
    public function setMessage(?string $message): ContactModel
    {
        $this->message = $message;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'email'   => $this->email,
            'name'    => $this->name
        ];
    }
}
