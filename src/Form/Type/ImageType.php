<?php /** @noinspection DuplicatedCode */

namespace App\Form\Type;

use App\Entity\Photo\Image;
use App\Validator\ImageSize;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image as ImageValidator;

/**
 * Class ImageType
 * @package App\Form\Type
 */
class ImageType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if ($options['type'] === 0) {
            $this->buildSeriesItem($builder, $options);

            return;
        }

        if ($options['type'] === 2) {
            return;
        }

        if ($options['type'] === 4) {
            $this->buildProductItem($builder, $options);

            return;
        }

        $this->buildSingle($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => Image::class,
            'allow_extra_fields' => true,
            'allow_file_upload'  => true,
            'type'               => 1,
            'configuration'      => null,
        ]);
    }

    /**
     * @param $builder
     * @param $options
     */
    private function buildSingle(FormBuilderInterface $builder, array $options): void
    {
        if ($options['method'] === 'POST') {
            $builder
                ->add('file', FileType::class, ['required' => false,'label' => 'Click or Drag & Drop File', 'attr' => ['accept' => 'image/jpeg']]);

        }

        $builder
            ->add('camera', null, ['required' => false, 'attr' => ['placeholder' => 'Camera']])
            ->add('lens', null, ['required' => false, 'attr' => ['placeholder' => 'Lens']])
            ->add('focalLength', null, ['required' => false, 'attr' => ['placeholder' => 'Focal Length']])
            ->add('shutterSpeed', null, ['required' => false, 'attr' => ['placeholder' => 'Shutter Speed']])
            ->add('ISO', null, ['required' => false, 'label' => 'ISO', 'attr' => ['placeholder' => 'ISO']])
            ->add('aperture', null, ['required' => false, 'attr' => ['placeholder' => 'Aperture']])
            ->add('exposure', null, ['required' => false, 'attr' => ['placeholder' => 'Exposure']])
            ->add('flash', null, ['required' => false, 'attr' => ['placeholder' => 'Flash']])
            ->add('filter', null, ['required' => false, 'attr' => ['placeholder' => 'Filter']])
            ->add('tripod', null, ['required' => false, 'attr' => ['placeholder' => 'Tripod']]);

    }

    /**
     * @param $builder
     * @param $options
     */
    private function buildSeriesItem(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('file', FileType::class, [
            'required'    => false,
            'label' => 'Click or Drag & Drop File',
            'attr'        => ['accept' => 'image/jpeg']
            ]);
    }

    /**
     * @param $builder
     * @param $options
     */
    private function buildProductItem(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('file', FileType::class, [
            'attr' => ['accept' => 'image/jpeg'],
            'constraints' => [new ImageSize(['min' => $options['configuration']->getMinPrintSize()])]
        ]);
    }
}
