<?php

namespace App\Form\Type;

use App\Entity\Configuration;
use App\Entity\Photo\Image;
use App\Entity\Store\Product;
use App\Helper\ProductHelper;
use App\Repository\ConfigurationRepository;
use DateTime;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class ProductType
 * @package App\Form\Type
 */
class ProductType extends AbstractType
{
    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * ProductType constructor.
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Configuration $configuration
         */
        $configuration = $options['configuration'];

        if ($options['type'] === 1) {

            $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($configuration) {

                /**
                 * @var Product $model
                 */
                $model = $event->getData();
                $builder = $event->getForm();

                $priceChangeDate = $model ? $model->getPriceChangedAt() : false;

                $passed = $priceChangeDate && $configuration->getPriceUpdateInterval() ? $priceChangeDate->modify('+ ' . $configuration->getPriceUpdateInterval() . 'days') < new DateTime() : false;

                if ($configuration->getPriceUpdateInterval() === 0 || $model->getPriceChangedAt() === null || ($configuration->getPriceUpdateInterval() !== null && $passed)) {

                    $builder->add('commission', NumberType::class, [
                        'html5' => true,
                        'required'    => true,
                        'attr' => [
                            'step' => 'any',
                            'placeholder' => '0'
                        ],
                        'constraints' => new Range([
                            'min'               => $configuration->getMinCommission(),
                            'notInRangeMessage' => 'Commission have to be between {{ min }} and {{ max }}.',
                            'max'               => $configuration->getMaxCommission()
                                                   ])
                    ]);

                }

                if ($model->getPost()->getImage()->getPrint() === null && ProductHelper::needPrint($model->getPost()->getImage(), $this->configurationRepository->getConfig()->getMinPrintSize())) {

                    $builder->add('post', PostType::class, ['type' => 4, 'configuration' => $configuration]);

                }

            });

        }

        if ($options['type'] === 2) {

            $builder->add('children', CollectionType::class, [
                'by_reference'  => false,
                'allow_add'     => false,
                'allow_delete'  => false,
                'entry_type'    => __CLASS__,
                'entry_options' => compact('configuration')
            ]);
        }

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Product::class,
            'type'          => 1,
            'configuration' => null,
        ]);
    }

}
