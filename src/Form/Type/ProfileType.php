<?php

namespace App\Form\Type;

use App\Entity\User\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName');
        $builder->add('lastName');

        if($options['route'] === 'register') {
            return;
        }

        $builder->add('facebook');
        $builder->add('twitter');
        $builder->add('instagram');
        $builder->add('website');
        $builder->add('nationality',CountryType::class, ['placeholder' => 'Select Nationality', 'label' => 'Nationality']);
        $builder->add('address',AddressType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
            'route'      => null]);
    }
}
