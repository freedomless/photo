<?php

namespace App\Form\Type;

use App\Entity\Photo\Album;
use App\Entity\Photo\Post;
use App\Enum\PostStatusEnum;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use phpDocumentor\Reflection\DocBlock\Description;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

/**
 * Class AlbumType
 * @package App\Form\Type
 */
class AlbumType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $options['user'];
        $album = $builder->getData();

        $builder->add('name');
        $builder->add('order', HiddenType::class);

        $qb = function (bool $limit = true) use ($builder, $user, $album, $options) {

            return function (PostRepository $repository) use ($builder, $limit, $user, $album, $options) {

                $qb = $repository->createQueryBuilder('p')
                    ->where('p.user = :user')
                    ->leftJoin('p.albums', 'a')
                    ->andWhere('p.type = 1')
                    ->andWhere('p.isDeleted = false')
                    ->setParameter('user', $user);

                if ($options['method'] === 'PUT') {
                    $qb->select(['p', 'CASE WHEN :album MEMBER OF p.albums THEN 1 ELSE 200 END AS HIDDEN order', 'CASE WHEN p = :cover THEN 0 ELSE 1 END AS HIDDEN cover'])
                        ->setParameter('album', $album)
                        ->setParameter('cover', $album->getCover())
                        ->orderBy('cover', 'asc')
                        ->addOrderBy('order', 'asc');

                }

                if ($limit) {
                    $qb->setMaxResults(20);
                }

                return $qb;
            };
        };

        $children = [
            'by_reference'  => false,
            'class'         => Post::class,
            'multiple'      => true,
            'expanded'      => true,
            'query_builder' => $qb(true),
        ];

        $builder->add('posts', EntityType::class, $children);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($children, $qb) {

            $builder = $event->getForm();

            $builder->remove('posts');

            $children['query_builder'] = $qb(false);

            $builder->add('posts', EntityType::class, $children);

        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA,function(FormEvent $event){

            $album = $event->getData();

            $order = [];

            if ($album->getCover()) {
                $order[] = $album->getCover()->getId();
            }

            foreach ($album->getPosts() as $post) {
                $order[] = $post->getId();
            }

            $album->setOrder(implode(',', array_unique($order)));

        });


    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
            'user'       => null
        ]);
    }
}
