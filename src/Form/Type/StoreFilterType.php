<?php

namespace App\Form\Type;

use App\Dto\Request\StoreFilterDto;
use App\Entity\Photo\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StoreFilterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orientation', ChoiceType::class, ['placeholder' => 'Select Orientation', 'choices' => ['Portrait' => 2, 'Landscape' => 1]])
            ->add('isGreyscale', ChoiceType::class, ['placeholder' => 'All', 'label' => 'Black&White', 'choices' => ['Yes' => 1, 'No' => 0]])
            ->add('isNude', ChoiceType::class, ['placeholder' => 'All', 'label' => 'Nude', 'choices' => ['Yes' => 1, 'No' => 0]])
            ->add('category', EntityType::class, ['class' => Category::class, 'placeholder' => 'Select Category','query_builder' => function(EntityRepository $qb){
                return $qb->createQueryBuilder('c')
                    ->orderBy('c.name','ASC');
            }])
            ->add('keyword', null, ['attr' => ['placeholder' => 'Photo title, photographer, tag and more']])
            ->add('filter', SubmitType::class, ['attr' => ['class' => 'btn btn-primary']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StoreFilterDto::class,
            'method'     => 'GET',
            'required' => false
        ]);
    }
}
