<?php

namespace App\Form\Type;

use App\Entity\Poll\Poll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'attr' => [
                    'placeholder' => 'Poll title',
                    'class' => 'form-control',
                ],
                'label' => 'Poll Title:',
            ])
            ->add('description', null, [
                'attr' => [
                    'placeholder' => 'Poll description',
                    'class' => 'form-control',
                ],
                'label' => 'Poll Description:',
            ])
            ->add('link', null, [
                'attr' => [
                    'placeholder' => 'Link to the discussion topic',
                    'class' => 'form-control',
                ],
                'label' => 'Poll Discussion Link:',
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Poll::class,
            'attr' => [
                'novalidate' => true,
            ],
        ]);
    }
}
