<?php

namespace App\Form\Type;

use App\Entity\Forum\Section;
use App\Entity\Forum\Topic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TopicType
 * @package App\AlbumForm\Type
 */
class TopicType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('opinions', CollectionType::class, [
                'prototype'  => false,
                'entry_type' => OpinionType::class,
            ]);

        if ($options['update']) {
            $builder->add('section', EntityType::class, ['class' => Section::class]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Topic::class,
            'update'     => false,
            'sections'   => []
        ]);
    }
}
