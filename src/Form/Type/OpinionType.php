<?php

namespace App\Form\Type;

use App\Entity\Forum\Opinion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OpinionType
 * @package App\Form\Type
 */
class OpinionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, ['label' => 'Reply', 'attr' => ['rows' => 7, 'class' => 'd-none']]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, static function (FormEvent $event) {

            $opinion = $event->getData()->getText();
            $opinion = trim(str_replace('&nbsp;', '', strip_tags($opinion)));

            if(empty($opinion)){
                $event->getForm()->get('text')->addError(new FormError('Message cannot be empty!'));
            }

        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Opinion::class,
            'required' => false
        ]);
    }
}
