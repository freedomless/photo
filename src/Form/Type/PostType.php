<?php /** @noinspection DuplicatedCode */

namespace App\Form\Type;

use App\Entity\Photo\Category;
use App\Entity\Photo\Post;
use App\Entity\Photo\Tag;
use App\Enum\PostStatusEnum;
use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PostType
 * @package App\Form\Type
 */
class PostType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * ImageType constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $storage
     */
    public function __construct(EntityManagerInterface $em,
                                ValidatorInterface $validator,
                                TokenStorageInterface $storage)
    {
        $this->em = $em;
        $this->storage = $storage;
        $this->validator = $validator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        //Series item
        if ($options['type'] === 0) {
            $this->buildSeriesItem($builder, $options);

            return;
        }

        //Single
        if ($options['type'] === 1) {
            $this->buildSingle($builder, $options);

            return;
        }

        //Series
        if ($options['type'] === 2) {

            $this->buildSeries($builder, $options);

            return;
        }

        //Series from published
        if ($options['type'] === 3) {

            $this->buildSeriesFromPublished($builder, $options);

            return;
        }

        //Products prints
        if ($options['type'] === 4) {

            $this->buildProductItem($builder, $options);

            return;
        }

        //Series from curator
        if ($options['type'] === 5) {
            $this->buildSeriesFromCurator($builder, $options);
        }

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'         => Post::class,
            'allow_extra_fields' => true,
            'allow_file_upload'  => true,
            'type'               => 1,
            'settings'           => ['min' => 3, 'max' => 5],
            'configuration'      => null

        ]);
    }

    /**
     * @param $builder
     * @param $options
     */
    private function buildSingle(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('title', null, ['required' => false, 'attr' => ['placeholder' => 'Photo Title']])
            ->add('tags', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Tags (Comma Separated. Eg: Tag A, Tag B, etc.)']])
            ->add('image', ImageType::class, ['type' => $options['type'], 'method' => $options['method']])
            ->add('description', TextareaType::class, ['required' => false, 'attr' => ['placeholder' => 'Description']])
            ->add('isNude', CheckboxType::class, ['required' => false, 'label' => 'Contains nudity?'])
            ->add('isGreyscale', CheckboxType::class, ['required' => false, 'label' => 'Black and white image?'])
            ->add('category', EntityType::class, [
                'class'       => Category::class,
                'placeholder' => 'Select Category *',
                'required'    => true
            ])
            ->add('upload', SubmitType::class, ['label' => $options['method'] === 'PUT' ? 'Save' : 'Upload']);

        $this->transformTags($builder);

    }

    /**
     * @param $builder
     * @param $options
     */
    public function buildSeries(FormBuilderInterface $builder, array $options): void
    {

        /**
         * @var Post $series
         */
        $series = $builder->getData();

        if ($options['method'] === 'POST') {
            if (count($series->getChildren()) === 0) {
                for ($i = 0; $i < $options['settings']['min']; $i++) {
                    $series->addChild(new Post());
                }
            }
        }

        $builder
            ->add('title', null, ['required' => true, 'attr' => ['placeholder' => 'Photo Title']])
            ->add('tags', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Tags (Comma Separated. Eg: Tag A, Tag B, etc.)']])
            ->add('description', TextareaType::class, ['required' => false, 'attr' => ['placeholder' => 'Description']])
            ->add('isNude', CheckboxType::class, ['required' => false, 'label' => 'Contains nudity?'])
            ->add('isGreyscale', CheckboxType::class, ['required' => false, 'label' => 'Black and white image?'])
            ->add('category', EntityType::class, [
                'class'       => Category::class,
                'placeholder' => 'Select Category *',
                'required'    => true
            ])
            ->add('upload', SubmitType::class, ['label' => $options['method'] === 'PUT' ? 'Save' : 'Upload']);

        if ($builder->getData()->getStatus() === PostStatusEnum::NEW) {

            $builder->add('children', CollectionType::class, [
                'prototype'          => true,
                'allow_add'          => true,
                'allow_delete'       => true,
                'allow_extra_fields' => true,
                'allow_file_upload'  => true,
                'entry_type'         => __CLASS__,
                'constraints'        => [new Count(['min' => $options['settings']['min'], 'max' => $options['settings']['max']])],
                'entry_options'      => ['type' => 0, 'method' => $options['method'], 'settings' => $options['settings']]
            ]);

        }

        $this->transformTags($builder);

    }

    /**
     * @param $builder
     * @param $options
     */
    public function buildSeriesItem(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {

            $builder = $event->getForm();
            $data = $event->getData();

            $builder->add('title', null, ['required' => false, 'attr' => ['placeholder' => 'Photo Title']]);

            if ($data === null || $data->getImage()->getUrl() === null) {
                $builder->add('image', ImageType::class, ['type' => $options['type'], 'method' => $options['method']]);
            }

        });

    }

    /**
     * @param $builder
     * @param $options
     */
    public function buildProductItem(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('image', ImageType::class, ['type' => $options['type'], 'configuration' => $options['configuration']]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildSeriesFromPublished(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', null, ['required' => true, 'attr' => ['placeholder' => 'Photo Title']])
            ->add('category', EntityType::class, ['class' => Category::class, 'placeholder' => 'Select Category'])
            ->add('tags', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Tags (Comma Separated. Eg: Tag A, Tag B, etc.)']])
            ->add('description', TextareaType::class, ['required' => false, 'attr' => ['placeholder' => 'Description']])
            ->add('isNude', CheckboxType::class, ['required' => false, 'label' => 'Contains nudity?'])
            ->add('isGreyscale', CheckboxType::class, ['required' => false, 'label' => 'Black and white image?'])
            ->add('create', SubmitType::class)
            ->add('position', HiddenType::class);

        //If post is for curate do not allow to change images
        if ($builder->getData()->getStatus() === PostStatusEnum::NEW) {

            $qb = function (bool $limit = true, ?string $ids = null) use ($builder) {
                return function (PostRepository $repository) use ($builder, $limit, $ids) {

                    $parent = $builder->getData()->getId();

                    $user = $this->storage->getToken()->getUser();

                    $qb = $repository->createQueryBuilder('p')
                        ->select(['p', 'CASE WHEN p.parent = :parent THEN p.position ELSE 200 END AS HIDDEN position'])
                        ->where('p.type = 1')
                        ->andWhere('p.status > 2')
                        ->andWhere('p.parent is null or p.parent = :parent')
                        ->andWhere('p.user = :user')
                        ->andWhere('p.isDeleted = false')
                        ->andWhere('p.showDate < :now')
                        ->setParameter('now', new \DateTime())
                        ->setParameter('parent', $parent)
                        ->setParameter('user', $user)
                        ->orderBy('position', 'ASC')
                        ->addOrderBy('p.id', 'DESC');

                    if ($ids !== null) {
                        $qb->andWhere('p.id IN(:ids)')
                            ->setParameter('ids', explode(',', $ids));
                    }

                    if ($limit) {
                        $qb->setMaxResults(20);
                    }

                    return $qb;
                };
            };

            $children = [
                'by_reference'  => false,
                'class'         => Post::class,
                'multiple'      => true,
                'expanded'      => true,
                'query_builder' => $qb(true),
                'constraints'   => [
                    new Count(['min'        => $options['settings']['min'],
                               'minMessage' => 'Series must contain {{ limit }} or more.',
                               'max'        => $options['settings']['max'],
                               'maxMessage' => 'Series must be no more than {{ limit }}'
                    ])
                ],
            ];

            $builder->add('children', EntityType::class, $children);

            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($children, $qb, $options) {

                $builder = $event->getForm();

                $data = $event->getData();

                if (count($data['children']) < $options['settings']['min']) {
                    return;
                }

                $children['query_builder'] = $qb(false, $data['position']);

                $builder->remove('children');

                $builder->add('children', EntityType::class, $children);
            });
        }

        $this->transformTags($builder);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildSeriesFromCurator(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['required' => false, 'attr' => ['placeholder' => 'Photo Title']])
            ->add('category', EntityType::class, ['class' => Category::class, 'placeholder' => 'Select Category'])
            ->add('tags', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Tags (Comma Separated. Eg: Tag A, Tag B, etc.)']])
            ->add('description', TextareaType::class, ['required' => false, 'attr' => ['placeholder' => 'Description']])
            ->add('isNude', CheckboxType::class, ['required' => false, 'label' => 'Contains nudity?'])
            ->add('isGreyscale', CheckboxType::class, ['required' => false, 'label' => 'Black and white image?'])
            ->add('create', SubmitType::class)
            ->add('position', HiddenType::class);

        $qb = function (bool $limit = true, ?string $ids = null) use ($builder) {

            return function (PostRepository $repository) use ($builder, $limit, $ids) {

                $parent = $builder->getData()->getId();

                $qb = $repository->createQueryBuilder('p')
                    ->select(['DISTINCT p', 'CASE WHEN p.parent = :parent THEN p.position ELSE 200 END AS HIDDEN position'])
                    ->where('p.type = 1')
                    ->andWhere('p.parent is null or p.parent = :parent')
                    ->andWhere('p.status IN (3,4)')
                    ->andWhere('p.isDeleted = false')
                    ->setParameter('parent', $parent)
                    ->orderBy('position', 'ASC')
                    ->addOrderBy('p.sentForCurateDate', 'DESC');

                if ($ids !== null) {
                    $qb->andWhere('p.id IN(:ids)')
                        ->setParameter('ids', explode(',', $ids));
                }

                if ($limit) {
                    $qb->setMaxResults(20);
                }

                return $qb;
            };

        };

        $children = [
            'by_reference'  => false,
            'class'         => Post::class,
            'multiple'      => true,
            'expanded'      => true,
            'query_builder' => $qb(true),
            'constraints'   => [
                new Count(['min'        => $options['settings']['min'],
                           'minMessage' => 'Series must contain {{ limit }} or more.',
                           'max'        => $options['settings']['max'],
                           'maxMessage' => 'Series must be no more than {{ limit }}'
                ])
            ],
        ];

        $builder->add('children', EntityType::class, $children);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($children, $qb, $options) {

            $builder = $event->getForm();

            $data = $event->getData();

            if (count($data['children']) < $options['settings']['min']) {
                return;
            }

            $children['query_builder'] = $qb(false, $data['position']);

            $builder->remove('children');

            $builder->add('children', EntityType::class, $children);

        });

        $this->transformTags($builder);
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function transformTags(FormBuilderInterface $builder): void
    {
        $builder->get('tags')->addModelTransformer(new CallbackTransformer(static function ($tags) {

            $data = [];

            /**
             * @var Tag $tag
             */
            foreach ($tags as $tag) {
                $data[] = $tag->getName();
            }

            return implode(',', $data);

        }, function ($tags) {

            $tags = explode(',', trim($tags));
            $tags = array_unique($tags);

            $collection = new ArrayCollection();

            $tagsRepository = $this->em->getRepository(Tag::class);

            foreach ($tags as $tag) {

                $entity = $tagsRepository->findOneBy(['name' => trim($tag)]);

                if ($entity) {
                    $collection->add($entity);
                } else {
                    $entity = new Tag();
                    $entity->setName(trim($tag));

                    $this->em->persist($entity);

                    $collection->add($entity);
                }

            }

            return $collection;
        }));
    }
}
