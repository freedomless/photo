<?php

namespace App\Form\Type;

use App\Entity\User\Settings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isPortfolioVisible')
            ->add('isEmailNotificationAllowed')
            ->add('isWebNotificationAllowed')
            ->add('isPushNotificationAllowed')
            ->add('showNudes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Settings::class,
        ]);
    }
}
