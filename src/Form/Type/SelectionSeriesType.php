<?php

namespace App\Form\Type;

use App\Entity\Photo\SelectionSeries;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class SelectionSeriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');


        $builder
            ->add('imageCover', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new Image(['groups' => ['create','update']])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'empty_data' => new SelectionSeries(),
            'data_class' => SelectionSeries::class,
            'attr' => [
                'novalidate' => true
            ]
        ]);
    }
}
