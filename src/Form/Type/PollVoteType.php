<?php

namespace App\Form\Type;

use App\Entity\Poll\PollChoice;
use App\Repository\Poll\PollRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class PollVoteType extends AbstractType
{
    private $pollRepository;

    private $router;

    public function __construct(
        PollRepository $pollRepository,
        RouterInterface $router
    )
    {
        $this->pollRepository = $pollRepository;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $activePoll = $this->pollRepository->findOneActivePoll();

        $builder
            ->setAction($this->router->generate('api_poll_vote'))
            ->add('choices', EntityType::class, [
                'constraints' => [
                    new NotNull(['message' => 'You should select an option to Vote!'])
                ],
                'class' => PollChoice::class,
                'choice_label' => 'title',
                'required' => true,
                'choices' => $activePoll->getChoices(),
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => true,
            'attr' => [
                'novalidate' => false,
            ],
        ]);
    }
}