<?php

namespace App\Repository;

use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use App\Helper\SerializationTrait;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method SelectionSeries|null find($id, $lockMode = null, $lockVersion = null)
 * @method SelectionSeries|null findOneBy(array $criteria, array $orderBy = null)
 * @method SelectionSeries[]    findAll()
 * @method SelectionSeries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SelectionSeriesRepository extends BaseRepository
{
    use SerializationTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SelectionSeries::class);
    }

    /**
     * @param Post $series
     * @return SelectionSeries|null
     * @throws NonUniqueResultException
     */
    public function findOneBySeries(Post $series): ?SelectionSeries
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.post = :post')
            ->setParameter('post', $series)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getVisible()
    {
        return $this->cache->get('selection_series', function () {
            return $this->normalize($this->findOneBy(['visibility' => 1], [], false), ['groups' => 'post']);
        });
    }
}
