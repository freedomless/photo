<?php

namespace App\Repository;

use App\Entity\Configuration;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class ConfigurationRepository
 * @package App\Repository
 */
class ConfigurationRepository extends BaseRepository
{

    /**
     * ConfigurationRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configuration::class);
    }

    /**
     * @return Configuration|null
     * @throws InvalidArgumentException
     */
    public function getConfig()
    {
        return $this->cache->get('configuration', function () {
            return $this->findOneBy([]);
        });
    }

}
