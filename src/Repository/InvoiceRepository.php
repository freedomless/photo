<?php

namespace App\Repository;

use App\Entity\Store\Invoice;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class InvoiceRepository
 * @package App\Repository
 */
class InvoiceRepository extends BaseRepository
{

    /**
     * InvoiceRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Invoice::class); }

    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getLast()
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
