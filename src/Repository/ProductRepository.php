<?php

namespace App\Repository;

use App\Dto\Request\StoreFilterDto;
use App\Dto\Response\ListDto;
use App\Entity\Photo\Post;
use App\Entity\Store\Product;
use App\Entity\User\User;
use App\Helper\PriceCalculator;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository extends BaseRepository
{
    /**
     * @var PriceCalculator
     */
    private PriceCalculator $calculator;

    /**
     * ProductRepository constructor.
     * @param ManagerRegistry $registry
     * @param PriceCalculator $calculator
     */
    public function __construct(ManagerRegistry $registry, PriceCalculator $calculator)
    {
        parent::__construct($registry, Product::class);
        $this->calculator = $calculator;
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return ListDto|PaginationInterface
     */
    public function getProductPosts(User $user, int $page, int $limit)
    {
        $qb = $this->_em->getRepository(Post::class)
            ->createQueryBuilder('p')
            ->leftJoin('p.product', 'pr')
            ->where('p.isDeleted = false')
            //->andWhere('p.parent is null')
            ->andWhere('p.user = :user')
            ->andWhere('pr is not null')
            ->andWhere('p.deletionDate > :now or p.deletionDate is null')
            ->setParameter('now', new DateTime())
            ->setParameter('user', $user)
            ->orderBy('pr.id', 'desc');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param StoreFilterDto $filters
     * @param int $page
     * @param int $limit
     * @param bool $series
     */
    public function getAllFiltered(
        StoreFilterDto $filters,
        int $page = 1,
        int $limit = 20,
        bool $series = false
    ) {
        $qb = $this->_em->getRepository(Post::class)
            ->createQueryBuilder('ps')
            ->select(['DISTINCT p', 'ps', 'i'])
            ->join('ps.product', 'p')
            ->join('ps.image', 'i')
            ->where('p.status = 3')
            ->andWhere('ps.deletionDate IS NULL OR ps.deletionDate > :now')
            ->setParameter('now', new DateTime());

        if ($series) {
            $qb->andWhere('ps.type > 1');
        } else {
            $qb->andWhere('ps.type = 1');
        }

        if ($filters->getCheapest()) {
            $qb->orderBy('p.commission', $filters->getCheapest() === '1' ? 'ASC' : 'DESC');
        } else {
            if ($filters->getMostLiked()) {
                $qb->leftJoin('ps.favorites', 'fv')
                    ->select(['COUNT(fv) AS HIDDEN likes', 'p', 'ps', 'i'])
                    ->orderBy('likes', $filters->getMostLiked() === '1' ? 'DESC' : 'ASC')
                    ->groupBy('ps.id');
            } else {
                if ($filters->getMostSold()) {
                    $qb->leftJoin('p.orderItems', 'ord')
                        ->select(['COUNT(ord) AS HIDDEN orders', 'p', 'ps', 'i'])
                        ->orderBy('orders', $filters->getMostSold() === '1' ? 'DESC' : 'ASC')
                        ->groupBy('p.id');
                } else {
                    $qb->orderBy(
                        'p.approvedOn',
                        $filters->getNewest() === '1' || $filters->getNewest() === null ? 'DESC' : 'ASC '
                    );
                }
            }
        }

        if ($filters->getUser()) {
            $qb->andWhere('ps.user = :user')
                ->setParameter('user', $filters->getUser());
        }

        $this->filter($qb, $filters);


        $posts = $this->paginate($qb, $page, $limit);


        foreach ($posts as $post) {
            $post->getProduct()->setTotal($this->calculator->calculatePrice($post->getProduct())['total']);
        }

        return $posts;
    }

    /**
     * @param QueryBuilder $qb
     * @param StoreFilterDto $filters
     */
    private function filter(QueryBuilder $qb, StoreFilterDto $filters): void
    {
        if ($filters->getOrientation()) {
            $qb->leftJoin('i.orientation', 'o')
                ->andWhere('o.id = :orientation')
                ->setParameter('orientation', $filters->getOrientation());
        }

        if ($filters->getGreyscale() !== null) {
            $qb->andWhere('ps.isGreyscale = :greyscale');
            $qb->setParameter('greyscale', $filters->getGreyscale());
        }

        if ($filters->getCategories()) {
            $qb->andWhere('ps.category IN(:categories)');
            $qb->setParameter('categories', explode(',', $filters->getCategories()));
        }

        if ($filters->getSeries() === '1') {
            $qb->andWhere('ps.parent is not null');
        } else {
            if ($filters->getSeries() === '0') {
                $qb->andWhere('ps.parent is null');
            }
        }

        if ($filters->getNude() === '1') {
            $qb->andWhere('ps.isNude = true');
        } else {
            if ($filters->getNude() === '0') {
                $qb->andWhere('ps.isNude = false');
            }
        }

        if ($filters->getKeyword() !== null) {
            $qb->leftJoin('ps.tags', 'tg');
            $qb->leftJoin('ps.user', 'u');
            $qb->leftJoin('u.profile', 'pr');
            $qb->andWhere(
                'LOWER(u.username) like LOWER(:keyword) OR LOWER(pr.firstName) like LOWER(:keyword) OR LOWER(pr.lastName) like LOWER(:keyword) OR LOWER(tg.name) like LOWER(:keyword) OR LOWER(ps.title) like LOWER(:keyword)'
            );
            $qb->setParameter('keyword', "%{$filters->getKeyword()}%");
        }
    }

}
