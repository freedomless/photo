<?php

namespace App\Repository;

use App\Entity\Store\CartItem;
use App\Entity\User\User;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CartRepository
 * @package App\Repository
 */
class CartItemRepository extends BaseRepository
{
    /**
     * CartRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, CartItem::class); }

    /**
     * @param User $user
     * @param int|null $product
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function findUserProduct(User $user, ?int $product)
    {
        return $this->createQueryBuilder('cp')
            ->where('cp.id = :product')
            ->setParameter('product', $product)
            ->join('cp.cart', 'c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
