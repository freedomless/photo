<?php

namespace App\Repository;

use App\Entity\Photo\Award;
use App\Helper\SerializationTrait;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Cache\CacheItemInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="t_awards")
 */
class AwardRepository extends BaseRepository
{
    use SerializationTrait;

    /**
     * AwardRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Award::class);
    }

    /**
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function findCurrentPhotoOfTheDay()
    {

        return $this->cache->get('photo_of_the_day', function (CacheItemInterface $item) {

            $expires = strtotime("tomorrow", time()) - time();

            $item->expiresAfter($expires);

            $today = new DateTime('now');

            $post = $this->createQueryBuilder('a')
                ->join('a.type', 't')
                ->where('t.name = :name')
                ->setParameter('name', 'photo_of_the_day')
                ->andWhere('a.createdAt >= :date_start')
                ->andWhere('a.createdAt <= :date_end')
                ->setParameter('date_start', $today->format('Y-m-d 00:00:00'))
                ->setParameter('date_end', $today->format('Y-m-d 23:59:59'))
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

            return $this->normalize($post,['groups' => ['post','award']]);

        });
    }

    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findLastPhotoInQueue()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.createdAt > :now')
            ->setParameter('now', new DateTime('now'))
            ->join('a.type', 't')
            ->andWhere('t.name = :name')
            ->setParameter('name', 'photo_of_the_day')
            ->distinct()
            ->addOrderBy('a.createdAt', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param DateTime|null $startDate
     * @return mixed
     * @throws Exception
     */
    public function findUpcomingPhotos(DateTime $startDate = null)
    {
        if (!$startDate) {
            $startDate = new DateTime('now');
        }

        return $this->createQueryBuilder('a')
            ->select(['a', 'p', 'i', 'at'])
            ->join('a.type', 't')
            ->where('t.name = :name')
            ->setParameter('name', 'photo_of_the_day')
            ->join('a.post', 'p')
            ->join('p.image', 'i')
            ->join('a.type', 'at')->andWhere('a.createdAt > :now')
            ->distinct()
            ->setParameter('now', $startDate)
            ->addOrderBy('a.createdAt', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function findOneByIdOrCurrent(int $id)
    {
        $award = $this->createQueryBuilder('a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->andWhere('a.createdAt < :now')
            ->setParameter('now', new DateTime())
        ->setMaxResults(1)
        ->getQuery()->getOneOrNullResult();

        if($award){
            return $award;
        }


        $today = new DateTime('now');

        $award = $this->createQueryBuilder('a')
            ->join('a.type', 't')
            ->where('t.name = :name')
            ->setParameter('name', 'photo_of_the_day')
            ->andWhere('a.createdAt >= :date_start')
            ->andWhere('a.createdAt <= :date_end')
            ->setParameter('date_start', $today->format('Y-m-d 00:00:00'))
            ->setParameter('date_end', $today->format('Y-m-d 23:59:59'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return  $award;
    }
}
