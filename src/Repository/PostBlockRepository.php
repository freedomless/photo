<?php

namespace App\Repository;

use App\Entity\Photo\Post;
use App\Entity\Photo\PostBlock;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostBlock|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostBlock|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostBlock[]    findAll()
 * @method PostBlock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostBlockRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostBlock::class);
    }

    public function getActive(Post $post)
    {
        return $this->createQueryBuilder('pb')
            ->where('pb.post = :post')
            ->setParameter('post', $post)
            ->andWhere('pb.endDate > :now')
            ->setParameter('now', new \DateTime())
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }
}
