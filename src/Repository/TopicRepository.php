<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Forum\Section;
use App\Entity\Forum\Topic;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TopicRepository
 * @package App\Repository
 */
class TopicRepository extends BaseRepository
{
    /**
     * @var string
     */
    protected string $type = 'web';

    /**
     * TopicRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Topic::class);
    }

    /**
     * @param Section $section
     * @param int $page
     * @param int $limit
     *
     * @return ListDto|mixed
     */
    public function getTopics(Section $section, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.section =:section')
            ->orderBy('t.updatedOn', 'DESC')
            ->setParameter('section',$section);

        return $this->paginate($qb, $page, $limit);
    }
}
