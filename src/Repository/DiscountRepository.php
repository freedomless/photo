<?php

namespace App\Repository;

use App\Entity\Store\Discount;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class DiscountRepository
 * @package App\Repository
 */
class DiscountRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Discount::class); }

    public function findForSeries(int $total)
    {
        return $this->createQueryBuilder('d')
            ->where('d.isEnabled = true')
            ->andWhere('d.series = true')
            ->andWhere('d.total = :total')
            ->setParameter('total', $total)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
