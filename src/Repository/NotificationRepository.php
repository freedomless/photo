<?php

namespace App\Repository;

use App\Entity\Notification\Notification;
use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class NotificationRepository extends BaseRepository
{

    /**
     * PostRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function getWebNotifications(User $user, int $page, int $limit)
    {

        $qb = $this->createQueryBuilder('n')
            ->join('n.type', 't')
            ->where('t.isWeb = true')
            ->andWhere('n.receiver =:user')
            ->setParameter('user', $user)
            ->andWhere('n.isDeleted = false')
            ->orderBy('n.createdAt', 'DESC');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param $user
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getUnseenCount($user)
    {
        return (int)$this->createQueryBuilder('n')
            ->select(['COUNT(n.id)'])
            ->join('n.type', 't')
            ->where('n.isSeen = false')
            ->andWhere('n.receiver =:user')
            ->andWhere('t.isWeb = true')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

}
