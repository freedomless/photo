<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Photo\Post;
use App\Entity\User\Founder;
use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends BaseRepository
{
    /**
     * PostRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $query
     * @param User|null $user
     * @return mixed
     */
    public function search(string $query, ?User $user)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.profile', 'p')
            ->where('LOWER(u.username) LIKE LOWER(:query) OR LOWER(CONCAT(p.firstName,\' \', p.lastName)) LIKE LOWER(:query)')
            ->andWhere('u.isDeleted = false')
            ->setParameter('query', '%' . $query . '%')
            ->setMaxResults(20);


        if ($user !== null) {
            $qb->andWhere('u != :user')->setParameter('user', $user);
        }

        return $qb->getQuery()->getResult();
    }


    /**
     * @param User $user
     * @return array
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getAccountStats(User $user)
    {
        $published = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->where('p.status > 2')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.user =:user')
            ->andWhere('p.showDate <= :now')
            ->setParameter('user', $user)
            ->setParameter('now', new DateTime())
            ->getQuery()->getSingleScalarResult();

        $portfolio = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $single_published = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->where('p.status > 2')
            ->andWhere('p.type = 1')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.user =:user')
            ->andWhere('p.showDate <= :now')
            ->setParameter('user', $user)
            ->setParameter('now', new DateTime())
            ->getQuery()->getSingleScalarResult();

        $single_portfolio = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.type = 1')
            ->andWhere('p.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $series_published = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.type > 1')
            ->andWhere('p.status > 2')
            ->andWhere('p.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $series_portfolio = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.type > 1')
            ->andWhere('p.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $albums = $this->createQueryBuilder('u')
            ->select(['COUNT(a.id)'])
            ->join('u.albums', 'a')
            ->andWhere('a.isDeleted = false')
            ->andWhere('a.user =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $followers = $this->createQueryBuilder('u')
            ->select(['COUNT(f.id)'])
            ->join('u.followers', 'f')
            ->andWhere('f.followed =:user')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $favorites = $this->createQueryBuilder('u')
            ->select(['COUNT(fav.id)'])
            ->join('u.favorites', 'fav')
            ->join('fav.post', 'p')
            ->where('u =:user')
            ->andWhere('p.status > 2')
            ->andWhere('p.isDeleted = false')
            ->setParameter('user', $user)
            ->getQuery()->getSingleScalarResult();

        $followed = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.following', 'f')
            ->join('f.followed', 'fl')
            ->join('fl.posts', 'p')
            ->andWhere('u = :user')
            ->andWhere('p.status > 2')
            ->andWhere('p.isDeleted = false')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        $products_single = $this->createQueryBuilder('u')
            ->select(['COUNT(distinct p.id)'])
            ->join('u.posts', 'p')
            ->join('p.product', 'pr')
            ->where('u = :user')
            ->andWhere('p.type = 1')
            ->andWhere('p.status = 3')
            ->andWhere('p.deletionDate IS NULL OR p.deletionDate > :now')
            ->setParameter('now', new DateTime())
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        $products_series = $this->createQueryBuilder('u')
            ->select(['COUNT(p.id)'])
            ->join('u.posts', 'p')
            ->join('p.product', 'pr')
            ->where('u = :user')
            ->andWhere('p.type > 1')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        $products = $products_series + $products_single;

        return compact('published', 'portfolio','single_portfolio','single_published','series_published',
                       'series_portfolio', 'albums', 'followers', 'favorites', 'followed', 'products', 'products_single', 'products_series');
    }

    /**
     * @return mixed
     */
    public function getGroupedUsersByCountries()
    {
        return $this->createQueryBuilder('u')
            ->join('u.profile', 'pr')
            ->select(['COUNT(pr.nationality) as total', 'pr'])
            ->where('pr.nationality IS NOT NULL')
            ->andWhere('u.isDeleted = false')
            ->groupBy('pr.nationality')
            ->orderBy('pr.nationality', 'ASC')
            ->getQuery()
            ->getResult();

    }

    /**
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getGroupedUsersWithoutCountry()
    {
        return $this->createQueryBuilder('u')
            ->select(['COUNT(u)'])
            ->join('u.profile', 'p')
            ->where('p.nationality IS NULL')
            ->andWhere('u.isDeleted = false')
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $iso
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getUsersByCountry(string $iso, int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.profile', 'p')
            ->leftJoin('p.nationality', 'c')
            ->where('c.iso =:iso')
            ->andWhere('u.isDeleted = false')
            ->setParameter('iso', $iso);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return ListDto|PaginationInterface
     */
    public function getUsersWithoutCountry(int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.profile', 'p')
            ->where('p.nationality IS NULL')
            ->andWhere('u.isDeleted = false');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getFollowingUsers(User $user, int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.followers', 'f')
            ->where('f.follower = :user')
            ->andWhere('u.isDeleted = false')
            ->setParameter('user', $user)
            ->andWhere('u.roles not like :role_curator')
            ->andWhere('u.roles not like :role_admin')
            ->setParameter('role_curator', '%ROLE_CURATOR%')
            ->setParameter('role_admin', '%ROLE_SUPER_ADMIN%');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getTopRankUsers(int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('u')
            ->select(['u'])
            ->leftJoin('u.points', 'p')
            ->leftJoin('p.type', 't')
            ->andWhere('u.isDeleted = false')
            ->groupBy('u.id', 'p.user')
            ->orderBy('SUM(CASE WHEN t.points IS NULL THEN 0 ELSE t.points END)', 'DESC');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getUsersAlphabetically(int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('u')
            ->select(['u', 'pr'])
            ->leftJoin('u.profile', 'pr')
            ->andWhere('u.roles not like :role_curator')
            ->andWhere('u.roles not like :role_admin')
            ->andWhere('u.isDeleted = false')
            ->setParameter('role_curator', '%ROLE_CURATOR%')
            ->setParameter('role_admin', '%ROLE_SUPER_ADMIN%')
            ->groupBy('u.id')
            ->orderBy('pr.firstName', 'ASC')
            ->addOrderBy('pr.lastName', 'ASC')
            ->addOrderBy('u.username', 'ASC');

        $this->_em->getClassMetadata(User::class)->setAssociationOverride('profile', ['fetch' => 3]);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function getFounders(int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.roles like :role')
            ->andWhere('u.isDeleted = false')
            ->setParameter('role','%ROLE_FOUNDER%');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param $user
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getFollowers(User $user, int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.following', 'f')
            ->andWhere('f.followed = :user')
            ->setParameter('user', $user->getId())
            ->andWhere('u.isDeleted = false')
            ->andWhere('u.roles not like :role_curator')
            ->andWhere('u.roles not like :role_admin')
            ->setParameter('role_curator', '%ROLE_CURATOR%')
            ->setParameter('role_admin', '%ROLE_SUPER_ADMIN%');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param $post
     * @return ListDto|PaginationInterface
     */
    public function getFavorites(Post $post)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.favorites', 'f')
            ->join('f.post', 'p')
            ->where('p =:post')
            ->andWhere('u.isDeleted = false')
            ->setParameter('post', $post);

        return $this->paginate($qb, 0, 0);
    }

}
