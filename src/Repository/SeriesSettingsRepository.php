<?php

namespace App\Repository;

use App\Entity\Photo\SeriesSettings;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class SeriesSettingsRepository
 * @package App\Repository
 */
class SeriesSettingsRepository extends BaseRepository
{
    /**
     * SeriesSettingsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, SeriesSettings::class); }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getSettings()
    {
        return $this->cache->get('ss', function () {
            return $this->normalize($this->findOneBy([]));
        });
    }
}
