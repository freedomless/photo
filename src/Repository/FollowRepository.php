<?php

namespace App\Repository;

use App\Entity\User\Follow;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class FollowRepository
 * @package App\Repository
 */
class FollowRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Follow::class); }
}
