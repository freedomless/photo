<?php

namespace App\Repository;

use App\Entity\User\Settings;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class SettingsRepository
 * @package App\Repository
 */
class SettingsRepository extends BaseRepository
{

    /**
     * SettingsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Settings::class);
    }

    /**
     * @param $user
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getSettings($user)
    {
        if(!$user){
            return  null;
        }

        return $this->cache->get('settings_' . (is_int($user) ? $user : $user->getId()), function () use ($user) {

            $settings = $this->findOneBy(compact('user'), [], false);

            return $this->normalize($settings, ['groups' => 'settings']);
        });
    }
}
