<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Exception\ApiException;
use App\Exception\ExceptionCodes;
use App\Helper\CacheTrait;
use App\Helper\DispatcherTrait;
use App\Helper\PaginatorTrait;
use App\Helper\SerializationTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BaseRepository
 *
 * @ORM\Entity
 * @ORM\Table(name="base_repository")
 *
 */
class BaseRepository extends ServiceEntityRepository
{

    use DispatcherTrait,
        PaginatorTrait,
        SerializationTrait,
        CacheTrait;

    /**
     * BaseRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param null $entityClass
     */
    public function __construct(ManagerRegistry $registry, $entityClass = null)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @param bool $exception
     * @return object|null
     */
    public function find($id, $lockMode = null, $lockVersion = null,bool $exception = true)
    {
        $entity = parent::find($id, $lockMode, $lockVersion);

        if ($entity === null && $exception) {
            throw new NotFoundHttpException();
        }

        return $entity;
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param bool $exception
     * @return object|null
     */
    public function findOneBy(array $criteria, array $orderBy = null,bool $exception = true)
    {

        $entity = parent::findOneBy($criteria, $orderBy);

        if ($entity === null && $exception) {
            throw new NotFoundHttpException();
        }

        return $entity;
    }

}
