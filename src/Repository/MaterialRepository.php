<?php

namespace App\Repository;

use App\Entity\Store\Material;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class MaterialRepository
 * @package App\Repository
 */
class MaterialRepository extends BaseRepository
{

    /**
     * MaterialRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Material::class); }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getMaterials()
    {
        return $this->cache->get('materials', function () {
            return $this->normalize($this->findBy(['isEnabled' => true],['price' => 'ASC']));
        });
    }
}
