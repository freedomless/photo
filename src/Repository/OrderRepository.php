<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Store\Order;
use App\Entity\User\User;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class OrderRepository
 * @package App\Repository
 */
class OrderRepository extends BaseRepository
{
    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     * @param null $entityClass
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Order::class); }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return ListDto|PaginationInterface
     */
    public function getSuccessfulByUser(User $user, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('o')
            ->join('o.transaction', 'trn')
            ->where('o.customer = :user')
            ->andWhere('trn.status = 1')
            ->setParameter('user', $user);

        $this->setPaginationType('web');

        return $this->paginate($qb, $page, $limit);
    }

}
