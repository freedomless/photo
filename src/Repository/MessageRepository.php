<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Messenger\Message;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MessageRepository
 * @package App\Repository
 */
class MessageRepository extends BaseRepository
{
    /**
     * MessageRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return ListDto
     */
    public function getMainMessages(User $user, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('m')
            ->where('m.sender =:user or m.receiver =:user')
            ->setParameter('user', $user)
            ->andWhere('m.parent IS NULL')
            ->andWhere('m.isDeleted = false')
            ->orderBy('m.id', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit)
            ->getQuery()->getResult();

        $qb = array_values( array_filter($qb, function ($res) use ($user) {
            return !($res->getSender()->getId() === $user->getId() && $res->getIsDeletedForSender() === true) &&
                !($res->getReceiver()->getId() === $user->getId() && $res->getIsDeletedForReceiver() === true);
        }));

        return $this->toList($qb, count($qb), $page, $limit);
    }

    /**
     * @param User $user
     * @param int $id
     * @param int $page
     * @param int $limit
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMessage(User $user, int $id, int $page = 1, $limit = 20)
    {
        $result = $this->createQueryBuilder('m')
            ->where('m.id =:id')
            ->setParameter('id', $id)
            ->andWhere('m.sender =:user OR m.receiver =:user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($result !== null) {
            $children = new ArrayCollection($result->getChildren()->slice(($page - 1) * $limit, $limit));
            $result->setChildren($children);
        }

        return $result;
    }

}
