<?php

namespace App\Repository;

use App\Entity\Forum\Section;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class SectionRepository
 * @package App\Repository
 */
class SectionRepository extends BaseRepository
{
    protected string $type = 'web';

    /**
     * SectionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Section::class);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     */
    public function getSections(int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('s');

        return $this->paginate($qb, $page, $limit);
    }
}
