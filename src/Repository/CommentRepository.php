<?php

namespace App\Repository;

use App\Entity\Photo\Comment;
use App\Entity\Photo\Post;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

class CommentRepository extends BaseRepository
{

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Comment::class);
    }

    public function getByPost($post, ?int $comment, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('c')
            ->join('c.post','p')
            ->where('p.id = :post')
            ->andWhere('c.isDeleted = false')
            ->orderBy('c.id', 'DESC')
            ->setParameter('post', $post);

        $result = $this->paginate($qb, $page, $limit);

        if ($comment) {

            $ordered = false;

            $comments = $result->getData();

            foreach ($comments as $i => $item) {
                if ($item->getId() === $comment) {
                    $ordered = true;
                    $selected = array_splice($comments, $i, 1)[0];
                    break;
                }
            }

            if (!$ordered) {

                $selected = $this->findOneBy(['post' => $post, 'id' => $comment]);

                $comments = $result->getData();
            }
            array_unshift($comments, $selected);
            $result->setData(array_values(array_filter($comments)));
        }

        return $result;
    }
}
