<?php

namespace App\Repository;

use App\Dto\Request\PostFilterDto;
use App\Dto\Request\UserFilterDto;
use App\Dto\Response\ListDto;
use App\Entity\Configuration;
use App\Entity\Photo\Album;
use App\Entity\Photo\Category;
use App\Entity\Photo\Favorite;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends BaseRepository
{
    /**
     * @var Configuration
     */
    private ?Configuration $configuration;

    /**
     * PostRepository constructor.
     * @param ManagerRegistry $registry
     * @param ConfigurationRepository $configuration
     * @throws InvalidArgumentException
     */
    public function __construct(ManagerRegistry $registry, ConfigurationRepository $configuration)
    {
        parent::__construct($registry, Post::class);
        $this->configuration = $configuration->getConfig();
    }

    /**
     * @param int $page
     * @param int $limit
     * @param UserFilterDto $filters
     * @return PaginationInterface
     */
    public function adminGetForCurate(int $page, int $limit, UserFilterDto $filters)
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.curates', 'c')
            ->join('p.image', 'i')
            ->select(['SUM(c.rating) / COUNT(c.id) as rating', 'p as post', 'COUNT(c) as total'])
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.status = 1')
            ->andWhere('p.parent is null')
            ->groupBy('p')
            ->orderBy('p.sentForCurateDate', 'ASC');

        $this->curateFilter($qb, $filters);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @param UserFilterDto $filters
     * @return PaginationInterface
     */
    public function adminGetPublished(int $page, int $limit, UserFilterDto $filters)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->andWhere('p.parent is null')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.status > 2')
            ->andWhere('p.parent is null')
            ->orderBy('p.showDate', 'DESC');

        $this->curateFilter($qb, $filters);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @param UserFilterDto $filters
     * @return PaginationInterface
     */
    public function adminGetRejected(int $page, int $limit, UserFilterDto $filters)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->andWhere('p.status = 2')
            ->andWhere('p.parent is null')
            ->orderBy('p.curatedAt', 'DESC');

        $this->curateFilter($qb, $filters);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return int|mixed|string
     */
    public function adminGetSeries(int $page, int $limit)
    {
        return $this->createQueryBuilder('p')
            ->where('p.type = 4')
            ->getQuery()->getResult();
    }

    /**
     * @param Post $post
     *
     * @return int
     */
    public function adminGetNextForCurate(Post $post)
    {
        return $this->createQueryBuilder('p')
            ->where('p.status = 1')
            ->andWhere('p.sentForCurateDate > :date')
            ->setParameter('date', $post->getSentForCurateDate())
            ->orderBy('p.sentForCurateDate', 'asc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Post $post
     *
     * @return int
     */
    public function adminGetPreviousForCurate(Post $post)
    {
        return $this->createQueryBuilder('p')
            ->where('p.status = 1')
            ->andWhere('p.sentForCurateDate < :date')
            ->setParameter('date', $post->getSentForCurateDate())
            ->orderBy('p.sentForCurateDate', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findOneByIdOrSlug($id)
    {
        $where = filter_var($id, FILTER_VALIDATE_INT)
            ? 'p.id = :id' : 'p.slug = :id';

        return $this->createQueryBuilder('p')
            ->andWhere($where)
            ->setParameter('id', $id)
            ->andWhere('p.isDeleted = false')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param array $posts
     * @return array
     */
    public function getFavoritePostsByUser(User $user, array $posts)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id')
            ->leftJoin('p.favorites', 'f')
            ->where('f.user = :user')
            ->andWhere('f.post in (:posts)')
            ->andWhere('p.isDeleted = false')
            ->setParameters(compact('user', 'posts'))
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * @param User $user
     * @param array $posts
     * @return array
     */
    public function getFollowedByPosts(User $user, array $posts)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id')
            ->join('p.user', 'u')
            ->leftJoin('u.followers', 'f')
            ->where('p.id in (:posts)')
            ->andWhere('f.follower = :user')
            ->andWhere('p.isDeleted = false')
            ->setParameters(compact('user', 'posts'))
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * @param int $page
     * @param int $limit
     * @param PostFilterDto $filter
     * @param bool $series
     *
     * @return array
     *
     * @throws ExceptionInterface
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getLatest(int $page, int $limit, ?PostFilterDto $filter = null, bool $series = false)
    {
        $qb = $this->createQueryBuilder('p')
            ->select(['p', 'i'])
            ->join('p.image', 'i');

        if ($filter) {
            $this->filter($qb, $filter);
        } else {
            $qb->orderBy('p.showDate', 'DESC');
        }

        $this->publishedAndNotDeleted($qb, $series, $filter === null);

        $qb = $qb->getQuery()->setFetchMode(Post::class, 'user', ClassMetadataInfo::FETCH_EAGER)
            ->setFetchMode(Post::class, 'category', ClassMetadataInfo::FETCH_EAGER);

        $groups = ['groups' => ['post', 'api']];

        if ($series) {
            $groups['groups'][] = 'series';
        }

        if ($filter) {
            return $this->normalize($this->paginate($qb, $page, $limit), $groups);
        }

        return $this->getCached(
            $qb,
            $page,
            $limit,
            $series ? 'series' : 'latest',
            function ($result) use ($page, $limit) {

                $last = $this->createQueryBuilder('l')
                    ->where('l.status > 2')
                    ->andWhere('l.parent is null')
                    ->andWhere('l.showDate < :now')
                    ->setParameter('now', new DateTime())
                    ->orderBy('l.showDate', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()->getSingleResult();

                $showDate = $last->getShowDate();

                $span = $this->configuration->getPublishTimespan();

                $showDate->modify('+ ' . $span . 'minutes');

                return strtotime($showDate->format('Y-m-d H:i:s')) - time();

            },
            $groups
        );

    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getPopular(int $page, int $limit)
    {

        $config = $this->getEntityManager()->getRepository(Configuration::class)->findOneBy([]);

        $hoursAgo = new DateTime();

        /** @var $config Configuration */
        $hoursAgo->modify('-' . ($config->getPopularHoursAgo() ?? 48) . ' hours');

        $qb = $this->createQueryBuilder('p')
            ->select(['p', 'i'])
            ->join('p.image', 'i')
            ->join('p.user', 'u')
            ->join('u.profile', 'pr')
            ->leftJoin('p.favorites', 'fr')
            ->groupBy('p.id')
            ->setParameter('hours_ago', $hoursAgo);

        $this->publishedAndNotDeleted($qb);

        $qb->addSelect(['(CASE WHEN p.showDate >= :hours_ago THEN COUNT(fr) ELSE 0 END) AS HIDDEN t']);
        $qb->orderBy('t', 'DESC');
        $qb->addOrderBy('p.showDate', 'DESC');

        $qb = $qb->getQuery()->setFetchMode(Post::class, 'user', ClassMetadataInfo::FETCH_EAGER)
            ->setFetchMode(Post::class, 'category', ClassMetadataInfo::FETCH_EAGER);

        return $this->getCached($qb, $page, $limit, 'popular', 900, ['groups' => ['post']]);
    }

    /**
     * @param User $account
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     * @throws Exception
     */
    public function getPublished($account, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->andWhere('p.user = :account')
            ->setParameter('account', $account);

        $this->publishedAndNotDeleted($qb);

        $this->_em->getClassMetadata(Post::class)->setAssociationOverride('image', ['fetch' => 3]);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param User $account
     * @param int $page
     * @param int $limit
     * @param bool $series
     *
     * @param bool $published
     * @return PaginationInterface
     *
     * @throws InvalidArgumentException
     * @throws MappingException
     */
    public function getPortfolio($account, int $page, int $limit, bool $series = false, bool $published = false)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->andWhere('p.user = :account')
            ->orderBy('p.id', 'DESC')
            ->setParameter('account', $account)
            ->andWhere('p.isDeleted = false');

        if ($series) {
            $qb->andWhere('p.type > 1');
        } else {
            $qb->andWhere('p.type = 1');
        }

        if ($published) {
            $qb->andWhere('p.status > 2');
        }

        $this->_em->getClassMetadata(Post::class)->setAssociationOverride('image', ['fetch' => 3]);

        $key = 'portfolio_' . (is_int($account) ? $account : $account->getId());

        $groups = ['groups' => ['post', 'api']];

        if ($series) {
            $key .= '_series';

            if ($published) {
                $key .= '_published';
            }

            $groups['groups'][] = 'series';
        }

        return $this->getCached($qb, $page, $limit, $key, 900, $groups, $key);
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     * @throws Exception
     */
    public function getFromFollowingUsers(User $user, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->select(
                [
                    'DISTINCT p',
                    'f',
                    'u',
                    'i',
                    'fr',
                ]
            )
            ->join('p.user', 'u')
            ->join('u.followers', 'f')
            ->join('p.image', 'i')
            ->leftJoin('p.favorites', 'fr', Join::WITH, 'fr.user =:user')
            ->andWhere('f.follower = :user')
            ->setParameter('user', $user);

        $this->publishedAndNotDeleted($qb);

        $this->_em->getClassMetadata(Post::class)->setAssociationOverride('user', ['fetch' => 3]);

        return $this->paginate($qb, $page, $limit);

    }

    /**
     * @param Category $category
     * @param int $page
     * @param int $limit
     *
     * @return PaginationInterface
     *
     * @throws Exception
     */
    public function getByCategory(Category $category, int $page, int $limit)
    {

        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.category = :category')
            ->setParameter('category', $category);

        $this->publishedAndNotDeleted($qb);

        return $this->paginate($qb, $page, $limit);

    }

    /**
     * @param string $tag
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     * @throws Exception
     */
    public function getByTag(string $tag, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.tags', 't')
            ->where('LOWER(t.name) = LOWER(:tag)')
            ->setParameter('tag', $tag);

        $this->publishedAndNotDeleted($qb);

        return $this->paginate($qb, $page, $limit);

    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     *
     * @return PaginationInterface
     *
     * @throws Exception
     */
    public function getFavorites(User $user, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->select(
                [
                    'DISTINCT p',
                    'u',
                    'fr',
                ]
            )
            ->join('p.favorites', 'fr')
            ->join('p.image', 'i')
            ->join('p.user', 'u')
            ->where('fr.user = :user')
            ->andWhere('p.isDeleted = false')
            ->orderBy('p.createdAt', 'DESC')
            ->setParameter('user', $user);

        $this->_em->getClassMetadata(Post::class)->setAssociationOverride('user', ['fetch' => 3]);
        $this->_em->getClassMetadata(Post::class)->setAssociationOverride('image', ['fetch' => 3]);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param Album $album
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     */
    public function getByAlbum(Album $album, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->join('p.albums', 'a')
            ->where('a = :album')
            ->setParameter('album', $album)
            ->andWhere('p.isDeleted = false');

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @param PostFilterDto|null $filter
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function getAwarded(int $page, int $limit, ?PostFilterDto $filter = null)
    {
        $tomorrow = new DateTime('tomorrow');

        $qb = $this->createQueryBuilder('p')
            ->join('p.awards', 'a')
            ->orderBy('a.createdAt', 'DESC');

        $this->publishedAndNotDeleted($qb, false, false);

        if ($filter) {
            $this->filter($qb, $filter);
        } else {
            $qb->andWhere('a.createdAt < :tomorrow')
                ->setParameter(':tomorrow', $tomorrow);
        }

        $qb = $qb->getQuery()->setFetchMode(Post::class, 'user', ClassMetadataInfo::FETCH_EAGER)
            ->setFetchMode(Post::class, 'category', ClassMetadataInfo::FETCH_EAGER);

        if ($filter) {
            return $this->normalize($this->paginate($qb, $page, $limit), ['groups' => ['post', 'award', 'api']]);
        }

        return $this->getCached(
            $qb,
            $page,
            $limit,
            'awarded',
            function () {
                return strtotime((new DateTime('tomorrow'))->format('Y-m-d H:i:s')) - time();
            },
            ['groups' => ['post', 'award', 'api']]
        );
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @param bool $series
     * @return PaginationInterface
     */
    public function getByUser(User $user, int $page, int $limit, bool $series = false)
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.curates', 'c')
            ->join('p.image', 'i')
            ->select(['SUM(c.rating) / COUNT(c.id) as rating', 'p as post'])
            ->andWhere('p.user = :user')
            ->setParameter('user', $user)
            ->orderBy('p.id', 'DESC')
            ->groupBy('p.id')
            ->andWhere('p.isDeleted = false');

        if ($series) {
            $qb->andWhere('p.type > 1');
        } else {
            $qb->andWhere('p.type = 1');
        }

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param User $user
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getForMembersCurate(User $user)
    {
        $subQuery = 'SELECT tp.id from App\Entity\Photo\Post tp LEFT JOIN tp.curates tc WHERE tc.users = :user';

        return $this->createQueryBuilder('p')
            ->join('p.image', 'i')
            ->andWhere('p.status = 1')
            ->andWhere('p.type = 1')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.user != :user')
            ->setParameter('user', $user)
            ->leftJoin('p.curates', 'c')
            ->andWhere('p.id NOT IN (' . $subQuery . ')')
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(1)->getQuery()
            ->getOneOrNullResult();

    }

    /**
     * @param User $user
     * @param int $id
     * @return int|mixed|string
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getNotUserPost(User $user, int $id)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.favorites', 'f')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->andWhere('p.user != :user')
            ->andWhere('p.isDeleted = false')
            ->setParameter('user', $user)
            ->andWhere('f is null or f.user != :user')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $query
     * @return mixed
     */
    public function search(string $query)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('LOWER(p.title) LIKE LOWER(:query)')
            ->orWhere('LOWER(p.description) LIKE LOWER(:query)')
            ->andWhere('p.status = 3')
            ->andWhere('p.isDeleted = false')
            ->setParameter('query', '%' . $query . '%')
            ->setMaxResults(20)
            ->andWhere('p.status = 3');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param $post
     * @return bool
     * @throws NonUniqueResultException
     */
    public function isFavorite($user, $post): bool
    {
        $favorite = $this->_em
            ->getRepository(Favorite::class)
            ->createQueryBuilder('f')
            ->where('f.post = :post')
            ->andWhere('f.user = :user')
            ->setParameter('post', $post)
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $favorite !== null;
    }

    /**
     * @param $post
     * @param $direction
     * @param $places
     * @return mixed
     * @throws Exception
     */
    public function getPostsToBeMoved($post, $direction, $places)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.status = 3')
            ->andWhere('p.isDeleted = false')
            ->andWhere('p.parent is null')
            ->andWhere('p.showDate ' . ($direction ? '>' : '<') . ' :show_date AND p.showDate > :now')
            ->setParameter('show_date', $post->getShowDate())
            ->setParameter('now', new DateTime())
            ->groupBy('p.showDate')
            ->orderBy('p.showDate', ($direction ? 'ASC' : 'DESC'))
            ->setMaxResults($places)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    /**
     * @param User $user
     * @param DateTime $interval
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getPostsByIntervalAndForCurate(User $user, DateTime $interval)
    {
        return $this->createQueryBuilder('p')
            ->select(['COUNT(p) as total'])
            ->where('p.sentForCurateDate >= :interval')
            ->setParameter('interval', $interval)
            ->andWhere('p.user =:user')
            ->setParameter('user', $user)
            //->andWhere('p.status = 1')
            ->groupBy('p.user')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Post|null $parent
     * @param User|null $user
     * @param int $page
     * @param int $limit
     * @return ListDto|PaginationInterface
     */
    public function getPostsForSeriesFromExisting(?Post $parent = null, ?User $user = null, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->select(['p', 'CASE WHEN p.parent = :parent THEN p.position ELSE 200 END AS HIDDEN position'])
            ->where('p.type = 1')
            ->andWhere('p.parent is null or p.parent = :parent')
            ->andWhere('p.isDeleted = false')
            ->setParameter('parent', $parent)
            ->orderBy('position', 'ASC')
            ->addOrderBy('p.id', 'DESC');

        $qb->andWhere('p.status > 2');

        if ($user) {
            $qb->andWhere('p.user = :user')->setParameter('user', $user);
        }

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param $posts
     * @param User|null $user
     * @return mixed
     */
    public function getFavoritesAndFollows($posts, ?User $user = null)
    {

        if (count($posts) === 0) {
            return $posts;
        }

        foreach ($posts['items'] as &$post) {
            $post['follow'] = false;
            $post['favorite'] = false;
        }

        $posts['items'] = array_column($posts['items'], null, 'id');

        if ($user) {

            $ids = array_map(
                function ($val) {
                    return $val['id'];
                },
                $posts['items']
            );

            $favorite = $this->getFavoritePostsByUser($user, $ids);

            $follow = $this->getFollowedByPosts($user, $ids);

            foreach ($favorite as $f) {
                $posts['items'][$f['id']]['favorite'] = true;
            }

            foreach ($follow as $f) {
                $posts['items'][$f['id']]['follow'] = true;
            }
        }

        $posts['items'] = array_values($posts['items']);

        return $posts;
    }

    /**
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getLastPublished()
    {
        return $this->createQueryBuilder('p')
            ->where('p.status > 2')
            ->andWhere('p.parent is null')
            ->andWhere('p.isDeleted = false')
            ->orderBy('p.showDate', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param bool $series
     * @param bool $orderByShowDate
     * @throws Exception
     */
    private function publishedAndNotDeleted(QueryBuilder $qb, bool $series = false, bool $orderByShowDate = true)
    {
        $qb->andWhere('p.status > 2');
        $qb->andWhere('p.isDeleted = false');
        $qb->andWhere('p.showDate <= :showDate');
        $qb->setParameter('showDate', new DateTime());

        if ($series === true) {
            $qb->andWhere('p.type > 1');
        } else {
            $qb->andWhere('p.type = 1');
        }

        if ($orderByShowDate) {
            $qb->orderBy('p.showDate', 'DESC');
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param PostFilterDto $filters
     */
    private function filter(QueryBuilder $qb, PostFilterDto $filters): void
    {
        if ($filters->getOrientation()) {
            $qb->leftJoin('i.orientation', 'o')
                ->andWhere('o.id = :orientation')
                ->setParameter('orientation', $filters->getOrientation());
        }

        if ($filters->getGreyscale() !== null) {
            $qb->andWhere('p.isGreyscale = :greyscale');
            $qb->setParameter('greyscale', $filters->getGreyscale());
        }

        if ($filters->getCategories()) {
            $qb->andWhere('p.category IN(:categories)');
            $qb->setParameter('categories', explode(',', $filters->getCategories()));
        }

        if ($filters->getSeries() === '1') {
            $qb->join('p.parent', 'pr')
                ->andWhere('pr.status > 2')
                ->orderBy('pr.showDate', 'DESC')
                ->addOrderBy('p.position', 'ASC')
            ;

        } else if ($filters->getSeries() === '0') {
            $qb->andWhere('p.parent is null')
                ->orderBy('p.showDate', 'DESC');

        } else {
            $qb->orderBy('p.showDate', 'DESC');
        }

        if ($filters->getNude() === '1') {
            $qb->andWhere('p.isNude = true');
        } else {
            if ($filters->getNude() === '0') {
                $qb->andWhere('p.isNude = false');
            }
        }

        if ($filters->getKeyword() !== null) {
            $qb->leftJoin('p.tags', 'tg');
            $qb->leftJoin('p.user', 'u');
            $qb->leftJoin('u.profile', 'pro');
            $qb->andWhere(
                'LOWER(u.username) like LOWER(:keyword) OR LOWER(pro.firstName) like LOWER(:keyword) OR LOWER(pro.lastName) like LOWER(:keyword) OR LOWER(tg.name) like LOWER(:keyword) OR LOWER(p.title) like LOWER(:keyword)'
            );
            $qb->setParameter('keyword', "%{$filters->getKeyword()}%");
        }

        if ($filters->getStartDate() && $filters->getEndDate()) {

            $filters->setStartDate($filters->getStartDate()->setTime(0, 0, 0));
            $filters->setEndDate($filters->getEndDate() > new DateTime() ? new DateTime() : $filters->getEndDate());
            $filters->setEndDate($filters->getEndDate()->setTime(23, 59, 59));

            $qb->andWhere('a.createdAt >= :start AND a.createdAt <= :end')
                ->setParameter('start', $filters->getStartDate())
                ->setParameter('end', $filters->getEndDate());
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param UserFilterDto $filters
     */
    private function curateFilter(QueryBuilder $qb, UserFilterDto $filters): void
    {
        $qb->leftJoin('p.user', 'u');

        if ($filters->getUsername()) {
            $qb->andWhere('LOWER(u.slug) = LOWER(:username) or LOWER(u.username) = LOWER(:username)')
                ->setParameter('username', $filters->getUsername());
        }

        if ($filters->getName()) {

            $name = explode(' ', $filters->getName());

            $filters->setFirstName($name[0] ?? null);
            $filters->setLastName($name[0] ?? $name[1] ?? null);

            $qb->leftJoin('u.profile', 'pr')
                ->andWhere('LOWER(pr.firstName) = LOWER(:fname) or LOWER(pr.lastName ) = LOWER(:lname)')
                ->setParameter('fname', $filters->getFirstName())
                ->setParameter('lname', $filters->getLastName());
        }
    }

    /**
     * @param $qb
     * @param $page
     * @param $limit
     * @param $key
     * @param $duration
     * @param array $groups
     * @param null $tag
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function getCached($qb, $page, $limit, $key, $duration = null, $groups = [], $tag = null)
    {
        return $this->cache->get(
            $key . $page . $limit,
            function (ItemInterface $item) use ($qb, $page, $limit, $duration, $groups, $tag) {

                $collection = $this->paginate($qb, $page, $limit);

                if ($collection instanceof ListDto) {
                    $groups['groups'][] = 'api';
                }

                if ($tag !== null) {
                    $item->tag($tag);
                }

                if ($duration !== null) {
                    $item->expiresAfter(is_callable($duration) ? $duration($collection) : $duration);
                }

                return $this->normalize($collection, $groups);
            }
        );
    }

}
