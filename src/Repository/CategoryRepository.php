<?php

namespace App\Repository;

use App\Entity\Photo\Category;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class CategoryRepository
 * @package App\Repository
 */
class CategoryRepository extends BaseRepository
{
    /**
     * CategoryRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Category::class); }


    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getCategories()
    {
        return $this->cache->get('categories', function () {
            return $this->normalize($this->findBy([],['name' => 'ASC']),['groups' => 'category']);
        });
    }
}
