<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Forum\Opinion;
use App\Entity\Forum\Section;
use App\Entity\Forum\Topic;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class OpinionRepository
 * @package App\Repository
 */
class OpinionRepository extends BaseRepository
{
    protected string $type = 'web';

    /**
     * OpinionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Opinion::class);
    }

    /**
     * @param Topic $topic
     * @param int $page
     * @param int $limit
     * @return ListDto|mixed
     */
    public function getOpinions(Topic $topic, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.topic =:topic')
            ->setParameter('topic', $topic);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param Section $section
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getLastForSection(Section $section)
    {
        return $this->createQueryBuilder('o')
            ->join('o.topic', 't')
            ->join('t.section', 's')
            ->where('s = :section')
            ->setParameter('section', $section)
            ->orderBy('o.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int $limit
     * @return float
     */
    public function getLastPage(int $topic,int $limit)
    {
        return ceil($this->count(compact('topic')) / $limit);
    }
}

