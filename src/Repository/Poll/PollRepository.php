<?php

namespace App\Repository\Poll;

use App\Entity\Poll\Poll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Poll|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poll|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poll[]    findAll()
 * @method Poll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollRepository extends ServiceEntityRepository
{
    /**
     * PollRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poll::class);
    }

    /**
     * @return Poll[]|null
     */
    public function findAllSorted()
    {
        return $this->createQueryBuilder('p')
            ->addOrderBy('p.status', 'desc')
            ->addOrderBy('p.id', 'desc')
            ->getQuery()
            ->execute();
    }

    /**
     * @return mixed
     */
    public function deactivateAllPolls()
    {
        return $this->createQueryBuilder('p')
            ->update()
            ->set('p.status', 0)
            ->getQuery()
            ->execute();
    }

    /**
     * @return Poll|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneActivePoll(): ?Poll
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.status = 1')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param Poll $poll
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalVotesPerPoll(Poll $poll)
    {
        return $this->createQueryBuilder('poll')
            ->leftJoin('poll.choices', 'c')
            ->leftJoin('c.votes', 'v')
            ->select('count(v.id) as t')
            ->andWhere('poll = :poll')
            ->setParameter('poll', $poll)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
