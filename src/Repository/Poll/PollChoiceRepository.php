<?php

namespace App\Repository\Poll;

use App\Entity\Poll\Poll;
use App\Entity\Poll\PollChoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PollChoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method PollChoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method PollChoice[]    findAll()
 * @method PollChoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollChoiceRepository extends ServiceEntityRepository
{
    /**
     * PollChoiceRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PollChoice::class);
    }

    /**
     * @return mixed
     */
    public function findAllOrderedByIndex()
    {
        return $this->createQueryBuilder('c')
            ->addOrderBy('c.orderIndex', 'asc')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Poll $poll
     * @return mixed
     */
    public function findChoicesOfPoll(Poll $poll)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.poll = :poll')
            ->setParameter('poll', $poll)
            ->getQuery()
            ->getResult();
    }
}
