<?php

namespace App\Repository;

use App\Entity\Photo\Album;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="album_repository")
 */
class AlbumRepository extends BaseRepository
{
    /**
     * PostRepository constructor.
     * @param ManagerRegistry $registry
     * @param PaginatorInterface $paginator
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);

        $this->setPaginationType('web');
    }

    public function getOrderedByCover(Album $album)
    {
        return $this->_em->getRepository(Post::class)
            ->createQueryBuilder('p')
            ->select(['p', 'CASE WHEN p = :cover THEN 0 ELSE 1 END AS HIDDEN cover'])
            ->leftJoin('p.albums', 'a')
            ->where('a = :album')
            ->setParameter('album', $album)
            ->setParameter('cover', $album->getCover())
            ->orderBy('cover','ASC')
            ->getQuery()
            ->getResult();
    }

    public function getPosts(User $user, ?Album $album = null, int $page = 2)
    {
        $qb = $this->_em->getRepository(Post::class)
            ->createQueryBuilder('p')
            ->where('p.user = :user')
            ->andWhere('p.type = 1')
            ->andWhere('p.isDeleted = false')
            ->setParameter('user', $user);

        if ($album) {
            $qb->select(['p', 'CASE WHEN :album MEMBER OF p.albums THEN 1 ELSE 200 END AS HIDDEN order', 'CASE WHEN p = :cover THEN 0 ELSE 1 END AS HIDDEN cover'])
                ->setParameter('album', $album)
                ->setParameter('cover', $album->getCover())
                ->orderBy('cover', 'asc')
                ->addOrderBy('order', 'asc');


        }

        return $this->paginate($qb, $page, 20);
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return PaginationInterface
     */
    public function getByUser(User $user, int $page, int $limit)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.user =:user')
            ->orderBy('a.id', 'DESC')
            ->setParameter('user', $user);

        $this->deleted($qb);

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param QueryBuilder $qb
     * @param bool $deleted
     */
    private function deleted(QueryBuilder $qb, bool $deleted = false)
    {
        $qb->andWhere('a.isDeleted = :deleted');
        $qb->setParameter('deleted', $deleted);
    }
}
