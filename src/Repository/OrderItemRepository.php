<?php

namespace App\Repository;

use App\Dto\Response\ListDto;
use App\Entity\Store\OrderItem;
use App\Entity\User\User;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class OrderItemRepository
 * @package App\Repository
 */
class OrderItemRepository extends BaseRepository
{
    /**
     * OrderItemRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, OrderItem::class); }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     * @return ListDto|PaginationInterface
     */
    public function getByUser(User $user, int $page, int $limit)
    {
        $this->type = 'web';

        $qb = $this->createQueryBuilder('oi')
            ->join('oi.product', 'pr')
            ->join('pr.post', 'p')
            ->join('oi.order', 'o')
            ->join('o.transaction', 't')
            ->where('p.user = :user')
            ->andWhere('t.status = 1')
            ->setParameter('user', $user);

        return $this->paginate($qb, $page, $limit);

    }

    public function getExpected(User $user)
    {
        return $this->createQueryBuilder('oi')
            ->select(['SUM(oi.commission) as total'])
            ->join('oi.product', 'pr')
            ->join('pr.post', 'p')
            ->where('p.user = :user')
            ->setParameter('user', $user)
            ->andWhere('oi.commissionAddedToBalance = false')
            ->getQuery()->getSingleScalarResult();
    }
}
