<?php

namespace App\Repository;

use App\Entity\Store\Size;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Cache\InvalidArgumentException;

/**
 * Class SizeRepository
 * @package App\Repository
 */
class SizeRepository extends BaseRepository
{
    /**
     * SizeRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Size::class); }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getSizes()
    {
        return $this->cache->get('sizes', function () {
            return $this->normalize($this->findBy(['isEnabled' => true], ['order' => 'ASC']));
        });
    }
}
