<?php

namespace App\EntityManager;

use App\Entity\EntityInterface;
use App\Entity\User\Balance;
use App\Entity\User\ForgottenPassword;
use App\Entity\User\Payout;
use App\Entity\User\Settings;
use App\Entity\User\User;
use App\Entity\User\Verification;
use App\Enum\FileStorageFolderEnum;
use App\Enum\ImageSizeEnum;
use App\Event\Notification\UserConfirmedEvent;
use App\Event\Notification\UserRegisteredEvent;
use App\FileManager\S3FileManager;
use App\Helper\DispatcherTrait;
use App\Processor\ImageProcessor;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserManager
 * @package App\Manager
 */
class UserManager extends BaseManager
{
    use DispatcherTrait;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    /**
     * @var ImageProcessor
     */
    private ImageProcessor $processor;

    /**
     * @var S3FileManager
     */
    private S3FileManager $fileManager;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $em
     * @param ImageProcessor $processor
     * @param S3FileManager $fileManager
     * @param UserPasswordEncoderInterface $encoder
     * @param string|null $entity
     */
    public function __construct(EntityManagerInterface $em,
                                ImageProcessor $processor,
                                S3FileManager $fileManager,
                                UserPasswordEncoderInterface $encoder,
                                ?string $entity = null)
    {
        parent::__construct($em, $entity);
        $this->encoder = $encoder;
        $this->processor = $processor;
        $this->fileManager = $fileManager;
    }

    /**
     * @param $socialUser
     * @param $network
     * @return mixed
     * @throws Exception
     */
    public function createSocialUser($socialUser, $network)
    {

        //Check if social user exists
        $user = $this->socialUserExists($socialUser->getId(), $network);

        if ($user && $user->getIsDeleted() === true) {
            return null;
        }

        if ($user) {
            return $user;
        }

        //Check if user with same email exists
        $user = $this->userExists($socialUser->getEmail());

        if ($user && $user->getIsDeleted() === true) {
            return null;
        }

        if ($user) {

            $network === 'facebook' ? $user->setFacebookId($socialUser->getId()) : $user->setGoogleId($socialUser->getId());

            $this->_em->flush();

            return $user;
        }

        //If non of above create user
        return $this->creatUser($socialUser, $network);
    }

    /**
     * @param User $user
     * @throws Exception
     */
    public function createVerification(User $user)
    {
        $verification = new Verification($user);
        $this->_em->persist($verification);
        $this->_em->flush();

        return $verification;
    }

    /**
     * @param string $token
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function verificationTokenExists(string $token)
    {
        return $this->repository->createQueryBuilder('u')
            ->join('u.verifications', 'v')
            ->where('v.token =:token')
            ->setParameter('token', $token)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     */
    public function verify(User $user): void
    {
        $user->setIsVerified(true);
        $user->getVerifications()->first()->setUsed(true);

        $this->_em->flush();
    }

    /**
     * @param string $email
     * @return bool|string|null
     * @throws \Exception
     */
    public function createForgottenPasswordToken(string $email)
    {
        /**
         * @var $user User
         */
        $user = $this->repository->findOneBy(compact('email'));

        if ($user === null) {
            return false;
        }

        $token = new ForgottenPassword();
        $token->setUser($user);
        $token->setExpiresIn(15);
        $token->setToken(bin2hex(random_bytes(32)));
        $token->setIsUsed(false);

        $this->_em->persist($token);

        $this->_em->flush();

        return $token;
    }

    /**
     * @param string $email
     * @param string $token
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function verifyForgottenPasswordToken(string $email, string $token)
    {
        $date = new \DateTime();
        $date = $date->sub(new \DateInterval('PT15M'));

        $q = $this->repository->createQueryBuilder('u')
            ->join('u.forgottenPasswords', 'f')
            ->where('f.token = :token')
            ->andWhere('u.email = :email')
            ->andWhere('f.isUsed = false')
            ->setParameter('token', $token)
            ->setParameter('email', $email)
            ->andWhere('f.createdAt >= :end')
            ->setParameter('end', $date);

        return $q->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param string $token
     */
    public function makeForgottenTokenAsUsed(string $token): void
    {
        $forgotten = $this->em->getRepository(ForgottenPassword::class)
            ->findOneBy(compact('token'));

        $forgotten->setIsUsed(true);

        $this->_em->flush();
    }

    /**
     * @param User $user
     * @param string $password
     */
    public function resetPassword(User $user, string $password): void
    {
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $this->_em->flush();
    }

    /**
     * @param $user
     */
    public function createPayoutRequest($user): void
    {
        $balance = $this->_em->getRepository(Balance::class)->findOneBy(['user' => $user]);

        $balance->setBlocked(($balance->getBlocked() ?? 0) + $balance->getTotal());

        $payout = new Payout();
        $payout->setUser($user);
        $payout->setSum($balance->getTotal());
        $payout->setStatus(Payout::PENDING);
        $balance->setTotal(0);

        $this->_em->persist($payout);

        $this->_em->flush();
    }

    /**
     * @param User $user
     * @param UploadedFile $avatar
     * @throws Exception
     */
    public function updateAvatar(User $user, UploadedFile $avatar)
    {
        $image = $this->processor->process(ImageSizeEnum::AVATAR, $avatar->getPathname());

        $uri = $this->fileManager->add($image, FileStorageFolderEnum::AVATAR);

        $user->getProfile()->setPicture($uri);

        $this->_em->flush();
    }

    /**
     * @param User $user
     * @param UploadedFile $cover
     * @throws Exception
     */
    public function updateCover(User $user, UploadedFile $cover)
    {

        $uri = $this->fileManager->add($cover, FileStorageFolderEnum::COVER);

        $user->getProfile()->setCover($uri);

        $this->_em->flush();

    }

    /**
     * @param User $user
     * @param string $password
     */
    public function changePassword(User $user, string $password)
    {
        $user->setPassword($this->encoder->encodePassword($user, $password));

        $this->_em->flush();
    }

    /**
     * @param EntityInterface $entity
     */
    protected function preCreate(EntityInterface $entity): void
    {
        $entity->setPassword($this->encoder->encodePassword($entity, $entity->getPassword()));
    }

    /**
     * @param string $token
     * @param string $network
     * @return User|bool|object|null
     */
    private function socialUserExists(string $token, string $network): ?UserInterface
    {
        return $this->_em->getRepository(User::class)->findOneBy([$network . 'Id' => $token],[], false);
    }

    /**
     * @param string $email
     * @param string $network
     * @param string $token
     * @return User|bool|object|null
     */
    private function userExists(string $email): ?UserInterface
    {
        // Do we have a matching user by email?
        return $this->_em->getRepository(User::class)->findOneBy(compact('email'),[], false);
    }

    /**
     * @param $socialUser
     * @param $network
     * @return User
     * @throws Exception
     */
    private function creatUser($socialUser, $network): User
    {
        $user = new User();

        $profile = $user->getProfile();
        $profile->setFirstName($socialUser->getFirstName());
        $profile->setLastName($socialUser->getLastName());
        $profile->setPicture($network === 'facebook' ? $socialUser->getPictureUrl() : $socialUser->getAvatar());

        $user->setCurateAllowanceLimitReached(false);
        $user->setCurateLimitStartDate(new DateTime());
        $user->setIsVerified(true);
        $user->setUsername(strtolower($socialUser->getFirstName() . '_' . $socialUser->getLastName()));
        //Generate password
        // Maybe you just want to "register" them by creating
        // a User object
        $user->setEmail($socialUser->getEmail());

        $network === 'facebook' ? $user->setFacebookId($socialUser->getId()) : $user->setGoogleId($socialUser->getId());

        $this->_em->persist($user);

        $this->_em->flush();

        $this->dispatch(new UserConfirmedEvent(null, $user, ['social' => true]));

        return $user;
    }

}
