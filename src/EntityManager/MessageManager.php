<?php

namespace App\EntityManager;

use App\Entity\Messenger\Message;
use App\Entity\Photo\Post;
use App\Entity\Store\Product;
use App\Entity\User\User;
use App\Event\Notification\NewMessageEvent;
use App\Helper\DispatcherTrait;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class MessageManager
 * @package App\Manager
 */
class MessageManager extends BaseManager
{
    use DispatcherTrait;

    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * MessageManager constructor.
     * @param EntityManagerInterface $em
     * @param Environment $twig
     * @param string|null $entity
     */
    public function __construct(EntityManagerInterface $em, Environment $twig, string $entity = null)
    {
        parent::__construct($em, $entity);
        $this->twig = $twig;
    }

    /**
     * @param Message $message
     */
    public function deleteForReceiver(Message $message)
    {
        $message->setIsDeletedForReceiver(true);
        if ($message->getIsDeletedForSender()) {
            $this->delete($message);
        }

        $this->_em->flush();
    }

    /**
     * @param Message $message
     */
    public function deleteForSender(Message $message)
    {
        $message->setIsDeletedForSender(true);
        if ($message->getIsDeletedForReceiver()) {
            $this->delete($message);
        }

        $this->_em->flush();
    }

    /**
     * @param Post $post
     * @param User $curator
     * @param array $reasons
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendRejectReasons(Post $post, User $curator, array $reasons)
    {
        $message = new Message();
        $message->setReceiver($post->getUser());
        $message->setSender($curator);
        $message->setText($this->twig->render('photo/reject-reasons.html.twig', compact('post', 'reasons')));

        $this->_em->persist($message);

        $this->_em->flush();

        $this->dispatch(new NewMessageEvent($curator, $post->getUser(), ['conversation' => $message->getId()]));
    }

    /**
     * @param Product $product
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendProductRejected(Product $product)
    {
        $message = new Message();
        $receiver = $product->getPost()->getUser();
        $message->setReceiver($receiver);
        $admin = $this->_em->find(User::class, 7);
        $message->setSender($admin);
        $message->setText($this->twig->render('product/_partials/rejected.html.twig', compact('product')));

        $this->_em->persist($message);

        $this->_em->flush();

        $this->dispatch(new NewMessageEvent($admin, $receiver, ['conversation' => $message->getId()]));

    }
}
