<?php
/**
 * Created by PhpStorm.
 * User: dimitartanev
 * DateCreated: 2019-04-20
 * Time: 13:15
 */

namespace App\EntityManager;

use App\Entity\EntityInterface;

/**
 * Class InvoiceManager
 * @package App\Manager\Store
 */
class InvoiceManager extends BaseManager
{

    /**
     * @param EntityInterface $entity
     * @throws \Exception
     */
    protected function preCreate(EntityInterface $entity): void
    {
        $invoiceId = $this->repository->getLast() ? $this->repository->getLast()->getId() + 1 : 1;
        $entity->setInvoiceId($invoiceId);
    }

}
