<?php

namespace App\EntityManager;

use App\Entity\Photo\Image;
use App\Entity\Store\Product;
use App\Enum\FileStorageFolderEnum;
use App\Enum\ImageSizeEnum;
use App\Enum\ProductStatusEnum;
use App\FileManager\S3FileManager;
use App\Helper\DispatcherTrait;
use App\Helper\ProductHelper;
use App\Processor\ImageProcessor;
use App\Repository\ConfigurationRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ProductManager
 * @package App\EntityManager
 */
class ProductManager extends BaseManager
{

    use DispatcherTrait;

    /**
     * @var ImageProcessor
     */
    private ImageProcessor $imageProcessor;

    /**
     * @var S3FileManager
     */
    private S3FileManager $s3FileManager;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * ProductManager constructor.
     * @param EntityManagerInterface $em
     * @param ConfigurationRepository $configurationRepository
     * @param S3FileManager $s3FileManager
     * @param ImageProcessor $imageProcessor
     * @param string|null $entity
     */
    public function __construct(EntityManagerInterface $em,
                                ConfigurationRepository $configurationRepository,
                                S3FileManager $s3FileManager,
                                ImageProcessor $imageProcessor,
                                string $entity = null)
    {
        parent::__construct($em, $entity);
        $this->imageProcessor = $imageProcessor;
        $this->s3FileManager = $s3FileManager;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Add print to storage and create product
     * @param Product $product
     * @return array
     * @throws InvalidArgumentException
     */
    public function addToStore(Product $product)
    {

        $min = $this->configurationRepository->getConfig()->getMinPrintSize();

        $this->_em->persist($product);

        $completed = 0;

        $commission = 0;

        $autoApprove = 0;

        $children = count($product->getChildren());

        $this->approve($product, $completed, $commission, $autoApprove, $min);

        if ($completed >= $children) {

            if($children > 0 && $product->getCommission() > 0){
                $product->setNewCommission($commission);
            }

            if ($autoApprove > 0 && $autoApprove >= $children) {
                $product->setStatus(ProductStatusEnum::PUBLISHED);
                $product->setApprovedOn($product->getApprovedOn() ?? new DateTime());

                foreach ($product->getChildren() as $child) {
                    $child->setStatus(ProductStatusEnum::PUBLISHED);
                    $child->setApprovedOn($child->getApprovedOn() ?? new DateTime());
                }

            } else {
                $product->setStatus($product->getStatus() < ProductStatusEnum::COMPLETED ?? ProductStatusEnum::COMPLETED);
            }

        }

        if($product->getCommission() === null){
            $product->setCommission($commission);
        }

        $this->_em->flush();

        return [$completed, $autoApprove];
    }

    /**
     * @param Product $product
     * @param int $completed
     * @param float $commission
     * @param int $autoApprove
     * @param int $min
     * @throws Exception
     */
    private function approve(Product $product, int &$completed, float &$commission, int &$autoApprove, int $min)
    {
        $image = $product->getPost()->getImage();

        if ($image->getFile()) {
            $this->uploadPrint($image);
        }

        $this->isCompleted($product, $completed, $commission, $min);

        $autoApprove += $this->isAutoApproved($image, $min);

        foreach ($product->getChildren() as $child) {
            $this->approve($child, $completed, $commission, $autoApprove, $min);
        }

        $this->isPriceChanged($product);

    }

    /**
     * @param Image $image
     * @throws Exception
     */
    protected function uploadPrint(Image $image)
    {

        $uri = $this->s3FileManager->add($image->getFile(), FileStorageFolderEnum::IMAGES_PRINT, null, false);

        $image->setPrint(urldecode($uri));

        $this->dispatcher->addListener(KernelEvents::TERMINATE, function () use ($image) {

            $this->imageProcessor->setImage($image->getFile());
            $this->imageProcessor->resize(ImageSizeEnum::IMAGE_THUMB_MID);

            $thumbnail = $this->s3FileManager->add($this->imageProcessor->getBlob(), FileStorageFolderEnum::IMAGES_PRINT_THUMBNAIL, null, false);

            $image->setPrintThumbnail($thumbnail);

            $this->_em->flush();
        });

    }

    /**
     * @param Product $product
     * @param int $completed
     * @param float $commission
     * @param int $min
     */
    protected function isCompleted(Product $product, int &$completed, float &$commission, int $min)
    {

        $image = $product->getPost()->getImage();

        if ($product->getCommission() > 0 && ($image->getPrint() || !ProductHelper::needPrint($image, $min))) {

            $completed++;

            $commission += $product->getCommission();

            $product->setStatus($product->getStatus() > ProductStatusEnum::COMPLETED ? $product->getStatus() : ProductStatusEnum::COMPLETED);
        }

    }

    /**
     * @param Image $image
     * @param int $min
     * @return int
     */
    protected function isAutoApproved(Image $image, int $min)
    {
        return $image->getPrint() === null && !ProductHelper::needPrint($image, $min) ? 1 : 0;
    }

    /**
     * @param Product $product
     */
    protected function isPriceChanged(Product $product)
    {
        $originalData = $this->_em->getUnitOfWork()->getOriginalEntityData($product);

        if ($product->getId() === null || (array_key_exists('commission', $originalData) && $originalData['commission'] !== $product->getCommission())) {

            $changeInterval = $this->configurationRepository->getConfig()->getPriceWillChangeInterval();

            if ($changeInterval && isset($originalData['commission'])) {


                $product->setNewCommission($product->getCommission());
                $product->setCommission($originalData['commission']);
                $product->setCommissionChangingOn((new DateTime())->modify("+ $changeInterval days"));
            }

            $product->setPriceChangedAt(new DateTime());

        }
    }

}
