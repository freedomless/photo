<?php

namespace App\EntityManager;

use App\Entity\Photo\Post;
use App\Entity\Photo\SelectionSeries;
use App\Event\Notification\SeriesSelectedEvent;
use App\FileManager\S3FileManager;
use App\Helper\DispatcherTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class SelectionSeriesManager
 * @package App\Manager
 */
class SelectionSeriesManager
{
    use DispatcherTrait;

    private const COVER_DIR = "series-cover/";

    /**
     * @var S3FileManager
     */
    private S3FileManager $cdn;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * SelectionSeriesManager constructor.
     * @param S3FileManager $cdn
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        S3FileManager $cdn,
        EntityManagerInterface $entityManager
    )
    {
        $this->cdn = $cdn;
        $this->entityManager = $entityManager;
    }

    /**
     * @param SelectionSeries $selectionSeries
     * @param Post $series
     * @param UploadedFile $cover
     * @throws Exception
     */
    public function create(SelectionSeries $selectionSeries, Post $series, $cover)
    {

        $selectionSeries
            ->setCreatedAt(new DateTime())
            ->setPost($series)
            ->setVisibility(false);

        if ($cover instanceof UploadedFile) {
            $cover = $this->cdn->add($cover, self::COVER_DIR);
        }

        $selectionSeries->setCover($cover);

        $this->entityManager->persist($selectionSeries);
        $this->entityManager->flush();
    }

    /**
     * @param SelectionSeries $selectionSeries
     * @param UploadedFile|null $cover
     * @throws Exception
     */
    public function update(SelectionSeries $selectionSeries, ?UploadedFile $cover)
    {
        if (null !== $cover) {
            $this->cdn->remove($selectionSeries->getCover());
            $cover = $this->cdn->add($cover, self::COVER_DIR);
            $selectionSeries->setCover($cover);
        }

        $this->entityManager->flush();
    }

    /**
     * @param SelectionSeries $selectionSeries
     */
    public function delete(SelectionSeries $selectionSeries)
    {
        $this->cdn->remove($selectionSeries->getCover());
        $this->entityManager->remove($selectionSeries);
        $this->entityManager->flush();
    }

    /**
     * @param SelectionSeries $selectionSeries
     */
    public function showOnFrontPage(SelectionSeries $selectionSeries)
    {
        $this->hideAll();

        $selectionSeries->setVisibility(true);

        if (false === $selectionSeries->getHasNotification()) {

            $posts = $selectionSeries->getPost()->getChildren();
            foreach ($posts as $post) {
                $this->dispatch(new SeriesSelectedEvent(null, $post->getUser(), [
                    'post'      => $post,
                    'selection' => $selectionSeries
                ]));

                sleep(2); // todo: fix this shit - use messenger!
            }
        }

        $selectionSeries->setHasNotification(true);

        $this->entityManager->flush();
    }

    /**
     * @param SelectionSeries $selectionSeries
     */
    public function hideFromFrontPage(SelectionSeries $selectionSeries)
    {
        $selectionSeries->setVisibility(false);

        $this->entityManager->flush();
    }

    private function hideAll()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->update('App:Photo\SelectionSeries', 's')
            ->set('s.visibility', 0)
            ->getQuery()
            ->execute();
    }
}
