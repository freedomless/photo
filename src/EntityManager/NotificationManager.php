<?php

namespace App\EntityManager;

use App\Entity\User\User;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class NotificationManager extends BaseManager
{


    /**
     * @param $notifications
     */
    public function markAsSeen($notifications): void
    {
        foreach ($notifications as $notification) {
            $notification->setIsSeen(true);
        }

        $this->_em->flush();
    }

    /**
     * @param User $user
     */
    public function markAllAsSeen(User $user): void
    {
        $qb = $this->repository->createQueryBuilder('n');
        $qb->update()
            ->set('n.isSeen', '?1')
            ->setParameter(1, $qb->expr()->literal(true))
            ->where('n.receiver = :user')
            ->setParameter('user', $user)
            ->getQuery()->execute();
    }
}
