<?php

namespace App\EntityManager;

use App\Entity\EntityInterface;
use App\Entity\Store\Cart;
use App\Entity\Store\CartItem;
use App\Entity\Store\Order;
use App\Entity\Store\OrderItem;
use App\Enum\TransactionStatusEnum;
use App\Event\Notification\System\NewOrderEvent;
use App\Helper\DispatcherTrait;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Class OrderManager
 * @property ObjectRepository repository
 * @package App\Manager\Store
 */
class OrderManager extends BaseManager
{
    use DispatcherTrait;

    /**
     * @param Cart $cart
     * @param Order $order
     */
    public function copyCartItems(Cart $cart, Order $order): void
    {
        $total = 0;

        /**
         * @var $item CartItem
         */
        foreach ($cart->getItems() as $item) {

            $orderItem = new OrderItem();
            $orderItem->setProduct($item->getProduct());
            $orderItem->setMaterial($item->getMaterial());
            $orderItem->setSize($item->getSize());
            $orderItem->setQuantity($item->getQuantity());
            $orderItem->setOrder($order);
            $orderItem->setCommissionAddedToBalance(false);
            $orderItem->setCommissionPaid(false);
            $orderItem->setPrice($item->getTotal());

            $total += $item->getTotal();

            $order->addItem($orderItem);

            $this->_em->persist($orderItem);

        }

        $order->setTotal($total);
    }

}
