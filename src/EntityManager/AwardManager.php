<?php

namespace App\EntityManager;

use App\Entity\Notification\Notification;
use App\Entity\Notification\NotificationType;
use App\Entity\Photo\Award;
use App\Entity\Photo\AwardType;
use App\Entity\Photo\Post;
use App\Repository\AwardRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\PersistentCollection;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AwardManager
 * @package App\Manager\Photo
 */
class AwardManager
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $manager;

    /** @var AwardRepository */
    private AwardRepository $repository;

    /** @var ValidatorInterface */
    private ValidatorInterface $validator;

    /**
     * AwardManager constructor.
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface $validator
     * @param AwardRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        ValidatorInterface $validator,
        AwardRepository $repository
    )
    {
        $this->manager = $manager;
        $this->validator = $validator;
        $this->repository = $repository;
    }

    /**
     * @param Post $photo
     * @return bool
     * @throws NonUniqueResultException
     */
    public function create(Post $photo): bool
    {
        $type = $this->manager->getRepository(AwardType::class)->findOneBy(['name' => 'photo_of_the_day']);

        if (!$type) {
            return false;
        }

        $award = new Award();

        $award->setType($type);
        $award->setPost($photo);

        if (!$this->validator->validate($award)) {
            return false;
        }

        /** @var Award $lastInQueue */
        $lastInQueue = $this->repository->findLastPhotoInQueue();

        $postDate = (new DateTime('now'))
            ->modify('+1 day')
            ->setTime('0', '0', '0');

        if ($lastInQueue) {
            /** @var DateTime $postDate */
            $postDate = $lastInQueue->getCreatedAt(false);
            $postDate = $postDate
                ->modify('+1 day')
                ->setTime('0', '0', '0');
        }

        $award->setCreatedAt($postDate);

        $this->manager->persist($award);

        $this->manager->flush();

        return true;
    }

    /**
     * @param string $ids
     * @return bool
     */
    public function switch(string $ids): bool
    {
        $ids = array_map('intval', array_values(explode(',', $ids)));

        if (count($ids) !== 2) {
            return false;
        }

        $photoA = $this->repository->findOneBy(['id' => $ids[0]]);
        $photoB = $this->repository->findOneBy(['id' => $ids[1]]);

        if (!$photoA || !$photoB) {
            return false;
        }

        $dateB = clone $photoB->getCreatedAt(false);
        $dateA = clone $photoA->getCreatedAt(false);

        $photoA->setCreatedAt($dateB);
        $photoB->setCreatedAt($dateA);

        $this->manager->flush();

        return true;
    }

    /**
     * @param Award $photo
     * @return bool
     * @throws Exception
     */
    public function delete(Award $photo): bool
    {
        if ($photo->getCreatedAt(false) <= new DateTime('now')) {
            return false;
        }

        /** @var PersistentCollection|Award[] $upcoming */
        $upcoming = $this->repository->findUpcomingPhotos($photo->getCreatedAt(false));

        if ($upcoming) {
            foreach ($upcoming as $uPhoto) {
                /** @var DateTime $oldDate */
                $oldDate = clone $uPhoto->getCreatedAt(false);

                $uPhoto->setCreatedAt(
                    $oldDate->modify('-1 day')
                );

                $this->manager->persist($uPhoto);
            }
        }

        $this->manager->remove($photo);

        $this->manager->flush();

        return true;
    }

    /**
     * @param Post $post
     * @return bool
     */
    public function hasBeenPhotoOfTheDay(Post $post): bool
    {
        $award = $this->getRepository()->findOneBy([
            'post' => $post
        ], [], false);

        return null !== $award && $award instanceof Award;
    }

    /**
     * @return AwardRepository
     */
    public function getRepository(): AwardRepository
    {
        return $this->repository;
    }
}
