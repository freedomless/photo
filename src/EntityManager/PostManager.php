<?php /** @noinspection PhpDocSignatureInspection */

namespace App\EntityManager;

use App\Entity\EntityInterface;
use App\Entity\Photo\Curate;
use App\Entity\Photo\Image;
use App\Entity\Photo\Orientation;
use App\Entity\Photo\Post;
use App\Entity\Store\Product;
use App\Entity\User\User;
use App\Enum\FileStorageFolderEnum;
use App\Enum\ImageSizeEnum;
use App\Enum\PostTypeEnum;
use App\Enum\ProductStatusEnum;
use App\FileManager\S3FileManager;
use App\Helper\DispatcherTrait;
use App\Processor\ImageProcessor;
use App\Repository\ConfigurationRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class PostManager
 * @package App\Manager
 * @property PostRepository $repository
 */
class PostManager extends BaseManager
{

    use DispatcherTrait;

    /**
     * @var S3FileManager
     */
    private S3FileManager $S3FileManager;

    /**
     * @var ImageProcessor
     */
    private ImageProcessor $imageProcessor;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * PostManager constructor.
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     * @param S3FileManager $s3FileManager
     * @param ImageProcessor $imageProcessor
     */
    public function __construct(EntityManagerInterface $em,
                                SessionInterface $session,
                                ConfigurationRepository $configurationRepository,
                                S3FileManager $S3FileManager,
                                ImageProcessor $imageProcessor,
                                LoggerInterface $logger,
                                ?string $entity = null)
    {
        parent::__construct($em, $entity);
        $this->S3FileManager = $S3FileManager;
        $this->imageProcessor = $imageProcessor;
        $this->session = $session;
        $this->configurationRepository = $configurationRepository;
        $this->logger = $logger;
    }

    /**
     * @param User $user
     * @param int $id
     * @param int $vote
     */
    public function vote(User $user, int $id, int $vote): void
    {
        $post = $this->repository->find($id);

        $curate = new Curate();
        $curate->setPosts($post);
        $curate->setUsers($user);
        $curate->setRating($vote);

        $this->_em->persist($curate);

        $this->_em->flush();
    }

    /**
     * @param User|null $user
     * @param Post $post
     */
    public function addView(Post $post)
    {
        /** Increase views on every refresh */
        $post->setViews($post->getViews() + 1);
        $this->_em->flush();
    }

    /**
     * @param Post $post
     * @param bool $direction
     * @param int $places
     *
     * @return int
     *
     * @throws InvalidArgumentException
     */
    public function move(Post $post, bool $up, ?int $places = null)
    {

        $config = $this->configurationRepository->getConfig();

        $posts = $this->repository->getPostsToBeMoved($post, $up, $places);

        $count = count($posts);

        if ($count === 0) {
            return 0;
        }

        $places = $places !== null ? $places : $count;

        $places = $places > $count ? $count : $places;

        $showData = $post->getShowDate();

        $publishTimespan = $config->getPublishTimespan();

        $minutes = $publishTimespan * $places;

        $post->setShowDate(clone $showData->modify(($up ? '+ ' : '- ') . "{$minutes} minutes"));

        foreach ($posts as $i => $p) {

            $p->setShowDate(clone $p->getShowDate()->modify(($up ? '- ' : '+ ') . $publishTimespan . 'minutes'));

            $this->preFlush($p);

        }

        $this->preFlush($post);

        $this->_em->flush();

        return $places;
    }

    /**
     * @param Post $post
     *
     * @throws Exception
     */
    protected function process(Post $post): void
    {
        //Get full name for watermark
        $name = $post->getUser()->getProfile()->getFullName();

        /**
         * @var Image $image
         */
        $image = $post->getImage();

        //Process images in all sizes
        $small = $this->imageProcessor->process(ImageSizeEnum::IMAGE_THUMB_SMALL, $image->getFile()->getPathname());

        /**
         * @var Orientation $orientation
         */
        $orientation = $this->_em->getRepository(Orientation::class)->findOneBy(['name' => $this->imageProcessor->getOrientation()]);
        //Set image orientation
        $image->setOrientation($orientation);

        //Set size in kilobytes
        $image->setSize(round($image->getFile()->getSize() / 1000));

        //Set width and height
        $image->setWidth($this->imageProcessor->getWidth());
        $image->setHeight($this->imageProcessor->getHeight());

        //Upload images
        $this->S3FileManager->setIsBatch(true);

        //Image uris will be returned in order uploaded!
        $this->S3FileManager->add($image->getFile(), FileStorageFolderEnum::IMAGES_ORIGINAL);
        $this->S3FileManager->add($small, FileStorageFolderEnum::IMAGES_THUMBS_SMALL);

        $urls = $this->S3FileManager->execute();

        $image->setUrl($urls[0]);
        $image->setThumbnail($urls[1]);

        $this->dispatcher->addListener(KernelEvents::TERMINATE, function () use ($image, $name) {

            $large = $this->imageProcessor->process(ImageSizeEnum::IMAGE_THUMB_LARGE, $image->getFile()->getPathname(), $name);
            $mid = $this->imageProcessor->process(ImageSizeEnum::IMAGE_THUMB_MID, $image->getFile()->getPathname(), null, 93);

            $this->S3FileManager->add($large, FileStorageFolderEnum::IMAGES_THUMBS_LARGE);
            $this->S3FileManager->add($mid, FileStorageFolderEnum::IMAGES_THUMBS_MID);

            $urls = $this->S3FileManager->execute();

            $image->setLarge($urls[0]);
            $image->setSmall($urls[1]);

            $this->_em->flush();

        });

    }

    /**
     * @param Post $entity
     */
    protected function preFlush(EntityInterface $entity): void
    {
        /**
         * @var Post $child
         */
        foreach ($entity->getChildren() as $child) {

            if ($entity->getType() === PostTypeEnum::SERIES) {
                $child->setStatus($entity->getStatus());
                $child->setIsDeleted($entity->getIsDeleted());
                $child->setShowDate($entity->getShowDate());
                $child->setSentForCurateDate($entity->getSentForCurateDate());
                $child->setCuratedAt($entity->getCuratedAt());
                $child->setHasNotification($entity->getHasNotification());
                $child->setIsNude($entity->getIsNude());
                $child->setIsGreyscale($entity->isGreyscale());
            }

            if ($entity->getProduct() && $child->getProduct()) {
                $child->getProduct()->setStatus($entity->getProduct()->getStatus());
            }

            $this->_em->persist($child);
        }

        //Order items in series from published
        if ($entity->getType() === PostTypeEnum::SERIES_FROM_PUBLISHED || $entity->getType() === PostTypeEnum::SERIES_FROM_CURATOR) {

            if (is_string($entity->getPosition())) {
                $order = explode(',', $entity->getPosition());

                /**
                 * @var Post $child
                 */
                foreach ($entity->getChildren() as $i => $child) {

                    $child->setParent($entity);
                    $child->setPosition(array_keys($order, $child->getId())[0]);

                    if ($child->getId() === (int)$order[0]) {
                        $entity->setCover($child);
                    }

                    $this->_em->persist($child);

                }

            }

        }

        $this->_em->getUnitOfWork()->computeChangeSets();
    }

    /**
     * @param Post $entity
     *
     * @throws Exception
     */
    protected function preCreate(EntityInterface $entity): void
    {
        //Upload image for single
        if ($entity->getType() === PostTypeEnum::SINGLE) {
            $this->process($entity);
        }

        //Upload images for each child of series
        if ($entity->getType() === PostTypeEnum::SERIES) {

            //Add cover
            $entity->setCover($entity->getChildren()->first());

            foreach ($entity->getChildren() as $i => $child) {

                //Set owner of the child
                $child->setUser($entity->getUser());
                $child->setCategory($entity->getCategory());
                $child->setParent($entity);
                $this->process($child);
            }
        }

        // Set published all when from curator
        if ($entity->getType() === PostTypeEnum::SERIES_FROM_CURATOR) {

            $showDate = new \DateTime();

            $entity->setStatus(3);
            $entity->setShowDate($showDate);

            /**
             * @var Post $child
             */
            foreach ($entity->getChildren() as $child) {

                if ($child->getStatus() < 3) {
                    $child->setShowDate($showDate);
                    $child->setStatus(3);
                }

                if ($child->getShowDate() > new \DateTime()) {
                    $child->setShowDate($showDate);
                }

                $this->_em->persist($child);

            }

        }

    }

    /**
     * @param Post $entity
     *
     * @throws Exception
     */
    protected function preUpdate(EntityInterface $entity): void
    {
        if ($entity->getType() === PostTypeEnum::SERIES) {

            foreach ($entity->getChildren() as $child) {

                if (!$this->_em->contains($child)) {

                    $child->setParent($entity);
                    $child->setUser($entity->getUser());
                    $child->setCategory($entity->getCategory());

                    $this->process($child);

                    $this->_em->persist($child);
                }

            }

            $this->_em->getUnitOfWork()->computeChangeSets();

        }

    }

    /**
     * @param Post $entity
     * @param bool $soft
     */
    protected function preDelete(EntityInterface $entity, bool &$soft = true): void
    {

        if ($entity->getStatus() === 0 && ($entity->getType() === PostTypeEnum::SINGLE || $entity->getType() === PostTypeEnum::SERIES)) {

            $soft = false;

            $this->S3FileManager->remove($entity->getImage()->getUrl());
            $this->S3FileManager->remove($entity->getImage()->getThumbnail());
            $this->S3FileManager->remove($entity->getImage()->getLarge());
            $this->S3FileManager->remove($entity->getImage()->getSmall());
            $this->S3FileManager->remove($entity->getImage()->getSocial());
            $this->S3FileManager->remove($entity->getImage()->getPrint());
            $this->S3FileManager->remove($entity->getImage()->getPrintThumbnail());

            if ($entity->getType() === PostTypeEnum::SERIES) {

                $entity->setCover(null);

                foreach ($entity->getChildren() as $child) {
                    $this->delete($child, false);
                }
            }
        }

        if ($entity->getStatus() === 2 && $entity->getType() === PostTypeEnum::SERIES) {

            foreach ($entity->getChildren() as $child) {
                $child->setIsDeleted(true);
            }
            $this->_em->getUnitOfWork()->computeChangeSets();
        }

        if ($entity->getType() === PostTypeEnum::SERIES_FROM_PUBLISHED || $entity->getType() === PostTypeEnum::SERIES_FROM_CURATOR) {
            foreach ($entity->getChildren() as $child) {
                $child->setParent(null);
                $this->_em->persist($child);
            }
        }

        if (!$soft) {

            $soft = true;

            $this->S3FileManager->remove($entity->getImage()->getUrl());
            $this->S3FileManager->remove($entity->getImage()->getLarge());
            $this->S3FileManager->remove($entity->getImage()->getSmall());
            $this->S3FileManager->remove($entity->getImage()->getSocial());
            $this->S3FileManager->remove($entity->getImage()->getPrint());
            $this->S3FileManager->remove($entity->getImage()->getPrintThumbnail());

        }

    }

}
