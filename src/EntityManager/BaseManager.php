<?php
/** @noinspection MagicMethodsValidityInspection */

namespace App\EntityManager;

use App\Entity\EntityInterface;
use App\Entity\Notification\Notification;
use App\Exception\ApiException;
use App\Helper\CacheTrait;
use App\Helper\SerializationTrait;
use App\Helper\ValidationTrait;
use App\Repository\BaseRepository;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\ServiceRepositoryCompilerPass;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use RuntimeException;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class BaseManager
 *
 * @package Paynetics\Manager
 *
 * @property BaseRepository repository
 *
 */
class BaseManager
{
    use SerializationTrait,
        ValidationTrait,
        CacheTrait;

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $_em;

    /**
     * @var BaseRepository
     */
    protected $_repository;

    /**
     * @var string
     */
    protected ?string $entity;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param string $entity
     */
    public function __construct(EntityManagerInterface $em, string $entity = null)
    {
        $this->_em = $em;
        $this->entity = $entity;

    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        if ($name === 'repository') {

            if ($this->_repository === null) {
                $this->_repository = $this->getRepository($this->entity);
            }

            return $this->_repository;
        }

        if ($name === 'em') {
            return $this->_em;
        }

    }

    /**
     * @param $data
     * @param bool $validate
     * @param string|array $groups
     * @param bool $build
     *
     * @return object|string
     *
     * @throws ApiException
     */
    public function create($data, bool $validate = true, $groups = 'create', bool $build = false)
    {

        if (is_string($data)) {
            /**
             * @var $data EntityInterface
             */
            $data = $this->deserialize($data ?: '{}', $this->entity, compact('groups'));
        }

        if ($validate) {
            $this->validate($data, $groups);
        }

        $this->_em->persist($data);

        $this->preCreate($data);

        $this->preFlush($data);

        $this->_em->flush();

        $this->postCreate($data);


        return $build ? $this->serialize($data, $groups) : $data;
    }

    /**
     * @param $data
     * @param EntityInterface|int $entity
     * @param bool $validate
     * @param string|array $groups
     * @param bool $build
     *
     * @return EntityInterface|string
     *
     * @throws ApiException
     */
    public function update($data,
                           $entity,
                           bool $validate = true,
                           $groups = 'update',
                           bool $build = false)
    {
        if (!$entity instanceof EntityInterface) {
            /**
             * @var $entity EntityInterface
             */
            $entity = $this->repository->find($entity);
        }

        if (is_string($data)) {
            /**
             * @var $data EntityInterface
             */
            $data = $this->deserialize($data, $this->entity, compact('groups'), $entity);
        }

        if ($validate) {
            $this->validate($data, $groups);
        }

        $this->preUpdate($data);

        $this->preFlush($data);

        $this->_em->flush();

        $this->postUpdate($data);

        return $build ? $this->serialize($data, $groups) : $data;
    }

    /**
     * @param $entity
     * @param bool $soft
     *
     * @return bool
     */
    public function delete($entity, bool $soft = true): bool
    {

        if($entity == null){
            return false;
        }

        if (is_int($entity)) {
            /**
             * @var $data EntityInterface
             */
            $entity = $this->repository->find($entity);

        }

        $this->preDelete($entity, $soft);

        if ($soft) {

            $entity->setIsDeleted(true);

            $this->_em->flush();

            return true;
        }

        $this->_em->remove($entity);

        $this->_em->flush();

        return true;

    }

    /**
     * @param null $entity
     *
     * @throws Exception
     */
    public function flush($entity = null): void
    {

        if ($entity) {

            $this->preFlush($entity);
            $this->_em->getUnitOfWork()->commit($entity);

            return;
        }

        $this->_em->flush();
    }

    /**
     * @param EntityInterface $entity
     */
    protected function preCreate(EntityInterface $entity): void
    {

    }

    /**
     * @param EntityInterface $entity
     */
    protected function preUpdate(EntityInterface $entity): void
    {

    }

    /**
     * @param EntityInterface $entity
     */
    protected function preFlush(EntityInterface $entity): void
    {

    }

    /**
     * @param EntityInterface $entity
     * @param bool $soft
     */
    protected function preDelete(EntityInterface $entity, bool &$soft): void
    {

    }

    /**
     * @param EntityInterface $entity
     */
    protected function postCreate(EntityInterface $entity): void
    {

    }

    /**
     * @param EntityInterface $entity
     */
    protected function postUpdate(EntityInterface $entity): void
    {

    }

    /**
     * {@inheritdoc}
     */
    private function getRepository(string $entityName)
    {
        $metadata = $this->_em->getClassMetadata($entityName);
        $repositoryServiceId = $metadata->customRepositoryClassName;

        $customRepositoryName = $metadata->customRepositoryClassName;
        if ($customRepositoryName !== null) {
            // fetch from the container
            if ($this->container && $this->container->has($customRepositoryName)) {
                $repository = $this->container->get($customRepositoryName);

                if (!$repository instanceof ObjectRepository) {
                    throw new RuntimeException(sprintf('The service "%s" must implement ObjectRepository (or extend a base class, like ServiceEntityRepository).', $repositoryServiceId));
                }

                return $repository;
            }

            if (!class_exists($customRepositoryName)) {
                throw new RuntimeException(sprintf('The "%s" entity has a repositoryClass set to "%s", but this is not a valid class. Check your class naming. If this is meant to be a service id, make sure this service exists and is tagged with "%s".', $metadata->name, $customRepositoryName, ServiceRepositoryCompilerPass::REPOSITORY_SERVICE_TAG));
            }

        }

        $class = explode('\\', $metadata->getName());

        return $this->container->get('app.repository.' . Container::underscore(end($class)));
    }

}
