<?php

namespace App\EntityManager;

use App\Entity\Poll\Poll;
use App\Entity\Poll\PollChoice;
use App\Entity\User\User;
use App\Repository\Poll\PollChoiceRepository;
use App\Repository\Poll\PollRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class PollManager
 * @package App\EntityManager
 */
class PollManager
{
    const POLL_ACTIVE_KEY = 'poll_active';
    const POLL_VOTED_KEY = 'poll_voted_';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var PollChoiceRepository
     */
    private PollChoiceRepository $choiceRepository;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var AdapterInterface
     */
    private AdapterInterface $cache;

    /**
     * @var PollRepository
     */
    private PollRepository $pollRepository;

    /**
     * @var Security
     */
    private Security $security;

    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * PollManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param PollChoiceRepository $choiceRepository
     * @param PollRepository $pollRepository
     * @param SessionInterface $session
     * @param AdapterInterface $cache
     * @param Security $security
     * @param Environment $twig
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PollChoiceRepository $choiceRepository,
        PollRepository $pollRepository,
        SessionInterface $session,
        AdapterInterface $cache,
        Security $security,
        Environment $twig
    )
    {
        $this->entityManager = $entityManager;
        $this->choiceRepository = $choiceRepository;
        $this->session = $session;
        $this->cache = $cache;
        $this->pollRepository = $pollRepository;
        $this->security = $security;
        $this->twig = $twig;
    }

    /**
     * @param string[] $choices
     * @param Poll $poll
     */
    public function persistChoices($choices, Poll $poll)
    {
        $choices = array_unique(
            array_filter(array_map('trim', $choices), function ($item) {
                return $item != '' && null !== $item;
            })
        );

        $definedChoices = $this->choiceRepository->findBy([
            'poll' => $poll
        ]);

        foreach ($definedChoices as $definedChoice) {
            if (!in_array($definedChoice->getTitle(), $choices)) {
                $this->entityManager->remove($definedChoice);
            }
        }

        for ($i = 0; $i < count($choices); $i++) {
            if (!$oldChoice = $this->choiceRepository->findOneBy(
                [
                    'title' => $choices[$i],
                    'poll' => $poll
                ]
            )) {
                $choice = new PollChoice();
                $choice->setTitle($choices[$i]);
                $choice->setPoll($poll);
                $choice->setOrderIndex($i);

                $this->entityManager->persist($choice);
            } else {
                $oldChoice->setOrderIndex($i);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @return Poll|null
     * @throws NonUniqueResultException
     * @throws InvalidArgumentException
     */
    public function getActivePoll()
    {
        $item = $this->cache->getItem(self::POLL_ACTIVE_KEY);

        if (!$item->isHit()) {
            $activePoll = $this->pollRepository->findOneActivePoll();

            $item->set(
                $activePoll instanceof Poll ? $activePoll->getId() : 0
            );

            $this->cache->save($item);
        }

        if (filter_var($item->get(), FILTER_VALIDATE_INT) && 0 !== $item->get()) {
            $sessionKey = $this->buildSessionKey($item->get());

            if (
                !$this->session->has($sessionKey)
                || false === $this->session->get($sessionKey)
            ) {
                if (!isset($activePoll)) {
                    $activePoll = $this->pollRepository->findOneActivePoll();
                }

                /** @var User $currentUser */
                $currentUser = $this->security->getUser();

                $voted = $activePoll->hasUser($currentUser);

                $this->session->set($sessionKey, $voted);

                if (!$voted) {
                    return $activePoll;
                }
            }
        }

        return null;
    }

    /**
     * @param Poll $poll
     * @return string
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getPollResultsAsHtml(Poll $poll)
    {
        $total = $this->pollRepository->getTotalVotesPerPoll($poll);

        return $this->twig->render('poll/_results.html.twig', [
            'poll' => $poll,
            'total' => $total
        ]);
    }

    /**
     * @param int $pollId
     */
    public function setPoolAsVoted(int $pollId)
    {
        $this->session->set($this->buildSessionKey($pollId), true);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function clearCache()
    {
        $this->cache->deleteItem(self::POLL_ACTIVE_KEY);
    }

    /**
     * @param int $pollId
     * @return string
     */
    private function buildSessionKey(int $pollId): string
    {
        return self::POLL_VOTED_KEY . $pollId;
    }
}
