<?php

namespace App\EntityManager;

use App\Entity\EntityInterface;
use App\Entity\Photo\Post;

/**
 * Class AlbumManager
 * @package App\Manager
 */
class AlbumManager extends BaseManager
{

    /**
     * @param EntityInterface $entity
     */
    protected function preFlush(EntityInterface $entity): void
    {
        if (is_string($entity->getOrder())) {
            $order = explode(',', $entity->getOrder());

            /**
             * @var Post $child
             */
            foreach ($entity->getPosts() as $i => $child) {

                if ($child->getId() === (int)$order[0]) {
                    $entity->setCover($child);
                }

                $this->_em->persist($child);

            }

        }
    }
}
