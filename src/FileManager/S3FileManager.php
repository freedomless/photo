<?php

namespace App\FileManager;

use Aws\CommandPool;
use Aws\S3\S3Client;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class PhotoUploadManager
 * @package App\FileManager
 */
class S3FileManager implements FileManagerInterface
{
    /**
     * @var S3Client
     */
    private S3Client $client;

    /**
     * @var array;
     */
    private array $queue;

    /**
     * @var bool
     */
    private bool $isBatch;

    /**
     * PhotoUploadManager constructor.
     * @param S3Client $client
     */
    public function __construct(S3Client $client)
    {
        $this->isBatch = false;

        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function get(string $uri): string
    {
        preg_match('/\.\w+\/(\w.+)/', $uri, $match);

        $object = $this->client->getCommand('GetObject', [
            'Bucket' => getenv('AWS_S3_BUCKET'),
            'Key'    => $match[1],
        ]);

        $request = $this->client->createPresignedRequest($object, '+5 minutes');

        return (string)$request->getUri();
    }

    /**
     * @param mixed $file
     * @param string $folder
     * @param string|null $name
     * @param bool $isPrivate
     *
     * @return string|null
     *
     * @throws Exception
     */
    public function add($file, string $folder, ?string $name = null, bool $isPrivate = false): ?string
    {
        //Create file name and append extension
        $uri = $folder . (random_int(1000000, 9999999) . '.' . time() . '.' . bin2hex(random_bytes(8)) . '.' . 'jpg');

        //Get file content
        $content = $file instanceof UploadedFile ? fopen($file->getPathname(), 'rb') : $file;

        if ($this->isBatch()) {
            $this->queue[] = $this->client->getCommand('PutObject', ['Bucket' => getenv('AWS_S3_BUCKET'), 'Key' => $uri, 'Body' => $content, 'ACL' => $isPrivate ? 'private' : 'public-read', 'ContentType' => 'image/jpeg']);

            return null;
        }

        $response = $this->client->upload(getenv('AWS_S3_BUCKET'), $uri, $content, $isPrivate ? 'private' : 'public-read');

        return $response->get('ObjectURL');
    }

    /**
     * @inheritDoc
     */
    public function remove(?string $uri): bool
    {
        try {

            if ($uri === null) {
                return true;
            }

            preg_match('/\.\w+\/(\w.+)/', $uri, $match);

            $this->client->deleteObject([
                'Bucket' => getenv('AWS_S3_BUCKET'),
                'Key'    => $match[1],
            ]);

            return true;

        } catch (\Throwable $e) {
            return false;
        }
    }

    /**
     * @return array
     */
    public function execute(): array
    {
        $responses = CommandPool::batch($this->client, $this->queue);

        $this->queue = [];

        $urls = [];

        foreach ($responses as $response) {
            $urls[] = $response->get('ObjectURL');
        }

        return $urls;
    }

    /**
     * @return bool
     */
    public function isBatch(): bool
    {
        return $this->isBatch;
    }

    /**
     * @param bool $isBatch
     * @return S3FileManager
     */
    public function setIsBatch(bool $isBatch): self
    {
        $this->isBatch = $isBatch;

        return $this;
    }

}
