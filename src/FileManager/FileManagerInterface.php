<?php

namespace App\FileManager;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FileManagerInterface
 * @package App\FileManager
 */
interface FileManagerInterface
{
    /**
     * @param string $uri
     * @return string
     */
    public function get(string $uri): string;

    /**
     * @param UploadedFile $file
     * @param string $folder
     * @param string|null $name
     * @param bool $isPrivate
     * @return string|null
     */
    public function add($file, string $folder, ?string $name = null, bool $isPrivate = false): ?string;

    /**
     * @param string $uri
     * @return bool
     */
    public function remove(string $uri): bool;
}
