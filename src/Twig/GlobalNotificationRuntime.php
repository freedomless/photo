<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * Class GlobalNotificationRuntime
 * @package App\Twig
 */
class GlobalNotificationRuntime implements RuntimeExtensionInterface
{
    /**
     * @var Security
     */
    private Security $security;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * GlobalNotificationRuntime constructor.
     * @param Security $security
     * @param SessionInterface $session
     */
    public function __construct(Security $security, SessionInterface $session)
    {
        $this->security = $security;
        $this->session = $session;
    }

    /**
     * @param Environment $environment
     * @param string $templateName
     * @param string $userRole
     * @param bool $isGranted
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderNotification(
        Environment $environment,
        string $templateName,
        string $userRole,
        bool $isGranted = true
    )
    {
        if ($this->security->isGranted($userRole) === $isGranted) {

            if (!$this->session->has(hash('md5', $templateName))) {
                $templateFile = 'site_notifications/' . $templateName . '.html.twig';

                return $environment->render($templateFile, ['templateName' => $templateName]);
            }

        }
    }
}
