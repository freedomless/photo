<?php

namespace App\Twig;

use App\Repository\BaseRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class CartCountExtension
 * @package App\Twig
 */
class CartCountExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var BaseRepository
     */
    private BaseRepository $cartRepository;

    /**
     * CartCountExtension constructor.
     * @param ContainerInterface $container
     * @param BaseRepository $cartRepository
     */
    public function __construct(ContainerInterface $container, BaseRepository $cartRepository)
    {
        $this->container = $container;
        $this->cartRepository = $cartRepository;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('cart_count', [$this, 'getCartCount']),
        ];
    }

    /**
     * @return int
     */
    public function getCartCount(): int
    {
        $storage = $this->container->get('token');
        $session= $this->container->get('session')->getId();

        $user = $storage->getToken()->getUser() !== 'anon.' ? $storage->getToken()->getUser() : null;

        if ($user) {
            $cart = $this->cartRepository->findOneBy(compact('user'), [], false);

        } else {
            $cart = $this->cartRepository->findOneBy(compact('session'), [], false);
        }

        if ($cart) {
            return $cart->getItems()->count();
        }

        return 0;

    }

    /**
     * Returns an array of service types required by such instances, optionally keyed by the service names used internally.
     *
     * For mandatory dependencies:
     *
     *  * ['logger' => 'Psr\Log\LoggerInterface'] means the objects use the "logger" name
     *    internally to fetch a service which must implement Psr\Log\LoggerInterface.
     *  * ['loggers' => 'Psr\Log\LoggerInterface[]'] means the objects use the "loggers" name
     *    internally to fetch an iterable of Psr\Log\LoggerInterface instances.
     *  * ['Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => 'Psr\Log\LoggerInterface']
     *
     * otherwise:
     *
     *  * ['logger' => '?Psr\Log\LoggerInterface'] denotes an optional dependency
     *  * ['loggers' => '?Psr\Log\LoggerInterface[]'] denotes an optional iterable dependency
     *  * ['?Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => '?Psr\Log\LoggerInterface']
     *
     * @return array The required service types, optionally keyed by service names
     */
    public static function getSubscribedServices(): array
    {
        return [
            'token'   => TokenStorageInterface::class,
            'session' => SessionInterface::class
        ];
    }
}
