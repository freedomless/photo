<?php

namespace App\Twig;

use App\Entity\User\Balance;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UserBalanceExtension extends AbstractExtension
{
    /**
     * @var BaseManager
     */
    private BaseManager $balanceManager;

    public function __construct(BaseManager $balanceManager)
    {
        $this->balanceManager = $balanceManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('balance', [$this, 'getBalance']),
        ];
    }

    public function getBalance(User $user)
    {

        /**
         * @var Balance $balance
         */
        $balance = $this->balanceManager->repository->findOneBy(['user' => $user->getId()],[],false);

        if($balance){
            return $balance;
        }

        return 0;
    }
}
