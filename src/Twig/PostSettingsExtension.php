<?php

namespace App\Twig;

use App\Entity\User\User;
use App\Repository\SettingsRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFunction;

/**
 * Class UserSettingsExtension
 * @package App\Twig
 */
class PostSettingsExtension extends AbstractExtension implements ServiceSubscriberInterface, GlobalsInterface
{

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * UserSettingsExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('user_settings', [$this, 'getUserSettings']),
        ];
    }

    public function getUserSettings($user)
    {
        $repository = $this->container->get('repository');

        return $repository->getSettings($user) ?? ['is_web_notification_allowed' => false,'is_portfolio_visible' => false];
    }

    /**
     * @return array|null
     */
    public function getPostSettings()
    {
        $repository = $this->container->get('repository');
        $storage = $this->container->get('storage');

        $user = $storage->getToken() ? $storage->getToken()->getUser() : false;

        if ($user instanceof User) {

            $settings = $repository->getSettings($user);

            return ['nude' => $settings['show_nudes'] ?? false, 'auth' => $user->getId()];
        }

        return ['nude' => false, 'auth' => false];;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedServices()
    {
        return [
            'repository' => SettingsRepository::class,
            'storage'    => TokenStorageInterface::class,
        ];
    }

    /**
     * @return array
     */
    public function getGlobals(): array
    {
        return $this->getPostSettings();
    }
}
