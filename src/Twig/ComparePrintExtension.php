<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ComparePrintExtension extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_compare_style', [ComparePrintRuntime::class, 'getStyle'], [
                'is_safe' => ['html'],
            ]),
        ];
    }
}
