<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class GlobalNotificationExtension
 * @package App\Twig
 */
class GlobalNotificationExtension extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('print_notification', [GlobalNotificationRuntime::class, 'renderNotification'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }
}
