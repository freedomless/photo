<?php

namespace App\Twig;

use App\Repository\SizeRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class SizeExtension
 * @package App\Twig
 */
class SizeExtension extends AbstractExtension
{
    /**
     * @var SizeRepository
     */
    private SizeRepository $sizeRepository;

    /**
     * SizeExtension constructor.
     * @param SizeRepository $sizeRepository
     */
    public function __construct(SizeRepository $sizeRepository) {
        $this->sizeRepository = $sizeRepository;
    }


    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('sizes', [$this, 'sizes']),
        ];
    }

    /**
     * @param $value
     */
    public function sizes()
    {
        return $this->sizeRepository->getSizes();
    }
}
