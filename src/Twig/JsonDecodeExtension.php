<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class JsonDecodeExtension
 * @package App\Twig
 */
class JsonDecodeExtension extends AbstractExtension
{
    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('json_decode', [$this, 'decodeJson']),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('json_decode', [$this, 'decodeJson']),
        ];
    }

    /**
     * @param $value
     * @return mixed
     */
    public function decodeJson($value)
    {
        return json_decode($value, true);
    }
}
