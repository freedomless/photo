<?php

namespace App\Twig;

use App\Entity\Photo\Image;
use App\Helper\ProductHelper;
use App\Repository\ConfigurationRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class ProductNeedPrintExtension
 * @package App\Twig
 */
class ProductNeedPrintExtension extends AbstractExtension
{
    /**
     * @var ConfigurationRepository
     */
    private ConfigurationRepository $repository;

    /**
     * ProductNeedPrintExtension constructor.
     * @param ConfigurationRepository $repository
     */
    public function __construct(ConfigurationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [

            new TwigFilter('need_print', [$this, 'doSomething']),
        ];
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('need_print', [$this, 'doSomething']),
        ];
    }

    /**
     * @param Image $image
     */
    public function doSomething(Image $image)
    {
        $min = $this->repository->getConfig()->getMinPrintSize();

        return ProductHelper::needPrint($image, $min);

    }
}
