<?php

namespace App\Twig;

use App\Entity\Store\Order;
use App\Entity\Store\OrderItem;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class FilterAdminActionsExtension
 * @package App\Twig
 */
class FilterAdminActionsExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_admin_actions', [$this, 'doSomething']),
        ];
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('filter_admin_actions', [$this, 'doSomething']),
        ];
    }

    /**
     * @param array $itemActions
     * @param Order $order
     * @return array
     */
    public function doSomething(array $itemActions, Order $order)
    {
        $updated = 0;

        /**
         * @var OrderItem $item
         */
        foreach ($order->getItems() as $item){

            if($item->getCommissionAddedToBalance()){
                ++$updated;
            }

        }

        if($updated === count($order->getItems())){
            unset($itemActions['updateBalance']);
        }

        return $itemActions;
    }
}
