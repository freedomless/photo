<?php

namespace App\Twig;

use App\Repository\ConfigurationRepository;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ConfigExtension extends AbstractExtension implements ServiceSubscriberInterface
{

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('config', [$this, 'getConfig']),
        ];
    }

    public function getConfig()
    {
       return $this->container->get('repository')->getConfig();
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedServices()
    {
      return ['repository' => ConfigurationRepository::class];
    }
}
