<?php

namespace App\Twig;

use App\Entity\Poll\Poll;
use App\Form\Type\PollVoteType;
use App\EntityManager\PollManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * Class PollRuntime
 * @package App\Twig
 */
class PollRuntime implements RuntimeExtensionInterface
{
    /**
     * @var Security
     */
    private Security $security;

    /**
     * @var PollManager
     */
    private PollManager $pollManager;

    /**
     * @var FormFactoryInterface
     */
    private FormFactoryInterface $formFactory;

    /**
     * PollRuntime constructor.
     * @param Security $security
     * @param PollManager $pollManager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        Security $security,
        PollManager $pollManager,
        FormFactoryInterface $formFactory
    )
    {
        $this->security = $security;
        $this->pollManager = $pollManager;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Environment $environment
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function renderPoll(Environment $environment)
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $poll = $this->pollManager->getActivePoll();


            if ($poll instanceof Poll) {
                $form = $this->formFactory->create(PollVoteType::class);

                return $environment->render('poll/_vote.html.twig', [
                    'poll' => $poll,
                    'form' => $form->createView(),
                ]);
            }
        }
    }
}
