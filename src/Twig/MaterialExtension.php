<?php

namespace App\Twig;

use App\Repository\MaterialRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class MaterialExtension
 * @package App\Twig
 */
class MaterialExtension extends AbstractExtension
{
    /**
     * @var MaterialRepository
     */
    private MaterialRepository $materialRepository;

    /**
     * MaterialExtension constructor.
     * @param MaterialRepository $materialRepository
     */
    public function __construct(MaterialRepository $materialRepository) {
        $this->materialRepository = $materialRepository;
    }



    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('materials', [$this, 'materials']),
        ];
    }

    /**
     * @param $value
     */
    public function materials()
    {
        return $this->materialRepository->getMaterials();
    }
}
