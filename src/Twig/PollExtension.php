<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class PollExtension
 * @package App\Twig
 */
class PollExtension extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('print_poll', [PollRuntime::class, 'renderPoll'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }
}
