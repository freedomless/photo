<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ActiveNavBarExtension
 * @package App\Twig
 */
class ActiveNavBarExtension extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    private RequestStack $request;

    /**
     * ActiveNavBarExtension constructor.
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('active', [$this, 'isActive']),
        ];
    }

    /**
     * @param $value
     * @param $param
     * @param string $class
     * @return string
     */
    public function isActive($value, $param, string $class = 'active'): string
    {
        $route = $this->request->getCurrentRequest()->get($param);

        return $route === $value ? $class : '';
    }
}
