<?php

namespace App\Twig;

use App\FileManager\S3FileManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class SignedLinkExtension
 * @package App\Twig
 */
class SignedLinkExtension extends AbstractExtension
{
    /**
     * @var S3FileManager
     */
    private S3FileManager $fileManager;

    /**
     * SignedLinkExtension constructor.
     * @param S3FileManager $fileManager
     */
    public function __construct(S3FileManager  $fileManager) {
        $this->fileManager = $fileManager;
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('signed_link', [$this, 'generate']),
        ];
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('signed_link', [$this, 'generate']),
        ];
    }

    /**
     * @param $value
     */
    public function generate($value)
    {
        return $this->fileManager->get($value);
    }
}
