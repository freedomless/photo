<?php

namespace App\Twig;

use App\EntityManager\UserManager;
use App\Repository\UserRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class AccountStatsExtension
 * @package App\Twig
 */
class AccountStatsExtension extends AbstractExtension implements ServiceSubscriberInterface
{

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var array|null
     */
    private $stats;

    /**
     * AccountStatsExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('account_stats', [$this, 'getAccountStats']),
        ];
    }

    /**
     * @param string|null $slug
     * @return mixed
     */
    public function getAccountStats(?string $slug = null)
    {
        $user = null;
        $repository = $this->container->get('repository');
        $token = $this->container->get('token');

        if ($slug !== null) {
            $user = $repository->findOneBy(compact('slug'));
        }

        if ($slug === null) {
            $token = $token->getToken();
            $user = $token->getUser();
        }

        if ($user === null || !$user instanceof UserInterface) {
            return ['products' => '', 'products_single' => '', 'products_series' => '', 'single_portfolio' => '', 'single_published' => '', 'published' => '', 'series_published' => '', 'series_portfolio' => '', 'portfolio' => '', 'albums' => '', 'followers' => '', 'favorites' => '', 'followed' => ''];
        }

        $this->stats === null ? $this->stats = $repository->getAccountStats($user) : $this->stats;

        return $this->stats;
    }

    /**
     * Returns an array of service types required by such instances, optionally keyed by the service names used internally.
     *
     * For mandatory dependencies:
     *
     *  * ['logger' => 'Psr\Log\LoggerInterface'] means the objects use the "logger" name
     *    internally to fetch a service which must implement Psr\Log\LoggerInterface.
     *  * ['loggers' => 'Psr\Log\LoggerInterface[]'] means the objects use the "loggers" name
     *    internally to fetch an iterable of Psr\Log\LoggerInterface instances.
     *  * ['Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => 'Psr\Log\LoggerInterface']
     *
     * otherwise:
     *
     *  * ['logger' => '?Psr\Log\LoggerInterface'] denotes an optional dependency
     *  * ['loggers' => '?Psr\Log\LoggerInterface[]'] denotes an optional iterable dependency
     *  * ['?Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => '?Psr\Log\LoggerInterface']
     *
     * @return array The required service types, optionally keyed by service names
     */
    public static function getSubscribedServices()
    {
        return [
            'repository' => UserRepository::class,
            'token'      => TokenStorageInterface::class
        ];
    }
}
