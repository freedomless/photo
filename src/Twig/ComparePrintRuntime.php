<?php

namespace App\Twig;

use App\Enum\ImageSizeEnum;
use Twig\Extension\RuntimeExtensionInterface;

class ComparePrintRuntime implements RuntimeExtensionInterface
{
    const MIN_HEIGHT = 'min-height: 1024px;';
    const MIN_WIDTH = 'min-width: 1024px';

    /**
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getStyle(int $width, int $height): string
    {
        $min = intval(ImageSizeEnum::IMAGE_THUMB_MID[0]);

        if ($width > $height) {
            if ($width < $min) {
                return self::MIN_WIDTH;
            }
        } else {
            if ($height < $min) {
                return self::MIN_HEIGHT;
            }
        }

        return '';
    }
}
