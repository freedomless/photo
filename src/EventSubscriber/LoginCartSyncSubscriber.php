<?php

namespace App\EventSubscriber;

use App\Entity\Store\Cart;
use App\Entity\Store\CartItem;
use App\EntityManager\BaseManager;
use App\EntityManager\ProductManager;
use App\Exception\ApiException;
use App\Helper\SerializationTrait;
use App\Repository\MaterialRepository;
use App\Repository\SizeRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class LoginCartSyncSubscriber
 * @package App\EventSubscriber
 */
class LoginCartSyncSubscriber implements EventSubscriberInterface
{
    use SerializationTrait;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var BaseManager
     */
    private BaseManager $cartManager;

    /**
     * @var BaseManager
     */
    private BaseManager $cartItemManager;

    /**
     * @var BaseManager
     */
    private BaseManager $productManager;

    /**
     * @var MaterialRepository
     */
    private MaterialRepository $materialRepository;

    /**
     * @var SizeRepository
     */
    private SizeRepository $sizeRepository;

    /**
     * LoginCartSyncSubscriber constructor.
     * @param SessionInterface $session
     * @param BaseManager $cartManager
     * @param BaseManager $productManager
     * @param MaterialRepository $materialRepository
     * @param SizeRepository $sizeRepository
     * @param BaseManager $cartItemManager
     */
    public function __construct(
        SessionInterface $session,
        BaseManager $cartManager,
        ProductManager $productManager,
        MaterialRepository $materialRepository,
        SizeRepository $sizeRepository,
        BaseManager $cartItemManager
    )
    {
        $this->session = $session;
        $this->cartManager = $cartManager;
        $this->cartItemManager = $cartItemManager;
        $this->productManager = $productManager;
        $this->materialRepository = $materialRepository;
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param AuthenticationEvent $event
     * @return void
     * @throws \Exception
     */
    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $userCart = $this->cartManager->repository->findOneBy(compact('user'), [], false);

        if (!$user instanceof UserInterface || $userCart !== null) {
            return;
        }

        $session = $this->session->getId();


        /**
         * @var Cart $sessionCart
         */
        $sessionCart = $this->cartManager->repository->findOneBy(compact('session'), [], false);

        $userCart = new Cart();
        $userCart->setUser($user);

        $this->cartManager->create($userCart);

        if ($sessionCart) {

            foreach ($sessionCart->getItems() as $item) {
                $item->setCart($userCart);
            }
        }

        $this->cartManager->flush();

        $this->cartManager->delete($sessionCart, false);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            //'security.authentication.success' => 'onSecurityAuthenticationSuccess',
        ];
    }
}
