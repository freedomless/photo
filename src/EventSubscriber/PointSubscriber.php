<?php

namespace App\EventSubscriber;

use App\Entity\EntityInterface;
use App\Entity\Notification\Notification;
use App\Entity\User\Point;
use App\Entity\User\PointType;
use App\Entity\User\User;
use App\EntityManager\BaseManager;
use App\Event\Notification\CommentRepliedEvent;
use App\Event\Notification\CurateLimitActiveEvent;
use App\Event\Notification\CurateLimitLeftEvent;
use App\Event\Notification\CurateLimitReachedEvent;
use App\Event\Notification\InvoiceCreatedEvent;
use App\Event\Notification\NewMessageEvent;
use App\Event\Notification\NotificationEvent;
use App\Event\Notification\PayoutRequestedEvent;
use App\Event\Notification\PhotoAwardedEvent;
use App\Event\Notification\PhotoCommentedEvent;
use App\Event\Notification\PhotoLikedEvent;
use App\Event\Notification\PhotoPublishedEvent;
use App\Event\Notification\TrackIdAddedEvent;
use App\Event\Notification\UserConfirmedEvent;
use App\Event\Notification\UserRegisteredEvent;
use App\Event\Point\CommentPointEvent;
use App\Event\Point\PhotoCuratePointEvent;
use App\Event\Point\PointEvent;
use App\Event\Point\PostCreatedPointEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use App\Mailer\Mailer;
use App\Repository\BaseRepository;
use App\Repository\SettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class NotificationSubscriber
 * @package App\EventSubscriber
 */
class PointSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * NotificationSubscriber constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CommentPointEvent::class     => 'handle',
            PhotoCuratePointEvent::class => 'handle',
            PostCreatedPointEvent::class => 'handle',
        ];
    }

    /**
     * @param PointEvent $event
     * @return bool
     */
    public function handle(PointEvent $event)
    {
        /**
         * @var PointType $type
         */
        $type = $this->em->getRepository(PointType::class)->findOneBy(['name' => $event->getType()]);

        if ($type === null) {
            return false;
        }

        if ($type->getEnabled() === false) {
            return false;
        }

        $user = $event->getReceiver();

        $user->setPoint($user->getPoint() + $type->getPoints());

        $this->em->flush();
    }

}
