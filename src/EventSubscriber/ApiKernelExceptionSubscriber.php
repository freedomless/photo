<?php

namespace App\EventSubscriber;

use App\Exception\ApiException;
use App\Exception\ExceptionCodes;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ApiKernelExceptionSubscriber
 * @package App\EventSubscriber
 */
class ApiKernelExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * ApiKernelExceptionSubscriber constructor.
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {

        return;
        $api = stripos($event->getRequest()->attributes->get('_route'), 'api') === 0;

        $event->allowCustomResponseCode();

        $exception = $event->getThrowable();

        $code = $this->getExceptionCode($exception);

        $this->logError($code, $exception);

        if (!$api) {
            throw new HttpException(500, $code);
        }

        $response = [
            'code'    => $code,
            'message' => $this->translator->trans($code),
            'key'     => $code,
            'errors'  => $this->getErrors($exception)
        ];

        $event->setResponse(new JsonResponse(array_filter($response)));

    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }

    /**
     * @param $exception
     * @return int
     */
    private function getExceptionCode($exception): int
    {
        if ($exception instanceof UniqueConstraintViolationException) {
            return ExceptionCodes::NON_UNIQUE_VALUE;
        }

        if ($exception instanceof UnexpectedValueException) {
            return ExceptionCodes::UNPROCESSABLE_ENTITY;
        }

        if ($exception instanceof ApiException) {
            return $exception->getCode();
        }

        if ($exception instanceof HttpException) {

            switch ($exception->getStatusCode()) {
                case 404:
                    return ExceptionCodes::HTTP_NOT_FOUND_EXCEPTION;
                case 500:
                    return ExceptionCodes::HTTP_SERVER_EXCEPTION;
                default:
                    return ExceptionCodes::HTTP_EXCEPTION;

            }
        }

        return ExceptionCodes::GENERAL;
    }

    /**
     * @param $exception
     * @return array|null
     */
    private function getErrors(\Throwable $exception): ?array
    {
        if (method_exists($exception, 'getErrors')) {
            return $exception->getErrors();
        }

        return null;
    }

    /**
     * @param $code
     * @param $exception
     */
    private function logError($code, $exception): void
    {
        $unknown = $code === ExceptionCodes::GENERAL;

        $this->logger->error('---------------- START EXCEPTION ----------------');
        $this->logger->error("Message: {$exception->getMessage()}");
        $this->logger->error("File: {$exception->getFile()}");
        $this->logger->error("Line: {$exception->getLine()}");
        $this->logger->error('---------------- END EXCEPTION ----------------');

        $message = "Message: {$exception->getMessage()} \n";
        $message .= "File: {$exception->getFile()} \n";
        $message .= "Line: {$exception->getLine()}\n";

        $unknown ? $this->logger->critical($message) : null;

    }
}
