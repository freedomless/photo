<?php

namespace App\EventSubscriber;

use App\Entity\EntityInterface;
use App\Entity\Notification\Notification;
use App\EntityManager\BaseManager;
use App\Event\Notification\CommentRepliedEvent;
use App\Event\Notification\CurateLimitActiveEvent;
use App\Event\Notification\CurateLimitLeftEvent;
use App\Event\Notification\CurateLimitReachedEvent;
use App\Event\Notification\ForgottenPasswordEvent;
use App\Event\Notification\FundsReceivedEvent;
use App\Event\Notification\InvoiceCreatedEvent;
use App\Event\Notification\NewMessageEvent;
use App\Event\Notification\NotificationEvent;
use App\Event\Notification\PayoutCompletedEvent;
use App\Event\Notification\PayoutRequestedEvent;
use App\Event\Notification\PhotoAwardedEvent;
use App\Event\Notification\PhotoCommentedEvent;
use App\Event\Notification\PhotoLikedEvent;
use App\Event\Notification\PhotoPublishedEvent;
use App\Event\Notification\SeriesPublishedEvent;
use App\Event\Notification\SeriesSelectedEvent;
use App\Event\Notification\System\NewOrderEvent;
use App\Event\Notification\TrackIdAddedEvent;
use App\Event\Notification\UserConfirmedEvent;
use App\Event\Notification\UserFollowedEvent;
use App\Event\Notification\UserRegisteredEvent;
use App\Exception\ApiException;
use App\Helper\DispatcherTrait;
use App\Mailer\Mailer;
use App\Repository\BaseRepository;
use App\Repository\SettingsRepository;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class NotificationSubscriber
 * @package App\EventSubscriber
 */
class NotificationSubscriber implements EventSubscriberInterface
{
    use DispatcherTrait;

    /**
     * @var Mailer
     */
    private Mailer $mailer;

    /**
     * @var SettingsRepository
     */
    private SettingsRepository $settingsRepository;

    /**
     * @var BaseManager
     */
    private BaseManager $notificationManager;

    /**
     * @var BaseRepository
     */
    private BaseRepository $notificationTypeRepository;

    /**
     * @var RouterInterface
     */
    private RouterInterface $router;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * NotificationSubscriber constructor.
     * @param Mailer $mailer
     * @param LoggerInterface $logger
     * @param RouterInterface $router
     * @param SettingsRepository $settingsRepository
     * @param BaseManager $notificationManager
     * @param BaseRepository $notificationTypeRepository
     */
    public function __construct(Mailer $mailer,
                                LoggerInterface $logger,
                                RouterInterface $router,
                                SettingsRepository $settingsRepository,
                                BaseManager $notificationManager,
                                BaseRepository $notificationTypeRepository)
    {
        $this->notificationManager = $notificationManager;
        $this->notificationTypeRepository = $notificationTypeRepository;
        $this->mailer = $mailer;
        $this->settingsRepository = $settingsRepository;
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            //Post Events
            CommentRepliedEvent::class     => 'handleNotification',
            PhotoAwardedEvent::class       => 'handleNotification',
            PhotoCommentedEvent::class     => 'handleNotification',
            PhotoLikedEvent::class         => 'handleNotification',
            PhotoPublishedEvent::class     => 'handleNotification',
            SeriesPublishedEvent::class    => 'handleNotification',
            SeriesSelectedEvent::class     => 'handleNotification',

            //Curate limit Events
            CurateLimitActiveEvent::class  => 'handleNotification',
            CurateLimitLeftEvent::class    => 'handleNotification',
            CurateLimitReachedEvent::class => 'handleNotification',

            //Message Events
            NewMessageEvent::class         => 'handleNotification',

            //Store Events
            InvoiceCreatedEvent::class     => 'handleNotification',
            TrackIdAddedEvent::class       => 'handleNotification',
            NewOrderEvent::class           => 'handleNotification',
            FundsReceivedEvent::class      => 'handleNotification',
            PayoutRequestedEvent::class    => 'handleNotification',
            PayoutCompletedEvent::class    => 'handleNotification',

            //User Events
            UserConfirmedEvent::class      => 'handleNotification',
            UserRegisteredEvent::class     => 'handleNotification',
            UserFollowedEvent::class       => 'handleNotification',
            ForgottenPasswordEvent::class  => 'handleNotification',
        ];
    }

    /**
     * @param NotificationEvent $event
     * @throws ApiException
     */
    public function handleNotification(NotificationEvent $event)
    {
        $event->setRouter($this->router);


        if ($event->getCreate()) {

            $notification = $this->createNotification(
                $event->getSender(),
                $event->getReceiver(),
                $event->getEmail(),
                $event->getType(),
                $event->getPayload());

            if ($notification === false) {
                $this->logger->critical('Notification type not found!');

                return;
            }

            try {
                $this->sendEmailNotification($notification);
            } catch (\Throwable $e) {
                $this->logger->critical('Error sending email.' . $e->getMessage());
            }

        }

        //Handle chained notifications
        while ($event->getNext()) {

            $event = $event->getNext();

            $this->dispatch($event);

            if (!method_exists($event, 'getNext')) {
                break;
            }

        }

    }

    /**
     * @param UserInterface|null $sender
     * @param UserInterface|null $receiver
     * @param string $email
     * @param string $type
     * @param $payload
     * @return EntityInterface|Notification|object|string
     * @throws ApiException
     */
    private function createNotification(?UserInterface $sender, ?UserInterface $receiver, string $email, string $type, $payload)
    {
        $notificationType = $this->notificationTypeRepository->findOneBy(['name' => $type], [], false);


        if ($notificationType === null) {
            return false;
        }

        $notification = new Notification();
        $notification->setType($notificationType);
        $notification->setSender($sender);
        $notification->setReceiver($receiver);
        $notification->setPayload($payload);
        $notification->setEmail($email);



        return $this->notificationManager->create($notification);
    }

    /**
     * @param $notifications
     * @throws InvalidArgumentException
     */
    private function sendEmailNotification($notifications): void
    {
        if (!is_array($notifications)) {
            $notifications = [$notifications];
        }


        foreach ($notifications as $notification) {


            $settings = $this->settingsRepository->getSettings($notification->getReceiver());

            if ($settings && !$settings['is_email_notification_allowed'] && !$notification->getType()->getIsForce()) {
                continue;
            }


            if (!($notification instanceof Notification)) {
                continue;
            }

            if ($notification->getReceiver()) {

                $allowed = $settings['is_email_notification_allowed'];

                if (!$allowed && $notification->getSender() !== null) {
                    continue;
                }

            }

            $settings = $notification->getType();

            if ($settings->getIsEmail()) {
                $this->sendEmail($settings->getSubject(), $notification->getEmail(), $settings->getTemplate(), $notification->getPayload());
                $notification->setIsEmailSent(true);
                $this->notificationManager->flush($notification);
            }
        }
    }

    public function sendEmail(string $subject, string $receiver, string $template, array $payload = [])
    {
        $this->mailer->send($subject, $receiver, $template, $payload);
    }
}
