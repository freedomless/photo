<?php

namespace App\EventSubscriber;

use App\Entity\Photo\ImageCurateAllowance;
use App\Entity\Photo\Post;
use App\Entity\User\User;
use App\EntityManager\PostManager;
use App\Event\Notification\CurateLimitLeftEvent;
use App\Event\Notification\CurateLimitReachedEvent;
use App\Helper\DispatcherTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Workflow\Event\GuardEvent;

/**
 * Class ToCurateWorkflowSubscriber
 * @package App\EventSubscriber
 */
class ToCurateWorkflowGuardSubscriber implements EventSubscriberInterface
{
    use DispatcherTrait;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var PostManager
     */
    private PostManager $postManager;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * ToCurateWorkflowGuardSubscriber constructor.
     * @param EntityManagerInterface $em
     * @param PostManager $postManager
     * @param SessionInterface $session
     */
    public function __construct(EntityManagerInterface $em, PostManager $postManager, SessionInterface $session)
    {
        $this->em = $em;
        $this->postManager = $postManager;
        $this->session = $session;
    }

    /**
     * @param GuardEvent $event
     *
     * @return int|void
     */
    public function onWorkflowPostPublishingGuardToCurate(GuardEvent $event)
    {
        /**
         * @var Post $post
         */
        $post = $event->getSubject();

        /**
         * @var User $user
         */
        $user = $post->getUser();

        $numberOfImages = $post->getType() > 1 ? count($post->getChildren()) : 1;

        $allowance = $this->em->getRepository(ImageCurateAllowance::class)->findOneBy(['role' => static::getHighestRole($user->getRoles())]);

        if ($allowance === null) {
            return;
        }

        if ($allowance->getIsActive() === false) {
            return;
        }

        $initial = $user->getCurateLimitStartDate() ?? $user->getCreatedAt();

        //Get difference in days date
        $dateFromStartDate = (new DateTime())->diff($initial);
        $daysFromStartDate = $dateFromStartDate->days;

        $daysPastInCurrentPeriod = $daysFromStartDate % $allowance->getSpan();

        $dateForCurrentPeriodStart = (new DateTime("-$daysPastInCurrentPeriod days"));
        $dateForCurrentPeriodStart->modify("- $dateFromStartDate->h hours");
        $dateForCurrentPeriodStart->modify("- $dateFromStartDate->i minutes");
        $dateForCurrentPeriodStart->modify("- $dateFromStartDate->s seconds");

        $posts = $this->postManager->repository->getPostsByIntervalAndForCurate($user, $dateForCurrentPeriodStart);

        $posts = $posts['total'] ?? 0;

        $leftSlots = $allowance->getQuantity() - $posts - $numberOfImages;

        $color = 'red';

        switch ($leftSlots) {
            case 1:
            case 3:

                $message = 'You image was sent to our curators! Will try to respond as soon as possible. ' . (is_int($leftSlots) ? 'Your have ' . $leftSlots . ' image left for curate.' : '');

                $this->session->getFlashBag()->add('toast', json_encode(compact('message')));

                $this->dispatch(new CurateLimitLeftEvent(null, $user, ['left' => $leftSlots]));

                break;

            case 0:

                $message = 'You image was sent to our curators! Will try to respond as soon as possible.';

                $this->session->getFlashBag()->add('toast', json_encode(compact('message')));

                $message = 'You have reached your curate limit!';

                $this->session->getFlashBag()->add('toast', json_encode(compact('message', 'color')));

                $this->dispatch(new CurateLimitReachedEvent(null, $user, ['left' => $allowance->getSpan()]));

                $user->setCurateAllowanceLimitReached(true);

                break;

            default:

                if ($leftSlots < 0) {

                    $event->setBlocked(true);

                    $message = 'Not enough curator slots available!';

                    $this->session->getFlashBag()->add('toast', json_encode(compact('message', 'color')));

                    if ($numberOfImages - abs($leftSlots) < 0) {
                        $user->setCurateAllowanceLimitReached(true);
                    }

                } else {

                    $message = 'You image was sent to our curators! Will try to respond as soon as possible. ' . (is_int($leftSlots) ? 'Your have ' . $leftSlots . ' image left for curate.' : '');

                    $this->session->getFlashBag()->add('toast', json_encode(compact('message')));
                }

                break;
        }

        $this->em->flush();
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.post_publishing.guard.to_curate' => [['onWorkflowPostPublishingGuardToCurate', 100]],
        ];
    }

    /**
     * @param $roles
     * @return bool|string
     */
    public static function getHighestRole($roles)
    {

        if (in_array('ROLE_SUPER_ADMIN', $roles, true)) {
            return 'ROLE_SUPER_ADMIN';
        }

        if (in_array('ROLE_ADMIN', $roles, true)) {
            return 'ROLE_ADMIN';
        }

        if (in_array('ROLE_CURATOR', $roles, true)) {
            return 'ROLE_CURATOR';
        }

        if (in_array('ROLE_MEMBER', $roles, true)) {
            return 'ROLE_MEMBER';
        }

        if (in_array('ROLE_USER', $roles, true)) {
            return 'ROLE_USER';
        }

        return false;
    }
}
