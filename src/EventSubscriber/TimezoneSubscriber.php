<?php

namespace App\EventSubscriber;

use GeoIp2\Database\Reader;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Intl\Timezones;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Twig\Extension\CoreExtension;

class TimezoneSubscriber implements EventSubscriberInterface
{

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;

    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * @var KernelInterface
     */
    private KernelInterface $kernel;

    public function __construct(Environment $twig,
                                KernelInterface $kernel,
                                TokenStorageInterface $storage)
    {

        $this->storage = $storage;
        $this->twig = $twig;
        $this->kernel = $kernel;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $country = null;

        try {
            $reader = new Reader($this->kernel->getCacheDir() . '/GeoLite2-Country.mmdb');
            $country = $reader->country($event->getRequest()->getClientIp())->country->isoCode;
        } catch (\Throwable $e) {

            try {
                $user = $this->storage->getToken();

                if ($user) {
                    $user = $user->getUser();
                    $country = $user->getProfile()->getAddress()->getCountry();
                }
            } catch (\Throwable $e) {

            }

        }

        if ($country) {

            $timezone = Timezones::forCountryCode($country)[0];

            $core = $this->twig->getExtension(CoreExtension::class);
            $core->setTimezone($timezone);
        }

    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.controller' => 'onKernelController',
        ];
    }
}
