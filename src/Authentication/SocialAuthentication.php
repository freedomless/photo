<?php
/**
 * Created by PhpStorm.
 * User: dtanev
 * DateCreated: 2019-01-23
 * Time: 00:17
 */

namespace App\Authentication;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SocialAuthenticationService
 * @package App\Service\Authentication
 */
class SocialAuthentication
{
    /**
     *
     */
    private const SCOPES = [
        'facebook' => ['public_profile', 'email'],
        'google'   => [
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
        ],
    ];

    /**
     * @var ClientRegistry
     */
    private ClientRegistry $clientRegistry;

    /**
     * SocialLoginService constructor.
     * @param ClientRegistry $clientRegistry
     */
    public function __construct(ClientRegistry $clientRegistry)
    {
        $this->clientRegistry = $clientRegistry;
    }

    /**
     * @param $network
     * @return RedirectResponse
     */
    public function authenticate($network)
    {
        $client = $this->clientRegistry->getClient($network);

        return $client->redirect(self::SCOPES[$network], []);
    }

    /**
     * Link to this controller to start the "connect" process
     *
     * @param string $network
     * @return RedirectResponse
     */
    public function connect(string $network): RedirectResponse
    {
        $client = $this->clientRegistry->getClient($network);

        return $client->redirect(self::SCOPES[$network], []);
    }
}
