<?php
/**
 * Created by PhpStorm.
 * User: dtanev
 * DateCreated: 2019-01-23
 * Time: 00:12
 */

namespace App\Authentication;

use App\Entity\User\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AuthenticationService
 * @package App\Service\Authentication
 */
class BaseAuthentication
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;

    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    /**
     * @var Request
     */
    private ?Request $request;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $dispatcher;

    /**
     * AuthenticationService constructor.
     * @param TokenStorageInterface $storage
     * @param SessionInterface $session
     * @param RequestStack $request
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        TokenStorageInterface $storage,
        SessionInterface $session,
        RequestStack $request,
        EventDispatcherInterface $dispatcher
    )
    {
        $this->storage = $storage;
        $this->session = $session;
        $this->request = $request->getCurrentRequest();
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param $user
     * @param null $credentials
     */
    public function authenticate(User $user, $credentials = null)
    {
        $token = new UsernamePasswordToken($user, $credentials, 'main', $user->getRoles());
        $this->storage->setToken($token);

        $this->session->set('_security_main', serialize($token));

        // Fire the login event manually
        $event = new InteractiveLoginEvent($this->request, $token);

        $this->dispatcher->dispatch($event);
    }
}
