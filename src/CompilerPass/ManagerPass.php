<?php

namespace App\CompilerPass;

use App\EntityManager\BaseManager;
use App\Repository\BaseRepository;
use phpDocumentor\Reflection\DocBlock\Description;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class ManagerPass implements CompilerPassInterface
{

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        $this->defineManagersAndRepositories($container);

    }

    /**
     * @inheritDoc
     */
    private function defineManagersAndRepositories(ContainerBuilder $container)
    {

        $entities = [];

        $projectDir = $container->getParameter('kernel.project_dir') . '/src/Entity/';

        $this->getEntities($entities, $projectDir ?? '', $projectDir ?? '', $projectDir ?? '');

        foreach ($entities as $entity) {

            $exploded = explode('\\', $entity);

            $id = 'app.manager.' . Container::underscore(end($exploded));

            $managerClass = 'App\EntityManager\\' . end($exploded) . 'Manager';
            $entityClass = 'App\Entity\\' . $entity;

            if (!class_exists($managerClass)) {
                $childDefinition = new ChildDefinition(BaseManager::class);
                $childDefinition->setArgument('$entity', $entityClass);
                $container->setDefinition($id, $childDefinition);
                $container->registerAliasForArgument($id, BaseManager::class, lcfirst(end($exploded)) . 'Manager');
            } else if (in_array(BaseManager::class, class_parents($managerClass))) {

                $definition = $container->getDefinition($managerClass);
                $definition->setArgument('$entity', $entityClass);

            }

            $id = 'app.repository.' . Container::underscore(end($exploded));

            if (class_exists('App\Repository\\' . end($exploded) . 'Repository')) {

                $definition = new Definition('App\Repository\\' . end($exploded) . 'Repository');
                $definition->setPublic(true);
                $definition->setAutowired(true);
                $definition->setAutoconfigured(true);
                $container->setDefinition($id, $definition);
                $container->registerAliasForArgument('App\Repository\\' . end($exploded) . 'Repository', 'App\Repository\\' . end($exploded) . 'Repository', lcfirst(end($exploded)) . 'Repository');
                continue;
            }

            $definition = new Definition(BaseRepository::class);
            $definition->setArgument('$entityClass', $entityClass);
            $definition->setPublic(true);
            $definition->setAutowired(true);
            $definition->setAutoconfigured(true);

            $container->setDefinition($id, $definition);
            $container->registerAliasForArgument($id, BaseRepository::class, lcfirst(end($exploded)) . 'Repository');

        }

    }

    /**
     * @param $entities
     * @param $baseDir
     * @param string $dir
     * @param string $current
     * @return array
     */
    private function getEntities(&$entities, $baseDir, $dir = '', $current = ''): array
    {

        $dir = scandir($dir);

        foreach ($dir as $file) {

            if (is_dir($baseDir . $file) && $file !== '.' && $file !== '..') {
                $this->getEntities($entities, $baseDir, $baseDir . $file, $baseDir . $file);
            }

            $regex = preg_match_all('/\.php/', $file);

            $class = str_replace('.php', '', $file);

            if ($regex && $class !== 'EntityInterface' && $class !== 'BaseEntity') {
                $tmp = explode('../Entity', $current);

                $namespace = explode('/Entity/', $current);

                $entities[] = (isset($namespace[1]) ? $namespace[1] . '\\' : '') . str_replace('/', '', isset($tmp[1]) ? $tmp[1] . '\\' . $class : $class);
            }
        }

        return $entities;
    }
}
