<?php

namespace ContainerNJr0G7c;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getPost9Service extends App_KernelProdContainer
{
    /*
     * Gets the private '.errored..service_locator.lrO_S2F.App\Entity\Photo\Post' shared service.
     *
     * @return \App\Entity\Photo\Post
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.lrO_S2F": it references class "App\\Entity\\Photo\\Post" but no such service exists.');
    }
}
