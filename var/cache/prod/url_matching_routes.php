<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/curator/photo-of-the-day' => [[['_route' => 'admin_manage_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::index'], null, null, null, false, false, null]],
        '/curator/photo-of-the-day/switch' => [[['_route' => 'admin_switch_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::switch'], null, null, null, false, false, null]],
        '/curator/series' => [[['_route' => 'admin_series_create', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::create'], null, null, null, false, false, null]],
        '/admin/dashboard' => [[['_route' => 'admin_dashboard', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\DashboardController::dashboardAction'], null, null, null, false, false, null]],
        '/admin/email-tester' => [[['_route' => 'admin_email_tester', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::index'], null, null, null, false, false, null]],
        '/admin/email-tester/photo' => [[['_route' => 'admin_email_tester_photo', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testPhoto'], null, null, null, false, false, null]],
        '/admin/email-tester/series' => [[['_route' => 'admin_email_tester_series', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testSeries'], null, null, null, false, false, null]],
        '/admin/email-tester/selection-series' => [[['_route' => 'admin_email_tester_selection_series', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testSelectionSeries'], null, null, null, false, false, null]],
        '/admin/email-tester/potd' => [[['_route' => 'admin_email_tester_potd', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testPhotoOfTheDay'], null, null, null, false, false, null]],
        '/admin/poll' => [[['_route' => 'poll_admin_list', '_controller' => 'App\\Controller\\Admin\\PollController::list'], null, null, null, false, false, null]],
        '/admin/poll/new' => [[['_route' => 'poll_admin_new', '_controller' => 'App\\Controller\\Admin\\PollController::new'], null, null, null, false, false, null]],
        '/curator/selection' => [[['_route' => 'admin_series_selection_list', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::list'], null, ['GET' => 0], null, false, false, null]],
        '/api/avatar' => [[['_route' => 'api_update_avatar', '_controller' => 'App\\Controller\\Api\\AccountController::avatar'], null, null, null, false, false, null]],
        '/api/cover' => [[['_route' => 'api_update_cover', '_controller' => 'App\\Controller\\Api\\AccountController::cover'], null, null, null, false, false, null]],
        '/api/accept-cookies' => [[['_route' => 'api_accept_cookies', '_controller' => 'App\\Controller\\Api\\AccountController::acceptCookies'], null, null, null, false, false, null]],
        '/api/cart' => [[['_route' => 'api_cart_create', '_controller' => 'App\\Controller\\Api\\CartController::create'], null, ['POST' => 0], null, false, false, null]],
        '/api/comments' => [[['_route' => 'app_api_comment_create', '_controller' => 'App\\Controller\\Api\\CommentController::create'], null, ['POST' => 0], null, false, false, null]],
        '/api/notifiations/total' => [[['_route' => 'api_notifications_total', '_controller' => 'App\\Controller\\Api\\NotificationController::getCount'], null, null, null, false, false, null]],
        '/api/notifiations/makr-as-seen' => [[['_route' => 'api_notifications_seen', '_controller' => 'App\\Controller\\Api\\NotificationController::markAllAsSeen'], null, ['POST' => 0], null, false, false, null]],
        '/api/order' => [[['_route' => 'api_order_create', '_controller' => 'App\\Controller\\Api\\PaymentController::order'], null, null, null, false, false, null]],
        '/api/poll/vote' => [[['_route' => 'api_poll_vote', '_controller' => 'App\\Controller\\Api\\PollController::vote'], null, null, null, false, false, null]],
        '/api/posts/follow-favorite' => [[['_route' => 'api_photos_follow_favorite', '_controller' => 'App\\Controller\\Api\\PostController::getFollowFavorite'], null, null, null, false, false, null]],
        '/api/replies' => [[['_route' => 'api_replies_create', '_controller' => 'App\\Controller\\Api\\ReplyController::create'], null, ['POST' => 0], null, false, false, null]],
        '/profile/edit' => [[['_route' => 'web_profile_edit', '_controller' => 'App\\Controller\\Web\\AccountController::update'], null, null, null, false, false, null]],
        '/payout-request' => [[['_route' => 'web_profile_payout', '_controller' => 'App\\Controller\\Web\\AccountController::payoutRequest'], null, null, null, false, false, null]],
        '/settings' => [[['_route' => 'web_settings', '_controller' => 'App\\Controller\\Web\\AccountController::settings'], null, null, null, false, false, null]],
        '/change-password' => [[['_route' => 'web_change_password', '_controller' => 'App\\Controller\\Web\\AccountController::changePassword'], null, null, null, false, false, null]],
        '/album' => [[['_route' => 'web_album_create', '_controller' => 'App\\Controller\\Web\\AlbumController::create'], null, null, null, false, false, null]],
        '/cart' => [[['_route' => 'web_cart_index', '_controller' => 'App\\Controller\\Web\\CartController::index'], null, null, null, false, false, null]],
        '/contacts' => [[['_route' => 'web_contacts', '_controller' => 'App\\Controller\\Web\\ContactController::index'], null, null, null, false, false, null]],
        '/forgotten-password' => [[['_route' => 'web_forgotten_password', '_controller' => 'App\\Controller\\Web\\ForgottenPasswordController::create'], null, null, null, false, false, null]],
        '/photos/awarded' => [[['_route' => 'web_gallery_awarded', 'page' => 'awarded', '_controller' => 'App\\Controller\\Web\\GalleryController::awarded'], null, null, null, false, false, null]],
        '/photos/series' => [[['_route' => 'web_gallery_series', '_controller' => 'App\\Controller\\Web\\GalleryController::series'], null, null, null, false, false, null]],
        '/latest' => [[['_route' => 'web_latest', 'page' => 'latest', '_controller' => 'App\\Controller\\Web\\HomeController::latest'], null, null, null, false, false, null]],
        '/' => [
            [['_route' => 'web_popular', 'page' => 'popular', '_controller' => 'App\\Controller\\Web\\HomeController::popular'], null, null, null, false, false, null],
            [['_route' => 'web_members_by_country', '_controller' => 'App\\Controller\\Web\\MemberController::byCountry'], null, null, null, false, false, null],
        ],
        '/series' => [[['_route' => 'web_series', '_controller' => 'App\\Controller\\Web\\HomeController::series'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'web_login', '_controller' => 'App\\Controller\\Web\\LoginController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'web_logout', '_controller' => 'App\\Controller\\Web\\LoginController::logout'], null, null, null, false, false, null]],
        '/messages' => [[['_route' => 'web_messages', '_controller' => 'App\\Controller\\Web\\MessageController::index'], null, null, null, false, false, null]],
        '/product-details' => [[['_route' => 'web_product_details', '_controller' => 'App\\Controller\\Web\\PagesController::media'], null, null, null, false, false, null]],
        '/privacy-policy' => [[['_route' => 'web_privacy_policy', '_controller' => 'App\\Controller\\Web\\PagesController::privacy'], null, null, null, false, false, null]],
        '/terms-of-service' => [[['_route' => 'web_tos', '_controller' => 'App\\Controller\\Web\\PagesController::tos'], null, null, null, false, false, null]],
        '/faq' => [[['_route' => 'web_faq', '_controller' => 'App\\Controller\\Web\\PagesController::faq'], null, null, null, false, false, null]],
        '/user-agreement' => [[['_route' => 'web_user_agreement', '_controller' => 'App\\Controller\\Web\\PagesController::userAgreement'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'web_register', '_controller' => 'App\\Controller\\Web\\RegisterController::register'], null, null, null, false, false, null]],
        '/resend-confirmation' => [[['_route' => 'web_resend_confirmation', '_controller' => 'App\\Controller\\Web\\RegisterController::resendConfirmation'], null, null, null, false, false, null]],
        '/search' => [[['_route' => 'web_search', '_controller' => 'App\\Controller\\Web\\SearchController::index'], null, null, null, false, false, null]],
        '/vote' => [[['_route' => 'web_vote', '_controller' => 'App\\Controller\\Web\\VoteController::index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], null, null, null, true, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/for(?'
                    .'|\\-sale/([^/]++)(?:/(single|series))?(*:50)'
                    .'|um/(?'
                        .'|topic/([^/]++)(?:/(\\d+))?(*:88)'
                        .'|opinion/([^/]++)/(?'
                            .'|delete(*:121)'
                            .'|update(*:135)'
                        .')'
                    .')'
                .')'
                .'|/curator/(?'
                    .'|p(?'
                        .'|hoto(?'
                            .'|\\-of\\-the\\-day/(?'
                                .'|add/([^/]++)(*:199)'
                                .'|delete/([^/]++)(*:222)'
                            .')'
                            .'|/([^/]++)(?'
                                .'|(*:243)'
                                .'|/(?'
                                    .'|next(*:259)'
                                    .'|p(?'
                                        .'|rev(*:274)'
                                        .'|ublished/move/([^/]++)/([^/]++)(*:313)'
                                    .')'
                                    .'|u(?'
                                        .'|pdate(*:331)'
                                        .'|nblock(*:345)'
                                    .')'
                                    .'|c(?'
                                        .'|urate/(0|1)(*:369)'
                                        .'|omment(*:383)'
                                    .')'
                                    .'|vote/([^/]++)(*:405)'
                                    .'|block(*:418)'
                                .')'
                            .')'
                        .')'
                        .'|ending(?:/([^/]++)(?:/([^/]++))?)?(*:463)'
                        .'|ublished(?:/([^/]++)(?:/([^/]++))?)?(*:507)'
                    .')'
                    .'|rejected(?:/([^/]++)(?:/([^/]++))?)?(*:552)'
                    .'|manage/series(?:/([^/]++)(?:/([^/]++))?)?(*:601)'
                    .'|se(?'
                        .'|ries/([^/]++)/(?'
                            .'|edit(*:635)'
                            .'|delete(*:649)'
                        .')'
                        .'|lection/(?'
                            .'|new/([^/]++)(*:681)'
                            .'|([^/]++)/(?'
                                .'|edit(*:705)'
                                .'|delete(*:719)'
                                .'|show(*:731)'
                                .'|hide(*:743)'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/a(?'
                    .'|dmin/p(?'
                        .'|roduct/approve/([^/]++)(*:792)'
                        .'|oll/([^/]++)/(?'
                            .'|choice/manage(*:829)'
                            .'|edit(*:841)'
                            .'|de(?'
                                .'|lete(*:858)'
                                .'|activate(*:874)'
                            .')'
                            .'|activate(*:891)'
                        .')'
                    .')'
                    .'|pi/(?'
                        .'|c(?'
                            .'|art/([^/]++)(?'
                                .'|(*:926)'
                            .')'
                            .'|omments/([^/]++)(?'
                                .'|(?:/([^/]++))?(*:968)'
                                .'|(*:976)'
                            .')'
                        .')'
                        .'|f(?'
                            .'|avorites/([^/]++)(?'
                                .'|(*:1010)'
                            .')'
                            .'|ollow/([^/]++)(?'
                                .'|(*:1037)'
                            .')'
                        .')'
                        .'|notification(?'
                            .'|/([^/]++)/hide(*:1077)'
                            .'|s(?'
                                .'|(?:/([^/]++))?(*:1104)'
                                .'|/([^/]++)(*:1122)'
                            .')'
                        .')'
                        .'|me(?'
                            .'|mbers(?'
                                .'|/fo(?'
                                    .'|unders(?:/([^/]++))?(*:1172)'
                                    .'|llow(?'
                                        .'|ing(?:/([^/]++))?(*:1205)'
                                        .'|ers(?:/([^/]++))?(*:1231)'
                                    .')'
                                .')'
                                .'|(?:/([^/]++))?(*:1256)'
                            .')'
                            .'|ssage(?'
                                .'|s(?'
                                    .'|(?:/([^/]++)(?:/([^/]++))?)?(*:1306)'
                                    .'|(?:/([^/]++))?(*:1329)'
                                    .'|/([^/]++)(*:1347)'
                                .')'
                                .'|/([^/]++)(?:/([^/]++))?(*:1380)'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/members/top(?:/([^/]++))?(*:1419)'
                .'|/([^/]++)/(?'
                    .'|answer(*:1447)'
                    .'|cancel(*:1462)'
                .')'
                .'|/a(?'
                    .'|pi/(?'
                        .'|p(?'
                            .'|oll/([^/]++)/hide(*:1504)'
                            .'|hoto(?'
                                .'|s/(?'
                                    .'|([^/]++)(*:1533)'
                                    .'|p(?'
                                        .'|ublished(?:/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?)?(*:1596)'
                                        .'|o(?'
                                            .'|rtfolio(?:/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?)?(*:1658)'
                                            .'|pular(?:/([^/]++)(?:/([^/]++))?)?(*:1700)'
                                        .')'
                                    .')'
                                    .'|latest(?:/([^/]++)(?:/([^/]++))?)?(*:1745)'
                                    .'|f(?'
                                        .'|ollowing(?:/([^/]++)(?:/([^/]++))?)?(*:1794)'
                                        .'|avorites(?:/([^/]++)(?:/([^/]++))?)?(*:1839)'
                                    .')'
                                    .'|category/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:1894)'
                                    .'|tag/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:1943)'
                                    .'|a(?'
                                        .'|warded(?:/([^/]++)(?:/([^/]++))?)?(*:1990)'
                                        .'|lbum/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:2040)'
                                    .')'
                                    .'|series(?:/([^/]++)(?:/([^/]++))?)?(*:2084)'
                                    .'|([^/]++)/info(*:2106)'
                                    .'|store(?:/([^/]++))?(*:2134)'
                                .')'
                                .'|/([^/]++)/add\\-view(*:2163)'
                            .')'
                            .'|rice/([^/]++)/([^/]++)/([^/]++)/([^/]++)(*:2213)'
                        .')'
                        .'|s(?'
                            .'|e(?'
                                .'|ries/([^/]++)(*:2244)'
                                .'|arch/(?'
                                    .'|([^/]++)(*:2269)'
                                    .'|users/([^/]++)(*:2292)'
                                .')'
                            .')'
                            .'|tore(?:/([^/]++)(?:/([^/]++))?)?(*:2335)'
                        .')'
                        .'|replies/([^/]++)(*:2361)'
                    .')'
                    .'|lbum/([^/]++)(?'
                        .'|(*:2387)'
                        .'|/(?'
                            .'|update(*:2406)'
                            .'|delete(*:2421)'
                        .')'
                    .')'
                .')'
                .'|/p(?'
                    .'|ro(?'
                        .'|file/([^/]++)(?'
                            .'|(?:/(single|series))?(*:2480)'
                            .'|/(?'
                                .'|p(?'
                                    .'|ortfolio(?:/(single|series))?(*:2526)'
                                    .'|rints\\-for\\-sale(?:/(single|series))?(*:2572)'
                                .')'
                                .'|series(*:2588)'
                                .'|f(?'
                                    .'|ollowe(?'
                                        .'|rs(*:2612)'
                                        .'|d(*:2622)'
                                    .')'
                                    .'|avorites(*:2640)'
                                .')'
                                .'|albums(?:/([^/]++))?(*:2670)'
                            .')'
                        .')'
                        .'|duct/([^/]++)/delete(*:2701)'
                    .')'
                    .'|hoto(?'
                        .'|s(?:/([^/]++))?(*:2733)'
                        .'|\\-of\\-the\\-day/([^/]++)(?:/([^/]++))?(*:2779)'
                        .'|/([^/]++)/(?'
                            .'|edit(*:2805)'
                            .'|series/cover(*:2826)'
                            .'|curate(*:2841)'
                            .'|delete(*:2856)'
                            .'|([^/]++)(?:/([^/]++))?(*:2887)'
                        .')'
                    .')'
                .')'
                .'|/orders(?:/([^/]++)(?:/([^/]++))?)?(*:2934)'
                .'|/for(?'
                    .'|gotten\\-password/([^/]++)/([^/]++)(*:2984)'
                    .'|um(?'
                        .'|(?:/([^/]++))?(*:3012)'
                        .'|/(?'
                            .'|([^/]++)/section(?:/([^/]++))?(*:3055)'
                            .'|topic/([^/]++)/update(*:3085)'
                        .')'
                    .')'
                .')'
                .'|/s(?'
                    .'|e(?'
                        .'|lection\\-series/([^/]++)(?:/([^/]++))?(*:3144)'
                        .'|ries/([^/]++)(?'
                            .'|(*:3169)'
                            .'|/view(*:3183)'
                        .')'
                    .')'
                    .'|old(?:/([^/]++))?(*:3211)'
                    .'|tore(?:/(single|series)(?:/([^/]++)(?:/([^/]++))?)?)?(*:3273)'
                .')'
                .'|/login/(?'
                    .'|([^/]++)(*:3301)'
                    .'|check/([^/]++)(*:3324)'
                .')'
                .'|/m(?'
                    .'|embers(?:/([^/]++)(?:/([^/]++))?)?(*:3373)'
                    .'|anage/p(?'
                        .'|hotos(?:/(single|series))?(*:3418)'
                        .'|roducts(?:/([^/]++))?(*:3448)'
                    .')'
                .')'
                .'|/upload(?:/(single|series|series-from-published))?(*:3509)'
                .'|/confirmation/([^/]++)(*:3540)'
                .'|/invoice/([^/]++)(*:3566)'
                .'|/t(?'
                    .'|ag/([^/]++)(*:3591)'
                    .'|opic/([^/]++)/(?'
                        .'|create(*:3623)'
                        .'|delete(*:3638)'
                    .')'
                .')'
                .'|/vote/([^/]++)(*:3663)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        50 => [[['_route' => 'web_photo_for_sale', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\ProductController::addToStore'], ['id', 'type'], null, null, false, true, null]],
        88 => [[['_route' => 'web_forum_section_opinion', 'page' => '1', '_controller' => 'App\\Controller\\Web\\OpinionController::index'], ['slug', 'page'], null, null, false, true, null]],
        121 => [[['_route' => 'web_forum_opinion_destroy', '_controller' => 'App\\Controller\\Web\\OpinionController::delete'], ['id'], null, null, false, false, null]],
        135 => [[['_route' => 'web_forum_opinion_update', '_controller' => 'App\\Controller\\Web\\OpinionController::update'], ['id'], null, null, false, false, null]],
        199 => [[['_route' => 'admin_create_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::create'], ['id'], null, null, false, true, null]],
        222 => [[['_route' => 'admin_delete_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::delete'], ['id'], null, null, false, true, null]],
        243 => [[['_route' => 'admin_photo_show', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::show'], ['id'], null, null, false, true, null]],
        259 => [[['_route' => 'admin_photo_show_next', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::next'], ['id'], null, null, false, false, null]],
        274 => [[['_route' => 'admin_photo_show_prev', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::previous'], ['id'], null, null, false, false, null]],
        313 => [[['_route' => 'admin_photo_published_move', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::move'], ['id', 'up', 'page'], ['POST' => 0], null, false, true, null]],
        331 => [[['_route' => 'admin_photo_update', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::update'], ['id'], null, null, false, false, null]],
        345 => [[['_route' => 'admin_curator_unblock_post', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::unblockPost'], ['id'], null, null, false, false, null]],
        369 => [[['_route' => 'admin_photo_curate', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::action'], ['id', 'action'], null, null, false, true, null]],
        383 => [[['_route' => 'admin_curator_comment', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::comment'], ['id'], ['POST' => 0], null, false, false, null]],
        405 => [[['_route' => 'admin_photo_vote', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::vote'], ['id', 'vote'], ['POST' => 0], null, false, true, null]],
        418 => [[['_route' => 'admin_curator_block_post', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::blockPost'], ['id'], null, null, false, false, null]],
        463 => [[['_route' => 'admin_photo_for_curate', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::pending'], ['page', 'limit'], null, null, false, true, null]],
        507 => [[['_route' => 'admin_photo_published', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::published'], ['page', 'limit'], null, null, false, true, null]],
        552 => [[['_route' => 'admin_photo_rejected', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::rejected'], ['page', 'limit'], null, null, false, true, null]],
        601 => [[['_route' => 'admin_series', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::index'], ['page', 'limit'], null, null, false, true, null]],
        635 => [[['_route' => 'admin_series_update', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::update'], ['id'], null, null, false, false, null]],
        649 => [[['_route' => 'admin_series_delete', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::delete'], ['id'], null, null, false, false, null]],
        681 => [[['_route' => 'admin_series_selection_new', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::add'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        705 => [[['_route' => 'admin_series_selection_edit', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        719 => [[['_route' => 'admin_series_selection_delete', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::delete'], ['id'], ['POST' => 0], null, false, false, null]],
        731 => [[['_route' => 'admin_series_selection_show', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::show'], ['id'], ['POST' => 0], null, false, false, null]],
        743 => [[['_route' => 'admin_series_selection_hide', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::hide'], ['id'], ['POST' => 0], null, false, false, null]],
        792 => [[['_route' => 'admin_approve_product', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\ProductController::approveProduct'], ['id'], null, null, false, true, null]],
        829 => [[['_route' => 'poll_admin_manage_choices', '_controller' => 'App\\Controller\\Admin\\PollController::manageChoices'], ['id'], null, null, false, false, null]],
        841 => [[['_route' => 'poll_admin_edit', '_controller' => 'App\\Controller\\Admin\\PollController::edit'], ['id'], null, null, false, false, null]],
        858 => [[['_route' => 'poll_admin_delete', '_controller' => 'App\\Controller\\Admin\\PollController::delete'], ['id'], null, null, false, false, null]],
        874 => [[['_route' => 'poll_admin_deactivate', '_controller' => 'App\\Controller\\Admin\\PollController::deactivate'], ['id'], null, null, false, false, null]],
        891 => [[['_route' => 'poll_admin_activate', '_controller' => 'App\\Controller\\Admin\\PollController::activate'], ['id'], null, null, false, false, null]],
        926 => [
            [['_route' => 'api_cart_update', '_controller' => 'App\\Controller\\Api\\CartController::update'], ['id'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_cart_delete', '_controller' => 'App\\Controller\\Api\\CartController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        968 => [[['_route' => 'api_comments_list', 'page' => '1', '_controller' => 'App\\Controller\\Api\\CommentController::index'], ['id', 'page'], ['GET' => 0], null, false, true, null]],
        976 => [[['_route' => 'app_api_comment_destroy', '_controller' => 'App\\Controller\\Api\\CommentController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1010 => [
            [['_route' => 'api_favorties_create', '_controller' => 'App\\Controller\\Api\\FavoriteController::create'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null],
            [['_route' => 'api_favroties_delete', '_controller' => 'App\\Controller\\Api\\FavoriteController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        1037 => [
            [['_route' => 'app_api_follow_create', '_controller' => 'App\\Controller\\Api\\FollowController::create'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null],
            [['_route' => 'app_api_follow_delete', '_controller' => 'App\\Controller\\Api\\FollowController::delete'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        1077 => [[['_route' => 'api_global_notification_hide', '_controller' => 'App\\Controller\\Api\\GlobalNotificationController::hideGlobalNotificationForSession'], ['notificationName'], null, null, false, false, null]],
        1104 => [[['_route' => 'api_notifications_index', 'page' => '1', '_controller' => 'App\\Controller\\Api\\NotificationController::index'], ['page'], ['GET' => 0], null, false, true, null]],
        1122 => [[['_route' => 'api_notifications_delete', '_controller' => 'App\\Controller\\Api\\NotificationController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1172 => [[['_route' => 'api_founders', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::founders'], ['page'], null, null, false, true, null]],
        1205 => [[['_route' => 'api_members_following', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::following'], ['page'], null, null, false, true, null]],
        1231 => [[['_route' => 'api_members_followed', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::followers'], ['page'], null, null, false, true, null]],
        1256 => [[['_route' => 'api_members_alphabetical', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::alphabetical'], ['page'], null, null, false, true, null]],
        1306 => [[['_route' => 'api_messages_index', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\MessageController::index'], ['page', 'limit'], ['GET' => 0], null, false, true, null]],
        1329 => [[['_route' => 'api_messages_create', 'id' => null, '_controller' => 'App\\Controller\\Api\\MessageController::create'], ['id'], ['POST' => 0], null, false, true, null]],
        1347 => [[['_route' => 'api_messages_delete', '_controller' => 'App\\Controller\\Api\\MessageController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1380 => [[['_route' => 'api_messages_show', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MessageController::show'], ['id', 'page'], ['GET' => 0], null, false, true, null]],
        1419 => [[['_route' => 'api_members', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::top'], ['page'], null, null, false, true, null]],
        1447 => [[['_route' => 'api_order_answer', 'gateway' => 'paypal', '_controller' => 'App\\Controller\\Api\\PaymentController::answer'], ['gateway'], null, null, false, false, null]],
        1462 => [[['_route' => 'api_order_cancel', 'gateway' => 'paypal', '_controller' => 'App\\Controller\\Api\\PaymentController::cancel'], ['gateway'], null, null, false, false, null]],
        1504 => [[['_route' => 'api_poll_hide_for_session', '_controller' => 'App\\Controller\\Api\\PollController::hidePollForSession'], ['id'], null, null, false, false, null]],
        1533 => [[['_route' => 'api_photos_show', '_controller' => 'App\\Controller\\Api\\PostController::show'], ['id'], null, null, false, true, null]],
        1596 => [[['_route' => 'api_photos_published', 'page' => '1', 'user' => null, 'series' => '0', '_controller' => 'App\\Controller\\Api\\PostController::published'], ['page', 'user', 'series'], null, null, false, true, null]],
        1658 => [[['_route' => 'api_photos_portfolio', 'page' => '1', 'series' => '0', 'user' => null, '_controller' => 'App\\Controller\\Api\\PostController::portfolio'], ['page', 'user', 'series'], null, null, false, true, null]],
        1700 => [[['_route' => 'api_photos_popular', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::popular'], ['page', 'limit'], null, null, false, true, null]],
        1745 => [[['_route' => 'api_photos_latest', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::latest'], ['page', 'limit'], null, null, false, true, null]],
        1794 => [[['_route' => 'api_photos_following', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::following'], ['page', 'limit'], null, null, false, true, null]],
        1839 => [[['_route' => 'api_photos_favorites', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::favorite'], ['page', 'limit'], null, null, false, true, null]],
        1894 => [[['_route' => 'api_photos_by_category', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::category'], ['id', 'page', 'limit'], null, null, false, true, null]],
        1943 => [[['_route' => 'api_photos_by_tag', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::tag'], ['tag', 'page', 'limit'], null, null, false, true, null]],
        1990 => [[['_route' => 'app_api_post_awarded', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::awarded'], ['page', 'limit'], null, null, false, true, null]],
        2040 => [[['_route' => 'api_photos_by_album', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::album'], ['id', 'page', 'limit'], null, null, false, true, null]],
        2084 => [[['_route' => 'api_photos_series_all', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::series'], ['page', 'limit'], null, null, false, true, null]],
        2106 => [[['_route' => 'api_photos_info', '_controller' => 'App\\Controller\\Api\\PostController::info'], ['id'], null, null, false, false, null]],
        2134 => [[['_route' => 'app_api_post_store', 'page' => 1, '_controller' => 'App\\Controller\\Api\\PostController::store'], ['page'], null, null, false, true, null]],
        2163 => [[['_route' => 'api_add_view', '_controller' => 'App\\Controller\\Api\\PostController::addView'], ['id'], null, null, false, false, null]],
        2213 => [[['_route' => 'api_product_price', '_controller' => 'App\\Controller\\Api\\StoreController::calculateProductPrice'], ['product', 'quantity', 'size', 'material'], null, null, false, true, null]],
        2244 => [[['_route' => 'api_series_photos', '_controller' => 'App\\Controller\\Api\\PostController::seriesPhotos'], ['id'], null, null, false, true, null]],
        2269 => [[['_route' => 'api_search', '_controller' => 'App\\Controller\\Api\\SearchController::index'], ['query'], ['GET' => 0], null, false, true, null]],
        2292 => [[['_route' => 'api_search_users', '_controller' => 'App\\Controller\\Api\\SearchController::users'], ['query'], ['GET' => 0], null, false, true, null]],
        2335 => [[['_route' => 'app_api_store_single', 'type' => 'single', 'page' => 1, '_controller' => 'App\\Controller\\Api\\StoreController::single'], ['type', 'page'], null, null, false, true, null]],
        2361 => [[['_route' => 'api_replies_delete', '_controller' => 'App\\Controller\\Api\\ReplyController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        2387 => [[['_route' => 'web_album_show', '_controller' => 'App\\Controller\\Web\\AlbumController::show'], ['id'], null, null, false, true, null]],
        2406 => [[['_route' => 'web_album_update', '_controller' => 'App\\Controller\\Web\\AlbumController::update'], ['id'], null, null, false, false, null]],
        2421 => [[['_route' => 'web_album_delete', '_controller' => 'App\\Controller\\Web\\AlbumController::destroy'], ['id'], null, null, false, false, null]],
        2480 => [[['_route' => 'web_profile', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::index'], ['slug', 'type'], null, null, false, true, null]],
        2526 => [[['_route' => 'web_profile_portfolio', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::portfolio'], ['slug', 'type'], null, null, false, true, null]],
        2572 => [[['_route' => 'web_profile_products', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::products'], ['slug', 'type'], null, null, false, true, null]],
        2588 => [[['_route' => 'web_profile_series', '_controller' => 'App\\Controller\\Web\\AccountController::series'], ['slug'], null, null, false, false, null]],
        2612 => [[['_route' => 'web_profile_followers', '_controller' => 'App\\Controller\\Web\\AccountController::followers'], ['slug'], null, null, false, false, null]],
        2622 => [[['_route' => 'web_profile_followed', '_controller' => 'App\\Controller\\Web\\AccountController::followed'], ['slug'], null, null, false, false, null]],
        2640 => [[['_route' => 'web_profile_favorites', '_controller' => 'App\\Controller\\Web\\AccountController::favorites'], ['slug'], null, null, false, false, null]],
        2670 => [[['_route' => 'web_albums', 'page' => '1', '_controller' => 'App\\Controller\\Web\\AlbumController::index'], ['slug', 'page'], null, null, false, true, null]],
        2701 => [[['_route' => 'web_product_delete', '_controller' => 'App\\Controller\\Web\\ProductController::delete'], ['id'], null, null, false, false, null]],
        2733 => [[['_route' => 'web_gallery', 'page' => 'latest', '_controller' => 'App\\Controller\\Web\\GalleryController::index'], ['page'], null, null, false, true, null]],
        2779 => [[['_route' => 'web_photo_of_the_day', 'date' => null, '_controller' => 'App\\Controller\\Web\\HomeController::photoOfTheDayHistory'], ['id', 'date'], null, null, false, true, null]],
        2805 => [[['_route' => 'web_photo_update', '_controller' => 'App\\Controller\\Web\\PostController::update'], ['id'], null, null, false, false, null]],
        2826 => [[['_route' => 'web_photo_set_cover', '_controller' => 'App\\Controller\\Web\\PostController::setCover'], ['id'], null, null, false, false, null]],
        2841 => [[['_route' => 'web_photo_for_curate', '_controller' => 'App\\Controller\\Web\\PostController::sendForCurate'], ['id'], null, null, false, false, null]],
        2856 => [[['_route' => 'web_photo_delete', '_controller' => 'App\\Controller\\Web\\PostController::destroy'], ['id'], null, null, false, false, null]],
        2887 => [[['_route' => 'web_photo_show', 'payload' => null, '_controller' => 'App\\Controller\\Web\\PostController::show'], ['page', 'id', 'payload'], null, null, false, true, null]],
        2934 => [[['_route' => 'web_orders', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Web\\AccountController::orders'], ['page', 'limit'], null, null, false, true, null]],
        2984 => [[['_route' => 'web_forgotten_password_reset', '_controller' => 'App\\Controller\\Web\\ForgottenPasswordController::reset'], ['email', 'token'], null, null, false, true, null]],
        3012 => [[['_route' => 'web_forum', 'page' => '1', '_controller' => 'App\\Controller\\Web\\SectionController::index'], ['page'], null, null, false, true, null]],
        3055 => [[['_route' => 'web_forum_section', 'page' => '1', '_controller' => 'App\\Controller\\Web\\SectionController::show'], ['slug', 'page'], null, null, false, true, null]],
        3085 => [[['_route' => 'web_forum_topic_update', '_controller' => 'App\\Controller\\Web\\TopicController::update'], ['slug'], null, null, false, false, null]],
        3144 => [[['_route' => 'series_selection_history', 'slug' => null, '_controller' => 'App\\Controller\\Web\\HomeController::selectionSeriesHistory'], ['id', 'slug'], ['GET' => 0], null, false, true, null]],
        3169 => [[['_route' => 'web_series_show_old', '_controller' => 'App\\Controller\\Web\\PostController::series'], ['id'], null, null, false, true, null]],
        3183 => [[['_route' => 'web_series_show', '_controller' => 'App\\Controller\\Web\\PostController::series'], ['id'], null, null, false, false, null]],
        3211 => [[['_route' => 'web_manage_sold', 'page' => '1', '_controller' => 'App\\Controller\\Web\\ProductController::sold'], ['page'], null, null, false, true, null]],
        3273 => [[['_route' => 'web_store', 'type' => 'single', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Web\\StoreController::index'], ['type', 'page', 'limit'], null, null, false, true, null]],
        3301 => [[['_route' => 'web_social_login', '_controller' => 'App\\Controller\\Web\\LoginController::social'], ['network'], null, null, false, true, null]],
        3324 => [[['_route' => 'web_social_login_auth', '_controller' => 'App\\Controller\\Web\\LoginController::social'], ['network'], null, null, false, true, null]],
        3373 => [[['_route' => 'web_members', 'page' => 'alphabetical', 'iso' => null, '_controller' => 'App\\Controller\\Web\\MemberController::index'], ['page', 'iso'], null, null, false, true, null]],
        3418 => [[['_route' => 'web_photos_manage', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\PostController::index'], ['type'], null, null, false, true, null]],
        3448 => [[['_route' => 'web_manage_products', 'page' => '1', '_controller' => 'App\\Controller\\Web\\ProductController::index'], ['page'], null, null, false, true, null]],
        3509 => [[['_route' => 'web_photo_create', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\PostController::create'], ['type'], null, null, false, true, null]],
        3540 => [[['_route' => 'web_confirmation', '_controller' => 'App\\Controller\\Web\\RegisterController::confirm'], ['token'], null, null, false, true, null]],
        3566 => [[['_route' => 'web_invoice', '_controller' => 'App\\Controller\\Web\\StoreController::invoice'], ['token'], null, null, false, true, null]],
        3591 => [[['_route' => 'web_photo_by_tag', '_controller' => 'App\\Controller\\Web\\TagController::show'], ['tag'], ['GET' => 0], null, false, true, null]],
        3623 => [[['_route' => 'web_forum_topic_create', '_controller' => 'App\\Controller\\Web\\TopicController::create'], ['slug'], null, null, false, false, null]],
        3638 => [[['_route' => 'web_forum_topic_delete', '_controller' => 'App\\Controller\\Web\\TopicController::destroy'], ['slug'], null, null, false, false, null]],
        3663 => [
            [['_route' => 'web_vote_action', '_controller' => 'App\\Controller\\Web\\VoteController::vote'], ['vote'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
