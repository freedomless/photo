<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/poll/edit.html.twig */
class __TwigTemplate_ce912b5d2835c36a48f2ea8ee2565cb06e44b5a4273f066f38422d2c3797067b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'header_left' => [$this, 'block_header_left'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/poll/edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "Edit Poll
";
    }

    // line 7
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <div class=\"col-12\">

        <div class=\"default-form-layout\">

            ";
        // line 13
        echo twig_include($this->env, $context, "admin/poll/_form.html.twig", ["action" => "Edit"]);
        // line 15
        echo "

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/poll/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 15,  67 => 13,  60 => 8,  56 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/poll/edit.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/poll/edit.html.twig");
    }
}
