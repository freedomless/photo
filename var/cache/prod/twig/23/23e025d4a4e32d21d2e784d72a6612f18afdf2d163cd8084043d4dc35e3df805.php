<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/template.html.twig */
class __TwigTemplate_62303981a6fd5694a54db5e94f5bf5d735531e7e4b8f891411c28d0107550ed0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "security/template.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    <div class=\"security-form\">

        <div class=\"container\">

            <div class=\"row\">

                <div class=\"col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1\">

                    ";
        // line 14
        echo "                    <div class=\"row\">

                        <div class=\"col-sm-8 offset-sm-2\">

                            <h1 class=\"security-header\">

                                ";
        // line 20
        $this->displayBlock('form_title', $context, $blocks);
        // line 21
        echo "
                            </h1>

                        </div>

                    </div>

                    ";
        // line 29
        echo "                    <div class=\"security-main basic-layout\">

                        ";
        // line 31
        $this->displayBlock('form_body', $context, $blocks);
        // line 32
        echo "
                    </div>


                    ";
        // line 37
        echo "                    <div class=\"security-footer text-center\">

                        ";
        // line 39
        $this->displayBlock('form_footer', $context, $blocks);
        // line 40
        echo "
                    </div>

                </div>

            </div>

        </div>

    </div>

";
    }

    // line 20
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 31
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 39
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "security/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 39,  122 => 31,  116 => 20,  101 => 40,  99 => 39,  95 => 37,  89 => 32,  87 => 31,  83 => 29,  74 => 21,  72 => 20,  64 => 14,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/template.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/template.html.twig");
    }
}
