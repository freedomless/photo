<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gallery/series.html.twig */
class __TwigTemplate_58e6ef627d2ab05bd10cfce6bf234e5de54b62180160647853eb199f853c6f99 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "gallery/series.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Gallery";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
  <div class=\"container photos sm-full\">

      <div class=\"row photos\">

          <div class=\"col-12\">

              ";
        // line 13
        $this->loadTemplate("gallery/nav.html.twig", "gallery/series.html.twig", 13)->display($context);
        // line 14
        echo "
              ";
        // line 15
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "query", [], "any", false, false, false, 15), "all", [], "method", false, false, false, 15);
        // line 16
        echo "
              ";
        // line 17
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 1, "categories" =>         // line 19
($context["categories"] ?? null), "filters" =>         // line 20
($context["filters"] ?? null), "page" => "gallery", "series" => twig_get_attribute($this->env, $this->source,         // line 22
($context["series"] ?? null), "items", [], "any", false, false, false, 22)]]);
        echo "

          </div>

      </div>

  </div>

";
    }

    public function getTemplateName()
    {
        return "gallery/series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 22,  79 => 20,  78 => 19,  77 => 17,  74 => 16,  72 => 15,  69 => 14,  67 => 13,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "gallery/series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/gallery/series.html.twig");
    }
}
