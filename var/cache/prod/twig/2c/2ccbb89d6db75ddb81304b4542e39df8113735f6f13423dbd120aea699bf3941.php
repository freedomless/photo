<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/index.html.twig */
class __TwigTemplate_a787861a503685c1e98e53fe7d5c26d03ab7233f2df204b0fbe3fe106da2956e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'row_class' => [$this, 'block_row_class'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["title"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "get", [0 => "type"], "method", false, false, false, 3), "single"))) ? ("Photo") : ("Series"));
        // line 1
        $this->parent = $this->loadTemplate("template.html.twig", "post/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo " Manager";
    }

    // line 7
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo " Manager";
    }

    // line 9
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create", ["type" => "series-from-published"]);
        echo "\"
       class=\"btn btn-blue\"
       title=\"Create series\">

        <i class=\"fa fa-plus\"></i> Create Series

    </a>

";
    }

    // line 21
    public function block_row_class($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "image-browse-container";
    }

    // line 23
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "

    ";
        // line 26
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["posts"] ?? null)), 0))) {
            // line 27
            echo "
        <div class=\"col-md-12\">

            <div class=\"row infinite-photos\">

                ";
            // line 32
            $this->loadTemplate("post/posts.html.twig", "post/index.html.twig", 32)->display($context);
            // line 33
            echo "
            </div>

            ";
            // line 36
            $this->loadTemplate("_partials/loader.html.twig", "post/index.html.twig", 36)->display($context);
            // line 37
            echo "
        </div>

    ";
        } else {
            // line 41
            echo "
        ";
            // line 42
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 42), "get", [0 => "page"], "method", false, false, false, 42), 1))) {
                // line 43
                echo "
            <div class=\"col-12\">

                <p class=\"text-muted\">You don't have any photos uploaded yet.</p>

            </div>

        ";
            } else {
                // line 51
                echo "
            <div class=\"col-12\">

                <p class=\"text-muted\">No photos available.</p>

            </div>

        ";
            }
            // line 59
            echo "
    ";
        }
        // line 61
        echo "


";
    }

    // line 66
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "
    <script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.infinite-photos')</script>

    ";
        // line 72
        $macros["__internal_39f9ea6bfaf698b3fccf3eb6ce6b8a25b7cbcdeaf22734e4e2344d93cbd6260c"] = $this->loadTemplate("_partials/warning.html.twig", "post/index.html.twig", 72)->unwrap();
        // line 73
        echo "
    ";
        // line 74
        echo twig_call_macro($macros["__internal_39f9ea6bfaf698b3fccf3eb6ce6b8a25b7cbcdeaf22734e4e2344d93cbd6260c"], "macro_warning", ["delete-warning-modal", "delete-warning-form", "delete-warning-btn", "Confirm!", "Are you sure you want delete this photo?"], 74, $context, $this->getSourceContext());
        echo "

    ";
        // line 76
        echo twig_call_macro($macros["__internal_39f9ea6bfaf698b3fccf3eb6ce6b8a25b7cbcdeaf22734e4e2344d93cbd6260c"], "macro_warning", ["curator-warning-modal", "curator-warning-form", "curator-warning-btn", "Confirm!", "Are you sure you want to send this photo to curator?"], 76, $context, $this->getSourceContext());
        echo "

";
    }

    public function getTemplateName()
    {
        return "post/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 76,  186 => 74,  183 => 73,  181 => 72,  174 => 68,  171 => 67,  167 => 66,  160 => 61,  156 => 59,  146 => 51,  136 => 43,  134 => 42,  131 => 41,  125 => 37,  123 => 36,  118 => 33,  116 => 32,  109 => 27,  107 => 26,  103 => 24,  99 => 23,  92 => 21,  79 => 11,  76 => 10,  72 => 9,  63 => 7,  54 => 5,  49 => 1,  47 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "post/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/index.html.twig");
    }
}
