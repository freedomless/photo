<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/rejected.html.twig */
class __TwigTemplate_12b87aed6ed325cdc2a395d2f815707803ee30413bf5f87bf7658cc0730285b6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/rejected.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Rejected";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"curate-posts basic-layout\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/rejected.html.twig", 9)->display($context);
        // line 10
        echo "
        <hr>

        ";
        // line 13
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/rejected.html.twig", 13)->display($context);
        // line 14
        echo "
        <hr>

        <div class=\"d-flex justify-content-center\">
            ";
        // line 18
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "
        </div>

        <div class=\"curate-posts\">
            <div class=\"container photos\">

                <div class=\"row\">

                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 27
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 34
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "isGreyscale", [], "any", false, false, false, 34), true))) {
                // line 35
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 39
            echo "
                                    ";
            // line 41
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 41))) {
                // line 42
                echo "                                        ";
                $context["comments"] = 0;
                // line 43
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 43));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 44
                    echo "                                            ";
                    $context["comments"] = (($context["comments"] ?? null) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 44)));
                    // line 45
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "
                                        ";
                // line 47
                if ((1 === twig_compare(($context["comments"] ?? null), 0))) {
                    // line 48
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 50
                    echo twig_escape_filter($this->env, ($context["comments"] ?? null), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 54
                echo "
                                    ";
            } else {
                // line 56
                echo "
                                        ";
                // line 57
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 57)), 0))) {
                    // line 58
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 60
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 60)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 64
                echo "
                                    ";
            }
            // line 66
            echo "                                    ";
            // line 67
            echo "
                                    ";
            // line 68
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 68), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 68), "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68)));
            // line 69
            echo "
                                    <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 70)]), "html", null, true);
            echo "\">

                                        <img src=\"";
            // line 72
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\" alt=\"\" width=\"auto\"
                                             style=\"max-height: 300px;max-width: 100%;\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    ";
            // line 83
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "sentForCurateDate", [], "any", false, false, false, 83), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-footer d-flex text-danger\">

                                    <strong class=\"flex-grow-1 text-danger\">Rejected:</strong>

                                    ";
            // line 91
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 91), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-text small text-center\">
                                    <span class=\"text-danger\">Rejected by:</span>
                                    &nbsp;
                                    ";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "publisher", [], "any", false, false, false, 98), "profile", [], "any", false, false, false, 98), "fullName", [], "any", false, false, false, 98), "html", null, true);
            echo "
                                </div>

                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "
                </div>

            </div>
        </div>


        <div class=\"d-flex justify-content-center\">

            ";
        // line 115
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/curator/rejected.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 115,  237 => 106,  223 => 98,  213 => 91,  202 => 83,  188 => 72,  183 => 70,  180 => 69,  178 => 68,  175 => 67,  173 => 66,  169 => 64,  162 => 60,  158 => 58,  156 => 57,  153 => 56,  149 => 54,  142 => 50,  138 => 48,  136 => 47,  133 => 46,  127 => 45,  124 => 44,  119 => 43,  116 => 42,  113 => 41,  110 => 39,  104 => 35,  102 => 34,  93 => 27,  89 => 26,  78 => 18,  72 => 14,  70 => 13,  65 => 10,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/rejected.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/rejected.html.twig");
    }
}
