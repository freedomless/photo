<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/TwigBundle/Exception/error.html.twig */
class __TwigTemplate_26c62f992dc72d1ca4a93a995f2dc2a1d8dca2d3a511fdccf1b9d43b8d9cb2b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'css' => [$this, 'block_css'],
            'title' => [$this, 'block_title'],
            'main_navigation' => [$this, 'block_main_navigation'],
            'body' => [$this, 'block_body'],
            'error_title' => [$this, 'block_error_title'],
            'error_message' => [$this, 'block_error_message'],
            'error_info' => [$this, 'block_error_info'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "bundles/TwigBundle/Exception/error.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <style>
        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .error-actions .btn {
            margin-right: 10px;
        }
    </style>
";
    }

    // line 21
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Error";
    }

    // line 23
    public function block_main_navigation($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 25
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    <div class=\"col-12\">

        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"error-template\">
                        <h1 class=\"h3\">Oops!</h1>
                        <h2 class=\"h4\">";
        // line 33
        $this->displayBlock('error_title', $context, $blocks);
        echo "</h2>

                        <hr>

                        <div class=\"error-details\">
                            <strong>";
        // line 38
        $this->displayBlock('error_message', $context, $blocks);
        echo "</strong>
                            <p>";
        // line 39
        $this->displayBlock('error_info', $context, $blocks);
        echo "</p>
                        </div>
                        <div class=\"error-actions\">
                            <a href=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_popular");
        echo "\" class=\"btn btn-primary btn-sm\">
                                <span class=\"fa fa-home\"></span> Take Me Home
                            </a>
                            <a href=\"";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 45), "headers", [], "any", false, false, false, 45), "get", [0 => "referer"], "method", false, false, false, 45), "html", null, true);
        echo "\" class=\"btn btn-dark btn-sm\">
                                <span class=\"fa fa-fast-backward\"></span> Go Back
                            </a>
                            <a href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
        echo "\" class=\"btn btn-default btn-sm\">
                                <span class=\"fa fa-envelope\"></span> Contact Support
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
";
    }

    // line 33
    public function block_error_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Error ";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? null), "html", null, true);
        echo "!";
    }

    // line 38
    public function block_error_message($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Sorry, an error has occured!";
    }

    // line 39
    public function block_error_info($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "bundles/TwigBundle/Exception/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 39,  155 => 38,  146 => 33,  131 => 48,  125 => 45,  119 => 42,  113 => 39,  109 => 38,  101 => 33,  92 => 26,  88 => 25,  82 => 23,  75 => 21,  56 => 4,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "bundles/TwigBundle/Exception/error.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/bundles/TwigBundle/Exception/error.html.twig");
    }
}
