<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vote/index.html.twig */
class __TwigTemplate_fa4bb71e9eca3b71417203995c2e6eb53413aed5715552f5076368b545661880 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["footer"] = false;
        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vote/index.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Curate";
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 5), "isVerified", [], "any", false, false, false, 5), false))) {
            // line 6
            echo "        ";
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "vote/index.html.twig", 6)->display($context);
            // line 7
            echo "    ";
        } else {
            // line 8
            echo "        <div class=\"curate\">
            ";
            // line 9
            if (($context["post"] ?? null)) {
                // line 10
                echo "                <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 10), "small", [], "any", false, false, false, 10), "html", null, true);
                echo "\" alt=\"\" class=\"photo-content\">
                <div class=\"actions shadow\">
                    ";
                // line 12
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "type", [], "any", false, false, false, 12), 1))) {
                    // line 13
                    echo "                        ";
                    echo twig_include($this->env, $context, "vote/yes_no/index.html.twig");
                    echo "
                    ";
                } else {
                    // line 15
                    echo "                        ";
                    echo twig_include($this->env, $context, "vote/rating/index.html.twig");
                    echo "
                    ";
                }
                // line 17
                echo "                </div>
            ";
            } else {
                // line 19
                echo "                <h3 style=\"position:absolute; top:50%;margin-top: -20px\" class=\"text-muted\">No more photos to curate</h3>
            ";
            }
            // line 21
            echo "        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "vote/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 21,  99 => 19,  95 => 17,  89 => 15,  83 => 13,  81 => 12,  75 => 10,  73 => 9,  70 => 8,  67 => 7,  64 => 6,  61 => 5,  57 => 4,  50 => 3,  45 => 2,  43 => 1,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "vote/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/vote/index.html.twig");
    }
}
