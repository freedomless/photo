<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_social.html.twig */
class __TwigTemplate_8ed93e7ae6e8f47ed623e151fc5b790aeb7936049d959790a958cde8043d3c89 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "Follow us on:
<a href=\"https://facebook.com/piart.community\" target=\"_blank\" title=\"Follow us on Facebook\">Facebook</a> |
<a href=\"https://www.pinterest.com/photoimaginart/pins/\" target=\"_blank\" title=\"Follow us on Pinterest\">Pinterest</a> |
<a href=\"https://twitter.com/photoimaginart\" target=\"_blank\" title=\"Follow us on Twitter\">Twitter</a> |
<a href=\"https://instagram.com/photo_imaginart\" target=\"_blank\" title=\"Follow us on Instagram\">Instagram</a>";
    }

    public function getTemplateName()
    {
        return "emails/_social.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/_social.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_social.html.twig");
    }
}
