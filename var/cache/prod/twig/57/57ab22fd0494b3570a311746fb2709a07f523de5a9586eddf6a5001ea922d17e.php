<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-votes.html.twig */
class __TwigTemplate_be5af396a204601da47a0c2d17567a4132166c538a5965b325f9bd9a698327f4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["votes"] ?? null)), 0))) {
            // line 2
            echo "    <div class=\"card mt-3 mb-3\">
        <div class=\"card-header\">
            <strong>Curator votes</strong>
        </div>
        <div class=\"card-body\">
            <ul class=\"list-group p-0\">
                ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["votes"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["vote"]) {
                // line 9
                echo "                    <li class=\"list-group-item d-flex\">
                        <strong class=\"flex-grow-1 bold\"
                                data-toggle=\"tooltip\"
                                data-placement=\"top\" title=\"";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vote"], "voter", [], "any", false, false, false, 12), "username", [], "any", false, false, false, 12), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vote"], "voter", [], "any", false, false, false, 12), "profile", [], "any", false, false, false, 12), "fullName", [], "any", false, false, false, 12), "html", null, true);
                echo "</strong>

                        <span class=\"badge badge-";
                // line 14
                echo ((twig_get_attribute($this->env, $this->source, $context["vote"], "vote", [], "any", false, false, false, 14)) ? ("success") : ("danger"));
                echo " mb-1\">";
                echo ((twig_get_attribute($this->env, $this->source, $context["vote"], "vote", [], "any", false, false, false, 14)) ? ("Yes") : ("No"));
                echo "</span>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vote'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            </ul>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-votes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 17,  63 => 14,  56 => 12,  51 => 9,  47 => 8,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/curator-votes.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-votes.html.twig");
    }
}
