<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* album/album.html.twig */
class __TwigTemplate_d180b038e280b8f12170c6aa9f9846f7da206454ba61782221ba49e867681b13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["albums"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 2
            echo "
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_show", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 9)]), "html", null, true);
            echo "\" class=\"text-decoration-none\">
                    ";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 10)) {
                // line 11
                echo "                        <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 11), "image", [], "any", false, false, false, 11), "thumbnail", [], "any", false, false, false, 11), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 11), "title", [], "any", false, false, false, 11), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 13
                echo "                        <div class=\"d-flex flex-column justify-content-center align-items-center p-5 \">
                            <i class=\"fas fa-image\"></i>
                        </div>
                    ";
            }
            // line 17
            echo "                </a>

            </div>

            <div class=\"image-thumb-metadata\">

                <p class=\"text-center\">
                    ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["album"], "name", [], "any", false, false, false, 24), "html", null, true);
            echo "
                </p>

                ";
            // line 28
            echo "                ";
            if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 28) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "user", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28))))) {
                // line 29
                echo "                    <hr>

                    <div class=\"actions mb-1\">
                        <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_update", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 32)]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Edit Album\"
                           class=\"btn btn-default\">
                            <i class=\"fa fa-edit\"></i>
                        </a>

                        <button type=\"button\" data-url=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\"
                                data-toggle=\"modal\" data-target=\"#delete-confirm\"
                                title=\"Delete Album\"
                                class=\"btn btn-link open-confirm\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>

                        ";
                // line 47
                echo "                        <div class=\"modal fade mt-5\" id=\"delete-confirm\" tabindex=\"-1\"
                             role=\"dialog\" aria-hidden=\"true\">
                            <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                aria-label=\"Close\">
                                            <span aria-hidden=\"true\">&times;</span>
                                        </button>
                                    </div>
                                    <div class=\"modal-body\">
                                        Are you sure you want to delete this Album?
                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-secondary\"
                                                data-dismiss=\"modal\">No
                                        </button>

                                        <form action=\"#\"
                                              method=\"post\"
                                              class=\"d-inline-block\">
                                            <button class=\"btn btn-info\">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                ";
            }
            // line 78
            echo "
            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "album/album.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 78,  113 => 47,  103 => 39,  93 => 32,  88 => 29,  85 => 28,  79 => 24,  70 => 17,  64 => 13,  56 => 11,  54 => 10,  50 => 9,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "album/album.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/album/album.html.twig");
    }
}
