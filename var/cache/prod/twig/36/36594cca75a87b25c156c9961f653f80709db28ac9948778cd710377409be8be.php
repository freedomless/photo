<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/history.html.twig */
class __TwigTemplate_6499450d8991d0d357033701d0097a4d7f36e9312856a273954012eb03e4343a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/selection/history.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        echo $this->env->getRuntime('App\Twig\PollRuntime')->renderPoll($this->env);
        echo "

    <div class=\"container photos sm-full photos-infinite\">

        ";
        // line 11
        $this->loadTemplate("home/selection_series.html.twig", "admin/selection/history.html.twig", 11)->display($context);
        // line 12
        echo "
        ";
        // line 13
        $this->loadTemplate("home/nav.html.twig", "admin/selection/history.html.twig", 13)->display($context);
        // line 14
        echo "
        ";
        // line 15
        $context["nude"] = (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 15) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 15), "settings", [], "any", false, false, false, 15), "showNudes", [], "any", false, false, false, 15));
        // line 16
        echo "
        ";
        // line 17
        $context["auth"] = ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 17)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17)) : (null));
        // line 18
        echo "
        ";
        // line 19
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>         // line 21
($context["nude"] ?? null), "auth" =>         // line 22
($context["auth"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" =>         // line 26
($context["page"] ?? null)]]);
        echo "

    </div>

    ";
        // line 30
        echo twig_include($this->env, $context, "_partials/_social_icons.html.twig");
        echo "

";
    }

    // line 34
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    <script>
        \$(document).ready(function () {
            \$('.share-btn').click(function (e) {
                e.preventDefault();
                window.open(\$(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + (\$(window).height() / 2 - 275) + ', left=' + (\$(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                return false;
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "admin/selection/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 35,  106 => 34,  99 => 30,  92 => 26,  91 => 22,  90 => 21,  89 => 19,  86 => 18,  84 => 17,  81 => 16,  79 => 15,  76 => 14,  74 => 13,  71 => 12,  69 => 11,  62 => 7,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/selection/history.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/history.html.twig");
    }
}
