<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vote/yes_no/index.html.twig */
class __TwigTemplate_316a0a3290926524779c33d79b3ca1c7d5d51c911ffa88b2337a9d1ac891c2a4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => 1]);
        echo "\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-success action shadow action  action-publish\"><i class=\"fas fa-check\"></i>
    </button>
</form>
<div class=\"d-inline-block\">
    ";
        // line 6
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Favorite", ["rendering" => "client_side", "props" => ["post" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 6), "favorite" => ($context["favorite"] ?? null)]]);
        echo "
</div>
<form action=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => 0]);
        echo "\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-danger action shadow  action action-reject\"><i class=\"fas fa-times\"></i>
    </button>
</form>
";
    }

    public function getTemplateName()
    {
        return "vote/yes_no/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "vote/yes_no/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/vote/yes_no/index.html.twig");
    }
}
