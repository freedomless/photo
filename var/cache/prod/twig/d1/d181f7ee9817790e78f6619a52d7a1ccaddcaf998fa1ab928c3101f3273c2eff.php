<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/sold.html.twig */
class __TwigTemplate_f3a49b0189c09bdd2617751f5cbd461863cc3dbdb6f453c3aa1622f2aab3f6a3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "product/sold.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "

<div class=\"container basic-layout\">
    <h3 class=\"mb-5\">Sold Picture</h3>
    <table class=\"table\">
        <thead>
          <tr>
              <th>Picture</th>
              <th>Media</th>
              <th>Size</th>
              <th>Commission</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
            ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 24
            echo "            <tr>
                <td>
                    ";
            // line 26
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 26), "post", [], "any", false, false, false, 26), "type", [], "any", false, false, false, 26), 1))) {
                // line 27
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 27), "post", [], "any", false, false, false, 27), "children", [], "any", false, false, false, 27));
                foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                    // line 28
                    echo "                            <a class=\"text-decoration-none\"
                               href=\"";
                    // line 29
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 29), "type" => "series"]), "html", null, true);
                    echo "\">
                                <img src=\"";
                    // line 30
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 30), "thumbnail", [], "any", false, false, false, 30), "html", null, true);
                    echo "\" alt=\"Item\" class=\"rounded\" width=\"100px\"
                                     height=\"100px\" style=\"object-fit: cover\">
                            </a>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "
                    ";
            } else {
                // line 36
                echo "                        <a class=\"text-decoration-none\"
                           href=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 37), "post", [], "any", false, false, false, 37), "id", [], "any", false, false, false, 37)]), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 38), "post", [], "any", false, false, false, 38), "image", [], "any", false, false, false, 38), "thumbnail", [], "any", false, false, false, 38), "html", null, true);
                echo "\" alt=\"Item\" class=\"rounded\"
                                 width=\"100px\" height=\"100px\" style=\"object-fit: cover\"/>
                        </a>
                    ";
            }
            // line 42
            echo "
                </td>
                <td>
                    <div>
                        ";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 46), "width", [], "any", false, false, false, 46), "html", null, true);
            echo "x";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 46), "height", [], "any", false, false, false, 46), "html", null, true);
            echo "
                    </div>

                </td>
                <td>

                    <div>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "material", [], "any", false, false, false, 52), "name", [], "any", false, false, false, 52), "html", null, true);
            echo "</div>

                </td>
                <td>
                    ";
            // line 56
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "commission", [], "any", false, false, false, 56), "html", null, true);
            echo " &euro;
                </td>
                <td>
                    ";
            // line 59
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "commissionAddedToBalance", [], "any", false, false, false, 59), true))) ? ("<span class=\"text-success\">Completed</span>") : ("<span class=\"text-warning\">Pending</span>"));
            echo "
                </td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "        </tbody>
    </table>

    <div class=\"d-flex justify-content-center\">
        ";
        // line 67
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["items"] ?? null));
        echo "
    </div>
</div>


";
    }

    public function getTemplateName()
    {
        return "product/sold.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 67,  167 => 63,  157 => 59,  151 => 56,  144 => 52,  133 => 46,  127 => 42,  120 => 38,  116 => 37,  113 => 36,  109 => 34,  99 => 30,  95 => 29,  92 => 28,  87 => 27,  85 => 26,  81 => 24,  77 => 23,  60 => 8,  56 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product/sold.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/sold.html.twig");
    }
}
