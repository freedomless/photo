<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/filter.html.twig */
class __TwigTemplate_083cb1399632a868b67e2802adfe85944be0dc4165ea1b892f62555382dc82bc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["clearLink"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1));
        // line 2
        echo "
<div class=\"container\">

    <div class=\"default-form-layout\">

        ";
        // line 7
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 7), "query", [], "any", false, false, false, 7), "get", [0 => "filters", 1 => []], "method", false, false, false, 7);
        // line 8
        echo "
        <form>

            <div class=\"form row\">

                <div class=\"col-sm-12 col-md-3\">
                    <div class=\"form-group\">
                        <label for=\"username\">Filter by username</label>
                        <input class=\"form-control\" type=\"text\" id=\"username\"
                               placeholder=\"Filter by Username\"
                               value=\"";
        // line 18
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "username", [], "any", true, true, false, 18)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "username", [], "any", false, false, false, 18))) : ("")), "html", null, true);
        echo "\" name=\"filters[username]\">
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-3\">

                    <div class=\"form-group\">
                        <label for=\"name\">Filter by name</label>
                        <input class=\"form-control\" type=\"text\" id=\"name\"
                               placeholder=\"Filter by Name\"
                               value=\"";
        // line 28
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "name", [], "any", true, true, false, 28)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "name", [], "any", false, false, false, 28))) : ("")), "html", null, true);
        echo "\"
                               name=\"filters[name]\">
                    </div>

                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <button class=\"form-control btn btn-primary\">Filter</button>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, ($context["clearLink"] ?? null), "html", null, true);
        echo "\" class=\"btn btn-warning form-control text-white\">Clear</a>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_create");
        echo "\" class=\"btn btn-secondary form-control\">Create Series</a>
                    </div>
                </div>

            </div>

        </form>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/filter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 54,  94 => 46,  73 => 28,  60 => 18,  48 => 8,  46 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/filter.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/filter.html.twig");
    }
}
