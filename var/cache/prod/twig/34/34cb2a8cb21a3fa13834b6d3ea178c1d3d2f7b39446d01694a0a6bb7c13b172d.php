<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/list.html.twig */
class __TwigTemplate_58d8a97ac4756573e74d6427b877112c1e01208421603511b5ee5ab1423d16d0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/selection/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Manage Selection Series";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Manage Selection Series";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"col-12\">

        <div class=\"row\">
            ";
        // line 14
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["series"] ?? null)), 0))) {
            // line 15
            echo "
                ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["series"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 17
                echo "
                    <div data-id=\"";
                // line 18
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 18), "html", null, true);
                echo "\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">
                        <div class=\"card mb-4 text-center\">

                            ";
                // line 21
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 21), 1))) {
                    // line 22
                    echo "                                <div class=\"alert-success position-absolute\" style=\"top: 0; width: 100%\">
                                    Shown on Front page
                                </div>
                            ";
                }
                // line 26
                echo "
                            <img src=\"";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "cover", [], "any", false, false, false, 27), "html", null, true);
                echo "\" class=\"card-img-top\" alt=\"...\"
                                 style=\"object-fit: cover; width: 100%; height: 200px\">


                            <div class=\"card-body\">

                                <p class=\"text-info\">
                                    ";
                // line 34
                echo $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->truncateText($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 34), 100);
                echo "
                                </p>

                                <p class=\"text-muted small\">Added: ";
                // line 37
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "createdAt", [], "any", false, false, false, 37), "d/M/Y H:i:s"), "html", null, true);
                echo "</p>

                                <div>

                                    ";
                // line 41
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 41), 1))) {
                    // line 42
                    echo "                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to show this on Front Page?\"
                                                data-url=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_show", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 45)]), "html", null, true);
                    echo "\"
                                                title=\"Show Series\"
                                                class=\"btn btn-sm btn-success text-white confirm-action\">
                                            Show
                                        </button>
                                    ";
                }
                // line 51
                echo "
                                    ";
                // line 52
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 52), 1))) {
                    // line 53
                    echo "                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to hide this from Front Page?\"
                                                data-url=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_hide", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 56)]), "html", null, true);
                    echo "\"
                                                title=\"Hide Series\"
                                                class=\"btn btn-sm btn-light text-white confirm-action\">
                                            Hide
                                        </button>
                                    ";
                }
                // line 62
                echo "
                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to edit this Selection Series?\"
                                            data-url=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 66)]), "html", null, true);
                echo "\"
                                            title=\"Edit Selection Series\"
                                            class=\"btn btn-sm btn-warning text-white confirm-action\">
                                        <i class=\"fa fa-edit\"></i>
                                    </button>

                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to delete this Selection Series?\"
                                            data-url=\"";
                // line 75
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 75)]), "html", null, true);
                echo "\"
                                            title=\"Delete Selection Series\"
                                            class=\"btn btn-sm btn-danger text-white confirm-action\">
                                        <i class=\"fa fa-times\"></i>
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "
                <div class=\"modal fade mt-5\" id=\"action-confirm\" tabindex=\"-1\"
                     role=\"dialog\" aria-hidden=\"true\">
                    <div class=\"modal-dialog\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                        aria-label=\"Close\">
                                    <span aria-hidden=\"true\">&times;</span>
                                </button>
                            </div>
                            <div class=\"modal-body\"></div>
                            <div class=\"modal-footer\">
                                <button type=\"button\" class=\"btn btn-secondary\"
                                        data-dismiss=\"modal\">No
                                </button>

                                <form action=\"\"
                                      method=\"post\"
                                      class=\"d-inline-block\">
                                    <button class=\"btn btn-info\">Yes, I am!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            ";
        } else {
            // line 117
            echo "
                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No Series yet.</h6>
                </div>

            ";
        }
        // line 123
        echo "

        </div>

    </div>

    <form id=\"js-switch-form\" action=\"";
        // line 129
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_switch_photo_of_the_day");
        echo "\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

";
    }

    // line 136
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 137
        echo "
    <script>
        \$(document).ready(function () {
            \$('.confirm-action').on('click', function (e) {
                let modalText = \$(this).data('modal-text');
                let url = \$(this).data('url');

                \$('.modal .modal-body').html(modalText);
                \$('.modal form').attr('action', url);
            })
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "admin/selection/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 137,  261 => 136,  251 => 129,  243 => 123,  235 => 117,  204 => 88,  185 => 75,  173 => 66,  167 => 62,  158 => 56,  153 => 53,  151 => 52,  148 => 51,  139 => 45,  134 => 42,  132 => 41,  125 => 37,  119 => 34,  109 => 27,  106 => 26,  100 => 22,  98 => 21,  92 => 18,  89 => 17,  85 => 16,  82 => 15,  80 => 14,  74 => 10,  70 => 9,  64 => 7,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/selection/list.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/list.html.twig");
    }
}
