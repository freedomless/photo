<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* member/by-country.html.twig */
class __TwigTemplate_65f6067ba02f85830c4cbba576dfa343d3ac0d2e941b8b5a06be886a0e970e85 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "member/by-country.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    Members by Country
";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"container basic-layout\">

        <div class=\"members-nav\">
            ";
        // line 9
        echo twig_include($this->env, $context, "member/nav.html.twig");
        echo "
        </div>

        <div class=\"row\">

            ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 15
            echo "
                <div class=\"col-md-2 col-sm-3 col-4\">

                    <a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("members", ["page" => "by-country", "iso" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], 0, [], "any", false, false, false, 18), "iso", [], "any", false, false, false, 18)]), "html", null, true);
            echo "\">
                        <img class=\"rounded-corner\"
                             style=\"object-fit: contain; width: 100%\"
                             src=\"";
            // line 21
            echo twig_escape_filter($this->env, (($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/flags/") . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], 0, [], "any", false, false, false, 21), "iso", [], "any", false, false, false, 21))) . ".svg"), "html", null, true);
            echo "\" alt=\"\">

                        <p class=\"text-center\">
                            ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], 0, [], "any", false, false, false, 24), "name", [], "any", false, false, false, 24), "html", null, true);
            echo "
                        </p>
                    </a>

                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "
        </div>

        ";
        // line 34
        if (($context["noCountry"] ?? null)) {
            // line 35
            echo "
            <div class=\"row\">

                <div class=\"col\">

                    <hr>

                    <a href=\"";
            // line 42
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("members", ["page" => "by-country"]);
            echo "\">Worldwide users</a>

                </div>

            </div>

        ";
        }
        // line 49
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "member/by-country.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 49,  123 => 42,  114 => 35,  112 => 34,  107 => 31,  94 => 24,  88 => 21,  82 => 18,  77 => 15,  73 => 14,  65 => 9,  60 => 6,  56 => 5,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "member/by-country.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/member/by-country.html.twig");
    }
}
