<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/header.html.twig */
class __TwigTemplate_d6c000ce403bd8c688ee06808dd8ef7e9294ecdaef15df7c435250169a08b096 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid profile\">

    <div class=\"row\">

        <div class=\"cover\">

            <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", true, true, false, 7) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", false, false, false, 7)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", false, false, false, 7)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/cover.jpg"))), "html", null, true);
        echo "\" alt=\"Profile Cover\">

        </div>

    </div>

</div>

<div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

    <div class=\"row pb-4\">

        <div class=\"col-lg-2\">

            <div class=\"profile-avatar text-center\">

                <div class=\"circle\">

                    <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", true, true, false, 25) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", false, false, false, 25)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", false, false, false, 25)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
        echo "\" alt=\"Profile Picture\">
                </div>

            </div>

        </div>

        <div class=\"col-lg-10\">

            ";
        // line 35
        echo "            <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                <h1>
                    <strong>
                        ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 39), "firstName", [], "any", false, false, false, 39), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 39), "lastName", [], "any", false, false, false, 39), "html", null, true);
        echo "
                    </strong>
                </h1>

                <div class=\"mt-3\">

                    ";
        // line 45
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 45), "facebook", [], "any", false, false, false, 45))) {
            // line 46
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46))));
            // line 47
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-facebook-f\" style=\"color: #3b5998;\"></i></a>
                    ";
        }
        // line 50
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 50), "instagram", [], "any", false, false, false, 50))) {
            // line 51
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51))));
            // line 52
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-instagram\"></i></a>
                    ";
        }
        // line 55
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 55), "twitter", [], "any", false, false, false, 55))) {
            // line 56
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56))));
            // line 57
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-twitter\" style=\"color: #38A1F3;\"></i></a>
                    ";
        }
        // line 60
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 60), "website", [], "any", false, false, false, 60))) {
            // line 61
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61))));
            // line 62
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fas fa-globe\"></i></a>
                    ";
        }
        // line 65
        echo "
                    ";
        // line 66
        if (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 66) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 66), "id", [], "any", false, false, false, 66), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 66)))) && twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 66))) {
            // line 67
            echo "                        <span class=\"text-muted\">
                            <strong>Balance:</strong> ";
            // line 68
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 68)), "total", [], "any", true, true, false, 68)) ? (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 68)), "total", [], "any", false, false, false, 68)) : (0)), 2), "html", null, true);
            echo " &euro;
                        </span>
                        ";
            // line 70
            if ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 70)), "blocked", [], "any", true, true, false, 70) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 70)), "blocked", [], "any", false, false, false, 70), 0)))) {
                // line 71
                echo "                            |
                            <span class=\"text-muted\">
                            <strong>Awaiting payout:</strong> ";
                // line 73
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 73)), "blocked", [], "any", false, false, false, 73), 2), "html", null, true);
                echo " &euro;
                        </span>
                        ";
            }
            // line 76
            echo "                    ";
        }
        // line 77
        echo "
                </div>
            </div>

            ";
        // line 82
        echo "            <div class=\"profile-actions mt-3 text-center text-lg-left\">

                ";
        // line 85
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 85) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 85), "id", [], "any", false, false, false, 85), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 85))))) {
            // line 86
            echo "                    <div class=\"circle mr-3\">
                        ";
            // line 87
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Follow", ["rendering" => "client_side", "props" => ["following" => $this->extensions['App\Twig\IsFollowingExtension']->isFollowing(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 87), ($context["user"] ?? null)), "user" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 87)]]);
            echo "
                    </div>
                ";
        }
        // line 90
        echo "
                ";
        // line 92
        echo "
                ";
        // line 93
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 93), "id", [], "any", false, false, false, 93), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 93))))) {
            // line 94
            echo "
                    <div class=\"circle mr-3\">
                        ";
            // line 96
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
                // line 97
                echo "
                            <a href=\"";
                // line 98
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["_switch_user" => "_exit", "slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 98)]), "html", null, true);
                echo "\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Exit from switch\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        ";
            } else {
                // line 107
                echo "
                            <a href=\"";
                // line 108
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["_switch_user" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 108), "slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 108)]), "html", null, true);
                echo "\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Switch to user\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        ";
            }
            // line 117
            echo "                    </div>

                ";
        }
        // line 120
        echo "
                ";
        // line 122
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 122)) {
            // line 123
            echo "                    <div class=\"circle mr-3\">

                        <a href=\"";
            // line 125
            (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 125) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 125), "id", [], "any", false, false, false, 125), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 125))))) ? (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages", ["user" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 125)]), "html", null, true))) : (print ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages"))));
            echo "\"
                           class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Messages\">
                            <i class=\"far fa-envelope-open mr-1 ml-1\"></i>
                        </a>

                    </div>
                ";
        }
        // line 134
        echo "
                ";
        // line 135
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 135) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 135), "id", [], "any", false, false, false, 135), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 135))))) {
            // line 136
            echo "
                    ";
            // line 138
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 138)) {
                // line 139
                echo "
                        <div class=\"circle mr-3\">

                            <a href=\"";
                // line 142
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_payout");
                echo "\" class=\"profile-icon\" data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Request Payout\">
                                <i class=\"fas fa-wallet mr-1 ml-1\"></i>
                            </a>

                        </div>

                    ";
            }
            // line 151
            echo "
                    ";
            // line 153
            echo "                    <div class=\"circle mr-3\">

                        <a href=\"";
            // line 155
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_edit");
            echo "\" class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Settings\">
                            <i class=\"fas fa-user-edit ml-1\"></i>
                        </a>

                    </div>

                ";
        }
        // line 164
        echo "
            </div>

        </div>

    </div>

    <div class=\"row pb-4\">

        <div class=\"col-12\">

            ";
        // line 175
        echo twig_include($this->env, $context, "user/profile/navigation.html.twig");
        echo "

        </div>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "user/profile/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  327 => 175,  314 => 164,  302 => 155,  298 => 153,  295 => 151,  283 => 142,  278 => 139,  275 => 138,  272 => 136,  270 => 135,  267 => 134,  255 => 125,  251 => 123,  248 => 122,  245 => 120,  240 => 117,  228 => 108,  225 => 107,  213 => 98,  210 => 97,  208 => 96,  204 => 94,  202 => 93,  199 => 92,  196 => 90,  190 => 87,  187 => 86,  184 => 85,  180 => 82,  174 => 77,  171 => 76,  165 => 73,  161 => 71,  159 => 70,  154 => 68,  151 => 67,  149 => 66,  146 => 65,  139 => 62,  136 => 61,  133 => 60,  126 => 57,  123 => 56,  120 => 55,  113 => 52,  110 => 51,  107 => 50,  100 => 47,  97 => 46,  95 => 45,  84 => 39,  78 => 35,  66 => 25,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/header.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/header.html.twig");
    }
}
