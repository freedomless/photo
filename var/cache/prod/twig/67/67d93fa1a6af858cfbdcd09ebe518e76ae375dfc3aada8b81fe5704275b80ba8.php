<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/_verify.html.twig */
class __TwigTemplate_7bcfa389ffdd54aa8dbc7e16eee5af00fdda870cb1fd95a05b98c5a79710931f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1), "isVerified", [], "any", false, false, false, 1) == false)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1) == "web_popular"))) {
            // line 2
            echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"alert alert-danger confirm-email-notification\">
                <span class=\"text-uppercase\">You need to confirm your email address.</span><br>
                Check your email <b>inbox</b>.
                If you can't find the verification email, check your <b>spam/junk</b> box,
                <a class=\"text-white\" href=\"";
            // line 9
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_resend_confirmation");
            echo "\">Re-Send</a> it
                or <a class=\"text-white\" href=\"";
            // line 10
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
            echo "\">Contact us</a>.
            </div>
        </div>
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "_partials/_verify.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  48 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/_verify.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/_verify.html.twig");
    }
}
