<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/cookie_accept.html.twig */
class __TwigTemplate_81b1ceaa1ea9092d5d64d785c3b7ec4e07137ce963c0185ee8572e4a3de3a576 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1), "acceptPolicies", [], "any", false, false, false, 1))) || ((null === twig_get_attribute($this->env, $this->source,         // line 2
($context["app"] ?? null), "user", [], "any", false, false, false, 2)) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 2), "cookies", [], "any", false, false, false, 2), "get", [0 => "cookies"], "method", false, false, false, 2))))) {
            // line 3
            echo "
    <div class=\"cookies\">
        <span>We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.</span>
        <button class=\"btn btn-info btn-sm ml-3\" id=\"accept-cookies\">Accept</button>
    </div>

    <script>
        \$(document).ready(function () {
            \$('#accept-cookies').click(function () {
                \$('.cookies').remove();
                \$.ajax({
                    url: '/api/accept-cookies'
                });
            })
        })
    </script>

";
        }
    }

    public function getTemplateName()
    {
        return "_partials/cookie_accept.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 3,  38 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/cookie_accept.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/cookie_accept.html.twig");
    }
}
