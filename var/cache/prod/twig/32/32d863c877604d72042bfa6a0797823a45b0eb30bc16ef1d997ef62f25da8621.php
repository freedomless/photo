<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/pending.html.twig */
class __TwigTemplate_bfdee7991fac9124fd0e6f71473238058b593415648bd51e2af2fc95612559e3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/pending.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pending";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "

    <div class=\"curator-page basic-layout\">

        ";
        // line 10
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/pending.html.twig", 10)->display($context);
        // line 11
        echo "
        <hr>

        ";
        // line 14
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/pending.html.twig", 14)->display($context);
        // line 15
        echo "
        <hr>

        <div class=\"d-flex justify-content-center\">

            ";
        // line 20
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "

        </div>

        <div class=\"curate-photos\">
            <div class=\"photos container\">

                <div class=\"row w-100 \">

                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 30
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 text-center w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 37
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 37), "isGreyscale", [], "any", false, false, false, 37), true))) {
                // line 38
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 42
            echo "
                                    ";
            // line 44
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), "cover", [], "any", false, false, false, 44))) {
                // line 45
                echo "                                        ";
                $context["comments"] = 0;
                // line 46
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 46), "children", [], "any", false, false, false, 46));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 47
                    echo "                                            ";
                    $context["comments"] = (($context["comments"] ?? null) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 47)));
                    // line 48
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "
                                        ";
                // line 50
                if ((1 === twig_compare(($context["comments"] ?? null), 0))) {
                    // line 51
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 53
                    echo twig_escape_filter($this->env, ($context["comments"] ?? null), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 57
                echo "
                                    ";
            } else {
                // line 59
                echo "
                                        ";
                // line 60
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 60), "curateComments", [], "any", false, false, false, 60)), 0))) {
                    // line 61
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 63
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 63), "curateComments", [], "any", false, false, false, 63)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 67
                echo "
                                    ";
            }
            // line 69
            echo "                                    ";
            // line 70
            echo "
                                    <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 71), "id", [], "any", false, false, false, 71)]), "html", null, true);
            echo "\">

                                        ";
            // line 73
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "type", [], "any", false, false, false, 73), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "image", [], "any", false, false, false, 73), "thumbnail", [], "any", false, false, false, 73)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "cover", [], "any", false, false, false, 73), "image", [], "any", false, false, false, 73), "thumbnail", [], "any", false, false, false, 73)));
            // line 74
            echo "
                                        <img src=\"";
            // line 75
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\" alt=\"\" style=\"max-width:100%;max-height: 300px\">


                                    </a>


                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    ";
            // line 87
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 87), "sentForCurateDate", [], "any", false, false, false, 87), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Rating:</strong>

                                    <strong class=\"text-success mr-1\">";
            // line 95
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 95), 2), "html", null, true);
            echo "</strong>

                                    (";
            // line 97
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "total", [], "any", false, false, false, 97), "html", null, true);
            echo " votes)

                                </div>

                                ";
            // line 101
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 101), "type", [], "any", false, false, false, 101), 1))) {
                // line 102
                echo "
                                    <div class=\"card-footer d-flex\">

                                    <span class=\"flex-grow-1\">

                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>

                                    </span>

                                        Series

                                    </div>

                                ";
            }
            // line 117
            echo "
                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center\">

            ";
        // line 131
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "

        </div>


    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/curator/pending.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 131,  259 => 123,  248 => 117,  231 => 102,  229 => 101,  222 => 97,  217 => 95,  206 => 87,  191 => 75,  188 => 74,  186 => 73,  181 => 71,  178 => 70,  176 => 69,  172 => 67,  165 => 63,  161 => 61,  159 => 60,  156 => 59,  152 => 57,  145 => 53,  141 => 51,  139 => 50,  136 => 49,  130 => 48,  127 => 47,  122 => 46,  119 => 45,  116 => 44,  113 => 42,  107 => 38,  105 => 37,  96 => 30,  92 => 29,  80 => 20,  73 => 15,  71 => 14,  66 => 11,  64 => 10,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/pending.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/pending.html.twig");
    }
}
