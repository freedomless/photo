<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/favorites.html.twig */
class __TwigTemplate_520e640a91816a2f2f8494fda04171c8b7efdec036a69146e341e1e19783e614 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/favorites.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "



    <div class=\"col-12\">

        ";
        // line 10
        if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "posts", [], "any", false, false, false, 10)), 0))) {
            // line 11
            echo "
            <h3 class=\"mt-5 mb-5 text-center text-muted\">No Photos.</h3>

        ";
        } else {
            // line 15
            echo "
            ";
            // line 16
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["nude" =>             // line 17
($context["nude"] ?? null), "auth" =>             // line 18
($context["auth"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,             // line 19
($context["posts"] ?? null), "items", [], "any", false, false, false, 19), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,             // line 20
($context["posts"] ?? null), "total", [], "any", false, false, false, 20), 0)), "mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "page" => "favorites"]]);
            // line 23
            echo "

        ";
        }
        // line 26
        echo "
    </div>


";
    }

    public function getTemplateName()
    {
        return "user/profile/favorites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 26,  75 => 23,  73 => 20,  72 => 19,  71 => 18,  70 => 17,  69 => 16,  66 => 15,  60 => 11,  58 => 10,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/favorites.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/favorites.html.twig");
    }
}
