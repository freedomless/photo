<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site_notifications/guests_message.html.twig */
class __TwigTemplate_da6da90cec4c24659da2eb29bbf0c5b5bcb0f5e34cb67102a0ff7610d4e72e3f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'message_title' => [$this, 'block_message_title'],
            'message_content' => [$this, 'block_message_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "site_notifications/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("site_notifications/_base.html.twig", "site_notifications/guests_message.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_message_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"text-center\">
        Upload your photos to get a chance to be published!
    </div>
";
    }

    // line 9
    public function block_message_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    <div class=\"text-center d-none d-md-block\">
        Each photo is carefully analyzed by professional curators.<br>
        Only the most impressive pictures of those sent for curation are posted in
        the ";
        // line 13
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " gallery.

    </div>
    <hr class=\"d-none d-md-block\">

    <div>
        <a class=\"btn btn-success btn-block text-white\" href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_register");
        echo "\">Register</a>
        <a class=\"btn btn-primary  btn-block text-white\" href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
        echo "\">Login</a>
    </div>
";
    }

    public function getTemplateName()
    {
        return "site_notifications/guests_message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 20,  76 => 19,  67 => 13,  62 => 10,  58 => 9,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "site_notifications/guests_message.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/site_notifications/guests_message.html.twig");
    }
}
