<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/reject-reasons.html.twig */
class __TwigTemplate_98d77e322fe6fcfe5810c5fab4fd261351eb4599d6fd7e516730a81ef28b07a1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p>Hi there,</p>
<p>We are informing you that your photo was rejected because:</p>

<hr>

<ol class=\"list-group \">
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["reasons"] ?? null));
        foreach ($context['_seq'] as $context["index"] => $context["reason"]) {
            // line 8
            echo "        <li  class=\"list-group-item bg-transparent\">";
            echo twig_escape_filter($this->env, ($context["index"] + 1), "html", null, true);
            echo ". ";
            echo twig_escape_filter($this->env, $context["reason"], "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['reason'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</ol>

<img src=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12), "html", null, true);
        echo "\" class=\"mt-5\" alt=\"\" width=\"100%\">
";
    }

    public function getTemplateName()
    {
        return "photo/reject-reasons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  60 => 10,  49 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/reject-reasons.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/reject-reasons.html.twig");
    }
}
