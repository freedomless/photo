<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/track-id.html.twig */
class __TwigTemplate_f8e966854e1f83adecbb8bfb2ae5e4d983720c172e8e5197d10a4496cee5f51a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/track-id.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Dispatched";
    }

    // line 5
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "name", [], "any", false, false, false, 5), "html", null, true);
    }

    // line 7
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "Dispatched
";
    }

    public function getTemplateName()
    {
        return "emails/track-id.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 8,  62 => 7,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/track-id.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/track-id.html.twig");
    }
}
