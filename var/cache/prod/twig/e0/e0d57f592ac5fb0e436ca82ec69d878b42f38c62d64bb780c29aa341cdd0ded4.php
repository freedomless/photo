<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/posts.html.twig */
class __TwigTemplate_8d18664f47691f06d9ed8d4394831b1a27b71be06b689be8bd1ebbf5efd666ce extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)]), "html", null, true);
            echo "\">

                    ";
            // line 12
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "type", [], "any", false, false, false, 12), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "cover", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)));
            // line 13
            echo "
                    <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\"
                         alt=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 15), "title", [], "any", false, false, false, 15), "html", null, true);
            echo "\">
                </a>

                ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 18), 1)) {
                // line 19
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 25
            echo "
                ";
            // line 26
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 26), 3) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 26), "showDate", [], "any", false, false, false, 26), twig_date_converter($this->env))))) {
                // line 27
                echo "
                    ";
                // line 28
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 28), "curatedAt", [], "any", false, false, false, 28), ($context["site_publish_visibility_delay"] ?? null)), twig_date_converter($this->env)))) {
                    // line 29
                    echo "
                        <div class=\"sticker pending semi-transparent\">
                            Pending Review!
                        </div>

                    ";
                } else {
                    // line 35
                    echo "
                        <div class=\"sticker approved semi-transparent\">
                            Scheduled for Publish!
                        </div>

                    ";
                }
                // line 41
                echo "
                ";
            }
            // line 43
            echo "
                ";
            // line 44
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), 3) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), "showDate", [], "any", false, false, false, 44), twig_date_converter($this->env))))) {
                // line 45
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        Published!
                    </div>

                ";
            }
            // line 51
            echo "
                ";
            // line 52
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 52), 2)) {
                // line 53
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 59
            echo "
            </div>

            <div class=\"image-thumb-metadata\">

                Stats

                <div class=\"stats mt-1 mb-1 text-center\">

                    <i class=\"far fa-eye\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Views\"></i> ";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 69), "views", [], "any", false, false, false, 69), "html", null, true);
            echo "

                    <i class=\"ml-5 far fa-heart\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Favorite\"></i> ";
            // line 72
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 72), "favorites", [], "any", false, false, false, 72)), "html", null, true);
            echo "

                    ";
            // line 74
            if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 74), 0))) {
                // line 75
                echo "                        <i class=\"ml-5 fas fa-fire-alt\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"Popularity\"></i>
                        ";
                // line 77
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 77), 2, ".", ","), "html", null, true);
                echo " / 5
                    ";
            }
            // line 79
            echo "
                    ";
            // line 80
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 80), "isGreyscale", [], "any", false, false, false, 80)) {
                // line 81
                echo "                        <i class=\"ml-5 fas fa-adjust\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Black&White\"></i>
                    ";
            }
            // line 84
            echo "
                </div>

                <hr class=\"no-gutter\">

                Actions

                <div class=\"actions mb-1\">


                    ";
            // line 94
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 94), 0) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 94), "parent", [], "any", false, false, false, 94)))) {
                // line 95
                echo "

                        <button class=\"btn curator-warning-btn btn-info\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Send to Curators\"
                                data-url=\"";
                // line 101
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_curate", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 101), "id", [], "any", false, false, false, 101), "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 101), "get", [0 => "page"], "method", false, false, false, 101)]), "html", null, true);
                echo "\">
                            Send to curators
                        </button>


                    ";
            }
            // line 107
            echo "
                    <a href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 108), "id", [], "any", false, false, false, 108)]), "html", null, true);
            echo "\"
                       data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Edit\"
                       class=\"btn btn-default\">
                        <i class=\"fa fa-edit\"></i>
                    </a>

                    ";
            // line 115
            if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 115), true)) && $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source,             // line 116
$context["data"], "post", [], "any", false, false, false, 116), 3)) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 116), "showDate", [], "any", false, false, false, 116), twig_date_converter($this->env))))) {
                // line 117
                echo "
                        <a href=\"";
                // line 118
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 118), "id", [], "any", false, false, false, 118), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 118), "get", [0 => "type"], "method", false, false, false, 118)]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"";
                // line 122
                echo (( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 122), "product", [], "any", false, false, false, 122))) ? ("Update Products") : ("Add to store"));
                echo "\">

                            ";
                // line 124
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 124), "product", [], "any", false, false, false, 124))) {
                    // line 125
                    echo "                                <i class=\"fas fa-store\"></i>
                            ";
                } else {
                    // line 127
                    echo "                                <span class=\"btn btn-outline-success btn-sm\">
                                    SELL
                                </span>
                            ";
                }
                // line 131
                echo "
                        </a>

                    ";
            }
            // line 135
            echo "

                    ";
            // line 137
            if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), 0) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), 2)) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), "parent", [], "any", false, false, false, 137)))) {
                // line 138
                echo "
                        <button type=\"button\"
                                class=\"btn delete-warning-btn\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Delete\"
                                data-url=\"";
                // line 144
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 144), "id", [], "any", false, false, false, 144)]), "html", null, true);
                echo "\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>
                        ";
                // line 148
                echo "                    ";
            }
            // line 149
            echo "
                </div>

            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "post/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 149,  281 => 148,  275 => 144,  267 => 138,  265 => 137,  261 => 135,  255 => 131,  249 => 127,  245 => 125,  243 => 124,  238 => 122,  231 => 118,  228 => 117,  226 => 116,  225 => 115,  215 => 108,  212 => 107,  203 => 101,  195 => 95,  193 => 94,  181 => 84,  176 => 81,  174 => 80,  171 => 79,  166 => 77,  162 => 75,  160 => 74,  155 => 72,  149 => 69,  137 => 59,  129 => 53,  127 => 52,  124 => 51,  116 => 45,  114 => 44,  111 => 43,  107 => 41,  99 => 35,  91 => 29,  89 => 28,  86 => 27,  84 => 26,  81 => 25,  73 => 19,  71 => 18,  65 => 15,  61 => 14,  58 => 13,  56 => 12,  51 => 10,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "post/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/posts.html.twig");
    }
}
