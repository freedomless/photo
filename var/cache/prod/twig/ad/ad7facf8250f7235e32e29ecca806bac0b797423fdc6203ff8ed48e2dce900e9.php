<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/show.html.twig */
class __TwigTemplate_ab9ef77c808523bb71bf09457e0784866a6b425e54f394d4712e703e34b77004 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", true, true, false, 3) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 3)))) ? (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 3)) : (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "slug", [], "any", false, false, false, 3))), "html", null, true);
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"mt-5\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/show.html.twig", 9)->display($context);
        // line 10
        echo "
    </div>

    <hr>

    <div class=\"container basic-layout\">


        ";
        // line 18
        if (twig_in_filter("pending", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 18), "get", [0 => "back"], "method", false, false, false, 18))) {
            // line 19
            echo "
        <div class=\"clearfix\">

            <div class=\"float-left\">
                <i class=\"fa fa-backward\"></i>
                <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show_prev", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 24)]), "html", null, true);
            echo "\" class=\"btn btn-link\">
                    Previous
                </a>
            </div>

            <div class=\"float-right\">
                <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show_next", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\" class=\"btn btn-link\">
                    Next
                </a>
                <i class=\"fa fa-forward\"></i>
            </div>

        </div>

        ";
        }
        // line 39
        echo "
        <div class=\"card\">

            <a class=\"position-absolute\" style=\"right: 10px;top:10px;\"
               href=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 43), "get", [0 => "back"], "method", false, false, false, 43), "html", null, true);
        echo "\">
                <i class=\"fas fa-times\"></i>
            </a>

            ";
        // line 47
        $this->loadTemplate("admin/curator/_partials/curator-actions.html.twig", "admin/curator/show.html.twig", 47)->display($context);
        // line 48
        echo "
            <div class=\"card-body\">

                ";
        // line 51
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "type", [], "any", false, false, false, 51), 1))) {
            // line 52
            echo "
                    ";
            // line 53
            $this->loadTemplate("admin/curator/_partials/single.html.twig", "admin/curator/show.html.twig", 53)->display($context);
            // line 54
            echo "
                ";
        } else {
            // line 56
            echo "
                    ";
            // line 57
            $this->loadTemplate("admin/curator/_partials/photo-update.html.twig", "admin/curator/show.html.twig", 57)->display($context);
            // line 58
            echo "
                ";
        }
        // line 60
        echo "            </div>
        </div>

        ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 63));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 64
            echo "
            <div class=\"card mt-5\">

                <div class=\"card-body\">

                    ";
            // line 69
            $context["post"] = $context["child"];
            // line 70
            echo "
                    ";
            // line 71
            $this->loadTemplate("admin/curator/_partials/single.html.twig", "admin/curator/show.html.twig", 71)->display($context);
            // line 72
            echo "
                </div>

            </div>

        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "
    </div>

    <div class=\"large-image large\">
        <button class=\"close-btn\">
            <i class=\"fas fa-times\"></i>
        </button>
        <img src=\"\" alt=\"\" id=\"large-img\">
    </div>

";
    }

    // line 90
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 91
        echo "    <script>
        let img = \$('#large-img');
        let large = \$('.large');

        \$('.post').click(function (e) {
            large.addClass('show');
            img.attr('src', \$(this).data('url'))
        });

        \$('.close-btn').click(function (e) {
            img.attr('src', '');
            large.removeClass('show');
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 91,  215 => 90,  201 => 78,  182 => 72,  180 => 71,  177 => 70,  175 => 69,  168 => 64,  151 => 63,  146 => 60,  142 => 58,  140 => 57,  137 => 56,  133 => 54,  131 => 53,  128 => 52,  126 => 51,  121 => 48,  119 => 47,  112 => 43,  106 => 39,  94 => 30,  85 => 24,  78 => 19,  76 => 18,  66 => 10,  64 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/show.html.twig");
    }
}
