<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/poll/list.html.twig */
class __TwigTemplate_bb07f63897d6047ed81d94de6608083ba707e9d86a5ee3cf39b3d71606158470 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
            'css' => [$this, 'block_css'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/poll/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Manage polls";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Polls";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_new");
        echo "\" class=\"btn btn-success text-white\"><i class=\"fa fa-plus\"></i> Create New Poll</a>
";
    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
    <div class=\"col-12\">

        <div class=\"row\">

            ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["polls"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["poll"]) {
            // line 18
            echo "
                <div class=\"col-12 col-md-6 col-lg-4 mb-4\">

                    <div class=\"card ";
            // line 21
            echo ((twig_get_attribute($this->env, $this->source, $context["poll"], "status", [], "any", false, false, false, 21)) ? ("") : ("inactive-pool"));
            echo "\">

                        <div class=\"card-header bg-";
            // line 23
            echo ((twig_get_attribute($this->env, $this->source, $context["poll"], "status", [], "any", false, false, false, 23)) ? ("success") : ("dark"));
            echo "\">

                            ";
            // line 25
            if (twig_get_attribute($this->env, $this->source, $context["poll"], "status", [], "any", false, false, false, 25)) {
                // line 26
                echo "
                                <span class=\"badge badge-success\" style=\"font-size: 1rem\">Active</span>

                                <a href=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_deactivate", ["id" => twig_get_attribute($this->env, $this->source,                 // line 30
$context["poll"], "id", [], "any", false, false, false, 30)]), "html", null, true);
                // line 31
                echo "\"
                                   class=\"btn btn-sm text-danger float-right\"><i class=\"fa fa-times\"></i> Deactivate</a>

                            ";
            } else {
                // line 35
                echo "
                                <span class=\"badge badge-light\" style=\"font-size: 1rem\">Inactive</span>

                                <a href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_activate", ["id" => twig_get_attribute($this->env, $this->source,                 // line 39
$context["poll"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                // line 40
                echo "\" class=\"btn btn-sm text-success float-right\"><i class=\"fa fa-check\"></i> Activate</a>

                            ";
            }
            // line 43
            echo "
                        </div>

                        <div class=\"card-body\">
                            <h5 class=\"card-title\">";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "title", [], "any", false, false, false, 47), "html", null, true);
            echo "</h5>

                            <p class=\"text-muted card-text\">

                                <span>";
            // line 51
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "createdAt", [], "any", false, false, false, 51)), "html", null, true);
            echo "</span>

                                <br>
                                ";
            // line 54
            if (twig_get_attribute($this->env, $this->source, $context["poll"], "description", [], "any", false, false, false, 54)) {
                // line 55
                echo "                                    <span>";
                echo twig_get_attribute($this->env, $this->source, $context["poll"], "description", [], "any", false, false, false, 55);
                echo "</span>
                                ";
            }
            // line 57
            echo "
                                <br>
                                ";
            // line 59
            if (twig_get_attribute($this->env, $this->source, $context["poll"], "link", [], "any", false, false, false, 59)) {
                // line 60
                echo "                                    <span><a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "link", [], "any", false, false, false, 60), "html", null, true);
                echo "\">Link</a></span>
                                ";
            }
            // line 62
            echo "                            </p>


                            <ul class=\"list-group\">

                                <li class=\"list-group-item bg-secondary pt-1 pb-1\">
                                    ";
            // line 68
            echo (((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["poll"], "choices", [], "any", false, false, false, 68)), 0))) ? ("-- No Choises --") : ("Choices:"));
            echo "
                                </li>

                                ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["poll"], "choices", [], "any", false, false, false, 71));
            foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
                // line 72
                echo "                                    <li class=\"list-group-item pt-1 pb-1\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "title", [], "any", false, false, false, 72), "html", null, true);
                echo "
                                        <span class=\"badge badge-secondary float-right\">";
                // line 73
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "votes", [], "any", false, false, false, 73)), "html", null, true);
                echo "</span>
                                    </li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "                            </ul>

                            <a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_manage_choices", ["id" => twig_get_attribute($this->env, $this->source,             // line 79
$context["poll"], "id", [], "any", false, false, false, 79)]), "html", null, true);
            // line 80
            echo "\" class=\"btn btn-primary text-white mt-3\">Manage Choices</a>

                        </div>

                        <div class=\"card-footer\">
                            <a href=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_edit", ["id" => twig_get_attribute($this->env, $this->source,             // line 86
$context["poll"], "id", [], "any", false, false, false, 86)]), "html", null, true);
            // line 87
            echo "\" class=\"btn btn-warning text-white mr-3\"><i class=\"fa fa-edit\"></i> Edit</a>
                            <a href=\"";
            // line 88
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_delete", ["id" => twig_get_attribute($this->env, $this->source,             // line 89
$context["poll"], "id", [], "any", false, false, false, 89)]), "html", null, true);
            // line 90
            echo "\"
                               class=\"btn btn-danger text-white\"
                               onclick=\"return confirm('Sure?')\"
                            ><i class=\"fa fa-times\"></i> Delete</a>
                        </div>

                    </div>

                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['poll'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "
        </div>

    </div>

";
    }

    // line 108
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 109
        echo "    <style>
        .inactive-pool {
            opacity: .5;
        }

        .inactive-pool:hover {
            opacity: 1;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "admin/poll/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 109,  253 => 108,  244 => 101,  228 => 90,  226 => 89,  225 => 88,  222 => 87,  220 => 86,  219 => 85,  212 => 80,  210 => 79,  209 => 78,  205 => 76,  196 => 73,  191 => 72,  187 => 71,  181 => 68,  173 => 62,  167 => 60,  165 => 59,  161 => 57,  155 => 55,  153 => 54,  147 => 51,  140 => 47,  134 => 43,  129 => 40,  127 => 39,  126 => 38,  121 => 35,  115 => 31,  113 => 30,  112 => 29,  107 => 26,  105 => 25,  100 => 23,  95 => 21,  90 => 18,  86 => 17,  79 => 12,  75 => 11,  68 => 8,  64 => 7,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/poll/list.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/poll/list.html.twig");
    }
}
