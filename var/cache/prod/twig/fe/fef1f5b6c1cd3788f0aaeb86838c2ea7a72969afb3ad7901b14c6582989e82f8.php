<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_button.html.twig */
class __TwigTemplate_4f30e4e84443ae1ce59b949a99632752577a0975a0d934a007c3e95af76350e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><a href=\"";
        (((array_key_exists("button_url", $context) &&  !(null === ($context["button_url"] ?? null)))) ? (print (twig_escape_filter($this->env, ($context["button_url"] ?? null), "html", null, true))) : (print ("#")));
        echo "\"
                                                                         class=\"btn btn-primary\"
                                                                         style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #ffffff;padding: 10px 15px;border-radius: 10px;background: #375a7f;\">";
        // line 3
        (((array_key_exists("button_title", $context) &&  !(null === ($context["button_title"] ?? null)))) ? (print (twig_escape_filter($this->env, ($context["button_title"] ?? null), "html", null, true))) : (print ("Click Here")));
        echo "</a>
</p>";
    }

    public function getTemplateName()
    {
        return "emails/_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/_button.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_button.html.twig");
    }
}
