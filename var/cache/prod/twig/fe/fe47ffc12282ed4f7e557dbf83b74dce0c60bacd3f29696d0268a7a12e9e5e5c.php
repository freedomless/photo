<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maintenance.html */
class __TwigTemplate_2aa4258c5aecf2ba20d808599cd45ffaf46fa3ba4df95c56dcbc6d6ef0fcbe54 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<head>
    <meta charset=\"utf-8\">
    <title>Site temporarily under maintenance</title>
    <link rel=\"stylesheet\" href=\"/build/app.css\">
</head>
<body class=\"vh-100\">
    <div class=\"center flex-column\">
        <div class=\"bg-primary circle p-3\">
            <img src=\"/images/logo.png\" alt=\"Logo\">
        </div>
        <h1>Site temporarily under maintenance</h1>
        <p>Sorry for the inconvenience, we will get back soon !</p>
    </div>
</body>
</html>

";
    }

    public function getTemplateName()
    {
        return "maintenance.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "maintenance.html", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/maintenance.html");
    }
}
