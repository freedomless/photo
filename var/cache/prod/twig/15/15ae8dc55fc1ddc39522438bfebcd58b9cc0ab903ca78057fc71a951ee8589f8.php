<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/analytics.html.twig */
class __TwigTemplate_9c21c316325f9da761e4aa1749f4107d194de6ae7e21ec88b41ac262d85fdb3e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script async src=\"";
        echo twig_escape_filter($this->env, (($context["google_analytics_url"] ?? null) . ($context["google_analytics_id"] ?? null)), "html", null, true);
        echo "\"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '";
        // line 11
        echo twig_escape_filter($this->env, ($context["google_analytics_id"] ?? null), "html", null, true);
        echo "');
</script>";
    }

    public function getTemplateName()
    {
        return "_partials/analytics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/analytics.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/analytics.html.twig");
    }
}
