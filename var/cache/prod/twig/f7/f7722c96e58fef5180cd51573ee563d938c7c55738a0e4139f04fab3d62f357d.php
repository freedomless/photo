<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_post_award.html.twig */
class __TwigTemplate_c868dd59e1707772739a3b172d21f573c5df476e163e081cf874eba6bec5ab4e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 1), "type", [], "any", false, false, false, 1), 1))) {
            // line 2
            echo "    <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 2), "image", [], "any", false, false, false, 2), "thumbnail", [], "any", false, false, false, 2), "html", null, true);
            echo "\" alt=\"\" width=\"100px\" class=\"rounded shadow-sm\">
";
        } else {
            // line 4
            echo "    <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 4), "cover", [], "any", false, false, false, 4), "image", [], "any", false, false, false, 4), "thumbnail", [], "any", false, false, false, 4), "html", null, true);
            echo "\" alt=\"\" width=\"100px\" class=\"rounded shadow-sm\">
";
        }
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_post_award.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/_post_award.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_post_award.html.twig");
    }
}
