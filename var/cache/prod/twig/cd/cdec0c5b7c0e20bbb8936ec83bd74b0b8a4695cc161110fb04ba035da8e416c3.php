<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* album/form.html.twig */
class __TwigTemplate_5de08792fe793510810decfd6c88d6595700038c412bb7374dc514075c5d416f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "album/form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Album";
    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <style>
        .series-option {
            text-align: center;
        }

        .series-option label img {
            max-width: 100%;
            max-height: 300px;
            border: 5px solid transparent;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            cursor: pointer;

        }

        .series-option span {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translateY(-50%) translateX(-50%);
            font-weight: bold;
            font-size: 40px;
            -webkit-text-stroke: 2px black;
        }

        .series-option input {
            display: none;
        }

        .series-option input:checked + img {
            border: 5px dashed #aaa;
        }

        .series-option input:checked + img.cover {
            border: 5px dashed #375a7f !important;
        }
    </style>
";
    }

    // line 46
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "
    ";
        // line 48
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "form_div_layout.html.twig"], true);
        // line 49
        echo "
    <div class=\"container basic-layout\">

        <div class=\"row\">

            ";
        // line 54
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

            <div class=\"col-12 default-form-layout\">

                <h3>

                    ";
        // line 60
        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 60), "method", [], "any", false, false, false, 60), "PUT"))) ? ("Update") : ("Create"));
        echo " Album

                    <span class=\"float-right\">
                        <a href=\"";
        // line 63
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 63), "session", [], "any", false, true, false, 63), "get", [0 => "back"], "method", true, true, false, 63) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 63), "session", [], "any", false, true, false, 63), "get", [0 => "back"], "method", false, false, false, 63)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 63), "session", [], "any", false, true, false, 63), "get", [0 => "back"], "method", false, false, false, 63)) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_albums", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 64
($context["app"] ?? null), "user", [], "any", false, false, false, 64), "slug", [], "any", false, false, false, 64)]))), "html", null, true);
        // line 65
        echo "\">
                        </a>
                    </span>

                </h3>

                <hr>

                <div class=\"form-group row\">

                    <div class=\"col-md-2 col-form-label\">
                        ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 76), 'label', ["label" => "Album Name:"]);
        echo "
                    </div>

                    <div class=\"col-md-10\">
                        ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 80), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 81), 'errors');
        echo "
                    </div>

                </div>

                <hr>

                <div class=\"d-flex\">

                    <div class=\"flex-grow-1\">
                        Select Images:
                    </div>

                    <div class=\"text-muted\">Image bordered in different color will be used for cover.</div>

                </div>

                <hr>

                <div class=\"text-danger\">
                    ";
        // line 101
        echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "posts", [], "any", false, false, false, 101), 'errors')), "html", null, true);
        echo "
                </div>

                <div class=\"row cover-option mt-5\">

                    ";
        // line 106
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "posts", [], "any", false, false, false, 106));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 107
            echo "
                        <div class=\"col-md-4 col-sm-6 col-lg-3 d-flex justify-content-center align-items-center mb-5 series-option\">

                            <label for=\"";
            // line 110
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "vars", [], "any", false, false, false, 110), "id", [], "any", false, false, false, 110), "html", null, true);
            echo "\">

                                ";
            // line 112
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["post"], 'widget');
            echo "

                                <img src=\"";
            // line 114
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "vars", [], "any", false, false, false, 114), "label", [], "any", false, false, false, 114), "html", null, true);
            echo "\" alt=\"\">

                            </label>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "

                </div>

                <hr>

                <div class=\"text-center\">
                    <button class=\"btn btn-info\">";
        // line 128
        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 128), "method", [], "any", false, false, false, 128), "PUT"))) ? ("Update") : ("Create"));
        echo "</button>
                    <button class=\"btn btn-light\" id=\"reset-selection\">Clear Selection</button>
                </div>

            </div>


            ";
        // line 135
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

        </div>

    </div>

";
    }

    // line 143
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 144
        echo "    <script>
        \$(document).ready(\$('#reset-selection').on('click', function (e) {
            e.preventDefault();
            \$('input[type=\"radio\"]').prop('checked', false);
        }));

    </script>

    <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.cover-option ', '.cover-option ');</script>

    <script>

        \$(function () {

            let hiddenOrder = \$('#album_order');

            let order = hiddenOrder.val() ? hiddenOrder.val().split(',') : [];

            let selected = \$('.selected-images');

            function updateNumbers() {
                \$('.series-option input[type=\"checkbox\"]').each(function (index, item) {

                    item = \$(item);

                    let i = order.indexOf(item.val());


                    if (i > -1) {


                        item.siblings('img').removeClass('cover');

                        if (i === 0) {
                            item.siblings('img').addClass('cover');
                        }

                        item.parent().siblings('.order').text(i + 1)
                        item.prop('checked', true)
                    } else {
                        item.parent().siblings('.order').html('')
                    }

                });
            }

            updateNumbers();

            function updateCheckbox(e) {

                e.preventDefault();

                if (e.target.checked) {

                    order.push(e.target.value);

                    \$(e.target).parent().siblings('.order').text(order.length)

                } else {

                    order = order.filter(function (v) {
                        return v != e.target.value;
                    })

                }

                updateNumbers();

                hiddenOrder.val(order);

                selected.text(order.length);

            }

            \$(document).on('change', '.series-option input[type=\"checkbox\"]', updateCheckbox)

            \$('#reset-selection').on('click', function (e) {
                e.preventDefault();
                \$('.series-option input[type=\"checkbox\"]').prop('checked', false);
                \$('.selected-images').text(0);
                order = [];

                updateNumbers()
            })
        });

        \$(function () {
            let h = \$('#selected-images');

            \$(document).scroll(\$.throttle(250, function (e) {

                if (\$(window).scrollTop() > 700) {

                    h.css({
                        'position': 'fixed',
                        'left': '50%',
                        'top': '100px',
                        'background': '#fff',
                        'zIndex': 100,
                        'transform': 'translateX(-50%)'
                    });
                    h.addClass('shadow rounded p-2')

                } else {

                    h.removeAttr('style');
                    h.removeClass('shadow rounded p-2')

                }
            }))
        });

    </script>


";
        // line 262
        echo "
";
        // line 264
        echo "
";
        // line 282
        echo "
";
        // line 285
        echo "
";
        // line 287
        echo "
";
        // line 289
        echo "
";
        // line 294
        echo "
";
        // line 298
        echo "
";
        // line 301
        echo "
";
        // line 303
        echo "
";
        // line 305
        echo "
";
        // line 315
        echo "
";
        // line 317
        echo "
";
        // line 320
        echo "
";
        // line 324
        echo "
";
        // line 326
        echo "

";
    }

    public function getTemplateName()
    {
        return "album/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  424 => 326,  421 => 324,  418 => 320,  415 => 317,  412 => 315,  409 => 305,  406 => 303,  403 => 301,  400 => 298,  397 => 294,  394 => 289,  391 => 287,  388 => 285,  385 => 282,  382 => 264,  379 => 262,  267 => 152,  257 => 144,  253 => 143,  242 => 135,  232 => 128,  223 => 121,  210 => 114,  205 => 112,  200 => 110,  195 => 107,  191 => 106,  183 => 101,  160 => 81,  156 => 80,  149 => 76,  136 => 65,  134 => 64,  133 => 63,  127 => 60,  118 => 54,  111 => 49,  109 => 48,  106 => 47,  102 => 46,  60 => 6,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "album/form.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/album/form.html.twig");
    }
}
