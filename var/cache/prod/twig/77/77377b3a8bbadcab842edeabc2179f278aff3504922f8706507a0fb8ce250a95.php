<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/author-details.html.twig */
class __TwigTemplate_fbe174e943ad8319956b712531706339af2a658a555b767a22ab693f2a6ee814 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header text-info\">

        <strong class=\"h4\">";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 5), "profile", [], "any", false, false, false, 5), "firstName", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 5), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "</strong>

        <div class=\"float-right\">
            ";
        // line 8
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "type", [], "any", false, false, false, 8), 1))) {
            // line 9
            echo "
                <div class=\"text-center\">

                    ";
            // line 12
            if ( !($context["hasBeenPhotoOfTheDay"] ?? null)) {
                // line 13
                echo "
                        <form method=\"POST\">
                            <button formaction=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_create_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 15)]), "html", null, true);
                echo "\"
                                    class=\"btn btn-sm btn-success\">
                                Make Photo of the day
                            </button>
                        </form>

                    ";
            } else {
                // line 22
                echo "
                        <span class=\"text-muted\">This photo was already <strong>Photo of the Day</strong></span>

                    ";
            }
            // line 26
            echo "
                </div>

            ";
        }
        // line 30
        echo "        </div>

    </div>

    <div class=\"card-body p-0\">

        <div class=\"m-3\">
            ";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 37), "email", [], "any", false, false, false, 37), "html", null, true);
        echo "
        </div>

        <div class=\"m-3\">

            <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_for_curate", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 42), "username", [], "any", false, false, false, 42)]), "html", null, true);
        echo "\" class=\"btn btn-outline-warning\">Pending</a>

            <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 44), "username", [], "any", false, false, false, 44)]), "html", null, true);
        echo "\" class=\"btn btn-outline-success\">Published</a>

            <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_rejected", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 46), "username", [], "any", false, false, false, 46)]), "html", null, true);
        echo "\" class=\"btn btn-outline-danger\">Rejected</a>

        </div>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/author-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 46,  108 => 44,  103 => 42,  95 => 37,  86 => 30,  80 => 26,  74 => 22,  64 => 15,  60 => 13,  58 => 12,  53 => 9,  51 => 8,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/author-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/author-details.html.twig");
    }
}
