<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/_social_icons.html.twig */
class __TwigTemplate_fe8e92b48f705335ae5b9788b0494a3a9ba8487bd20a3b139ab1bc9fa82d2044 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"follow-us-icons\">
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Facebook\">
        <a href=\"//facebook.com/piart.community\" target=\"_blank\">
            <i class=\"fab fa-facebook-square fa-2x \"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Pinterest\">
        <a href=\"//www.pinterest.com/photoimaginart/pins/\" target=\"_blank\">
            <i class=\"fab fa-pinterest-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Twitter\">
        <a href=\"//twitter.com/photoimaginart\" target=\"_blank\">
            <i class=\"fab fa-twitter-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-offset=\"2\" data-toggle=\"tooltip\" title=\"Follow us on Instagram\">
        <a href=\"//instagram.com/photo_imaginart\" target=\"_blank\">
            <i class=\"fab fa-instagram fa-2x\"></i>
        </a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_partials/_social_icons.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/_social_icons.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/_social_icons.html.twig");
    }
}
