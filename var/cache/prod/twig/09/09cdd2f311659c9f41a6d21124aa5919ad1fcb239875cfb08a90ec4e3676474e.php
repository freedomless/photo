<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/forgotten-password.html.twig */
class __TwigTemplate_051a4477109964b25a9017001c1ac8197e0f41265df79e6772607623995ec106 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
            'email_footer_1' => [$this, 'block_email_footer_1'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/forgotten-password.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Reset Your ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " password.";
    }

    // line 5
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "first_name", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "last_name", [], "any", false, false, false, 5), "html", null, true);
    }

    // line 7
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <p>
        Resetting your password is easy. Just press the link below and follow the instructions. We'll have you up and
        running in no time.
    </p>
";
    }

    // line 14
    public function block_email_footer_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    <br>
    ";
        // line 16
        echo twig_include($this->env, $context, "emails/_button.html.twig", ["button_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_forgotten_password_reset", ["token" => twig_get_attribute($this->env, $this->source,         // line 17
($context["payload"] ?? null), "token", [], "any", false, false, false, 17), "email" => twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver_email", [], "any", false, false, false, 17)]), "button_title" => "Reset Password"]);
        // line 19
        echo "
";
    }

    public function getTemplateName()
    {
        return "emails/forgotten-password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 19,  87 => 17,  86 => 16,  83 => 15,  79 => 14,  71 => 8,  67 => 7,  58 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/forgotten-password.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/forgotten-password.html.twig");
    }
}
