<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/video.html.twig */
class __TwigTemplate_5d4a2335563eefedfba371ec1c1152a531a22d68fb30953dac5cc3ea6a6cbeb7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
<div class=\"row\">

    <div class=\"col-12 text-center\">

        <div class=\"video-player\">

            <a href=\"#\">

                <video
                        id=\"my-video\"
                        class=\"video-js vjs-16-9\"
                        controls
                        width=\"640\"
                        height=\"320\"
";
        // line 17
        echo "                        data-setup='{ \"controls\": true, \"autoplay\": \"muted\", \"preload\": \"auto\", \"loop\":true, \"responsive\": true}'
                >
                    <source src=\"/videos/";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "filename", [], "any", false, false, false, 19), "html", null, true);
        echo "\" type=\"video/mp4\" />
                    <source src=\"/videos/";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "filename", [], "any", false, false, false, 20), "html", null, true);
        echo ".webm\" type=\"video/webm\" />
                </video>

                <div class=\"author\"></div>
            </a>

        </div>

        <div class=\"social\">
            <span class=\"photo-of-the-day-title\">Happy Holidays!</span>
        </div>

";
        // line 33
        echo "
        <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "home/video.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 33,  62 => 20,  58 => 19,  54 => 17,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/video.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/video.html.twig");
    }
}
