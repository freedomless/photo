<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/dashboard.html.twig */
class __TwigTemplate_382e06bff407455ddfbb2cdf5f7caf0f5b68e552188605bc4cc723a43859be5c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'page_title' => [$this, 'block_page_title'],
            'content_header' => [$this, 'block_content_header'],
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@EasyAdmin/default/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["_content_title"] = "Admin dashboard";
        // line 1
        $this->parent = $this->loadTemplate("@EasyAdmin/default/layout.html.twig", "admin/easyadmin/dashboard.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_page_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "Dashboard";
    }

    // line 6
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <h3 class=\"title\">Dashboard</h3>
";
    }

    // line 9
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    <div class=\"row\">

    </div>
";
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 10,  68 => 9,  63 => 7,  59 => 6,  55 => 4,  51 => 3,  46 => 1,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/dashboard.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/dashboard.html.twig");
    }
}
