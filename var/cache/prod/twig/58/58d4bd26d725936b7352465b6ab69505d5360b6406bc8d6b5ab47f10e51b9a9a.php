<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/show.html.twig */
class __TwigTemplate_7580b4e76abc0e21a2161b2b046a6bfcf3e55c6de00a8b32c7d3f81a7ae6f91a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["footer"] = false;
        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "post/show.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        (((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", true, true, false, 6) && (1 === twig_compare(twig_length_filter($this->env, twig_trim_filter(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 6))), 0)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 6), "html", null, true))) : (print ("Photo ")));
        echo "
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    ";
        // line 11
        $context["comment"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 11), "query", [], "any", false, false, false, 11), "get", [0 => "comment"], "method", false, false, false, 11);
        // line 12
        echo "    ";
        $context["curate"] = (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 12)])) : (null));
        // line 13
        echo "    ";
        $context["auth"] = ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 13)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13)) : (null));
        // line 14
        echo "
    ";
        // line 15
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("PostSinglePage", ["rendering" => "client_side", "props" => ["post" =>         // line 16
($context["post"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "settings" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "comment" =>         // line 21
($context["comment"] ?? null), "curate" =>         // line 22
($context["curate"] ?? null), "page" =>         // line 23
($context["page"] ?? null), "single" => 1, "category" =>         // line 25
($context["payload"] ?? null), "album" =>         // line 26
($context["payload"] ?? null), "user" =>         // line 27
($context["payload"] ?? null), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "full" => true, "auth" =>         // line 30
($context["auth"] ?? null)]]]);
        // line 31
        echo "
";
    }

    public function getTemplateName()
    {
        return "post/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 31,  87 => 30,  86 => 27,  85 => 26,  84 => 25,  83 => 23,  82 => 22,  81 => 21,  80 => 16,  79 => 15,  76 => 14,  73 => 13,  70 => 12,  68 => 11,  65 => 10,  61 => 9,  54 => 6,  50 => 5,  45 => 3,  43 => 1,  36 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "post/show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/show.html.twig");
    }
}
