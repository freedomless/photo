<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* template.html.twig */
class __TwigTemplate_374ad1eb5c57255529aca3f9f83ae8b36bc2b65c460414750ba745a9b41a210c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'main_before' => [$this, 'block_main_before'],
            'container_class' => [$this, 'block_container_class'],
            'main_header' => [$this, 'block_main_header'],
            'header_center' => [$this, 'block_header_center'],
            'header_left_container' => [$this, 'block_header_left_container'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'row' => [$this, 'block_row'],
            'row_class' => [$this, 'block_row_class'],
            'main_content' => [$this, 'block_main_content'],
            'main_footer' => [$this, 'block_main_footer'],
            'main_after' => [$this, 'block_main_after'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "template.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    ";
        // line 5
        $this->displayBlock('main_before', $context, $blocks);
        // line 6
        echo "
    <div class=\"container basic-layout ";
        // line 7
        $this->displayBlock('container_class', $context, $blocks);
        echo "\">

        ";
        // line 9
        $this->displayBlock('main_header', $context, $blocks);
        // line 58
        echo "
        ";
        // line 59
        $this->displayBlock('row', $context, $blocks);
        // line 68
        echo "
        ";
        // line 69
        $this->displayBlock('main_footer', $context, $blocks);
        // line 70
        echo "
    </div>

    ";
        // line 73
        $this->displayBlock('main_after', $context, $blocks);
        // line 74
        echo "
";
    }

    // line 5
    public function block_main_before($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 7
    public function block_container_class($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_main_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
            <div class=\"row\">

                <div class=\"col-12\">

                    ";
        // line 15
        $this->displayBlock('header_center', $context, $blocks);
        // line 52
        echo "
                </div>

            </div>

        ";
    }

    // line 15
    public function block_header_center($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "
                        <div class=\"row\">

                            <div class=\"col-lg-6 col-md-7\">

                                ";
        // line 21
        $this->displayBlock('header_left_container', $context, $blocks);
        // line 30
        echo "
                            </div>

                            <div class=\"col-lg-6 col-md-5 text-md-right\">

                                <div class=\"row h-100\">

                                    <div class=\"col-sm-12 my-auto\">

                                        ";
        // line 39
        $this->displayBlock('header_right', $context, $blocks);
        // line 40
        echo "
                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr>

                    ";
    }

    // line 21
    public function block_header_left_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "
                                <h1 class=\"float-left\">

                                    <span class=\"align-middle\">";
        // line 25
        $this->displayBlock('header_left', $context, $blocks);
        echo "</span>

                                </h1>

                                ";
    }

    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 39
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 59
    public function block_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "
            <div class=\"row default-form-layout ";
        // line 61
        $this->displayBlock('row_class', $context, $blocks);
        echo "\">

                ";
        // line 63
        $this->displayBlock('main_content', $context, $blocks);
        // line 64
        echo "
            </div>

        ";
    }

    // line 61
    public function block_row_class($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 63
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 69
    public function block_main_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 73
    public function block_main_after($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 73,  236 => 69,  230 => 63,  224 => 61,  217 => 64,  215 => 63,  210 => 61,  207 => 60,  203 => 59,  197 => 39,  183 => 25,  178 => 22,  174 => 21,  159 => 40,  157 => 39,  146 => 30,  144 => 21,  137 => 16,  133 => 15,  124 => 52,  122 => 15,  115 => 10,  111 => 9,  105 => 7,  99 => 5,  94 => 74,  92 => 73,  87 => 70,  85 => 69,  82 => 68,  80 => 59,  77 => 58,  75 => 9,  70 => 7,  67 => 6,  65 => 5,  62 => 4,  58 => 3,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "template.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/template.html.twig");
    }
}
