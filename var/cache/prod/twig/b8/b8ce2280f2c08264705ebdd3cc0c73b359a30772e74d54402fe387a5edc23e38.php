<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* poll/_vote.html.twig */
class __TwigTemplate_64a57bc8d703f0abde750afc71821349886758070c2917437f2ad3aee29d0bed extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid poll-container\">

    <div class=\"row\">

        <div class=\"container\">

            <div class=\"row\">

                <div class=\"col-lg-8 offset-lg-2 col-md-10 offset-md-1 mt-5 mt-sm-2 pt-2 poll-wrapper\">

                    <div class=\"poll-vote p-3 p-md-4 rounded\">

                        <div class=\"d-none d-md-inline\">
                            <h1 class=\"h5\">
                                🙋‍♂️ Hey! We have a new poll. Please vote.
                            </h1>

                            <a href=\"#\" class=\"js-hide-poll close-poll\"
                               data-api-url=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 20
($context["poll"] ?? null), "id", [], "any", false, false, false, 20)]), "html", null, true);
        // line 21
        echo "\">
                                <i class=\"fa fa-times\" title=\"Close\"></i></a>

                            <hr>
                        </div>

                        <div class=\"alert alert-danger\" style=\"display: none\" id=\"js-vote-error\">
                            <strong>Error:</strong> <span class=\"msg\"></span>
                        </div>

                        <h2 class=\"h4\">
                            ";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "title", [], "any", false, false, false, 32), "html", null, true);
        echo "
                        </h2>

                        <a href=\"#\" class=\"js-hide-poll d-md-none d-inline\"
                           data-api-url=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 37
($context["poll"] ?? null), "id", [], "any", false, false, false, 37)]), "html", null, true);
        // line 38
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>

                        <hr class=\"d-block d-md-none\">

                        <p class=\"text-muted poll-desc\">";
        // line 44
        echo twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "description", [], "any", false, false, false, 44);
        echo "</p>

                        <div class=\"mb-3\">
                            <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "link", [], "any", false, false, false, 47), "html", null, true);
        echo "\" target=\"_blank\">
                                <i class=\"fas fa-comment-dots\"></i> Read more (discussion thread)</a>
                        </div>

                        <div class=\"poll-choices\">
                            ";
        // line 52
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["attr" => ["id" => "vote-form", "data-api-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_vote")]]);
        // line 56
        echo "

                            ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "choices", [], "any", false, false, false, 58));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 59
            echo "
                                <div class=\"radio\">
                                    ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["choice"], 'widget');
            echo "
                                    ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["choice"], 'label');
            echo "
                                </div>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "

                            <button class=\"btn btn-primary\" type=\"submit\">Vote!</button>
                            <button class=\"btn btn-link gray-text js-hide-poll\"
                                    data-api-url=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 71
($context["poll"] ?? null), "id", [], "any", false, false, false, 71)]), "html", null, true);
        // line 72
        echo "\">Not now (I will vote later)
                            </button>

                            ";
        // line 75
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "poll/_vote.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 75,  142 => 72,  140 => 71,  139 => 70,  133 => 66,  123 => 62,  119 => 61,  115 => 59,  111 => 58,  107 => 56,  105 => 52,  97 => 47,  91 => 44,  83 => 38,  81 => 37,  80 => 36,  73 => 32,  60 => 21,  58 => 20,  57 => 19,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "poll/_vote.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/poll/_vote.html.twig");
    }
}
