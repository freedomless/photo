<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_sharing.html.twig */
class __TwigTemplate_963b18d8eb724f6d9cbeab107dd96ae7370d6e67b372a33525137f8d40612554 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<span style=\"line-height: 24px; height: 24px\">
    Share it with the world:&nbsp;

    <a href=\"https://www.facebook.com/dialog/share?app_id=2360993147521648&display=popup&href=";
        // line 4
        echo twig_escape_filter($this->env, ($context["share_url"] ?? null), "html", null, true);
        echo "\"><img
                src=\"https://";
        // line 5
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_facebook.png"), "html", null, true);
        echo "\"
                alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://pinterest.com/pin/create/button/?url=";
        // line 8
        echo twig_escape_filter($this->env, ($context["share_url"] ?? null), "html", null, true);
        echo "&media=";
        echo twig_escape_filter($this->env, ($context["media"] ?? null), "html", null, true);
        echo "&description=\">
        <img src=\"https://";
        // line 9
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_pinterest.png"), "html", null, true);
        echo "\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://twitter.com/intent/tweet?url=";
        // line 12
        echo twig_escape_filter($this->env, ($context["share_url"] ?? null), "html", null, true);
        echo "\">
        <img src=\"https://";
        // line 13
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_twitter.png"), "html", null, true);
        echo "\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

</span>
";
    }

    public function getTemplateName()
    {
        return "emails/_sharing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 13,  66 => 12,  59 => 9,  53 => 8,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/_sharing.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_sharing.html.twig");
    }
}
