<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/notification.html.twig */
class __TwigTemplate_925e2d24a72cd114e588c9ab1c91b1cdd991de895476e293f8fa634203cc66cc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "toast"], "method", false, false, false, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "
    ";
            // line 3
            $context["data"] = $this->extensions['App\Twig\JsonDecodeExtension']->decodeJson($context["message"]);
            // line 4
            echo "

    ";
            // line 6
            if ( !(null === ($context["data"] ?? null))) {
                // line 7
                echo "        <script>
            iziToast.show({
                theme: '',
                color: '";
                // line 10
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", true, true, false, 10) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", false, false, false, 10)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", false, false, false, 10), "html", null, true))) : (print ("")));
                echo "',
                icon: '";
                // line 11
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", true, true, false, 11) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", false, false, false, 11)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", false, false, false, 11), "html", null, true))) : (print ("far fa-image")));
                echo "',
                image: '";
                // line 12
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", true, true, false, 12) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", false, false, false, 12)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", false, false, false, 12), "html", null, true))) : (print ("")));
                echo "',
                message: '";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "message", [], "any", false, false, false, 13), "html", null, true);
                echo "',
                position: '";
                // line 14
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", true, true, false, 14) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", false, false, false, 14)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", false, false, false, 14), "html", null, true))) : (print ("bottomLeft")));
                echo "', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });
        </script>
    ";
            }
            // line 19
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "_partials/notification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 19,  73 => 14,  69 => 13,  65 => 12,  61 => 11,  57 => 10,  52 => 7,  50 => 6,  46 => 4,  44 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/notification.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/notification.html.twig");
    }
}
