<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/photo-details.html.twig */
class __TwigTemplate_138dee49c411b7b0f2377d9181738a50728936c33fdccb6519b0458ad443dc92 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"card mt-3\">
    <div class=\"card-header\">
        <strong>Photo Details:</strong>
    </div>
    <div class=\"card-body p-0\">
        <ul class=\"list-group-flush p-0\">
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Date sent:</span>
                ";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "sentForCurateDate", [], "any", false, false, false, 9)), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Title:</span>
                ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 13), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Category:</span>
                ";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "category", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Views:</span>
                ";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "views", [], "any", false, false, false, 21), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Contains nudity:</span>
                ";
        // line 25
        echo ((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "isNude", [], "any", false, false, false, 25)) ? ("Yes") : ("No"));
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Resolution:</span>
                ";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 29), "width", [], "any", false, false, false, 29), "html", null, true);
        echo "px x ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 29), "height", [], "any", false, false, false, 29), "html", null, true);
        echo "px
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Orientation:</span>
                ";
        // line 33
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 33), "orientation", [], "any", false, true, false, 33), "name", [], "any", true, true, false, 33)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 33), "orientation", [], "any", false, true, false, 33), "name", [], "any", false, false, false, 33), "")) : (""))), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Greyscale:</span>
                ";
        // line 37
        echo ((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "isGreyscale", [], "any", false, false, false, 37)) ? ("Yes") : ("No"));
        echo "
            </li>
            <li class=\"list-group-item d-flex flex-wrap w-100 text-center\">
                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "tags", [], "any", false, false, false, 40));
        foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
            // line 41
            echo "                    <span class=\"badge badge-info ml-2 mr-2 mb-2\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 41), "html", null, true);
            echo "</span>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "            </li>


        </ul>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/photo-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 43,  108 => 41,  104 => 40,  98 => 37,  91 => 33,  82 => 29,  75 => 25,  68 => 21,  61 => 17,  54 => 13,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/photo-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/photo-details.html.twig");
    }
}
