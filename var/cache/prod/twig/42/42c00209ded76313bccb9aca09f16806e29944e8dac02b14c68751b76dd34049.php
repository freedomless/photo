<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/meta.html.twig */
class __TwigTemplate_13349a119f7a900db1fb4cf2f5b19aecb481d5bb900b304cfbdcca49bc9359d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["title"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "title", [], "any", false, false, false, 1)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.title")));
        // line 2
        $context["description"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "description", [], "any", false, false, false, 2)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.description")));
        // line 3
        $context["image"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "image", [], "any", false, false, false, 3)) : ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "getSchemeAndHttpHost", [], "method", false, false, false, 3) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(($context["site_social_default_image"] ?? null)))));
        // line 4
        $context["imageWidth"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "imageWidth", [], "any", false, false, false, 4)) : (($context["site_social_default_image_width"] ?? null)));
        // line 5
        $context["imageHeight"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "imageHeight", [], "any", false, false, false, 5)) : (($context["site_social_default_image_height"] ?? null)));
        // line 6
        $context["creator"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "creator", [], "any", false, false, false, 6)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.creator")));
        // line 7
        $context["createdAt"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "createdAt", [], "any", false, false, false, 7)) : ("now"));
        // line 8
        $context["tags"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "tags", [], "any", false, false, false, 8)) : (""));
        // line 9
        echo "
<meta name=\"description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "\"/>
<!-- Schema.org markup for Google -->
<meta itemprop=\"name\" content=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "\">
<meta itemprop=\"description\" content=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["description"] ?? null), "html", null, true);
        echo "\">
<meta itemprop=\"image\" content=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\">

<!-- Twitter Card data -->
<meta name=\"twitter:card\" content=\"summary_large_image\">
<meta name=\"twitter:site\" content=\"@publisher_handle\">
<meta name=\"twitter:title\" content=\"";
        // line 19
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "\">
<meta name=\"twitter:description\" content=\"";
        // line 20
        echo twig_escape_filter($this->env, ($context["description"] ?? null), "html", null, true);
        echo "\">
<meta name=\"twitter:creator\" content=\"";
        // line 21
        echo twig_escape_filter($this->env, ($context["creator"] ?? null), "html", null, true);
        echo "\">
<meta name=\"twitter:image\" content=\"";
        // line 22
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\">

<!-- Open Graph data -->
<meta property=\"og:title\" content=\"";
        // line 25
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"og:type\" content=\"article\"/>
<meta property=\"og:url\" content=\"";
        // line 27
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", true, true, false, 27) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", false, false, false, 27)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", false, false, false, 27)) : (($context["site_url"] ?? null))), "html", null, true);
        echo "\"/>
<meta property=\"og:image\" content=\"";
        // line 28
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"og:image:width\" content=\"";
        // line 29
        echo twig_escape_filter($this->env, ($context["imageWidth"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"og:image:height\" content=\"";
        // line 30
        echo twig_escape_filter($this->env, ($context["imageHeight"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"og:description\" content=\"";
        // line 31
        echo twig_escape_filter($this->env, ($context["description"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"og:site_name\" content=\"";
        // line 32
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"article:published_time\" content=\"";
        // line 33
        echo twig_escape_filter($this->env, ($context["createdAt"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"article:section\" content=\"Article Section\"/>
<meta property=\"article:tag\" content=\"";
        // line 35
        echo twig_escape_filter($this->env, ($context["tags"] ?? null), "html", null, true);
        echo "\"/>
<meta property=\"fb:app_id\" content=\"2360993147521648\"/>

<!-- Canonical -->
<link rel=\"canonical\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", true, true, false, 39) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", false, false, false, 39)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", false, false, false, 39)) : (($context["site_url"] ?? null))), "html", null, true);
        echo "\"/>
<base href=\"/\">

<!-- Chrome, Firefox OS and Opera -->
<meta name=\"theme-color\" content=\"#375a7f\">

<!-- Windows Phone -->
<meta name=\"msapplication-navbutton-color\" content=\"#375a7f\">

<!-- iOS Safari -->
<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"#375a7f\">
";
    }

    public function getTemplateName()
    {
        return "_partials/meta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 39,  129 => 35,  124 => 33,  120 => 32,  116 => 31,  112 => 30,  108 => 29,  104 => 28,  100 => 27,  95 => 25,  89 => 22,  85 => 21,  81 => 20,  77 => 19,  69 => 14,  65 => 13,  61 => 12,  56 => 10,  53 => 9,  51 => 8,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/meta.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/meta.html.twig");
    }
}
