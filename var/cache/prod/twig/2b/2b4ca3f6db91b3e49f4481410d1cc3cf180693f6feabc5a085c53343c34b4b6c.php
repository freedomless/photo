<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/for-sale.html.twig */
class __TwigTemplate_b58ed93a5e3d4c1ac11460eff12ba17c74895ac0e623026dbd6553e5bc78dd77 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "post/for-sale.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <div class=\"container mt-5\">


        <h3 class=\"text-center\">Add photo to store</h3>

        <div class=\"row\">

            <div class=\"col-12 d-flex justify-content-center align-content-center align-items-center flex-column\">

                <div>

                    <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 19), "thumbnail", [], "any", false, false, false, 19), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 19), "html", null, true);
        echo "\">

                </div>

                ";
        // line 23
        if ((0 === twig_compare(($context["canEdit"] ?? null), false))) {
            // line 24
            echo "
                    <div class=\"row\">


                        <div class=\"col-12\">
                            <p class=\"bold p-3 text-center\">

                                You will be able to edit you product
                                on ";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "priceChangeAt", [], "any", false, false, false, 32), "modify", [0 => (("+ " . twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "priceChangeTimespan", [], "any", false, false, false, 32)) . "days")], "method", false, false, false, 32), "d-m-Y H:i"), "html", null, true);
            echo "

                                <br>

                                <strong>Price: <small>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commission", [], "any", false, false, false, 36), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "currency", [], "any", false, false, false, 36), "html", null, true);
            echo "</small></strong>

                            </p>


                        </div>

                    </div>

                ";
        }
        // line 46
        echo "
                ";
        // line 47
        if ((($context["canEdit"] ?? null) || ($context["print"] ?? null))) {
            // line 48
            echo "
                    ";
            // line 49
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
            echo "

                    <div class=\"form-row mt-3\">

                        ";
            // line 53
            if (($context["canEdit"] ?? null)) {
                // line 54
                echo "
                            <div class=\"form-group col-12 p-0\">
                                ";
                // line 56
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", false, false, false, 56), 'label');
                echo "
                                ";
                // line 57
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", false, false, false, 57), 'widget', ["attr" => ["class" => "form-control"]]);
                echo "
                            </div>


                        ";
            }
            // line 62
            echo "
                        ";
            // line 63
            if (($context["print"] ?? null)) {
                // line 64
                echo "
                            <div class=\"form-group col-12 p-0\">
                                ";
                // line 66
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "print", [], "any", false, false, false, 66), 'label');
                echo "
                                ";
                // line 67
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "print", [], "any", false, false, false, 67), 'widget', ["attr" => ["class" => "form-control"]]);
                echo "
                            </div>


                        ";
            }
            // line 72
            echo "

                        <button class=\"btn btn-blue btn-block\">Save</button>


                    </div>
                    ";
            // line 78
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
            echo "


                ";
        }
        // line 82
        echo "
            </div>

        </div>


    </div>
";
    }

    public function getTemplateName()
    {
        return "post/for-sale.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 82,  178 => 78,  170 => 72,  162 => 67,  158 => 66,  154 => 64,  152 => 63,  149 => 62,  141 => 57,  137 => 56,  133 => 54,  131 => 53,  124 => 49,  121 => 48,  119 => 47,  116 => 46,  101 => 36,  94 => 32,  84 => 24,  82 => 23,  73 => 19,  60 => 8,  56 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "post/for-sale.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/for-sale.html.twig");
    }
}
