<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/portfolio.html.twig */
class __TwigTemplate_75d1275556a340b97d71a1697f2d2cf180065b2e0fe23b0484f1268dfe040b05 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/portfolio.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "'s
    Portfolio
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"col-12\">

        ";
        // line 13
        if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "posts", [], "any", false, false, false, 13)), 0))) {
            // line 14
            echo "
            <h3 class=\"mt-5 mb-5 text-center text-muted\">No Photos.</h3>

        ";
        } else {
            // line 18
            echo "
            ";
            // line 19
            $context["stats"] = $this->extensions['App\Twig\AccountStatsExtension']->getAccountStats(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 19), "get", [0 => "slug"], "method", false, false, false, 19));
            // line 20
            echo "
            ";
            // line 21
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 21), "get", [0 => "type"], "method", false, false, false, 21), "single"))) {
                // line 22
                echo "                ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>                 // line 24
($context["nude"] ?? null), "auth" =>                 // line 25
($context["auth"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" => "portfolio", "posts" => twig_get_attribute($this->env, $this->source,                 // line 30
($context["posts"] ?? null), "items", [], "any", false, false, false, 30), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,                 // line 31
($context["posts"] ?? null), "total", [], "any", false, false, false, 31), 0)), "user" => twig_get_attribute($this->env, $this->source,                 // line 32
($context["user"] ?? null), "id", [], "any", false, false, false, 32)]]);
                echo "
            ";
            } else {
                // line 34
                echo "                ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 0, "series" => twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "items", [], "any", false, false, false, 34), "page" => "portfolio", "account" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 34)]]);
                echo "
            ";
            }
            // line 36
            echo "

        ";
        }
        // line 39
        echo "
    </div>

";
    }

    public function getTemplateName()
    {
        return "user/profile/portfolio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 39,  107 => 36,  101 => 34,  96 => 32,  95 => 31,  94 => 30,  93 => 25,  92 => 24,  90 => 22,  88 => 21,  85 => 20,  83 => 19,  80 => 18,  74 => 14,  72 => 13,  67 => 10,  63 => 9,  56 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/portfolio.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/portfolio.html.twig");
    }
}
