<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/product-details.html.twig */
class __TwigTemplate_3ede08bee11e4cf54a58fa869519bd02bab9f64a98b65c16ff58c6d0e4e40656 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "pages/product-details.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product Details";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product Details";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"col-md-12\">

        <p>
            Each artwork you choose will be printed on the latest professional Epson printer model.
            Print will be on the media of your choice with the original Epson inks.
            The printed media will be fixed on Alu-Dibond. (see the pictures bellow for detailed view)
        </p>

        <div class=\"row mt-3\">
            <div class=\"col-md-4\" style=\"min-height: 200px\">
                <div class=\"video-container\">
                    <iframe src=\"https://www.youtube.com/embed/upA_HjZUuyA\" frameborder=\"0\" class=\"video\"
                            allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\"
                            allowfullscreen></iframe>
                </div>
            </div>

            <div class=\"col-md-4\">
                <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_000.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                <img src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_000.jpg"), "html", null, true);
        echo "\"
                     alt=\"Product\"
                     class=\"product-details-image\">
                </a>
            </div>

            <div class=\"col-md-4\">
                <a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_002.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_002.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>
        <hr>
        <div class=\"row mb-3\">
            <div class=\"col-md-4\">
                <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_003.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_003.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_004.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_004.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_005.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_005.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>

        <p>
            Alu-Dibond is a photo medium made from aluminum.
        </p>
        <p>
            Is a high-quality, dimensionally stable image medium which is ideal for showing off professional photography
            exquisitely and durably.
        </p>


        <p>
            On the back of the board there will be an additional aluminum frame with a wall mounting element.
            <br>
            You can choose from 5 media that offer the best gallery quality:
        </p>


        <p>
        <ul>

            <li>
                <strong>Matte</strong> - Epson Enhanced Matte Photo Paper 189g/m². No light glare and reflections.
                <a href=\"https://www.epson.eu/products/consumables/paper/enhanced-matte-paper-24-x-305-m-189gm-c13s041595\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Luster</strong> - Epson Premium Luster Photo Paper 260g/m². Semi gloss surface. Favoured by
                professional
                photographers.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-luster-photo-paper-24-x-305-m-260gm-c13s042081\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Glossy</strong> - Epson Premium Glossy Photo Paper 260g/m². Glossy surface. Very close to real
                photo paper.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-glossy-photo-paper-roll-24-x-305-m-260gm-c13s041638#products\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Smooth</strong> - Epson Fine Art Cotton Smooth 300 g/m². Exceptional colour and
                detail. Archival
                quality.
                Suitable for works with big smooth areas.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-smooth-bright-24-x-15m-c13s450271\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Textured</strong> - Epson Fine Art Cotton Textured 300 g/m². Exceptional colour
                and detail.
                Archival
                quality. Тextured surface for fine art reproduction.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-textured-bright-24-x-15m-c13s450285\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
        </ul>
        </p>


        Each product will have free delivery in Europe with big discounts for buying more than one product!

        </p>

    </div>

";
    }

    public function getTemplateName()
    {
        return "pages/product-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 59,  147 => 58,  139 => 53,  135 => 52,  127 => 47,  123 => 46,  112 => 38,  108 => 37,  98 => 30,  94 => 29,  73 => 10,  69 => 9,  63 => 7,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "pages/product-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/pages/product-details.html.twig");
    }
}
