<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/orders_list.html.twig */
class __TwigTemplate_379059a1292c0fedc919992187f5d33b570575390781c8d13d6909b97791afe5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'item_actions' => [$this, 'block_item_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@EasyAdmin/default/list.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@EasyAdmin/default/list.html.twig", "admin/easyadmin/orders_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_item_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    ";
        // line 5
        $context["_list_item_actions"] = $this->extensions['App\Twig\FilterAdminActionsExtension']->doSomething(($context["_list_item_actions"] ?? null), ($context["item"] ?? null));
        // line 6
        echo "
    ";
        // line 7
        $this->displayParentBlock("item_actions", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/orders_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 7,  55 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/orders_list.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/orders_list.html.twig");
    }
}
