<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vote/rating/index.html.twig */
class __TwigTemplate_878e966f233a9da263df253f2903afea9b9493715e68c39e1daeef03cad8d502 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"d-flex\">

    <form method=\"post\">

        <div class=\"rating\">";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 6), 1));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "<button
                data-toggle=\"tooltip\"
                        formaction=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => $context["i"]]), "html", null, true);
            echo "\"
                        title=\"Rating ";
            // line 9
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">

                    <i class=\"fas fa-star\"></i>
                </button>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "</div>

    </form>

    <div>
        ";
        // line 17
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Favorite", ["rendering" => "client_side", "props" => ["post" => twig_get_attribute($this->env, $this->source,         // line 18
($context["post"] ?? null), "id", [], "any", false, false, false, 18), "favorite" => ($context["favorite"] ?? null)]]);
        echo "
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "vote/rating/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 18,  71 => 17,  64 => 12,  54 => 9,  50 => 8,  43 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "vote/rating/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/vote/rating/index.html.twig");
    }
}
