<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/_form.html.twig */
class __TwigTemplate_85cafc5c609fa16084b41561ad80db4d4baedefd6a749d3c3b56835bd42745d2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

<div class=\"form-group\">
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 4), 'label', ["label" => "Series Decritption:"]);
        echo "
    ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 5), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Series Decritption"]]);
        echo "

    ";
        // line 7
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 7), "vars", [], "any", false, false, false, 7), "errors", [], "any", false, false, false, 7)), 0))) {
            // line 8
            echo "        <div class=\"form-error-list alert alert-danger small mt-2\">
            ";
            // line 9
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 9), 'errors');
            echo "
        </div>
    ";
        }
        // line 12
        echo "</div>

<div class=\"form-group\">
    ";
        // line 15
        if ((((array_key_exists("series", $context)) ? (_twig_default_filter(($context["series"] ?? null))) : ("")) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["series"] ?? null), "cover", [], "any", false, false, false, 15)))) {
            // line 16
            echo "        <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["series"] ?? null), "cover", [], "any", false, false, false, 16), "html", null, true);
            echo "\" alt=\"Cover\" style=\"height: 150px\">

        <hr>
    ";
        }
        // line 20
        echo "
    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "imageCover", [], "any", false, false, false, 21), 'widget', ["attr" => ["class" => "form-control-file", "placeholder" => "Cover", "data-label" => "Cover"]]);
        echo "

    ";
        // line 23
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "imageCover", [], "any", false, false, false, 23), "vars", [], "any", false, false, false, 23), "errors", [], "any", false, false, false, 23)), 0))) {
            // line 24
            echo "        <div class=\"form-error-list alert alert-danger small mt-2\">
            ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "imageCover", [], "any", false, false, false, 25), 'errors');
            echo "
        </div>
    ";
        }
        // line 28
        echo "</div>

<div class=\"form-group\">
    <button type=\"submit\" class=\"btn btn-primary\">";
        // line 31
        echo twig_escape_filter($this->env, ($context["button_title"] ?? null), "html", null, true);
        echo "</button>
</div>

";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/selection/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 34,  102 => 31,  97 => 28,  91 => 25,  88 => 24,  86 => 23,  81 => 21,  78 => 20,  70 => 16,  68 => 15,  63 => 12,  57 => 9,  54 => 8,  52 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/selection/_form.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/_form.html.twig");
    }
}
