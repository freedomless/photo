<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/orders.html.twig */
class __TwigTemplate_0cee6dd1be5c5ad9fddf9cd2bc3e2bc439b301d6fdb3ffbd6c05e943f2467f48 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/orders.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"container basic-layout\">

        <h3>Orders</h3>

        <div class=\"table-responsive\">
            <table class=\"table shadow rounded \">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Size</th>
                        <th>Media</th>
                        <th>
                            Order Id
                        </th>
                        <th>
                            Transaction Status
                        </th>
                        <th>
                            Track ID
                        </th>
                        <th>Dispatched</th>
                        <th>Invoice</th>
                        <th>
                            Total Amount
                        </th>
                    </tr>
                </thead>
                <tbody>
                    ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["orders"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 33
            echo "                        <tr>
                            <td>
                                ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 35));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 36
                echo "                                    ";
                if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 36), "post", [], "any", false, false, false, 36), "type", [], "any", false, false, false, 36), 1))) {
                    // line 37
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 37), "post", [], "any", false, false, false, 37), "children", [], "any", false, false, false, 37));
                    foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                        // line 38
                        echo "                                            <img src=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 38), "thumbnail", [], "any", false, false, false, 38), "html", null, true);
                        echo "\" alt=\"Item\" class=\"rounded mb-1\"
                                                 width=\"100px\" height=\"100px\" style=\"object-fit: cover\">
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 41
                    echo "
                                    ";
                } else {
                    // line 43
                    echo "                                        <img src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 43), "post", [], "any", false, false, false, 43), "image", [], "any", false, false, false, 43), "thumbnail", [], "any", false, false, false, 43), "html", null, true);
                    echo "\" alt=\"Item\" class=\"rounded\"
                                             width=\"100px\" height=\"100px\" style=\"object-fit: cover\"
                                    ";
                }
                // line 46
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                            </td>
                            <td>
                                ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 49));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 50
                echo "
                                    ";
                // line 51
                if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 51), "post", [], "any", false, false, false, 51), "type", [], "any", false, false, false, 51), 1))) {
                    // line 52
                    echo "                                      <div>
                                          ";
                    // line 53
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 53), "width", [], "any", false, false, false, 53), "html", null, true);
                    echo "x";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 53), "height", [], "any", false, false, false, 53), "html", null, true);
                    echo "
                                      </div>
                                    ";
                } else {
                    // line 56
                    echo "                                        <div>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 56), "width", [], "any", false, false, false, 56), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, twig_round((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 56), "post", [], "any", false, false, false, 56), "opposite", [], "any", false, false, false, 56) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 56), "width", [], "any", false, false, false, 56))), "html", null, true);
                    echo "</div>
                                    ";
                }
                // line 58
                echo "
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                            </td>
                            <td>
                                ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 62));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 63
                echo "
                                   <div>";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "material", [], "any", false, false, false, 64), "name", [], "any", false, false, false, 64), "html", null, true);
                echo "</div>

                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "                            </td>
                            <td>";
            // line 68
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["order"], "token", [], "any", false, false, false, 68), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 70
            $context["status"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "transaction", [], "any", false, false, false, 70), "status", [], "any", false, false, false, 70);
            // line 71
            echo "                                ";
            if ((0 === twig_compare(($context["status"] ?? null), 1))) {
                // line 72
                echo "                                    <span class=\"text-success\"> Paid</span>
                                ";
            } elseif ((0 === twig_compare(            // line 73
($context["status"] ?? null), 0))) {
                // line 74
                echo "                                    Pending
                                ";
            } elseif ((0 === twig_compare(            // line 75
($context["status"] ?? null),  -1))) {
                // line 76
                echo "                                    Failed
                                ";
            } elseif ((0 === twig_compare(            // line 77
($context["status"] ?? null),  -2))) {
                // line 78
                echo "                                    Canceled
                                ";
            }
            // line 80
            echo "                            </td>
                            <td>";
            // line 81
            ((twig_get_attribute($this->env, $this->source, $context["order"], "trackId", [], "any", false, false, false, 81)) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["order"], "trackId", [], "any", false, false, false, 81), "html", null, true))) : (print ("Not added")));
            echo "</td>
                            <td>";
            // line 82
            echo ((twig_get_attribute($this->env, $this->source, $context["order"], "dispatched", [], "any", false, false, false, 82)) ? ("Yes") : ("No"));
            echo "</td>
                            <td>
                                ";
            // line 84
            if ((0 === twig_compare(($context["status"] ?? null), 1))) {
                // line 85
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_invoice", ["token" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "invoice", [], "any", false, false, false, 85), "token", [], "any", false, false, false, 85)]), "html", null, true);
                echo "\" target=\"_blank\">View</a>
                                ";
            }
            // line 87
            echo "                            </td>
                            <td>
                                ";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "transaction", [], "any", false, false, false, 89), "paymentGross", [], "any", false, false, false, 89), "html", null, true);
            echo "
                                &euro;
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "
                    ";
        // line 95
        if ((0 === twig_compare(twig_length_filter($this->env, ($context["orders"] ?? null)), 0))) {
            // line 96
            echo "                        <h1 class=\"mt-5 pt-5\">
                            No orders available
                        </h1>
                    ";
        }
        // line 100
        echo "                </tbody>
            </table>
        </div>

        <div class=\"d-flex justify-content-center\">
            ";
        // line 105
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["orders"] ?? null));
        echo "
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "user/profile/orders.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 105,  264 => 100,  258 => 96,  256 => 95,  253 => 94,  242 => 89,  238 => 87,  232 => 85,  230 => 84,  225 => 82,  221 => 81,  218 => 80,  214 => 78,  212 => 77,  209 => 76,  207 => 75,  204 => 74,  202 => 73,  199 => 72,  196 => 71,  194 => 70,  189 => 68,  186 => 67,  177 => 64,  174 => 63,  170 => 62,  166 => 60,  159 => 58,  151 => 56,  143 => 53,  140 => 52,  138 => 51,  135 => 50,  131 => 49,  127 => 47,  121 => 46,  114 => 43,  110 => 41,  100 => 38,  95 => 37,  92 => 36,  88 => 35,  84 => 33,  80 => 32,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/orders.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/orders.html.twig");
    }
}
