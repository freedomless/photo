<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_post_status.html.twig */
class __TwigTemplate_020a89297288602fd467633b05d0c1ecaaa713f1817f9aa80ebee927b597a2b4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((0 === twig_compare(($context["value"] ?? null), 0))) {
            // line 2
            echo "    <span class=\"text-info\">New</span>
";
        } elseif ((0 === twig_compare(        // line 3
($context["value"] ?? null), 1))) {
            // line 4
            echo "    <span class=\"text-warning\">For Curate</span>
";
        } elseif ((0 === twig_compare(        // line 5
($context["value"] ?? null), 2))) {
            // line 6
            echo "    <span class=\"text-danger\">Rejected</span>
";
        } elseif ((0 === twig_compare(        // line 7
($context["value"] ?? null), 3))) {
            // line 8
            echo "    <span class=\"text-success\">Published</span>
";
        } elseif ((0 === twig_compare(        // line 9
($context["value"] ?? null), 4))) {
            // line 10
            echo "    <span class=\"text-primary\">For Sale</span>
";
        }
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_post_status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 10,  57 => 9,  54 => 8,  52 => 7,  49 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/_post_status.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_post_status.html.twig");
    }
}
