<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/selection_series.html.twig */
class __TwigTemplate_30404c53714be5bb6448b3068869465468832c5eaebf9a9ce7dfe9b2e5a43ece extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("selection_series", $context) &&  !(null === ($context["selection_series"] ?? null)))) {
            // line 2
            echo "
    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_series_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["selection_series"] ?? null), "post", [], "any", false, false, false, 9), "id", [], "any", false, false, false, 9)]), "html", null, true);
            echo "\">

                    <img src=\"";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["selection_series"] ?? null), "cover", [], "any", false, false, false, 11), "html", null, true);
            echo "\"
                         alt=\"Selection Series\">

                    <div class=\"author\"></div>
                </a>

            </div>

            <div class=\"social\">
                <span class=\"photo-of-the-day-title\">PiART Selection Series</span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-selection-series-dropdown\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share it!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-selection-series-dropdown\">

                        ";
            // line 31
            echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 33
($context["selection_series"] ?? null), "id", [], "any", false, false, false, 33)]), "classes" => "pr-3 pl-3"]);
            // line 36
            echo "

                        ";
            // line 38
            echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 40
($context["selection_series"] ?? null), "id", [], "any", false, false, false, 40)]), "media" => twig_get_attribute($this->env, $this->source,             // line 42
($context["selection_series"] ?? null), "cover", [], "any", false, false, false, 42), "description" => "Curators Series", "classes" => "pr-2"]);
            // line 45
            echo "

                        ";
            // line 47
            echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 49
($context["selection_series"] ?? null), "id", [], "any", false, false, false, 49)])]);
            // line 51
            echo "
                    </div>

                </span>

            </div>

            ";
            // line 58
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["selection_series"] ?? null), "post", [], "any", false, true, false, 58), "description", [], "any", true, true, false, 58)) {
                // line 59
                echo "                <p class=\"quote text-muted mt-2\">
                    ";
                // line 60
                echo twig_get_attribute($this->env, $this->source, ($context["selection_series"] ?? null), "title", [], "any", false, false, false, 60);
                echo "
                </p>
            ";
            }
            // line 63
            echo "
            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

        </div>

    </div>

";
        }
    }

    public function getTemplateName()
    {
        return "home/selection_series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 63,  108 => 60,  105 => 59,  103 => 58,  94 => 51,  92 => 49,  91 => 47,  87 => 45,  85 => 42,  84 => 40,  83 => 38,  79 => 36,  77 => 33,  76 => 31,  53 => 11,  48 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/selection_series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/selection_series.html.twig");
    }
}
