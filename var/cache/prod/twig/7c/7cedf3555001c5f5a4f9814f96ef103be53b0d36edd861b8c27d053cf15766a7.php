<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/right.html.twig */
class __TwigTemplate_53c4781c468874e5bc944ada5b67571c5e6826533feabaad12a22f0a9abd1737 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"main-nav-links navbar-nav mr-lg-3 pr-lg-1 right-nav\">

    <li class=\"nav-item ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery", "_route"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_series", "_route"), "html", null, true);
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 4
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery", ["page" => "latest"]);
        echo "\">Gallery</a>
    </li>

    ";
        // line 8
        echo "    <li class=\"nav-item dropdown ";
        if ((is_string($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 8), "attributes", [], "any", false, false, false, 8), "get", [0 => "_route"], "method", false, false, false, 8)) && is_string($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = "web_photo_create") && ('' === $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 || 0 === strpos($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4, $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144)))) {
            echo "active";
        }
        echo "\">

        <a class=\"nav-link dropdown-toggle ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_photo_create", "_route"), "html", null, true);
        echo "\"
           href=\"#\" id=\"account\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            Upload
        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-left\"
             aria-labelledby=\"account\">
            <a class=\"dropdown-item\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create");
        echo "\">Photo</a>
            <a class=\"dropdown-item\" href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create", ["type" => "series"]);
        echo "\">Series</a>
        </div>

    </li>

    <li class=\"nav-item ";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_vote", "_route"), "html", null, true);
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote");
        echo "\">Vote</a>
    </li>

    <li class=\"nav-item ";
        // line 28
        if ((is_string($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 28), "attributes", [], "any", false, false, false, 28), "get", [0 => "_route"], "method", false, false, false, 28)) && is_string($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = "web_members") && ('' === $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 || 0 === strpos($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b, $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002)))) {
            echo "active";
        }
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members");
        echo "\">Members</a>
    </li>

    <li class=\"nav-item ";
        // line 32
        if ((is_string($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 32), "attributes", [], "any", false, false, false, 32), "get", [0 => "_route"], "method", false, false, false, 32)) && is_string($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = "web_forum") && ('' === $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 || 0 === strpos($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4, $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666)))) {
            echo "active";
        }
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum");
        echo "\">Forums</a>
    </li>

    ";
        // line 36
        if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 36)) {
            // line 37
            echo "        <li class=\"nav-item ";
            if ((is_string($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 37), "attributes", [], "any", false, false, false, 37), "get", [0 => "_route"], "method", false, false, false, 37)) && is_string($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = "web_store") && ('' === $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 || 0 === strpos($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e, $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52)))) {
                echo "active";
            }
            echo "\">
            <a class=\"nav-link print-store-label\" href=\"";
            // line 38
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store");
            echo "\">Art Prints</a>
        </li>
    ";
        }
        // line 41
        echo "
    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\"
           href=\"#\" id=\"more\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <span class=\"d-none d-md-block\"><span class=\"navbar-toggler-icon\"></span></span>
            <span class=\"d-md-none\">More</span>
        </a>

        ";
        // line 52
        echo "        <div class=\"dropdown-menu shadow border-0 dropdown-menu-more\">

            <a class=\"dropdown-item\" href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_tos");
        echo "\">Terms of Service</a>

            <a class=\"dropdown-item\" href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_privacy_policy");
        echo "\">Privacy Policy</a>

            <a class=\"dropdown-item\" href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_user_agreement");
        echo "\">User Agreement</a>

            <a class=\"dropdown-item\" href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_faq");
        echo "\">F.A.Q</a>

            <a class=\"dropdown-item\" href=\"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
        echo "\">Contact Us</a>

        </div>

    </li>

    <li class=\"divider-vertical\"></li>


    ";
        // line 72
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 73
            echo "
        ";
            // line 74
            $this->loadTemplate("_partials/navigation/user.html.twig", "_partials/navigation/right.html.twig", 74)->display($context);
            // line 75
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\PostSettingsExtension']->getUserSettings(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 75)), "is_web_notification_allowed", [], "any", false, false, false, 75)) {
                // line 76
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Notifications", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\NotificationsCountExtension']->getUnseenNotifications(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 76))]]);
                echo "
        ";
            }
            // line 78
            echo "
        ";
            // line 79
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 79)) {
                // line 80
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("CartButton", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\CartCountExtension']->getCartCount(), "href" => "/cart"]]);
                echo "
        ";
            }
            // line 82
            echo "
        ";
            // line 84
            echo "        ";
            $this->loadTemplate("_partials/navigation/admin.html.twig", "_partials/navigation/right.html.twig", 84)->display($context);
            // line 85
            echo "
    ";
        } else {
            // line 87
            echo "
        ";
            // line 88
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 88)) {
                // line 89
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("CartButton", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\CartCountExtension']->getCartCount(), "href" => "/cart"]]);
                echo "
        ";
            }
            // line 91
            echo "
        ";
            // line 93
            echo "        <li class=\"nav-item\">
            <a href=\"";
            // line 94
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
            echo "\" class=\"nav-link text-white\">Log in</a>
        </li>

        <li class=\"nav-item\">
            <a href=\"";
            // line 98
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_register");
            echo "\" class=\"nav-link text-white\">Register</a>
        </li>

    ";
        }
        // line 102
        echo "
    ";
        // line 104
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 105
            echo "
        <li class=\"nav-item\">

            ";
            // line 109
            echo "            ";
            // line 110
            echo "            ";
            // line 111
            echo "
            ";
            // line 113
            echo "
            ";
            // line 115
            echo "
            <a href=\"";
            // line 116
            echo twig_escape_filter($this->env, ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 116), "attributes", [], "any", false, false, false, 116), "get", [0 => "_route"], "method", false, false, false, 116), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 117
($context["app"] ?? null), "request", [], "any", false, false, false, 117), "attributes", [], "any", false, false, false, 117), "get", [0 => "_route_params"], "method", false, false, false, 117)) . "?_switch_user=_exit"), "html", null, true);
            echo "\" class=\"nav-link\"
               data-toggle=\"tooltip\"
               data-placement=\"bottom\"
               title=\"Exit from switch\">

                <i class=\"fas fa-user-minus\"></i>

            </a>

        </li>

    ";
        }
        // line 129
        echo "
</ul>
";
    }

    public function getTemplateName()
    {
        return "_partials/navigation/right.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 129,  275 => 117,  274 => 116,  271 => 115,  268 => 113,  265 => 111,  263 => 110,  261 => 109,  256 => 105,  253 => 104,  250 => 102,  243 => 98,  236 => 94,  233 => 93,  230 => 91,  224 => 89,  222 => 88,  219 => 87,  215 => 85,  212 => 84,  209 => 82,  203 => 80,  201 => 79,  198 => 78,  192 => 76,  189 => 75,  187 => 74,  184 => 73,  181 => 72,  169 => 62,  164 => 60,  159 => 58,  154 => 56,  149 => 54,  145 => 52,  133 => 41,  127 => 38,  120 => 37,  118 => 36,  112 => 33,  106 => 32,  100 => 29,  94 => 28,  88 => 25,  84 => 24,  76 => 19,  72 => 18,  61 => 10,  53 => 8,  47 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/navigation/right.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/right.html.twig");
    }
}
