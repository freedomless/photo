<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload-series.html.twig */
class __TwigTemplate_54c33b8a08ee6bae358d94f9ba0ca62ad5b5f2759ab8c970c0fea1aa0d9903b3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload-series.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Upload Series";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 7), "isVerified", [], "any", false, false, false, 7), false))) {
            // line 8
            echo "        ";
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/upload-series.html.twig", 8)->display($context);
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    ";
            // line 16
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
            echo "

                    <hr>

                    <h3 class=\"d-flex\">
                        <span class=\"flex-grow-1\">";
            // line 21
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "method", [], "any", false, false, false, 21), "POST"))) ? ("Create") : ("Update"));
            echo " Series</span>

                        ";
            // line 23
            if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "method", [], "any", false, false, false, 23), "PUT")) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 23), "vars", [], "any", true, true, false, 23))) {
                // line 24
                echo "                            <button class=\"btn btn-blue curator-warning-btn\"
                                    data-toggle=\"tooltip\"
                                    data-placement=\"bottom\"
                                    data-title=\"Send to Curators\"
                                    type=\"button\"
                                    data-url=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_curate", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 29), "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 29), "get", [0 => "page"], "method", false, false, false, 29)]), "html", null, true);
                echo "\">
                                Send For Curate
                            </button>

                        ";
            }
            // line 34
            echo "
                    </h3>

                    <hr>

                    ";
            // line 39
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "

                    <div class=\"row mb-5\">

                        <div class=\"col-12\">

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 47
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 47), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 48
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 48), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 54
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 55
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 55), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 61), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 62), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 68
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 68), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 69
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 69), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"row pl-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-md-9\">

                                    ";
            // line 79
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 79), 'label');
            echo "
                                    ";
            // line 80
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 80), 'widget');
            echo "

                                    ";
            // line 82
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 82), 'label');
            echo "
                                    ";
            // line 83
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 83), 'widget');
            echo "

                                </div>

                            </div>

                        </div>

                    </div>

                    ";
            // line 93
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 93), "vars", [], "any", true, true, false, 93)) {
                // line 94
                echo "
                        <h3 class=\"text-muted\">
                            Photos: (<span class=\"counter\">";
                // line 96
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 96)), "html", null, true);
                echo "</span>/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 96), "html", null, true);
                echo ")
                            <small class=\"float-right text-danger ml-5 errors-min d-none\">You must add at least <span class=\"counter\">";
                // line 97
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 97)), "html", null, true);
                echo "</span> photos!</small>
                        </h3>

                        <hr>

                        <div class=\"row series-list mb-5\"
                             data-prototype=\"";
                // line 103
                echo twig_call_macro($macros["_self"], "macro_image", [], 103, $context, $this->getSourceContext());
                echo "\"
                             data-widget-counter=\"";
                // line 104
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 104)), "html", null, true);
                echo "\">

                            ";
                // line 106
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 106));
                foreach ($context['_seq'] as $context["index"] => $context["child"]) {
                    // line 107
                    echo "
                                <div class=\"col-md-3 mb-3\">

                                    ";
                    // line 110
                    if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 110), "method", [], "any", false, false, false, 110), "POST")) || (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 110), "value", [], "any", false, false, false, 110)))) {
                        // line 111
                        echo "
                                        <div class=\"d-flex justify-content-center align-items-center\">

                                            ";
                        // line 114
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 114), "file", [], "any", false, false, false, 114), 'widget', ["attr" => ["class" => "auto", "data-label" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 114), "file", [], "any", false, false, false, 114), "vars", [], "any", false, false, false, 114), "label", [], "any", false, false, false, 114)]]);
                        echo "

                                        </div>


                                        ";
                        // line 119
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 119), 'widget', ["attr" => ["placeholder" => "Title"]]);
                        echo "

                                    ";
                    } else {
                        // line 122
                        echo "
                                        <div class=\"text-center mb-3 d-flex justify-content-center align-items-center\"
                                             style=\"height: 200px\">

                                            <a href=\"";
                        // line 126
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 126), "value", [], "any", false, false, false, 126), "id", [], "any", false, false, false, 126)]), "html", null, true);
                        echo "\">
                                                <img src=\"";
                        // line 127
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 127), "value", [], "any", false, false, false, 127), "image", [], "any", false, false, false, 127), "thumbnail", [], "any", false, false, false, 127), "html", null, true);
                        echo "\"
                                                     alt=\"";
                        // line 128
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 128), "value", [], "any", false, false, false, 128), "title", [], "any", false, false, false, 128), "html", null, true);
                        echo "\"
                                                     style=\"max-width: 100%;max-height: 200px\"
                                                />
                                            </a>

                                        </div>

                                        <div class=\"d-flex\">

                                            ";
                        // line 137
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 137), 'widget', ["attr" => ["placeholder" => "Title"]]);
                        echo "

                                            ";
                        // line 139
                        if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 0) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "cover", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 139), "value", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139))))) {
                            // line 140
                            echo "
                                                <button class=\"btn ml-2\"
                                                        data-toggle=\"tooltip\"
                                                        title=\"Set as cover\"
                                                        formaction=\"";
                            // line 144
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_set_cover", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 144), "value", [], "any", false, false, false, 144), "id", [], "any", false, false, false, 144)]), "html", null, true);
                            echo "\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"far fa-file-image\"></i>

                                                </button>

                                            ";
                        }
                        // line 152
                        echo "
                                            ";
                        // line 153
                        if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 0) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "cover", [], "any", false, false, false, 153), "id", [], "any", false, false, false, 153), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 153), "value", [], "any", false, false, false, 153), "id", [], "any", false, false, false, 153)))) && (1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 153)), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "min", [], "any", false, false, false, 153))))) {
                            // line 154
                            echo "                                                <button class=\"btn text-danger ml-2 delete-warning-btn\"
                                                        data-toggle=\"tooltip\"
                                                        type=\"button\"
                                                        title=\"Remove from series\"
                                                        data-url=\"";
                            // line 158
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 158), "value", [], "any", false, false, false, 158), "id", [], "any", false, false, false, 158)]), "html", null, true);
                            echo "\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"fa fa-trash-alt\"></i>

                                                </button>
                                            ";
                        }
                        // line 165
                        echo "                                        </div>

                                    ";
                    }
                    // line 168
                    echo "
                                </div>

                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['index'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 172
                echo "
                            ";
                // line 173
                if ((-1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 173)), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 173)))) {
                    // line 174
                    echo "                                <div class=\"col-md-3 mb-3 series-add-button-wrapper\">

                                    <button type=\"button\" class=\"series-add-button shadow\">
                                        <i class=\"fas fa-plus\"></i>
                                    </button>

                                </div>

                            ";
                }
                // line 183
                echo "
                        </div>

                    ";
            } else {
                // line 187
                echo "
                        <div class=\"row\">

                            ";
                // line 190
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 190));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 191
                    echo "
                                <div class=\"col-md-3 mb-3\">

                                    <img src=\"";
                    // line 194
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 194), "thumbnail", [], "any", false, false, false, 194), "html", null, true);
                    echo "\" alt=\"\" height=\"200px\" width=\"100%\"
                                         style=\"object-fit: cover\">

                                </div>

                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 200
                echo "
                        </div>

                    ";
            }
            // line 204
            echo "
                    <div class=\"text-center\">

                        ";
            // line 207
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "upload", [], "any", false, false, false, 207), 'widget', ["label" => (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 207), "method", [], "any", false, false, false, 207), "POST"))) ? ("Create") : ("Update"))]);
            echo "

                    </div>

                    ";
            // line 211
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
            echo "
                </div>

            </div>

        </div>
    ";
        }
        // line 218
        echo "

";
    }

    // line 222
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 223
        echo "
    <script>

        \$.min = '";
        // line 226
        (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 226), "method", [], "any", false, false, false, 226), "POST"))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "min", [], "any", false, false, false, 226), "html", null, true))) : (print (0)));
        echo "'

        jQuery(document).ready(function () {

            \$('.custom-file').dropzone({form: \$('form')})

            let list = jQuery('.series-list');
            // Try to find the counter of the list or use the length of the list
            let counter = list.data('widget-counter') || list.children().length;

            let \$series = \$('.series-add-button-wrapper');
            let \$counter = \$('.counter');

            jQuery('.series-add-button').click(function (e) {
                // grab the prototype template
                var newWidget = list.attr('data-prototype');
                // replace the \"__name__\" used in the id and name of the prototype
                // with a number that's unique to your emails
                // end name attribute looks like name=\"contact[emails][2]\"
                newWidget = newWidget.replace(/__name__/g, counter);
                // Increase the counter
                counter++;
                // And store it, the length cannot be used if deleting widgets is allowed
                list.data('widget-counter', counter);

                // create a new list element and add it to the list
                var newElem = jQuery(newWidget);

                var remove = \$('<button class=\"remove-series\"><i class=\"fas fa-times\"></i></button>')

                newElem.prepend(remove)

                \$counter.text(counter);

                remove.on('click', function () {
                    newElem.remove()
                    counter--;
                    \$counter.text(counter);
                    list.data('widget-counter', counter);

                    let event = new CustomEvent('series-child-removed')
                    document.dispatchEvent(event);

                    --\$.min

                    if (counter < '";
        // line 271
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 271), "html", null, true);
        echo "') {
                        \$series.show()
                    }
                })

                \$series.before(newElem)

                ++\$.min

                newElem.find('.custom-file').dropzone({form: \$('form')});

                if (counter == '";
        // line 282
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 282), "html", null, true);
        echo "') {
                    \$series.hide()
                }

            });
        });

    </script>

    ";
        // line 291
        $macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"] = $this->loadTemplate("_partials/warning.html.twig", "photo/upload-series.html.twig", 291)->unwrap();
        // line 292
        echo "
    ";
        // line 293
        echo twig_call_macro($macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"], "macro_warning", ["delete-warning-modal", "delete-warning-form", "delete-warning-btn", "Confirm!", "Are you sure you want delete this photo?"], 293, $context, $this->getSourceContext());
        echo "

    ";
        // line 295
        echo twig_call_macro($macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"], "macro_warning", ["curator-warning-modal", "curator-warning-form", "curator-warning-btn", "Confirm!", "Are you sure you want to send this photo to curator?"], 295, $context, $this->getSourceContext());
        echo "

";
    }

    // line 299
    public function macro_image(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 300
            echo "    <div class='col-md-3 mb-3'>
        <div class='d-flex justify-content-center align-items-center'>
            <div class='custom-file'>
                <input type='file'
                       id='post_children___name___image_file'
                       name='post[children][__name__][image][file]'
                       class='auto custom-file-input'
                       data-label='Click or Drag & Drop File'
                       accept='image/jpeg,image/png'>
                <label for='post_children___name___image_file' class='custom-file-label'></label>
            </div>
        </div>
        <input type='text' id='post_children___name___title' name='post[children][__name__][title]' placeholder='Title'
               class='form-control'></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "photo/upload-series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  548 => 300,  536 => 299,  529 => 295,  524 => 293,  521 => 292,  519 => 291,  507 => 282,  493 => 271,  445 => 226,  440 => 223,  436 => 222,  430 => 218,  420 => 211,  413 => 207,  408 => 204,  402 => 200,  390 => 194,  385 => 191,  381 => 190,  376 => 187,  370 => 183,  359 => 174,  357 => 173,  354 => 172,  345 => 168,  340 => 165,  330 => 158,  324 => 154,  322 => 153,  319 => 152,  308 => 144,  302 => 140,  300 => 139,  295 => 137,  283 => 128,  279 => 127,  275 => 126,  269 => 122,  263 => 119,  255 => 114,  250 => 111,  248 => 110,  243 => 107,  239 => 106,  234 => 104,  230 => 103,  221 => 97,  215 => 96,  211 => 94,  209 => 93,  196 => 83,  192 => 82,  187 => 80,  183 => 79,  170 => 69,  166 => 68,  157 => 62,  153 => 61,  144 => 55,  140 => 54,  131 => 48,  127 => 47,  116 => 39,  109 => 34,  101 => 29,  94 => 24,  92 => 23,  87 => 21,  79 => 16,  71 => 10,  68 => 9,  65 => 8,  63 => 7,  60 => 6,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/upload-series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload-series.html.twig");
    }
}
