<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_base.html.twig */
class __TwigTemplate_6d08fd550aa6ec9c5e938c6dea15b8ec3295f30ae13f534222c344da82645ec7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_logo_text' => [$this, 'block_email_logo_text'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
            'email_poster' => [$this, 'block_email_poster'],
            'email_footer_1' => [$this, 'block_email_footer_1'],
            'email_footer_2' => [$this, 'block_email_footer_2'],
            'email_footer_3' => [$this, 'block_email_footer_3'],
            'site_email_address' => [$this, 'block_site_email_address'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"
      style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #f1f1f1;margin: 0 auto !important;padding: 0 !important;height: 100% !important;width: 100% !important;\">
<head style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
    <meta charset=\"utf-8\" style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
    <meta name=\"viewport\" content=\"width=device-width\"
          style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"
          style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
    <meta name=\"x-apple-disable-message-reformatting\"
          style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
    <meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" />
    <title style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">";
        // line 13
        $this->displayBlock('email_title', $context, $blocks);
        echo "</title>
    <link href=\"https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700&display=swap&subset=cyrillic\"
          rel=\"stylesheet\">

</head>

<body width=\"100%\"
      style=\"margin: 0 auto !important;padding: 0 !important;mso-line-height-rule: exactly;background-color: transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #f1f1f1;font-family: 'Roboto', sans-serif;font-weight: 400;font-size: 15px;line-height: 1.8;color: rgba(0, 0, 0, .4);height: 100% !important;width: 100% !important;\">
<div style=\"display: none;font-size: 1px;max-height: 0px;max-width: 0px;opacity: 0;overflow: hidden;mso-hide: all;font-family: sans-serif;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
</div>
<div style=\"max-width: 600px;margin: 0 auto;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"
     class=\"email-container\">
    <!-- BEGIN BODY -->
    <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"
           style=\"margin: auto;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;\">
        <tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
            <td class=\"bg_white logo\"
                style=\"padding: 1em 2.5em;text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #ffffff;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;\">
                <h1 style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Roboto', serif;color: #000000;margin-top: 0;margin: 0;\">
                    <a href=\"#\"
                       style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #000;font-size: 20px;font-weight: 700;text-transform: uppercase;font-family: 'Roboto', sans-serif;\">";
        // line 33
        $this->displayBlock('email_logo_text', $context, $blocks);
        echo "</a>
                </h1>
            </td>
        </tr>

        <tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
            <td class=\"bg_white\"
                style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #ffffff;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;\">
                <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"
                       style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;\">

                    <tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
                        <td class=\"bg_light email-section\"
                            style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #fafafa;padding: 2.5em;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;\">
                            <table style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;\">
                                <tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
                                    <td style=\"color: #000000;font-size: 18px;padding-top: 10px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;\">
                                        <h2 style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;margin-top: 0;\">
                                            Dear, ";
        // line 51
        $this->displayBlock('email_user', $context, $blocks);
        echo "</h2>
                                        <p style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;\">";
        // line 52
        $this->displayBlock('email_content', $context, $blocks);
        echo "</p>

                                        ";
        // line 54
        $this->displayBlock('email_poster', $context, $blocks);
        // line 55
        echo "
                                        ";
        // line 56
        $this->displayBlock('email_footer_1', $context, $blocks);
        // line 57
        echo "                                        ";
        $this->displayBlock('email_footer_2', $context, $blocks);
        // line 58
        echo "                                        ";
        $this->displayBlock('email_footer_3', $context, $blocks);
        // line 59
        echo "                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">
                        <td class=\"bg_white email-section\"
                            style=\"font-size: 18px;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background: #ffffff;padding: 2.5em;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;\">
                            <div style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">Kind Regards,<br
                                        style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">";
        // line 69
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                                Team
                            </div>

                            ";
        // line 73
        echo twig_include($this->env, $context, "emails/_social.html.twig");
        echo "

                            <br>
                            <small style=\"opacity: .7;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">";
        // line 76
        $this->displayBlock('site_email_address', $context, $blocks);
        // line 77
        echo "</small>

                        </td>
                    </tr>

                </table>

            </td>
        </tr>
    </table>

</div>
</body>
</html>";
    }

    // line 13
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 33
    public function block_email_logo_text($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PHOTOIMAGINART";
    }

    // line 51
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " Member";
    }

    // line 52
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 54
    public function block_email_poster($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 56
    public function block_email_footer_1($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 57
    public function block_email_footer_2($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 58
    public function block_email_footer_3($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 76
    public function block_site_email_address($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "                                    ";
        echo twig_escape_filter($this->env, ($context["site_email"] ?? null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "emails/_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 77,  221 => 76,  215 => 58,  209 => 57,  203 => 56,  197 => 54,  191 => 52,  183 => 51,  176 => 33,  170 => 13,  153 => 77,  151 => 76,  145 => 73,  138 => 69,  126 => 59,  123 => 58,  120 => 57,  118 => 56,  115 => 55,  113 => 54,  108 => 52,  104 => 51,  83 => 33,  60 => 13,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/_base.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_base.html.twig");
    }
}
