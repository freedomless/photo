<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload-print.html.twig */
class __TwigTemplate_9daf0a6632a94bff64c4f37eac848e34b5361749ed1b8aeec0650cb671ef5e5f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload-print.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product";
    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
    ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 8), "set", [0 => "total", 1 => 0], "method", false, false, false, 8), "html", null, true);
        echo "

    <div class=\"container basic-layout\">

        <div class=\"upload-single\">

            <div class=\"upload pt-5\">

                <h3 class=\"d-flex\">
                    <span class=\"flex-grow-1\">Product</span>
                    ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 18), "session", [], "any", false, false, false, 18), "get", [0 => "product-back"], "method", false, false, false, 18)) {
            // line 19
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 19), "session", [], "any", false, false, false, 19), "get", [0 => "product-back"], "method", false, false, false, 19), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 19), "session", [], "any", false, false, false, 19), "get", [0 => "product-back-arguments"], "method", false, false, false, 19)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-times\"></i>
                        </a>
                    ";
        }
        // line 23
        echo "
                </h3>
                <hr>

                ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

                ";
        // line 31
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 31), "get", [0 => "type"], "method", false, false, false, 31), "single"))) {
            // line 32
            echo "
                    <div class=\"row\">

                        <div class=\"col-4\"></div>

                        ";
            // line 37
            echo twig_call_macro($macros["_self"], "macro_upload", [($context["form"] ?? null), ($context["product"] ?? null), ($context["configuration"] ?? null)], 37, $context, $this->getSourceContext());
            echo "

                    </div>


                ";
        } else {
            // line 43
            echo "

                    <div class=\"row\">

                        ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 47));
            foreach ($context['_seq'] as $context["index"] => $context["f"]) {
                // line 48
                echo "
                            ";
                // line 49
                echo twig_call_macro($macros["_self"], "macro_upload", [$context["f"], (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "children", [], "any", false, false, false, 49)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["index"]] ?? null) : null), ($context["configuration"] ?? null)], 49, $context, $this->getSourceContext());
                echo "

                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['f'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "
                    </div>


                ";
        }
        // line 57
        echo "
                <div class=\"text-center\">
                    ";
        // line 59
        if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 59), "attributes", [], "any", false, false, false, 59), "get", [0 => "type"], "method", false, false, false, 59), "series")) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "status", [], "any", false, false, false, 59), 3)))) {
            // line 60
            echo "                        <hr>
                        ";
            // line 61
            if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "newCommission", [], "any", false, false, false, 61)) {
                // line 62
                echo "                            <p class=\"text-info\">Your series price will be
                                                 changed on ";
                // line 63
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commissionChangingOn", [], "any", false, false, false, 63), "d M H:iA"), "html", null, true);
                echo "
                                                 to ";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "newCommission", [], "any", false, false, false, 64), "html", null, true);
                echo " &euro;.</p>
                        ";
            }
            // line 66
            echo "
                        <h4 class=\"text-muted\">Price <strong>";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commission", [], "any", false, false, false, 67), "html", null, true);
            echo " &euro;</strong></h4>
                        <hr>
                    ";
        }
        // line 70
        echo "
                </div>

                <div class=\"text-center mt-5\">

                    ";
        // line 75
        if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 75), "get", [0 => "total", 1 => 0], "method", false, false, false, 75), 0))) {
            // line 76
            echo "                        <button class=\"btn btn-blue\">Save</button>
                    ";
        }
        // line 78
        echo "
                    <div>
                        <a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage", ["type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 80), "attributes", [], "any", false, false, false, 80), "get", [0 => "type"], "method", false, false, false, 80)]), "html", null, true);
        echo "\"
                           class=\"btn\">Back to
                                       Manage ";
        // line 82
        echo (((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 82), "attributes", [], "any", false, false, false, 82), "get", [0 => "type"], "method", false, false, false, 82), "series"))) ? ("Photos") : ("Series"));
        echo " </a>
                    </div>

                </div>

                <div class=\"d-none\">
                    ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
                </div>

                ";
        // line 91
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "


            </div>

        </div>

    </div>

";
    }

    // line 102
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 103
        echo "    <script>
        \$(function () {
            \$('.custom-file-input').on('change', function () {

                let \$label = \$(this).parent().find('label');

                \$label.addClass('text-left')
                \$label.text('Added')

            })
        })
    </script>
";
    }

    // line 117
    public function macro_upload($__form__ = null, $__product__ = null, $__configuration__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "product" => $__product__,
            "configuration" => $__configuration__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 118
            echo "
    <div class=\"col-4 mb-4 d-flex\">

        <div class=\"card p-0\">

            <div class=\"card-body p-0\">

                ";
            // line 125
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["product"] ?? null), 1)) {
                // line 126
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 132
            echo "
                ";
            // line 133
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["product"] ?? null), 2)) {
                // line 134
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 140
            echo "
                ";
            // line 141
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["product"] ?? null), 3)) {
                // line 142
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                ";
            }
            // line 148
            echo "
                <img src=\"";
            // line 149
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 149), "image", [], "any", false, false, false, 149), "thumbnail", [], "any", false, false, false, 149), "html", null, true);
            echo "\" alt=\"\"
                     style=\"object-fit: cover\" width=\"100%\" height=\"300px\">
            </div>

            ";
            // line 153
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 153), "image", [], "any", false, false, false, 153), "print", [], "any", false, false, false, 153))) {
                // line 154
                echo "
                <div class=\"card-footer\">

                    <p class=\"text-success m-0\">Print file uploaded!</p>

                </div>

            ";
            }
            // line 162
            echo "
            ";
            // line 163
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "post", [], "any", true, true, false, 163)) {
                // line 164
                echo "
                ";
                // line 165
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 165), "set", [0 => "total", 1 => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 165), "get", [0 => "total", 1 => 0], "method", false, false, false, 165) + 1)], "method", false, false, false, 165), "html", null, true);
                echo "


                <div class=\"card-footer\">

                    ";
                // line 170
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "post", [], "any", false, false, false, 170), "image", [], "any", false, false, false, 170), "file", [], "any", false, false, false, 170), 'errors');
                echo "

                    <small class=\"text-muted\">";
                // line 172
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, "Print File"), "html", null, true);
                echo " <span class=\"text-danger\">*</span></small>

                    ";
                // line 174
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "post", [], "any", false, false, false, 174), "image", [], "any", false, false, false, 174), "file", [], "any", false, false, false, 174), 'widget', ["attr" => ["class" => "product"]]);
                echo "

                    <small class=\"text-muted\">Print width and height should be >= ";
                // line 176
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "minPrintSize", [], "any", false, false, false, 176), "html", null, true);
                echo "
                                              pixels</small>


                </div>

            ";
            }
            // line 183
            echo "
            ";
            // line 184
            $context["eligible"] = (0 === twig_compare($this->extensions['App\Twig\ProductNeedPrintExtension']->doSomething(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 184), "image", [], "any", false, false, false, 184)), false));
            // line 185
            echo "
            ";
            // line 186
            if (((null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 186), "image", [], "any", false, false, false, 186), "print", [], "any", false, false, false, 186)) && ($context["eligible"] ?? null))) {
                // line 187
                echo "
                <div class=\"card-footer\">

                    <div class=\"d-flex text-success\">

                       <span class=\"flex-grow-1  text-success\">

                          Your current file size is eligible for store no need to upload file.

                       </span>

                    </div>

                </div>

            ";
            }
            // line 203
            echo "
            <div class=\"card-footer\">

                ";
            // line 206
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", true, true, false, 206)) {
                // line 207
                echo "
                    ";
                // line 208
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 208), "set", [0 => "total", 1 => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 208), "get", [0 => "total", 1 => 0], "method", false, false, false, 208) + 1)], "method", false, false, false, 208), "html", null, true);
                echo "

                    <small class=\"text-muted\"> ";
                // line 210
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, "commission in euro €"), "html", null, true);
                echo ": <span class=\"text-danger\">*</span></small>

                    ";
                // line 212
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", false, false, false, 212), 'widget');
                echo "

                    <small class=\"text-muted\">Range ";
                // line 214
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "minCommission", [], "any", false, false, false, 214), "html", null, true);
                echo "
                                              - ";
                // line 215
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "maxCommission", [], "any", false, false, false, 215), "html", null, true);
                echo "
                        ";
                // line 216
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 216), "html", null, true);
                echo "
                    </small>

                    ";
                // line 219
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", false, false, false, 219), 'errors');
                echo "

                ";
            } else {
                // line 222
                echo "
                    ";
                // line 223
                if ((twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "priceUpdateInterval", [], "any", true, true, false, 223) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "priceUpdateInterval", [], "any", false, false, false, 223), 0)))) {
                    // line 224
                    echo "
                        <p class=\"text-info\">You will be able to change the price
                                             on: ";
                    // line 226
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "priceChangedAt", [], "any", false, false, false, 226), "d M H:iA"), "html", null, true);
                    echo ".</p>


                    ";
                } else {
                    // line 230
                    echo "
                        <p class=\"text-muted\">Commission
                            <strong>";
                    // line 232
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commission", [], "any", false, false, false, 232), 2), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 232), "html", null, true);
                    echo "</strong>
                        </p>

                    ";
                }
                // line 236
                echo "
                    ";
                // line 237
                if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "newCommission", [], "any", false, false, false, 237)) {
                    // line 238
                    echo "                        <p class=\"text-info\">Your price will be
                                             changed on ";
                    // line 239
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commissionChangingOn", [], "any", false, false, false, 239), "d M H:iA"), "html", null, true);
                    echo "
                                             to ";
                    // line 240
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "newCommission", [], "any", false, false, false, 240), "html", null, true);
                    echo " &euro;.</p>
                    ";
                }
                // line 242
                echo "
                    <p class=\"text-muted\">Price <strong>";
                // line 243
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "commission", [], "any", false, false, false, 243), "html", null, true);
                echo " &euro;</strong></p>


                ";
            }
            // line 247
            echo "
            </div>

        </div>

    </div>

";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "photo/upload-print.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  504 => 247,  497 => 243,  494 => 242,  489 => 240,  485 => 239,  482 => 238,  480 => 237,  477 => 236,  468 => 232,  464 => 230,  457 => 226,  453 => 224,  451 => 223,  448 => 222,  442 => 219,  436 => 216,  432 => 215,  428 => 214,  423 => 212,  418 => 210,  413 => 208,  410 => 207,  408 => 206,  403 => 203,  385 => 187,  383 => 186,  380 => 185,  378 => 184,  375 => 183,  365 => 176,  360 => 174,  355 => 172,  350 => 170,  342 => 165,  339 => 164,  337 => 163,  334 => 162,  324 => 154,  322 => 153,  315 => 149,  312 => 148,  304 => 142,  302 => 141,  299 => 140,  291 => 134,  289 => 133,  286 => 132,  278 => 126,  276 => 125,  267 => 118,  252 => 117,  236 => 103,  232 => 102,  218 => 91,  212 => 88,  203 => 82,  198 => 80,  194 => 78,  190 => 76,  188 => 75,  181 => 70,  175 => 67,  172 => 66,  167 => 64,  163 => 63,  160 => 62,  158 => 61,  155 => 60,  153 => 59,  149 => 57,  142 => 52,  133 => 49,  130 => 48,  126 => 47,  120 => 43,  111 => 37,  104 => 32,  102 => 31,  97 => 29,  92 => 27,  86 => 23,  78 => 19,  76 => 18,  63 => 8,  60 => 7,  56 => 6,  49 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/upload-print.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload-print.html.twig");
    }
}
