<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* album/show.html.twig */
class __TwigTemplate_b6b65909f2a41a96d97e9fbdf07868f3b911a100725673c9b40b78b2c9a25536 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "album/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["album"] ?? null), "name", [], "any", false, false, false, 4), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <div class=\"container basic-layout\">

        <div class=\"row\">

            <div class=\"col-12\">

                <h3>
                    ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["album"] ?? null), "name", [], "any", false, false, false, 16), "html", null, true);
        echo "

                    <span class=\"float-right\">
                        <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 19), "session", [], "any", false, true, false, 19), "get", [0 => "back"], "method", true, true, false, 19) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 19), "session", [], "any", false, true, false, 19), "get", [0 => "back"], "method", false, false, false, 19)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 19), "session", [], "any", false, true, false, 19), "get", [0 => "back"], "method", false, false, false, 19)) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_albums", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["album"] ?? null), "user", [], "any", false, false, false, 19), "slug", [], "any", false, false, false, 19)]))), "html", null, true);
        echo "\">
                            <i class=\"fas fa-times\"></i>
                        </a>
                    </span>
                </h3>

                <hr>

            </div>

        </div>

        <div class=\"row\">

            <div class=\"col-12\">

                ";
        // line 35
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "auth" =>         // line 37
($context["auth"] ?? null), "posts" =>         // line 38
($context["posts"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "album" => twig_get_attribute($this->env, $this->source,         // line 42
($context["album"] ?? null), "id", [], "any", false, false, false, 42), "page" => "album"]]);
        // line 43
        echo "

            </div>

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "album/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 43,  100 => 42,  99 => 38,  98 => 37,  97 => 35,  78 => 19,  72 => 16,  62 => 8,  58 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "album/show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/album/show.html.twig");
    }
}
