<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/index.html.twig */
class __TwigTemplate_036d57996a75a21bc6d598192a3c5dae2c6cf2fdba511db55a0185a439b18182 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "store/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Art Prints";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"basic-layout store\">

        <ul class=\"nav justify-content-center custom-active store-nav\">

            <li class=\"nav-item\">
                <a class=\"nav-link ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("single", "type"), "html", null, true);
        echo "\"
                   href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store");
        echo "\"><strong>SINGLE</strong></a>
            </li>

            <li class=\"nav-item \">
                <a class=\"nav-link  ";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("series", "type"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store", ["type" => "series"]);
        echo "\">
                    <strong>SERIES</strong>
                </a>
            </li>

            <li class=\"nav-item \">
                <a class=\"text-info nav-link\" target=\"_blank\" href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_product_details");
        echo "\">
                    <strong>PRODUCT DETAILS</strong>
                </a>
            </li>

        </ul>

        <div class=\"container photos mt-5\">

            <hr>

            ";
        // line 34
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 34), "query", [], "any", false, false, false, 34), "all", [], "method", false, false, false, 34);
        // line 35
        echo "
            <i class=\"fas fa-lightbulb d-inline d-md-none js-wall-switch\" id=\"js-bulb\"></i>
            <button class=\"btn btn-black-txt btn-light d-none d-md-inline js-wall-switch\" id=\"js-swich\">LITE WALL</button>

            ";
        // line 39
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 39), "attributes", [], "any", false, false, false, 39), "get", [0 => "type"], "method", false, false, false, 39), "single"))) {
            // line 40
            echo "                ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>             // line 42
($context["nude"] ?? null), "auth" =>             // line 43
($context["auth"] ?? null), "sorting" => true, "more" => twig_get_attribute($this->env, $this->source,             // line 45
($context["posts"] ?? null), "has_more", [], "any", false, false, false, 45), "page" => "store", "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "filter" => true, "filters" =>             // line 50
($context["filters"] ?? null), "categories" =>             // line 51
($context["categories"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,             // line 52
($context["posts"] ?? null), "items", [], "any", false, false, false, 52), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,             // line 53
($context["posts"] ?? null), "total", [], "any", false, false, false, 53), 0)), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")]]);
            // line 54
            echo "
            ";
        } else {
            // line 56
            echo "                ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 1, "sorting" => true, "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "categories" =>             // line 61
($context["categories"] ?? null), "series" => twig_get_attribute($this->env, $this->source,             // line 62
($context["posts"] ?? null), "items", [], "any", false, false, false, 62), "page" => "store"]]);
            // line 63
            echo "
            ";
        }
        // line 65
        echo "
        </div>

    </div>


";
    }

    // line 73
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 74
        echo "<script>
    \$(document).ready(function() {
        \$('.js-wall-switch').click(function(e){
            \$('.main-body').toggleClass('main-store');

            let bulb = \$('#js-bulb');
            bulb.toggleClass('fas')
            bulb.toggleClass('far')

            let bg_switch = \$('#js-swich');
            bg_switch.toggleClass('btn-light')
            bg_switch.toggleClass('btn-dark')
            bg_switch.toggleClass('btn-black-txt')
            bg_switch.text(function(i, text){
                return text === \"LITE WALL\" ? \"DARK WALL\" : \"LITE WALL\";
            })
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "store/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 74,  146 => 73,  136 => 65,  132 => 63,  130 => 62,  129 => 61,  127 => 56,  123 => 54,  121 => 53,  120 => 52,  119 => 51,  118 => 50,  117 => 45,  116 => 43,  115 => 42,  113 => 40,  111 => 39,  105 => 35,  103 => 34,  89 => 23,  78 => 17,  71 => 13,  67 => 12,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "store/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/index.html.twig");
    }
}
