<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/cart.html.twig */
class __TwigTemplate_a9e1518a8b8b0f261918d7271114554569f33fc5d8254fb6a9eb6287eb6d84b1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "store/cart.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Cart";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"container-fluid basic-layout\">

        <div class=\"cart\">
            ";
        // line 10
        if (((0 === twig_compare(twig_length_filter($this->env, ($context["sizes"] ?? null)), 0)) || (0 === twig_compare(twig_length_filter($this->env, ($context["materials"] ?? null)), 0)))) {
            // line 11
            echo "               <div class=\"text-center\" style=\"position:absolute; top: 50%; left:0;right: 0; margin-top: -100px\">
                   <i class=\"fas fa-store\" style=\"font-size: 80px\"></i>
                   <h3 class=\"text-muted text-center\" style=\"margin-top: 500px\">
                      <strong> We will be back soon</strong>
                   </h3>
               </div>
            ";
        } else {
            // line 18
            echo "                ";
            $context["user"] = $this->extensions['App\Twig\CartUserExtension']->getUserCart(1);
            // line 19
            echo "                ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Cart", ["rendering" => "client_side", "props" => ["countries" =>             // line 20
($context["countries"] ?? null), "configuration" =>             // line 21
($context["configuration"] ?? null), "user" =>             // line 22
($context["user"] ?? null), "items" =>             // line 23
($context["items"] ?? null), "materials" =>             // line 24
($context["materials"] ?? null), "sizes" =>             // line 25
($context["sizes"] ?? null)]]);
            echo "
            ";
        }
        // line 27
        echo "        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "store/cart.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 27,  85 => 25,  84 => 24,  83 => 23,  82 => 22,  81 => 21,  80 => 20,  78 => 19,  75 => 18,  66 => 11,  64 => 10,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "store/cart.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/cart.html.twig");
    }
}
