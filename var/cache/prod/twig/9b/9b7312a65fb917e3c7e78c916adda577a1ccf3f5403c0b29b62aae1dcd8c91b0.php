<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_product_series_cover.html.twig */
class __TwigTemplate_4d91adeb07d053ee58fcd86c129da4f8e77e3dfc9f058ae00f08eda70778aa59 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 1), "type", [], "any", false, false, false, 1), 1)) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 1), "parent", [], "any", false, false, false, 1)))) {
            // line 2
            echo "    <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 2), "parent", [], "any", false, false, false, 2), "cover", [], "any", false, false, false, 2), "image", [], "any", false, false, false, 2), "thumbnail", [], "any", false, false, false, 2), "html", null, true);
            echo "\" alt=\"\" width=\"100px\" class=\"rounded shadow-sm\">
";
        }
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_product_series_cover.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/_product_series_cover.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_product_series_cover.html.twig");
    }
}
