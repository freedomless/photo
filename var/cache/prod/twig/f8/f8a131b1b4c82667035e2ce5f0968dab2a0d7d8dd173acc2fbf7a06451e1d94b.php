<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site_notifications/_base.html.twig */
class __TwigTemplate_e6a4c41c0cb4ebda67967491fa6b82c0efdfdd307130277352111547565ae32a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'message_header' => [$this, 'block_message_header'],
            'message_title' => [$this, 'block_message_title'],
            'message_content' => [$this, 'block_message_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"notification-container\">

    <div class=\"container\">

        <div class=\"row\">

            <div class=\"notification-wrapper\">

                <div class=\"poll-vote p-3 p-md-4 rounded\">

                    <div class=\"d-none d-md-inline\">
                        <h1 class=\"h5\">
                            ";
        // line 13
        $this->displayBlock('message_header', $context, $blocks);
        // line 14
        echo "                        </h1>

                        <a href=\"#\" class=\"js-hide-notification close-poll\"
                           data-api-url=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_global_notification_hide", ["notificationName" =>         // line 18
($context["templateName"] ?? null)]), "html", null, true);
        // line 19
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close</a>
                        <hr>
                    </div>

                    <h2 class=\"h4\">
                        ";
        // line 25
        $this->displayBlock('message_title', $context, $blocks);
        // line 26
        echo "                    </h2>

                    <hr class=\"d-block d-md-none\">

                    <div class=\"poll-choices \">
                        ";
        // line 31
        $this->displayBlock('message_content', $context, $blocks);
        // line 32
        echo "                    </div>

                    <div class=\"text-center d-block d-md-none mt-2\">
                        <a href=\"#\" class=\"js-hide-notification\"
                           data-api-url=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_global_notification_hide", ["notificationName" =>         // line 37
($context["templateName"] ?? null)]), "html", null, true);
        // line 38
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>";
    }

    // line 13
    public function block_message_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "🔔 Notification";
    }

    // line 25
    public function block_message_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 31
    public function block_message_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "site_notifications/_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 31,  116 => 25,  109 => 13,  92 => 38,  90 => 37,  89 => 36,  83 => 32,  81 => 31,  74 => 26,  72 => 25,  64 => 19,  62 => 18,  61 => 17,  56 => 14,  54 => 13,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "site_notifications/_base.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/site_notifications/_base.html.twig");
    }
}
