<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/resent-confirmation.html.twig */
class __TwigTemplate_676fc1f78f0ce48021e525caff56c0455374f5f8834dd303242d2b71363f8c37 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
<div class=\"text-center center flex-column\">
    <h3 class=\"text-danger\">
        You have to be verified before you can upload photo.
    </h3>

    <a href=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_resend_confirmation");
        echo "\" class=\"btn btn-blue\">Resent confirmation email</a>
</div>
";
    }

    public function getTemplateName()
    {
        return "_partials/resent-confirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/resent-confirmation.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/resent-confirmation.html.twig");
    }
}
