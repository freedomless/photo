<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/series/index.html.twig */
class __TwigTemplate_0a90e481bcbf39451e9ec1d08af899de4aca54988617e298ad99fc325cb58cb8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/series/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Series Manager";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"container\">
        <hr>
        <h3 class=\"d-flex\">
            <div class=\"flex-grow-1\">Series Manager</div>
            <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_create");
        echo "\" class=\"btn btn-blue\">Create Series</a>
        </h3>
        <hr>
        <div class=\"row\">
            ";
        // line 15
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["posts"] ?? null)), 0))) {
            // line 16
            echo "                ";
            $this->loadTemplate("admin/curator/series/posts.html.twig", "admin/curator/series/index.html.twig", 16)->display($context);
            // line 17
            echo "            ";
        }
        // line 18
        echo "        </div>
    </div>


";
    }

    // line 24
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "
    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.infinite-photos')</script>

    ";
        // line 30
        $macros["__internal_bc8cf11cc65861a8296deb9a0ff7127a61f8b19ded03b5005c3645280972d1bb"] = $this->loadTemplate("_partials/warning.html.twig", "admin/curator/series/index.html.twig", 30)->unwrap();
        // line 31
        echo "
    ";
        // line 32
        echo twig_call_macro($macros["__internal_bc8cf11cc65861a8296deb9a0ff7127a61f8b19ded03b5005c3645280972d1bb"], "macro_warning", ["delete-warning-modal", "delete-warning-form", "delete-warning-btn", "Confirm!", "Are you sure you want delete this photo?"], 32, $context, $this->getSourceContext());
        echo "

";
    }

    public function getTemplateName()
    {
        return "admin/curator/series/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 32,  105 => 31,  103 => 30,  96 => 26,  93 => 25,  89 => 24,  81 => 18,  78 => 17,  75 => 16,  73 => 15,  66 => 11,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/series/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/series/index.html.twig");
    }
}
