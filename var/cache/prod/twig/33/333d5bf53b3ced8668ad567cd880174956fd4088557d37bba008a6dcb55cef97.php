<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/single.html.twig */
class __TwigTemplate_6cac67b0fc4595216758d2b950e35ce97b92b0c6a31e3030106acd904db63776 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"row  flex-sm-column-reverse flex-md-row\">

    <div class=\"col-md-4 col-sm-12\">


        ";
        // line 6
        $this->loadTemplate("admin/curator/_partials/photo-update.html.twig", "admin/curator/_partials/single.html.twig", 6)->display($context);
        // line 7
        echo "
        ";
        // line 8
        $this->loadTemplate("admin/curator/_partials/reject-reasons.html.twig", "admin/curator/_partials/single.html.twig", 8)->display($context);
        // line 9
        echo "        ";
        $this->loadTemplate("admin/curator/_partials/curator-votes.html.twig", "admin/curator/_partials/single.html.twig", 9)->display($context);
        // line 10
        echo "
        ";
        // line 11
        $this->loadTemplate("admin/curator/_partials/curator-comment.html.twig", "admin/curator/_partials/single.html.twig", 11)->display($context);
        // line 12
        echo "
        ";
        // line 13
        if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 2) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 3))) {
            // line 14
            echo "
            ";
            // line 15
            $this->loadTemplate("admin/curator/_partials/curated-by.html.twig", "admin/curator/_partials/single.html.twig", 15)->display($context);
            // line 16
            echo "
        ";
        }
        // line 18
        echo "
        ";
        // line 19
        $this->loadTemplate("admin/curator/_partials/photo-details.html.twig", "admin/curator/_partials/single.html.twig", 19)->display($context);
        // line 20
        echo "


    </div>

    <div class=\"col-md-8 col-sm-12\">

        <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 27), "small", [], "any", false, false, false, 27), "html", null, true);
        echo "\" class=\"post\" style=\"cursor: zoom-in;\" data-url=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 27), "large", [], "any", false, false, false, 27), "html", null, true);
        echo "\"
             alt=\"";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 28), "html", null, true);
        echo "\" width=\"100%\">

        <div class=\"text-center\">
            <h4>
                ";
        // line 32
        if (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 32)) {
            // line 33
            echo "                    ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 33), "html", null, true);
            echo "
                ";
        } else {
            // line 35
            echo "                    <span class=\"text-muted\">untitled</span>
                ";
        }
        // line 37
        echo "            </h4>
        </div>

        ";
        // line 40
        $this->loadTemplate("admin/curator/_partials/author-details.html.twig", "admin/curator/_partials/single.html.twig", 40)->display($context);
        // line 41
        echo "
    </div>


</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/single.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 41,  117 => 40,  112 => 37,  108 => 35,  102 => 33,  100 => 32,  93 => 28,  87 => 27,  78 => 20,  76 => 19,  73 => 18,  69 => 16,  67 => 15,  64 => 14,  62 => 13,  59 => 12,  57 => 11,  54 => 10,  51 => 9,  49 => 8,  46 => 7,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/single.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/single.html.twig");
    }
}
