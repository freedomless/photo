<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/product.html.twig */
class __TwigTemplate_9393ca5ecda81e521a93c97896bb0f863901364d9b7cc8f411c2491af71b7501 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "store/product.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("SingleProduct", ["rendering" => "client_side", "props" => ["sizes" =>         // line 8
($context["sizes"] ?? null), "size" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 9
($context["app"] ?? null), "request", [], "any", false, false, false, 9), "query", [], "any", false, false, false, 9), "getInt", [0 => "size", 1 => 1], "method", false, false, false, 9), "materials" =>         // line 10
($context["materials"] ?? null), "material" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 11
($context["app"] ?? null), "request", [], "any", false, false, false, 11), "query", [], "any", false, false, false, 11), "getInt", [0 => "material", 1 => 1], "method", false, false, false, 11), "quantity" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 12
($context["app"] ?? null), "request", [], "any", false, false, false, 12), "query", [], "any", false, false, false, 12), "getInt", [0 => "quantity", 1 => 1], "method", false, false, false, 12), "product" =>         // line 13
($context["product"] ?? null)]]);
        // line 14
        echo "

";
    }

    public function getTemplateName()
    {
        return "store/product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 14,  67 => 13,  66 => 12,  65 => 11,  64 => 10,  63 => 9,  62 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "store/product.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/product.html.twig");
    }
}
