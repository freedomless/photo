<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/forgotten_password/restore.html.twig */
class __TwigTemplate_5e670993d875c8d585e3168d85bcffec76fe1f77ab762e7d078813c34bec30d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "/security/template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("/security/template.html.twig", "security/forgotten_password/restore.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Reset Password";
    }

    // line 5
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Restore Access";
    }

    // line 7
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <div class=\"basic-layout\">

        <div class=\"default-form-layout\">

            ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

            <div class=\"form-group\">

                ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 17), "first", [], "any", false, false, false, 17), 'label');
        echo "
                ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 18), "first", [], "any", false, false, false, 18), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                <div class=\"text-danger\">
                    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 21), "first", [], "any", false, false, false, 21), 'errors');
        echo "
                </div>

            </div>

            <div class=\"form-group\">
                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 27), "second", [], "any", false, false, false, 27), 'label');
        echo "
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 28), "second", [], "any", false, false, false, 28), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                <div class=\"text-danger\">
                    ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 31), "second", [], "any", false, false, false, 31), 'errors');
        echo "
                </div>

            </div>

            <hr>

            <div class=\"text-center ml-5 mr-5 mt-3\">
                <button class=\"btn btn-info\" type=\"submit\">Reset Password</button>
            </div>

            ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "notification"], "method", false, false, false, 42));
        foreach ($context['_seq'] as $context["_key"] => $context["flash"]) {
            // line 43
            echo "                <div class=\"text-center text-success\">
                    ";
            // line 44
            echo twig_escape_filter($this->env, $context["flash"], "html", null, true);
            echo "
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "
            ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 48), 'widget');
        echo "

            ";
        // line 50
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

        </div>

    </div>

";
    }

    // line 58
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "security/forgotten_password/restore.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 58,  148 => 50,  143 => 48,  140 => 47,  131 => 44,  128 => 43,  124 => 42,  110 => 31,  104 => 28,  100 => 27,  91 => 21,  85 => 18,  81 => 17,  74 => 13,  67 => 8,  63 => 7,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/forgotten_password/restore.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/forgotten_password/restore.html.twig");
    }
}
