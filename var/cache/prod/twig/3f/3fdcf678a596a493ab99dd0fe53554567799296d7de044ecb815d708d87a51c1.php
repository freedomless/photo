<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload.html.twig */
class __TwigTemplate_45c7958553b4d0bbe570d888aff8da38cb41c8b064bdf362a9a9df6284534f75 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Upload Photo";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 7), "isVerified", [], "any", false, false, false, 7), false))) {
            // line 8
            echo "
        ";
            // line 9
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/upload.html.twig", 9)->display($context);
            // line 10
            echo "
    ";
        } else {
            // line 12
            echo "
        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    ";
            // line 19
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
            echo "

                    <h3>";
            // line 21
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "method", [], "any", false, false, false, 21), "POST"))) ? ("Upload") : ("Update"));
            echo " Photo </h3>

                    <hr>

                    ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "

                    <div class=\"row mb-5\">

                        <div class=\"col-xl-4\">

                            ";
            // line 31
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "method", [], "any", false, false, false, 31), "POST"))) {
                // line 32
                echo "
                                ";
                // line 33
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 33), "file", [], "any", false, false, false, 33), 'widget', ["attr" => ["data-label" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 33), "file", [], "any", false, false, false, 33), "vars", [], "any", false, false, false, 33), "label", [], "any", false, false, false, 33)]]);
                echo "

                                ";
                // line 35
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 35), "file", [], "any", false, false, false, 35), 'errors');
                echo "

                                <div class=\"mt-3\">

                                    <ul class=\"list-unstyled small\">

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Min Res.: 1000px per one of the sides
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Max file size: 30 MB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Format JPEG sRGB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No watermarks or signatures on the
                                                                                   image
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No borders on the image
                                        </li>

                                    </ul>

                                </div>

                            ";
            } else {
                // line 67
                echo "
                                <img src=\"";
                // line 68
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 68), "html", null, true);
                echo "\" width=\"100%\">

                                ";
                // line 70
                if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition(($context["post"] ?? null), "to_curate") || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 2)) && (null === twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 70)))) {
                    // line 71
                    echo "
                                    <button type=\"button\"
                                            class=\"btn btn-danger warning-action mt-3\"
                                            data-url=\"";
                    // line 74
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 74)]), "html", null, true);
                    echo "\">
                                        Delete
                                    </button>

                                ";
                }
                // line 79
                echo "
                            ";
            }
            // line 81
            echo "
                        </div>

                        <div class=\"col-xl-8\">

                            <div class=\"form-group row  pl-3 pr-3\">

                                ";
            // line 88
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 88), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 89
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 89), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 95
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 95), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 96
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 96), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 102
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 102), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 103
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 103), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 109
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 109), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 110
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 110), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"row pl-3 pr-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-9\">

                                    <div>

                                        ";
            // line 122
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 122), 'widget');
            echo "

                                    </div>

                                    <div>

                                        ";
            // line 128
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 128), 'widget');
            echo "

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <h3 class=\"text-muted\">Optional Details:</h3>

                    <hr>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 146
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 146), "camera", [], "any", false, false, false, 146), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 148
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 148), "lens", [], "any", false, false, false, 148), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 150
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 150), "focalLength", [], "any", false, false, false, 150), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 152
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 152), "shutterSpeed", [], "any", false, false, false, 152), 'row');
            echo "</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 158
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 158), "ISO", [], "any", false, false, false, 158), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 160
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 160), "exposure", [], "any", false, false, false, 160), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 162
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 162), "flash", [], "any", false, false, false, 162), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 164
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 164), "filter", [], "any", false, false, false, 164), 'row');
            echo "</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 170
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 170), "tripod", [], "any", false, false, false, 170), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 172
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "image", [], "any", false, false, false, 172), "aperture", [], "any", false, false, false, 172), 'row');
            echo "</div>

                    </div>

                    <div class=\"text-center mt-5\">

                        ";
            // line 178
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "upload", [], "any", false, false, false, 178), 'widget');
            echo "

                    </div>

                    ";
            // line 182
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
            echo "

                </div>

            </div>

        </div>

    ";
        }
        // line 191
        echo "
";
    }

    // line 194
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 195
        echo "
    ";
        // line 196
        $context["message"] = "Are you sure you want to delete this photo permantly?";
        // line 197
        echo "
    ";
        // line 198
        $this->loadTemplate("_partials/warning-modal.html.twig", "photo/upload.html.twig", 198)->display($context);
        // line 199
        echo "
    <script>
        \$(function () {
            \$('.custom-file').dropzone({form: \$('form')});
        })
    </script>

";
    }

    public function getTemplateName()
    {
        return "photo/upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  374 => 199,  372 => 198,  369 => 197,  367 => 196,  364 => 195,  360 => 194,  355 => 191,  343 => 182,  336 => 178,  327 => 172,  322 => 170,  313 => 164,  308 => 162,  303 => 160,  298 => 158,  289 => 152,  284 => 150,  279 => 148,  274 => 146,  253 => 128,  244 => 122,  229 => 110,  225 => 109,  216 => 103,  212 => 102,  203 => 96,  199 => 95,  190 => 89,  186 => 88,  177 => 81,  173 => 79,  165 => 74,  160 => 71,  158 => 70,  151 => 68,  148 => 67,  113 => 35,  108 => 33,  105 => 32,  103 => 31,  94 => 25,  87 => 21,  82 => 19,  73 => 12,  69 => 10,  67 => 9,  64 => 8,  62 => 7,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/upload.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload.html.twig");
    }
}
