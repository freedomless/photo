<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/curate_potd.html.twig */
class __TwigTemplate_f7d9ac3567660296c4448a54ebc48173d16c342453cad8ce3b542d2269d201cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
            'css' => [$this, 'block_css'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/curator/curate_potd.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Manage Photo of the Day";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Manage Photos of the Day";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " <strong>The Time is:</strong> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_converter($this->env), "d/M/Y H:i:s"), "html", null, true);
    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"col-12\">

        <div class=\"row\">

            ";
        // line 15
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["photos"] ?? null)), 0))) {
            // line 16
            echo "
                ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["photos"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
                // line 18
                echo "
                    <div data-id=\"";
                // line 19
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 19), "html", null, true);
                echo "\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">

                        <div class=\"card mb-4 text-center\">

                            <a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_show", ["page" => "portfolio", "id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 25
$context["photo"], "post", [], "any", false, false, false, 25), "id", [], "any", false, false, false, 25), "payload" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 26
$context["photo"], "post", [], "any", false, false, false, 26), "user", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26)]), "html", null, true);
                // line 27
                echo "\" target=\"_blank\" style=\"cursor: zoom-in; position: relative\">
                                ";
                // line 28
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 28)) {
                    // line 29
                    echo "                                    <div class=\"alert-success position-absolute\" style=\"bottom: 0; width: 100%\">Shows
                                        tomorrow
                                    </div>
                                ";
                }
                // line 33
                echo "
                                <img src=\"";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 34), "image", [], "any", false, false, false, 34), "thumbnail", [], "any", false, false, false, 34), "html", null, true);
                echo "\" class=\"card-img-top\" alt=\"...\"
                                     style=\"object-fit: cover; width: 100%; height: 200px\">
                            </a>


                            <div class=\"card-body\">
                                <div class=\"clearfix text-cente position-relative\">

                                    <i class=\"js-move-left fa-2x fa fa-arrow-circle-left float-left ";
                // line 42
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 42)) {
                    echo "d-none";
                }
                echo "\"></i>

                                    <i class=\"js-move-right fa-2x  fa fa-arrow-circle-right float-right ";
                // line 44
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 44)) {
                    echo "d-none";
                }
                echo "\"></i>

                                </div>

                                <div></div>

                                <form action=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_delete_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 50)]), "html", null, true);
                echo "\" method=\"post\">
                                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\"/>
                                    <input type=\"hidden\" name=\"photo_id\" value=\"";
                // line 52
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 52), "html", null, true);
                echo "\"/>
                                    <button class=\"btn btn-danger btn-sm text-white mt-3\">Delete</button>
                                </form>

                                <hr class=\"no-gutter\">

                                <div>";
                // line 58
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "createdAt", [], "any", false, false, false, 58), "d/M/Y H:i:s"), "html", null, true);
                echo "</div>

                                <div class=\"small text-muted\">";
                // line 60
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 60), "user", [], "any", false, false, false, 60), "profile", [], "any", false, false, false, 60), "firstName", [], "any", false, false, false, 60), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 60), "user", [], "any", false, false, false, 60), "profile", [], "any", false, false, false, 60), "lastName", [], "any", false, false, false, 60), "html", null, true);
                echo "</div>

                            </div>
                        </div>
                    </div>

                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "
            ";
        } else {
            // line 69
            echo "
                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No photos in the queue.</h6>
                </div>

            ";
        }
        // line 75
        echo "
        </div>

    </div>

    <form id=\"js-switch-form\" action=\"";
        // line 80
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_switch_photo_of_the_day");
        echo "\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

";
    }

    // line 87
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 88
        echo "    <script>
        \$(document).ready(function () {
            \$('.js-move-left').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').prev().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            });

            \$('.js-move-right').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').next().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            })
        })
    </script>
";
    }

    // line 109
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 110
        echo "    <style>
        .fa {
            cursor: pointer;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/curate_potd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 110,  261 => 109,  238 => 88,  234 => 87,  224 => 80,  217 => 75,  209 => 69,  205 => 67,  182 => 60,  177 => 58,  168 => 52,  163 => 50,  152 => 44,  145 => 42,  134 => 34,  131 => 33,  125 => 29,  123 => 28,  120 => 27,  118 => 26,  117 => 25,  116 => 23,  109 => 19,  106 => 18,  89 => 17,  86 => 16,  84 => 15,  77 => 10,  73 => 9,  65 => 7,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/curate_potd.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/curate_potd.html.twig");
    }
}
