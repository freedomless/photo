<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/photo-update.html.twig */
class __TwigTemplate_57373d95fb1a7806e422f8aab6c107e51bc3ad680b8453b46c6c5ed90a7ce269 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header d-flex\">

        <strong class=\"flex-grow-1\">";
        // line 5
        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "type", [], "any", false, false, false, 5), 1))) ? ("Photo") : ("Series"));
        echo " Update</strong>

    </div>
    <div class=\"card-body\">

        <div class=\"card-text\">

            <form action=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_update", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 12)]), "html", null, true);
        echo "\" method=\"post\">

                <div class=\"form-group\">
                    <label for=\"category_";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 15), "html", null, true);
        echo "\">Category</label>
                    <select name=\"category\" class=\"form-control\" id=\"category_";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 16), "html", null, true);
        echo "\">
                        <option value=\"\" disabled>Select Category</option>
                        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 19
            echo "                            <option ";
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "category", [], "any", false, false, false, 19), "id", [], "any", false, false, false, 19), twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 19)))) ? ("selected") : (""));
            echo "
                                    value=\"";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 20), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 20), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                    </select>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_greyscale_";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 26), "html", null, true);
        echo "\">
                        <input type=\"hidden\" value=\"0\" name=\"is_greyscale\">
                        <input type=\"checkbox\" id=\"is_greyscale_";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 28), "html", null, true);
        echo "\" name=\"is_greyscale\"
                               value=\"1\" ";
        // line 29
        echo ((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "isGreyscale", [], "any", false, false, false, 29)) ? ("checked") : (""));
        echo ">
                        Black & White
                    </label>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_nude_";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 35), "html", null, true);
        echo "\">
                        <input type=\"hidden\" value=\"0\" name=\"is_nude\">
                        <input type=\"checkbox\" id=\"is_nude_";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 37), "html", null, true);
        echo "\" name=\"is_nude\"
                               value=\"1\" ";
        // line 38
        echo ((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "isNude", [], "any", false, false, false, 38)) ? ("checked") : (""));
        echo ">
                        Nude Content
                    </label>
                </div>

                <div class=\"text-center m-2\">
                    <button class=\"btn btn-primary\">
                        Save
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/photo-update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 38,  116 => 37,  111 => 35,  102 => 29,  98 => 28,  93 => 26,  87 => 22,  77 => 20,  72 => 19,  68 => 18,  63 => 16,  59 => 15,  53 => 12,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/photo-update.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/photo-update.html.twig");
    }
}
