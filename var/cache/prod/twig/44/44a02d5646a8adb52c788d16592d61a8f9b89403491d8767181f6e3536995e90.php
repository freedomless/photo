<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/register/index.html.twig */
class __TwigTemplate_2bc0b21ef949b627dd547e2f3959dd37651160395134d3304c988835a5722dbc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "security/template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("security/template.html.twig", "security/register/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Registration";
    }

    // line 5
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Registration";
    }

    // line 7
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <div class=\"basic-layout\">

        <div class=\"default-form-layout\">

            ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

            ";
        // line 16
        echo "            ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "errors", [], "any", false, false, false, 16))) {
            // line 17
            echo "
                <div class=\"alert alert-error\">
                    ";
            // line 19
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                </div>

            ";
        }
        // line 23
        echo "
            <div class=\"form-group mb-3\">

                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 26), 'label');
        echo "
                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 27), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 33), "first", [], "any", false, false, false, 33), 'label');
        echo "
                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 34), "first", [], "any", false, false, false, 34), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 40), "second", [], "any", false, false, false, 40), 'label');
        echo "
                ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "password", [], "any", false, false, false, 41), "second", [], "any", false, false, false, 41), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <hr>

            <div class=\"form-group mb-3\">

                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, false, 49), 'label');
        echo "
                ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, false, 50), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                <small id=\"emailHelp\" class=\"form-text text-muted\">
                    Note: Once set, it cannot be changed later.
                </small>

            </div>

            <hr>

            <div class=\"form-group mb-1\">
                <small id=\"emailHelp\" class=\"form-text text-warning\">
                    Note: It is recommended to use latin characters for your First and Last name. Otherwise your photos
                    won't be watermarked.
                </small>
            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 68
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 68), "firstName", [], "any", false, false, false, 68), 'label');
        echo "
                ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 69), "firstName", [], "any", false, false, false, 69), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 75), "lastName", [], "any", false, false, false, 75), 'label');
        echo "
                ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 76), "lastName", [], "any", false, false, false, 76), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <hr>

            <div class=\"registration-agreement rounded-corner\">
                By clicking <strong>Agree and Continue</strong>, I hereby: <br>
                Agree and consent to the
                <br><br>
                <input type=\"checkbox\" id=\"js-terms-agreement\"> <label for=\"js-terms-agreement\"><b>Agree
                                                                                                   and
                                                                                                   Continue</b></label>
            </div>

            <hr>

            <div class=\"text-center ml-5 mr-5 mt-3\">
                <button class=\"btn btn-info disabled\" id=\"js-registraion-btn\" type=\"submit\">Register</button>
            </div>

            <div class=\"text-center mt-3 mb-3\">
                Already have an account? <strong><a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
        echo "\">Login</a></strong>
            </div>

            ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 101), 'widget');
        echo "

            ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "recaptcha", [], "any", false, false, false, 103), 'widget');
        echo "

            ";
        // line 105
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

        </div>

        <div
                class=\"g-recaptcha\"
                data-sitekey=\"";
        // line 111
        echo twig_escape_filter($this->env, ($context["google_recaptcha_site_key"] ?? null), "html", null, true);
        echo "\"
                data-callback=\"recaptchaOkay\"
                data-size=\"invisible\"
                id=\"cadastro-captcha\">
        </div>

    </div>

";
    }

    // line 121
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 122
        echo "    <h6 class=\"h6\">Register with Your social media account:</h6>

    <a href=\"";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "facebook"]);
        echo "\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-facebook fa-2x\"></i>
    </a>

    <a href=\"";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "google"]);
        echo "\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-google fa-2x\"></i>
    </a>

";
    }

    // line 134
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 135
        echo "
    <script src=\"https://www.google.com/recaptcha/api.js?render=";
        // line 136
        echo twig_escape_filter($this->env, ($context["google_recaptcha_site_key"] ?? null), "html", null, true);
        echo "\"></script>

    <script>

            grecaptcha.ready(function () {
                grecaptcha.execute('";
        // line 141
        echo twig_escape_filter($this->env, ($context["google_recaptcha_site_key"] ?? null), "html", null, true);
        echo "', {action: 'submit'}).then(function (token) {
                    \$('#registration_recaptcha').val(token)
                });
            });


    </script>

    <script>

        \$(document).ready(function () {

            var selector = '#js-registraion-btn, .js-social-reg';

            toggleBind(selector, true);

            \$('#js-terms-agreement').click(function () {
                if (\$(this).is(':checked')) {
                    toggleBind(selector, false);
                } else {
                    toggleBind(selector, true);
                }
            });

            function toggleBind(selector, set) {
                if (set) {
                    \$(selector).addClass('disabled');
                    \$(selector).bind('click', function (e) {
                        e.preventDefault();
                    });
                } else {
                    \$(selector).removeClass('disabled');
                    \$(selector).unbind('click')
                }
            }
        })

    </script>

";
    }

    public function getTemplateName()
    {
        return "security/register/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 141,  276 => 136,  273 => 135,  269 => 134,  260 => 128,  253 => 124,  249 => 122,  245 => 121,  232 => 111,  223 => 105,  218 => 103,  213 => 101,  207 => 98,  182 => 76,  178 => 75,  169 => 69,  165 => 68,  144 => 50,  140 => 49,  129 => 41,  125 => 40,  116 => 34,  112 => 33,  103 => 27,  99 => 26,  94 => 23,  87 => 19,  83 => 17,  80 => 16,  75 => 13,  68 => 8,  64 => 7,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/register/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/register/index.html.twig");
    }
}
