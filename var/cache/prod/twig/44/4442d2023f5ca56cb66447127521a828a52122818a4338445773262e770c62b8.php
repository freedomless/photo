<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/award.html.twig */
class __TwigTemplate_490e6524cc57c8d4941ecad042fcfa4d59c40c00f64276f21fe15c2577e2f43f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("photo_of_the_day", $context) &&  !(null === ($context["photo_of_the_day"] ?? null)))) {
            // line 2
            echo "
    ";
            // line 4
            echo "    ";
            // line 5
            echo "    ";
            // line 6
            echo "    ";
            // line 7
            echo "    ";
            // line 8
            echo "
    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_show", ["page" => "awarded", "id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)]), "html", null, true);
            echo "\">

                    ";
            // line 18
            echo "                    ";
            // line 19
            echo "                    ";
            // line 20
            echo "                    ";
            // line 21
            echo "                    ";
            // line 22
            echo "
                    <img src=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 23), "image", [], "any", false, false, false, 23), "small", [], "any", false, false, false, 23), "html", null, true);
            echo "\"
                         alt=\"Photo of the day!\">

                    <div class=\"author\">

                        <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 29
($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 29), "user", [], "any", false, false, false, 29), "slug", [], "any", false, false, false, 29)]), "html", null, true);
            // line 30
            echo "\">
                            &copy;
                            ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 32), "user", [], "any", false, false, false, 32), "profile", [], "any", false, false, false, 32), "first_name", [], "any", false, false, false, 32), "html", null, true);
            echo "
                            ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 33), "user", [], "any", false, false, false, 33), "profile", [], "any", false, false, false, 33), "last_name", [], "any", false, false, false, 33), "html", null, true);
            echo "
                        </a>

                    </div>

                </a>

            </div>

            <div class=\"social\">

                <span class=\"photo-of-the-day-title\">PHOTO OF THE DAY&nbsp;&nbsp;<span class=\"date\">
                        ";
            // line 45
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "created_at", [], "any", false, false, false, 45), "d M Y"), "html", null, true);
            echo "</span>
                </span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-drop-right-share\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share It!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-drop-right-share\">
                        ";
            // line 56
            echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 58
($context["photo_of_the_day"] ?? null), "id", [], "any", false, false, false, 58), "date" => twig_get_attribute($this->env, $this->source,             // line 59
($context["photo_of_the_day"] ?? null), "urlized_date", [], "any", false, false, false, 59)]), "classes" => "pr-2"]);
            // line 62
            echo "

                        ";
            // line 64
            echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 66
($context["photo_of_the_day"] ?? null), "id", [], "any", false, false, false, 66), "date" => twig_get_attribute($this->env, $this->source,             // line 67
($context["photo_of_the_day"] ?? null), "urlized_date", [], "any", false, false, false, 67)]), "media" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 69
($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", true, true, false, 69) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", false, false, false, 69)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", false, false, false, 69)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, false, false, 69), "image", [], "any", false, false, false, 69), "small", [], "any", false, false, false, 69))), "description" => "Photo of the Day!", "classes" => "pr-2"]);
            // line 72
            echo "

                        ";
            // line 74
            echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 76
($context["photo_of_the_day"] ?? null), "id", [], "any", false, false, false, 76), "date" => twig_get_attribute($this->env, $this->source,             // line 77
($context["photo_of_the_day"] ?? null), "urlized_date", [], "any", false, false, false, 77)])]);
            // line 79
            echo "
                    </div>

                </span>

            </div>

            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

            <!--
            <div class=\"photo-of-the-day-title\" style=\"text-transform: none\">Our best wishes to all PiART
                photographers.<br>
                Merry Christmas!
            </div>
            -->

        </div>

    </div>

    ";
            // line 100
            echo "    ";
            // line 101
            echo "    ";
            // line 102
            echo "    ";
            // line 103
            echo "    ";
            // line 104
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "home/award.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 104,  172 => 103,  170 => 102,  168 => 101,  166 => 100,  144 => 79,  142 => 77,  141 => 76,  140 => 74,  136 => 72,  134 => 69,  133 => 67,  132 => 66,  131 => 64,  127 => 62,  125 => 59,  124 => 58,  123 => 56,  109 => 45,  94 => 33,  90 => 32,  86 => 30,  84 => 29,  83 => 28,  75 => 23,  72 => 22,  70 => 21,  68 => 20,  66 => 19,  64 => 18,  59 => 15,  50 => 8,  48 => 7,  46 => 6,  44 => 5,  42 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/award.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/award.html.twig");
    }
}
