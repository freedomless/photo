<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_d98aea5bfd47dffdd15aae1a95576efbee82bb61b179fd3b9dea3ec1ccd61652 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home";
    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <link href=\"https://vjs.zencdn.net/7.10.2/video-js.css\" rel=\"stylesheet\" />
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    ";
        // line 11
        echo $this->env->getRuntime('App\Twig\PollRuntime')->renderPoll($this->env);
        echo "

    <div class=\"container photos sm-full photos-infinite\">

        ";
        // line 15
        $this->loadTemplate("home/posters.html.twig", "home/index.html.twig", 15)->display($context);
        // line 16
        echo "
        ";
        // line 17
        $this->loadTemplate("home/nav.html.twig", "home/index.html.twig", 17)->display($context);
        // line 18
        echo "
        ";
        // line 19
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>         // line 21
($context["nude"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,         // line 22
($context["posts"] ?? null), "items", [], "any", false, false, false, 22), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,         // line 25
($context["posts"] ?? null), "total", [], "any", false, false, false, 25), 0)), "auth" =>         // line 26
($context["auth"] ?? null), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 28
($context["app"] ?? null), "request", [], "any", false, false, false, 28), "attributes", [], "any", false, false, false, 28), "get", [0 => "page"], "method", false, false, false, 28)]]);
        echo "

    </div>

    ";
        // line 32
        echo twig_include($this->env, $context, "_partials/_social_icons.html.twig");
        echo "

";
    }

    // line 36
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    <script src=\"https://vjs.zencdn.net/7.10.2/video.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$('.share-btn').click(function (e) {
                e.preventDefault();
                window.open(\$(this).attr('href'), 'fbShareWindow', 'height=400, width=700, top=' + (\$(window).height() / 2 - 275) + ', left=' + (\$(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                return false;
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 37,  108 => 36,  101 => 32,  94 => 28,  93 => 26,  92 => 25,  91 => 22,  90 => 21,  89 => 19,  86 => 18,  84 => 17,  81 => 16,  79 => 15,  72 => 11,  69 => 10,  65 => 9,  60 => 6,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/index.html.twig");
    }
}
