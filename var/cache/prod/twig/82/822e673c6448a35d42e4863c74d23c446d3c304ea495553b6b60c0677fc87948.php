<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/_partials/rejected.html.twig */
class __TwigTemplate_b2002b7dcaf8a82a7e3d4a799b94116005faa71b109dd906a0855d6446f91ee4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p>You photo was rejected for store</p>

<div class=\"row\">
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Actual</h4>
        <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 6), "image", [], "any", false, false, false, 6), "thumbnail", [], "any", false, false, false, 6), "html", null, true);
        echo "\" alt=\"\" width=\"200px\">
    </div>
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Uploaded</h4>
        <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", true, true, false, 10) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", false, false, false, 10)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", false, false, false, 10)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, false, false, 10), "image", [], "any", false, false, false, 10), "thumbnail", [], "any", false, false, false, 10))), "html", null, true);
        echo "\" alt=\"\" width=\"200px\">
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "product/_partials/rejected.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product/_partials/rejected.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/_partials/rejected.html.twig");
    }
}
