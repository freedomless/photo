<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/published.html.twig */
class __TwigTemplate_263de3393e32d6cba0bab4ac5edb4704ec4d43d70d7cbfd6b5cb45bf0b8d0224 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Published";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"curate-posts basic-layout\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/published.html.twig", 9)->display($context);
        // line 10
        echo "
        <hr>

        ";
        // line 13
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/published.html.twig", 13)->display($context);
        // line 14
        echo "
        <hr>

        <div class=\"d-flex justify-content-center pagination\">
            ";
        // line 18
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "
        </div>

        <div class=\"curate-photos\">

            <div class=\"container photos\">

                <div class=\"row d-flex\">

                    ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["index"] => $context["post"]) {
            // line 28
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-3 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 35
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "isGreyscale", [], "any", false, false, false, 35), true))) {
                // line 36
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 40
            echo "
                                    ";
            // line 42
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 42))) {
                // line 43
                echo "                                        ";
                $context["comments"] = 0;
                // line 44
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 44));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 45
                    echo "                                            ";
                    $context["comments"] = (($context["comments"] ?? null) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 45)));
                    // line 46
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 47
                echo "
                                        ";
                // line 48
                if ((1 === twig_compare(($context["comments"] ?? null), 0))) {
                    // line 49
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 51
                    echo twig_escape_filter($this->env, ($context["comments"] ?? null), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 55
                echo "
                                    ";
            } else {
                // line 57
                echo "
                                        ";
                // line 58
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 58)), 0))) {
                    // line 59
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 61
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 61)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 65
                echo "
                                    ";
            }
            // line 67
            echo "                                    ";
            // line 68
            echo "
                                    <a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 69)]), "html", null, true);
            echo "\">

                                        ";
            // line 71
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 71), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 71), "thumbnail", [], "any", false, false, false, 71)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 71), "image", [], "any", false, false, false, 71), "thumbnail", [], "any", false, false, false, 71)));
            // line 72
            echo "                                        <img src=\"";
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 72), "html", null, true);
            echo "\" width=\"auto\"
                                             style=\"max-width:100%;max-height:300px\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">Sent</span>
                                    ";
            // line 81
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "sentForCurateDate", [], "any", false, false, false, 81), "d/M/Y H:i"), "html", null, true);
            echo "
                                </div>

                                <div class=\"card-footer\">

                                    ";
            // line 86
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 86), twig_date_converter($this->env)))) {
                // line 87
                echo "
                                        <div class=\"d-flex text-success\">
                                        <span class=\"flex-grow-1  text-success\">
                                            Published
                                        ";
                // line 91
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 91), ($context["site_publish_visibility_delay"] ?? null)), twig_date_converter($this->env)))) {
                    // line 92
                    echo "                                            <div class=\"small text-light muted\">User not notified</div>
                                        ";
                }
                // line 94
                echo "                                        </span>
                                            ";
                // line 95
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 95), "d/M/Y H:i"), "html", null, true);
                echo "
                                        </div>

                                    ";
            }
            // line 99
            echo "
                                    ";
            // line 100
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 100), twig_date_converter($this->env)))) {
                // line 101
                echo "
                                        <div class=\"d-flex text-warning\">
                                        <span class=\"flex-grow-1  text-warning\">
                                            Scheduled
                                           ";
                // line 105
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 105), ($context["site_publish_visibility_delay"] ?? null)), twig_date_converter($this->env)))) {
                    // line 106
                    echo "                                               <div class=\"small text-light muted\">User not notified</div>
                                           ";
                }
                // line 108
                echo "                                        </span>
                                            ";
                // line 109
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 109), "d/M/Y H:i"), "html", null, true);
                echo "
                                        </div>

                                    ";
            }
            // line 113
            echo "

                                </div>

                                ";
            // line 117
            if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 117)), 0))) {
                // line 118
                echo "
                                    <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">
                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>
                                    </span>
                                        Series
                                    </div>

                                ";
            }
            // line 128
            echo "
                                ";
            // line 129
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 129), twig_date_converter($this->env)))) {
                // line 130
                echo "
                                    <div class=\"card-footer\">

                                        <div class=\"d-flex justify-content-center\">

                                            <form method=\"post\" class=\"d-flex\">

                                                ";
                // line 137
                if (((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 137), "attributes", [], "any", false, false, false, 137), "get", [0 => "page"], "method", false, false, false, 137), 1)) || (1 === twig_compare($context["index"], 0)))) {
                    // line 138
                    echo "                                                    <button class=\"btn btn-blue\"
                                                            formaction=\"";
                    // line 139
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published_move", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 139), "up" => 1, "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 139), "get", [0 => "page"], "method", false, false, false, 139)]), "html", null, true);
                    echo "\">
                                                        <i class=\"fas fa-chevron-left\"></i></button>
                                                ";
                }
                // line 142
                echo "
                                                <input type=\"text\" class=\"form-control\" name=\"places\" id=\"places\"
                                                       placeholder=\"Places\" value=\"1\">

                                                <button class=\"btn btn-blue\"
                                                        formaction=\"";
                // line 147
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published_move", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 147), "up" => 0, "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 147), "get", [0 => "page"], "method", false, false, false, 147)]), "html", null, true);
                echo "\">
                                                    <i class=\"fas fa-chevron-right\"></i></button>

                                            </form>

                                        </div>

                                    </div>

                                ";
            }
            // line 157
            echo "
                                <div class=\"card-text small text-center\">
                                    <span class=\"text-success\">Publisher:</span>
                                    &nbsp;
                                    ";
            // line 161
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "publisher", [], "any", false, false, false, 161), "profile", [], "any", false, false, false, 161), "fullName", [], "any", false, false, false, 161), "html", null, true);
            echo "
                                </div>

                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center pagination\">
            ";
        // line 175
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["posts"] ?? null));
        echo "
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/curator/published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  353 => 175,  345 => 169,  331 => 161,  325 => 157,  312 => 147,  305 => 142,  299 => 139,  296 => 138,  294 => 137,  285 => 130,  283 => 129,  280 => 128,  268 => 118,  266 => 117,  260 => 113,  253 => 109,  250 => 108,  246 => 106,  244 => 105,  238 => 101,  236 => 100,  233 => 99,  226 => 95,  223 => 94,  219 => 92,  217 => 91,  211 => 87,  209 => 86,  201 => 81,  186 => 72,  184 => 71,  179 => 69,  176 => 68,  174 => 67,  170 => 65,  163 => 61,  159 => 59,  157 => 58,  154 => 57,  150 => 55,  143 => 51,  139 => 49,  137 => 48,  134 => 47,  128 => 46,  125 => 45,  120 => 44,  117 => 43,  114 => 42,  111 => 40,  105 => 36,  103 => 35,  94 => 28,  90 => 27,  78 => 18,  72 => 14,  70 => 13,  65 => 10,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/published.html.twig");
    }
}
