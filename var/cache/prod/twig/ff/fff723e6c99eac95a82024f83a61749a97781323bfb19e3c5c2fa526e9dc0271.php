<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/reject-reasons.html.twig */
class __TwigTemplate_d8f3bfe37b2fa9009ec550cc9c2f17914725d469f19b334b702f2a5cac996e75 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "rejectReasons", [], "any", false, false, false, 1)), 0))) {
            // line 2
            echo "
    <div class=\"card mt-3\">

        <div class=\"card-header d-flex\">

            <strong class=\"flex-grow-1\">Reject Reasons</strong>

        </div>
        <div class=\"card-body\">


            <ul class=\"list-group\">
                ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "rejectReasons", [], "any", false, false, false, 14));
            foreach ($context['_seq'] as $context["_key"] => $context["reason"]) {
                // line 15
                echo "                    <ol class=\"list-group-item\">Reason: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "reason", [], "any", false, false, false, 15), "html", null, true);
                echo "</ol>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reason'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            </ul>

        </div>

    </div>



";
        }
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/reject-reasons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 17,  57 => 15,  53 => 14,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/reject-reasons.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/reject-reasons.html.twig");
    }
}
