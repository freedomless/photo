<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login/index.html.twig */
class __TwigTemplate_66b71d71772f862c0a2c03e39b5390c566a9daf77ab8456b8b4e43dce0f7623f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'metadata' => [$this, 'block_metadata'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "security/template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("security/template.html.twig", "security/login/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 5
    public function block_metadata($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $context["social"] = ["title" => "Login", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_login")];
        // line 7
        echo "    ";
        $this->loadTemplate("_partials/meta.html.twig", "security/login/index.html.twig", 7)->display(twig_array_merge($context, ($context["social"] ?? null)));
    }

    // line 10
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 12
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "
    <form method=\"post\" class=\"default-form-layout\">

        ";
        // line 16
        if (($context["error"] ?? null)) {
            // line 17
            echo "            <div class=\"alert alert-danger text-center\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 17), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 17), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 19
        echo "
        ";
        // line 20
        $context["error"] = false;
        // line 21
        echo "
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "error"], "method", false, false, false, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 23
            echo "
            ";
            // line 24
            $context["error"] = true;
            // line 25
            echo "
            <div class=\"alert alert-danger text-center\">";
            // line 26
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
        <div class=\"form-group\">

            <label for=\"email\">Email</label>

            <input type=\"email\"
                   id=\"email\"
                   class=\"form-control\"
                   name=\"email\"
                   required
                   value=\"";
        // line 39
        (((array_key_exists("last_username", $context) &&  !(null === ($context["last_username"] ?? null)))) ? (print (twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true))) : (print ("")));
        echo "\">

        </div>

        <div class=\"form-group password\">

            <label for=\"password\">Password</label>

            <input type=\"password\"
                   class=\"form-control\"
                   id=\"password\"
                   required
                   name=\"password\">

            <small class=\"text-right d-block pull-right mr-4 mt-1\">
                <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forgotten_password");
        echo "\">Forgotten Password?</a>
            </small>

        </div>

        <div class=\"form-group text-center\">

            <label class=\"custom-control custom-checkbox\">
                <input type=\"checkbox\" checked name=\"_remember_me\" class=\"custom-control-input\"/>
                <span class=\"custom-control-indicator\"></span>
                Remember Me
            </label>

        </div>

        <div class=\"text-center mt-3 mb-3\">
            New here?<a href=\"";
        // line 70
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_register");
        echo "\" class=\"btn\">Register</a>
        </div>

        <hr>

        <div class=\"text-center ml-5 mr-5 mt-3\">
            <button class=\"btn btn-info\" type=\"submit\">Login</button>
        </div>

        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">

    </form>
";
    }

    // line 85
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 86
        echo "
    <h6 class=\"h6\">Login with Your social media account:</h6>

    <a href=\"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "facebook"]);
        echo "\" class=\"btn facebook-login\">
        <i class=\"fab fa-facebook\"></i>
    </a>

    <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "google"]);
        echo "\" class=\"btn google-login\">
        <i class=\"fab fa-google\"></i>
    </a>
";
    }

    public function getTemplateName()
    {
        return "security/login/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 93,  200 => 89,  195 => 86,  191 => 85,  183 => 79,  171 => 70,  152 => 54,  134 => 39,  122 => 29,  113 => 26,  110 => 25,  108 => 24,  105 => 23,  101 => 22,  98 => 21,  96 => 20,  93 => 19,  87 => 17,  85 => 16,  80 => 13,  76 => 12,  69 => 10,  64 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/login/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/login/index.html.twig");
    }
}
