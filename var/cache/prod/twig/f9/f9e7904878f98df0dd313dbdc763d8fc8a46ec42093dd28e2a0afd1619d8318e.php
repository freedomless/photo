<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/series.html.twig */
class __TwigTemplate_e9e3273125187fe7df7c2385f693ec1e0f2790ca3f2fb8a2e6938a07665ae00e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "home/series.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Series";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"container photos sm-full\">

        ";
        // line 9
        $this->loadTemplate("home/posters.html.twig", "home/series.html.twig", 9)->display($context);
        // line 10
        echo "
        ";
        // line 11
        $this->loadTemplate("home/nav.html.twig", "home/series.html.twig", 11)->display($context);
        // line 12
        echo "
        ";
        // line 13
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 0, "series" => twig_get_attribute($this->env, $this->source, ($context["series"] ?? null), "items", [], "any", false, false, false, 13), "page" => "home"]]);
        echo "

    </div>

";
    }

    public function getTemplateName()
    {
        return "home/series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  70 => 12,  68 => 11,  65 => 10,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/series.html.twig");
    }
}
