<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/warning.html.twig */
class __TwigTemplate_4aa475063258dff178062d7f8e8d443f9e0b40dbe1cd089d4c7196200b8996c2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 51
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "        <script>

            \$(function () {
                \$(document).on('click', '.";
        // line 55
        echo twig_escape_filter($this->env, ($context["btn_class"] ?? null), "html", null, true);
        echo "', function () {
                    \$('#";
        // line 56
        echo twig_escape_filter($this->env, ($context["modal_id"] ?? null), "html", null, true);
        echo "').modal('show');
                    \$('#";
        // line 57
        echo twig_escape_filter($this->env, ($context["form_id"] ?? null), "html", null, true);
        echo "').prop('action', \$(this).data('url'));
                })
            })

        </script>

    ";
    }

    // line 1
    public function macro_warning($__modal_id__ = "warning-modal", $__form_id__ = "warning-form", $__btn_class__ = "warning-action", $__header__ = "Confirm!", $__message__ = "Are you sure you want to take this action?", $__yes__ = "Yes", $__no__ = "No", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "modal_id" => $__modal_id__,
            "form_id" => $__form_id__,
            "btn_class" => $__btn_class__,
            "header" => $__header__,
            "message" => $__message__,
            "yes" => $__yes__,
            "no" => $__no__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 8
            echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"";
            // line 9
            echo twig_escape_filter($this->env, ($context["modal_id"] ?? null), "html", null, true);
            echo "\">

        <div class=\"modal-dialog\" role=\"document\">

            <form method=\"post\" id=\"";
            // line 13
            echo twig_escape_filter($this->env, ($context["form_id"] ?? null), "html", null, true);
            echo "\">

                <div class=\"modal-content\">

                    <div class=\"modal-header\">

                        <h5 class=\"modal-title\">";
            // line 19
            echo twig_escape_filter($this->env, ($context["header"] ?? null), "html", null, true);
            echo "</h5>

                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                            <span aria-hidden=\"true\">&times;</span>

                        </button>

                    </div>

                    <div class=\"modal-body\">

                        <p>";
            // line 31
            echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
            echo "</p>

                    </div>

                    <div class=\"modal-footer\">

                        <button class=\"btn btn-primary\">";
            // line 37
            echo twig_escape_filter($this->env, ($context["yes"] ?? null), "html", null, true);
            echo "</button>

                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
            // line 39
            echo twig_escape_filter($this->env, ($context["no"] ?? null), "html", null, true);
            echo "</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

    ";
            // line 51
            $this->displayBlock('js', $context, $blocks);
            // line 64
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "_partials/warning.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 64,  150 => 51,  135 => 39,  130 => 37,  121 => 31,  106 => 19,  97 => 13,  90 => 9,  87 => 8,  68 => 1,  57 => 57,  53 => 56,  49 => 55,  44 => 52,  40 => 51,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/warning.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/warning.html.twig");
    }
}
