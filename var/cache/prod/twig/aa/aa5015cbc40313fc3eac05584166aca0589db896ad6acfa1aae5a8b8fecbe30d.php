<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/user.html.twig */
class __TwigTemplate_21b77c91e239f38f7e4c90a2099d605a4cc4c8b06dd5cdcc50b8e02437184e40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<li class=\"nav-item dropdown\">

    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"account\" role=\"button\"
       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">My Profile</a>

    <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
         aria-labelledby=\"account\">

        <a class=\"dropdown-item\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 9), "slug", [], "any", false, false, false, 9)]), "html", null, true);
        echo "\">View</a>

        <a class=\"dropdown-item\" href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages");
        echo "\">Messages</a>

        <a class=\"dropdown-item\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_edit");
        echo "\">Settings</a>

        ";
        // line 15
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 15), "orders", [], "any", false, false, false, 15)), 0))) {
            // line 16
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_orders");
            echo "\">Orders</a>
        ";
        }
        // line 18
        echo "
        ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 19)) {
            // line 20
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_manage_sold");
            echo "\">Sold</a>
        ";
        }
        // line 22
        echo "
        <div class=\"dropdown-divider\"></div>
        <span class=\"ml-4 text-muted\" style=\"font-weight: 300\">Manage:</span>

        <a class=\"dropdown-item\" href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage");
        echo "\">Photos</a>
        <a class=\"dropdown-item\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage", ["type" => "series"]);
        echo "\">Series</a>

        ";
        // line 29
        if ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", true, true, false, 29) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 29), true)))) {
            // line 30
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_manage_products");
            echo "\">Products</a>
        ";
        }
        // line 32
        echo "
        <div class=\"dropdown-divider\"></div>

        <a class=\"dropdown-item\" href=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_logout");
        echo "\"><i class=\"fa fa-sign-out-alt\"></i> Sign-out</a>

    </div>

</li>
";
    }

    public function getTemplateName()
    {
        return "_partials/navigation/user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 35,  104 => 32,  98 => 30,  96 => 29,  91 => 27,  87 => 26,  81 => 22,  75 => 20,  73 => 19,  70 => 18,  64 => 16,  62 => 15,  57 => 13,  52 => 11,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/navigation/user.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/user.html.twig");
    }
}
