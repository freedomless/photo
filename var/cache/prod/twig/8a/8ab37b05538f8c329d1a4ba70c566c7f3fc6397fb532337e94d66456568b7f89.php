<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/user-agreement.html.twig */
class __TwigTemplate_93bc098dfa15f279babaad0114535ea35d3e5a7cee77e8018d8d505932e25f30 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "pages/user-agreement.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "User Agreement";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "User Agreement";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <span class=\"text-muted\">Last Update: 28 Aug, 2019</span>
";
    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
    <div class=\"col-md-12\">

        <div>

            <h2>Membership at ";
        // line 17
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["site_url"] ?? null)), "html", null, true);
        echo "</h2>

            <p>This member agreement applies between
                ";
        // line 20
        echo twig_escape_filter($this->env, ($context["site_business_name"] ?? null), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ") and You (the Member) where You will obtain
                membership at ";
        // line 21
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website and You grant ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " the right
                to display your uploaded photographs.</p>

            <h2>1. Definitions</h2>

            <p>The following terms shall have the meanings
                assigned to them herein below:</p>

            <p>1.1 \"Agreement\" means the terms
                and conditions below, to which You have agreed to be bound.</p>

            <p>1.2 \"Member\" (or \"You\")
                means the individual or entity that has accepted and agreed to be bound by the
                terms and conditions of this Agreement.</p>

            <p>1.3 \"";
        // line 36
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website\"
                shall mean ";
        // line 37
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s websites ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ".com, personal homepages
                hosted by ";
        // line 38
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and any subdomains.</p>

            <p>1.4 \"Gallery\" shall mean the
                collection of photographs approved and selected to be published on
                ";
        // line 42
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website by the curators of ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ".</p>

            <p>1.5 \"Your Photographs\" shall mean
                the photographs that you have uploaded to ";
        // line 45
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>1.6 \"Your Submitted Material\",
                shall mean Your Photographs, your avatar photo, your cover photo, your
                biography, photo descriptions and any other text, material or information,
                submitted to ";
        // line 50
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " by You.</p>

            <p>1.7 ”Your Account\" shall mean the
                account You register at ";
        // line 53
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>1.8 \"Account Menu\" shall mean the
                menu in the top navigation to the far right with a small silhouette or your
                presentation photo.</p>

            <p>1.9 ”Your Manage Photos Page” shall mean
                the overview of Your Photographs in the Account Menu where you manage Your
                Photographs when You are logged in to ";
        // line 61
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "’s website.</p>

            <p>1.10 ”Your Settings Page” shall mean your
                Settings page in Your Account Menu where You edit Settings and view agreements</span>.</p>

            <h2>2. License Grant</h2>

            <p>2.1 ";
        // line 68
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is hereby granted the
                right to display, process and store Your Photographs and all Your Submitted
                Material on ";
        // line 70
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website. You will remain the copyright owner of
                Your Photographs and all Your Submitted Material.</p>

            <p>2.2 ";
        // line 73
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is hereby granted the
                right to display Your Photographs in thumbnail versions websites other than
                ";
        // line 75
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website. ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is also granted the right to display
                share-buttons for social network sites below Your Photographs.</p>

            <p>2.3 ";
        // line 78
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is hereby granted the
                right to display, process and store Your Photographs and Your Submitted
                Material, including, but not limited to, in computers, mobile devices, tablets,
                TVs, monitors, media centers, image streaming devices and digital picture
                frames and in apps for any and all such devices.</p>

            <h2>3. Terms of Service</h2>

            <p>3.1 For terms and conditions for using
                ";
        // line 87
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website and/or services the Terms of Service apply, to which
                you accept and agree to be bound (TODO: Link).</p>

            <h2>4. Privacy Policy</h2>

            <p>4.1 For terms and conditions regarding
                ";
        // line 93
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s processing of your personal data and use of cookies, the
                Privacy Policy applies, to which you accept and agree to be bound (TODO: Link).</p>

            <h2>5. Warranties and Indemnifications</h2>

            <p>5.1 You warrant that You are 18 years old
                or above, which is the minimum age required to use ";
        // line 99
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s services.</p>

            <p>5.2 You warrant that You are the copyright
                owner of Your Photographs and that You have all necessary permits and
                sufficient rights in and to Your Photographs to grant to ";
        // line 103
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and
                ";
        // line 104
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners the rights indicated under Section 2 above. For
                avoidance of any doubt, if Your Photographs is a montage, You warrant that You
                are the copyright owner and that you have all necessary permits and sufficient
                rights in and to all parts of Your Photographs to grant ";
        // line 107
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and
                ";
        // line 108
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners the rights indicated under Section 2 above.</p>

            <p>5.3 You undertake to defend, at your own
                expense, ";
        // line 111
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners against any claims from
                third parties alleging that ";
        // line 112
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s or ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "’s Partners use
                of Your Photographs in accordance with this Agreement constitute an
                infringement of any rights of a third party (hereinafter referred to as
                \"Claims\") and to indemnify and hold ";
        // line 115
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and
                ";
        // line 116
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "’s Partner’s harmless from any costs, damages, liabilities,
                and/or expenses (including reasonable legal fees) suffered or incurred by
                ";
        // line 118
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " or ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners due to or arising from such Claims.</p>

            <p>5.4 You warrant that You have all the
                necessary model releases and that You have obtained all the necessary approvals
                from persons that appear in Your Photographs to grant to ";
        // line 122
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and
                ";
        // line 123
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners the rights indicated under Section 2 above.</p>


            <p>5.5 You warrant that You have all the
                necessary property releases and that you have obtained all the necessary
                approvals for any property, buildings, architecture, structures or sculptures
                appearing in Your Photographs to grant to ";
        // line 129
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners the rights indicated under Section 2 above.</p>

            <p>5.6 You warrant that You have all the
                necessary permits, rights and releases and that You have obtained all the
                necessary approvals for any live performances appearing in Your Photographs to
                grant to ";
        // line 135
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners the rights indicated
                under Section 2 above.</p>

            <p>5.7 You warrant that Your Photographs do
                not contain any illegal material and/or in themselves constitute illegal
                material.</p>

            <p>5.8 You warrant that Your Photographs do
                not contain anything that may constitute a violation of the personal integrity
                and privacy of an individual.</p>

            <p>5.9 You warrant that Your Photographs do
                not contain anything that may offend, slander, defame or discriminate any
                individual, entity or group of people.</p>

            <p>5.10 You warrant that Your Photographs do
                not contain any pornographic material.</p>

            <p>5.11 You warrant that Your Photographs do
                not contain any offensive material.</p>

            <p>5.12 You undertake to indemnify
                ";
        // line 157
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners for any and all costs, losses
                and/or damages incurred by ";
        // line 158
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " due to or arising from a breach of
                any of the warranties under this Section 5.</p>

            <p>5.13 You acknowledge and agree that if You
                fail to fulfill any of your undertakings under this Section 5, ";
        // line 162
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " may
                ban or delete Your Account. If Your Account is banned or deleted no money for
                paid membership fees will be refunded.</p>

            <h2>6. Member's Undertakings</h2>

            <p>6.1 You undertake to register a valid email
                address when you create Your Account and to keep Your Account information
                updated with a valid, active email address in Your Settings Page at all times.</p>

            <p>6.2 You undertake to register a PayPal
                account at PayPal's website PayPal.com and to provide a valid email address to
                your registered PayPal account in Your Settings Page. In the box where you
                enter your Paypal email address You undertake not to enter your Paypal email
                address only without any additional names, words or characters. You acknowledge
                and agree that if You fail to provide a valid email address to your registered
                PayPal account, ";
        // line 178
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " will not transfer any earnings from sold Printed
                Copies of Your Photographs to You.</p>

            <p>6.3 You warrant that you are the author of
                all of Your Photographs and if Your Photographs is a montage, that You are the
                author of all parts of Your Photographs. You are not allowed to upload
                photographs to ";
        // line 184
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website, which someone else is the original
                author of, for example stock photography or creative commons material, or use
                such material in any part of Your Photographs. If in doubt, ";
        // line 186
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                might request original digital source files, RAW-files or negative scans from
                You to verify that You are the author of Your Photographs.</p>

            <p>6.4 You warrant that each of Your
                Photographs mainly consists of digital original photographic source files from
                camera or film negatives and are not computer generated. If in doubt,
                ";
        // line 193
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " might request digital original source files or negative scans
                from You to verify that Your Photographs is not computer generated.</p>

            <p>6.5 You undertake to only register one
                account at ";
        // line 197
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website. If Your Account has been banned, or if You
                have lost your username or password, or for any other reason, You are not
                allowed to register a new account, without the written consent of ";
        // line 199
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ",
                instead contact ";
        // line 200
        echo twig_escape_filter($this->env, ($context["site_email"] ?? null), "html", null, true);
        echo ".</p>

            <p>6.6 You undertake to keep your password
                safe and never share it with anyone or write it down. ";
        // line 203
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s staff
                will never ask for your password. If you have lost your password You are able
                to reset it on ";
        // line 205
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>6.7 You warrant that the residential
                country You have selected is the country You actually reside in. You undertake
                to always keep your residential country updated in Your Settings Page.</p>

            <p>6.8 You acknowledge and agree that if you
                fail to fulfill any of your undertakings under this Section 6, ";
        // line 212
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                may delete one or more of Your Photographs and may ban or delete Your Account.
                If Your Account is banned or deleted no money for paid membership fees will be
                refunded.</p>

            <h2>7. ";
        // line 217
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Undertakings</h2>

            <p>7.1 ";
        // line 219
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " decides exclusively and
                in its sole discretion which of Your Photographs to publish or not publish in
                the Gallery. A decision not to publish Your Photographs in the Gallery can not
                be appealed. No reasons will be given why Your Photographs were published in
                the Gallery or not. If Your Photographs have been published in the Gallery,
                ";
        // line 224
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " undertakes to display them in the Gallery.</p>

            <p>7.2 ";
        // line 226
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " undertakes to
                acknowledge and identify you as the author of Your Photographs. Such
                acknowledgement will be made on ";
        // line 228
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website and on social network
                sites, blogs, photo sites, magazine sites, news sites and any other sites where
                Your Photographs have been posted. For thumbnail versions of Your Photographs
                you will be acknowledged as the author directly below the thumbnail or when
                Your Photographs are opened up in full size.</p>

            <h2>8. ";
        // line 234
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Handling of Your Photographs</h2>

            <p>8.1 Once Your Photographs have been
                published in the Gallery, it is not possible to delete them unless you delete
                Your Account. Your Photographs posted to social network sites, blogs, photo
                sites, magazine sites, news sites or included in ";
        // line 239
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s blog,
                magazine, photo tutorials or contests will not be deleted even if Your Account
                is deleted. If there are special circumstances for deleting one or more of Your
                Photographs published in the Gallery, for example copyright issues or personal
                integrity issues, contact ";
        // line 243
        echo twig_escape_filter($this->env, ($context["site_email"] ?? null), "html", null, true);
        echo " and state your reasons for
                deletion. ";
        // line 244
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " decides exclusively and in its sole discretion if Your
                Photographs will be deleted or not in such a case. A copy of all Your
                Photographs sent to the curators may be saved as a history of your uploads for
                the curators and are not possible to delete unless you end your membership.
                Your Photographs not sent to the curators or published in the Gallery can be
                deleted by You at any time.</p>

            <p>8.2 ";
        // line 251
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " may at any time publish
                Your Photographs in the Gallery or remove Your Photographs from the Gallery.</p>

            <p>8.3 You may upload high resolution files of
                Your Photographs. By default Your Photographs will be displayed on
                ";
        // line 256
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website in the original size you have uploaded, but Your
                Photographs will never be displayed in greater width than 2500 pixels or height
                than 2500 pixels. If Your Photographs are uploaded in greater resolution than
                2500 pixels width or height, they will be downsized so the maximum width and
                height do not exceed 2500 pixels, when displayed to the public on ";
        // line 260
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                website. ";
        // line 261
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " will keep copies of the image files of Your Photographs
                in the highest resolution uploaded.</p>

            <p>8.4 You acknowledge and agree that
                ";
        // line 265
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " have the right to delete any of Your Photographs, high
                resolution files or any other of Your Submitted Material at any time and for
                any reason, from ";
        // line 267
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website or other websites where Your
                Photographs are displayed.</p>

            <h2>9. Rules for Communicating on ";
        // line 270
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Website</h2>

            <p>9.1 When communicating on ";
        // line 272
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ",
                in all forms of communication and written messages, including but not limited
                to photo titles, photo comments, photo descriptions, contest posts,
                presentation texts, always use a friendly, polite language. Never write in
                anger, insult, slander, defame, offend, discriminate or make personal
                accusations or attacks against someone. Never use a rude language. When
                commenting on another member's photos you are required to be extra careful.
                Insulting, offending, slandering, defaming or discriminating someone for any
                reason is strictly prohibited.</p>

            <p>9.2 You are not allowed to question the
                decisions of ";
        // line 283
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and its curators to publish or not publish photos
                in the Gallery or discuss individual decisions in public in any and all forms
                of communication on ";
        // line 285
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>9.3 You acknowledge and agree that
                ";
        // line 288
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " has the right to delete any written message or other text or
                material submitted by You at any time and for any reason.</p>

            <p>9.4 You acknowledge and agree that if You
                fail to fulfil any of your undertakings under this Section 9, ";
        // line 292
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                may temporarily or permanently ban Your Account from writing posts and comments
                on ";
        // line 294
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website or temporarily or permanently ban or delete Your
                Account from ";
        // line 295
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website. If Your Account is banned or deleted no
                money for paid membership fees will be refunded.</p>

            <h2>10. Usage of and Linking to Photos</h2>

            <p>10.1 Copying, downloading, taking
                screenshots, displaying, printing or otherwise using photos on ";
        // line 301
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                website without the written consent of ";
        // line 302
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is strictly prohibited.
                It's strictly forbidden to remove the watermark or copyright information
                attached to photos on ";
        // line 304
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>10.2 When linking to photos on
                ";
        // line 307
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website, You are only allowed to link to the full image page
                with all information included (https://";
        // line 308
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo "/photo/_context_/_id_), never link
                directly to the image file or in any other way. You are not allowed to link to
                photos on ";
        // line 310
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website by copying images and use them as links,
                only text links are allowed on other sites.</p>

            <p>10.3 You acknowledge and agree that if You
                fail to fulfill any of your undertakings under this Section 10, ";
        // line 314
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                might temporarily or permanently ban or delete Your Account. If Your Account is
                banned or deleted no money for paid membership fees will be refunded. 
                ";
        // line 317
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " has the right to economical compensation for violations against
                Section 10.1 above.</p>

            <p>11. Membership Types and Prices</p>

            <p>11.1 For a detailed list of conditions and
                prices for the different memberships, refer to:
                <a href=\"";
        // line 324
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_faq");
        echo "\">F.A.Q</a> or the
                information you received when you signed up or made a payment for membership
                fees. These conditions may be subject to change at any time.</p>

            <h2>12. Ending Membership</h2>

            <p>12.1 If you end your membership by
                explicitly contacting us by email, Your Account will be deleted immediately.
                Your Photographs and Your Submitted Material will be deleted immediately. All
                Your Photographs and high resolution files of Your Photographs will be deleted
                within maximum 6 months after you deleted Your Account. Your Photographs, Your
                Submitted Material and personal information may still remain on backups, not
                accessible by the public, after Your Account has been deleted. Your Photographs
                and Your Submitted Material posted on social network sites, blogs, photo sites,
                magazine sites, news sites and any other sites and associated information will
                not be deleted.</p>

            <p>12.2 For terms and conditions regarding
                deletion of your personal data when ending your membership the terms and
                conditions in the Privacy Policy apply
                (<a href=\"";
        // line 344
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_privacy_policy");
        echo "\">Privacy Policy</a>).</p>

            <p>12.3 If you end your membership, no
                payments for membership fees will be refunded.</p>

            <p>12.4 If you have ended your membership you
                are not allowed to register a new account at ";
        // line 350
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website without
                the written consent of ";
        // line 351
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ".</p>

            <h2>13. Liability and Limitation Thereof</h2>

            <p>13.1 ";
        // line 355
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " undertakes to inform
                ";
        // line 356
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners and subcontractors in accordance with Sections 2,5 and
                6 above. ";
        // line 357
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " does not, however, undertake any liability for
                ";
        // line 358
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners' and subcontractors' compliance with such information
                and/or ";
        // line 359
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners' and subcontractors' use of Your Photographs.</p>

            <p>13.2 ";
        // line 361
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners is relieved from liability from any consequences of hacker attacks or
                otherwise unauthorised access to their servers or databases, which results in a
                third party's possession or spreading of any personal information, Photographs,
                image files other material or information.</p>

            <p>13.3 ";
        // line 367
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners are relieved from liability from any consequences if ";
        // line 368
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                or ";
        // line 369
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Partners' website or any of ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s services or
                products (including, but not limited to, personal homepages) are unavailable
                for any period of time.</p>

            <p>13.4 ";
        // line 373
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners are relieved from liability from any consequences of errors or faults
                in ";
        // line 375
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website or its services (including, but not limited to,
                personal homepages and prints sales).</p>

            <p>13.5 ";
        // line 378
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners are relieved from liability from any consequences of any illegal,
                slandering, offending, discriminating or defamatory comments or other material
                provided by its members. You are personally responsible for any such comments
                or material which You submit to ";
        // line 382
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>13.6 ";
        // line 384
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                Partners are relieved from liability for a failure to perform an obligation under
                this Agreement if such failure is due to a circumstance of the type stated
                below (Relieving circumstance) and the circumstance prevents or makes
                substantially more difficult the timely performance of such obligation. A
                Relieving circumstance shall be deemed to include inter alia acts or omissions
                of authorities, new or amended legislation, leaving of personnel, illness or
                other reduction of work capacity, death, conflicts on the labour market,
                blockade, fire, flood, war, loss or destruction of property or data of major
                significance or a major accident.</p>

            <p>13.7 ";
        // line 395
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " AND ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'S
                PARTNERS SHALL IN NO EVENT BE LIABLE TOWARDS YOU FOR ANY INDIRECT OR
                CONSEQUENTIAL DAMAGES, INCLUDING, BUT NOT LIMITED TO, LOSS OF PROFITS OR LOSS
                OF ANY DATA, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT OR SERVICES OR
                PRODUCTS PROVIDED BY ";
        // line 399
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " OR ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'S Partners.</p>

            <p>13.8 ";
        // line 401
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'S AND ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                PARTNERS AGGREGATE LIABILITY ARISING OUT OF OR RELATING TO THIS AGREEMENT,
                REGARDLESS OF THE FORM OF ACTION, WHETHER FOR BREACH OF WARRANTY, CONTRACT,
                TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, SHALL NOT EXCEED BGN 1000.</p>

            <h2>14. Termination</h2>

            <p>14.1 If you have ended your membership and
                Your Account has been deleted, this Agreement is terminated. Sections 2.5 and
                2.6 (License grant), Section 5 (Warranties and Indemnifications), Section 13
                (Liability and limitation thereof), Section 17 (Legal disputes) and Section 18
                (Miscellaneous) survive termination of this Agreement.</p>

            <p>14.2 If Your Account has been deleted by
                ";
        // line 415
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " in accordance with any of the provisions in this Agreement, this
                Agreement is immediately terminated. Sections 2.5 and 2.6 (License Grant),
                Section 5 (Warranties and Indemnifications), Section 13 (Liability and Limitation
                Thereof), Section 17 (Legal Disputes) and Section 18 (Miscellaneous) survive
                termination of this Agreement.</p>

            <p>14.3 If this Agreement has been terminated
                in accordance with Section 14.1 or 14.2 above, you are not allowed to register
                a new account at ";
        // line 423
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website.</p>

            <p>14.4 If this Agreement is terminated no
                money for paid membership fees will be refunded to You.</p>

            <h2>15. Refunds of Membership Fees</h2>

            <p>15.1 Once a payment for membership fees has
                been made to ";
        // line 431
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ", no money will be refunded, except if You have made
                a double payment by mistake. In such a case one of the payments will be
                refunded upon a request made to ";
        // line 433
        echo twig_escape_filter($this->env, ($context["site_email"] ?? null), "html", null, true);
        echo " within 14 days from
                when the payment was made. However, ";
        // line 434
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " will comply with mandatory
                law.</p>

            <p>15.2 If Your Account has been banned or
                deleted in accordance with any of the provisions under this Agreement, no money
                for membership fees will be refunded.</p>

            <h2>16. Changes</h2>

            <p>16.1 ";
        // line 443
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " may at any time change
                the provisions of this Agreement. ";
        // line 444
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " shall notify you of any
                changes when you are logged in to ";
        // line 445
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s website and by sending an
                email to your registered email address in Your Settings Page. You will be
                requested to accept such changes, included in a new agreement on ";
        // line 447
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s
                website. If you decline the new agreement, this Agreement is terminated and the
                terms and conditions for termination under Section 15 apply. If You do not
                either accept or decline the new agreement within 30 days, the new agreement
                will replace this Agreement and You will be bound by the new agreement.</p>

            <h2>17. Legal Disputes</h2>

            <p>17.1 Any dispute which may arise between
                You and ";
        // line 456
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " regarding the terms and conditions of this Agreement
                shall first be settled by friendly arrangements after discussions with our
                customer support. If and when such a dispute is not settled by such
                arrangements, any dispute, controversy or claim arising out of or in connection
                with this Agreement, or the breach, termination or invalidity thereof, shall be
                finally settled by arbitration in accordance with the Arbitration Rules of the
                Arbitration Institute of the Bulgaria Chamber of Commerce. The arbitral
                tribunal shall be composed of a sole arbitrator. The seat of arbitration shall
                be Sofia, Bulgaria. The language of the arbitration shall be English. This
                Agreement shall be governed by the substantive law of Bulgaria, disregarding
                conflict of law provisions.</p>

            <h2>18. Miscellaneous</h2>

            <p>18.1 The parties are independent entities
                and neither shall be considered an agent, employee, commercial representative,
                partner, franchisee or joint venturer of the other. Neither party shall have
                any authority, absent express written permission from the other party, to enter
                into any agreement, assume or create any obligations or liabilities, or make
                representations on behalf of the other party not stated in this Agreement.</p>

            <p>18.2 To the extent any provision in this
                Agreement is deemed illegal, void or unenforceable, that provision shall not
                affect the remaining provisions of this Agreement.</p>

            <p>18.3 This Agreement, including the Privacy
                Policy (<a href=\"";
        // line 482
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_privacy_policy");
        echo "\">Privacy Policy</a>) and Terms of Service
                (<a href=\"";
        // line 483
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_tos");
        echo "\">Terms of Service</a>), incorporates the entire understanding
                between the parties. This Agreement supersedes any prior agreements between the
                parties, whether made orally, in writing or otherwise. No modification,
                amendment or waiver of any provision of this Agreement shall be binding unless
                executed in writing by both parties. Notwithstanding the above, ";
        // line 487
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "
                may change the terms and conditions of the Privacy Policy and/or Terms of
                Service in accordance with the provisions regarding changes in the Privacy
                Policy and Terms of Service.</p>

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "pages/user-agreement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  920 => 487,  913 => 483,  909 => 482,  880 => 456,  868 => 447,  863 => 445,  859 => 444,  855 => 443,  843 => 434,  839 => 433,  834 => 431,  823 => 423,  812 => 415,  793 => 401,  786 => 399,  777 => 395,  761 => 384,  756 => 382,  747 => 378,  741 => 375,  734 => 373,  725 => 369,  721 => 368,  715 => 367,  704 => 361,  699 => 359,  695 => 358,  691 => 357,  687 => 356,  683 => 355,  676 => 351,  672 => 350,  663 => 344,  640 => 324,  630 => 317,  624 => 314,  617 => 310,  612 => 308,  608 => 307,  602 => 304,  597 => 302,  593 => 301,  584 => 295,  580 => 294,  575 => 292,  568 => 288,  562 => 285,  557 => 283,  543 => 272,  538 => 270,  532 => 267,  527 => 265,  520 => 261,  516 => 260,  509 => 256,  501 => 251,  491 => 244,  487 => 243,  480 => 239,  472 => 234,  463 => 228,  458 => 226,  453 => 224,  445 => 219,  440 => 217,  432 => 212,  422 => 205,  417 => 203,  411 => 200,  407 => 199,  402 => 197,  395 => 193,  385 => 186,  380 => 184,  371 => 178,  352 => 162,  345 => 158,  339 => 157,  312 => 135,  301 => 129,  292 => 123,  288 => 122,  279 => 118,  274 => 116,  270 => 115,  262 => 112,  256 => 111,  250 => 108,  246 => 107,  240 => 104,  236 => 103,  229 => 99,  220 => 93,  211 => 87,  199 => 78,  191 => 75,  186 => 73,  180 => 70,  175 => 68,  165 => 61,  154 => 53,  148 => 50,  140 => 45,  132 => 42,  125 => 38,  119 => 37,  115 => 36,  95 => 21,  89 => 20,  83 => 17,  76 => 12,  72 => 11,  67 => 8,  63 => 7,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "pages/user-agreement.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/pages/user-agreement.html.twig");
    }
}
