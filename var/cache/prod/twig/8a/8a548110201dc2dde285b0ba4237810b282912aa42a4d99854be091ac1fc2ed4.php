<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/template.html.twig */
class __TwigTemplate_ff3e02aad9c02f3a6690c31fe83588ba79efacd45073e1bb8a7f5a476b611818 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p></p>
<div class=\"media pt-3 pl-3 pb-3 border-left mt-5\">
    <div class=\"media-body\">
        <h6 class=\"mb-3\">
            <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("account", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["opinion"] ?? null), "user", [], "any", false, false, false, 5), "slug", [], "any", false, false, false, 5)]), "html", null, true);
        echo "\">
                ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["opinion"] ?? null), "user", [], "any", false, false, false, 6), "profile", [], "any", false, false, false, 6), "firstName", [], "any", false, false, false, 6), "html", null, true);
        echo "
                ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["opinion"] ?? null), "user", [], "any", false, false, false, 7), "profile", [], "any", false, false, false, 7), "lastName", [], "any", false, false, false, 7), "html", null, true);
        echo "
            </a>

        </h6>
        <div class=\"text-muted float-right\" style=\"float: right;margin: -30px 0 0 \">
            ";
        // line 12
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["opinion"] ?? null), "createdAt", [], "any", false, false, false, 12), "Y-m-d H:i"), "html", null, true);
        echo "
        </div>
        <div>
            ";
        // line 15
        echo twig_get_attribute($this->env, $this->source, ($context["opinion"] ?? null), "text", [], "any", false, false, false, 15);
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "forum/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 15,  59 => 12,  51 => 7,  47 => 6,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "forum/template.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/forum/template.html.twig");
    }
}
