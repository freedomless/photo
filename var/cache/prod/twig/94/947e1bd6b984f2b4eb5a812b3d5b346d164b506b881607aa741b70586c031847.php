<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/success.html.twig */
class __TwigTemplate_41337d8ad3fec6eb461240cd2d857afde1afd0bf1983018308d983e4526c6cb7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'style' => [$this, 'block_style'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "store/success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    Invoice
";
    }

    // line 5
    public function block_style($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <style>
        @media print {
            .navigation-bar {
                display: none !important;
            }
        }
    </style>
";
    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "
    <div class=\"container mt-5 mb-5\" id=\"pdf\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card shadow border-0\">
                    <div class=\"card-body p-0\">
                        <div class=\"row p-5\">
                            <div class=\"col-md-6\">
                                <h3>Invoice</h3>
                                <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" width=\"50px\">
                            </div>
                            <div class=\"col-md-6 text-right\">
                                <p class=\"fmb-1\">
                                    <button class=\"btn btn-blue\" onclick=\"window.print()\">
                                        <i class=\"fas fa-print\"></i>
                                    </button>
                                    <button class=\"btn btn-blue\" id=\"download-pdf\">
                                        <i class=\"far fa-file-pdf\"></i>
                                    </button>
                                </p>
                                <p class=\"font-weight-bold mb-1\">Receipt #";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "id", [], "any", false, false, false, 35), "html", null, true);
        echo "</p>
                                <p class=\"font-weight-bold mb-1\">Order #";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 36), "token", [], "any", false, false, false, 36), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <hr class=\"my-5\">
                        <div class=\"row pb-5 p-5\">
                            <div class=\"col-md-6\">
                                <p class=\"font-weight-bold mb-4\">Client Information</p>
                                <p class=\"mb-1\">";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 43), "billingAddress", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "</p>
                                <p class=\"mb-1\">";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 44), "billingAddress", [], "any", false, false, false, 44), "city", [], "any", false, false, false, 44), "html", null, true);
        echo "
                                    , ";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 45), "billingAddress", [], "any", false, false, false, 45), "country", [], "any", false, false, false, 45), "html", null, true);
        echo "</p>
                                <p class=\"mb-1\">";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 46), "billingAddress", [], "any", false, false, false, 46), "zip", [], "any", false, false, false, 46), "html", null, true);
        echo "</p>
                            </div>
                            <div class=\"col-md-3\"></div>
                            <div class=\"col-md-3\">
                                <p class=\"font-weight-bold mb-4\">Payment Details</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">VAT: </span> ";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vatNumber", [], "any", false, false, false, 52), "html", null, true);
        echo "</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Payment Type: </span> ";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 54), "transaction", [], "any", false, false, false, 54), "gateway", [], "any", false, false, false, 54), "html", null, true);
        echo "
                                </p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Name: </span> ";
        // line 57
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "name", [], "any", false, false, false, 57), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <div class=\"row p-5\">
                            <div class=\"col-md-12\">
                                <table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">ID</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Media</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Size</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Quantity</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ";
        // line 75
        $context["subtotal"] = 0;
        // line 76
        echo "                                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 76), "items", [], "any", false, false, false, 76));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 77
            echo "                                            ";
            $context["subtotal"] = ((($context["subtotal"] ?? null) + twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 77)) * twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 77));
            // line 78
            echo "                                            <tr>
                                                <td>";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 79), "id", [], "any", false, false, false, 79), "html", null, true);
            echo "</td>
                                                <td>";
            // line 80
            echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", true, true, false, 80) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", false, false, false, 80)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", false, false, false, 80)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 80), "post", [], "any", false, false, false, 80), "id", [], "any", false, false, false, 80))), "html", null, true);
            echo "</td>
                                                <td>";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "material", [], "any", false, false, false, 81), "name", [], "any", false, false, false, 81), "html", null, true);
            echo "</td>
                                                <td>";
            // line 82
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 82), "width", [], "any", false, false, false, 82), "html", null, true);
            echo "x";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 82), "height", [], "any", false, false, false, 82), "html", null, true);
            echo "</td>
                                                <td>";
            // line 83
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 83), "html", null, true);
            echo "</td>
                                                <td>";
            // line 84
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 84), 2), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 84), "html", null, true);
            echo "</td>
                                                <td>";
            // line 85
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 85) * twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 85)), 2), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 85), "html", null, true);
            echo "</td>
                                                <td>
                                                </td>
                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "                                        <tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class=\"d-flex flex-row-reverse bg-dark text-white p-4\">
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Grand Total</div>
                                <div class=\"h2 font-weight-light\">
                                    ";
        // line 99
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 99), "transaction", [], "any", false, false, false, 99), "paymentGross", [], "any", false, false, false, 99), 2), "html", null, true);
        echo "
                                    ";
        // line 100
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 100), "transaction", [], "any", false, false, false, 100), "currencyCode", [], "any", false, false, false, 100), "html", null, true);
        echo "
                                </div>
                            </div>

                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Tax</div>
                                <div class=\"h2 font-weight-light\">";
        // line 106
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "tax", [], "any", false, false, false, 106) * ($context["subtotal"] ?? null)), 2), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 106), "html", null, true);
        echo "</div>
                            </div>
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Sub - Total amount</div>
                                <div class=\"h2 font-weight-light\">";
        // line 110
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["subtotal"] ?? null), 2), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "currency", [], "any", false, false, false, 110), "html", null, true);
        echo "</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 120
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 121
        echo "    <script src=\"//cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.1/jspdf.debug.js\" crossorigin=\"anonymous\"></script>
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js\" crossorigin=\"anonymous\"></script>

    <script>
        \$('#download-pdf').on('click',function(){

            html2canvas(document.querySelector('#pdf'), {imageTimeout: 5000, useCORS: true}).then(canvas => {
                document.getElementById('pdf').appendChild(canvas)
                let img = canvas.toDataURL('image/png')
                let pdf = new jsPDF('landscape')
                pdf.addImage(img, 'JPEG', 0, 0, 300, 210)
                pdf.save('order-";
        // line 132
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["invoice"] ?? null), "order", [], "any", false, false, false, 132), "token", [], "any", false, false, false, 132), "html", null, true);
        echo ".pdf')
                \$('canvas').remove();
            })

        })
    </script>

";
    }

    public function getTemplateName()
    {
        return "store/success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 132,  276 => 121,  272 => 120,  256 => 110,  247 => 106,  238 => 100,  234 => 99,  223 => 90,  210 => 85,  204 => 84,  200 => 83,  194 => 82,  190 => 81,  186 => 80,  182 => 79,  179 => 78,  176 => 77,  171 => 76,  169 => 75,  148 => 57,  142 => 54,  137 => 52,  128 => 46,  124 => 45,  120 => 44,  116 => 43,  106 => 36,  102 => 35,  88 => 24,  77 => 15,  73 => 14,  62 => 6,  58 => 5,  53 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "store/success.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/success.html.twig");
    }
}
