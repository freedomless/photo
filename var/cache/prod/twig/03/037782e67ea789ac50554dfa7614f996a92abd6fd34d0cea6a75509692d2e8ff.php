<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/settings/index.html.twig */
class __TwigTemplate_fc5ae1a50e9bc85b3969cf75c1a70c2dc4808571c09a5c64cc93d9797993a93c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "user/settings/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Account Settings";
    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css\">
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"profile-settings-page\">

        <div class=\"container-fluid profile\" style=\"max-width: 1200px\">

            <div class=\"row\">

                <div class=\"cover\">

                    <label for=\"cover\" class=\"btn btn-warning\">
                        <input id=\"cover\" type=\"file\" accept=\"image/jpeg\"/>
                        Upload
                    </label>

                    <div class=\"options\">

                        <button class=\"btn btn-primary\" id=\"save\" type=\"button\">
                            <i class=\"fas fa-check\"></i>
                        </button>

                        <button class=\"btn btn-danger shadow\" id=\"cancel\" type=\"button\">
                            <i class=\"fas fa-times\"></i>
                        </button>

                    </div>

                    <small class=\"text-muted bg-white p-2 rounded  position-absolute\" style=\"right:10px;top:10px;\">
                        Recommended size: 1200x350 px
                    </small>

                    <img src=\"";
        // line 40
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", true, true, false, 40) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", false, false, false, 40)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", false, false, false, 40)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/cover.jpg"))), "html", null, true);
        echo "\" id=\"cover-image\"
                         alt=\"Cover\"/>

                    <img src=\"\" id=\"cover-image-cropper\" alt=\"Cover\"/>

                    <div class=\"dots-loader\"></div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

            <div class=\"row\" style=\"padding-bottom: 3.5rem\">

                <div class=\"col-lg-2\">

                    <div class=\"profile-avatar profile-avatar-settings circle text-center\">

                        <div class=\"avatar-loader\"></div>

                        <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", true, true, false, 63) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", false, false, false, 63)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", false, false, false, 63)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
        echo "\"
                             alt=\"Profile Picture\"
                             class=\"shadow circle\">

                        <input type=\"file\" id=\"avatar\" accept=\"image/jpeg,image/png\" class=\"circle\"/>

                    </div>

                </div>

                <div class=\"col-lg-10\">

                    <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                        <h1>
                            <strong>
                                ";
        // line 79
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 79), "firstName", [], "any", false, false, false, 79), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 79), "lastName", [], "any", false, false, false, 79), "html", null, true);
        echo "
                            </strong>
                        </h1>


                        <div class=\"mt-3\">

                            <i class=\"fa fa-lock\"></i>
                            <a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_change_password");
        echo "\">
                                Change Password
                            </a>

                            <i class=\"fas fa-wrench ml-4\"></i>
                            <a href=\"";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_settings");
        echo "\">
                                Site Settings
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout mt-5\">

            ";
        // line 108
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

            <div class=\"row default-form-layout\">

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Personal Details:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 119), "firstName", [], "any", false, false, false, 119), 'label');
        echo "
                        ";
        // line 120
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 120), "firstName", [], "any", false, false, false, 120), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 124), "lastName", [], "any", false, false, false, 124), 'label');
        echo "
                        ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 125), "lastName", [], "any", false, false, false, 125), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    ";
        // line 128
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "balance", [], "any", true, true, false, 128)) {
            // line 129
            echo "                        <div class=\"form-group\">
                            ";
            // line 130
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "balance", [], "any", false, false, false, 130), "payout", [], "any", false, false, false, 130), 'label', ["label" => "Paypal Email"]);
            echo "
                            ";
            // line 131
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "balance", [], "any", false, false, false, 131), "payout", [], "any", false, false, false, 131), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "
                        </div>
                    ";
        }
        // line 134
        echo "
                    <h3 class=\"text-muted\">Social:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 140
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 140), "facebook", [], "any", false, false, false, 140), 'label');
        echo "
                        ";
        // line 141
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 141), "facebook", [], "any", false, false, false, 141), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 145
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 145), "twitter", [], "any", false, false, false, 145), 'label');
        echo "
                        ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 146), "twitter", [], "any", false, false, false, 146), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 150), "instagram", [], "any", false, false, false, 150), 'label');
        echo "
                        ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 151), "instagram", [], "any", false, false, false, 151), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 155
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 155), "website", [], "any", false, false, false, 155), 'label');
        echo "
                        ";
        // line 156
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 156), "website", [], "any", false, false, false, 156), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                </div>

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Address:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 168
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 168), "nationality", [], "any", false, false, false, 168), 'label');
        echo "
                        ";
        // line 169
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 169), "nationality", [], "any", false, false, false, 169), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 173
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 173), "address", [], "any", false, false, false, 173), "country", [], "any", false, false, false, 173), 'label');
        echo "
                        ";
        // line 174
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 174), "address", [], "any", false, false, false, 174), "country", [], "any", false, false, false, 174), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 178
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 178), "address", [], "any", false, false, false, 178), "county", [], "any", false, false, false, 178), 'label');
        echo "
                        ";
        // line 179
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 179), "address", [], "any", false, false, false, 179), "county", [], "any", false, false, false, 179), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 183
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 183), "address", [], "any", false, false, false, 183), "address1", [], "any", false, false, false, 183), 'label');
        echo "
                        ";
        // line 184
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 184), "address", [], "any", false, false, false, 184), "address1", [], "any", false, false, false, 184), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 188
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 188), "address", [], "any", false, false, false, 188), "address2", [], "any", false, false, false, 188), 'label');
        echo "
                        ";
        // line 189
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 189), "address", [], "any", false, false, false, 189), "address2", [], "any", false, false, false, 189), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 193
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 193), "address", [], "any", false, false, false, 193), "zip", [], "any", false, false, false, 193), 'label');
        echo "
                        ";
        // line 194
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 194), "address", [], "any", false, false, false, 194), "zip", [], "any", false, false, false, 194), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 198
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 198), "address", [], "any", false, false, false, 198), "city", [], "any", false, false, false, 198), 'label');
        echo "
                        ";
        // line 199
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 199), "address", [], "any", false, false, false, 199), "city", [], "any", false, false, false, 199), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 203
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 203), "address", [], "any", false, false, false, 203), "phone", [], "any", false, false, false, 203), 'label');
        echo "
                        ";
        // line 204
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, false, false, 204), "address", [], "any", false, false, false, 204), "phone", [], "any", false, false, false, 204), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                </div>

            </div>

            <div class=\"row\">

                <div class=\"col-12 text-center\">

                    <hr>

                    <button class=\"btn btn-info\">
                        Save
                    </button>

                </div>

            </div>

            ";
        // line 225
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

        </div>

    </div>

";
    }

    // line 233
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 234
        echo "    <script src=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js\"></script>
    <script src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/cover-cropper.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "user/settings/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 235,  416 => 234,  412 => 233,  401 => 225,  377 => 204,  373 => 203,  366 => 199,  362 => 198,  355 => 194,  351 => 193,  344 => 189,  340 => 188,  333 => 184,  329 => 183,  322 => 179,  318 => 178,  311 => 174,  307 => 173,  300 => 169,  296 => 168,  281 => 156,  277 => 155,  270 => 151,  266 => 150,  259 => 146,  255 => 145,  248 => 141,  244 => 140,  236 => 134,  230 => 131,  226 => 130,  223 => 129,  221 => 128,  215 => 125,  211 => 124,  204 => 120,  200 => 119,  186 => 108,  167 => 92,  159 => 87,  146 => 79,  127 => 63,  101 => 40,  69 => 10,  65 => 9,  60 => 6,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/settings/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/settings/index.html.twig");
    }
}
