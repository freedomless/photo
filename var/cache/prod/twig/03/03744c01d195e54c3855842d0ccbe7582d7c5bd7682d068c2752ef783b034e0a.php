<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/followers.html.twig */
class __TwigTemplate_cfe9042329466bc818c9a476a1ec31df39f8f78c3ff830efa4c7cfd868b1a20a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/followers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "'s
    Followers
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
        <div class=\"col-12 members\">

            ";
        // line 13
        if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "followers", [], "any", false, false, false, 13)), 0))) {
            // line 14
            echo "
                <h3 class=\"text-center text-muted mt-5\">
                    No Followers.
                </h3>

            ";
        } else {
            // line 20
            echo "
                ";
            // line 21
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteUsers", ["rendering" => "client_side", "props" => ["page" => "followers", "user" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 21)]]);
            echo "

            ";
        }
        // line 24
        echo "
        </div>

";
    }

    public function getTemplateName()
    {
        return "user/profile/followers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 24,  85 => 21,  82 => 20,  74 => 14,  72 => 13,  67 => 10,  63 => 9,  56 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/followers.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/followers.html.twig");
    }
}
