<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/settings/general.html.twig */
class __TwigTemplate_f68c41d39347ec367779d9afd6d14b9013773c2b330a3d6955411fa93d935f7c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "user/settings/general.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Site Settings";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Site Settings";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <a href=\"";
        // line 9
        (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 9), "session", [], "any", false, true, false, 9), "get", [0 => "back"], "method", true, true, false, 9) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 9), "session", [], "any", false, true, false, 9), "get", [0 => "back"], "method", false, false, false, 9)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 9), "session", [], "any", false, true, false, 9), "get", [0 => "back"], "method", false, false, false, 9), "html", null, true))) : (print ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_edit"))));
        echo "\">
        <i class=\"fas fa-times\"></i>
    </a>

";
    }

    // line 15
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "

    <div class=\"col-12\">
        ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "

        <h6>Notifications:</h6>
        <hr class=\"no-gutter\">
        <label class=\"custom-control custom-checkbox\">
            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isEmailNotificationAllowed", [], "any", false, false, false, 24), 'widget', ["attr" => ["class" => "custom-control-input"]]);
        echo "
            <span class=\"custom-control-indicator\"></span>
            Receive Email notifications
        </label>

        <label class=\"custom-control custom-checkbox\">
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isWebNotificationAllowed", [], "any", false, false, false, 30), 'widget', ["attr" => ["class" => "custom-control-input"]]);
        echo "
            <span class=\"custom-control-indicator\"></span>
            Receive Web notifications (Website Notifications)
        </label>

        <label class=\"custom-control custom-checkbox\">
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isPushNotificationAllowed", [], "any", false, false, false, 36), 'widget', ["attr" => ["class" => "custom-control-input"]]);
        echo "
            <span class=\"custom-control-indicator\"></span>
            Receive Push notifications (Phone App Notifications)
        </label>

        <h6 class=\"mt-5\">General settings:</h6>
        <hr class=\"no-gutter\">
        <label class=\"custom-control custom-checkbox\">
            ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "showNudes", [], "any", false, false, false, 44), 'widget', ["attr" => ["class" => "custom-control-input"]]);
        echo "
            <span class=\"custom-control-indicator\"></span>
            Show nude content
        </label>

        <label class=\"custom-control custom-checkbox\">
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isPortfolioVisible", [], "any", false, false, false, 50), 'widget', ["attr" => ["class" => "custom-control-input"]]);
        echo "
            <span class=\"custom-control-indicator\"></span>
            Visible Uploads
        </label>

        <hr>

        <button class=\"btn btn-info\">Save</button>

        ";
        // line 59
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>


";
    }

    public function getTemplateName()
    {
        return "user/settings/general.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 59,  134 => 50,  125 => 44,  114 => 36,  105 => 30,  96 => 24,  88 => 19,  83 => 16,  79 => 15,  70 => 9,  67 => 8,  63 => 7,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/settings/general.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/settings/general.html.twig");
    }
}
