<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @EasyAdmin/default/paginator.html.twig */
class __TwigTemplate_af9b2fed77e7d7a3f508ceea41a732a8856be1564e2730f74c110ede86e6f09f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "
";
        // line 4
        $context["_paginator_request_parameters"] = twig_array_merge(($context["_request_parameters"] ?? null), ["referer" => null]);
        // line 5
        echo "
";
        // line 6
        if (twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "haveToPaginate", [], "any", false, false, false, 6)) {
            // line 7
            echo "    <div class=\"list-pagination\">
        <div class=\"list-pagination-counter\">
            ";
            // line 9
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.counter", ["%start%" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPageOffsetStart", [], "any", false, false, false, 9), "%end%" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPageOffsetEnd", [], "any", false, false, false, 9), "%results%" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "nbResults", [], "any", false, false, false, 9)], "EasyAdminBundle");
            echo " ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("easy_admin.results", [], "EasyAdminBundle"), "html", null, true);
            echo "
        </div>

        <nav class=\"pager list-pagination-paginator ";
            // line 12
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasPreviousPage", [], "any", false, false, false, 12)) ? ("first-page") : (""));
            echo " ";
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 12)) ? ("last-page") : (""));
            echo "\">
            <ul class=\"pagination list-pagination-paginator ";
            // line 13
            echo (((0 === twig_compare(1, twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 13)))) ? ("first-page") : (""));
            echo " ";
            echo ((twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 13)) ? ("") : ("last-page"));
            echo "\">
                <li class=\"page-item ";
            // line 14
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasPreviousPage", [], "any", false, false, false, 14)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 16
            (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasPreviousPage", [], "any", false, false, false, 16)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => 1])), "html", null, true))));
            echo "\">
                        <i class=\"fa fa-angle-double-left mx-1\"></i><span class=\"btn-label\">";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.first", [], "EasyAdminBundle"), "html", null, true);
            echo "</span>
                    </a>
                </li>

                <li class=\"page-item ";
            // line 21
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasPreviousPage", [], "any", false, false, false, 21)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 23
            (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasPreviousPage", [], "any", false, false, false, 23)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "previousPage", [], "any", false, false, false, 23)])), "html", null, true))));
            echo "\">
                        <i class=\"fa fa-angle-left mx-1\"></i> <span class=\"btn-label\">";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.previous", [], "EasyAdminBundle"), "html", null, true);
            echo "</span>
                    </a>
                </li>

                ";
            // line 29
            echo "                ";
            // line 30
            echo "                ";
            $context["nearbyPagesLimit"] = 3;
            // line 31
            echo "
                ";
            // line 32
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 32), 1))) {
                // line 33
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range((twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 33) - ($context["nearbyPagesLimit"] ?? null)), (twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 33) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 34
                    echo "                        ";
                    if ((1 === twig_compare($context["i"], 0))) {
                        // line 35
                        echo "                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"";
                        // line 37
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => $context["i"]])), "html", null, true);
                        echo "\">
                                    <span class=\"btn-label\">";
                        // line 38
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "</span>
                                </a>
                            </li>
                        ";
                    }
                    // line 42
                    echo "                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "                ";
            }
            // line 44
            echo "                <li class=\"page-item\">
                    <a class=\"page-link current\" style=\"color: red\">";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 45), "html", null, true);
            echo "</a>
                </li>
                ";
            // line 47
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 47), twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "nbPages", [], "any", false, false, false, 47)))) {
                // line 48
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range((twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 48) + 1), (twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "currentPage", [], "any", false, false, false, 48) + ($context["nearbyPagesLimit"] ?? null))));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 49
                    echo "                        ";
                    if ((0 >= twig_compare($context["i"], twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "nbPages", [], "any", false, false, false, 49)))) {
                        // line 50
                        echo "                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"";
                        // line 52
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => $context["i"]])), "html", null, true);
                        echo "\">
                                    <span class=\"btn-label\">";
                        // line 53
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "</span>
                                </a>
                            </li>
                        ";
                    }
                    // line 57
                    echo "                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                ";
            }
            // line 59
            echo "
                ";
            // line 61
            echo "
                <li class=\"page-item ";
            // line 62
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 62)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 64
            (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 64)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "nextPage", [], "any", false, false, false, 64)])), "html", null, true))));
            echo "\">
                        <span class=\"btn-label\">";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.next", [], "EasyAdminBundle"), "html", null, true);
            echo "</span> <i class=\"fa fa-angle-right mx-1\"></i>
                    </a>
                </li>

                <li class=\"page-item ";
            // line 69
            echo (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 69)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 71
            (( !twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "hasNextPage", [], "any", false, false, false, 71)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge(($context["_paginator_request_parameters"] ?? null), ["page" => twig_get_attribute($this->env, $this->source, ($context["paginator"] ?? null), "nbPages", [], "any", false, false, false, 71)])), "html", null, true))));
            echo "\">
                        <span class=\"btn-label\">";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.last", [], "EasyAdminBundle"), "html", null, true);
            echo "</span> <i class=\"fa fa-angle-double-right\"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 72,  214 => 71,  209 => 69,  202 => 65,  198 => 64,  193 => 62,  190 => 61,  187 => 59,  184 => 58,  178 => 57,  171 => 53,  167 => 52,  163 => 50,  160 => 49,  155 => 48,  153 => 47,  148 => 45,  145 => 44,  142 => 43,  136 => 42,  129 => 38,  125 => 37,  121 => 35,  118 => 34,  113 => 33,  111 => 32,  108 => 31,  105 => 30,  103 => 29,  96 => 24,  92 => 23,  87 => 21,  80 => 17,  76 => 16,  71 => 14,  65 => 13,  59 => 12,  51 => 9,  47 => 7,  45 => 6,  42 => 5,  40 => 4,  37 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "@EasyAdmin/default/paginator.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/bundles/EasyAdminBundle/default/paginator.html.twig");
    }
}
