<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/product_show.html.twig */
class __TwigTemplate_5695df1a10c13627d0a9f148f12aa0f8e49336648acc574513bc86d9927eb6bc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'show_fields' => [$this, 'block_show_fields'],
            'show_field' => [$this, 'block_show_field'],
            'item_actions' => [$this, 'block_item_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@EasyAdmin/default/show.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@EasyAdmin/default/show.html.twig", "admin/easyadmin/product_show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "
    <div class=\"form-horizontal\">

        ";
        // line 8
        $context["_fields_visible_by_user"] = twig_array_filter($this->env, ($context["fields"] ?? null), function ($__metadata__, $__field__) use ($context, $macros) { $context["metadata"] = $__metadata__; $context["field"] = $__field__; return $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->isGranted(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "permission", [], "any", false, false, false, 8)); });
        // line 9
        echo "
        ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, ($context["_fields_visible_by_user"] ?? null), "post", [], "any", true, true, false, 10)) {
            // line 11
            echo "
            ";
            // line 12
            echo $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->renderEntityField($this->env, "show", twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", [], "any", false, false, false, 12), ($context["entity"] ?? null), twig_get_attribute($this->env, $this->source, ($context["_fields_visible_by_user"] ?? null), "post", [], "any", false, false, false, 12));
            echo "

        ";
        }
        // line 15
        echo "
        <hr>

        ";
        // line 18
        $this->displayBlock('show_fields', $context, $blocks);
        // line 47
        echo "    </div>


    <section class=\"content-footer\">
        <div class=\"form-actions\">
            ";
        // line 52
        $this->displayBlock('item_actions', $context, $blocks);
        // line 66
        echo "        </div>
    </section>


";
    }

    // line 18
    public function block_show_fields($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "            ";
        $context["_fields_visible_by_user"] = twig_array_filter($this->env, ($context["fields"] ?? null), function ($__metadata__, $__field__) use ($context, $macros) { $context["metadata"] = $__metadata__; $context["field"] = $__field__; return $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->isGranted(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "permission", [], "any", false, false, false, 19)); });
        // line 20
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["_fields_visible_by_user"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["field"] => $context["metadata"]) {
            // line 21
            echo "                ";
            $context["continue"] = false;
            // line 22
            echo "                ";
            $this->displayBlock('show_field', $context, $blocks);
            // line 45
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field'], $context['metadata'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        ";
    }

    // line 22
    public function block_show_field($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "                    ";
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "label", [], "any", false, false, false, 23), "Post"))) {
            // line 24
            echo "                        ";
            $context["continue"] = true;
            // line 25
            echo "                    ";
        }
        // line 26
        echo "                    ";
        if ((0 === twig_compare(($context["continue"] ?? null), false))) {
            // line 27
            echo "                        <div class=\"form-group field-";
            echo twig_escape_filter($this->env, twig_lower_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "type", [], "any", true, true, false, 27)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "type", [], "any", false, false, false, 27), "default")) : ("default"))), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "css_class", [], "any", true, true, false, 27)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "css_class", [], "any", false, false, false, 27), "")) : ("")), "html", null, true);
            echo "\">
                            <label class=\"control-label\">
                                ";
            // line 29
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "label", [], "any", false, false, false, 29), ($context["_trans_parameters"] ?? null));
            echo "
                            </label>
                            <div class=\"form-widget\">
                                <div class=\"form-control\">
                                    ";
            // line 33
            echo $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->renderEntityField($this->env, "show", twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", [], "any", false, false, false, 33), ($context["entity"] ?? null), ($context["metadata"] ?? null));
            echo "
                                </div>

                                ";
            // line 36
            if ((0 !== twig_compare(((twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "help", [], "any", true, true, false, 36)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "help", [], "any", false, false, false, 36), "")) : ("")), ""))) {
                // line 37
                echo "                                    <small class=\"form-help\"><i
                                                class=\"fa fa-fw fa-info-circle\"></i> ";
                // line 38
                echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["metadata"] ?? null), "help", [], "any", false, false, false, 38));
                echo "
                                    </small>
                                ";
            }
            // line 41
            echo "                            </div>
                        </div>
                    ";
        }
        // line 44
        echo "                ";
    }

    // line 52
    public function block_item_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "                ";
        $context["_show_actions"] = $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->getActionsForItem("show", twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", [], "any", false, false, false, 53));
        // line 54
        echo "                ";
        $context["_request_parameters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 54), "query", [], "any", false, false, false, 54), "all", [], "any", false, false, false, 54);
        // line 55
        echo "
                ";
        // line 56
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_actions.html.twig", ["actions" =>         // line 57
($context["_show_actions"] ?? null), "entity_config" =>         // line 58
($context["_entity_config"] ?? null), "request_parameters" =>         // line 59
($context["_request_parameters"] ?? null), "translation_domain" => twig_get_attribute($this->env, $this->source,         // line 60
($context["_entity_config"] ?? null), "translation_domain", [], "any", false, false, false, 60), "trans_parameters" =>         // line 61
($context["_trans_parameters"] ?? null), "item_id" =>         // line 62
($context["_entity_id"] ?? null), "item" =>         // line 63
($context["entity"] ?? null)], false);
        // line 64
        echo "
            ";
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/product_show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 64,  224 => 63,  223 => 62,  222 => 61,  221 => 60,  220 => 59,  219 => 58,  218 => 57,  217 => 56,  214 => 55,  211 => 54,  208 => 53,  204 => 52,  200 => 44,  195 => 41,  189 => 38,  186 => 37,  184 => 36,  178 => 33,  171 => 29,  163 => 27,  160 => 26,  157 => 25,  154 => 24,  151 => 23,  147 => 22,  143 => 46,  129 => 45,  126 => 22,  123 => 21,  105 => 20,  102 => 19,  98 => 18,  90 => 66,  88 => 52,  81 => 47,  79 => 18,  74 => 15,  68 => 12,  65 => 11,  63 => 10,  60 => 9,  58 => 8,  53 => 5,  49 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/product_show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/product_show.html.twig");
    }
}
