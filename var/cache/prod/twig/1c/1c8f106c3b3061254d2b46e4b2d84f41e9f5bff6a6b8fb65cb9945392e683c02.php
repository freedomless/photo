<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/published.html.twig */
class __TwigTemplate_714d41512cc5ff5226401998ededf7172763d75e0f810aa35617edb791f02795 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "'s
    Published Photos
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"col-12\">

        ";
        // line 13
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 13), "get", [0 => "type"], "method", false, false, false, 13), "single"))) {
            // line 14
            echo "            ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>             // line 16
($context["nude"] ?? null), "auth" =>             // line 17
($context["auth"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" => "published", "posts" => twig_get_attribute($this->env, $this->source,             // line 22
($context["posts"] ?? null), "items", [], "any", false, false, false, 22), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,             // line 23
($context["posts"] ?? null), "total", [], "any", false, false, false, 23), 0)), "user" => twig_get_attribute($this->env, $this->source,             // line 24
($context["user"] ?? null), "id", [], "any", false, false, false, 24)]]);
            echo "

        ";
        } else {
            // line 27
            echo "            ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 0, "series" => twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "items", [], "any", false, false, false, 27), "page" => "published", "account" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 27)]]);
            echo "

        ";
        }
        // line 30
        echo "

    </div>

";
    }

    public function getTemplateName()
    {
        return "user/profile/published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 30,  86 => 27,  80 => 24,  79 => 23,  78 => 22,  77 => 17,  76 => 16,  74 => 14,  72 => 13,  67 => 10,  63 => 9,  56 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/published.html.twig");
    }
}
