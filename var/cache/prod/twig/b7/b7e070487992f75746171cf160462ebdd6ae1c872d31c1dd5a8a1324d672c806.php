<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/buttons/_pinterest_share.html.twig */
class __TwigTemplate_fec32efe560aa42d4a7302c5699cbf17cdc25ddb4f3cc7fcef0411ca9ab42a7c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<a
        target=\"_blank\"
        class=\"btn share-btn ";
        // line 3
        (((array_key_exists("classes", $context) &&  !(null === ($context["classes"] ?? null)))) ? (print (twig_escape_filter($this->env, ($context["classes"] ?? null), "html", null, true))) : (print ("")));
        echo "\"
        href=\"//pinterest.com/pin/create/button/?url=";
        // line 4
        echo twig_escape_filter($this->env, ($context["share_url"] ?? null), "html", null, true);
        echo "&media=";
        echo twig_escape_filter($this->env, ($context["media"] ?? null), "html", null, true);
        echo "&description=";
        echo twig_escape_filter($this->env, ($context["description"] ?? null), "html", null, true);
        echo "\">
    <i class=\"fab fa-pinterest-p btn-share-pinterest ";
        // line 5
        (((array_key_exists("icon_classes", $context) &&  !(null === ($context["icon_classes"] ?? null)))) ? (print (twig_escape_filter($this->env, ($context["icon_classes"] ?? null), "html", null, true))) : (print ("")));
        echo "\"></i>
</a>";
    }

    public function getTemplateName()
    {
        return "_partials/buttons/_pinterest_share.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 5,  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/buttons/_pinterest_share.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/buttons/_pinterest_share.html.twig");
    }
}
