<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/contact-us.html.twig */
class __TwigTemplate_1ca8f92f4b3b06254859fa8a4136d2f0dbcd61ca7d6aff661017b6925f8c9575 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<p>
    Name: ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "name", [], "any", false, false, false, 12), "html", null, true);
        echo "
</p>
<p>
    Original Email: ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "app_user_email", [], "any", false, false, false, 15), "html", null, true);
        echo "
</p>
<p>
    Reply-To Email: ";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "email", [], "any", false, false, false, 18), "html", null, true);
        echo "
</p>
<p>
    IP: ";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "app_user_ip", [], "any", false, false, false, 21), "html", null, true);
        echo "
</p>

<hr>

<p>
    ";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "message", [], "any", false, false, false, 27), "html", null, true);
        echo "
</p>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "emails/contact-us.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 27,  68 => 21,  62 => 18,  56 => 15,  50 => 12,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/contact-us.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/contact-us.html.twig");
    }
}
