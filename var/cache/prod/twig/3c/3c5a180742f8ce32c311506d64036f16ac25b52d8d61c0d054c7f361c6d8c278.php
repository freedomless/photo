<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_product_compare_images.html.twig */
class __TwigTemplate_f1af275f1407d7f2dc5f9a0a398dbe38b03bd9d5e703a98a4cc02cf2151db710 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 1), "type", [], "any", false, false, false, 1), 1))) {
            // line 2
            echo "    ";
            // line 3
            echo "
    <div class=\"card mb-3\">

        <div class=\"card-header\">

            <span class=\"h6 card-title mb-0\">Photo ID #";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8), "html", null, true);
            echo "</span>

            ";
            // line 10
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 10), "image", [], "any", false, false, false, 10), "printThumbnail", [], "any", false, false, false, 10))) {
                // line 11
                echo "
                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 13), "image", [], "any", false, false, false, 13), "print", [], "any", false, false, false, 13)), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> Download Print File
                </a>

                <a class=\"btn btn-outline-primary ml-2 float-right\" target=\"_blank\"
                   href=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 18), "image", [], "any", false, false, false, 18), "print", [], "any", false, false, false, 18)), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> View in browser
                </a>

            ";
            } else {
                // line 23
                echo "
                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 25), "image", [], "any", false, false, false, 25), "url", [], "any", false, false, false, 25), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> Download Original File
                </a>

            ";
            }
            // line 30
            echo "
            ";
            // line 31
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 31), "product", [], "any", false, false, false, 31), "status", [], "any", false, false, false, 31), 3))) {
                // line 32
                echo "
                <a href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 33), "product", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\"
                   class=\"btn btn-success float-right\">
                    <i class=\"fa fa-thumbs-up\"></i> Approve
                </a>

            ";
            }
            // line 39
            echo "
        </div>

        <div class=\"card-body\">

            <div class=\"compare\" onclick=\"toggle('compare-print')\">

                <div class=\"compare-original\">

                    <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 48), "image", [], "any", false, false, false, 48), "small", [], "any", false, false, false, 48), "html", null, true);
            echo "\"
                         alt=\"\"
                         style=\"";
            // line 50
            echo $this->env->getRuntime('App\Twig\ComparePrintRuntime')->getStyle(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 50), "image", [], "any", false, false, false, 50), "width", [], "any", false, false, false, 50), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 50), "image", [], "any", false, false, false, 50), "height", [], "any", false, false, false, 50));
            echo "\"
                    >

                    <span class=\"small text-muted\">original</span>

                    <div class=\"compare-print\" id=\"compare-print\">

                        ";
            // line 57
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 57), "image", [], "any", false, false, false, 57), "printThumbnail", [], "any", false, false, false, 57))) {
                // line 58
                echo "
                            <img src=\"";
                // line 59
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 59), "image", [], "any", false, false, false, 59), "printThumbnail", [], "any", false, false, false, 59), "html", null, true);
                echo "\" alt=\"\">

                            <span class=\"small text-muted\">print</span>

                        ";
            } else {
                // line 64
                echo "
                            <div class=\"no-print small text-muted\">No print file available</div>

                        ";
            }
            // line 68
            echo "
                    </div>

                </div>

            </div>

        </div>

    </div>

";
        } else {
            // line 80
            echo "    ";
            // line 81
            echo "
    ";
            // line 82
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 82), "product", [], "any", false, false, false, 82), "status", [], "any", false, false, false, 82), 3))) {
                // line 83
                echo "
        <a href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "post", [], "any", false, false, false, 84), "product", [], "any", false, false, false, 84), "id", [], "any", false, false, false, 84)]), "html", null, true);
                echo "\"
           class=\"btn btn-outline-success mb-3\">
            <i class=\"fa fa-thumbs-up\"></i> Approve Complete Series
        </a>

        <br>

    ";
            }
            // line 92
            echo "
    ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "children", [], "any", false, false, false, 93));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 94
                echo "
        <div class=\"card mb-3\">

            <div class=\"card-header\">

                <span class=\"h6 card-title mb-0\">Photo ID #";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 99), "id", [], "any", false, false, false, 99), "html", null, true);
                echo "</span>

                ";
                // line 101
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 101), "image", [], "any", false, false, false, 101), "printThumbnail", [], "any", false, false, false, 101))) {
                    // line 102
                    echo "
                    <a class=\"btn btn-outline-primary float-right\" href=\"";
                    // line 103
                    echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 103), "image", [], "any", false, false, false, 103), "print", [], "any", false, false, false, 103)), "html", null, true);
                    echo "\">
                        <i class=\"fa fa-cloud-download\"></i> Download Print File
                    </a>

                    <a class=\"btn btn-outline-primary mr-2 float-right\" target=\"_blank\"
                       href=\"";
                    // line 108
                    echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 108), "image", [], "any", false, false, false, 108), "print", [], "any", false, false, false, 108)), "html", null, true);
                    echo "\">
                        <i class=\"fa fa-cloud-download\"></i> View in browser
                    </a>

                ";
                }
                // line 113
                echo "
                ";
                // line 114
                if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 114), "product", [], "any", false, false, false, 114), "status", [], "any", false, false, false, 114), 3))) {
                    // line 115
                    echo "
                    <a href=\"";
                    // line 116
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 116), "product", [], "any", false, false, false, 116), "id", [], "any", false, false, false, 116)]), "html", null, true);
                    echo "\"
                       class=\"btn btn-success float-right\">
                        <i class=\"fa fa-thumbs-up\"></i> Approve Photo
                    </a>

                ";
                }
                // line 122
                echo "
            </div>

            <div class=\"card-body\">

                <div class=\"compare mb-3\" onclick=\"toggle('compare-print')\">

                    <div class=\"compare-original\">

                        <img src=\"";
                // line 131
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 131), "image", [], "any", false, false, false, 131), "small", [], "any", false, false, false, 131), "html", null, true);
                echo "\"
                             class=\"shadow-sm\"
                             style=\"";
                // line 133
                echo $this->env->getRuntime('App\Twig\ComparePrintRuntime')->getStyle(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 133), "image", [], "any", false, false, false, 133), "width", [], "any", false, false, false, 133), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 133), "image", [], "any", false, false, false, 133), "height", [], "any", false, false, false, 133));
                echo "\"
                             alt=\"\"
                        >

                        <span class=\"small text-muted\">original</span>

                        <div class=\"compare-print\" id=\"compare-print\">

                            ";
                // line 141
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 141), "image", [], "any", false, false, false, 141), "printThumbnail", [], "any", false, false, false, 141))) {
                    // line 142
                    echo "
                                <img src=\"";
                    // line 143
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 143), "image", [], "any", false, false, false, 143), "printThumbnail", [], "any", false, false, false, 143), "html", null, true);
                    echo "\" alt=\"\">

                                <span class=\"small text-muted\">print</span>

                            ";
                } else {
                    // line 148
                    echo "
                                <div class=\"no-print small text-muted\">No print file available</div>

                            ";
                }
                // line 152
                echo "
                        </div>

                    </div>

                </div>

            </div>

        </div>

    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 164
            echo "
";
        }
        // line 166
        echo "
<style>
    .compare-original {
        position: relative;
        cursor: pointer;
    }

    .compare-original span {
        position: absolute;
        left: 20px;
        top: 10px;
        color: white;
        mix-blend-mode: difference;
    }

    .compare-print {
        position: absolute;
        top: 0;
    }

    .no-print {
        position: absolute;
        left: 20px;
        top: 30px;
        background: darkgoldenrod;
        color: #000 !important;
        width: 130px;
        padding: .5rem;
    }
</style>

<script>
    function toggle(target) {
        \$(\".\" + target).toggle();
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_product_compare_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 166,  304 => 164,  287 => 152,  281 => 148,  273 => 143,  270 => 142,  268 => 141,  257 => 133,  252 => 131,  241 => 122,  232 => 116,  229 => 115,  227 => 114,  224 => 113,  216 => 108,  208 => 103,  205 => 102,  203 => 101,  198 => 99,  191 => 94,  187 => 93,  184 => 92,  173 => 84,  170 => 83,  168 => 82,  165 => 81,  163 => 80,  149 => 68,  143 => 64,  135 => 59,  132 => 58,  130 => 57,  120 => 50,  115 => 48,  104 => 39,  95 => 33,  92 => 32,  90 => 31,  87 => 30,  79 => 25,  75 => 23,  67 => 18,  59 => 13,  55 => 11,  53 => 10,  48 => 8,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/_product_compare_images.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_product_compare_images.html.twig");
    }
}
