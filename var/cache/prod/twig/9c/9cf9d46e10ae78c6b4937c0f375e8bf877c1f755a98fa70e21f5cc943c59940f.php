<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/products.html.twig */
class __TwigTemplate_5b6a987ecb3b4d163fd74c06c7bced6a7df4cc3a81defda69df2afe9a73f9310 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/products.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "



    <div class=\"col-12\">

        ";
        // line 10
        if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "posts", [], "any", false, false, false, 10)), 0))) {
            // line 11
            echo "
            <h3 class=\"mt-5 mb-5 text-center text-muted\">No Products.</h3>

        ";
        } else {
            // line 15
            echo "
            ";
            // line 16
            $context["stats"] = $this->extensions['App\Twig\AccountStatsExtension']->getAccountStats(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 16), "get", [0 => "slug"], "method", false, false, false, 16));
            // line 17
            echo "
            ";
            // line 18
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 18), "get", [0 => "type"], "method", false, false, false, 18), "single"))) {
                // line 19
                echo "
                ";
                // line 20
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>                 // line 22
($context["nude"] ?? null), "auth" =>                 // line 23
($context["auth"] ?? null), "more" => twig_get_attribute($this->env, $this->source,                 // line 24
($context["posts"] ?? null), "has_more", [], "any", false, false, false, 24), "page" => "user-store", "user" => twig_get_attribute($this->env, $this->source,                 // line 26
($context["user"] ?? null), "id", [], "any", false, false, false, 26), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "posts" => twig_get_attribute($this->env, $this->source,                 // line 29
($context["posts"] ?? null), "items", [], "any", false, false, false, 29), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,                 // line 30
($context["posts"] ?? null), "total", [], "any", false, false, false, 30), 0)), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")]]);
                // line 31
                echo "

            ";
            } else {
                // line 34
                echo "                ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["sorting" => true, "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "series" => twig_get_attribute($this->env, $this->source,                 // line 38
($context["posts"] ?? null), "items", [], "any", false, false, false, 38), "page" => "store"]]);
                // line 39
                echo "

            ";
            }
            // line 42
            echo "
        ";
        }
        // line 44
        echo "
    </div>


";
    }

    public function getTemplateName()
    {
        return "user/profile/products.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 44,  101 => 42,  96 => 39,  94 => 38,  92 => 34,  87 => 31,  85 => 30,  84 => 29,  83 => 26,  82 => 24,  81 => 23,  80 => 22,  79 => 20,  76 => 19,  74 => 18,  71 => 17,  69 => 16,  66 => 15,  60 => 11,  58 => 10,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/products.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/products.html.twig");
    }
}
