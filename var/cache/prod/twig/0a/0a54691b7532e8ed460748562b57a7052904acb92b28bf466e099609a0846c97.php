<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/post-published.html.twig */
class __TwigTemplate_21eabf876e87a8e9201f403c90989090506f247e784ed4c1d3da857662bc7a4f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_poster' => [$this, 'block_email_poster'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
            'email_footer_1' => [$this, 'block_email_footer_1'],
            'email_footer_2' => [$this, 'block_email_footer_2'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/post-published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Your photo is published!";
    }

    // line 5
    public function block_email_poster($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo twig_include($this->env, $context, "emails/_poster.html.twig", ["poster_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 7
($context["payload"] ?? null), "post", [], "any", false, false, false, 7), "photo_url", [], "any", false, false, false, 7)]);
        // line 8
        echo "
";
    }

    // line 11
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 11), "first_name", [], "any", false, false, false, 11), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 11), "last_name", [], "any", false, false, false, 11), "html", null, true);
    }

    // line 13
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    It is our honor to inform you that your photo has been published in the ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " Gallery!
";
    }

    // line 17
    public function block_email_footer_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "
    <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"
           style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;\"><tr><td></td></tr>
    </table>

    <span>";
        // line 23
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo "</span>

    <h3 style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;margin-top: 0;\">
        PUBLISHED
    </h3>

    ";
        // line 29
        echo twig_include($this->env, $context, "emails/_button.html.twig", ["button_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 30
($context["payload"] ?? null), "post", [], "any", false, false, false, 30), "url", [], "any", false, false, false, 30), "button_title" => "View"]);
        // line 32
        echo "
";
    }

    // line 35
    public function block_email_footer_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    <br>
    <p>
        Each photo is carefully analyzed by professional curators and
        only the most impressive works are published in the ";
        // line 39
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " gallery!<br>

        ";
        // line 41
        echo twig_include($this->env, $context, "emails/_sharing.html.twig", ["share_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 42
($context["payload"] ?? null), "post", [], "any", false, false, false, 42), "url", [], "any", false, false, false, 42), "media" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 43
($context["payload"] ?? null), "post", [], "any", false, false, false, 43), "photo_url", [], "any", false, false, false, 43)]);
        // line 44
        echo "
    </p>
";
    }

    public function getTemplateName()
    {
        return "emails/post-published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 44,  135 => 43,  134 => 42,  133 => 41,  128 => 39,  123 => 36,  119 => 35,  114 => 32,  112 => 30,  111 => 29,  102 => 23,  95 => 18,  91 => 17,  84 => 14,  80 => 13,  71 => 11,  66 => 8,  64 => 7,  62 => 6,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/post-published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/post-published.html.twig");
    }
}
