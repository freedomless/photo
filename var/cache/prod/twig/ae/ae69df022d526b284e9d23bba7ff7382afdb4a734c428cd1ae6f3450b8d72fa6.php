<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curated-by.html.twig */
class __TwigTemplate_1736aaed8d802111ec6ae69e859f6e7c222b4e7aab108c4ee8806e105054832b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-text\">

        <ul class=\"list-group list-group-flush\">

            <li class=\"list-group-item\">

                ";
        // line 9
        if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 2)) {
            // line 10
            echo "                    <h3 class=\"h6 font-weight-bold\">
                        ";
            // line 11
            if ( !(null === twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 11))) {
                // line 12
                echo "                            <span class=\"text-danger\">Rejected</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 12), "publisher", [], "any", false, false, false, 12), "profile", [], "any", false, false, false, 12), "fullName", [], "any", false, false, false, 12), "html", null, true);
                echo "
                        ";
            } else {
                // line 14
                echo "                            <span class=\"text-danger\">Rejected</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "publisher", [], "any", false, false, false, 14), "profile", [], "any", false, false, false, 14), "fullName", [], "any", false, false, false, 14), "html", null, true);
                echo "
                        ";
            }
            // line 16
            echo "                    </h3>
                ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 17
($context["post"] ?? null), 3)) {
            // line 18
            echo "                    <h3 class=\"h6 font-weight-bold\">
                        ";
            // line 19
            if ( !(null === twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 19))) {
                // line 20
                echo "                            <span class=\"text-success\">Published</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 20), "publisher", [], "any", false, false, false, 20), "profile", [], "any", false, false, false, 20), "fullName", [], "any", false, false, false, 20), "html", null, true);
                echo "
                        ";
            } else {
                // line 22
                echo "                            <span class=\"text-success\">Published</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "publisher", [], "any", false, false, false, 22), "profile", [], "any", false, false, false, 22), "fullName", [], "any", false, false, false, 22), "html", null, true);
                echo "
                        ";
            }
            // line 24
            echo "                    </h3>
                ";
        }
        // line 26
        echo "
            </li>

        </ul>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curated-by.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 26,  88 => 24,  82 => 22,  76 => 20,  74 => 19,  71 => 18,  69 => 17,  66 => 16,  60 => 14,  54 => 12,  52 => 11,  49 => 10,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/curated-by.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curated-by.html.twig");
    }
}
