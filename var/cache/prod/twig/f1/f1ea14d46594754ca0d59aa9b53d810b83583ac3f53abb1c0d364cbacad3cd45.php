<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/following.html.twig */
class __TwigTemplate_711a65083027742145a66ab1abe68edac603f8ac41aabb6ca7982499e2f29c02 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/following.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    <div class=\"col-12\">

        ";
        // line 7
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["nude" =>         // line 8
($context["nude"] ?? null), "auth" =>         // line 9
($context["auth"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,         // line 10
($context["posts"] ?? null), "items", [], "any", false, false, false, 10), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,         // line 11
($context["posts"] ?? null), "total", [], "any", false, false, false, 11), 0)), "mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "page" => "following"]]);
        // line 13
        echo "

    </div>

";
    }

    public function getTemplateName()
    {
        return "user/profile/following.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 13,  59 => 11,  58 => 10,  57 => 9,  56 => 8,  55 => 7,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/following.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/following.html.twig");
    }
}
