<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/index.html.twig */
class __TwigTemplate_7db675bb1feb1050d47b7c5ce05d872be25f121fe02cf35dcc898b30a05cf6c7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<nav class=\"navbar navbar-expand-xl navbar-dark bg-primary shadow fixed-top\">

    ";
        // line 4
        echo "    <a class=\"navbar-brand\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_popular");
        echo "\">
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo_new.png"), "html", null, true);
        echo "\" alt=\"Logo\" height=\"40px\">
    </a>

    ";
        // line 9
        echo "    <button class=\"navbar-toggler border-0\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar\"
            aria-controls=\"navbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

        <span class=\"navbar-toggler-icon\"></span>

    </button>

    ";
        // line 17
        echo "    <div class=\"collapse navbar-collapse\" id=\"navbar\">

        <form class=\"form-inline nav-search\">
            ";
        // line 20
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("SearchBox", ["rendering" => "client_side"]);
        echo "
        </form>

        ";
        // line 23
        $this->loadTemplate("_partials/navigation/left.html.twig", "_partials/navigation/index.html.twig", 23)->display($context);
        // line 24
        echo "
        ";
        // line 25
        $this->loadTemplate("_partials/navigation/right.html.twig", "_partials/navigation/index.html.twig", 25)->display($context);
        // line 26
        echo "
    </div>

</nav>
";
    }

    public function getTemplateName()
    {
        return "_partials/navigation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 26,  77 => 25,  74 => 24,  72 => 23,  66 => 20,  61 => 17,  52 => 9,  46 => 5,  41 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/navigation/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/index.html.twig");
    }
}
