<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gallery/index.html.twig */
class __TwigTemplate_aca5cdc099c1e82084f7eda88b9f5a2712f53c2333e9aa6a22fae244df0da01c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "gallery/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Gallery";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"container photos sm-full\">

        <div class=\"row photos\">

            <div class=\"col-12 \">

                ";
        // line 13
        $this->loadTemplate("gallery/nav.html.twig", "gallery/index.html.twig", 13)->display($context);
        // line 14
        echo "
                ";
        // line 15
        $context["filter"] = (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "get", [0 => "page"], "method", false, false, false, 15), "latest"));
        // line 16
        echo "                ";
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 16), "query", [], "any", false, false, false, 16), "all", [], "method", false, false, false, 16);
        // line 17
        echo "                ";
        $context["page"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 17), "get", [0 => "page"], "method", false, false, false, 17);
        // line 18
        echo "                ";
        $context["category"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 18), "attributes", [], "any", false, false, false, 18), "get", [0 => "category"], "method", false, false, false, 18);
        // line 19
        echo "

                ";
        // line 21
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>         // line 23
($context["nude"] ?? null), "auth" =>         // line 24
($context["auth"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,         // line 25
($context["posts"] ?? null), "items", [], "any", false, false, false, 25), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,         // line 26
($context["posts"] ?? null), "total", [], "any", false, false, false, 26), 0)), "page" =>         // line 27
($context["page"] ?? null), "filter" =>         // line 28
($context["filter"] ?? null), "filters" =>         // line 29
($context["filters"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "categories" =>         // line 32
($context["categories"] ?? null), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "category" =>         // line 34
($context["category"] ?? null)]]);
        echo "

            </div>

        </div>

    </div>

    ";
        // line 42
        echo twig_include($this->env, $context, "_partials/_social_icons.html.twig");
        echo "

";
    }

    public function getTemplateName()
    {
        return "gallery/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 42,  96 => 34,  95 => 32,  94 => 29,  93 => 28,  92 => 27,  91 => 26,  90 => 25,  89 => 24,  88 => 23,  87 => 21,  83 => 19,  80 => 18,  77 => 17,  74 => 16,  72 => 15,  69 => 14,  67 => 13,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "gallery/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/gallery/index.html.twig");
    }
}
