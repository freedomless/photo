<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Twig/Exception/error500.html.twig */
class __TwigTemplate_6e6c226a675500e3e7385776f9315a1e6d94f6e200eb8bdf824b91fc02f08f83 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'error_title' => [$this, 'block_error_title'],
            'error_message' => [$this, 'block_error_message'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "bundles/TwigBundle/Exception/error.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("bundles/TwigBundle/Exception/error.html.twig", "@Twig/Exception/error500.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "404 Not Found";
    }

    // line 5
    public function block_error_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    500 - Internal Server Error
";
    }

    // line 9
    public function block_error_message($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    The web server is returning an internal error for
    ";
        // line 11
        $this->loadTemplate("bundles/TwigBundle/Exception/_home_button.html.twig", "@Twig/Exception/error500.html.twig", 11)->display($context);
    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error500.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 11,  68 => 10,  64 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@Twig/Exception/error500.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/bundles/TwigBundle/Exception/error500.html.twig");
    }
}
