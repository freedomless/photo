<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/footer.html.twig */
class __TwigTemplate_9dd0b5dadee55aedb1c164cb815fb87b1416b49204f22fd474498d01b4d1c651 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer class=\"footer mt-auto py-3 border-top container\">
    <div class=\"container\">
        <div class=\"row\">
           <div class=\"col-12\">
               <a href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"btn btn-link\">Contact us</a>
           </div>
        </div>
    </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "_partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/footer.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/footer.html.twig");
    }
}
