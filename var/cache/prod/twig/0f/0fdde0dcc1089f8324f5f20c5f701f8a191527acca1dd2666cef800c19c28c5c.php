<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_poster.html.twig */
class __TwigTemplate_776a4226e8e161f9ce0196e7fa5e766e7345c04c3b9903367186a7a0a1c8401b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
<img src=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["poster_url"] ?? null), "html", null, true);
        echo "\" alt=\"\" style=\"max-width: 300px\">";
    }

    public function getTemplateName()
    {
        return "emails/_poster.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 9,  37 => 8,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/_poster.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_poster.html.twig");
    }
}
