<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/tag.html.twig */
class __TwigTemplate_cf7d183556307c2fd13964dcc2f5ee48313905bebb14557cf390d6a2c60e4d19 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "photo/tag.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, ($context["tag"] ?? null), "html", null, true);
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"container photos sm-full photos-infinite\">
        ";
        // line 5
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>         // line 7
($context["nude"] ?? null), "posts" => twig_get_attribute($this->env, $this->source,         // line 8
($context["posts"] ?? null), "items", [], "any", false, false, false, 8), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,         // line 9
($context["posts"] ?? null), "total", [], "any", false, false, false, 9), 0)), "auth" =>         // line 10
($context["auth"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "tag" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 13
($context["app"] ?? null), "request", [], "any", false, false, false, 13), "get", [0 => "tag"], "method", false, false, false, 13), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" => "tag"]]);
        // line 15
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "photo/tag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 15,  66 => 13,  65 => 10,  64 => 9,  63 => 8,  62 => 7,  61 => 5,  58 => 4,  54 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/tag.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/tag.html.twig");
    }
}
