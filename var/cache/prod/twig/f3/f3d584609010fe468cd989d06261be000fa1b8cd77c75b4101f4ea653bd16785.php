<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/poll/choice_manage.html.twig */
class __TwigTemplate_1da14e9bfc3a811a1ccb968acdf6ed684bfe93b9e438a32dc2bcee459dd0a574 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'row' => [$this, 'block_row'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/poll/choice_manage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "title", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Add choices for: <strong>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "title", [], "any", false, false, false, 5), "html", null, true);
        echo "</strong>";
    }

    // line 6
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_list");
        echo "\">
        <i class=\"fa fa-times\"></i>
    </a>
";
    }

    // line 12
    public function block_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    <div class=\"row default-form-layout\">

        <div class=\"col-12\">

            <div style=\"display: none\" id=\"js-prototype\">

                <div class=\"form-group row\">
                    <div class=\"col-2 col-sm-1\">
                <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                    <i class=\"fa fa-arrows-alt text-white\"></i>
                </span>
                    </div>

                    <div class=\"col-8 col-sm-10\">
                        <input type=\"text\"
                               data-entity-id=\"\" id=\"choice-2\"
                               class=\"form-control js-choice-input\"
                               placeholder=\"Choice text\"
                        >
                    </div>
                    <div class=\"col-2 col-sm-1\">
                        <button class=\"btn btn-danger float-right js-remove-input\">
                            <i class=\"fa fa-minus\"></i>
                        </button>
                    </div>
                </div>

            </div>

            <form action=\"\" method=\"post\">

                <div id=\"js-form-inputs\">

                    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "choices", [], "any", false, false, false, 46));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 47
            echo "
                        <div class=\"form-group row\">
                            <div class=\"col-2 col-sm-1\">

                            <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                                <i class=\"fa fa-arrows-alt text-white\"></i>
                            </span>

                            </div>
                            <div class=\"col-8 col-sm-10\">
                                <input type=\"text\"
                                       data-entity-id=\"\"
                                       class=\"form-control js-choice-input\"
                                       placeholder=\"Choice text\"
                                       value=\"";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "title", [], "any", false, false, false, 61), "html", null, true);
            echo "\"
                                >
                            </div>
                            <div class=\"col-2 col-sm-1\">
                                <button class=\"btn btn-danger float-right js-remove-input\">
                                    <i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "
                </div>

                ";
        // line 76
        echo "                <div class=\"form-group row\">
                    <div class=\"col-12\">
                        <button
                                id=\"js-add-more-btn\"
                                class=\"btn btn-success float-right\">
                            <i class=\"fa fa-plus\"></i>
                        </button>
                    </div>
                </div>
                <div class=\"form-group\">

                    <button type=\"submit\" class=\"btn btn-primary\" id=\"js-submit-it\">Save Choices</button>
                </div>

                <input type=\"hidden\" name=\"token\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("submit_choices"), "html", null, true);
        echo "\"/>

            </form>

        </div>

    </div>

";
    }

    // line 100
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 101
        echo "    <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

    <script src=\"//code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
    <script>
        \$(function () {
            \$(\"#js-form-inputs\").sortable();
            \$(\"#js-form-inputs\").disableSelection();
        });
    </script>
    <script>

        \$(document).ready(function () {

            \$(document).on('click', '.js-remove-input', function (e) {
                e.preventDefault();

                \$(this).parent().parent().remove();

                return false;
            });

            \$('#js-add-more-btn').on('click', function (e) {
                e.preventDefault();

                let row = \$('#js-prototype').clone();
                \$('#js-form-inputs').append(\$(row).html());
            });

            \$('#js-submit-it').on('click', function (e) {

                e.preventDefault();

                let choices = [];
                let inputs = \$('.js-choice-input');

                inputs.each(function (i, item) {
                    let val = \$(item).val();
                    if (val !== '') {
                        choices.push(item);
                    }
                });

                if (choices.length === 0) {
                    return;
                }

                let formSelector = 'form';

                \$(choices).each(function (i, item) {
                    addInputToForm(formSelector, i, item)
                });

                \$(formSelector).submit();
            });
        });

        function addInputToForm(formSelector, index, value) {
            let form = \$(formSelector);
            form.append(
                \$('<input type=\"hidden\" name=\"choice[' + index + ']\" value=\"' + \$(value).val() + '\">')
            );
        }

    </script>
";
    }

    public function getTemplateName()
    {
        return "admin/poll/choice_manage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 101,  189 => 100,  176 => 90,  160 => 76,  155 => 72,  138 => 61,  122 => 47,  118 => 46,  83 => 13,  79 => 12,  70 => 7,  66 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/poll/choice_manage.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/poll/choice_manage.html.twig");
    }
}
