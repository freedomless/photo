<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_16f495777d5c8674e50dc0fcfb7715d151c815890759e30f0a14eb471e2bfb89 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'metadata' => [$this, 'block_metadata'],
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body_css_classes' => [$this, 'block_body_css_classes'],
            'main_navigation' => [$this, 'block_main_navigation'],
            'main_css_classes' => [$this, 'block_main_css_classes'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>

<html lang=\"en\">

    <head>
        ";
        // line 7
        echo "        <meta charset=\"UTF-8\">
        <meta name=\"viewport\"
              content=\"width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">

        <style>
            label.required:after {
                content: \" *\";
                color: red;
            }
        </style>

        ";
        // line 19
        $this->displayBlock('metadata', $context, $blocks);
        // line 24
        echo "
        ";
        // line 26
        echo "        <title>";
        $this->displayBlock('title', $context, $blocks);
        echo " :: ";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["site_url"] ?? null)), "html", null, true);
        echo "</title>

        ";
        // line 29
        echo "        ";
        $this->loadTemplate("_partials/css.html.twig", "base.html.twig", 29)->display($context);
        // line 30
        echo "
        <link rel=\"icon\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/favicon.ico"), "html", null, true);
        echo "\">

        <link rel=\"manifest\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/manifest.json"), "html", null, true);
        echo "\">

        ";
        // line 35
        $this->displayBlock('css', $context, $blocks);
        // line 36
        echo "
    </head>

    <body class=\"main-body ";
        // line 39
        $this->displayBlock('body_css_classes', $context, $blocks);
        echo "\">
        ";
        // line 41
        echo "        ";
        $this->displayBlock('main_navigation', $context, $blocks);
        // line 44
        echo "
        ";
        // line 46
        echo "        <main class=\"mb-5 ";
        $this->displayBlock('main_css_classes', $context, $blocks);
        echo "\" style=\"margin-top: 50px\">

            ";
        // line 48
        $this->loadTemplate("_partials/_verify.html.twig", "base.html.twig", 48)->display($context);
        // line 49
        echo "
            ";
        // line 50
        $this->displayBlock('body', $context, $blocks);
        // line 51
        echo "
            ";
        // line 52
        $this->loadTemplate("_partials/copyright-notice.html.twig", "base.html.twig", 52)->display($context);
        // line 53
        echo "
        </main>

        ";
        // line 57
        echo "        ";
        $this->loadTemplate("_partials/js.html.twig", "base.html.twig", 57)->display($context);
        // line 58
        echo "
        ";
        // line 59
        $this->displayBlock('js', $context, $blocks);
        // line 60
        echo "
        ";
        // line 62
        echo "        ";
        $this->loadTemplate("_partials/cookie_accept.html.twig", "base.html.twig", 62)->display($context);
        // line 63
        echo "
        ";
        // line 65
        echo "        ";
        $this->loadTemplate("_partials/notification.html.twig", "base.html.twig", 65)->display($context);
        // line 66
        echo "
        ";
        // line 68
        echo "        ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "debug", [], "any", false, false, false, 68) == false)) {
            // line 69
            echo "            ";
            $this->loadTemplate("_partials/analytics.html.twig", "base.html.twig", 69)->display($context);
            // line 70
            echo "        ";
        }
        // line 71
        echo "
    </body>
</html>
";
    }

    // line 19
    public function block_metadata($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "
            ";
        // line 21
        $this->loadTemplate("_partials/meta.html.twig", "base.html.twig", 21)->display($context);
        // line 22
        echo "
        ";
    }

    // line 26
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 35
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 39
    public function block_body_css_classes($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 41
    public function block_main_navigation($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "            ";
        $this->loadTemplate("_partials/navigation/index.html.twig", "base.html.twig", 42)->display($context);
        // line 43
        echo "        ";
    }

    // line 46
    public function block_main_css_classes($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 50
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 59
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 59,  223 => 50,  217 => 46,  213 => 43,  210 => 42,  206 => 41,  200 => 39,  194 => 35,  188 => 26,  183 => 22,  181 => 21,  178 => 20,  174 => 19,  167 => 71,  164 => 70,  161 => 69,  158 => 68,  155 => 66,  152 => 65,  149 => 63,  146 => 62,  143 => 60,  141 => 59,  138 => 58,  135 => 57,  130 => 53,  128 => 52,  125 => 51,  123 => 50,  120 => 49,  118 => 48,  112 => 46,  109 => 44,  106 => 41,  102 => 39,  97 => 36,  95 => 35,  90 => 33,  85 => 31,  82 => 30,  79 => 29,  71 => 26,  68 => 24,  66 => 19,  52 => 7,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/base.html.twig");
    }
}
