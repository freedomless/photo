<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/left.html.twig */
class __TwigTemplate_056e1bb2132b064b7c255d9c59d08c600575f044d9d1d6b81b7025b417cbbd3a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"navbar-nav mr-auto\">
    ";
        // line 2
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 3
            echo "        <li class=\"nav-item\">
            <span class=\"text-warning\"><i class=\"fas fa-exclamation-circle\"></i> Currently switched!</span>
        </li>
    ";
        }
        // line 7
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "_partials/navigation/left.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 7,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/navigation/left.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/left.html.twig");
    }
}
