<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/confirmation.html.twig */
class __TwigTemplate_ade54bbd1348e1aa2681de078066841717bb0a992e604fa47b240082f9f838ae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/confirmation.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Confirm Your account at ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
    }

    // line 5
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "first_name", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "last_name", [], "any", false, false, false, 5), "html", null, true);
    }

    // line 7
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    <p> You are one step from becoming ";
        // line 9
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " member.</p>
    <p> Just click the link bellow and all is settled.</p>

    <br>

    <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_confirmation", ["token" => twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "token", [], "any", false, false, false, 14)]), "html", null, true);
        echo "\" >Confirm</a>

";
    }

    public function getTemplateName()
    {
        return "emails/confirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 14,  72 => 9,  69 => 8,  65 => 7,  56 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/confirmation.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/confirmation.html.twig");
    }
}
