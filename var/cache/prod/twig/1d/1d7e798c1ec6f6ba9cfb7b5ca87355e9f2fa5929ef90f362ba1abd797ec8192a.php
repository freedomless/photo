<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* member/nav.html.twig */
class __TwigTemplate_25decec8f2f1721f7b66ce130d6fd0d52b254c3c8af13b7d62efff94fd0e5f6b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"nav justify-content-center custom-active basic-nav\">

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("alphabetical", "page", "active shadow"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members");
        echo "\">
            All Members</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("founders", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "founders"]);
        echo "\">
            PiART Founders</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("following", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "following"]);
        echo "\">Followed</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("followers", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "followers"]);
        echo "\">Followers</a>
    </li>

</ul>

<hr>
";
    }

    public function getTemplateName()
    {
        return "member/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 21,  75 => 20,  68 => 16,  64 => 15,  56 => 10,  52 => 9,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "member/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/member/nav.html.twig");
    }
}
