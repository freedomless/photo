<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/email_tester.html.twig */
class __TwigTemplate_141ff3e5457c9d2c6978b3d4a121d43b505c28ad4315badfd3eb9d447e636b92 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'page_title' => [$this, 'block_page_title'],
            'content_header' => [$this, 'block_content_header'],
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@EasyAdmin/page/content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["_content_title"] = "Test Emails";
        // line 1
        $this->parent = $this->loadTemplate("@EasyAdmin/page/content.html.twig", "admin/easyadmin/email_tester.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_page_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "Test Emails";
    }

    // line 8
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    <h3 class=\"title\">Test Emails</h3>
";
    }

    // line 12
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "
    <div class=\"container\">

        <div class=\"row\">

            <div class=\"col-sm-6\">

                <p>Emails will be sent to <strong>";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 20), "email", [], "any", false, false, false, 20), "html", null, true);
        echo "</strong></p>

                <div class=\"btn-group-vertical\" role=\"group\" aria-label=\"Button group with nested dropdown\">

                    <a class=\"btn btn-light btn-lg\"
                       href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_email_tester_photo");
        echo "\">Photo Published</a>

                    <a class=\"btn btn-light btn-lg\"
                       href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_email_tester_series");
        echo "\">Series Published</a>

                    <a class=\"btn btn-light btn-lg\"
                       href=\"";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_email_tester_potd");
        echo "\">Photo of the Day</a>

                    <a class=\"btn btn-light btn-lg\"
                       href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_email_tester_selection_series");
        echo "\">Selection Series</a>
                </div>

            </div>

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/email_tester.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 34,  101 => 31,  95 => 28,  89 => 25,  81 => 20,  72 => 13,  68 => 12,  63 => 9,  59 => 8,  55 => 5,  51 => 4,  46 => 1,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/email_tester.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/email_tester.html.twig");
    }
}
