<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* poll/_results.html.twig */
class __TwigTemplate_7abeead638bd2537f6405d92929d5b691087a74ade32f6cf356c17125d0905e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"list-unstyled poll-results\">

    <h4 class=\"h6 text-success font-italic\">Thank you for Your vote!</h4>

    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["poll"] ?? null), "choices", [], "any", false, false, false, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 6
            echo "
        <li class=\"mb-4\">

            <div class=\"mb-2 poll-choice-title\">
                ";
            // line 10
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "title", [], "any", false, false, false, 10)), "html", null, true);
            echo " <span class=\"text-muted\">(";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "votes", [], "any", false, false, false, 10)), "html", null, true);
            echo ")</span>
            </div>

            ";
            // line 13
            $context["val"] = twig_round(((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "votes", [], "any", false, false, false, 13)) / ($context["total"] ?? null)) * 100), 2);
            // line 14
            echo "
            <div class=\"progress\" style=\"height: 1.5rem\">
                <div class=\"progress-bar\" role=\"progressbar\"
                     style=\"width: ";
            // line 17
            echo twig_escape_filter($this->env, ($context["val"] ?? null), "html", null, true);
            echo "%;\"
                     aria-valuenow=\"";
            // line 18
            echo twig_escape_filter($this->env, ($context["val"] ?? null), "html", null, true);
            echo "\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                    <span class=\"ml-3\">";
            // line 19
            echo twig_escape_filter($this->env, ($context["val"] ?? null), "html", null, true);
            echo "%</span>
                </div>
            </div>

        </li>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
</ul>";
    }

    public function getTemplateName()
    {
        return "poll/_results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 26,  76 => 19,  72 => 18,  68 => 17,  63 => 14,  61 => 13,  53 => 10,  47 => 6,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "poll/_results.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/poll/_results.html.twig");
    }
}
