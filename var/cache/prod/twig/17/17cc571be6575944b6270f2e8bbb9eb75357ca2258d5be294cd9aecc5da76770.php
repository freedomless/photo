<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/payout-completed.html.twig */
class __TwigTemplate_464df5cad1ae29d5efeba44cf79af399e230042d601b35ec78647d50d1d42f76 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/payout-completed.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Your photo is selected as Photo of the day!";
    }

    // line 6
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "name", [], "any", false, false, false, 7), "html", null, true);
        echo "
";
    }

    // line 11
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    Completed ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "balance", [], "any", false, false, false, 12), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "status", [], "any", false, false, false, 12), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "emails/payout-completed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 12,  66 => 11,  59 => 7,  55 => 6,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/payout-completed.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/payout-completed.html.twig");
    }
}
