<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/edit.html.twig */
class __TwigTemplate_2959a1be7352a4227eb40d13a55f3b55552201c7ec856e16ba413542e02fa3dc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'header_right' => [$this, 'block_header_right'],
            'header_left' => [$this, 'block_header_left'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "admin/selection/edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_list");
        echo "\">
        <i class=\"fa fa-times\"></i>
    </a>
";
    }

    // line 9
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Add Series to Selection";
    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
    <div class=\"col-sm-6\">

        <div class=\"default-form-layout\">

            ";
        // line 17
        echo twig_include($this->env, $context, "admin/selection/_form.html.twig", ["button_title" => "Edit Series"]);
        // line 19
        echo "

        </div>

    </div>

";
    }

    // line 28
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    <script>

        \$(function(){
            \$('.custom-file').dropzone()
        })

    </script>
";
    }

    public function getTemplateName()
    {
        return "admin/selection/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 29,  92 => 28,  82 => 19,  80 => 17,  73 => 12,  69 => 11,  62 => 9,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/selection/edit.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/edit.html.twig");
    }
}
