<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/posts.html.twig */
class __TwigTemplate_07ceaa0703dd4272a161665b6a5f89d569aefdc432c211b9a94cf17e5120bdf7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">
                ";
            // line 9
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 9), 1))) {
                // line 10
                echo "                    <i class=\"fas fa-images position-absolute\" style=\"left: 5px;top:5px\" data-toggle=\"tooltip\" title=\"\"
                       data-original-title=\"Series\"></i>
                ";
            }
            // line 13
            echo "                ";
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 13), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 13), "thumbnail", [], "any", false, false, false, 13)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 13), "image", [], "any", false, false, false, 13), "thumbnail", [], "any", false, false, false, 13)));
            // line 14
            echo "
                <img src=\"";
            // line 15
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\"
                     alt=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 16), "html", null, true);
            echo "\">

                ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 18), 1)) {
                // line 19
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 25
            echo "
                ";
            // line 26
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 26), 2)) {
                // line 27
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 33
            echo "
                ";
            // line 34
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 34), 3)) {
                // line 35
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                ";
            }
            // line 41
            echo "
            </div>

            <div class=\"image-thumb-metadata\">

                ";
            // line 46
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "deletionDate", [], "any", false, false, false, 46))) {
                // line 47
                echo "                   <p class=\"text-center\">
                       This product will be removed from store on ";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "deletionDate", [], "any", false, false, false, 48), "Y-m-d"), "html", null, true);
                echo "
                   </p>
                ";
            } else {
                // line 51
                echo "                    <div class=\"actions mb-1 text-center\">

                        <a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 53), "type" => (((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 53), 1))) ? ("series") : (null))]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"";
                // line 57
                echo (( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 57))) ? ("Update Products") : ("Add to store"));
                echo "\">

                            <i class=\"fas fa-store\"></i>
                            <br>
                            <small class=\"text-muted\">Update Product</small>

                        </a>

                        ";
                // line 65
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 65), "status", [], "any", false, false, false, 65), 3))) {
                    // line 66
                    echo "
                            <button type=\"button\"
                                    class=\"btn delete-warning-btn\"
                                    data-url=\"";
                    // line 69
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_product_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 69)]), "html", null, true);
                    echo "\">
                                <i class=\"fas fa-trash text-danger\"></i>
                                <br>
                                <small class=\"text-muted\">Remove</small>
                            </button>

                        ";
                }
                // line 76
                echo "
                    </div>
                ";
            }
            // line 79
            echo "
            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "product/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 79,  165 => 76,  155 => 69,  150 => 66,  148 => 65,  137 => 57,  130 => 53,  126 => 51,  120 => 48,  117 => 47,  115 => 46,  108 => 41,  100 => 35,  98 => 34,  95 => 33,  87 => 27,  85 => 26,  82 => 25,  74 => 19,  72 => 18,  67 => 16,  63 => 15,  60 => 14,  57 => 13,  52 => 10,  50 => 9,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/posts.html.twig");
    }
}
