<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/warning-modal.html.twig */
class __TwigTemplate_4ea9301e9eff61c2258f113e66fb73bb52bdb3511f1336dc896eb9c506bceba7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["modal_id"] = (($context["modal_id"]) ?? ("warning-modal"));
        // line 2
        $context["form_id"] = (($context["form_id"]) ?? ("warning-form"));
        // line 3
        $context["btn_class"] = (($context["btn_id"]) ?? ("warning-action"));
        // line 4
        $context["header"] = (($context["header"]) ?? ("Confirm!"));
        // line 5
        $context["message"] = (($context["message"]) ?? ("Are you sure you want to take this action?"));
        // line 6
        $context["yes"] = (($context["yes"]) ?? ("Yes"));
        // line 7
        $context["no"] = (($context["no"]) ?? ("No"));
        // line 8
        echo "
<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["modal_id"] ?? null), "html", null, true);
        echo "\">

    <div class=\"modal-dialog\" role=\"document\">

        <form method=\"post\" id=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["form_id"] ?? null), "html", null, true);
        echo "\">

            <div class=\"modal-content\">

                <div class=\"modal-header\">

                    <h5 class=\"modal-title\">";
        // line 19
        echo twig_escape_filter($this->env, ($context["header"] ?? null), "html", null, true);
        echo "</h5>

                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                        <span aria-hidden=\"true\">&times;</span>

                    </button>

                </div>

                <div class=\"modal-body\">

                    <p>";
        // line 31
        echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
        echo "</p>

                </div>

                <div class=\"modal-footer\">

                    <button class=\"btn btn-primary\">";
        // line 37
        echo twig_escape_filter($this->env, ($context["yes"] ?? null), "html", null, true);
        echo "</button>

                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 39
        echo twig_escape_filter($this->env, ($context["no"] ?? null), "html", null, true);
        echo "</button>

                </div>

            </div>

        </form>

    </div>

</div>

";
        // line 51
        $this->displayBlock('js', $context, $blocks);
    }

    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "    <script>

        \$(function(){
            \$(document).on('click', '.";
        // line 55
        echo twig_escape_filter($this->env, ($context["btn_class"] ?? null), "html", null, true);
        echo "',function(){
                \$('#";
        // line 56
        echo twig_escape_filter($this->env, ($context["modal_id"] ?? null), "html", null, true);
        echo "').modal('show');
                \$('#";
        // line 57
        echo twig_escape_filter($this->env, ($context["form_id"] ?? null), "html", null, true);
        echo "').prop('action', \$(this).data('url'));
            })
        })

    </script>

";
    }

    public function getTemplateName()
    {
        return "_partials/warning-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 57,  131 => 56,  127 => 55,  122 => 52,  115 => 51,  100 => 39,  95 => 37,  86 => 31,  71 => 19,  62 => 13,  55 => 9,  52 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  42 => 3,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/warning-modal.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/warning-modal.html.twig");
    }
}
