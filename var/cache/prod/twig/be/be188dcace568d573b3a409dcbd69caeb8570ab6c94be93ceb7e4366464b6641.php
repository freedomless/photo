<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_post_type.html.twig */
class __TwigTemplate_41bb786d52aeaac438b2d92d4cfbf491a3c7dce9f232adc442f6a1aae7ea8f13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((0 === twig_compare(($context["value"] ?? null), 1))) {
            // line 2
            echo "    Single
";
        } elseif ((0 === twig_compare(        // line 3
($context["value"] ?? null), 2))) {
            // line 4
            echo "    Series
";
        } elseif ((0 === twig_compare(        // line 5
($context["value"] ?? null), 3))) {
            // line 6
            echo "    Series From Published
";
        } elseif ((0 === twig_compare(        // line 7
($context["value"] ?? null), 4))) {
            // line 8
            echo "    Curator Series
";
        }
    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_post_type.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 8,  52 => 7,  49 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/easyadmin/_post_type.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_post_type.html.twig");
    }
}
