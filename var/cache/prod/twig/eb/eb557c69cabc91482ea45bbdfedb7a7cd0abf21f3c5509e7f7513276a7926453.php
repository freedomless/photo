<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/welcome.html.twig */
class __TwigTemplate_38bf38d2634e0c43acc8a43b87b4f37e6272ca12b01bb92eecdd41aefc15f1bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
            'email_footer_1' => [$this, 'block_email_footer_1'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/welcome.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome to ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "!";
    }

    // line 5
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "first_name", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 5), "last_name", [], "any", false, false, false, 5), "html", null, true);
    }

    // line 7
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
    Welcome to ";
        // line 9
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " photographic community!<br>
    The ";
        // line 10
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " Photo gallery publish the most impressive photographic works selected by experienced professional curators.
    <br>
    <br>
    In addition to the professional curation, the gallery offers you the opportunity to upload series, filter the gallery in various ways, (soon) the ability to sale your prints and many more features!
    <br>
    Only the most impressive works have been published in the ";
        // line 15
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " gallery.<br>
    <br>
    We wish you success and wonderful moments in the community!<br>

    ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "social", [], "any", false, false, false, 19)) {
            // line 20
            echo "        <br>
        <p>If you want to login using password</p>
        <p>you have to first set one using the link bellow.</p>
        <a href=\"";
            // line 23
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_change_password");
            echo "\">Set password</a>
    ";
        }
        // line 25
        echo "
";
    }

    // line 28
    public function block_email_footer_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    <br>
    ";
        // line 30
        echo twig_include($this->env, $context, "emails/_button.html.twig", ["button_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_popular"), "button_title" => "Visit Site"]);
        // line 33
        echo "
";
    }

    public function getTemplateName()
    {
        return "emails/welcome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 33,  117 => 30,  114 => 29,  110 => 28,  105 => 25,  100 => 23,  95 => 20,  93 => 19,  86 => 15,  78 => 10,  74 => 9,  71 => 8,  67 => 7,  58 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/welcome.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/welcome.html.twig");
    }
}
