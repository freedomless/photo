<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/photo-of-the-day.html.twig */
class __TwigTemplate_bc4f388afcb7418cbddf578243e4d17d97a57d168b8c91c3f5bcf7e4cba4089d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'email_title' => [$this, 'block_email_title'],
            'email_poster' => [$this, 'block_email_poster'],
            'email_user' => [$this, 'block_email_user'],
            'email_content' => [$this, 'block_email_content'],
            'email_footer_1' => [$this, 'block_email_footer_1'],
            'email_footer_2' => [$this, 'block_email_footer_2'],
            'email_footer_3' => [$this, 'block_email_footer_3'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "emails/_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("emails/_base.html.twig", "emails/photo-of-the-day.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_email_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Your photo is selected as Photo of the day!";
    }

    // line 5
    public function block_email_poster($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo twig_include($this->env, $context, "emails/_poster.html.twig", ["poster_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 7
($context["payload"] ?? null), "post", [], "any", false, false, false, 7), "photo_url", [], "any", false, false, false, 7)]);
        // line 8
        echo "
";
    }

    // line 11
    public function block_email_user($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 11), "first_name", [], "any", false, false, false, 11), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "receiver", [], "any", false, false, false, 11), "last_name", [], "any", false, false, false, 11), "html", null, true);
    }

    // line 13
    public function block_email_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    It is our honor to inform you that the curators have selected your photo as \"Photo of the Day\" in the ";
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " Gallery!
";
    }

    // line 17
    public function block_email_footer_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    <table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"
           style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;\"><tr><td></td></tr>
    </table>

    <span>";
        // line 22
        echo twig_escape_filter($this->env, ($context["site_url"] ?? null), "html", null, true);
        echo "</span>

    <h3 style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;margin-top: 0;\">
        PHOTO OF THE DAY (";
        // line 25
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["payload"] ?? null), "post", [], "any", false, false, false, 25), "publish_date", [], "any", false, false, false, 25)), "html", null, true);
        echo ")
    </h3>
    ";
        // line 27
        echo twig_include($this->env, $context, "emails/_button.html.twig", ["button_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 28
($context["payload"] ?? null), "post", [], "any", false, false, false, 28), "url", [], "any", false, false, false, 28), "button_title" => "View"]);
        // line 30
        echo "
";
    }

    // line 33
    public function block_email_footer_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    <br>
    <p>
        Only the most impressive works have been published in the ";
        // line 36
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " gallery!
    </p>
";
    }

    // line 40
    public function block_email_footer_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    <br>
    ";
        // line 42
        echo twig_include($this->env, $context, "emails/_sharing.html.twig", ["share_url" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 43
($context["payload"] ?? null), "post", [], "any", false, false, false, 43), "url", [], "any", false, false, false, 43), "media" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 44
($context["payload"] ?? null), "post", [], "any", false, false, false, 44), "photo_url", [], "any", false, false, false, 44)]);
        // line 45
        echo "
";
    }

    public function getTemplateName()
    {
        return "emails/photo-of-the-day.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 45,  145 => 44,  144 => 43,  143 => 42,  140 => 41,  136 => 40,  129 => 36,  125 => 34,  121 => 33,  116 => 30,  114 => 28,  113 => 27,  108 => 25,  102 => 22,  96 => 18,  92 => 17,  85 => 14,  81 => 13,  72 => 11,  67 => 8,  65 => 7,  63 => 6,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/photo-of-the-day.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/photo-of-the-day.html.twig");
    }
}
