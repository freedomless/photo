<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* message/index.html.twig */
class __TwigTemplate_8fb52a2d4de20fd79cf1df5fe5f3a25851e73ccc5147b454e5db195bbb3153c1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "message/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Messages ";
    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <link href=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css\" rel=\"stylesheet\">
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    ";
        // line 11
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Messages", ["rendering" => "client_side", "props" => ["conversation" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 12
($context["app"] ?? null), "request", [], "any", false, false, false, 12), "query", [], "any", false, false, false, 12), "get", [0 => "conversation"], "method", false, false, false, 12), "mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "user" => twig_get_attribute($this->env, $this->source,         // line 14
($context["user"] ?? null), "id", [], "any", false, false, false, 14), "receiver" =>         // line 15
($context["receiver"] ?? null)]]);
        echo "

";
    }

    // line 19
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js\"></script>

<script>
    \$(function () {
        \$('#reply-editor').summernote({
            height: 300,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    })
</script>
";
    }

    public function getTemplateName()
    {
        return "message/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 20,  82 => 19,  75 => 15,  74 => 14,  73 => 12,  72 => 11,  69 => 10,  65 => 9,  60 => 6,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "message/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/message/index.html.twig");
    }
}
