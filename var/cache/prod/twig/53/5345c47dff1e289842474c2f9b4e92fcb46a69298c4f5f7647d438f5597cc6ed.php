<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-actions.html.twig */
class __TwigTemplate_7ad6505f9b92aed00368d6bc960575e715da04fc2f65d985f3311df11fcc9a9f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 1) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 2
($context["post"] ?? null), 2)) || ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 3
($context["post"] ?? null), 3) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "showDate", [], "any", false, false, false, 3), twig_date_converter($this->env)))))) {
            // line 4
            echo "
    <div class=\"card-header text-center\">

        <div>

            <form method=\"post\">

                ";
            // line 11
            if ((($context["blocked"] ?? null) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blocked"] ?? null), "blocker", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11))))) {
                // line 12
                echo "
                    <p>This post was blocked by ";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blocked"] ?? null), "blocker", [], "any", false, false, false, 13), "profile", [], "any", false, false, false, 13), "fullName", [], "any", false, false, false, 13), "html", null, true);
                echo "</p>

                ";
            } else {
                // line 16
                echo "
                    ";
                // line 17
                if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition(($context["post"] ?? null), "to_published")) {
                    // line 18
                    echo "
                        <button class=\"btn btn-outline-success\"
                                formaction=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 20), "action" => 1]), "html", null, true);
                    echo "\">

                            Publish

                        </button>

                    ";
                }
                // line 27
                echo "
                    ";
                // line 28
                if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition(($context["post"] ?? null), "to_rejected")) {
                    // line 29
                    echo "
                        <button class=\"btn btn-outline-danger\" ";
                    // line 30
                    (((1 === twig_compare(twig_length_filter($this->env, ($context["reasons"] ?? null)), 0))) ? (print (" type=\"button\" data-toggle=\"modal\" data-target=\"#reject\"")) : (print (twig_escape_filter($this->env, ("formaction=" . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 30), "action" => 0])), "html", null, true))));
                    echo ">

                            Reject

                        </button>

                    ";
                }
                // line 37
                echo "
                    ";
                // line 38
                if ((null === ($context["alreadyBlockedByCurator"] ?? null))) {
                    // line 39
                    echo "                        <button class=\"btn btn-outline-info\"
                                formaction=\"";
                    // line 40
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_block_post", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 40)]), "html", null, true);
                    echo "\">
                            Block
                        </button>
                    ";
                }
                // line 44
                echo "
                    ";
                // line 45
                if (($context["blocked"] ?? null)) {
                    // line 46
                    echo "                        <button class=\"btn btn-outline-warning\"
                                formaction=\"";
                    // line 47
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_unblock_post", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 47)]), "html", null, true);
                    echo "\">
                            Unblock
                        </button>
                    ";
                }
                // line 51
                echo "
                ";
            }
            // line 53
            echo "
                ";
            // line 54
            if (($context["blocked"] ?? null)) {
                // line 55
                echo "                    <div>
                        Blocked until ";
                // line 56
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blocked"] ?? null), "endDate", [], "any", false, false, false, 56), "Y-m-d H:i"), "html", null, true);
                echo "
                    </div>
                ";
            }
            // line 59
            echo "
                <hr>

                ";
            // line 62
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 1) && (0 === twig_compare(($context["voted"] ?? null), false)))) {
                // line 63
                echo "
                    <button class=\"btn btn-link\" formaction=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_vote", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 64), "vote" => 1]), "html", null, true);
                echo "\">
                        <i class=\"fa fa-thumbs-up\"></i> Yes
                    </button>

                    <button class=\"btn btn-link text-danger\"
                            formaction=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_vote", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 69), "vote" => 0]), "html", null, true);
                echo "\">
                        <i class=\"fa fa-thumbs-down\"></i> No
                    </button>

                ";
            }
            // line 74
            echo "
                ";
            // line 75
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(($context["post"] ?? null), 3) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "showDate", [], "any", false, false, false, 75), twig_date_converter($this->env))))) {
                // line 76
                echo "
                    <strong>Published
                            by: </strong> ";
                // line 78
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "publisher", [], "any", false, false, false, 78), "profile", [], "any", false, false, false, 78), "firstName", [], "any", false, false, false, 78), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "publisher", [], "any", false, false, false, 78), "profile", [], "any", false, false, false, 78), "lastName", [], "any", false, false, false, 78), "html", null, true);
                echo "

                ";
            }
            // line 81
            echo "
            </form>

        </div>

    </div>


    <div class=\"modal fade\" id=\"reject\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"reject\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <form action=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 91), "action" => 0]), "html", null, true);
            echo "\" method=\"post\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Reject Reasons</h5>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>
                    </div>
                    <div class=\"modal-body\">
                        ";
            // line 100
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["reasons"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["reason"]) {
                // line 101
                echo "
                            <div class=\"form-check\">
                                <input class=\"form-check-input\" type=\"checkbox\" name=\"reasons[]\" value=\"";
                // line 103
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 103), "html", null, true);
                echo "\"
                                       id=\"reason-";
                // line 104
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 104), "html", null, true);
                echo "\">
                                <label class=\"form-check-label\" for=\"reason-";
                // line 105
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 105), "html", null, true);
                echo "\">
                                    ";
                // line 106
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "reason", [], "any", false, false, false, 106), "html", null, true);
                echo "
                                </label>
                            </div>

                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reason'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                        <button class=\"btn btn-primary\">Reject</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


";
        }
        // line 123
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-actions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 123,  244 => 111,  233 => 106,  229 => 105,  225 => 104,  221 => 103,  217 => 101,  213 => 100,  201 => 91,  189 => 81,  181 => 78,  177 => 76,  175 => 75,  172 => 74,  164 => 69,  156 => 64,  153 => 63,  151 => 62,  146 => 59,  140 => 56,  137 => 55,  135 => 54,  132 => 53,  128 => 51,  121 => 47,  118 => 46,  116 => 45,  113 => 44,  106 => 40,  103 => 39,  101 => 38,  98 => 37,  88 => 30,  85 => 29,  83 => 28,  80 => 27,  70 => 20,  66 => 18,  64 => 17,  61 => 16,  55 => 13,  52 => 12,  50 => 11,  41 => 4,  39 => 3,  38 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/curator-actions.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-actions.html.twig");
    }
}
