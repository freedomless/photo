<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/admin.html.twig */
class __TwigTemplate_910ec69fcbee3a72608e5f6b204679a5a2ac27355ad43b290036b42b76b10f52 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
            // line 2
            echo "
    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"admin\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">

            <i class=\"fa fa-tools nav-admin-icon text-white d-none d-xl-inline-block\"></i>

            <span class=\"d-xl-none ml-xl-3\">ADMIN</span>

        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
             aria-labelledby=\"admin\">

            ";
            // line 17
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 18
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_for_curate");
                echo "\">Curator Panel</a>
            ";
            }
            // line 20
            echo "
            <div class=\"dropdown-divider\"></div>

            <span class=\"ml-4 text-muted\">Manage:</span>

            ";
            // line 25
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN")) {
                // line 26
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_list");
                echo "\">Polls</a>
            ";
            }
            // line 28
            echo "
            ";
            // line 29
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 30
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_manage_photo_of_the_day");
                echo "\">Photo of the day</a>
            ";
            }
            // line 32
            echo "
            ";
            // line 33
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 34
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series");
                echo "\">Curator Series</a>
            ";
            }
            // line 36
            echo "
            ";
            // line 37
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 38
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_list");
                echo "\">Series Selection</a>
            ";
            }
            // line 40
            echo "
            ";
            // line 41
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN")) {
                // line 42
                echo "
                <div class=\"dropdown-divider\"></div>
                
                <a class=\"dropdown-item\" href=\"";
                // line 45
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin");
                echo "\">Admin Panel</a>
            ";
            }
            // line 47
            echo "
        </div>

    </li>

";
        }
    }

    public function getTemplateName()
    {
        return "_partials/navigation/admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 47,  122 => 45,  117 => 42,  115 => 41,  112 => 40,  106 => 38,  104 => 37,  101 => 36,  95 => 34,  93 => 33,  90 => 32,  84 => 30,  82 => 29,  79 => 28,  73 => 26,  71 => 25,  64 => 20,  58 => 18,  56 => 17,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/navigation/admin.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/admin.html.twig");
    }
}
