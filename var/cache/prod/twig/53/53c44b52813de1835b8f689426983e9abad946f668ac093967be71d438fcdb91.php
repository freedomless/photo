<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-comment.html.twig */
class __TwigTemplate_43a4bc43eb14be7a42f048e3768ba3fefab7801fd2cdafa323d75cb6465ee589 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header\">

        <strong>Curator Comments</strong>

    </div>

    <div class=\"card-body p-0\">

        <ul class=\"list-group list-group-flush\">
            ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "curateComments", [], "any", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 13
            echo "                <li class=\"list-group-item\">
                    <p class=\"text-muted\"
                       data-toggle=\"tooltip\"
                       data-placement=\"top\" title=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["comment"], "user", [], "any", false, false, false, 16), "username", [], "any", false, false, false, 16), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["comment"], "user", [], "any", false, false, false, 16), "profile", [], "any", false, false, false, 16), "fullName", [], "any", false, false, false, 16), "html", null, true);
            echo "</p>
                    <p>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "text", [], "any", false, false, false, 17), "html", null, true);
            echo "</p>
                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </ul>

        <form action=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_comment", ["id" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 22)]), "html", null, true);
        echo "\" method=\"post\">

            <textarea name=\"comment\" id=\"curator-comment\" rows=\"5\" class=\"form-control\"
                      placeholder=\"Comment\"></textarea>

            <button class=\"btn btn-blue btn-block\">Add</button>

        </form>

    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 22,  74 => 20,  65 => 17,  59 => 16,  54 => 13,  50 => 12,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/_partials/curator-comment.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-comment.html.twig");
    }
}
