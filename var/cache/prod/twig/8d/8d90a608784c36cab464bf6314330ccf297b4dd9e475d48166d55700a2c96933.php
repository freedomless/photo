<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/navigation.html.twig */
class __TwigTemplate_1f38f5a420ff988499f982fbdba25a6ec299bc63450e7a789070b3262993a2f8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["stats"] = $this->extensions['App\Twig\AccountStatsExtension']->getAccountStats(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "get", [0 => "slug"], "method", false, false, false, 1));
        // line 2
        echo "
<ul class=\"nav justify-content-center custom-active  flex-column flex-sm-row\">

    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 8)]), "html", null, true);
        echo "\">
            Published (";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "published", [], "any", false, false, false, 9), "html", null, true);
        echo ")</a>

    </li>


    ";
        // line 14
        if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\PostSettingsExtension']->getUserSettings(($context["user"] ?? null)), "is_portfolio_visible", [], "any", false, false, false, 14), true)) || (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 14) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 14))))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"))) {
            // line 15
            echo "
        <li class=\"nav-item flex-sm-fill text-center\">

            <a class=\"nav-link no-shadow ";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_portfolio", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_portfolio", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">
                Uploaded (";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "portfolio", [], "any", false, false, false, 20), "html", null, true);
            echo ")</a>

        </li>

    ";
        }
        // line 25
        echo "

    <li class=\"nav-item flex-sm-fill text-center\">
        <a class=\"nav-link no-shadow ";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_products", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 29)]), "html", null, true);
        echo "\">
            Prints for Sale (";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "products", [], "any", false, false, false, 30), "html", null, true);
        echo ")</a>
    </li>


    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow ";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_albums", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_albums", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 37)]), "html", null, true);
        echo "\">
            Albums (";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "albums", [], "any", false, false, false, 38), "html", null, true);
        echo ")</a>

    </li>


    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 43) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 43), "id", [], "any", false, false, false, 43), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 43))))) {
            // line 44
            echo "
        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow ";
            // line 46
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_favorites", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_favorites", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\">
                Favorites (";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "favorites", [], "any", false, false, false, 48), "html", null, true);
            echo ")</a>
        </li>

        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow ";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_followed", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_followed", ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 53)]), "html", null, true);
            echo "\">
                Followed (";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "followed", [], "any", false, false, false, 54), "html", null, true);
            echo ")</a>
        </li>

    ";
        }
        // line 58
        echo "

</ul>

";
        // line 62
        if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 62), "get", [0 => "_route"], "method", false, false, false, 62), "web_profile")) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 63
($context["app"] ?? null), "request", [], "any", false, false, false, 63), "get", [0 => "_route"], "method", false, false, false, 63), "web_profile_portfolio"))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 64
($context["app"] ?? null), "request", [], "any", false, false, false, 64), "get", [0 => "_route"], "method", false, false, false, 64), "web_profile_products")))) {
            // line 65
            echo "    <hr>
    ";
            // line 66
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 66), "get", [0 => "_route"], "method", false, false, false, 66), "web_profile_products"))) {
                // line 67
                echo "
        <div class=\"text-center\">

            <strong>Share your Print Store!&nbsp;

                ";
                // line 72
                echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 74
($context["user"] ?? null), "slug", [], "any", false, false, false, 74), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 75
($context["app"] ?? null), "request", [], "any", false, false, false, 75), "get", [0 => "type"], "method", false, false, false, 75)]), "classes" => "pr-2", "icon_classes" => ""]);
                // line 79
                echo "

                ";
                // line 81
                echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 83
($context["user"] ?? null), "slug", [], "any", false, false, false, 83), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 84
($context["app"] ?? null), "request", [], "any", false, false, false, 84), "get", [0 => "type"], "method", false, false, false, 84)]), "media" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 86
($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", true, true, false, 86) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", false, false, false, 86)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", false, false, false, 86)) : ("")), "description" => "Best Quality Art Prints", "classes" => "pr-2"]);
                // line 89
                echo "

                ";
                // line 91
                echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 93
($context["user"] ?? null), "slug", [], "any", false, false, false, 93), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 94
($context["app"] ?? null), "request", [], "any", false, false, false, 94), "get", [0 => "type"], "method", false, false, false, 94)])]);
                // line 96
                echo "

            </strong>

        </div>

    ";
            }
            // line 103
            echo "
    <div class=\"d-flex justify-content-center\">
        <ul class=\"nav custom-active w-50\">
            <li class=\"nav-item flex-sm-fill text-center\">
                <a class=\"nav-link no-shadow ";
            // line 107
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("single", "type", "active shadow"), "html", null, true);
            echo "\"
                   href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 108), "get", [0 => "_route"], "method", false, false, false, 108), ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 108)]), "html", null, true);
            echo "\">
                    Single
                    ";
            // line 110
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 110), "get", [0 => "_route"], "method", false, false, false, 110), "web_profile"))) {
                // line 111
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "single_published", [], "any", false, false, false, 111), "html", null, true);
                echo ")
                    ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 112
($context["app"] ?? null), "request", [], "any", false, false, false, 112), "get", [0 => "_route"], "method", false, false, false, 112), "web_profile_products"))) {
                // line 113
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "products_single", [], "any", false, false, false, 113), "html", null, true);
                echo ")
                    ";
            } else {
                // line 115
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "single_portfolio", [], "any", false, false, false, 115), "html", null, true);
                echo ")
                    ";
            }
            // line 117
            echo "                </a>

            </li>
            <li class=\"nav-item flex-sm-fill text-center\">

                <a class=\"nav-link no-shadow ";
            // line 122
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("series", "type", "active shadow"), "html", null, true);
            echo "\"
                   href=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 123), "get", [0 => "_route"], "method", false, false, false, 123), ["slug" => twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "slug", [], "any", false, false, false, 123), "type" => "series"]), "html", null, true);
            echo "\">
                    Series
                    ";
            // line 125
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 125), "get", [0 => "_route"], "method", false, false, false, 125), "web_profile"))) {
                // line 126
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "series_published", [], "any", false, false, false, 126), "html", null, true);
                echo ")
                    ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 127
($context["app"] ?? null), "request", [], "any", false, false, false, 127), "get", [0 => "_route"], "method", false, false, false, 127), "web_profile_products"))) {
                // line 128
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "products_series", [], "any", false, false, false, 128), "html", null, true);
                echo ")
                    ";
            } else {
                // line 130
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["stats"] ?? null), "series_portfolio", [], "any", false, false, false, 130), "html", null, true);
                echo ")
                    ";
            }
            // line 132
            echo "                </a>

            </li>
        </ul>
    </div>

";
        }
    }

    public function getTemplateName()
    {
        return "user/profile/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 132,  278 => 130,  272 => 128,  270 => 127,  265 => 126,  263 => 125,  258 => 123,  254 => 122,  247 => 117,  241 => 115,  235 => 113,  233 => 112,  228 => 111,  226 => 110,  221 => 108,  217 => 107,  211 => 103,  202 => 96,  200 => 94,  199 => 93,  198 => 91,  194 => 89,  192 => 86,  191 => 84,  190 => 83,  189 => 81,  185 => 79,  183 => 75,  182 => 74,  181 => 72,  174 => 67,  172 => 66,  169 => 65,  167 => 64,  166 => 63,  165 => 62,  159 => 58,  152 => 54,  148 => 53,  144 => 52,  137 => 48,  133 => 47,  129 => 46,  125 => 44,  123 => 43,  115 => 38,  111 => 37,  107 => 36,  98 => 30,  94 => 29,  90 => 28,  85 => 25,  77 => 20,  73 => 19,  69 => 18,  64 => 15,  62 => 14,  54 => 9,  50 => 8,  46 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/profile/navigation.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/navigation.html.twig");
    }
}
