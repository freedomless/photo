<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/posters.html.twig */
class __TwigTemplate_ccb873f8a1393d52c467c16af16d01ac4f63957a7212e2270dd1dfcfca53a8dd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((((array_key_exists("selection_series", $context) &&  !(null === ($context["selection_series"] ?? null))) && array_key_exists("photo_of_the_day", $context)) &&  !(null === ($context["photo_of_the_day"] ?? null)))) {
            // line 2
            echo "
    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                ";
            // line 7
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 7)->display($context);
            // line 8
            echo "            </div>

            <div class=\"col-lg-4\">
                ";
            // line 11
            $this->loadTemplate("home/selection_series.html.twig", "home/posters.html.twig", 11)->display($context);
            // line 12
            echo "            </div>
        </div>
    </div>

";
        } elseif (((        // line 16
array_key_exists("photo_of_the_day", $context) &&  !(null === ($context["photo_of_the_day"] ?? null))) && array_key_exists("video", $context))) {
            // line 17
            echo "
    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                ";
            // line 22
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 22)->display($context);
            // line 23
            echo "            </div>

            <div class=\"col-lg-4\">
                ";
            // line 26
            $this->loadTemplate("home/video.html.twig", "home/posters.html.twig", 26)->display($context);
            // line 27
            echo "            </div>
        </div>
    </div>

";
        } elseif ((        // line 31
array_key_exists("photo_of_the_day", $context) &&  !(null === ($context["photo_of_the_day"] ?? null)))) {
            // line 32
            echo "
    ";
            // line 33
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 33)->display($context);
            // line 34
            echo "
";
        } else {
            // line 36
            echo "
    ";
            // line 37
            $this->loadTemplate("home/selection_series.html.twig", "home/posters.html.twig", 37)->display($context);
            // line 38
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "home/posters.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 38,  99 => 37,  96 => 36,  92 => 34,  90 => 33,  87 => 32,  85 => 31,  79 => 27,  77 => 26,  72 => 23,  70 => 22,  63 => 17,  61 => 16,  55 => 12,  53 => 11,  48 => 8,  46 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/posters.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/posters.html.twig");
    }
}
