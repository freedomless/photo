<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/faq.html.twig */
class __TwigTemplate_7f7417f56fdf7356f5f3cf2448c0313e5a8887c5e285e048ca42be818cc1aa73 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "pages/faq.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "F.A.Q";
    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "F.A.Q";
    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <span class=\"text-muted\">Last Update: 28 Aug, 2019</span>
";
    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
    <div class=\"col-md-12\">

        <div>
            <h2><strong>Q: What is ";
        // line 16
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A: </strong> Do you want to know whether your photos would be posted among photos carefully
                selected by experienced
                professional curators with the highest criteria? All photos posted at the ";
        // line 20
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s gallery have
                been carefully selected by a professional
                curatorial team. The curators in the team have many years of experience and world-class standards.
                ";
        // line 23
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " is a curated gallery, which means that the posted photos are only at the highest level,
                which meets the great number of different criteria of the curators.
            </p>
        </div>

        <div>
            <h2><strong>Q: How are my images protected?</strong></h2>

            <p><strong>A:</strong> We took safety precautions because we consider the illegal use of protected images as
                a very serious issue.
            <ul>
                <li>We have prevented the image from being downloaded by dragging or right-clicking.</li>
                <li>All images have a watermark with the author's name.</li>
                <li>The metadata of all images has been removed.</li>
            </ul>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I add photos to my favourites?</strong></h2>

            <p><strong>A:</strong> Use the heart icon in the toolbar below the photos to add photos to your favorites.
                You can access your favourite photos from:
                <br><code>Click on: \"My Profile\" in the top toolbar <b>>></b> View <b>>></b> Favourites.</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I follow my favourite photographers?</strong></h2>

            <p><strong>A:</strong> Log in to the author's profile and click on the log image icon, or use the same icon
                in the bottom toolbar of any of the author's photos when open in large size.</p>
        </div>

        <div>
            <h2><strong>Q: How do albums work?</strong></h2>

            <p><strong>A:</strong> With albums, you can organize your photos quickly and easily.
                Just create a new album and click on all the photos you want to add in it.
                You can choose a cover photo for your album.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I upload photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: “UPLOAD” button in the upper right corner <b>>></b> select
                    “PHOTO”</code><br>
                You can drag the photo into the blue square or press „Browse“ to select a photo from
                your device. Your photos should be in 8-bit standard sRGB and JPG format.</p>
        </div>

        <div>
            <h2><strong>Q: What are series?</strong></h2>

            <p><strong>A:</strong> The series is a set of 3 to 5 photos that have a common theme and are united by the
                idea the author wants to represent, through concept, composition, or editing, but always on the basis of
                a consistent and common idea.</p>
        </div>

        <div>
            <h2><strong>Q: How do I upload a series of photos?</strong></h2>

            <p><strong>A:</strong> Open the UPLOAD button in the upper right corner and select SERIES. You can drag a
                photo into the blue square or click Browse, to select a photo from your device. The photo uploaded at
                the first position will be the cover of the series. Press plus if you want to add more photos. The
                minimum number of series upload photos is 3 and the maximum is 5.
                The series should be created by photos that are related by theme, idea or story.
            </p>
        </div>

        <div>
            <h2><strong>Q: Can I create a series of photos already posted in ";
        // line 95
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Yes, you can. <br>
                <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos</code><br>
                There is a ‘Create a Series’ button in the upper right corner.
                This button opens the menu for editing a series of already posted photos. If the series is not approved
                by the curatorial team, this does not affect the photos already posted, they remain posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: What are the recommended photo upload sizes?</strong></h2>

            <p><strong>A:</strong> We recommend that you upload your photos in their original size if you want to use
                them to sell prints through ";
        // line 109
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ".
                The photos will automatically be resized to a smaller optimal size when displayed in ";
        // line 110
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " and
                the file with high resolution will not be posted. The photos sent to curators must be at least 1000
                pixels along the long side, but for better viewing, we recommend that you always upload photos of at
                least 1600 pixels on the long side or more.
            </p>
        </div>

        <div>
            <h2><strong>Q: Why don't the colours of the photo in ";
        // line 118
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " match the colours of the photo I
                    uploaded?</strong></h2>

            <p><strong>A:</strong> Remember to always convert the photos to 8-bit standard sRGB saved in JPG format
                before uploading them.</p>
        </div>

        <div>
            <h2><strong>Q: How do I send photos to curators for review in the gallery?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos
                    <b>>></b>
                    Send to Curators </code><br>
                By clicking the \"Send to Curators\" button you send the photo to the curatorial team. Users can send 10
                photos per week for curating.
                If your photo has been approved for publication by the curatorial team, it will be posted in the
                ";
        // line 134
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " gallery within a few days. When a photo is posted, the author will be notified.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does consumer voting work?</strong></h2>

            <p><strong>A:</strong> Clicking on the \"VOTE\" button in the upper right corner gives you the opportunity to
                vote for the photos that have been sent to the official curatorial team. You can give a 5 point rating
                to each photo, enabling the authors and curators to understand the user rating of the sent photos.
                This vote helps a lot the site development and performance, but it is not decisive whether or not a
                photo will be posted. The final decision is made by the official curatorial team and is not subject to
                discussion.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does curation work?</strong></h2>

            <p><strong>A:</strong> After you send a photo to the curators, it will be assessed by them in terms of idea,
                mood, aesthetics and technical quality.
                The originality and variety of the gallery are very important. The curatorial team will always keep in
                mind the rating of the consumer curation,
                but sometimes it is possible for a high-rated photo to be rejected, because too many photos with a
                similar motive were previously posted on the site; on the contrary, a photo with a very low rating can
                be posted because the curators saw something special in it.
            </p>
        </div>

        ";
        // line 164
        echo "        ";
        // line 165
        echo "
        ";
        // line 167
        echo "        ";
        // line 168
        echo "        ";
        // line 169
        echo "        ";
        // line 170
        echo "
        <div>
            <h2><strong>Q: </strong>How do I know whether my photo has been posted or rejected?</h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                Each photo has information in the upper right corner concerning its status. The user receives an
                additional notification when his photo is posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I edit information in photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                By clicking on the selected photo or on the edit icon, the edit menu opens.</p>
        </div>

        <div>
            <h2><strong>Q: How do I delete a photo?</strong></h2>

            <p><strong>A:</strong> From the \"My Profile\" in the top toolbar> Manage Your: Photos from the menu that
                opens by
                clicking on the selected photo or on the edit icon. You can only delete photos that have not been posted
                or have not been sent for curation. Posted photos and the ones that are in the process of curation have
                no deletion option.</p>
        </div>

        <div>
            <h2><strong>Q: Do you allow watermark, logo or other tagging on the photos?</strong></h2>

            <p><strong>A:</strong> All photos in ";
        // line 202
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " are automatically tagged by us with your name.
                Uploading
                photos with watermarks or other additional markings is not allowed.</p>
        </div>

        <div>
            <h2><strong>Q: Are photo frames and borders allowed?</strong></h2>

            <p><strong>A:</strong> The photos sent to the curators should not contain any frames or borders because we
                want to keep elegant style in the gallery and the frames sometimes distract. If your photo is diptych or
                triptych, borders are allowed, but please make them as discreet as possible.</p>
        </div>

        <div>
            <h2><strong>Q: Am I allowed to use stock photos from other photographs in my works?</strong></h2>

            <p><strong>A:</strong> All the parts in the photos you send to the curators should consist of photos taken
                by you. You are not allowed to use stock photos in photomontages.</p>
        </div>

        <div>
            <h2><strong>Q: What are the rules for writing comments in ";
        // line 223
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Offensive comments or personal attacks can cause your account to be blocked or
                deleted. Remember to always use friendly and polite language when communicating in ";
        // line 226
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ".
                Never write when you are angry. If you have a problem or conflict with another member of the site,
                contact ";
        // line 228
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " Support and we will assist you.</p>
        </div>

        <div>
            <h2><strong>Q: Can I sale my photos at ";
        // line 232
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Yes. You can list for sale on our Print store any of your Published photos/series.</p>
        </div>

        <div>
            <h2><strong>Q: How to list my photo(s)/series for sale at ";
        // line 238
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Photos/Series <b>>></b> Click on \"SALE\" button next to each published photo/series.</code>
                You can then manage each of your listed for sale items from <code> \"My Profile\" <b>>></b> Manage <b>>></b> Products</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my photo(s)/series that are listed for sale at ";
        // line 247
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Products <b>>></b> Click on \"Remove\" button next to each photo/series.</code>
                Important note: Each photo/series that is deleted from the Print Store Gallery will remain visible and available for purchase for \"30\" days after the deletion date. When the time expires the photo will be permanently removed from the store.
                In the meantime, you will still be paid for each sale that your photo/series generates.
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my account?</strong></h2>

            <p><strong>A:</strong> If you wish no longer to be a member of ";
        // line 259
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo " you have the opportunity to
                delete your account. To delete your account, please contact us using this <a
                        href=\"";
        // line 261
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
        echo "\">form</a> and will delete your account for you.
                Deleting your account is a final decision and we cannot get it back, so give yourself a few days before
                you decide to do such an action. Be sure to contact the site support first if you have any problems that
                made you proceed with deleting your account. The site team will help you solve the problems.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I filter nude contents?</strong></h2>

            <p><strong>A:</strong> Enable or disable the nude content filter in your account settings.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact ";
        // line 275
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Use the contact form.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact a member of ";
        // line 281
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> To contact a member of ";
        // line 283
        echo twig_escape_filter($this->env, ($context["site_name"] ?? null), "html", null, true);
        echo ", log in to their profile and click on the
                personal message icon.</p>
        </div>

        <div>
            <h2><strong>Q: How do I contact the support?</strong></h2>

            <p><strong>A:</strong> Before contacting the support, please read the FAQ carefully to make sure that your
                question has not been answered.
                You can contact us for any questions or suggestions.
                <br>The contact email is: <strong>";
        // line 293
        echo twig_escape_filter($this->env, ($context["site_email"] ?? null), "html", null, true);
        echo "</strong>
            </p>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "pages/faq.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  425 => 293,  412 => 283,  407 => 281,  398 => 275,  381 => 261,  376 => 259,  361 => 247,  349 => 238,  340 => 232,  333 => 228,  328 => 226,  322 => 223,  298 => 202,  264 => 170,  262 => 169,  260 => 168,  258 => 167,  255 => 165,  253 => 164,  221 => 134,  202 => 118,  191 => 110,  187 => 109,  170 => 95,  95 => 23,  89 => 20,  82 => 16,  76 => 12,  72 => 11,  67 => 8,  63 => 7,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "pages/faq.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/pages/faq.html.twig");
    }
}
