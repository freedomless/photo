<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* album/index.html.twig */
class __TwigTemplate_0f92f944dfe677e6c7323bc0e10dcd6b2092378998d4627f22aae142c8dae0cf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "album/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "'s
    Albums
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"container basic-layout\">

        ";
        // line 13
        if ((0 === twig_compare(twig_length_filter($this->env, ($context["albums"] ?? null)), 0))) {
            // line 14
            echo "
            <h3 class=\"text-muted text-center mt-5 mb-5\">
                You don't have any albums.
            </h3>

        ";
        }
        // line 20
        echo "
        ";
        // line 21
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 21) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21), twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 21))))) {
            // line 22
            echo "
            ";
            // line 23
            if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "posts", [], "any", false, false, false, 23)), 0))) {
                // line 24
                echo "
                <h3 class=\"text-muted text-center mt-5 mb-5\">
                    You have to add photo before you can create album
                </h3>

                <div class=\"mb-5 text-center\">

                    <a href=\"";
                // line 31
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create");
                echo "\"
                       class=\"btn btn-info text-white\">Upload Photos</a>

                </div>

            ";
            }
            // line 37
            echo "
            ";
            // line 38
            if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "posts", [], "any", false, false, false, 38)), 0))) {
                // line 39
                echo "
                <div class=\"mb-5\">

                    <a href=\"";
                // line 42
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_create");
                echo "\"
                       class=\"btn btn-info text-white\">Create new Album</a>

                </div>

            ";
            }
            // line 48
            echo "
        ";
        }
        // line 50
        echo "
        <div class=\"row image-browse-container albums mt-5\">

            ";
        // line 53
        $this->loadTemplate("album/album.html.twig", "album/index.html.twig", 53)->display($context);
        // line 54
        echo "
        </div>
        ";
        // line 56
        $this->loadTemplate("_partials/loader.html.twig", "album/index.html.twig", 56)->display($context);
        // line 57
        echo "
    </div>

";
    }

    // line 61
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo "    ";
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["albums"] ?? null)), 0))) {
            // line 63
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
            echo "\"></script>
        <script>
            infinite('.albums');

            \$(document).on('click', '.open-confirm', function () {
                let url = \$(this).data('url');
                \$('.modal form').attr('action', url);
            });
        </script>
    ";
        }
    }

    public function getTemplateName()
    {
        return "album/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 63,  158 => 62,  154 => 61,  147 => 57,  145 => 56,  141 => 54,  139 => 53,  134 => 50,  130 => 48,  121 => 42,  116 => 39,  114 => 38,  111 => 37,  102 => 31,  93 => 24,  91 => 23,  88 => 22,  86 => 21,  83 => 20,  75 => 14,  73 => 13,  68 => 10,  64 => 9,  57 => 5,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "album/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/album/index.html.twig");
    }
}
