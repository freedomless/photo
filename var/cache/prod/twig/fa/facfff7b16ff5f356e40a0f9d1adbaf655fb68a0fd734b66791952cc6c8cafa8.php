<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gallery/nav.html.twig */
class __TwigTemplate_910bdcd004da7320230c651ce163cd4f3a256e2564e8c80801c4cb7072797640 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div>
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("latest", "page", "active "), "html", null, true);
        echo "\"
               href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery", ["page" => "latest"]);
        echo "\"><strong>Single</strong></a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link ";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_series", "_route", "active"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery_series");
        echo "\">
                <strong>Series</strong>
            </a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_awarded", "_route", "active"), "html", null, true);
        echo "\"
               href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery_awarded");
        echo "\">
                <strong>Photo of the Day</strong>
            </a>
        </li>

    </ul>

    <hr>

</div>
";
    }

    public function getTemplateName()
    {
        return "gallery/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 16,  64 => 15,  53 => 9,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "gallery/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/gallery/nav.html.twig");
    }
}
