<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/nav.html.twig */
class __TwigTemplate_c5e67e00d194739e42b460e99c5c05fcc87cc3b7d80c569e4341e6c6336f804f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"mt-3\">
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_popular", "_route", "active shadow"), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("photo_of_the_day_history", "_route", "active shadow"), "html", null, true);
        echo "\"
               href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_popular");
        echo "\">
                <strong>Trending</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_latest", "_route", "active shadow"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_latest");
        echo "\">
                <strong>Single</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_series", "_route", "active shadow"), "html", null, true);
        echo "\"
               href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_series");
        echo "\">
                <strong>Series</strong>
            </a>
        </li>
    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "home/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 16,  65 => 15,  55 => 10,  47 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/nav.html.twig");
    }
}
