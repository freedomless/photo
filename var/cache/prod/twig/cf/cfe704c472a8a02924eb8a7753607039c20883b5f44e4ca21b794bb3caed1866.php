<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/index.html.twig */
class __TwigTemplate_bdb151b634b04d92f9fdcf05f50b57a7f3c10f49c1fcd9c2dab58b8fa8a27893 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'row_class' => [$this, 'block_row_class'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["title"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "get", [0 => "type"], "method", false, false, false, 3), "single"))) ? ("Photo") : ("Series"));
        // line 1
        $this->parent = $this->loadTemplate("template.html.twig", "product/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " Product Manager";
    }

    // line 7
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " Product Manager";
    }

    // line 9
    public function block_row_class($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "image-browse-container";
    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
    ";
        // line 13
        if ((1 === twig_compare(twig_length_filter($this->env, ($context["posts"] ?? null)), 0))) {
            // line 14
            echo "
        <div class=\"col-md-12\">

            <div class=\"row infinite-photos\">

                ";
            // line 19
            $this->loadTemplate("product/posts.html.twig", "product/index.html.twig", 19)->display($context);
            // line 20
            echo "
            </div>

            ";
            // line 23
            $this->loadTemplate("_partials/loader.html.twig", "product/index.html.twig", 23)->display($context);
            // line 24
            echo "
        </div>

    ";
        } else {
            // line 28
            echo "
        ";
            // line 29
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 29), "get", [0 => "page"], "method", false, false, false, 29), 1))) {
                // line 30
                echo "
            <div class=\"col-12\">

                <p class=\"text-muted\">You don't have any products added yet.</p>

            </div>

        ";
            } else {
                // line 38
                echo "
            <div class=\"col-12\">

                <p class=\"text-muted\">No products available.</p>

            </div>

        ";
            }
            // line 46
            echo "
    ";
        }
        // line 48
        echo "


";
    }

    // line 53
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "
    <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.infinite-photos')</script>

    ";
        // line 59
        $macros["__internal_9bb9c5493da7f60e4a9d7121b1ceb7c12e633692a8a501f075a74760697056ca"] = $this->loadTemplate("_partials/warning.html.twig", "product/index.html.twig", 59)->unwrap();
        // line 60
        echo "
    ";
        // line 61
        echo twig_call_macro($macros["__internal_9bb9c5493da7f60e4a9d7121b1ceb7c12e633692a8a501f075a74760697056ca"], "macro_warning", ["delete-warning-modal", "delete-warning-form", "delete-warning-btn", "Confirm!", "Are you sure you want delete this product?"], 61, $context, $this->getSourceContext());
        echo "


";
    }

    public function getTemplateName()
    {
        return "product/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 61,  157 => 60,  155 => 59,  148 => 55,  145 => 54,  141 => 53,  134 => 48,  130 => 46,  120 => 38,  110 => 30,  108 => 29,  105 => 28,  99 => 24,  97 => 23,  92 => 20,  90 => 19,  83 => 14,  81 => 13,  78 => 12,  74 => 11,  67 => 9,  60 => 7,  53 => 5,  48 => 1,  46 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/index.html.twig");
    }
}
