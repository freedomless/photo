<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/series-from-published.html.twig */
class __TwigTemplate_888a6e58180c0ff0659330fd0d88c91aa43eb7536d5ab620f8f714a6067db8f2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["order"] = [];
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_sort_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "value", [], "any", false, false, false, 5), "children", [], "any", false, false, false, 5), function ($__a__, $__b__) use ($context, $macros) { $context["a"] = $__a__; $context["b"] = $__b__; return (twig_get_attribute($this->env, $this->source, ($context["a"] ?? null), "position", [], "any", false, false, false, 5) <=> twig_get_attribute($this->env, $this->source, ($context["b"] ?? null), "position", [], "any", false, false, false, 5)); }));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 6
            $context["order"] = twig_array_merge(($context["order"] ?? null), [0 => twig_get_attribute($this->env, $this->source, $context["child"], "id", [], "any", false, false, false, 6)]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "photo/series-from-published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 9), "method", [], "any", false, false, false, 9), "POST"))) ? ("Create") : ("Update"));
        echo " Series";
    }

    // line 11
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    <style>
        .series-option {
            text-align: center;
        }

        .series-option label img {
            max-width: 100%;
            max-height: 300px;
            border: 5px solid transparent;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            cursor: pointer;

        }

        .series-option span {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translateY(-50%) translateX(-50%);
            font-weight: bold;
            font-size: 40px;
            -webkit-text-stroke: 2px black;
        }

        .series-option input {
            display: none;
        }

        .series-option input:checked + img {
            border: 5px dashed #aaa;
        }

        .series-option input:checked + img.cover {
            border: 5px dashed #375a7f !important;
        }
    </style>
";
    }

    // line 52
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "
    ";
        // line 54
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "form_div_layout.html.twig"], true);
        // line 55
        echo "
    ";
        // line 56
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 56), "isVerified", [], "any", false, false, false, 56), false))) {
            // line 57
            echo "        ";
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/series-from-published.html.twig", 57)->display($context);
            // line 58
            echo "    ";
        } else {
            // line 59
            echo "        ";
            if ((-1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 59)), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "min", [], "any", false, false, false, 59)))) {
                // line 60
                echo "
            <div class=\"container center\">

                <div class=\"jumbotron d-flex justify-content-center align-items-center flex-column \">

                    <h3 class=\"text-center text-muted\">You don not have enough published photos!</h3>

                    <small class=\"text-muted mb-3\">Send images to curator or </small>

                    <a href=\"";
                // line 69
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create");
                echo "\" class=\"btn btn-blue\">Upload</a>

                </div>

            </div>

        ";
            } else {
                // line 76
                echo "
            <div class=\"container basic-layout\">

                <hr>

                <h3>";
                // line 81
                echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 81), "method", [], "any", false, false, false, 81), "POST"))) ? ("Create") : ("Update"));
                echo " Series</h3>

                <hr>

                <div class=\"upload-single\">

                    <div class=\"upload\">

                        ";
                // line 89
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
                echo "

                        <div class=\"row\">

                            <div class=\"col-12\">

                                <div class=\"form-group row  pl-3 pr-3\">

                                    ";
                // line 97
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 97), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 98
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "category", [], "any", false, false, false, 98), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 104
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 104), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 105
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 105), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 111
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 111), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 112
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tags", [], "any", false, false, false, 112), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 118
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 118), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 119
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 119), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"row pl-3\">

                                    <div class=\"col-md-3\"></div>

                                    <div class=\"col-md-9\">

                                        <div class=\"form-check\">
                                            ";
                // line 130
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 130), 'widget', ["attr" => ["class" => "form-check-input"]]);
                echo "
                                            <label for=\"";
                // line 131
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 131), "vars", [], "any", false, false, false, 131), "id", [], "any", false, false, false, 131), "html", null, true);
                echo "\">
                                                ";
                // line 132
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isNude", [], "any", false, false, false, 132), "vars", [], "any", false, false, false, 132), "label", [], "any", false, false, false, 132), "html", null, true);
                echo "
                                            </label>
                                        </div>

                                        <div class=\"form-check\">
                                            ";
                // line 137
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 137), 'widget', ["attr" => ["class" => "form-check-input"]]);
                echo "
                                            <label for=\"";
                // line 138
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 138), "vars", [], "any", false, false, false, 138), "id", [], "any", false, false, false, 138), "html", null, true);
                echo "\">
                                                ";
                // line 139
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "isGreyscale", [], "any", false, false, false, 139), "vars", [], "any", false, false, false, 139), "label", [], "any", false, false, false, 139), "html", null, true);
                echo "
                                            </label>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr>

                        ";
                // line 153
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 153), "children", [], "any", true, true, false, 153)) {
                    // line 154
                    echo "
                            <div class=\"row\">

                                <div class=\"col-md-12\">

                                    <div class=\"row\">

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-muted\" id=\"selected-images\">Select photos</h5>

                                            <div class=\"text-danger\">";
                    // line 165
                    echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 165), 'errors')), "html", null, true);
                    echo "</div>

                                        </div>

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-right text-muted\">

                                                <span class=\"selected-images ";
                    // line 173
                    echo (((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 173), "value", [], "any", false, false, false, 173), "children", [], "any", false, false, false, 173)), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 173)))) ? ("text-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 173), "value", [], "any", false, false, false, 173), "children", [], "any", false, false, false, 173)), "html", null, true);
                    echo "</span>/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 173), "html", null, true);
                    echo "

                                            </h5>

                                        </div>

                                    </div>

                                    <hr class=\"no-gutter\">

                                </div>

                            </div>

                            <div class=\"row available-photos  d-flex justify-content-center align-items-center\"
                                 style=\"height: 600px;overflow: auto\">

                                ";
                    // line 190
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 190));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 191
                        echo "
                                    <div class=\"series-option col-lg-3 col-md-4 col-sm-6 col-12\">

                                        <div class=\"thumb-container\">

                                            <span class=\"order\"></span>

                                            <label for=\"post_children_";
                        // line 198
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 198), "value", [], "any", false, false, false, 198), "html", null, true);
                        echo "\">

                                                ";
                        // line 200
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                        echo "

                                                <img src=\"";
                        // line 202
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 202), "label", [], "any", false, false, false, 202), "html", null, true);
                        echo "\" alt=\"\">

                                            </label>

                                        </div>

                                    </div>

                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 211
                    echo "
                            </div>

                        ";
                } else {
                    // line 215
                    echo "
                            <div class=\"row\">

                                ";
                    // line 218
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 218));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 219
                        echo "
                                    <div class=\"col-md-3 mb-3\">

                                        <img src=\"";
                        // line 222
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 222), "thumbnail", [], "any", false, false, false, 222), "html", null, true);
                        echo "\" alt=\"\" height=\"200px\" width=\"100%\" style=\"object-fit: cover\">

                                    </div>

                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 227
                    echo "
                            </div>

                        ";
                }
                // line 231
                echo "
                        <div class=\"text-center mt-5\">

                            ";
                // line 234
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "create", [], "any", false, false, false, 234), 'widget', ["attr" => ["class" => "btn btn-blue"], "label" => (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 234), "method", [], "any", false, false, false, 234), "POST"))) ? ("Create") : ("Update"))]);
                echo "

                        </div>

                        ";
                // line 238
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
                echo "

                    </div>

                </div>

            </div>

        ";
            }
            // line 247
            echo "    ";
        }
        // line 248
        echo "


";
    }

    // line 253
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 254
        echo "
    <script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.available-photos', '.available-photos');</script>

    <script>

        \$(function () {

            let hiddenOrder = \$('#post_position');

            hiddenOrder.val('";
        // line 265
        echo twig_escape_filter($this->env, twig_join_filter(($context["order"] ?? null), ","), "html", null, true);
        echo "')

            let order = hiddenOrder.val() ? hiddenOrder.val().split(',') : [];

            let selected = \$('.selected-images');

            function updateNumbers() {
                \$('.series-option input[type=\"checkbox\"]').each(function (index, item) {

                    item = \$(item);

                    let i = order.indexOf(item.val());

                    if (i > -1) {

                        item.siblings('img').removeClass('cover');

                        if (i === 0) {
                            item.siblings('img').addClass('cover');
                        }

                        item.parent().siblings('.order').text(i + 1)
                        item.prop('checked', true)
                    } else {
                        item.parent().siblings('.order').html('')
                    }

                });
            }

            updateNumbers();

            function updateCheckbox(e) {

                e.preventDefault();

                if (e.target.checked) {

                    if (order.length + 1 > ";
        // line 303
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "max", [], "any", false, false, false, 303), "html", null, true);
        echo ") {
                        \$(e.target).prop('checked', false)
                        return false;
                    }

                    order.push(e.target.value);

                    \$(e.target).parent().siblings('.order').text(order.length)

                } else {

                    order = order.filter(function (v) {
                        return v != e.target.value;
                    })

                }

                updateNumbers();

                hiddenOrder.val(order);

                selected.text(order.length);

            }

            \$(document).on('change', '.series-option input[type=\"checkbox\"]', updateCheckbox)

            \$('#reset-selection').on('click', function (e) {
                e.preventDefault();
                \$('.series-option input[type=\"checkbox\"]').prop('checked', false);
                \$('.selected-images').text(0);
                order = [];

                updateNumbers()
            })
        });

        \$(function () {
            let h = \$('#selected-images');

            \$(document).scroll(\$.throttle(250, function (e) {

                if (\$(window).scrollTop() > 700) {

                    h.css({
                        'position': 'fixed',
                        'left': '50%',
                        'top': '100px',
                        'background': '#fff',
                        'zIndex': 100,
                        'transform': 'translateX(-50%)'
                    });
                    h.addClass('shadow rounded p-2')

                } else {

                    h.removeAttr('style');
                    h.removeClass('shadow rounded p-2')

                }
            }))
        });

    </script>
";
    }

    public function getTemplateName()
    {
        return "photo/series-from-published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  507 => 303,  466 => 265,  453 => 255,  450 => 254,  446 => 253,  439 => 248,  436 => 247,  424 => 238,  417 => 234,  412 => 231,  406 => 227,  395 => 222,  390 => 219,  386 => 218,  381 => 215,  375 => 211,  360 => 202,  355 => 200,  350 => 198,  341 => 191,  337 => 190,  313 => 173,  302 => 165,  289 => 154,  287 => 153,  270 => 139,  266 => 138,  262 => 137,  254 => 132,  250 => 131,  246 => 130,  232 => 119,  228 => 118,  219 => 112,  215 => 111,  206 => 105,  202 => 104,  193 => 98,  189 => 97,  178 => 89,  167 => 81,  160 => 76,  150 => 69,  139 => 60,  136 => 59,  133 => 58,  130 => 57,  128 => 56,  125 => 55,  123 => 54,  120 => 53,  116 => 52,  74 => 12,  70 => 11,  62 => 9,  57 => 1,  51 => 6,  47 => 5,  45 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "photo/series-from-published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/series-from-published.html.twig");
    }
}
