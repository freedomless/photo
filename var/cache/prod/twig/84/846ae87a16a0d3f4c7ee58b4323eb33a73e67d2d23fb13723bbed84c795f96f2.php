<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/series.html.twig */
class __TwigTemplate_d0713f326af401b12e72a651f61955c1ce0305ae86db7974d6e7be6208106118 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'container_class' => [$this, 'block_container_class'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("template.html.twig", "post/series.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 2), "html", null, true);
    }

    // line 4
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "
    ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 6), "html", null, true);
        echo "

    <div>
        <span class=\"small text-muted\">Share: </span>
        ";
        // line 10
        echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_series_show", ["id" => twig_get_attribute($this->env, $this->source,         // line 12
($context["post"] ?? null), "id", [], "any", false, false, false, 12)]), "classes" => "pr-2"]);
        // line 15
        echo "

        ";
        // line 17
        echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_series_show", ["id" => twig_get_attribute($this->env, $this->source,         // line 19
($context["post"] ?? null), "id", [], "any", false, false, false, 19)]), "media" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 21
($context["post"] ?? null), "image", [], "any", false, true, false, 21), "social", [], "any", true, true, false, 21) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 21), "social", [], "any", false, false, false, 21)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 21), "social", [], "any", false, false, false, 21)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, false, false, 21), "small", [], "any", false, false, false, 21))), "description" => "Photo of the Day!", "classes" => "pr-2"]);
        // line 24
        echo "

        ";
        // line 26
        echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_series_show", ["id" => twig_get_attribute($this->env, $this->source,         // line 28
($context["post"] ?? null), "id", [], "any", false, false, false, 28)])]);
        // line 30
        echo "
    </div>

    ";
        // line 33
        if (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "description", [], "any", false, false, false, 33)) {
            // line 34
            echo "        <div class=\"text-muted small mt-2\">
            ";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "description", [], "any", false, false, false, 35), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 38
        echo "
";
    }

    // line 41
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "
   <div class=\"d-flex justify-content-end\">
       ";
        // line 44
        if (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "for_sale", [], "any", false, false, false, 44)) {
            // line 45
            echo "
           ";
            // line 46
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Options", ["rendering" => "client_side", "props" => ["post" => ($context["post"] ?? null), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials()]]);
            echo "

       ";
        }
        // line 49
        echo "
       ";
        // line 50
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 50), "session", [], "any", false, false, false, 50), "get", [0 => "series-back"], "method", false, false, false, 50)) {
            // line 51
            echo "           <a class=\"ml-5\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 51), "session", [], "any", false, false, false, 51), "get", [0 => "series-back"], "method", false, false, false, 51), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 51), "session", [], "any", false, false, false, 51), "get", [0 => "series-back-arguments"], "method", false, false, false, 51)), "html", null, true);
            echo "\">
               <i class=\"fas fa-times\"></i>
           </a>
       ";
        }
        // line 55
        echo "   </div>
";
    }

    // line 58
    public function block_container_class($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "photos";
    }

    // line 60
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "
    <div class=\"col-12\">
        ";
        // line 63
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>         // line 65
($context["nude"] ?? null), "posts" =>         // line 66
($context["posts"] ?? null), "hidden" => (0 !== twig_compare(twig_length_filter($this->env,         // line 67
($context["posts"] ?? null)), 0)), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "has_more" => false, "series" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 71
($context["app"] ?? null), "request", [], "any", false, false, false, 71), "get", [0 => "id"], "method", false, false, false, 71), "auth" =>         // line 72
($context["auth"] ?? null), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"), "page" => "series"]]);
        // line 74
        echo "
    </div>

";
    }

    public function getTemplateName()
    {
        return "post/series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 74,  167 => 72,  166 => 71,  165 => 67,  164 => 66,  163 => 65,  162 => 63,  158 => 61,  154 => 60,  147 => 58,  142 => 55,  134 => 51,  132 => 50,  129 => 49,  123 => 46,  120 => 45,  118 => 44,  114 => 42,  110 => 41,  105 => 38,  99 => 35,  96 => 34,  94 => 33,  89 => 30,  87 => 28,  86 => 26,  82 => 24,  80 => 21,  79 => 19,  78 => 17,  74 => 15,  72 => 12,  71 => 10,  64 => 6,  61 => 5,  57 => 4,  50 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "post/series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/series.html.twig");
    }
}
