<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/series/posts.html.twig */
class __TwigTemplate_2e9d54b798b4515a169a7ae68a20e775afc8c0fc1e8f69894b132e0867911cfe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6 d-flex mb-5\">

        <div class=\"card\">

            <div class=\"card-body p-0\">

                <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_update", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 10)]), "html", null, true);
            echo "\">

                    ";
            // line 12
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 12), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)));
            // line 13
            echo "
                    <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 14), "html", null, true);
            echo "\" width=\"100%\" height=\"200px\"
                         style=\"object-fit: cover\"/>
                </a>

            </div>

            ";
            // line 20
            if (twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 20)) {
                // line 21
                echo "                <div class=\"card-footer p-2\">
                    <small class=\"text-muted\">Title:</small>
                    <h5 class=\"p-0\"> ";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 23), "html", null, true);
                echo "</h5>
                </div>
            ";
            }
            // line 26
            echo "
            ";
            // line 27
            if (twig_get_attribute($this->env, $this->source, $context["post"], "description", [], "any", false, false, false, 27)) {
                // line 28
                echo "
                <div class=\"card-footer\">
                    <small class=\"text-muted\">Description:</small>
                    <p class=\"text-muted\">
                        ";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "description", [], "any", false, false, false, 32), "html", null, true);
                echo "
                    </p>
                </div>

            ";
            }
            // line 37
            echo "
            <div class=\"card-footer\">

                <div class=\"text-muted\">
                    <small>Actions:</small>
                </div>

                <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_update", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 44)]), "html", null, true);
            echo "\"
                   data-toggle=\"tooltip\" data-placement=\"bottom\"
                   title=\"Edit\"
                   class=\"btn btn-default\">
                    <i class=\"fa fa-edit\"></i>
                </a>

                <button type=\"button\"
                        class=\"btn delete-warning-btn\"
                        data-toggle=\"tooltip\"
                        data-placement=\"bottom\"
                        data-title=\"Delete\"
                        data-url=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 56)]), "html", null, true);
            echo "\">
                    <i class=\"fas fa-trash-alt text-danger\"></i>
                </button>

                ";
            // line 61
            echo "                ";
            if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 61), 4)) && (null === twig_get_attribute($this->env, $this->source, $context["post"], "selectionSeries", [], "any", false, false, false, 61)))) {
                // line 62
                echo "                    <a class=\"btn btn-primary btn-sm\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_new", ["id" => twig_get_attribute($this->env, $this->source,                 // line 63
$context["post"], "id", [], "any", false, false, false, 63)]), "html", null, true);
                // line 64
                echo "\">Add to Selected</a>
                ";
            }
            // line 66
            echo "

            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "admin/curator/series/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 66,  141 => 64,  139 => 63,  137 => 62,  134 => 61,  127 => 56,  112 => 44,  103 => 37,  95 => 32,  89 => 28,  87 => 27,  84 => 26,  78 => 23,  74 => 21,  72 => 20,  61 => 14,  58 => 13,  56 => 12,  51 => 10,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/curator/series/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/series/posts.html.twig");
    }
}
