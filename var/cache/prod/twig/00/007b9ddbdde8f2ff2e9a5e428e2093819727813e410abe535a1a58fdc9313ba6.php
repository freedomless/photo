<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/copyright-notice.html.twig */
class __TwigTemplate_2810132a3f8022fa5ebd4e55c8dc3f48e383762dc96acdb166409ec14b2d1b1a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"modal fade\" id=\"copyright-notice\"
     style=\"padding-right: 0 !important;z-index: 10000\"
     tabindex=\"-100\" role=\"dialog\" aria-labelledby=\"copyrightNotice\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header bg-primary\">
                <strong class=\"modal-title\" id=\"copyrightNotice\">Copyright Notice</strong>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <strong>Photos on this website are subject to copyrights!</strong>
            </div>
            <div class=\"modal-footer text-left\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Ok</button>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_partials/copyright-notice.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_partials/copyright-notice.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/copyright-notice.html.twig");
    }
}
