<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerYvNy4c3\App_KernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerYvNy4c3/App_KernelProdContainer.php') {
    touch(__DIR__.'/ContainerYvNy4c3.legacy');

    return;
}

if (!\class_exists(App_KernelProdContainer::class, false)) {
    \class_alias(\ContainerYvNy4c3\App_KernelProdContainer::class, App_KernelProdContainer::class, false);
}

return new \ContainerYvNy4c3\App_KernelProdContainer([
    'container.build_hash' => 'YvNy4c3',
    'container.build_id' => 'c48ca20f',
    'container.build_time' => 1618940881,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerYvNy4c3');
