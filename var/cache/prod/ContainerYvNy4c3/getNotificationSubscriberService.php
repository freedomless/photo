<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getNotificationSubscriberService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\EventSubscriber\NotificationSubscriber' shared autowired service.
     *
     * @return \App\EventSubscriber\NotificationSubscriber
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/EventSubscriber/NotificationSubscriber.php';

        $container->privates['App\\EventSubscriber\\NotificationSubscriber'] = $instance = new \App\EventSubscriber\NotificationSubscriber(($container->privates['App\\Mailer\\Mailer'] ?? $container->load('getMailerService')), ($container->privates['monolog.logger'] ?? $container->getMonolog_LoggerService()), ($container->services['router'] ?? $container->getRouterService()), ($container->privates['App\\Repository\\SettingsRepository'] ?? $container->load('getSettingsRepositoryService')), ($container->privates['App\\EntityManager\\BaseManager'] ?? $container->load('getBaseManagerService')), ($container->services['app.repository.notification_type'] ?? $container->load('getApp_Repository_NotificationTypeService')));

        $instance->setDispatcher(($container->services['event_dispatcher'] ?? $container->getEventDispatcherService()));

        return $instance;
    }
}
