<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getApp_Repository_TagService extends App_KernelProdContainer
{
    /*
     * Gets the public 'app.repository.tag' shared autowired service.
     *
     * @return \App\Repository\BaseRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['app.repository.tag'] = $instance = new \App\Repository\BaseRepository(($container->services['doctrine'] ?? $container->getDoctrineService()), 'App\\Entity\\Photo\\Tag');

        $a = ($container->services['serializer'] ?? $container->getSerializerService());

        $instance->setDispatcher(($container->services['event_dispatcher'] ?? $container->getEventDispatcherService()));
        $instance->setPaginator(($container->services['knp_paginator'] ?? $container->getKnpPaginatorService()));
        $instance->setSerializer($a);
        $instance->setNormalizer($a);
        $instance->setDenormalizer($a);
        $instance->setServiceContainer($container);
        $instance->setCache(($container->privates['cache.app.taggable'] ?? $container->getCache_App_TaggableService()));

        return $instance;
    }
}
