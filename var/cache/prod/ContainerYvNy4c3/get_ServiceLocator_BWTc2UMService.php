<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_BWTc2UMService extends App_KernelProdContainer
{
    /*
     * Gets the private '.service_locator.bWTc2UM' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.bWTc2UM'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'manager' => ['privates', 'App\\EntityManager\\AwardManager', 'getAwardManagerService', true],
            'photo' => ['privates', '.errored..service_locator.bWTc2UM.App\\Entity\\Photo\\Post', NULL, 'Cannot autowire service ".service_locator.bWTc2UM": it references class "App\\Entity\\Photo\\Post" but no such service exists.'],
        ], [
            'manager' => 'App\\EntityManager\\AwardManager',
            'photo' => 'App\\Entity\\Photo\\Post',
        ]);
    }
}
