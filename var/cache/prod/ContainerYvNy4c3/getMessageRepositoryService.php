<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getMessageRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\MessageRepository' shared autowired service.
     *
     * @return \App\Repository\MessageRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Repository/MessageRepository.php';

        $container->privates['App\\Repository\\MessageRepository'] = $instance = new \App\Repository\MessageRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));

        $a = ($container->services['serializer'] ?? $container->getSerializerService());

        $instance->setDispatcher(($container->services['event_dispatcher'] ?? $container->getEventDispatcherService()));
        $instance->setPaginator(($container->services['knp_paginator'] ?? $container->getKnpPaginatorService()));
        $instance->setSerializer($a);
        $instance->setNormalizer($a);
        $instance->setDenormalizer($a);
        $instance->setServiceContainer($container);
        $instance->setCache(($container->privates['cache.app.taggable'] ?? $container->getCache_App_TaggableService()));

        return $instance;
    }
}
