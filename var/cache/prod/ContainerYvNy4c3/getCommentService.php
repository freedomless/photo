<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCommentService extends App_KernelProdContainer
{
    /*
     * Gets the private '.errored..service_locator.cL9JKZr.App\Entity\Photo\Comment' shared service.
     *
     * @return \App\Entity\Photo\Comment
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.cL9JKZr": it references class "App\\Entity\\Photo\\Comment" but no such service exists.');
    }
}
