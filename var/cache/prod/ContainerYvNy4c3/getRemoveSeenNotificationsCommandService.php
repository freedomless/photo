<?php

namespace ContainerYvNy4c3;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getRemoveSeenNotificationsCommandService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Command\RemoveSeenNotificationsCommand' shared autowired service.
     *
     * @return \App\Command\RemoveSeenNotificationsCommand
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/console/Command/Command.php';
        include_once \dirname(__DIR__, 4).'/src/Command/RemoveSeenNotificationsCommand.php';

        $container->privates['App\\Command\\RemoveSeenNotificationsCommand'] = $instance = new \App\Command\RemoveSeenNotificationsCommand(($container->services['doctrine.orm.default_entity_manager'] ?? $container->getDoctrine_Orm_DefaultEntityManagerService()));

        $instance->setName('app:notifications:remove');

        return $instance;
    }
}
