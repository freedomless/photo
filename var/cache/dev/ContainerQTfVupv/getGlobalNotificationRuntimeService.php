<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getGlobalNotificationRuntimeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Twig\GlobalNotificationRuntime' shared autowired service.
     *
     * @return \App\Twig\GlobalNotificationRuntime
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/twig/twig/src/Extension/RuntimeExtensionInterface.php';
        include_once \dirname(__DIR__, 4).'/src/Twig/GlobalNotificationRuntime.php';

        return $container->privates['App\\Twig\\GlobalNotificationRuntime'] = new \App\Twig\GlobalNotificationRuntime(($container->privates['security.helper'] ?? $container->load('getSecurity_HelperService')), ($container->services['session'] ?? $container->getSessionService()));
    }
}
