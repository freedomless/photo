<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_AlMXo6NService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.AlMXo6N' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.AlMXo6N'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'curatorCommentManager' => ['privates', 'App\\EntityManager\\BaseManager', 'getBaseManagerService', true],
            'post' => ['privates', '.errored..service_locator.AlMXo6N.App\\Entity\\Photo\\Post', NULL, 'Cannot autowire service ".service_locator.AlMXo6N": it references class "App\\Entity\\Photo\\Post" but no such service exists.'],
        ], [
            'curatorCommentManager' => 'App\\EntityManager\\BaseManager',
            'post' => 'App\\Entity\\Photo\\Post',
        ]);
    }
}
