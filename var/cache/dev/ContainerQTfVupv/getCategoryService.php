<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCategoryService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.c.vjTWN.App\Entity\Photo\Category' shared service.
     *
     * @return \App\Entity\Photo\Category
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.c.vjTWN": it references class "App\\Entity\\Photo\\Category" but no such service exists.');
    }
}
