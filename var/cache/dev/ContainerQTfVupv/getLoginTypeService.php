<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getLoginTypeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Form\Type\LoginType' shared autowired service.
     *
     * @return \App\Form\Type\LoginType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/Type/LoginType.php';

        return $container->privates['App\\Form\\Type\\LoginType'] = new \App\Form\Type\LoginType();
    }
}
