<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getConsole_Command_TranslationUpdateService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'console.command.translation_update' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Command\TranslationUpdateCommand
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/console/Command/Command.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Command/TranslationUpdateCommand.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Writer/TranslationWriterInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Writer/TranslationWriter.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/DumperInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/FileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/PhpFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/XliffFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/PoFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/MoFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/YamlFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/QtFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/CsvFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/IniFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/JsonFileDumper.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Dumper/IcuResFileDumper.php';

        $a = new \Symfony\Component\Translation\Writer\TranslationWriter();
        $a->addDumper('php', new \Symfony\Component\Translation\Dumper\PhpFileDumper());
        $a->addDumper('xlf', new \Symfony\Component\Translation\Dumper\XliffFileDumper());
        $a->addDumper('po', new \Symfony\Component\Translation\Dumper\PoFileDumper());
        $a->addDumper('mo', new \Symfony\Component\Translation\Dumper\MoFileDumper());
        $a->addDumper('yml', new \Symfony\Component\Translation\Dumper\YamlFileDumper());
        $a->addDumper('yaml', new \Symfony\Component\Translation\Dumper\YamlFileDumper('yaml'));
        $a->addDumper('ts', new \Symfony\Component\Translation\Dumper\QtFileDumper());
        $a->addDumper('csv', new \Symfony\Component\Translation\Dumper\CsvFileDumper());
        $a->addDumper('ini', new \Symfony\Component\Translation\Dumper\IniFileDumper());
        $a->addDumper('json', new \Symfony\Component\Translation\Dumper\JsonFileDumper());
        $a->addDumper('res', new \Symfony\Component\Translation\Dumper\IcuResFileDumper());

        $container->privates['console.command.translation_update'] = $instance = new \Symfony\Bundle\FrameworkBundle\Command\TranslationUpdateCommand($a, ($container->privates['translation.reader'] ?? $container->load('getTranslation_ReaderService')), ($container->privates['translation.extractor'] ?? $container->load('getTranslation_ExtractorService')), 'en', (\dirname(__DIR__, 4).'/translations'), (\dirname(__DIR__, 4).'/templates'), [0 => (\dirname(__DIR__, 4).'/vendor/symfony/validator/Resources/translations'), 1 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Resources/translations'), 2 => (\dirname(__DIR__, 4).'/vendor/symfony/security-core/Resources/translations'), 3 => (\dirname(__DIR__, 4).'/vendor/rollerworks/password-strength-validator/src/Resources/translations')], [0 => (\dirname(__DIR__, 4).'/vendor/symfony/twig-bridge/Resources/views/Email'), 1 => (\dirname(__DIR__, 4).'/vendor/symfony/twig-bridge/Resources/views/Form'), 2 => (\dirname(__DIR__, 4).'/src/Builder/Seo/IndexSeoBuilder.php'), 3 => (\dirname(__DIR__, 4).'/src/Builder/Seo/PostSeoBuilder.php'), 4 => (\dirname(__DIR__, 4).'/src/Builder/Seo/SeriesSeoBuilder.php'), 5 => (\dirname(__DIR__, 4).'/src/Builder/Seo/UserSeoBuilder.php'), 6 => (\dirname(__DIR__, 4).'/src/Builder/Seo/UserStoreSeoBuilder.php'), 7 => (\dirname(__DIR__, 4).'/src/Controller/Admin/AwardController.php'), 8 => (\dirname(__DIR__, 4).'/src/Controller/Admin/CuratorPanelController.php'), 9 => (\dirname(__DIR__, 4).'/src/Controller/Admin/CuratorSeriesController.php'), 10 => (\dirname(__DIR__, 4).'/src/Controller/Admin/PollController.php'), 11 => (\dirname(__DIR__, 4).'/src/Controller/Admin/SelectionSeriesController.php'), 12 => (\dirname(__DIR__, 4).'/src/Controller/Web/AccountController.php'), 13 => (\dirname(__DIR__, 4).'/src/Controller/Web/AlbumController.php'), 14 => (\dirname(__DIR__, 4).'/src/Controller/Web/CartController.php'), 15 => (\dirname(__DIR__, 4).'/src/Controller/Web/ContactController.php'), 16 => (\dirname(__DIR__, 4).'/src/Controller/Web/ForgottenPasswordController.php'), 17 => (\dirname(__DIR__, 4).'/src/Controller/Web/GalleryController.php'), 18 => (\dirname(__DIR__, 4).'/src/Controller/Web/MemberController.php'), 19 => (\dirname(__DIR__, 4).'/src/Controller/Web/OpinionController.php'), 20 => (\dirname(__DIR__, 4).'/src/Controller/Web/PostController.php'), 21 => (\dirname(__DIR__, 4).'/src/Controller/Web/ProductController.php'), 22 => (\dirname(__DIR__, 4).'/src/Controller/Web/RegisterController.php'), 23 => (\dirname(__DIR__, 4).'/src/Controller/Web/SearchController.php'), 24 => (\dirname(__DIR__, 4).'/src/Controller/Web/StoreController.php'), 25 => (\dirname(__DIR__, 4).'/src/Controller/Web/TagController.php'), 26 => (\dirname(__DIR__, 4).'/src/Controller/Web/TopicController.php'), 27 => (\dirname(__DIR__, 4).'/src/EventSubscriber/ApiKernelExceptionSubscriber.php'), 28 => (\dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Command/TranslationDebugCommand.php'), 29 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Extension/Core/Type/FileType.php'), 30 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Extension/Core/Type/ColorType.php'), 31 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Extension/Core/Type/TransformationFailureExtension.php'), 32 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Extension/Validator/Type/UploadValidatorExtension.php'), 33 => (\dirname(__DIR__, 4).'/vendor/symfony/form/Extension/Csrf/Type/FormTypeCsrfExtension.php'), 34 => (\dirname(__DIR__, 4).'/vendor/symfony/validator/ValidatorBuilder.php'), 35 => (\dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/CacheWarmer/TranslationsCacheWarmer.php'), 36 => (\dirname(__DIR__, 4).'/vendor/symfony/translation/DataCollector/TranslationDataCollector.php'), 37 => (\dirname(__DIR__, 4).'/vendor/symfony/twig-bridge/Extension/TranslationExtension.php'), 38 => (\dirname(__DIR__, 4).'/vendor/knplabs/knp-paginator-bundle/src/Helper/Processor.php'), 39 => (\dirname(__DIR__, 4).'/vendor/easycorp/easyadmin-bundle/src/Twig/EasyAdminTwigExtension.php'), 40 => (\dirname(__DIR__, 4).'/vendor/rollerworks/password-strength-validator/src/Validator/Constraints/PasswordStrengthValidator.php')]);

        $instance->setName('translation:update');

        return $instance;
    }
}
