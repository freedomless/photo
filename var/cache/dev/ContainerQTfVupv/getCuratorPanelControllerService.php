<?php

namespace ContainerQTfVupv;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCuratorPanelControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\Admin\CuratorPanelController' shared autowired service.
     *
     * @return \App\Controller\Admin\CuratorPanelController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Helper/TranslationTrait.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Web/BaseController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Admin/BaseController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Admin/CuratorPanelController.php';

        $container->services['App\\Controller\\Admin\\CuratorPanelController'] = $instance = new \App\Controller\Admin\CuratorPanelController(($container->privates['App\\EntityManager\\PostManager'] ?? $container->getPostManagerService()), ($container->privates['app.manager.vote'] ?? $container->load('getApp_Manager_VoteService')), ($container->services['session'] ?? $container->getSessionService()), ($container->privates['monolog.logger'] ?? $container->getMonolog_LoggerService()), ($container->privates['App\\Repository\\CategoryRepository'] ?? $container->load('getCategoryRepositoryService')), ($container->privates['App\\Repository\\ConfigurationRepository'] ?? $container->getConfigurationRepositoryService()));

        $a = ($container->services['serializer'] ?? $container->getSerializerService());

        $instance->setContainer(($container->privates['.service_locator.2Ca9hUK'] ?? $container->load('get_ServiceLocator_2Ca9hUKService'))->withContext('App\\Controller\\Admin\\CuratorPanelController', $container));
        $instance->setTranslator(($container->services['translator'] ?? $container->getTranslatorService()));
        $instance->setSerializer($a);
        $instance->setNormalizer($a);
        $instance->setDenormalizer($a);
        $instance->setServiceContainer($container);
        $instance->setPaginator(($container->services['knp_paginator'] ?? $container->getKnpPaginatorService()));
        $instance->setDispatcher(($container->services['event_dispatcher'] ?? $container->getEventDispatcherService()));
        $instance->setCache(($container->privates['cache.app.taggable'] ?? $container->getCache_App_TaggableService()));

        return $instance;
    }
}
