<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/orders.html.twig */
class __TwigTemplate_cfca8cdda5f34bbcf2fda542ea4d4afc6bcbd507da222955acbe693641919c40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/orders.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/orders.html.twig"));

        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/orders.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"container basic-layout\">

        <h3>Orders</h3>

        <div class=\"table-responsive\">
            <table class=\"table shadow rounded \">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Size</th>
                        <th>Media</th>
                        <th>
                            Order Id
                        </th>
                        <th>
                            Transaction Status
                        </th>
                        <th>
                            Track ID
                        </th>
                        <th>Dispatched</th>
                        <th>Invoice</th>
                        <th>
                            Total Amount
                        </th>
                    </tr>
                </thead>
                <tbody>
                    ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 32, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 33
            echo "                        <tr>
                            <td>
                                ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 35));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 36
                echo "                                    ";
                if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 36), "post", [], "any", false, false, false, 36), "type", [], "any", false, false, false, 36), 1))) {
                    // line 37
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 37), "post", [], "any", false, false, false, 37), "children", [], "any", false, false, false, 37));
                    foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                        // line 38
                        echo "                                            <img src=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 38), "thumbnail", [], "any", false, false, false, 38), "html", null, true);
                        echo "\" alt=\"Item\" class=\"rounded mb-1\"
                                                 width=\"100px\" height=\"100px\" style=\"object-fit: cover\">
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 41
                    echo "
                                    ";
                } else {
                    // line 43
                    echo "                                        <img src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 43), "post", [], "any", false, false, false, 43), "image", [], "any", false, false, false, 43), "thumbnail", [], "any", false, false, false, 43), "html", null, true);
                    echo "\" alt=\"Item\" class=\"rounded\"
                                             width=\"100px\" height=\"100px\" style=\"object-fit: cover\"
                                    ";
                }
                // line 46
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                            </td>
                            <td>
                                ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 49));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 50
                echo "
                                    ";
                // line 51
                if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 51), "post", [], "any", false, false, false, 51), "type", [], "any", false, false, false, 51), 1))) {
                    // line 52
                    echo "                                      <div>
                                          ";
                    // line 53
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 53), "width", [], "any", false, false, false, 53), "html", null, true);
                    echo "x";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 53), "height", [], "any", false, false, false, 53), "html", null, true);
                    echo "
                                      </div>
                                    ";
                } else {
                    // line 56
                    echo "                                        <div>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 56), "width", [], "any", false, false, false, 56), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, twig_round((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 56), "post", [], "any", false, false, false, 56), "opposite", [], "any", false, false, false, 56) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 56), "width", [], "any", false, false, false, 56))), "html", null, true);
                    echo "</div>
                                    ";
                }
                // line 58
                echo "
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                            </td>
                            <td>
                                ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["order"], "items", [], "any", false, false, false, 62));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 63
                echo "
                                   <div>";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "material", [], "any", false, false, false, 64), "name", [], "any", false, false, false, 64), "html", null, true);
                echo "</div>

                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "                            </td>
                            <td>";
            // line 68
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["order"], "token", [], "any", false, false, false, 68), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 70
            $context["status"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "transaction", [], "any", false, false, false, 70), "status", [], "any", false, false, false, 70);
            // line 71
            echo "                                ";
            if ((0 === twig_compare((isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 71, $this->source); })()), 1))) {
                // line 72
                echo "                                    <span class=\"text-success\"> Paid</span>
                                ";
            } elseif ((0 === twig_compare(            // line 73
(isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 73, $this->source); })()), 0))) {
                // line 74
                echo "                                    Pending
                                ";
            } elseif ((0 === twig_compare(            // line 75
(isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 75, $this->source); })()),  -1))) {
                // line 76
                echo "                                    Failed
                                ";
            } elseif ((0 === twig_compare(            // line 77
(isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 77, $this->source); })()),  -2))) {
                // line 78
                echo "                                    Canceled
                                ";
            }
            // line 80
            echo "                            </td>
                            <td>";
            // line 81
            ((twig_get_attribute($this->env, $this->source, $context["order"], "trackId", [], "any", false, false, false, 81)) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["order"], "trackId", [], "any", false, false, false, 81), "html", null, true))) : (print ("Not added")));
            echo "</td>
                            <td>";
            // line 82
            echo ((twig_get_attribute($this->env, $this->source, $context["order"], "dispatched", [], "any", false, false, false, 82)) ? ("Yes") : ("No"));
            echo "</td>
                            <td>
                                ";
            // line 84
            if ((0 === twig_compare((isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 84, $this->source); })()), 1))) {
                // line 85
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_invoice", ["token" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "invoice", [], "any", false, false, false, 85), "token", [], "any", false, false, false, 85)]), "html", null, true);
                echo "\" target=\"_blank\">View</a>
                                ";
            }
            // line 87
            echo "                            </td>
                            <td>
                                ";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["order"], "transaction", [], "any", false, false, false, 89), "paymentGross", [], "any", false, false, false, 89), "html", null, true);
            echo "
                                &euro;
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "
                    ";
        // line 95
        if ((0 === twig_compare(twig_length_filter($this->env, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 95, $this->source); })())), 0))) {
            // line 96
            echo "                        <h1 class=\"mt-5 pt-5\">
                            No orders available
                        </h1>
                    ";
        }
        // line 100
        echo "                </tbody>
            </table>
        </div>

        <div class=\"d-flex justify-content-center\">
            ";
        // line 105
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 105, $this->source); })()));
        echo "
        </div>

    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/profile/orders.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 105,  282 => 100,  276 => 96,  274 => 95,  271 => 94,  260 => 89,  256 => 87,  250 => 85,  248 => 84,  243 => 82,  239 => 81,  236 => 80,  232 => 78,  230 => 77,  227 => 76,  225 => 75,  222 => 74,  220 => 73,  217 => 72,  214 => 71,  212 => 70,  207 => 68,  204 => 67,  195 => 64,  192 => 63,  188 => 62,  184 => 60,  177 => 58,  169 => 56,  161 => 53,  158 => 52,  156 => 51,  153 => 50,  149 => 49,  145 => 47,  139 => 46,  132 => 43,  128 => 41,  118 => 38,  113 => 37,  110 => 36,  106 => 35,  102 => 33,  98 => 32,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'user/profile/index.html.twig' %}

{% block body %}
    <div class=\"container basic-layout\">

        <h3>Orders</h3>

        <div class=\"table-responsive\">
            <table class=\"table shadow rounded \">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Size</th>
                        <th>Media</th>
                        <th>
                            Order Id
                        </th>
                        <th>
                            Transaction Status
                        </th>
                        <th>
                            Track ID
                        </th>
                        <th>Dispatched</th>
                        <th>Invoice</th>
                        <th>
                            Total Amount
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {% for order in orders %}
                        <tr>
                            <td>
                                {% for item in order.items %}
                                    {% if item.product.post.type > 1 %}
                                        {% for post in item.product.post.children %}
                                            <img src=\"{{ post.image.thumbnail }}\" alt=\"Item\" class=\"rounded mb-1\"
                                                 width=\"100px\" height=\"100px\" style=\"object-fit: cover\">
                                        {% endfor %}

                                    {% else %}
                                        <img src=\"{{ item.product.post.image.thumbnail }}\" alt=\"Item\" class=\"rounded\"
                                             width=\"100px\" height=\"100px\" style=\"object-fit: cover\"
                                    {% endif %}
                                {% endfor %}
                            </td>
                            <td>
                                {% for item in order.items %}

                                    {% if item.product.post.type > 1 %}
                                      <div>
                                          {{ item.size.width }}x{{ item.size.height }}
                                      </div>
                                    {% else %}
                                        <div>{{ item.size.width }} x {{ (item.product.post.opposite * item.size.width) | round }}</div>
                                    {% endif %}

                                {% endfor %}
                            </td>
                            <td>
                                {% for item in order.items %}

                                   <div>{{ item.material.name }}</div>

                                {% endfor %}
                            </td>
                            <td>{{ order.token }}</td>
                            <td>
                                {% set status = order.transaction.status %}
                                {% if  status == 1 %}
                                    <span class=\"text-success\"> Paid</span>
                                {% elseif status == 0 %}
                                    Pending
                                {% elseif status == -1 %}
                                    Failed
                                {% elseif status == -2 %}
                                    Canceled
                                {% endif %}
                            </td>
                            <td>{{ order.trackId  ? : 'Not added' }}</td>
                            <td>{{ order.dispatched ? 'Yes' : 'No' }}</td>
                            <td>
                                {% if status == 1 %}
                                    <a href=\"{{ path('web_invoice',{token: order.invoice.token}) }}\" target=\"_blank\">View</a>
                                {% endif %}
                            </td>
                            <td>
                                {{ order.transaction.paymentGross }}
                                &euro;
                            </td>
                        </tr>
                    {% endfor %}

                    {% if orders | length == 0 %}
                        <h1 class=\"mt-5 pt-5\">
                            No orders available
                        </h1>
                    {% endif %}
                </tbody>
            </table>
        </div>

        <div class=\"d-flex justify-content-center\">
            {{ knp_pagination_render(orders) }}
        </div>

    </div>
{% endblock %}
", "user/profile/orders.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/orders.html.twig");
    }
}
