<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @EasyAdmin/default/paginator.html.twig */
class __TwigTemplate_c68dbdd99c0995db5274ca02a2cfa9f2bdd9423348258bc097dfe805c9bec249 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@EasyAdmin/default/paginator.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@EasyAdmin/default/paginator.html.twig"));

        // line 3
        echo "
";
        // line 4
        $context["_paginator_request_parameters"] = twig_array_merge((isset($context["_request_parameters"]) || array_key_exists("_request_parameters", $context) ? $context["_request_parameters"] : (function () { throw new RuntimeError('Variable "_request_parameters" does not exist.', 4, $this->source); })()), ["referer" => null]);
        // line 5
        echo "
";
        // line 6
        if (twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 6, $this->source); })()), "haveToPaginate", [], "any", false, false, false, 6)) {
            // line 7
            echo "    <div class=\"list-pagination\">
        <div class=\"list-pagination-counter\">
            ";
            // line 9
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.counter", ["%start%" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 9, $this->source); })()), "currentPageOffsetStart", [], "any", false, false, false, 9), "%end%" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 9, $this->source); })()), "currentPageOffsetEnd", [], "any", false, false, false, 9), "%results%" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 9, $this->source); })()), "nbResults", [], "any", false, false, false, 9)], "EasyAdminBundle");
            echo " ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("easy_admin.results", [], "EasyAdminBundle"), "html", null, true);
            echo "
        </div>

        <nav class=\"pager list-pagination-paginator ";
            // line 12
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 12, $this->source); })()), "hasPreviousPage", [], "any", false, false, false, 12)) ? ("first-page") : (""));
            echo " ";
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 12, $this->source); })()), "hasNextPage", [], "any", false, false, false, 12)) ? ("last-page") : (""));
            echo "\">
            <ul class=\"pagination list-pagination-paginator ";
            // line 13
            echo (((0 === twig_compare(1, twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 13, $this->source); })()), "currentPage", [], "any", false, false, false, 13)))) ? ("first-page") : (""));
            echo " ";
            echo ((twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 13, $this->source); })()), "hasNextPage", [], "any", false, false, false, 13)) ? ("") : ("last-page"));
            echo "\">
                <li class=\"page-item ";
            // line 14
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 14, $this->source); })()), "hasPreviousPage", [], "any", false, false, false, 14)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 16
            (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 16, $this->source); })()), "hasPreviousPage", [], "any", false, false, false, 16)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 16, $this->source); })()), ["page" => 1])), "html", null, true))));
            echo "\">
                        <i class=\"fa fa-angle-double-left mx-1\"></i><span class=\"btn-label\">";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.first", [], "EasyAdminBundle"), "html", null, true);
            echo "</span>
                    </a>
                </li>

                <li class=\"page-item ";
            // line 21
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 21, $this->source); })()), "hasPreviousPage", [], "any", false, false, false, 21)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 23
            (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 23, $this->source); })()), "hasPreviousPage", [], "any", false, false, false, 23)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 23, $this->source); })()), ["page" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 23, $this->source); })()), "previousPage", [], "any", false, false, false, 23)])), "html", null, true))));
            echo "\">
                        <i class=\"fa fa-angle-left mx-1\"></i> <span class=\"btn-label\">";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.previous", [], "EasyAdminBundle"), "html", null, true);
            echo "</span>
                    </a>
                </li>

                ";
            // line 29
            echo "                ";
            // line 30
            echo "                ";
            $context["nearbyPagesLimit"] = 3;
            // line 31
            echo "
                ";
            // line 32
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 32, $this->source); })()), "currentPage", [], "any", false, false, false, 32), 1))) {
                // line 33
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range((twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 33, $this->source); })()), "currentPage", [], "any", false, false, false, 33) - (isset($context["nearbyPagesLimit"]) || array_key_exists("nearbyPagesLimit", $context) ? $context["nearbyPagesLimit"] : (function () { throw new RuntimeError('Variable "nearbyPagesLimit" does not exist.', 33, $this->source); })())), (twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 33, $this->source); })()), "currentPage", [], "any", false, false, false, 33) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 34
                    echo "                        ";
                    if ((1 === twig_compare($context["i"], 0))) {
                        // line 35
                        echo "                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"";
                        // line 37
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 37, $this->source); })()), ["page" => $context["i"]])), "html", null, true);
                        echo "\">
                                    <span class=\"btn-label\">";
                        // line 38
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "</span>
                                </a>
                            </li>
                        ";
                    }
                    // line 42
                    echo "                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "                ";
            }
            // line 44
            echo "                <li class=\"page-item\">
                    <a class=\"page-link current\" style=\"color: red\">";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 45, $this->source); })()), "currentPage", [], "any", false, false, false, 45), "html", null, true);
            echo "</a>
                </li>
                ";
            // line 47
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 47, $this->source); })()), "currentPage", [], "any", false, false, false, 47), twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 47, $this->source); })()), "nbPages", [], "any", false, false, false, 47)))) {
                // line 48
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range((twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 48, $this->source); })()), "currentPage", [], "any", false, false, false, 48) + 1), (twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 48, $this->source); })()), "currentPage", [], "any", false, false, false, 48) + (isset($context["nearbyPagesLimit"]) || array_key_exists("nearbyPagesLimit", $context) ? $context["nearbyPagesLimit"] : (function () { throw new RuntimeError('Variable "nearbyPagesLimit" does not exist.', 48, $this->source); })()))));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 49
                    echo "                        ";
                    if ((0 >= twig_compare($context["i"], twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 49, $this->source); })()), "nbPages", [], "any", false, false, false, 49)))) {
                        // line 50
                        echo "                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"";
                        // line 52
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 52, $this->source); })()), ["page" => $context["i"]])), "html", null, true);
                        echo "\">
                                    <span class=\"btn-label\">";
                        // line 53
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "</span>
                                </a>
                            </li>
                        ";
                    }
                    // line 57
                    echo "                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                ";
            }
            // line 59
            echo "
                ";
            // line 61
            echo "
                <li class=\"page-item ";
            // line 62
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 62, $this->source); })()), "hasNextPage", [], "any", false, false, false, 62)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 64
            (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 64, $this->source); })()), "hasNextPage", [], "any", false, false, false, 64)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 64, $this->source); })()), ["page" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 64, $this->source); })()), "nextPage", [], "any", false, false, false, 64)])), "html", null, true))));
            echo "\">
                        <span class=\"btn-label\">";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.next", [], "EasyAdminBundle"), "html", null, true);
            echo "</span> <i class=\"fa fa-angle-right mx-1\"></i>
                    </a>
                </li>

                <li class=\"page-item ";
            // line 69
            echo (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 69, $this->source); })()), "hasNextPage", [], "any", false, false, false, 69)) ? ("disabled") : (""));
            echo "\">
                    <a class=\"page-link\"
                       href=\"";
            // line 71
            (( !twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 71, $this->source); })()), "hasNextPage", [], "any", false, false, false, 71)) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin", twig_array_merge((isset($context["_paginator_request_parameters"]) || array_key_exists("_paginator_request_parameters", $context) ? $context["_paginator_request_parameters"] : (function () { throw new RuntimeError('Variable "_paginator_request_parameters" does not exist.', 71, $this->source); })()), ["page" => twig_get_attribute($this->env, $this->source, (isset($context["paginator"]) || array_key_exists("paginator", $context) ? $context["paginator"] : (function () { throw new RuntimeError('Variable "paginator" does not exist.', 71, $this->source); })()), "nbPages", [], "any", false, false, false, 71)])), "html", null, true))));
            echo "\">
                        <span class=\"btn-label\">";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("paginator.last", [], "EasyAdminBundle"), "html", null, true);
            echo "</span> <i class=\"fa fa-angle-double-right\"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 72,  220 => 71,  215 => 69,  208 => 65,  204 => 64,  199 => 62,  196 => 61,  193 => 59,  190 => 58,  184 => 57,  177 => 53,  173 => 52,  169 => 50,  166 => 49,  161 => 48,  159 => 47,  154 => 45,  151 => 44,  148 => 43,  142 => 42,  135 => 38,  131 => 37,  127 => 35,  124 => 34,  119 => 33,  117 => 32,  114 => 31,  111 => 30,  109 => 29,  102 => 24,  98 => 23,  93 => 21,  86 => 17,  82 => 16,  77 => 14,  71 => 13,  65 => 12,  57 => 9,  53 => 7,  51 => 6,  48 => 5,  46 => 4,  43 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("{# tested with 2.3.5 #}
{% trans_default_domain 'EasyAdminBundle' %}

{% set _paginator_request_parameters = _request_parameters|merge({'referer': null}) %}

{% if paginator.haveToPaginate %}
    <div class=\"list-pagination\">
        <div class=\"list-pagination-counter\">
            {{ 'paginator.counter'|trans({ '%start%': paginator.currentPageOffsetStart, '%end%': paginator.currentPageOffsetEnd, '%results%': paginator.nbResults})|raw }} {{ 'easy_admin.results'|trans }}
        </div>

        <nav class=\"pager list-pagination-paginator {{ not paginator.hasPreviousPage ? 'first-page' }} {{ not paginator.hasNextPage ? 'last-page' }}\">
            <ul class=\"pagination list-pagination-paginator {{ 1 == paginator.currentPage ? 'first-page' : '' }} {{ paginator.hasNextPage ? '' : 'last-page' }}\">
                <li class=\"page-item {{ not paginator.hasPreviousPage ? 'disabled' }}\">
                    <a class=\"page-link\"
                       href=\"{{ not paginator.hasPreviousPage ? '#' : path('easyadmin', _paginator_request_parameters|merge({ page: 1 }) ) }}\">
                        <i class=\"fa fa-angle-double-left mx-1\"></i><span class=\"btn-label\">{{ 'paginator.first'|trans }}</span>
                    </a>
                </li>

                <li class=\"page-item {{ not paginator.hasPreviousPage ? 'disabled' }}\">
                    <a class=\"page-link\"
                       href=\"{{ not paginator.hasPreviousPage ? '#' : path('easyadmin', _paginator_request_parameters|merge({ page: paginator.previousPage }) ) }}\">
                        <i class=\"fa fa-angle-left mx-1\"></i> <span class=\"btn-label\">{{ 'paginator.previous'|trans }}</span>
                    </a>
                </li>

                {# BEGIN DISPLAYING PAGE NUMBERS #}
                {# the number of pages that are displayed around the active page #}
                {% set nearbyPagesLimit = 3 %}

                {% if paginator.currentPage > 1 %}
                    {% for i in range(paginator.currentPage-nearbyPagesLimit, paginator.currentPage-1) %}
                        {% if ( i > 0 ) %}
                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"{{ path('easyadmin', _paginator_request_parameters|merge({ page: i }) ) }}\">
                                    <span class=\"btn-label\">{{ i }}</span>
                                </a>
                            </li>
                        {% endif %}
                    {% endfor %}
                {% endif %}
                <li class=\"page-item\">
                    <a class=\"page-link current\" style=\"color: red\">{{ paginator.currentPage }}</a>
                </li>
                {% if paginator.currentPage < paginator.nbPages %}
                    {% for i in range(paginator.currentPage+1, paginator.currentPage + nearbyPagesLimit) %}
                        {% if ( i <= paginator.nbPages ) %}
                            <li class=\"page-item\">
                                <a class=\"page-link\"
                                   href=\"{{ path('easyadmin', _paginator_request_parameters|merge({ page: i }) ) }}\">
                                    <span class=\"btn-label\">{{ i }}</span>
                                </a>
                            </li>
                        {% endif %}
                    {% endfor %}
                {% endif %}

                {# END DISPLAYING PAGE NUMBERS #}

                <li class=\"page-item {{ not paginator.hasNextPage ? 'disabled' }}\">
                    <a class=\"page-link\"
                       href=\"{{ not paginator.hasNextPage ? '#' : path('easyadmin', _paginator_request_parameters|merge({ page: paginator.nextPage }) ) }}\">
                        <span class=\"btn-label\">{{ 'paginator.next'|trans }}</span> <i class=\"fa fa-angle-right mx-1\"></i>
                    </a>
                </li>

                <li class=\"page-item {{ not paginator.hasNextPage ? 'disabled' }}\">
                    <a class=\"page-link\"
                       href=\"{{ not paginator.hasNextPage ? '#' : path('easyadmin', _paginator_request_parameters|merge({ page: paginator.nbPages }) ) }}\">
                        <span class=\"btn-label\">{{ 'paginator.last'|trans }}</span> <i class=\"fa fa-angle-double-right\"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
{% endif %}", "@EasyAdmin/default/paginator.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/bundles/EasyAdminBundle/default/paginator.html.twig");
    }
}
