<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/copyright-notice.html.twig */
class __TwigTemplate_675219a7ee2eac5a85558d360e451e3cab16c7a949b7f39c63ed9c96415285f3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/copyright-notice.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/copyright-notice.html.twig"));

        // line 1
        echo "<div class=\"modal fade\" id=\"copyright-notice\"
     style=\"padding-right: 0 !important;z-index: 10000\"
     tabindex=\"-100\" role=\"dialog\" aria-labelledby=\"copyrightNotice\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header bg-primary\">
                <strong class=\"modal-title\" id=\"copyrightNotice\">Copyright Notice</strong>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <strong>Photos on this website are subject to copyrights!</strong>
            </div>
            <div class=\"modal-footer text-left\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Ok</button>
            </div>
        </div>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/copyright-notice.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"modal fade\" id=\"copyright-notice\"
     style=\"padding-right: 0 !important;z-index: 10000\"
     tabindex=\"-100\" role=\"dialog\" aria-labelledby=\"copyrightNotice\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header bg-primary\">
                <strong class=\"modal-title\" id=\"copyrightNotice\">Copyright Notice</strong>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                <strong>Photos on this website are subject to copyrights!</strong>
            </div>
            <div class=\"modal-footer text-left\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Ok</button>
            </div>
        </div>
    </div>
</div>", "_partials/copyright-notice.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/copyright-notice.html.twig");
    }
}
