<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/list.html.twig */
class __TwigTemplate_17a4f5e853c3730482f47a77b02be946158510b717b83c5e14e7d20ebb057529 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/selection/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/selection/list.html.twig"));

        $this->parent = $this->loadTemplate("template.html.twig", "admin/selection/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Manage Selection Series";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        echo "Manage Selection Series";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        // line 10
        echo "
    <div class=\"col-12\">

        <div class=\"row\">
            ";
        // line 14
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["series"]) || array_key_exists("series", $context) ? $context["series"] : (function () { throw new RuntimeError('Variable "series" does not exist.', 14, $this->source); })())), 0))) {
            // line 15
            echo "
                ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["series"]) || array_key_exists("series", $context) ? $context["series"] : (function () { throw new RuntimeError('Variable "series" does not exist.', 16, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 17
                echo "
                    <div data-id=\"";
                // line 18
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 18), "html", null, true);
                echo "\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">
                        <div class=\"card mb-4 text-center\">

                            ";
                // line 21
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 21), 1))) {
                    // line 22
                    echo "                                <div class=\"alert-success position-absolute\" style=\"top: 0; width: 100%\">
                                    Shown on Front page
                                </div>
                            ";
                }
                // line 26
                echo "
                            <img src=\"";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "cover", [], "any", false, false, false, 27), "html", null, true);
                echo "\" class=\"card-img-top\" alt=\"...\"
                                 style=\"object-fit: cover; width: 100%; height: 200px\">


                            <div class=\"card-body\">

                                <p class=\"text-info\">
                                    ";
                // line 34
                echo $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->truncateText($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 34), 100);
                echo "
                                </p>

                                <p class=\"text-muted small\">Added: ";
                // line 37
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "createdAt", [], "any", false, false, false, 37), "d/M/Y H:i:s"), "html", null, true);
                echo "</p>

                                <div>

                                    ";
                // line 41
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 41), 1))) {
                    // line 42
                    echo "                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to show this on Front Page?\"
                                                data-url=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_show", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 45)]), "html", null, true);
                    echo "\"
                                                title=\"Show Series\"
                                                class=\"btn btn-sm btn-success text-white confirm-action\">
                                            Show
                                        </button>
                                    ";
                }
                // line 51
                echo "
                                    ";
                // line 52
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "visibility", [], "any", false, false, false, 52), 1))) {
                    // line 53
                    echo "                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to hide this from Front Page?\"
                                                data-url=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_hide", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 56)]), "html", null, true);
                    echo "\"
                                                title=\"Hide Series\"
                                                class=\"btn btn-sm btn-light text-white confirm-action\">
                                            Hide
                                        </button>
                                    ";
                }
                // line 62
                echo "
                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to edit this Selection Series?\"
                                            data-url=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 66)]), "html", null, true);
                echo "\"
                                            title=\"Edit Selection Series\"
                                            class=\"btn btn-sm btn-warning text-white confirm-action\">
                                        <i class=\"fa fa-edit\"></i>
                                    </button>

                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to delete this Selection Series?\"
                                            data-url=\"";
                // line 75
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 75)]), "html", null, true);
                echo "\"
                                            title=\"Delete Selection Series\"
                                            class=\"btn btn-sm btn-danger text-white confirm-action\">
                                        <i class=\"fa fa-times\"></i>
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "
                <div class=\"modal fade mt-5\" id=\"action-confirm\" tabindex=\"-1\"
                     role=\"dialog\" aria-hidden=\"true\">
                    <div class=\"modal-dialog\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                        aria-label=\"Close\">
                                    <span aria-hidden=\"true\">&times;</span>
                                </button>
                            </div>
                            <div class=\"modal-body\"></div>
                            <div class=\"modal-footer\">
                                <button type=\"button\" class=\"btn btn-secondary\"
                                        data-dismiss=\"modal\">No
                                </button>

                                <form action=\"\"
                                      method=\"post\"
                                      class=\"d-inline-block\">
                                    <button class=\"btn btn-info\">Yes, I am!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            ";
        } else {
            // line 117
            echo "
                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No Series yet.</h6>
                </div>

            ";
        }
        // line 123
        echo "

        </div>

    </div>

    <form id=\"js-switch-form\" action=\"";
        // line 129
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_switch_photo_of_the_day");
        echo "\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 136
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 137
        echo "
    <script>
        \$(document).ready(function () {
            \$('.confirm-action').on('click', function (e) {
                let modalText = \$(this).data('modal-text');
                let url = \$(this).data('url');

                \$('.modal .modal-body').html(modalText);
                \$('.modal form').attr('action', url);
            })
        });
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/selection/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  331 => 137,  321 => 136,  305 => 129,  297 => 123,  289 => 117,  258 => 88,  239 => 75,  227 => 66,  221 => 62,  212 => 56,  207 => 53,  205 => 52,  202 => 51,  193 => 45,  188 => 42,  186 => 41,  179 => 37,  173 => 34,  163 => 27,  160 => 26,  154 => 22,  152 => 21,  146 => 18,  143 => 17,  139 => 16,  136 => 15,  134 => 14,  128 => 10,  118 => 9,  100 => 7,  81 => 5,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'template.html.twig' %}

{% block title %}Manage Selection Series{% endblock %}

{% block header_left %}Manage Selection Series{% endblock %}

{% block header_right %}{% endblock %}

{% block main_content %}

    <div class=\"col-12\">

        <div class=\"row\">
            {% if series|length > 0 %}

                {% for item in series %}

                    <div data-id=\"{{ item.id }}\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">
                        <div class=\"card mb-4 text-center\">

                            {% if item.visibility == 1 %}
                                <div class=\"alert-success position-absolute\" style=\"top: 0; width: 100%\">
                                    Shown on Front page
                                </div>
                            {% endif %}

                            <img src=\"{{ item.cover }}\" class=\"card-img-top\" alt=\"...\"
                                 style=\"object-fit: cover; width: 100%; height: 200px\">


                            <div class=\"card-body\">

                                <p class=\"text-info\">
                                    {{ item.title|easyadmin_truncate(100)|raw }}
                                </p>

                                <p class=\"text-muted small\">Added: {{ item.createdAt|date('d/M/Y H:i:s') }}</p>

                                <div>

                                    {% if item.visibility != 1 %}
                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to show this on Front Page?\"
                                                data-url=\"{{ path('admin_series_selection_show', {id: item.id}) }}\"
                                                title=\"Show Series\"
                                                class=\"btn btn-sm btn-success text-white confirm-action\">
                                            Show
                                        </button>
                                    {% endif %}

                                    {% if item.visibility == 1 %}
                                        <button data-toggle=\"modal\"
                                                data-target=\"#action-confirm\"
                                                data-modal-text=\"Are You sure that you want to hide this from Front Page?\"
                                                data-url=\"{{ path('admin_series_selection_hide', {id: item.id}) }}\"
                                                title=\"Hide Series\"
                                                class=\"btn btn-sm btn-light text-white confirm-action\">
                                            Hide
                                        </button>
                                    {% endif %}

                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to edit this Selection Series?\"
                                            data-url=\"{{ path('admin_series_selection_edit', {id: item.id}) }}\"
                                            title=\"Edit Selection Series\"
                                            class=\"btn btn-sm btn-warning text-white confirm-action\">
                                        <i class=\"fa fa-edit\"></i>
                                    </button>

                                    <button data-toggle=\"modal\"
                                            data-target=\"#action-confirm\"
                                            data-modal-text=\"Are You sure that you want to delete this Selection Series?\"
                                            data-url=\"{{ path('admin_series_selection_delete', {id: item.id}) }}\"
                                            title=\"Delete Selection Series\"
                                            class=\"btn btn-sm btn-danger text-white confirm-action\">
                                        <i class=\"fa fa-times\"></i>
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>

                {% endfor %}

                <div class=\"modal fade mt-5\" id=\"action-confirm\" tabindex=\"-1\"
                     role=\"dialog\" aria-hidden=\"true\">
                    <div class=\"modal-dialog\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                        aria-label=\"Close\">
                                    <span aria-hidden=\"true\">&times;</span>
                                </button>
                            </div>
                            <div class=\"modal-body\"></div>
                            <div class=\"modal-footer\">
                                <button type=\"button\" class=\"btn btn-secondary\"
                                        data-dismiss=\"modal\">No
                                </button>

                                <form action=\"\"
                                      method=\"post\"
                                      class=\"d-inline-block\">
                                    <button class=\"btn btn-info\">Yes, I am!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            {% else %}

                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No Series yet.</h6>
                </div>

            {% endif %}


        </div>

    </div>

    <form id=\"js-switch-form\" action=\"{{ path('admin_switch_photo_of_the_day') }}\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

{% endblock %}

{% block js %}

    <script>
        \$(document).ready(function () {
            \$('.confirm-action').on('click', function (e) {
                let modalText = \$(this).data('modal-text');
                let url = \$(this).data('url');

                \$('.modal .modal-body').html(modalText);
                \$('.modal form').attr('action', url);
            })
        });
    </script>

{% endblock %}
", "admin/selection/list.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/list.html.twig");
    }
}
