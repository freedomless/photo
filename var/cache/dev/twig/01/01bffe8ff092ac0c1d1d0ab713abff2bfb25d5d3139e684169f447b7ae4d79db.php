<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* poll/_results.html.twig */
class __TwigTemplate_035a2c523c85cfe6e0440baf8eb3eaad7bf6a4ded7a47101331d38800c14d589 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/_results.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/_results.html.twig"));

        // line 1
        echo "<ul class=\"list-unstyled poll-results\">

    <h4 class=\"h6 text-success font-italic\">Thank you for Your vote!</h4>

    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 5, $this->source); })()), "choices", [], "any", false, false, false, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 6
            echo "
        <li class=\"mb-4\">

            <div class=\"mb-2 poll-choice-title\">
                ";
            // line 10
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "title", [], "any", false, false, false, 10)), "html", null, true);
            echo " <span class=\"text-muted\">(";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "votes", [], "any", false, false, false, 10)), "html", null, true);
            echo ")</span>
            </div>

            ";
            // line 13
            $context["val"] = twig_round(((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "votes", [], "any", false, false, false, 13)) / (isset($context["total"]) || array_key_exists("total", $context) ? $context["total"] : (function () { throw new RuntimeError('Variable "total" does not exist.', 13, $this->source); })())) * 100), 2);
            // line 14
            echo "
            <div class=\"progress\" style=\"height: 1.5rem\">
                <div class=\"progress-bar\" role=\"progressbar\"
                     style=\"width: ";
            // line 17
            echo twig_escape_filter($this->env, (isset($context["val"]) || array_key_exists("val", $context) ? $context["val"] : (function () { throw new RuntimeError('Variable "val" does not exist.', 17, $this->source); })()), "html", null, true);
            echo "%;\"
                     aria-valuenow=\"";
            // line 18
            echo twig_escape_filter($this->env, (isset($context["val"]) || array_key_exists("val", $context) ? $context["val"] : (function () { throw new RuntimeError('Variable "val" does not exist.', 18, $this->source); })()), "html", null, true);
            echo "\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                    <span class=\"ml-3\">";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["val"]) || array_key_exists("val", $context) ? $context["val"] : (function () { throw new RuntimeError('Variable "val" does not exist.', 19, $this->source); })()), "html", null, true);
            echo "%</span>
                </div>
            </div>

        </li>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
</ul>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "poll/_results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 26,  82 => 19,  78 => 18,  74 => 17,  69 => 14,  67 => 13,  59 => 10,  53 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"list-unstyled poll-results\">

    <h4 class=\"h6 text-success font-italic\">Thank you for Your vote!</h4>

    {% for choice in poll.choices %}

        <li class=\"mb-4\">

            <div class=\"mb-2 poll-choice-title\">
                {{ choice.title|capitalize }} <span class=\"text-muted\">({{ choice.votes|length }})</span>
            </div>

            {% set val = ((choice.votes|length / total) * 100)|round(2) %}

            <div class=\"progress\" style=\"height: 1.5rem\">
                <div class=\"progress-bar\" role=\"progressbar\"
                     style=\"width: {{val}}%;\"
                     aria-valuenow=\"{{val}}\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                    <span class=\"ml-3\">{{val}}%</span>
                </div>
            </div>

        </li>

    {% endfor %}

</ul>", "poll/_results.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/poll/_results.html.twig");
    }
}
