<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/meta.html.twig */
class __TwigTemplate_a840c0dcb998b4c5247315eb84e54363c9f944307fcb32cb860f982f4484665a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/meta.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/meta.html.twig"));

        // line 1
        $context["title"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 1, $this->source); })()), "title", [], "any", false, false, false, 1)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.title")));
        // line 2
        $context["description"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 2, $this->source); })()), "description", [], "any", false, false, false, 2)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.description")));
        // line 3
        $context["image"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 3, $this->source); })()), "image", [], "any", false, false, false, 3)) : ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "request", [], "any", false, false, false, 3), "getSchemeAndHttpHost", [], "method", false, false, false, 3) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((isset($context["site_social_default_image"]) || array_key_exists("site_social_default_image", $context) ? $context["site_social_default_image"] : (function () { throw new RuntimeError('Variable "site_social_default_image" does not exist.', 3, $this->source); })())))));
        // line 4
        $context["imageWidth"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 4, $this->source); })()), "imageWidth", [], "any", false, false, false, 4)) : ((isset($context["site_social_default_image_width"]) || array_key_exists("site_social_default_image_width", $context) ? $context["site_social_default_image_width"] : (function () { throw new RuntimeError('Variable "site_social_default_image_width" does not exist.', 4, $this->source); })())));
        // line 5
        $context["imageHeight"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 5, $this->source); })()), "imageHeight", [], "any", false, false, false, 5)) : ((isset($context["site_social_default_image_height"]) || array_key_exists("site_social_default_image_height", $context) ? $context["site_social_default_image_height"] : (function () { throw new RuntimeError('Variable "site_social_default_image_height" does not exist.', 5, $this->source); })())));
        // line 6
        $context["creator"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 6, $this->source); })()), "creator", [], "any", false, false, false, 6)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("default.creator")));
        // line 7
        $context["createdAt"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 7, $this->source); })()), "createdAt", [], "any", false, false, false, 7)) : ("now"));
        // line 8
        $context["tags"] = ((array_key_exists("seo", $context)) ? (twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new RuntimeError('Variable "seo" does not exist.', 8, $this->source); })()), "tags", [], "any", false, false, false, 8)) : (""));
        // line 9
        echo "
<meta name=\"description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 10, $this->source); })()), "html", null, true);
        echo "\"/>
<!-- Schema.org markup for Google -->
<meta itemprop=\"name\" content=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "\">
<meta itemprop=\"description\" content=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["description"]) || array_key_exists("description", $context) ? $context["description"] : (function () { throw new RuntimeError('Variable "description" does not exist.', 13, $this->source); })()), "html", null, true);
        echo "\">
<meta itemprop=\"image\" content=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "html", null, true);
        echo "\">

<!-- Twitter Card data -->
<meta name=\"twitter:card\" content=\"summary_large_image\">
<meta name=\"twitter:site\" content=\"@publisher_handle\">
<meta name=\"twitter:title\" content=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "\">
<meta name=\"twitter:description\" content=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["description"]) || array_key_exists("description", $context) ? $context["description"] : (function () { throw new RuntimeError('Variable "description" does not exist.', 20, $this->source); })()), "html", null, true);
        echo "\">
<meta name=\"twitter:creator\" content=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["creator"]) || array_key_exists("creator", $context) ? $context["creator"] : (function () { throw new RuntimeError('Variable "creator" does not exist.', 21, $this->source); })()), "html", null, true);
        echo "\">
<meta name=\"twitter:image\" content=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 22, $this->source); })()), "html", null, true);
        echo "\">

<!-- Open Graph data -->
<meta property=\"og:title\" content=\"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 25, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"og:type\" content=\"article\"/>
<meta property=\"og:url\" content=\"";
        // line 27
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", true, true, false, 27) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", false, false, false, 27)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 27), "uri", [], "any", false, false, false, 27)) : ((isset($context["site_url"]) || array_key_exists("site_url", $context) ? $context["site_url"] : (function () { throw new RuntimeError('Variable "site_url" does not exist.', 27, $this->source); })()))), "html", null, true);
        echo "\"/>
<meta property=\"og:image\" content=\"";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 28, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"og:image:width\" content=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["imageWidth"]) || array_key_exists("imageWidth", $context) ? $context["imageWidth"] : (function () { throw new RuntimeError('Variable "imageWidth" does not exist.', 29, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"og:image:height\" content=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["imageHeight"]) || array_key_exists("imageHeight", $context) ? $context["imageHeight"] : (function () { throw new RuntimeError('Variable "imageHeight" does not exist.', 30, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"og:description\" content=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["description"]) || array_key_exists("description", $context) ? $context["description"] : (function () { throw new RuntimeError('Variable "description" does not exist.', 31, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"og:site_name\" content=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 32, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"article:published_time\" content=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["createdAt"]) || array_key_exists("createdAt", $context) ? $context["createdAt"] : (function () { throw new RuntimeError('Variable "createdAt" does not exist.', 33, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"article:section\" content=\"Article Section\"/>
<meta property=\"article:tag\" content=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["tags"]) || array_key_exists("tags", $context) ? $context["tags"] : (function () { throw new RuntimeError('Variable "tags" does not exist.', 35, $this->source); })()), "html", null, true);
        echo "\"/>
<meta property=\"fb:app_id\" content=\"2360993147521648\"/>

<!-- Canonical -->
<link rel=\"canonical\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", true, true, false, 39) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", false, false, false, 39)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 39), "uri", [], "any", false, false, false, 39)) : ((isset($context["site_url"]) || array_key_exists("site_url", $context) ? $context["site_url"] : (function () { throw new RuntimeError('Variable "site_url" does not exist.', 39, $this->source); })()))), "html", null, true);
        echo "\"/>
<base href=\"/\">

<!-- Chrome, Firefox OS and Opera -->
<meta name=\"theme-color\" content=\"#375a7f\">

<!-- Windows Phone -->
<meta name=\"msapplication-navbutton-color\" content=\"#375a7f\">

<!-- iOS Safari -->
<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"#375a7f\">
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/meta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 39,  135 => 35,  130 => 33,  126 => 32,  122 => 31,  118 => 30,  114 => 29,  110 => 28,  106 => 27,  101 => 25,  95 => 22,  91 => 21,  87 => 20,  83 => 19,  75 => 14,  71 => 13,  67 => 12,  62 => 10,  59 => 9,  57 => 8,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  47 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set title = seo is defined ? seo.title : 'default.title' | trans %}
{% set description = seo is defined ? seo.description : 'default.description' | trans %}
{% set image = seo is defined ? seo.image : app.request.getSchemeAndHttpHost() ~ asset(site_social_default_image) %}
{% set imageWidth = seo is defined ? seo.imageWidth : site_social_default_image_width %}
{% set imageHeight = seo is defined ? seo.imageHeight : site_social_default_image_height %}
{% set creator = seo is defined ? seo.creator : 'default.creator' | trans %}
{% set createdAt = seo is defined ? seo.createdAt : 'now' %}
{% set tags = seo is defined ? seo.tags : '' %}

<meta name=\"description\" content=\"{{ site_name }}\"/>
<!-- Schema.org markup for Google -->
<meta itemprop=\"name\" content=\"{{ title }}\">
<meta itemprop=\"description\" content=\"{{ description }}\">
<meta itemprop=\"image\" content=\"{{ image }}\">

<!-- Twitter Card data -->
<meta name=\"twitter:card\" content=\"summary_large_image\">
<meta name=\"twitter:site\" content=\"@publisher_handle\">
<meta name=\"twitter:title\" content=\"{{ title }}\">
<meta name=\"twitter:description\" content=\"{{ description }}\">
<meta name=\"twitter:creator\" content=\"{{ creator }}\">
<meta name=\"twitter:image\" content=\"{{ image }}\">

<!-- Open Graph data -->
<meta property=\"og:title\" content=\"{{ title }}\"/>
<meta property=\"og:type\" content=\"article\"/>
<meta property=\"og:url\" content=\"{{ app.request.uri ?? site_url }}\"/>
<meta property=\"og:image\" content=\"{{ image }}\"/>
<meta property=\"og:image:width\" content=\"{{ imageWidth }}\"/>
<meta property=\"og:image:height\" content=\"{{ imageHeight }}\"/>
<meta property=\"og:description\" content=\"{{ description }}\"/>
<meta property=\"og:site_name\" content=\"{{ site_name }}\"/>
<meta property=\"article:published_time\" content=\"{{ createdAt }}\"/>
<meta property=\"article:section\" content=\"Article Section\"/>
<meta property=\"article:tag\" content=\"{{ tags }}\"/>
<meta property=\"fb:app_id\" content=\"2360993147521648\"/>

<!-- Canonical -->
<link rel=\"canonical\" href=\"{{ app.request.uri ?? site_url }}\"/>
<base href=\"/\">

<!-- Chrome, Firefox OS and Opera -->
<meta name=\"theme-color\" content=\"#375a7f\">

<!-- Windows Phone -->
<meta name=\"msapplication-navbutton-color\" content=\"#375a7f\">

<!-- iOS Safari -->
<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"#375a7f\">
", "_partials/meta.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/meta.html.twig");
    }
}
