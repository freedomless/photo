<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_poster.html.twig */
class __TwigTemplate_29e5749d011788fb4391b90d2b2c0adaeeb6253c1417c4b795987e7e749c23b3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_poster.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_poster.html.twig"));

        // line 8
        echo "
<img src=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["poster_url"]) || array_key_exists("poster_url", $context) ? $context["poster_url"] : (function () { throw new RuntimeError('Variable "poster_url" does not exist.', 9, $this->source); })()), "html", null, true);
        echo "\" alt=\"\" style=\"max-width: 300px\">";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "emails/_poster.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 9,  43 => 8,);
    }

    public function getSourceContext()
    {
        return new Source("{#<tr style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\">#}
{#    <td valign=\"middle\" class=\"counter\">#}
{#        <div style=\"height: 8em !important; overflow: hidden\">#}
{#            <img src=\"{{ poster_url }}\" alt=\"\" width=\"100%\">#}
{#        </div>#}
{#    </td>#}
{#</tr>#}

<img src=\"{{ poster_url }}\" alt=\"\" style=\"max-width: 300px\">", "emails/_poster.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_poster.html.twig");
    }
}
