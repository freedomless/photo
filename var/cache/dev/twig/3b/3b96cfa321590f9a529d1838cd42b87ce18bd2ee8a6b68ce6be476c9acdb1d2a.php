<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/reject-reasons.html.twig */
class __TwigTemplate_d53a3e72acc6363b764a66b0be266c9f453f8af6d1233225446ca12f6307bf72 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/reject-reasons.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/reject-reasons.html.twig"));

        // line 1
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 1, $this->source); })()), "rejectReasons", [], "any", false, false, false, 1)), 0))) {
            // line 2
            echo "
    <div class=\"card mt-3\">

        <div class=\"card-header d-flex\">

            <strong class=\"flex-grow-1\">Reject Reasons</strong>

        </div>
        <div class=\"card-body\">


            <ul class=\"list-group\">
                ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 14, $this->source); })()), "rejectReasons", [], "any", false, false, false, 14));
            foreach ($context['_seq'] as $context["_key"] => $context["reason"]) {
                // line 15
                echo "                    <ol class=\"list-group-item\">Reason: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "reason", [], "any", false, false, false, 15), "html", null, true);
                echo "</ol>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reason'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            </ul>

        </div>

    </div>



";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/reject-reasons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 17,  63 => 15,  59 => 14,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if post.rejectReasons | length > 0%}

    <div class=\"card mt-3\">

        <div class=\"card-header d-flex\">

            <strong class=\"flex-grow-1\">Reject Reasons</strong>

        </div>
        <div class=\"card-body\">


            <ul class=\"list-group\">
                {% for reason in post.rejectReasons %}
                    <ol class=\"list-group-item\">Reason: {{ reason.reason }}</ol>
                {% endfor %}
            </ul>

        </div>

    </div>



{% endif %}
", "admin/curator/_partials/reject-reasons.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/reject-reasons.html.twig");
    }
}
