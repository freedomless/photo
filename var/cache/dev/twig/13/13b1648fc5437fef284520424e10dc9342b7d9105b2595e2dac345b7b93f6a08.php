<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/posts.html.twig */
class __TwigTemplate_20c518a6b562c53d825006467ddd2a6c0d3bcc8effb6ee9d98ee56cec6c5fcfa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/posts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/posts.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)]), "html", null, true);
            echo "\">

                    ";
            // line 12
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "type", [], "any", false, false, false, 12), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 12), "cover", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)));
            // line 13
            echo "
                    <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "html", null, true);
            echo "\"
                         alt=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 15), "title", [], "any", false, false, false, 15), "html", null, true);
            echo "\">
                </a>

                ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 18), 1)) {
                // line 19
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 25
            echo "
                ";
            // line 26
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 26), 3) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 26), "showDate", [], "any", false, false, false, 26), twig_date_converter($this->env))))) {
                // line 27
                echo "
                    ";
                // line 28
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 28), "curatedAt", [], "any", false, false, false, 28), (isset($context["site_publish_visibility_delay"]) || array_key_exists("site_publish_visibility_delay", $context) ? $context["site_publish_visibility_delay"] : (function () { throw new RuntimeError('Variable "site_publish_visibility_delay" does not exist.', 28, $this->source); })())), twig_date_converter($this->env)))) {
                    // line 29
                    echo "
                        <div class=\"sticker pending semi-transparent\">
                            Pending Review!
                        </div>

                    ";
                } else {
                    // line 35
                    echo "
                        <div class=\"sticker approved semi-transparent\">
                            Scheduled for Publish!
                        </div>

                    ";
                }
                // line 41
                echo "
                ";
            }
            // line 43
            echo "
                ";
            // line 44
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), 3) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), "showDate", [], "any", false, false, false, 44), twig_date_converter($this->env))))) {
                // line 45
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        Published!
                    </div>

                ";
            }
            // line 51
            echo "
                ";
            // line 52
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 52), 2)) {
                // line 53
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 59
            echo "
            </div>

            <div class=\"image-thumb-metadata\">

                Stats

                <div class=\"stats mt-1 mb-1 text-center\">

                    <i class=\"far fa-eye\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Views\"></i> ";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 69), "views", [], "any", false, false, false, 69), "html", null, true);
            echo "

                    <i class=\"ml-5 far fa-heart\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Favorite\"></i> ";
            // line 72
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 72), "favorites", [], "any", false, false, false, 72)), "html", null, true);
            echo "

                    ";
            // line 74
            if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 74), 0))) {
                // line 75
                echo "                        <i class=\"ml-5 fas fa-fire-alt\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"Popularity\"></i>
                        ";
                // line 77
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 77), 2, ".", ","), "html", null, true);
                echo " / 5
                    ";
            }
            // line 79
            echo "
                    ";
            // line 80
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 80), "isGreyscale", [], "any", false, false, false, 80)) {
                // line 81
                echo "                        <i class=\"ml-5 fas fa-adjust\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Black&White\"></i>
                    ";
            }
            // line 84
            echo "
                </div>

                <hr class=\"no-gutter\">

                Actions

                <div class=\"actions mb-1\">


                    ";
            // line 94
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 94), 0) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 94), "parent", [], "any", false, false, false, 94)))) {
                // line 95
                echo "

                        <button class=\"btn curator-warning-btn btn-info\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Send to Curators\"
                                data-url=\"";
                // line 101
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_curate", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 101), "id", [], "any", false, false, false, 101), "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 101, $this->source); })()), "request", [], "any", false, false, false, 101), "get", [0 => "page"], "method", false, false, false, 101)]), "html", null, true);
                echo "\">
                            Send to curators
                        </button>


                    ";
            }
            // line 107
            echo "
                    <a href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 108), "id", [], "any", false, false, false, 108)]), "html", null, true);
            echo "\"
                       data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Edit\"
                       class=\"btn btn-default\">
                        <i class=\"fa fa-edit\"></i>
                    </a>

                    ";
            // line 115
            if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 115), true)) && $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source,             // line 116
$context["data"], "post", [], "any", false, false, false, 116), 3)) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 116), "showDate", [], "any", false, false, false, 116), twig_date_converter($this->env))))) {
                // line 117
                echo "
                        <a href=\"";
                // line 118
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 118), "id", [], "any", false, false, false, 118), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 118, $this->source); })()), "request", [], "any", false, false, false, 118), "get", [0 => "type"], "method", false, false, false, 118)]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"";
                // line 122
                echo (( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 122), "product", [], "any", false, false, false, 122))) ? ("Update Products") : ("Add to store"));
                echo "\">

                            ";
                // line 124
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 124), "product", [], "any", false, false, false, 124))) {
                    // line 125
                    echo "                                <i class=\"fas fa-store\"></i>
                            ";
                } else {
                    // line 127
                    echo "                                <span class=\"btn btn-outline-success btn-sm\">
                                    SELL
                                </span>
                            ";
                }
                // line 131
                echo "
                        </a>

                    ";
            }
            // line 135
            echo "

                    ";
            // line 137
            if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), 0) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), 2)) && (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 137), "parent", [], "any", false, false, false, 137)))) {
                // line 138
                echo "
                        <button type=\"button\"
                                class=\"btn delete-warning-btn\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Delete\"
                                data-url=\"";
                // line 144
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 144), "id", [], "any", false, false, false, 144)]), "html", null, true);
                echo "\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>
                        ";
                // line 148
                echo "                    ";
            }
            // line 149
            echo "
                </div>

            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "post/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 149,  287 => 148,  281 => 144,  273 => 138,  271 => 137,  267 => 135,  261 => 131,  255 => 127,  251 => 125,  249 => 124,  244 => 122,  237 => 118,  234 => 117,  232 => 116,  231 => 115,  221 => 108,  218 => 107,  209 => 101,  201 => 95,  199 => 94,  187 => 84,  182 => 81,  180 => 80,  177 => 79,  172 => 77,  168 => 75,  166 => 74,  161 => 72,  155 => 69,  143 => 59,  135 => 53,  133 => 52,  130 => 51,  122 => 45,  120 => 44,  117 => 43,  113 => 41,  105 => 35,  97 => 29,  95 => 28,  92 => 27,  90 => 26,  87 => 25,  79 => 19,  77 => 18,  71 => 15,  67 => 14,  64 => 13,  62 => 12,  57 => 10,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for data in posts %}

    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"{{ path('web_photo_update',{id: data.post.id}) }}\">

                    {% set image = data.post.type == 1 ?  data.post.image.thumbnail  : data.post.cover.image.thumbnail %}

                    <img src=\"{{ image }}\"
                         alt=\"{{ data.post.title }}\">
                </a>

                {% if workflow_has_marked_place(data.post, 1) %}

                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(data.post, 3) and data.post.showDate > date() %}

                    {% if data.post.curatedAt|date_modify(site_publish_visibility_delay) > date() %}

                        <div class=\"sticker pending semi-transparent\">
                            Pending Review!
                        </div>

                    {% else %}

                        <div class=\"sticker approved semi-transparent\">
                            Scheduled for Publish!
                        </div>

                    {% endif %}

                {% endif %}

                {% if workflow_has_marked_place(data.post, 3) and data.post.showDate < date() %}

                    <div class=\"sticker approved semi-transparent\">
                        Published!
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(data.post, 2) %}

                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                {% endif %}

            </div>

            <div class=\"image-thumb-metadata\">

                Stats

                <div class=\"stats mt-1 mb-1 text-center\">

                    <i class=\"far fa-eye\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Views\"></i> {{ data.post.views }}

                    <i class=\"ml-5 far fa-heart\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Favorite\"></i> {{ data.post.favorites|length }}

                    {% if data.rating != 0 %}
                        <i class=\"ml-5 fas fa-fire-alt\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                            title=\"Popularity\"></i>
                        {{ data.rating|number_format(2, '.', ',') }} / 5
                    {% endif %}

                    {% if data.post.isGreyscale %}
                        <i class=\"ml-5 fas fa-adjust\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Black&White\"></i>
                    {% endif %}

                </div>

                <hr class=\"no-gutter\">

                Actions

                <div class=\"actions mb-1\">


                    {% if workflow_has_marked_place(data.post, 0) and data.post.parent is null %}


                        <button class=\"btn curator-warning-btn btn-info\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Send to Curators\"
                                data-url=\"{{ path('web_photo_for_curate',{id: data.post.id, page:app.request.get('page')}) }}\">
                            Send to curators
                        </button>


                    {% endif %}

                    <a href=\"{{ path('web_photo_update',{id:data.post.id}) }}\"
                       data-toggle=\"tooltip\" data-placement=\"bottom\"
                       title=\"Edit\"
                       class=\"btn btn-default\">
                        <i class=\"fa fa-edit\"></i>
                    </a>

                    {% if config().allowShopping == true and
                        workflow_has_marked_place(data.post, 3) and data.post.showDate < date() %}

                        <a href=\"{{ path('web_photo_for_sale', {id:data.post.id,type: app.request.get('type')}) }}\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"{{ data.post.product is not null ? 'Update Products' : 'Add to store' }}\">

                            {% if data.post.product is not null %}
                                <i class=\"fas fa-store\"></i>
                            {% else %}
                                <span class=\"btn btn-outline-success btn-sm\">
                                    SELL
                                </span>
                            {% endif %}

                        </a>

                    {% endif %}


                    {% if ( workflow_has_marked_place(data.post,0) or workflow_has_marked_place(data.post,2)) and data.post.parent is null %}

                        <button type=\"button\"
                                class=\"btn delete-warning-btn\"
                                data-toggle=\"tooltip\"
                                data-placement=\"bottom\"
                                data-title=\"Delete\"
                                data-url=\"{{ path('web_photo_delete',{id:data.post.id}) }}\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>
                        {##}
                    {% endif %}

                </div>

            </div>

        </div>

    </div>

{% endfor %}
", "post/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/posts.html.twig");
    }
}
