<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/success.html.twig */
class __TwigTemplate_ee4d3f557071de3f2420439b3eca2a6b29195fc07b23034d5a0ff5803588ed58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'style' => [$this, 'block_style'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "store/success.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "store/success.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "store/success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo "    Invoice
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_style($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "style"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "style"));

        // line 6
        echo "    <style>
        @media print {
            .navigation-bar {
                display: none !important;
            }
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "
    <div class=\"container mt-5 mb-5\" id=\"pdf\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card shadow border-0\">
                    <div class=\"card-body p-0\">
                        <div class=\"row p-5\">
                            <div class=\"col-md-6\">
                                <h3>Invoice</h3>
                                <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" width=\"50px\">
                            </div>
                            <div class=\"col-md-6 text-right\">
                                <p class=\"fmb-1\">
                                    <button class=\"btn btn-blue\" onclick=\"window.print()\">
                                        <i class=\"fas fa-print\"></i>
                                    </button>
                                    <button class=\"btn btn-blue\" id=\"download-pdf\">
                                        <i class=\"far fa-file-pdf\"></i>
                                    </button>
                                </p>
                                <p class=\"font-weight-bold mb-1\">Receipt #";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 35, $this->source); })()), "id", [], "any", false, false, false, 35), "html", null, true);
        echo "</p>
                                <p class=\"font-weight-bold mb-1\">Order #";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 36, $this->source); })()), "order", [], "any", false, false, false, 36), "token", [], "any", false, false, false, 36), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <hr class=\"my-5\">
                        <div class=\"row pb-5 p-5\">
                            <div class=\"col-md-6\">
                                <p class=\"font-weight-bold mb-4\">Client Information</p>
                                <p class=\"mb-1\">";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 43, $this->source); })()), "order", [], "any", false, false, false, 43), "billingAddress", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "</p>
                                <p class=\"mb-1\">";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 44, $this->source); })()), "order", [], "any", false, false, false, 44), "billingAddress", [], "any", false, false, false, 44), "city", [], "any", false, false, false, 44), "html", null, true);
        echo "
                                    , ";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 45, $this->source); })()), "order", [], "any", false, false, false, 45), "billingAddress", [], "any", false, false, false, 45), "country", [], "any", false, false, false, 45), "html", null, true);
        echo "</p>
                                <p class=\"mb-1\">";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 46, $this->source); })()), "order", [], "any", false, false, false, 46), "billingAddress", [], "any", false, false, false, 46), "zip", [], "any", false, false, false, 46), "html", null, true);
        echo "</p>
                            </div>
                            <div class=\"col-md-3\"></div>
                            <div class=\"col-md-3\">
                                <p class=\"font-weight-bold mb-4\">Payment Details</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">VAT: </span> ";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 52, $this->source); })()), "vatNumber", [], "any", false, false, false, 52), "html", null, true);
        echo "</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Payment Type: </span> ";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 54, $this->source); })()), "order", [], "any", false, false, false, 54), "transaction", [], "any", false, false, false, 54), "gateway", [], "any", false, false, false, 54), "html", null, true);
        echo "
                                </p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Name: </span> ";
        // line 57
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 57, $this->source); })()), "name", [], "any", false, false, false, 57), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <div class=\"row p-5\">
                            <div class=\"col-md-12\">
                                <table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">ID</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Media</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Size</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Quantity</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ";
        // line 75
        $context["subtotal"] = 0;
        // line 76
        echo "                                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 76, $this->source); })()), "order", [], "any", false, false, false, 76), "items", [], "any", false, false, false, 76));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 77
            echo "                                            ";
            $context["subtotal"] = (((isset($context["subtotal"]) || array_key_exists("subtotal", $context) ? $context["subtotal"] : (function () { throw new RuntimeError('Variable "subtotal" does not exist.', 77, $this->source); })()) + twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 77)) * twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 77));
            // line 78
            echo "                                            <tr>
                                                <td>";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 79), "id", [], "any", false, false, false, 79), "html", null, true);
            echo "</td>
                                                <td>";
            // line 80
            echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", true, true, false, 80) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", false, false, false, 80)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, true, false, 80), "post", [], "any", false, true, false, 80), "title", [], "any", false, false, false, 80)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 80), "post", [], "any", false, false, false, 80), "id", [], "any", false, false, false, 80))), "html", null, true);
            echo "</td>
                                                <td>";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "material", [], "any", false, false, false, 81), "name", [], "any", false, false, false, 81), "html", null, true);
            echo "</td>
                                                <td>";
            // line 82
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 82), "width", [], "any", false, false, false, 82), "html", null, true);
            echo "x";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "size", [], "any", false, false, false, 82), "height", [], "any", false, false, false, 82), "html", null, true);
            echo "</td>
                                                <td>";
            // line 83
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 83), "html", null, true);
            echo "</td>
                                                <td>";
            // line 84
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 84), 2), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 84, $this->source); })()), "currency", [], "any", false, false, false, 84), "html", null, true);
            echo "</td>
                                                <td>";
            // line 85
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 85) * twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 85)), 2), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 85, $this->source); })()), "currency", [], "any", false, false, false, 85), "html", null, true);
            echo "</td>
                                                <td>
                                                </td>
                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "                                        <tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class=\"d-flex flex-row-reverse bg-dark text-white p-4\">
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Grand Total</div>
                                <div class=\"h2 font-weight-light\">
                                    ";
        // line 99
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 99, $this->source); })()), "order", [], "any", false, false, false, 99), "transaction", [], "any", false, false, false, 99), "paymentGross", [], "any", false, false, false, 99), 2), "html", null, true);
        echo "
                                    ";
        // line 100
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 100, $this->source); })()), "order", [], "any", false, false, false, 100), "transaction", [], "any", false, false, false, 100), "currencyCode", [], "any", false, false, false, 100), "html", null, true);
        echo "
                                </div>
                            </div>

                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Tax</div>
                                <div class=\"h2 font-weight-light\">";
        // line 106
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 106, $this->source); })()), "tax", [], "any", false, false, false, 106) * (isset($context["subtotal"]) || array_key_exists("subtotal", $context) ? $context["subtotal"] : (function () { throw new RuntimeError('Variable "subtotal" does not exist.', 106, $this->source); })())), 2), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 106, $this->source); })()), "currency", [], "any", false, false, false, 106), "html", null, true);
        echo "</div>
                            </div>
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Sub - Total amount</div>
                                <div class=\"h2 font-weight-light\">";
        // line 110
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["subtotal"]) || array_key_exists("subtotal", $context) ? $context["subtotal"] : (function () { throw new RuntimeError('Variable "subtotal" does not exist.', 110, $this->source); })()), 2), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 110, $this->source); })()), "currency", [], "any", false, false, false, 110), "html", null, true);
        echo "</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 120
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 121
        echo "    <script src=\"//cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.1/jspdf.debug.js\" crossorigin=\"anonymous\"></script>
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js\" crossorigin=\"anonymous\"></script>

    <script>
        \$('#download-pdf').on('click',function(){

            html2canvas(document.querySelector('#pdf'), {imageTimeout: 5000, useCORS: true}).then(canvas => {
                document.getElementById('pdf').appendChild(canvas)
                let img = canvas.toDataURL('image/png')
                let pdf = new jsPDF('landscape')
                pdf.addImage(img, 'JPEG', 0, 0, 300, 210)
                pdf.save('order-";
        // line 132
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 132, $this->source); })()), "order", [], "any", false, false, false, 132), "token", [], "any", false, false, false, 132), "html", null, true);
        echo ".pdf')
                \$('canvas').remove();
            })

        })
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "store/success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 132,  330 => 121,  320 => 120,  298 => 110,  289 => 106,  280 => 100,  276 => 99,  265 => 90,  252 => 85,  246 => 84,  242 => 83,  236 => 82,  232 => 81,  228 => 80,  224 => 79,  221 => 78,  218 => 77,  213 => 76,  211 => 75,  190 => 57,  184 => 54,  179 => 52,  170 => 46,  166 => 45,  162 => 44,  158 => 43,  148 => 36,  144 => 35,  130 => 24,  119 => 15,  109 => 14,  92 => 6,  82 => 5,  71 => 3,  61 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %}
    Invoice
{% endblock %}
{% block style %}
    <style>
        @media print {
            .navigation-bar {
                display: none !important;
            }
        }
    </style>
{% endblock %}
{% block body %}

    <div class=\"container mt-5 mb-5\" id=\"pdf\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card shadow border-0\">
                    <div class=\"card-body p-0\">
                        <div class=\"row p-5\">
                            <div class=\"col-md-6\">
                                <h3>Invoice</h3>
                                <img src=\"{{ asset('images/logo.png') }}\" width=\"50px\">
                            </div>
                            <div class=\"col-md-6 text-right\">
                                <p class=\"fmb-1\">
                                    <button class=\"btn btn-blue\" onclick=\"window.print()\">
                                        <i class=\"fas fa-print\"></i>
                                    </button>
                                    <button class=\"btn btn-blue\" id=\"download-pdf\">
                                        <i class=\"far fa-file-pdf\"></i>
                                    </button>
                                </p>
                                <p class=\"font-weight-bold mb-1\">Receipt #{{ invoice.id }}</p>
                                <p class=\"font-weight-bold mb-1\">Order #{{ invoice.order.token }}</p>
                            </div>
                        </div>
                        <hr class=\"my-5\">
                        <div class=\"row pb-5 p-5\">
                            <div class=\"col-md-6\">
                                <p class=\"font-weight-bold mb-4\">Client Information</p>
                                <p class=\"mb-1\">{{ invoice.order.billingAddress.name }}</p>
                                <p class=\"mb-1\">{{ invoice.order.billingAddress.city }}
                                    , {{ invoice.order.billingAddress.country }}</p>
                                <p class=\"mb-1\">{{ invoice.order.billingAddress.zip }}</p>
                            </div>
                            <div class=\"col-md-3\"></div>
                            <div class=\"col-md-3\">
                                <p class=\"font-weight-bold mb-4\">Payment Details</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">VAT: </span> {{ configuration.vatNumber }}</p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Payment Type: </span> {{ invoice.order.transaction.gateway }}
                                </p>
                                <p class=\"mb-1 text-right\"><span
                                            class=\"text-muted float-left\">Name: </span> {{ configuration.name }}</p>
                            </div>
                        </div>
                        <div class=\"row p-5\">
                            <div class=\"col-md-12\">
                                <table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">ID</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Media</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Size</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Quantity</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Item</th>
                                            <th class=\"border-0 text-uppercase small font-weight-bold\">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {% set subtotal = 0 %}
                                        {% for item in invoice.order.items %}
                                            {% set subtotal = (subtotal + item.price) * item.quantity %}
                                            <tr>
                                                <td>{{ item.product.id }}</td>
                                                <td>{{ item.product.post.title ?? item.product.post.id }}</td>
                                                <td>{{ item.material.name }}</td>
                                                <td>{{ item.size.width }}x{{ item.size.height }}</td>
                                                <td>{{ item.quantity }}</td>
                                                <td>{{ (item.price ) | number_format(2)  }} {{ configuration.currency }}</td>
                                                <td>{{ (item.price * item.quantity) | number_format(2)  }} {{ configuration.currency }}</td>
                                                <td>
                                                </td>
                                            </tr>
                                        {% endfor %}
                                        <tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class=\"d-flex flex-row-reverse bg-dark text-white p-4\">
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Grand Total</div>
                                <div class=\"h2 font-weight-light\">
                                    {{ invoice.order.transaction.paymentGross | number_format(2) }}
                                    {{ invoice.order.transaction.currencyCode }}
                                </div>
                            </div>

                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Tax</div>
                                <div class=\"h2 font-weight-light\">{{ (configuration.tax * subtotal) | number_format(2)  }} {{ configuration.currency }}</div>
                            </div>
                            <div class=\"py-3 px-5 text-right\">
                                <div class=\"mb-2\">Sub - Total amount</div>
                                <div class=\"h2 font-weight-light\">{{ subtotal | number_format(2)  }} {{ configuration.currency }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
{% block js %}
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.1/jspdf.debug.js\" crossorigin=\"anonymous\"></script>
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js\" crossorigin=\"anonymous\"></script>

    <script>
        \$('#download-pdf').on('click',function(){

            html2canvas(document.querySelector('#pdf'), {imageTimeout: 5000, useCORS: true}).then(canvas => {
                document.getElementById('pdf').appendChild(canvas)
                let img = canvas.toDataURL('image/png')
                let pdf = new jsPDF('landscape')
                pdf.addImage(img, 'JPEG', 0, 0, 300, 210)
                pdf.save('order-{{ invoice.order.token }}.pdf')
                \$('canvas').remove();
            })

        })
    </script>

{% endblock %}

", "store/success.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/success.html.twig");
    }
}
