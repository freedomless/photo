<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* album/album.html.twig */
class __TwigTemplate_740f34d96d9eacf4a94dd4eaf16ab6950c3f4da40dd3c563b1d8ee9d24d961a1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "album/album.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "album/album.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) || array_key_exists("albums", $context) ? $context["albums"] : (function () { throw new RuntimeError('Variable "albums" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 2
            echo "
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_show", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 9)]), "html", null, true);
            echo "\" class=\"text-decoration-none\">
                    ";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 10)) {
                // line 11
                echo "                        <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 11), "image", [], "any", false, false, false, 11), "thumbnail", [], "any", false, false, false, 11), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "cover", [], "any", false, false, false, 11), "title", [], "any", false, false, false, 11), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 13
                echo "                        <div class=\"d-flex flex-column justify-content-center align-items-center p-5 \">
                            <i class=\"fas fa-image\"></i>
                        </div>
                    ";
            }
            // line 17
            echo "                </a>

            </div>

            <div class=\"image-thumb-metadata\">

                <p class=\"text-center\">
                    ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["album"], "name", [], "any", false, false, false, 24), "html", null, true);
            echo "
                </p>

                ";
            // line 28
            echo "                ";
            if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 28, $this->source); })()), "user", [], "any", false, false, false, 28) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 28, $this->source); })()), "user", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["album"], "user", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28))))) {
                // line 29
                echo "                    <hr>

                    <div class=\"actions mb-1\">
                        <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_update", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 32)]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Edit Album\"
                           class=\"btn btn-default\">
                            <i class=\"fa fa-edit\"></i>
                        </a>

                        <button type=\"button\" data-url=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_album_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["album"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\"
                                data-toggle=\"modal\" data-target=\"#delete-confirm\"
                                title=\"Delete Album\"
                                class=\"btn btn-link open-confirm\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>

                        ";
                // line 47
                echo "                        <div class=\"modal fade mt-5\" id=\"delete-confirm\" tabindex=\"-1\"
                             role=\"dialog\" aria-hidden=\"true\">
                            <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                aria-label=\"Close\">
                                            <span aria-hidden=\"true\">&times;</span>
                                        </button>
                                    </div>
                                    <div class=\"modal-body\">
                                        Are you sure you want to delete this Album?
                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-secondary\"
                                                data-dismiss=\"modal\">No
                                        </button>

                                        <form action=\"#\"
                                              method=\"post\"
                                              class=\"d-inline-block\">
                                            <button class=\"btn btn-info\">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                ";
            }
            // line 78
            echo "
            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "album/album.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 78,  119 => 47,  109 => 39,  99 => 32,  94 => 29,  91 => 28,  85 => 24,  76 => 17,  70 => 13,  62 => 11,  60 => 10,  56 => 9,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for album in albums %}

    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">

                <a href=\"{{ path('web_album_show',{id:album.id}) }}\" class=\"text-decoration-none\">
                    {% if album.cover %}
                        <img src=\"{{ album.cover.image.thumbnail }}\" alt=\"{{ album.cover.title }}\">
                    {% else %}
                        <div class=\"d-flex flex-column justify-content-center align-items-center p-5 \">
                            <i class=\"fas fa-image\"></i>
                        </div>
                    {% endif %}
                </a>

            </div>

            <div class=\"image-thumb-metadata\">

                <p class=\"text-center\">
                    {{ album.name }}
                </p>

                {# Actions #}
                {% if app.user and app.user.id == album.user.id %}
                    <hr>

                    <div class=\"actions mb-1\">
                        <a href=\"{{ path('web_album_update',{id:album.id}) }}\"
                           data-toggle=\"tooltip\" data-placement=\"bottom\"
                           title=\"Edit Album\"
                           class=\"btn btn-default\">
                            <i class=\"fa fa-edit\"></i>
                        </a>

                        <button type=\"button\" data-url=\"{{ path('web_album_delete',{id:album.id}) }}\"
                                data-toggle=\"modal\" data-target=\"#delete-confirm\"
                                title=\"Delete Album\"
                                class=\"btn btn-link open-confirm\">
                            <i class=\"fas fa-trash-alt text-danger\"></i>
                        </button>

                        {# Delete Modal #}
                        <div class=\"modal fade mt-5\" id=\"delete-confirm\" tabindex=\"-1\"
                             role=\"dialog\" aria-hidden=\"true\">
                            <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Confirm</h5>
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                aria-label=\"Close\">
                                            <span aria-hidden=\"true\">&times;</span>
                                        </button>
                                    </div>
                                    <div class=\"modal-body\">
                                        Are you sure you want to delete this Album?
                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-secondary\"
                                                data-dismiss=\"modal\">No
                                        </button>

                                        <form action=\"#\"
                                              method=\"post\"
                                              class=\"d-inline-block\">
                                            <button class=\"btn btn-info\">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                {% endif %}

            </div>

        </div>

    </div>

{% endfor %}
", "album/album.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/album/album.html.twig");
    }
}
