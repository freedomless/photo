<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vote/yes_no/index.html.twig */
class __TwigTemplate_a76598de0bd72eb572196d12b00631d558eaa90f26b69442a2f5d28971be9e25 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "vote/yes_no/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "vote/yes_no/index.html.twig"));

        // line 1
        echo "<form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => 1]);
        echo "\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-success action shadow action  action-publish\"><i class=\"fas fa-check\"></i>
    </button>
</form>
<div class=\"d-inline-block\">
    ";
        // line 6
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Favorite", ["rendering" => "client_side", "props" => ["post" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 6, $this->source); })()), "id", [], "any", false, false, false, 6), "favorite" => (isset($context["favorite"]) || array_key_exists("favorite", $context) ? $context["favorite"] : (function () { throw new RuntimeError('Variable "favorite" does not exist.', 6, $this->source); })())]]);
        echo "
</div>
<form action=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => 0]);
        echo "\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-danger action shadow  action action-reject\"><i class=\"fas fa-times\"></i>
    </button>
</form>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "vote/yes_no/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  52 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form action=\"{{ path('web_vote_action',{vote:1}) }}\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-success action shadow action  action-publish\"><i class=\"fas fa-check\"></i>
    </button>
</form>
<div class=\"d-inline-block\">
    {{ react_component('Favorite',{rendering:'client_side',props:{post:post.id,favorite:favorite}}) }}
</div>
<form action=\"{{ path('web_vote_action',{vote:0}) }}\" method=\"post\" class=\"d-inline\">
    <button class=\"btn btn-danger action shadow  action action-reject\"><i class=\"fas fa-times\"></i>
    </button>
</form>
", "vote/yes_no/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/vote/yes_no/index.html.twig");
    }
}
