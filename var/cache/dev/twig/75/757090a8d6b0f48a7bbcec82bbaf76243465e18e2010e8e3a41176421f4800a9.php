<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/show.html.twig */
class __TwigTemplate_0f5677644f40ee444239bd80011a6fd03970a3e0a8523bef572fd0c9e9e23091 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", true, true, false, 3) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 3)))) ? (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 3)) : (twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 3, $this->source); })()), "slug", [], "any", false, false, false, 3))), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"mt-5\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/show.html.twig", 9)->display($context);
        // line 10
        echo "
    </div>

    <hr>

    <div class=\"container basic-layout\">


        ";
        // line 18
        if (twig_in_filter("pending", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "session", [], "any", false, false, false, 18), "get", [0 => "back"], "method", false, false, false, 18))) {
            // line 19
            echo "
        <div class=\"clearfix\">

            <div class=\"float-left\">
                <i class=\"fa fa-backward\"></i>
                <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show_prev", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 24, $this->source); })()), "id", [], "any", false, false, false, 24)]), "html", null, true);
            echo "\" class=\"btn btn-link\">
                    Previous
                </a>
            </div>

            <div class=\"float-right\">
                <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show_next", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 30, $this->source); })()), "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\" class=\"btn btn-link\">
                    Next
                </a>
                <i class=\"fa fa-forward\"></i>
            </div>

        </div>

        ";
        }
        // line 39
        echo "
        <div class=\"card\">

            <a class=\"position-absolute\" style=\"right: 10px;top:10px;\"
               href=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "session", [], "any", false, false, false, 43), "get", [0 => "back"], "method", false, false, false, 43), "html", null, true);
        echo "\">
                <i class=\"fas fa-times\"></i>
            </a>

            ";
        // line 47
        $this->loadTemplate("admin/curator/_partials/curator-actions.html.twig", "admin/curator/show.html.twig", 47)->display($context);
        // line 48
        echo "
            <div class=\"card-body\">

                ";
        // line 51
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 51, $this->source); })()), "type", [], "any", false, false, false, 51), 1))) {
            // line 52
            echo "
                    ";
            // line 53
            $this->loadTemplate("admin/curator/_partials/single.html.twig", "admin/curator/show.html.twig", 53)->display($context);
            // line 54
            echo "
                ";
        } else {
            // line 56
            echo "
                    ";
            // line 57
            $this->loadTemplate("admin/curator/_partials/photo-update.html.twig", "admin/curator/show.html.twig", 57)->display($context);
            // line 58
            echo "
                ";
        }
        // line 60
        echo "            </div>
        </div>

        ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 63, $this->source); })()), "children", [], "any", false, false, false, 63));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 64
            echo "
            <div class=\"card mt-5\">

                <div class=\"card-body\">

                    ";
            // line 69
            $context["post"] = $context["child"];
            // line 70
            echo "
                    ";
            // line 71
            $this->loadTemplate("admin/curator/_partials/single.html.twig", "admin/curator/show.html.twig", 71)->display($context);
            // line 72
            echo "
                </div>

            </div>

        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "
    </div>

    <div class=\"large-image large\">
        <button class=\"close-btn\">
            <i class=\"fas fa-times\"></i>
        </button>
        <img src=\"\" alt=\"\" id=\"large-img\">
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 90
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 91
        echo "    <script>
        let img = \$('#large-img');
        let large = \$('.large');

        \$('.post').click(function (e) {
            large.addClass('show');
            img.attr('src', \$(this).data('url'))
        });

        \$('.close-btn').click(function (e) {
            img.attr('src', '');
            large.removeClass('show');
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 91,  251 => 90,  231 => 78,  212 => 72,  210 => 71,  207 => 70,  205 => 69,  198 => 64,  181 => 63,  176 => 60,  172 => 58,  170 => 57,  167 => 56,  163 => 54,  161 => 53,  158 => 52,  156 => 51,  151 => 48,  149 => 47,  142 => 43,  136 => 39,  124 => 30,  115 => 24,  108 => 19,  106 => 18,  96 => 10,  94 => 9,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}{{ post.title ?? post.slug }}{% endblock %}

{% block body %}

    <div class=\"mt-5\">

        {% include 'admin/curator/_partials/curator_nav.html.twig' %}

    </div>

    <hr>

    <div class=\"container basic-layout\">


        {% if 'pending' in app.session.get('back') %}

        <div class=\"clearfix\">

            <div class=\"float-left\">
                <i class=\"fa fa-backward\"></i>
                <a href=\"{{ path('admin_photo_show_prev', {'id': post.id}) }}\" class=\"btn btn-link\">
                    Previous
                </a>
            </div>

            <div class=\"float-right\">
                <a href=\"{{ path('admin_photo_show_next', {'id': post.id}) }}\" class=\"btn btn-link\">
                    Next
                </a>
                <i class=\"fa fa-forward\"></i>
            </div>

        </div>

        {% endif %}

        <div class=\"card\">

            <a class=\"position-absolute\" style=\"right: 10px;top:10px;\"
               href=\"{{ app.session.get('back') }}\">
                <i class=\"fas fa-times\"></i>
            </a>

            {% include 'admin/curator/_partials/curator-actions.html.twig' %}

            <div class=\"card-body\">

                {% if post.type == 1 %}

                    {% include 'admin/curator/_partials/single.html.twig' %}

                {% else %}

                    {% include 'admin/curator/_partials/photo-update.html.twig' %}

                {% endif %}
            </div>
        </div>

        {% for child in post.children %}

            <div class=\"card mt-5\">

                <div class=\"card-body\">

                    {% set post = child %}

                    {% include 'admin/curator/_partials/single.html.twig' %}

                </div>

            </div>

        {% endfor %}

    </div>

    <div class=\"large-image large\">
        <button class=\"close-btn\">
            <i class=\"fas fa-times\"></i>
        </button>
        <img src=\"\" alt=\"\" id=\"large-img\">
    </div>

{% endblock %}

{% block js %}
    <script>
        let img = \$('#large-img');
        let large = \$('.large');

        \$('.post').click(function (e) {
            large.addClass('show');
            img.attr('src', \$(this).data('url'))
        });

        \$('.close-btn').click(function (e) {
            img.attr('src', '');
            large.removeClass('show');
        })
    </script>
{% endblock %}

", "admin/curator/show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/show.html.twig");
    }
}
