<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/video.html.twig */
class __TwigTemplate_fe97f7357081fa71bc439fc5cae5019e4f1f4f1745ba79fd518232ee799b0f68 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/video.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/video.html.twig"));

        // line 1
        echo "
<div class=\"row\">

    <div class=\"col-12 text-center\">

        <div class=\"video-player\">

            <a href=\"#\">

                <video
                        id=\"my-video\"
                        class=\"video-js vjs-16-9\"
                        controls
                        width=\"640\"
                        height=\"320\"
";
        // line 17
        echo "                        data-setup='{ \"controls\": true, \"autoplay\": \"muted\", \"preload\": \"auto\", \"loop\":true, \"responsive\": true}'
                >
                    <source src=\"/videos/";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["video"]) || array_key_exists("video", $context) ? $context["video"] : (function () { throw new RuntimeError('Variable "video" does not exist.', 19, $this->source); })()), "filename", [], "any", false, false, false, 19), "html", null, true);
        echo "\" type=\"video/mp4\" />
                    <source src=\"/videos/";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["video"]) || array_key_exists("video", $context) ? $context["video"] : (function () { throw new RuntimeError('Variable "video" does not exist.', 20, $this->source); })()), "filename", [], "any", false, false, false, 20), "html", null, true);
        echo ".webm\" type=\"video/webm\" />
                </video>

                <div class=\"author\"></div>
            </a>

        </div>

        <div class=\"social\">
            <span class=\"photo-of-the-day-title\">Happy Holidays!</span>
        </div>

";
        // line 33
        echo "
        <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/video.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 33,  68 => 20,  64 => 19,  60 => 17,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<div class=\"row\">

    <div class=\"col-12 text-center\">

        <div class=\"video-player\">

            <a href=\"#\">

                <video
                        id=\"my-video\"
                        class=\"video-js vjs-16-9\"
                        controls
                        width=\"640\"
                        height=\"320\"
{#                        poster=\"/videos/{{ video.filename }}.jpg\"#}
                        data-setup='{ \"controls\": true, \"autoplay\": \"muted\", \"preload\": \"auto\", \"loop\":true, \"responsive\": true}'
                >
                    <source src=\"/videos/{{ video.filename }}\" type=\"video/mp4\" />
                    <source src=\"/videos/{{ video.filename }}.webm\" type=\"video/webm\" />
                </video>

                <div class=\"author\"></div>
            </a>

        </div>

        <div class=\"social\">
            <span class=\"photo-of-the-day-title\">Happy Holidays!</span>
        </div>

{#        <p class=\"quote text-muted mt-2\">Merry XMAS</p>#}

        <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

    </div>

</div>
", "home/video.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/video.html.twig");
    }
}
