<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/_social_icons.html.twig */
class __TwigTemplate_277072f6ee8481fd6b0ee4d5f90d8c8c8aa262994c687d72bb73de57ef91beca extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_social_icons.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_social_icons.html.twig"));

        // line 1
        echo "<div class=\"follow-us-icons\">
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Facebook\">
        <a href=\"//facebook.com/piart.community\" target=\"_blank\">
            <i class=\"fab fa-facebook-square fa-2x \"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Pinterest\">
        <a href=\"//www.pinterest.com/photoimaginart/pins/\" target=\"_blank\">
            <i class=\"fab fa-pinterest-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Twitter\">
        <a href=\"//twitter.com/photoimaginart\" target=\"_blank\">
            <i class=\"fab fa-twitter-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-offset=\"2\" data-toggle=\"tooltip\" title=\"Follow us on Instagram\">
        <a href=\"//instagram.com/photo_imaginart\" target=\"_blank\">
            <i class=\"fab fa-instagram fa-2x\"></i>
        </a>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/_social_icons.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"follow-us-icons\">
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Facebook\">
        <a href=\"//facebook.com/piart.community\" target=\"_blank\">
            <i class=\"fab fa-facebook-square fa-2x \"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Pinterest\">
        <a href=\"//www.pinterest.com/photoimaginart/pins/\" target=\"_blank\">
            <i class=\"fab fa-pinterest-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-toggle=\"tooltip\" title=\"Follow us on Twitter\">
        <a href=\"//twitter.com/photoimaginart\" target=\"_blank\">
            <i class=\"fab fa-twitter-square fa-2x\"></i>
        </a>
    </div>
    <div data-placement=\"left\" data-offset=\"2\" data-toggle=\"tooltip\" title=\"Follow us on Instagram\">
        <a href=\"//instagram.com/photo_imaginart\" target=\"_blank\">
            <i class=\"fab fa-instagram fa-2x\"></i>
        </a>
    </div>
</div>", "_partials/_social_icons.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/_social_icons.html.twig");
    }
}
