<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/pending.html.twig */
class __TwigTemplate_084edd9f498d0000ee2f606fdb341f91ceaac2e911b16aca8d3c76ce7a4c57cf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/pending.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/pending.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/pending.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Pending";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

    <div class=\"curator-page basic-layout\">

        ";
        // line 10
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/pending.html.twig", 10)->display($context);
        // line 11
        echo "
        <hr>

        ";
        // line 14
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/pending.html.twig", 14)->display($context);
        // line 15
        echo "
        <hr>

        <div class=\"d-flex justify-content-center\">

            ";
        // line 20
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 20, $this->source); })()));
        echo "

        </div>

        <div class=\"curate-photos\">
            <div class=\"photos container\">

                <div class=\"row w-100 \">

                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 29, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 30
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 text-center w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 37
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 37), "isGreyscale", [], "any", false, false, false, 37), true))) {
                // line 38
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 42
            echo "
                                    ";
            // line 44
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 44), "cover", [], "any", false, false, false, 44))) {
                // line 45
                echo "                                        ";
                $context["comments"] = 0;
                // line 46
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 46), "children", [], "any", false, false, false, 46));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 47
                    echo "                                            ";
                    $context["comments"] = ((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 47, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 47)));
                    // line 48
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "
                                        ";
                // line 50
                if ((1 === twig_compare((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 50, $this->source); })()), 0))) {
                    // line 51
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 53
                    echo twig_escape_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 53, $this->source); })()), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 57
                echo "
                                    ";
            } else {
                // line 59
                echo "
                                        ";
                // line 60
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 60), "curateComments", [], "any", false, false, false, 60)), 0))) {
                    // line 61
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 63
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 63), "curateComments", [], "any", false, false, false, 63)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 67
                echo "
                                    ";
            }
            // line 69
            echo "                                    ";
            // line 70
            echo "
                                    <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 71), "id", [], "any", false, false, false, 71)]), "html", null, true);
            echo "\">

                                        ";
            // line 73
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "type", [], "any", false, false, false, 73), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "image", [], "any", false, false, false, 73), "thumbnail", [], "any", false, false, false, 73)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 73), "cover", [], "any", false, false, false, 73), "image", [], "any", false, false, false, 73), "thumbnail", [], "any", false, false, false, 73)));
            // line 74
            echo "
                                        <img src=\"";
            // line 75
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 75, $this->source); })()), "html", null, true);
            echo "\" alt=\"\" style=\"max-width:100%;max-height: 300px\">


                                    </a>


                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    ";
            // line 87
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 87), "sentForCurateDate", [], "any", false, false, false, 87), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Rating:</strong>

                                    <strong class=\"text-success mr-1\">";
            // line 95
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "rating", [], "any", false, false, false, 95), 2), "html", null, true);
            echo "</strong>

                                    (";
            // line 97
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "total", [], "any", false, false, false, 97), "html", null, true);
            echo " votes)

                                </div>

                                ";
            // line 101
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "post", [], "any", false, false, false, 101), "type", [], "any", false, false, false, 101), 1))) {
                // line 102
                echo "
                                    <div class=\"card-footer d-flex\">

                                    <span class=\"flex-grow-1\">

                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>

                                    </span>

                                        Series

                                    </div>

                                ";
            }
            // line 117
            echo "
                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center\">

            ";
        // line 131
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 131, $this->source); })()));
        echo "

        </div>


    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/pending.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 131,  289 => 123,  278 => 117,  261 => 102,  259 => 101,  252 => 97,  247 => 95,  236 => 87,  221 => 75,  218 => 74,  216 => 73,  211 => 71,  208 => 70,  206 => 69,  202 => 67,  195 => 63,  191 => 61,  189 => 60,  186 => 59,  182 => 57,  175 => 53,  171 => 51,  169 => 50,  166 => 49,  160 => 48,  157 => 47,  152 => 46,  149 => 45,  146 => 44,  143 => 42,  137 => 38,  135 => 37,  126 => 30,  122 => 29,  110 => 20,  103 => 15,  101 => 14,  96 => 11,  94 => 10,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Pending{% endblock %}

{% block body %}


    <div class=\"curator-page basic-layout\">

        {% include 'admin/curator/_partials/curator_nav.html.twig' %}

        <hr>

        {% include 'admin/curator/_partials/filter.html.twig' %}

        <hr>

        <div class=\"d-flex justify-content-center\">

            {{ knp_pagination_render(posts) }}

        </div>

        <div class=\"curate-photos\">
            <div class=\"photos container\">

                <div class=\"row w-100 \">

                    {% for data in posts %}

                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 text-center w-100 d-flex justify-content-center align-items-center\">

                                    {% if data.post.isGreyscale == true %}
                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    {% endif %}

                                    {# Comments count START #}
                                    {% if data.post.cover is not null %}
                                        {% set comments = 0 %}
                                        {% for child in data.post.children %}
                                            {% set comments = comments + child.curateComments|length %}
                                        {% endfor %}

                                        {% if comments > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ comments }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% else %}

                                        {% if data.post.curateComments|length > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ data.post.curateComments|length }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% endif %}
                                    {# Comments count END #}

                                    <a href=\"{{ path('admin_photo_show', {id: data.post.id}) }}\">

                                        {% set image = data.post.type == 1 ? data.post.image.thumbnail : data.post.cover.image.thumbnail %}

                                        <img src=\"{{ image }}\" alt=\"\" style=\"max-width:100%;max-height: 300px\">


                                    </a>


                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    {{ data.post.sentForCurateDate | date('d/M/Y H:i') }}

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Rating:</strong>

                                    <strong class=\"text-success mr-1\">{{ data.rating | number_format(2) }}</strong>

                                    ({{ data.total }} votes)

                                </div>

                                {% if data.post.type > 1 %}

                                    <div class=\"card-footer d-flex\">

                                    <span class=\"flex-grow-1\">

                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>

                                    </span>

                                        Series

                                    </div>

                                {% endif %}

                            </div>

                        </div>

                    {% endfor %}

                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center\">

            {{ knp_pagination_render(posts) }}

        </div>


    </div>

{% endblock %}
", "admin/curator/pending.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/pending.html.twig");
    }
}
