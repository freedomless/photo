<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/analytics.html.twig */
class __TwigTemplate_d2929d8ff127b71a45c3af12f7382286c36864add11818bfd052fbd68148ddc0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/analytics.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/analytics.html.twig"));

        // line 1
        echo "<script async src=\"";
        echo twig_escape_filter($this->env, ((isset($context["google_analytics_url"]) || array_key_exists("google_analytics_url", $context) ? $context["google_analytics_url"] : (function () { throw new RuntimeError('Variable "google_analytics_url" does not exist.', 1, $this->source); })()) . (isset($context["google_analytics_id"]) || array_key_exists("google_analytics_id", $context) ? $context["google_analytics_id"] : (function () { throw new RuntimeError('Variable "google_analytics_id" does not exist.', 1, $this->source); })())), "html", null, true);
        echo "\"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["google_analytics_id"]) || array_key_exists("google_analytics_id", $context) ? $context["google_analytics_id"] : (function () { throw new RuntimeError('Variable "google_analytics_id" does not exist.', 11, $this->source); })()), "html", null, true);
        echo "');
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/analytics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 11,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script async src=\"{{ google_analytics_url ~ google_analytics_id }}\"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '{{ google_analytics_id }}');
</script>", "_partials/analytics.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/analytics.html.twig");
    }
}
