<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/nav.html.twig */
class __TwigTemplate_8b807e88e4575cd0b9864dd1a5f173c5d075db45292d382e90d71e43750808da extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/nav.html.twig"));

        // line 1
        echo "<div class=\"mt-3\">
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_popular", "_route", "active shadow"), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("photo_of_the_day_history", "_route", "active shadow"), "html", null, true);
        echo "\"
               href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_popular");
        echo "\">
                <strong>Trending</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_latest", "_route", "active shadow"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_latest");
        echo "\">
                <strong>Single</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_series", "_route", "active shadow"), "html", null, true);
        echo "\"
               href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_series");
        echo "\">
                <strong>Series</strong>
            </a>
        </li>
    </ul>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 16,  71 => 15,  61 => 10,  53 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"mt-3\">
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link {{ active('web_popular','_route','active shadow') }}{{ active('photo_of_the_day_history','_route','active shadow') }}\"
               href=\"{{ path('web_popular') }}\">
                <strong>Trending</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link {{ active('web_latest','_route','active shadow') }}\" href=\"{{ path('web_latest') }}\">
                <strong>Single</strong>
            </a>
        </li>
        <li class=\"nav-item\">
            <a class=\"nav-link {{ active('web_series','_route','active shadow') }}\"
               href=\"{{ path('web_series') }}\">
                <strong>Series</strong>
            </a>
        </li>
    </ul>
</div>
", "home/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/nav.html.twig");
    }
}
