<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/opinions.html.twig */
class __TwigTemplate_a6fb75fea14f9c2556095b7afc120d6d27a0877f66ca93de8745b24275e2eca4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/opinions.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/opinions.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "forum/opinions.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 4, $this->source); })()), "title", [], "any", false, false, false, 4), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 8
        echo "    <link href=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css\" rel=\"stylesheet\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    <div class=\"container basic-layout\">

        <div class=\"row\">

            <div class=\"col-10\">
                ";
        // line 17
        $this->loadTemplate("forum/nav.html.twig", "forum/opinions.html.twig", 17)->display(twig_array_merge($context, ["links" => [0 => ["title" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 20
(isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 20, $this->source); })()), "section", [], "any", false, false, false, 20), "name", [], "any", false, false, false, 20), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_section", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 20, $this->source); })()), "section", [], "any", false, false, false, 20), "slug", [], "any", false, false, false, 20)])], 1 => ["title" => twig_get_attribute($this->env, $this->source,         // line 21
(isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 21, $this->source); })()), "title", [], "any", false, false, false, 21), "url" => ""]]]));
        // line 23
        echo "            </div>

            <div class=\"col-2 text-right\">
                <button type=\"button\" class=\"btn btn-info mt-2\" id=\"add-reply\">Add Reply</button>
            </div>

        </div>

        <hr>

        <div class=\"row\">

            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["opinions"]) || array_key_exists("opinions", $context) ? $context["opinions"] : (function () { throw new RuntimeError('Variable "opinions" does not exist.', 35, $this->source); })()));
        foreach ($context['_seq'] as $context["index"] => $context["opinion"]) {
            // line 36
            echo "
                <div class=\"col-12\">

                    <div class=\"card\">

                        <div class=\"card-body forum-reply-body\">

                            <div class=\"row\">
                                <div class=\"col-md-4 col-lg-2 text-center\">
                                    <img class=\"shadow rounded-circle\"
                                         src=\"";
            // line 46
            echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, true, false, 46), "profile", [], "any", false, true, false, 46), "picture", [], "any", true, true, false, 46) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, true, false, 46), "profile", [], "any", false, true, false, 46), "picture", [], "any", false, false, false, 46)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, true, false, 46), "profile", [], "any", false, true, false, 46), "picture", [], "any", false, false, false, 46)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
            echo "\"
                                         alt=\"";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, false, false, 47), "username", [], "any", false, false, false, 47), "html", null, true);
            echo "\"
                                         width=\"80px\" height=\"80px\">
                                    <hr class=\"no-gutter\">
                                    <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, false, false, 50), "slug", [], "any", false, false, false, 50)]), "html", null, true);
            echo "\">
                                        ";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, false, false, 51), "profile", [], "any", false, false, false, 51), "firstName", [], "any", false, false, false, 51), "html", null, true);
            echo "
                                        ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, false, false, 52), "profile", [], "any", false, false, false, 52), "lastName", [], "any", false, false, false, 52), "html", null, true);
            echo "
                                    </a>
                                    <br>
                                    <small class=\"text-muted\">
                                        ";
            // line 56
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["opinion"], "createdAt", [], "any", false, false, false, 56), "Y-m-d H:i"), "html", null, true);
            echo "
                                    </small>
                                </div>

                                <div class=\"col-md-8 col-lg-10\">
                                    ";
            // line 61
            echo twig_trim_filter(twig_get_attribute($this->env, $this->source, $context["opinion"], "text", [], "any", false, false, false, 61));
            echo "
                                </div>
                            </div>
                        </div>

                        <div class=\"card-footer forum-reply-footer text-muted\">
                            ";
            // line 67
            if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 67, $this->source); })()), "user", [], "any", false, false, false, 67)) {
                // line 68
                echo "
                                <button class=\"btn btn-link gray-text quote\" type=\"button\"
                                        aria-expanded=\"false\"
                                        data-html=\"";
                // line 71
                echo twig_escape_filter($this->env, (("" . twig_call_macro($macros["_self"], "macro_opinionTemplate", [$context["opinion"]], 71, $context, $this->getSourceContext())) . twig_trim_filter("")), "html", null, true);
                echo "\">
                                    Quote
                                </button>

                            ";
            }
            // line 76
            echo "
                            ";
            // line 77
            if (((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 77, $this->source); })()), "user", [], "any", false, false, false, 77) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 77, $this->source); })()), "user", [], "any", false, false, false, 77), "id", [], "any", false, false, false, 77), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "user", [], "any", false, false, false, 77), "id", [], "any", false, false, false, 77)))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
                // line 78
                echo "
                                <button data-toggle=\"collapse\"
                                        data-target=\"#opinion-";
                // line 80
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 80), "html", null, true);
                echo "\"
                                        class=\"btn btn-link text-warning \">
                                    Edit
                                </button>

                                ";
                // line 85
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 85), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["opinion"], "topic", [], "any", false, false, false, 85), "opinions", [], "any", false, false, false, 85), "first", [], "any", false, false, false, 85), "id", [], "any", false, false, false, 85)))) {
                    // line 86
                    echo "                                    <button data-url=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_opinion_destroy", ["id" => twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 86)]), "html", null, true);
                    echo "\"
                                            class=\"btn btn-link text-danger delete-opinion\"
                                            data-toggle=\"modal\" data-target=\"#delete-opinion\">
                                        Delete
                                    </button>
                                ";
                }
                // line 92
                echo "
                                <div class=\"collapse edit-opinion\" id=\"opinion-";
                // line 93
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 93), "html", null, true);
                echo "\">
                                    <div class=\"card card-body\">
                                        <form action=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_opinion_update", ["id" => twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 95)]), "html", null, true);
                echo " \"
                                              method=\"post\">
                                            <textarea name=\"opinion[text]\"
                                                      id=\"";
                // line 98
                echo twig_escape_filter($this->env, ("opinion-text-" . twig_get_attribute($this->env, $this->source, $context["opinion"], "id", [], "any", false, false, false, 98)), "html", null, true);
                echo "\">
                                                ";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["opinion"], "text", [], "any", false, false, false, 99), "html", null, true);
                echo "
                                            </textarea>
                                            <div class=\"mt-3\">
                                                <button class=\"btn btn-primary\">Save</button>
                                            </div>
                                            <input type=\"hidden\" name=\"opinion[_token]\"
                                                   value=\"";
                // line 105
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("opinion"), "html", null, true);
                echo "\">
                                        </form>
                                    </div>
                                </div>

                            ";
            }
            // line 111
            echo "
                        </div>

                    </div>

                    <br>

                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['opinion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "        </div>
    </div>

    <div class=\"d-flex justify-content-center basic-layout\">
        ";
        // line 125
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["opinions"]) || array_key_exists("opinions", $context) ? $context["opinions"] : (function () { throw new RuntimeError('Variable "opinions" does not exist.', 125, $this->source); })()));
        echo "
    </div>

    ";
        // line 128
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 128, $this->source); })()), "user", [], "any", false, false, false, 128)) {
            // line 129
            echo "        <div class=\"container\">
            ";
            // line 130
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 130, $this->source); })()), 'form_start');
            echo "
            <div class=\"d-flex\">
                ";
            // line 132
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 132, $this->source); })()), "text", [], "any", false, false, false, 132), 'label');
            echo "
                <button type=\"button\" class=\"btn text-danger d-none cancel-update\" style=\"margin-top:-7px\"
                        data-toggle=\"tooltip\" data-title=\"Dismiss\">
                    <i class=\"far fa-times-circle\"></i>
                </button>
            </div>
            ";
            // line 138
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 138, $this->source); })()), "text", [], "any", false, false, false, 138), 'widget');
            echo "

            <hr>

            <div class=\"text-center mb-2\">
                <button type=\"submit\" class=\"btn btn-primary mb-3 \">Add Reply</button>
            </div>

            ";
            // line 146
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 146, $this->source); })()), 'form_end');
            echo "
        </div>
    ";
        } else {
            // line 149
            echo "        <div class=\"text-center mb-5 mt-3\">
            <a href=\"";
            // line 150
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
            echo "\" class=\"custom-button shadow gradient-dark-light-blue\">You have to be logged in
                                                                                                to give your opinion</a>
        </div>
    ";
        }
        // line 154
        echo "
    ";
        // line 155
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 155, $this->source); })()), "user", [], "any", false, false, false, 155)) {
            // line 156
            echo "        <div class=\"modal fade\" id=\"delete-opinion\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"delete-opinion\"
             aria-hidden=\"true\">
            <div class=\"modal-dialog  modal-dialog-centered\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-body\">
                        Are you sure you want to delete you reply?
                    </div>
                    <div class=\"modal-footer\">
                        <form method=\"post\" class=\"d-inline-block delete-opinion-form\">
                            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                            <button type=\"submit\" class=\"btn btn-danger\">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    ";
        }
        // line 173
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 176
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 177
        echo "    <script src=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js\"></script>

    <script>
        \$(function () {

            \$('#opinion_text').summernote({
                height: 300, popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            \$('#add-reply').on('click', function () {
                \$('html, body').animate({scrollTop: \$(document).height() - \$(window).height()});
            });

            \$('.quote').on('click', function () {
                \$('label[for=opinion_text]').text('Quote');

                \$('#opinion_text').summernote('code', \$(this).data('html'));

                \$('.cancel-update').removeClass('d-none');
                \$('form[name=opinion]').prop('action', '');
                \$('html, body').animate({scrollTop: \$(document).height() - \$(window).height()});
            });

            \$('.cancel-update').on('click', function () {
                \$('form[name=opinion]').prop('action', '');
                \$('#opinion_text').summernote('code', '')
                \$(this).addClass('d-none');
                \$('label[for=opinion_text]').text('Reply');
            });

            \$('.delete-opinion').on('click', function () {
                let url = \$(this).data('url');
                \$('.delete-opinion-form').prop('action', url)
            });

            ";
        // line 216
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 216, $this->source); })()), "text", [], "any", false, false, false, 216), "vars", [], "any", false, false, false, 216), "errors", [], "any", false, false, false, 216)), 0))) {
            // line 217
            echo "            \$('html, body').delay(100).animate({scrollTop: \$(document).height() - \$(window).height()});
            ";
        }
        // line 219
        echo "        });

        \$(function () {
            let editors = \$('.edit-opinion');
            editors.each(function (index, editor) {
                editor = \$(editor).find('textarea');
                editor.summernote({
                    height: 300, popover: {
                        image: [],
                        link: [],
                        air: []
                    }
                });
            })
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 238
    public function macro_opinionTemplate($__opinion__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "opinion" => $__opinion__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "opinionTemplate"));

            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "opinionTemplate"));

            echo "<p></p><br><div class=\"media pt-3 pl-3 pb-3 border-left mt-5\"><div class=\"media-body\"><h6 class=\"mb-3\"><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 238, $this->source); })()), "user", [], "any", false, false, false, 238), "slug", [], "any", false, false, false, 238)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 238, $this->source); })()), "user", [], "any", false, false, false, 238), "profile", [], "any", false, false, false, 238), "firstName", [], "any", false, false, false, 238), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 238, $this->source); })()), "user", [], "any", false, false, false, 238), "profile", [], "any", false, false, false, 238), "lastName", [], "any", false, false, false, 238), "html", null, true);
            echo "</a></h6><div>";
            echo twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 238, $this->source); })()), "text", [], "any", false, false, false, 238);
            echo "</div></div></div>";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

            
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "forum/opinions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 238,  444 => 219,  440 => 217,  438 => 216,  397 => 177,  387 => 176,  376 => 173,  357 => 156,  355 => 155,  352 => 154,  345 => 150,  342 => 149,  336 => 146,  325 => 138,  316 => 132,  311 => 130,  308 => 129,  306 => 128,  300 => 125,  294 => 121,  279 => 111,  270 => 105,  261 => 99,  257 => 98,  251 => 95,  246 => 93,  243 => 92,  233 => 86,  231 => 85,  223 => 80,  219 => 78,  217 => 77,  214 => 76,  206 => 71,  201 => 68,  199 => 67,  190 => 61,  182 => 56,  175 => 52,  171 => 51,  167 => 50,  161 => 47,  157 => 46,  145 => 36,  141 => 35,  127 => 23,  125 => 21,  124 => 20,  123 => 17,  116 => 12,  106 => 11,  95 => 8,  85 => 7,  72 => 4,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    {{ topic.title }}
{% endblock %}

{% block css %}
    <link href=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css\" rel=\"stylesheet\">
{% endblock %}

{% block body %}
    <div class=\"container basic-layout\">

        <div class=\"row\">

            <div class=\"col-10\">
                {% include 'forum/nav.html.twig'
                    with {'links':
                    [
                        {'title':topic.section.name, 'url': path('web_forum_section',{slug:topic.section.slug})},
                        {'title':topic.title, 'url': ''},
                    ]} %}
            </div>

            <div class=\"col-2 text-right\">
                <button type=\"button\" class=\"btn btn-info mt-2\" id=\"add-reply\">Add Reply</button>
            </div>

        </div>

        <hr>

        <div class=\"row\">

            {% for index,opinion in opinions %}

                <div class=\"col-12\">

                    <div class=\"card\">

                        <div class=\"card-body forum-reply-body\">

                            <div class=\"row\">
                                <div class=\"col-md-4 col-lg-2 text-center\">
                                    <img class=\"shadow rounded-circle\"
                                         src=\"{{ opinion.user.profile.picture ?? asset('images/avatar.jpg') }}\"
                                         alt=\"{{ opinion.user.username }}\"
                                         width=\"80px\" height=\"80px\">
                                    <hr class=\"no-gutter\">
                                    <a href=\"{{ path('web_profile',{slug:opinion.user.slug}) }}\">
                                        {{ opinion.user.profile.firstName }}
                                        {{ opinion.user.profile.lastName }}
                                    </a>
                                    <br>
                                    <small class=\"text-muted\">
                                        {{ opinion.createdAt | date('Y-m-d H:i') }}
                                    </small>
                                </div>

                                <div class=\"col-md-8 col-lg-10\">
                                    {{ (opinion.text | trim) | raw }}
                                </div>
                            </div>
                        </div>

                        <div class=\"card-footer forum-reply-footer text-muted\">
                            {% if app.user %}

                                <button class=\"btn btn-link gray-text quote\" type=\"button\"
                                        aria-expanded=\"false\"
                                        data-html=\"{{ ''~_self.opinionTemplate(opinion)~ '' | trim }}\">
                                    Quote
                                </button>

                            {% endif %}

                            {% if app.user and app.user.id == opinion.user.id or is_granted('ROLE_SUPER_ADMIN') %}

                                <button data-toggle=\"collapse\"
                                        data-target=\"#opinion-{{ opinion.id }}\"
                                        class=\"btn btn-link text-warning \">
                                    Edit
                                </button>

                                {% if opinion.id != opinion.topic.opinions.first.id %}
                                    <button data-url=\"{{ path('web_forum_opinion_destroy',{id: opinion.id}) }}\"
                                            class=\"btn btn-link text-danger delete-opinion\"
                                            data-toggle=\"modal\" data-target=\"#delete-opinion\">
                                        Delete
                                    </button>
                                {% endif %}

                                <div class=\"collapse edit-opinion\" id=\"opinion-{{ opinion.id }}\">
                                    <div class=\"card card-body\">
                                        <form action=\"{{ path('web_forum_opinion_update',{id:opinion.id}) }} \"
                                              method=\"post\">
                                            <textarea name=\"opinion[text]\"
                                                      id=\"{{ 'opinion-text-' ~ opinion.id }}\">
                                                {{ opinion.text }}
                                            </textarea>
                                            <div class=\"mt-3\">
                                                <button class=\"btn btn-primary\">Save</button>
                                            </div>
                                            <input type=\"hidden\" name=\"opinion[_token]\"
                                                   value=\"{{ csrf_token('opinion') }}\">
                                        </form>
                                    </div>
                                </div>

                            {% endif %}

                        </div>

                    </div>

                    <br>

                </div>

            {% endfor %}
        </div>
    </div>

    <div class=\"d-flex justify-content-center basic-layout\">
        {{ knp_pagination_render(opinions) }}
    </div>

    {% if app.user %}
        <div class=\"container\">
            {{ form_start(form) }}
            <div class=\"d-flex\">
                {{ form_label(form.text) }}
                <button type=\"button\" class=\"btn text-danger d-none cancel-update\" style=\"margin-top:-7px\"
                        data-toggle=\"tooltip\" data-title=\"Dismiss\">
                    <i class=\"far fa-times-circle\"></i>
                </button>
            </div>
            {{ form_widget(form.text) }}

            <hr>

            <div class=\"text-center mb-2\">
                <button type=\"submit\" class=\"btn btn-primary mb-3 \">Add Reply</button>
            </div>

            {{ form_end(form) }}
        </div>
    {% else %}
        <div class=\"text-center mb-5 mt-3\">
            <a href=\"{{ path('web_login') }}\" class=\"custom-button shadow gradient-dark-light-blue\">You have to be logged in
                                                                                                to give your opinion</a>
        </div>
    {% endif %}

    {% if app.user %}
        <div class=\"modal fade\" id=\"delete-opinion\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"delete-opinion\"
             aria-hidden=\"true\">
            <div class=\"modal-dialog  modal-dialog-centered\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-body\">
                        Are you sure you want to delete you reply?
                    </div>
                    <div class=\"modal-footer\">
                        <form method=\"post\" class=\"d-inline-block delete-opinion-form\">
                            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                            <button type=\"submit\" class=\"btn btn-danger\">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {% endif %}

{% endblock %}

{% block js %}
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js\"></script>

    <script>
        \$(function () {

            \$('#opinion_text').summernote({
                height: 300, popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            \$('#add-reply').on('click', function () {
                \$('html, body').animate({scrollTop: \$(document).height() - \$(window).height()});
            });

            \$('.quote').on('click', function () {
                \$('label[for=opinion_text]').text('Quote');

                \$('#opinion_text').summernote('code', \$(this).data('html'));

                \$('.cancel-update').removeClass('d-none');
                \$('form[name=opinion]').prop('action', '');
                \$('html, body').animate({scrollTop: \$(document).height() - \$(window).height()});
            });

            \$('.cancel-update').on('click', function () {
                \$('form[name=opinion]').prop('action', '');
                \$('#opinion_text').summernote('code', '')
                \$(this).addClass('d-none');
                \$('label[for=opinion_text]').text('Reply');
            });

            \$('.delete-opinion').on('click', function () {
                let url = \$(this).data('url');
                \$('.delete-opinion-form').prop('action', url)
            });

            {% if form.text.vars.errors | length > 0 %}
            \$('html, body').delay(100).animate({scrollTop: \$(document).height() - \$(window).height()});
            {% endif %}
        });

        \$(function () {
            let editors = \$('.edit-opinion');
            editors.each(function (index, editor) {
                editor = \$(editor).find('textarea');
                editor.summernote({
                    height: 300, popover: {
                        image: [],
                        link: [],
                        air: []
                    }
                });
            })
        })
    </script>
{% endblock %}

{# Do not make multiple line #}
{% macro opinionTemplate(opinion) %}<p></p><br><div class=\"media pt-3 pl-3 pb-3 border-left mt-5\"><div class=\"media-body\"><h6 class=\"mb-3\"><a href=\"{{ path('web_profile',{slug:opinion.user.slug}) }}\">{{ opinion.user.profile.firstName }}{{ opinion.user.profile.lastName }}</a></h6><div>{{ opinion.text | raw }}</div></div></div>{% endmacro %}
", "forum/opinions.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/forum/opinions.html.twig");
    }
}
