<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/single.html.twig */
class __TwigTemplate_26f50847e11438f64f824bc6265560b7d0c37727e1d60d63dbceb579648d23fd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/single.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/single.html.twig"));

        // line 1
        echo "<div class=\"row  flex-sm-column-reverse flex-md-row\">

    <div class=\"col-md-4 col-sm-12\">


        ";
        // line 6
        $this->loadTemplate("admin/curator/_partials/photo-update.html.twig", "admin/curator/_partials/single.html.twig", 6)->display($context);
        // line 7
        echo "
        ";
        // line 8
        $this->loadTemplate("admin/curator/_partials/reject-reasons.html.twig", "admin/curator/_partials/single.html.twig", 8)->display($context);
        // line 9
        echo "        ";
        $this->loadTemplate("admin/curator/_partials/curator-votes.html.twig", "admin/curator/_partials/single.html.twig", 9)->display($context);
        // line 10
        echo "
        ";
        // line 11
        $this->loadTemplate("admin/curator/_partials/curator-comment.html.twig", "admin/curator/_partials/single.html.twig", 11)->display($context);
        // line 12
        echo "
        ";
        // line 13
        if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 13, $this->source); })()), 2) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 13, $this->source); })()), 3))) {
            // line 14
            echo "
            ";
            // line 15
            $this->loadTemplate("admin/curator/_partials/curated-by.html.twig", "admin/curator/_partials/single.html.twig", 15)->display($context);
            // line 16
            echo "
        ";
        }
        // line 18
        echo "
        ";
        // line 19
        $this->loadTemplate("admin/curator/_partials/photo-details.html.twig", "admin/curator/_partials/single.html.twig", 19)->display($context);
        // line 20
        echo "


    </div>

    <div class=\"col-md-8 col-sm-12\">

        <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 27, $this->source); })()), "image", [], "any", false, false, false, 27), "small", [], "any", false, false, false, 27), "html", null, true);
        echo "\" class=\"post\" style=\"cursor: zoom-in;\" data-url=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 27, $this->source); })()), "image", [], "any", false, false, false, 27), "large", [], "any", false, false, false, 27), "html", null, true);
        echo "\"
             alt=\"";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "title", [], "any", false, false, false, 28), "html", null, true);
        echo "\" width=\"100%\">

        <div class=\"text-center\">
            <h4>
                ";
        // line 32
        if (twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 32, $this->source); })()), "title", [], "any", false, false, false, 32)) {
            // line 33
            echo "                    ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 33, $this->source); })()), "title", [], "any", false, false, false, 33), "html", null, true);
            echo "
                ";
        } else {
            // line 35
            echo "                    <span class=\"text-muted\">untitled</span>
                ";
        }
        // line 37
        echo "            </h4>
        </div>

        ";
        // line 40
        $this->loadTemplate("admin/curator/_partials/author-details.html.twig", "admin/curator/_partials/single.html.twig", 40)->display($context);
        // line 41
        echo "
    </div>


</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/single.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 41,  123 => 40,  118 => 37,  114 => 35,  108 => 33,  106 => 32,  99 => 28,  93 => 27,  84 => 20,  82 => 19,  79 => 18,  75 => 16,  73 => 15,  70 => 14,  68 => 13,  65 => 12,  63 => 11,  60 => 10,  57 => 9,  55 => 8,  52 => 7,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row  flex-sm-column-reverse flex-md-row\">

    <div class=\"col-md-4 col-sm-12\">


        {% include 'admin/curator/_partials/photo-update.html.twig' %}

        {% include 'admin/curator/_partials/reject-reasons.html.twig' %}
        {% include 'admin/curator/_partials/curator-votes.html.twig' %}

        {% include 'admin/curator/_partials/curator-comment.html.twig' %}

        {% if workflow_has_marked_place(post,2) or workflow_has_marked_place(post,3) %}

            {% include 'admin/curator/_partials/curated-by.html.twig' %}

        {% endif %}

        {% include 'admin/curator/_partials/photo-details.html.twig' %}



    </div>

    <div class=\"col-md-8 col-sm-12\">

        <img src=\"{{ post.image.small }}\" class=\"post\" style=\"cursor: zoom-in;\" data-url=\"{{ post.image.large }}\"
             alt=\"{{ post.title }}\" width=\"100%\">

        <div class=\"text-center\">
            <h4>
                {% if post.title %}
                    {{ post.title }}
                {% else %}
                    <span class=\"text-muted\">untitled</span>
                {% endif %}
            </h4>
        </div>

        {% include 'admin/curator/_partials/author-details.html.twig' %}

    </div>


</div>
", "admin/curator/_partials/single.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/single.html.twig");
    }
}
