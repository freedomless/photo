<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload.html.twig */
class __TwigTemplate_7dde2047e6cb193b13173a23a31cc6c630dacf376236c36cbe206e5d96a22a72 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Upload Photo";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7), "isVerified", [], "any", false, false, false, 7), false))) {
            // line 8
            echo "
        ";
            // line 9
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/upload.html.twig", 9)->display($context);
            // line 10
            echo "
    ";
        } else {
            // line 12
            echo "
        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    ";
            // line 19
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), 'form_start');
            echo "

                    <h3>";
            // line 21
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "vars", [], "any", false, false, false, 21), "method", [], "any", false, false, false, 21), "POST"))) ? ("Upload") : ("Update"));
            echo " Photo </h3>

                    <hr>

                    ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), 'errors');
            echo "

                    <div class=\"row mb-5\">

                        <div class=\"col-xl-4\">

                            ";
            // line 31
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "vars", [], "any", false, false, false, 31), "method", [], "any", false, false, false, 31), "POST"))) {
                // line 32
                echo "
                                ";
                // line 33
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "image", [], "any", false, false, false, 33), "file", [], "any", false, false, false, 33), 'widget', ["attr" => ["data-label" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "image", [], "any", false, false, false, 33), "file", [], "any", false, false, false, 33), "vars", [], "any", false, false, false, 33), "label", [], "any", false, false, false, 33)]]);
                echo "

                                ";
                // line 35
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 35, $this->source); })()), "image", [], "any", false, false, false, 35), "file", [], "any", false, false, false, 35), 'errors');
                echo "

                                <div class=\"mt-3\">

                                    <ul class=\"list-unstyled small\">

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Min Res.: 1000px per one of the sides
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Max file size: 30 MB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Format JPEG sRGB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No watermarks or signatures on the
                                                                                   image
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No borders on the image
                                        </li>

                                    </ul>

                                </div>

                            ";
            } else {
                // line 67
                echo "
                                <img src=\"";
                // line 68
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 68, $this->source); })()), "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 68, $this->source); })()), "title", [], "any", false, false, false, 68), "html", null, true);
                echo "\" width=\"100%\">

                                ";
                // line 70
                if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 70, $this->source); })()), "to_curate") || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 70, $this->source); })()), 2)) && (null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 70, $this->source); })()), "parent", [], "any", false, false, false, 70)))) {
                    // line 71
                    echo "
                                    <button type=\"button\"
                                            class=\"btn btn-danger warning-action mt-3\"
                                            data-url=\"";
                    // line 74
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 74, $this->source); })()), "id", [], "any", false, false, false, 74)]), "html", null, true);
                    echo "\">
                                        Delete
                                    </button>

                                ";
                }
                // line 79
                echo "
                            ";
            }
            // line 81
            echo "
                        </div>

                        <div class=\"col-xl-8\">

                            <div class=\"form-group row  pl-3 pr-3\">

                                ";
            // line 88
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 88, $this->source); })()), "category", [], "any", false, false, false, 88), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 89
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 89, $this->source); })()), "category", [], "any", false, false, false, 89), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 95
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 95, $this->source); })()), "title", [], "any", false, false, false, 95), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 96
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 96, $this->source); })()), "title", [], "any", false, false, false, 96), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 102
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 102, $this->source); })()), "tags", [], "any", false, false, false, 102), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 103
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 103, $this->source); })()), "tags", [], "any", false, false, false, 103), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 109
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 109, $this->source); })()), "description", [], "any", false, false, false, 109), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 110
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 110, $this->source); })()), "description", [], "any", false, false, false, 110), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"row pl-3 pr-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-9\">

                                    <div>

                                        ";
            // line 122
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 122, $this->source); })()), "isNude", [], "any", false, false, false, 122), 'widget');
            echo "

                                    </div>

                                    <div>

                                        ";
            // line 128
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 128, $this->source); })()), "isGreyscale", [], "any", false, false, false, 128), 'widget');
            echo "

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <h3 class=\"text-muted\">Optional Details:</h3>

                    <hr>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 146
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 146, $this->source); })()), "image", [], "any", false, false, false, 146), "camera", [], "any", false, false, false, 146), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 148
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 148, $this->source); })()), "image", [], "any", false, false, false, 148), "lens", [], "any", false, false, false, 148), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 150
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 150, $this->source); })()), "image", [], "any", false, false, false, 150), "focalLength", [], "any", false, false, false, 150), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 152
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 152, $this->source); })()), "image", [], "any", false, false, false, 152), "shutterSpeed", [], "any", false, false, false, 152), 'row');
            echo "</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 158
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 158, $this->source); })()), "image", [], "any", false, false, false, 158), "ISO", [], "any", false, false, false, 158), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 160
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 160, $this->source); })()), "image", [], "any", false, false, false, 160), "exposure", [], "any", false, false, false, 160), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 162
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 162, $this->source); })()), "image", [], "any", false, false, false, 162), "flash", [], "any", false, false, false, 162), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 164
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 164, $this->source); })()), "image", [], "any", false, false, false, 164), "filter", [], "any", false, false, false, 164), 'row');
            echo "</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">";
            // line 170
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 170, $this->source); })()), "image", [], "any", false, false, false, 170), "tripod", [], "any", false, false, false, 170), 'row');
            echo "</div>

                        <div class=\"col-md-3\">";
            // line 172
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 172, $this->source); })()), "image", [], "any", false, false, false, 172), "aperture", [], "any", false, false, false, 172), 'row');
            echo "</div>

                    </div>

                    <div class=\"text-center mt-5\">

                        ";
            // line 178
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 178, $this->source); })()), "upload", [], "any", false, false, false, 178), 'widget');
            echo "

                    </div>

                    ";
            // line 182
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 182, $this->source); })()), 'form_end');
            echo "

                </div>

            </div>

        </div>

    ";
        }
        // line 191
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 194
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 195
        echo "
    ";
        // line 196
        $context["message"] = "Are you sure you want to delete this photo permantly?";
        // line 197
        echo "
    ";
        // line 198
        $this->loadTemplate("_partials/warning-modal.html.twig", "photo/upload.html.twig", 198)->display($context);
        // line 199
        echo "
    <script>
        \$(function () {
            \$('.custom-file').dropzone({form: \$('form')});
        })
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "photo/upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 199,  414 => 198,  411 => 197,  409 => 196,  406 => 195,  396 => 194,  385 => 191,  373 => 182,  366 => 178,  357 => 172,  352 => 170,  343 => 164,  338 => 162,  333 => 160,  328 => 158,  319 => 152,  314 => 150,  309 => 148,  304 => 146,  283 => 128,  274 => 122,  259 => 110,  255 => 109,  246 => 103,  242 => 102,  233 => 96,  229 => 95,  220 => 89,  216 => 88,  207 => 81,  203 => 79,  195 => 74,  190 => 71,  188 => 70,  181 => 68,  178 => 67,  143 => 35,  138 => 33,  135 => 32,  133 => 31,  124 => 25,  117 => 21,  112 => 19,  103 => 12,  99 => 10,  97 => 9,  94 => 8,  92 => 7,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Upload Photo{% endblock %}

{% block body %}

    {% if app.user.isVerified == false %}

        {% include '_partials/resent-confirmation.html.twig' %}

    {% else %}

        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    {{ form_start(form) }}

                    <h3>{{ form.vars.method == 'POST' ? 'Upload' : 'Update' }} Photo </h3>

                    <hr>

                    {{ form_errors(form) }}

                    <div class=\"row mb-5\">

                        <div class=\"col-xl-4\">

                            {% if form.vars.method == 'POST' %}

                                {{ form_widget(form.image.file,{attr:{'data-label':form.image.file.vars.label}}) }}

                                {{ form_errors(form.image.file) }}

                                <div class=\"mt-3\">

                                    <ul class=\"list-unstyled small\">

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Min Res.: 1000px per one of the sides
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Max file size: 30 MB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> Format JPEG sRGB
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No watermarks or signatures on the
                                                                                   image
                                        </li>

                                        <li>
                                            <strong class=\"text-danger\">*</strong> No borders on the image
                                        </li>

                                    </ul>

                                </div>

                            {% else %}

                                <img src=\"{{ post.image.thumbnail }}\" alt=\"{{ post.title }}\" width=\"100%\">

                                {% if (workflow_can(post,'to_curate') or workflow_has_marked_place(post,2)) and post.parent is null %}

                                    <button type=\"button\"
                                            class=\"btn btn-danger warning-action mt-3\"
                                            data-url=\"{{ path('web_photo_delete',{id:post.id}) }}\">
                                        Delete
                                    </button>

                                {% endif %}

                            {% endif %}

                        </div>

                        <div class=\"col-xl-8\">

                            <div class=\"form-group row  pl-3 pr-3\">

                                {{ form_label(form.category, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.category,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.title, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.title,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.tags, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.tags,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.description, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.description,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"row pl-3 pr-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-9\">

                                    <div>

                                        {{ form_widget(form.isNude) }}

                                    </div>

                                    <div>

                                        {{ form_widget(form.isGreyscale) }}

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <h3 class=\"text-muted\">Optional Details:</h3>

                    <hr>

                    <div class=\"row\">

                        <div class=\"col-md-3\">{{ form_row(form.image.camera) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.lens) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.focalLength) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.shutterSpeed) }}</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">{{ form_row(form.image.ISO) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.exposure) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.flash) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.filter) }}</div>

                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-3\">{{ form_row(form.image.tripod) }}</div>

                        <div class=\"col-md-3\">{{ form_row(form.image.aperture) }}</div>

                    </div>

                    <div class=\"text-center mt-5\">

                        {{ form_widget(form.upload) }}

                    </div>

                    {{ form_end(form) }}

                </div>

            </div>

        </div>

    {% endif %}

{% endblock %}

{% block js %}

    {% set message = 'Are you sure you want to delete this photo permantly?' %}

    {% include '_partials/warning-modal.html.twig' %}

    <script>
        \$(function () {
            \$('.custom-file').dropzone({form: \$('form')});
        })
    </script>

{% endblock %}
", "photo/upload.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload.html.twig");
    }
}
