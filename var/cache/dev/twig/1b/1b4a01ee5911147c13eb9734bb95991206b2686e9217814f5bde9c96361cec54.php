<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_social.html.twig */
class __TwigTemplate_07fb9b549bb0bb8c6f761ee9062786be20c9f0fbc22ad8704a1e1bb38a7f6edb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_social.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_social.html.twig"));

        // line 1
        echo "Follow us on:
<a href=\"https://facebook.com/piart.community\" target=\"_blank\" title=\"Follow us on Facebook\">Facebook</a> |
<a href=\"https://www.pinterest.com/photoimaginart/pins/\" target=\"_blank\" title=\"Follow us on Pinterest\">Pinterest</a> |
<a href=\"https://twitter.com/photoimaginart\" target=\"_blank\" title=\"Follow us on Twitter\">Twitter</a> |
<a href=\"https://instagram.com/photo_imaginart\" target=\"_blank\" title=\"Follow us on Instagram\">Instagram</a>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "emails/_social.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("Follow us on:
<a href=\"https://facebook.com/piart.community\" target=\"_blank\" title=\"Follow us on Facebook\">Facebook</a> |
<a href=\"https://www.pinterest.com/photoimaginart/pins/\" target=\"_blank\" title=\"Follow us on Pinterest\">Pinterest</a> |
<a href=\"https://twitter.com/photoimaginart\" target=\"_blank\" title=\"Follow us on Twitter\">Twitter</a> |
<a href=\"https://instagram.com/photo_imaginart\" target=\"_blank\" title=\"Follow us on Instagram\">Instagram</a>", "emails/_social.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_social.html.twig");
    }
}
