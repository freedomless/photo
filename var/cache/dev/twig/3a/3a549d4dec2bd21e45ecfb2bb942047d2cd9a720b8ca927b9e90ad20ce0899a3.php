<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload-print.html.twig */
class __TwigTemplate_71f57c303cafa2bcc67664a151c6fc07c934bed64bd72b20b00369aa436e4c1d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload-print.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload-print.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload-print.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Product";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
    ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "session", [], "any", false, false, false, 8), "set", [0 => "total", 1 => 0], "method", false, false, false, 8), "html", null, true);
        echo "

    <div class=\"container basic-layout\">

        <div class=\"upload-single\">

            <div class=\"upload pt-5\">

                <h3 class=\"d-flex\">
                    <span class=\"flex-grow-1\">Product</span>
                    ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "request", [], "any", false, false, false, 18), "session", [], "any", false, false, false, 18), "get", [0 => "product-back"], "method", false, false, false, 18)) {
            // line 19
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "request", [], "any", false, false, false, 19), "session", [], "any", false, false, false, 19), "get", [0 => "product-back"], "method", false, false, false, 19), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "request", [], "any", false, false, false, 19), "session", [], "any", false, false, false, 19), "get", [0 => "product-back-arguments"], "method", false, false, false, 19)), "html", null, true);
            echo "\">
                            <i class=\"fa fa-times\"></i>
                        </a>
                    ";
        }
        // line 23
        echo "
                </h3>
                <hr>

                ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), 'form_start');
        echo "

                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), 'errors');
        echo "

                ";
        // line 31
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 31, $this->source); })()), "request", [], "any", false, false, false, 31), "get", [0 => "type"], "method", false, false, false, 31), "single"))) {
            // line 32
            echo "
                    <div class=\"row\">

                        <div class=\"col-4\"></div>

                        ";
            // line 37
            echo twig_call_macro($macros["_self"], "macro_upload", [(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })()), (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 37, $this->source); })()), (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 37, $this->source); })())], 37, $context, $this->getSourceContext());
            echo "

                    </div>


                ";
        } else {
            // line 43
            echo "

                    <div class=\"row\">

                        ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 47, $this->source); })()), "children", [], "any", false, false, false, 47));
            foreach ($context['_seq'] as $context["index"] => $context["f"]) {
                // line 48
                echo "
                            ";
                // line 49
                echo twig_call_macro($macros["_self"], "macro_upload", [$context["f"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 49, $this->source); })()), "children", [], "any", false, false, false, 49), $context["index"], [], "array", false, false, false, 49), (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 49, $this->source); })())], 49, $context, $this->getSourceContext());
                echo "

                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['f'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "
                    </div>


                ";
        }
        // line 57
        echo "
                <div class=\"text-center\">
                    ";
        // line 59
        if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 59, $this->source); })()), "request", [], "any", false, false, false, 59), "attributes", [], "any", false, false, false, 59), "get", [0 => "type"], "method", false, false, false, 59), "series")) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 59, $this->source); })()), "status", [], "any", false, false, false, 59), 3)))) {
            // line 60
            echo "                        <hr>
                        ";
            // line 61
            if (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 61, $this->source); })()), "newCommission", [], "any", false, false, false, 61)) {
                // line 62
                echo "                            <p class=\"text-info\">Your series price will be
                                                 changed on ";
                // line 63
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 63, $this->source); })()), "commissionChangingOn", [], "any", false, false, false, 63), "d M H:iA"), "html", null, true);
                echo "
                                                 to ";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 64, $this->source); })()), "newCommission", [], "any", false, false, false, 64), "html", null, true);
                echo " &euro;.</p>
                        ";
            }
            // line 66
            echo "
                        <h4 class=\"text-muted\">Price <strong>";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 67, $this->source); })()), "commission", [], "any", false, false, false, 67), "html", null, true);
            echo " &euro;</strong></h4>
                        <hr>
                    ";
        }
        // line 70
        echo "
                </div>

                <div class=\"text-center mt-5\">

                    ";
        // line 75
        if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 75, $this->source); })()), "session", [], "any", false, false, false, 75), "get", [0 => "total", 1 => 0], "method", false, false, false, 75), 0))) {
            // line 76
            echo "                        <button class=\"btn btn-blue\">Save</button>
                    ";
        }
        // line 78
        echo "
                    <div>
                        <a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage", ["type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 80, $this->source); })()), "request", [], "any", false, false, false, 80), "attributes", [], "any", false, false, false, 80), "get", [0 => "type"], "method", false, false, false, 80)]), "html", null, true);
        echo "\"
                           class=\"btn\">Back to
                                       Manage ";
        // line 82
        echo (((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 82, $this->source); })()), "request", [], "any", false, false, false, 82), "attributes", [], "any", false, false, false, 82), "get", [0 => "type"], "method", false, false, false, 82), "series"))) ? ("Photos") : ("Series"));
        echo " </a>
                    </div>

                </div>

                <div class=\"d-none\">
                    ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 88, $this->source); })()), 'rest');
        echo "
                </div>

                ";
        // line 91
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 91, $this->source); })()), 'form_end');
        echo "


            </div>

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 102
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 103
        echo "    <script>
        \$(function () {
            \$('.custom-file-input').on('change', function () {

                let \$label = \$(this).parent().find('label');

                \$label.addClass('text-left')
                \$label.text('Added')

            })
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 117
    public function macro_upload($__form__ = null, $__product__ = null, $__configuration__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "product" => $__product__,
            "configuration" => $__configuration__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "upload"));

            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "upload"));

            // line 118
            echo "
    <div class=\"col-4 mb-4 d-flex\">

        <div class=\"card p-0\">

            <div class=\"card-body p-0\">

                ";
            // line 125
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 125, $this->source); })()), 1)) {
                // line 126
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 132
            echo "
                ";
            // line 133
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 133, $this->source); })()), 2)) {
                // line 134
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 140
            echo "
                ";
            // line 141
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 141, $this->source); })()), 3)) {
                // line 142
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                ";
            }
            // line 148
            echo "
                <img src=\"";
            // line 149
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 149, $this->source); })()), "post", [], "any", false, false, false, 149), "image", [], "any", false, false, false, 149), "thumbnail", [], "any", false, false, false, 149), "html", null, true);
            echo "\" alt=\"\"
                     style=\"object-fit: cover\" width=\"100%\" height=\"300px\">
            </div>

            ";
            // line 153
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 153, $this->source); })()), "post", [], "any", false, false, false, 153), "image", [], "any", false, false, false, 153), "print", [], "any", false, false, false, 153))) {
                // line 154
                echo "
                <div class=\"card-footer\">

                    <p class=\"text-success m-0\">Print file uploaded!</p>

                </div>

            ";
            }
            // line 162
            echo "
            ";
            // line 163
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "post", [], "any", true, true, false, 163)) {
                // line 164
                echo "
                ";
                // line 165
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 165, $this->source); })()), "session", [], "any", false, false, false, 165), "set", [0 => "total", 1 => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 165, $this->source); })()), "session", [], "any", false, false, false, 165), "get", [0 => "total", 1 => 0], "method", false, false, false, 165) + 1)], "method", false, false, false, 165), "html", null, true);
                echo "


                <div class=\"card-footer\">

                    ";
                // line 170
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 170, $this->source); })()), "post", [], "any", false, false, false, 170), "image", [], "any", false, false, false, 170), "file", [], "any", false, false, false, 170), 'errors');
                echo "

                    <small class=\"text-muted\">";
                // line 172
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, "Print File"), "html", null, true);
                echo " <span class=\"text-danger\">*</span></small>

                    ";
                // line 174
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 174, $this->source); })()), "post", [], "any", false, false, false, 174), "image", [], "any", false, false, false, 174), "file", [], "any", false, false, false, 174), 'widget', ["attr" => ["class" => "product"]]);
                echo "

                    <small class=\"text-muted\">Print width and height should be >= ";
                // line 176
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 176, $this->source); })()), "minPrintSize", [], "any", false, false, false, 176), "html", null, true);
                echo "
                                              pixels</small>


                </div>

            ";
            }
            // line 183
            echo "
            ";
            // line 184
            $context["eligible"] = (0 === twig_compare($this->extensions['App\Twig\ProductNeedPrintExtension']->doSomething(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 184, $this->source); })()), "post", [], "any", false, false, false, 184), "image", [], "any", false, false, false, 184)), false));
            // line 185
            echo "
            ";
            // line 186
            if (((null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 186, $this->source); })()), "post", [], "any", false, false, false, 186), "image", [], "any", false, false, false, 186), "print", [], "any", false, false, false, 186)) && (isset($context["eligible"]) || array_key_exists("eligible", $context) ? $context["eligible"] : (function () { throw new RuntimeError('Variable "eligible" does not exist.', 186, $this->source); })()))) {
                // line 187
                echo "
                <div class=\"card-footer\">

                    <div class=\"d-flex text-success\">

                       <span class=\"flex-grow-1  text-success\">

                          Your current file size is eligible for store no need to upload file.

                       </span>

                    </div>

                </div>

            ";
            }
            // line 203
            echo "
            <div class=\"card-footer\">

                ";
            // line 206
            if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "commission", [], "any", true, true, false, 206)) {
                // line 207
                echo "
                    ";
                // line 208
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 208, $this->source); })()), "session", [], "any", false, false, false, 208), "set", [0 => "total", 1 => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 208, $this->source); })()), "session", [], "any", false, false, false, 208), "get", [0 => "total", 1 => 0], "method", false, false, false, 208) + 1)], "method", false, false, false, 208), "html", null, true);
                echo "

                    <small class=\"text-muted\"> ";
                // line 210
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, "commission in euro €"), "html", null, true);
                echo ": <span class=\"text-danger\">*</span></small>

                    ";
                // line 212
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 212, $this->source); })()), "commission", [], "any", false, false, false, 212), 'widget');
                echo "

                    <small class=\"text-muted\">Range ";
                // line 214
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 214, $this->source); })()), "minCommission", [], "any", false, false, false, 214), "html", null, true);
                echo "
                                              - ";
                // line 215
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 215, $this->source); })()), "maxCommission", [], "any", false, false, false, 215), "html", null, true);
                echo "
                        ";
                // line 216
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 216, $this->source); })()), "currency", [], "any", false, false, false, 216), "html", null, true);
                echo "
                    </small>

                    ";
                // line 219
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 219, $this->source); })()), "commission", [], "any", false, false, false, 219), 'errors');
                echo "

                ";
            } else {
                // line 222
                echo "
                    ";
                // line 223
                if ((twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "priceUpdateInterval", [], "any", true, true, false, 223) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 223, $this->source); })()), "priceUpdateInterval", [], "any", false, false, false, 223), 0)))) {
                    // line 224
                    echo "
                        <p class=\"text-info\">You will be able to change the price
                                             on: ";
                    // line 226
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 226, $this->source); })()), "priceChangedAt", [], "any", false, false, false, 226), "d M H:iA"), "html", null, true);
                    echo ".</p>


                    ";
                } else {
                    // line 230
                    echo "
                        <p class=\"text-muted\">Commission
                            <strong>";
                    // line 232
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 232, $this->source); })()), "commission", [], "any", false, false, false, 232), 2), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 232, $this->source); })()), "currency", [], "any", false, false, false, 232), "html", null, true);
                    echo "</strong>
                        </p>

                    ";
                }
                // line 236
                echo "
                    ";
                // line 237
                if (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 237, $this->source); })()), "newCommission", [], "any", false, false, false, 237)) {
                    // line 238
                    echo "                        <p class=\"text-info\">Your price will be
                                             changed on ";
                    // line 239
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 239, $this->source); })()), "commissionChangingOn", [], "any", false, false, false, 239), "d M H:iA"), "html", null, true);
                    echo "
                                             to ";
                    // line 240
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 240, $this->source); })()), "newCommission", [], "any", false, false, false, 240), "html", null, true);
                    echo " &euro;.</p>
                    ";
                }
                // line 242
                echo "
                    <p class=\"text-muted\">Price <strong>";
                // line 243
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 243, $this->source); })()), "commission", [], "any", false, false, false, 243), "html", null, true);
                echo " &euro;</strong></p>


                ";
            }
            // line 247
            echo "
            </div>

        </div>

    </div>

";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

            
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "photo/upload-print.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  558 => 247,  551 => 243,  548 => 242,  543 => 240,  539 => 239,  536 => 238,  534 => 237,  531 => 236,  522 => 232,  518 => 230,  511 => 226,  507 => 224,  505 => 223,  502 => 222,  496 => 219,  490 => 216,  486 => 215,  482 => 214,  477 => 212,  472 => 210,  467 => 208,  464 => 207,  462 => 206,  457 => 203,  439 => 187,  437 => 186,  434 => 185,  432 => 184,  429 => 183,  419 => 176,  414 => 174,  409 => 172,  404 => 170,  396 => 165,  393 => 164,  391 => 163,  388 => 162,  378 => 154,  376 => 153,  369 => 149,  366 => 148,  358 => 142,  356 => 141,  353 => 140,  345 => 134,  343 => 133,  340 => 132,  332 => 126,  330 => 125,  321 => 118,  300 => 117,  278 => 103,  268 => 102,  248 => 91,  242 => 88,  233 => 82,  228 => 80,  224 => 78,  220 => 76,  218 => 75,  211 => 70,  205 => 67,  202 => 66,  197 => 64,  193 => 63,  190 => 62,  188 => 61,  185 => 60,  183 => 59,  179 => 57,  172 => 52,  163 => 49,  160 => 48,  156 => 47,  150 => 43,  141 => 37,  134 => 32,  132 => 31,  127 => 29,  122 => 27,  116 => 23,  108 => 19,  106 => 18,  93 => 8,  90 => 7,  80 => 6,  61 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}


{% block title %}Product{% endblock %}

{% block body %}

    {{ app.session.set('total',0) }}

    <div class=\"container basic-layout\">

        <div class=\"upload-single\">

            <div class=\"upload pt-5\">

                <h3 class=\"d-flex\">
                    <span class=\"flex-grow-1\">Product</span>
                    {% if app.request.session.get('product-back') %}
                        <a href=\"{{ path( app.request.session.get('product-back'),app.request.session.get('product-back-arguments')) }}\">
                            <i class=\"fa fa-times\"></i>
                        </a>
                    {% endif %}

                </h3>
                <hr>

                {{ form_start(form) }}

                {{ form_errors(form) }}

                {% if app.request.get('type') == 'single' %}

                    <div class=\"row\">

                        <div class=\"col-4\"></div>

                        {{ _self.upload(form,product,configuration) }}

                    </div>


                {% else %}


                    <div class=\"row\">

                        {% for index,f in form.children %}

                            {{ _self.upload(f,product.children[index],configuration) }}

                        {% endfor %}

                    </div>


                {% endif %}

                <div class=\"text-center\">
                    {% if app.request.attributes.get('type') == 'series' and product.status == 3 %}
                        <hr>
                        {% if product.newCommission %}
                            <p class=\"text-info\">Your series price will be
                                                 changed on {{ product.commissionChangingOn|date('d M H:iA') }}
                                                 to {{ product.newCommission }} &euro;.</p>
                        {% endif %}

                        <h4 class=\"text-muted\">Price <strong>{{ product.commission }} &euro;</strong></h4>
                        <hr>
                    {% endif %}

                </div>

                <div class=\"text-center mt-5\">

                    {% if app.session.get('total',0) > 0 %}
                        <button class=\"btn btn-blue\">Save</button>
                    {% endif %}

                    <div>
                        <a href=\"{{ path('web_photos_manage', {type: app.request.attributes.get('type')}) }}\"
                           class=\"btn\">Back to
                                       Manage {{ app.request.attributes.get('type') != 'series' ? 'Photos' : 'Series' }} </a>
                    </div>

                </div>

                <div class=\"d-none\">
                    {{ form_rest(form) }}
                </div>

                {{ form_end(form) }}


            </div>

        </div>

    </div>

{% endblock %}

{% block js %}
    <script>
        \$(function () {
            \$('.custom-file-input').on('change', function () {

                let \$label = \$(this).parent().find('label');

                \$label.addClass('text-left')
                \$label.text('Added')

            })
        })
    </script>
{% endblock %}

{% macro upload(form,product,configuration) %}

    <div class=\"col-4 mb-4 d-flex\">

        <div class=\"card p-0\">

            <div class=\"card-body p-0\">

                {% if workflow_has_marked_place(product, 1) %}

                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(product, 2) %}

                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(product, 3) %}

                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                {% endif %}

                <img src=\"{{ product.post.image.thumbnail }}\" alt=\"\"
                     style=\"object-fit: cover\" width=\"100%\" height=\"300px\">
            </div>

            {% if product.post.image.print is not empty %}

                <div class=\"card-footer\">

                    <p class=\"text-success m-0\">Print file uploaded!</p>

                </div>

            {% endif %}

            {% if form.post is defined %}

                {{ app.session.set('total',app.session.get('total',0) + 1) }}


                <div class=\"card-footer\">

                    {{ form_errors(form.post.image.file) }}

                    <small class=\"text-muted\">{{ 'Print File' | upper }} <span class=\"text-danger\">*</span></small>

                    {{ form_widget(form.post.image.file,{attr:{class:'product'}}) }}

                    <small class=\"text-muted\">Print width and height should be >= {{ configuration.minPrintSize }}
                                              pixels</small>


                </div>

            {% endif %}

            {% set  eligible = need_print(product.post.image) == false %}

            {% if product.post.image.print is null and eligible %}

                <div class=\"card-footer\">

                    <div class=\"d-flex text-success\">

                       <span class=\"flex-grow-1  text-success\">

                          Your current file size is eligible for store no need to upload file.

                       </span>

                    </div>

                </div>

            {% endif %}

            <div class=\"card-footer\">

                {% if form.commission is defined %}

                    {{ app.session.set('total',app.session.get('total',0) + 1) }}

                    <small class=\"text-muted\"> {{ 'commission in euro €' | upper }}: <span class=\"text-danger\">*</span></small>

                    {{ form_widget(form.commission) }}

                    <small class=\"text-muted\">Range {{ configuration.minCommission }}
                                              - {{ configuration.maxCommission }}
                        {{ configuration.currency }}
                    </small>

                    {{ form_errors(form.commission) }}

                {% else %}

                    {% if configuration.priceUpdateInterval is defined and configuration.priceUpdateInterval > 0 %}

                        <p class=\"text-info\">You will be able to change the price
                                             on: {{ product.priceChangedAt|date('d M H:iA') }}.</p>


                    {% else %}

                        <p class=\"text-muted\">Commission
                            <strong>{{ product.commission | number_format(2) }} {{ configuration.currency }}</strong>
                        </p>

                    {% endif %}

                    {% if product.newCommission %}
                        <p class=\"text-info\">Your price will be
                                             changed on {{ product.commissionChangingOn|date('d M H:iA') }}
                                             to {{ product.newCommission }} &euro;.</p>
                    {% endif %}

                    <p class=\"text-muted\">Price <strong>{{ product.commission }} &euro;</strong></p>


                {% endif %}

            </div>

        </div>

    </div>

{% endmacro %}
", "photo/upload-print.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload-print.html.twig");
    }
}
