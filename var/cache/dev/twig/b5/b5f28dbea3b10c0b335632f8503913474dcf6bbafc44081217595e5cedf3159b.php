<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/curate_potd.html.twig */
class __TwigTemplate_95ba76863f40534d711935eac81a60c251f3eec58640fbf962620cffa8899a5d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
            'js' => [$this, 'block_js'],
            'css' => [$this, 'block_css'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/curate_potd.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/curate_potd.html.twig"));

        $this->parent = $this->loadTemplate("template.html.twig", "admin/curator/curate_potd.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Manage Photo of the Day";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        echo "Manage Photos of the Day";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        echo " <strong>The Time is:</strong> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_converter($this->env), "d/M/Y H:i:s"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        // line 10
        echo "
    <div class=\"col-12\">

        <div class=\"row\">

            ";
        // line 15
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["photos"]) || array_key_exists("photos", $context) ? $context["photos"] : (function () { throw new RuntimeError('Variable "photos" does not exist.', 15, $this->source); })())), 0))) {
            // line 16
            echo "
                ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) || array_key_exists("photos", $context) ? $context["photos"] : (function () { throw new RuntimeError('Variable "photos" does not exist.', 17, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
                // line 18
                echo "
                    <div data-id=\"";
                // line 19
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 19), "html", null, true);
                echo "\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">

                        <div class=\"card mb-4 text-center\">

                            <a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_show", ["page" => "portfolio", "id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 25
$context["photo"], "post", [], "any", false, false, false, 25), "id", [], "any", false, false, false, 25), "payload" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 26
$context["photo"], "post", [], "any", false, false, false, 26), "user", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26)]), "html", null, true);
                // line 27
                echo "\" target=\"_blank\" style=\"cursor: zoom-in; position: relative\">
                                ";
                // line 28
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 28)) {
                    // line 29
                    echo "                                    <div class=\"alert-success position-absolute\" style=\"bottom: 0; width: 100%\">Shows
                                        tomorrow
                                    </div>
                                ";
                }
                // line 33
                echo "
                                <img src=\"";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 34), "image", [], "any", false, false, false, 34), "thumbnail", [], "any", false, false, false, 34), "html", null, true);
                echo "\" class=\"card-img-top\" alt=\"...\"
                                     style=\"object-fit: cover; width: 100%; height: 200px\">
                            </a>


                            <div class=\"card-body\">
                                <div class=\"clearfix text-cente position-relative\">

                                    <i class=\"js-move-left fa-2x fa fa-arrow-circle-left float-left ";
                // line 42
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 42)) {
                    echo "d-none";
                }
                echo "\"></i>

                                    <i class=\"js-move-right fa-2x  fa fa-arrow-circle-right float-right ";
                // line 44
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 44)) {
                    echo "d-none";
                }
                echo "\"></i>

                                </div>

                                <div></div>

                                <form action=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_delete_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 50)]), "html", null, true);
                echo "\" method=\"post\">
                                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\"/>
                                    <input type=\"hidden\" name=\"photo_id\" value=\"";
                // line 52
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "id", [], "any", false, false, false, 52), "html", null, true);
                echo "\"/>
                                    <button class=\"btn btn-danger btn-sm text-white mt-3\">Delete</button>
                                </form>

                                <hr class=\"no-gutter\">

                                <div>";
                // line 58
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "createdAt", [], "any", false, false, false, 58), "d/M/Y H:i:s"), "html", null, true);
                echo "</div>

                                <div class=\"small text-muted\">";
                // line 60
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 60), "user", [], "any", false, false, false, 60), "profile", [], "any", false, false, false, 60), "firstName", [], "any", false, false, false, 60), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["photo"], "post", [], "any", false, false, false, 60), "user", [], "any", false, false, false, 60), "profile", [], "any", false, false, false, 60), "lastName", [], "any", false, false, false, 60), "html", null, true);
                echo "</div>

                            </div>
                        </div>
                    </div>

                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "
            ";
        } else {
            // line 69
            echo "
                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No photos in the queue.</h6>
                </div>

            ";
        }
        // line 75
        echo "
        </div>

    </div>

    <form id=\"js-switch-form\" action=\"";
        // line 80
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_switch_photo_of_the_day");
        echo "\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 87
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 88
        echo "    <script>
        \$(document).ready(function () {
            \$('.js-move-left').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').prev().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            });

            \$('.js-move-right').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').next().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            })
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 109
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 110
        echo "    <style>
        .fa {
            cursor: pointer;
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/curate_potd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 110,  333 => 109,  304 => 88,  294 => 87,  278 => 80,  271 => 75,  263 => 69,  259 => 67,  236 => 60,  231 => 58,  222 => 52,  217 => 50,  206 => 44,  199 => 42,  188 => 34,  185 => 33,  179 => 29,  177 => 28,  174 => 27,  172 => 26,  171 => 25,  170 => 23,  163 => 19,  160 => 18,  143 => 17,  140 => 16,  138 => 15,  131 => 10,  121 => 9,  101 => 7,  82 => 5,  63 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'template.html.twig' %}

{% block title %}Manage Photo of the Day{% endblock %}

{% block header_left %}Manage Photos of the Day{% endblock %}

{% block header_right %} <strong>The Time is:</strong> {{ date()|date('d/M/Y H:i:s') }}{% endblock %}

{% block main_content %}

    <div class=\"col-12\">

        <div class=\"row\">

            {% if photos|length > 0 %}

                {% for photo in photos %}

                    <div data-id=\"{{ photo.id }}\" class=\"js-main col-lg-3 col-md-4 col-sm-6\">

                        <div class=\"card mb-4 text-center\">

                            <a href=\"{{ path('web_photo_show', {
                                page:'portfolio',
                                id: photo.post.id,
                                payload: photo.post.user.id,
                            }) }}\" target=\"_blank\" style=\"cursor: zoom-in; position: relative\">
                                {% if loop.first %}
                                    <div class=\"alert-success position-absolute\" style=\"bottom: 0; width: 100%\">Shows
                                        tomorrow
                                    </div>
                                {% endif %}

                                <img src=\"{{ photo.post.image.thumbnail }}\" class=\"card-img-top\" alt=\"...\"
                                     style=\"object-fit: cover; width: 100%; height: 200px\">
                            </a>


                            <div class=\"card-body\">
                                <div class=\"clearfix text-cente position-relative\">

                                    <i class=\"js-move-left fa-2x fa fa-arrow-circle-left float-left {% if loop.first %}d-none{% endif %}\"></i>

                                    <i class=\"js-move-right fa-2x  fa fa-arrow-circle-right float-right {% if loop.last %}d-none{% endif %}\"></i>

                                </div>

                                <div></div>

                                <form action=\"{{ path('admin_delete_photo_of_the_day', {id:photo.id}) }}\" method=\"post\">
                                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\"/>
                                    <input type=\"hidden\" name=\"photo_id\" value=\"{{ photo.id }}\"/>
                                    <button class=\"btn btn-danger btn-sm text-white mt-3\">Delete</button>
                                </form>

                                <hr class=\"no-gutter\">

                                <div>{{ photo.createdAt|date('d/M/Y H:i:s') }}</div>

                                <div class=\"small text-muted\">{{ photo.post.user.profile.firstName }} {{ photo.post.user.profile.lastName }}</div>

                            </div>
                        </div>
                    </div>

                {% endfor %}

            {% else %}

                <div class=\"col-12\">
                    <h6 class=\"text-muted\">No photos in the queue.</h6>
                </div>

            {% endif %}

        </div>

    </div>

    <form id=\"js-switch-form\" action=\"{{ path('admin_switch_photo_of_the_day') }}\" method=\"post\" class=\"d-none\">
        <input type=\"hidden\" name=\"_method\" value=\"PUT\"/>
        <input id=\"js-switch\" type=\"hidden\" name=\"ids\" value=\"\">
    </form>

{% endblock %}

{% block js %}
    <script>
        \$(document).ready(function () {
            \$('.js-move-left').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').prev().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            });

            \$('.js-move-right').on('click', function (e) {
                let ids = [\$(this).parents('.js-main').attr('data-id'),
                    \$(this).parents('.js-main').next().attr('data-id')];

                \$('#js-switch').val(ids);
                \$('#js-switch-form').submit();
            })
        })
    </script>
{% endblock %}

{% block css %}
    <style>
        .fa {
            cursor: pointer;
        }
    </style>
{% endblock %}
", "admin/curator/curate_potd.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/curate_potd.html.twig");
    }
}
