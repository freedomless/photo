<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/navigation.html.twig */
class __TwigTemplate_afabf8814d8d9cf94b4852faf29cd720ce6505a18212f81f144f56f43d2b5c9a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/navigation.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/navigation.html.twig"));

        // line 1
        $context["stats"] = $this->extensions['App\Twig\AccountStatsExtension']->getAccountStats(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "get", [0 => "slug"], "method", false, false, false, 1));
        // line 2
        echo "
<ul class=\"nav justify-content-center custom-active  flex-column flex-sm-row\">

    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 8, $this->source); })()), "slug", [], "any", false, false, false, 8)]), "html", null, true);
        echo "\">
            Published (";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 9, $this->source); })()), "published", [], "any", false, false, false, 9), "html", null, true);
        echo ")</a>

    </li>


    ";
        // line 14
        if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\PostSettingsExtension']->getUserSettings((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 14, $this->source); })())), "is_portfolio_visible", [], "any", false, false, false, 14), true)) || (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 14, $this->source); })()), "id", [], "any", false, false, false, 14))))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR"))) {
            // line 15
            echo "
        <li class=\"nav-item flex-sm-fill text-center\">

            <a class=\"nav-link no-shadow ";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_portfolio", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_portfolio", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "slug", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">
                Uploaded (";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 20, $this->source); })()), "portfolio", [], "any", false, false, false, 20), "html", null, true);
            echo ")</a>

        </li>

    ";
        }
        // line 25
        echo "

    <li class=\"nav-item flex-sm-fill text-center\">
        <a class=\"nav-link no-shadow ";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_products", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 29, $this->source); })()), "slug", [], "any", false, false, false, 29)]), "html", null, true);
        echo "\">
            Prints for Sale (";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 30, $this->source); })()), "products", [], "any", false, false, false, 30), "html", null, true);
        echo ")</a>
    </li>


    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow ";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_albums", "_route", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_albums", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 37, $this->source); })()), "slug", [], "any", false, false, false, 37)]), "html", null, true);
        echo "\">
            Albums (";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 38, $this->source); })()), "albums", [], "any", false, false, false, 38), "html", null, true);
        echo ")</a>

    </li>


    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "user", [], "any", false, false, false, 43) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "user", [], "any", false, false, false, 43), "id", [], "any", false, false, false, 43), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 43, $this->source); })()), "id", [], "any", false, false, false, 43))))) {
            // line 44
            echo "
        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow ";
            // line 46
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_favorites", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_favorites", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 47, $this->source); })()), "slug", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\">
                Favorites (";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 48, $this->source); })()), "favorites", [], "any", false, false, false, 48), "html", null, true);
            echo ")</a>
        </li>

        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow ";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_profile_followed", "_route", "active shadow"), "html", null, true);
            echo "\"
               href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_followed", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 53, $this->source); })()), "slug", [], "any", false, false, false, 53)]), "html", null, true);
            echo "\">
                Followed (";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 54, $this->source); })()), "followed", [], "any", false, false, false, 54), "html", null, true);
            echo ")</a>
        </li>

    ";
        }
        // line 58
        echo "

</ul>

";
        // line 62
        if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 62, $this->source); })()), "request", [], "any", false, false, false, 62), "get", [0 => "_route"], "method", false, false, false, 62), "web_profile")) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 63
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 63, $this->source); })()), "request", [], "any", false, false, false, 63), "get", [0 => "_route"], "method", false, false, false, 63), "web_profile_portfolio"))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 64
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 64, $this->source); })()), "request", [], "any", false, false, false, 64), "get", [0 => "_route"], "method", false, false, false, 64), "web_profile_products")))) {
            // line 65
            echo "    <hr>
    ";
            // line 66
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 66, $this->source); })()), "request", [], "any", false, false, false, 66), "get", [0 => "_route"], "method", false, false, false, 66), "web_profile_products"))) {
                // line 67
                echo "
        <div class=\"text-center\">

            <strong>Share your Print Store!&nbsp;

                ";
                // line 72
                echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 74
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 74, $this->source); })()), "slug", [], "any", false, false, false, 74), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 75
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 75, $this->source); })()), "request", [], "any", false, false, false, 75), "get", [0 => "type"], "method", false, false, false, 75)]), "classes" => "pr-2", "icon_classes" => ""]);
                // line 79
                echo "

                ";
                // line 81
                echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 83
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 83, $this->source); })()), "slug", [], "any", false, false, false, 83), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 84
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 84, $this->source); })()), "request", [], "any", false, false, false, 84), "get", [0 => "type"], "method", false, false, false, 84)]), "media" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 86
($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", true, true, false, 86) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", false, false, false, 86)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 86), "cover", [], "any", false, false, false, 86)) : ("")), "description" => "Best Quality Art Prints", "classes" => "pr-2"]);
                // line 89
                echo "

                ";
                // line 91
                echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_profile_products", ["slug" => twig_get_attribute($this->env, $this->source,                 // line 93
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 93, $this->source); })()), "slug", [], "any", false, false, false, 93), "type" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 94
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 94, $this->source); })()), "request", [], "any", false, false, false, 94), "get", [0 => "type"], "method", false, false, false, 94)])]);
                // line 96
                echo "

            </strong>

        </div>

    ";
            }
            // line 103
            echo "
    <div class=\"d-flex justify-content-center\">
        <ul class=\"nav custom-active w-50\">
            <li class=\"nav-item flex-sm-fill text-center\">
                <a class=\"nav-link no-shadow ";
            // line 107
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("single", "type", "active shadow"), "html", null, true);
            echo "\"
                   href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 108, $this->source); })()), "request", [], "any", false, false, false, 108), "get", [0 => "_route"], "method", false, false, false, 108), ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 108, $this->source); })()), "slug", [], "any", false, false, false, 108)]), "html", null, true);
            echo "\">
                    Single
                    ";
            // line 110
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 110, $this->source); })()), "request", [], "any", false, false, false, 110), "get", [0 => "_route"], "method", false, false, false, 110), "web_profile"))) {
                // line 111
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 111, $this->source); })()), "single_published", [], "any", false, false, false, 111), "html", null, true);
                echo ")
                    ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 112
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 112, $this->source); })()), "request", [], "any", false, false, false, 112), "get", [0 => "_route"], "method", false, false, false, 112), "web_profile_products"))) {
                // line 113
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 113, $this->source); })()), "products_single", [], "any", false, false, false, 113), "html", null, true);
                echo ")
                    ";
            } else {
                // line 115
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 115, $this->source); })()), "single_portfolio", [], "any", false, false, false, 115), "html", null, true);
                echo ")
                    ";
            }
            // line 117
            echo "                </a>

            </li>
            <li class=\"nav-item flex-sm-fill text-center\">

                <a class=\"nav-link no-shadow ";
            // line 122
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("series", "type", "active shadow"), "html", null, true);
            echo "\"
                   href=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 123, $this->source); })()), "request", [], "any", false, false, false, 123), "get", [0 => "_route"], "method", false, false, false, 123), ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 123, $this->source); })()), "slug", [], "any", false, false, false, 123), "type" => "series"]), "html", null, true);
            echo "\">
                    Series
                    ";
            // line 125
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "request", [], "any", false, false, false, 125), "get", [0 => "_route"], "method", false, false, false, 125), "web_profile"))) {
                // line 126
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 126, $this->source); })()), "series_published", [], "any", false, false, false, 126), "html", null, true);
                echo ")
                    ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 127
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 127, $this->source); })()), "request", [], "any", false, false, false, 127), "get", [0 => "_route"], "method", false, false, false, 127), "web_profile_products"))) {
                // line 128
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 128, $this->source); })()), "products_series", [], "any", false, false, false, 128), "html", null, true);
                echo ")
                    ";
            } else {
                // line 130
                echo "                        (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 130, $this->source); })()), "series_portfolio", [], "any", false, false, false, 130), "html", null, true);
                echo ")
                    ";
            }
            // line 132
            echo "                </a>

            </li>
        </ul>
    </div>

";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "user/profile/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 132,  284 => 130,  278 => 128,  276 => 127,  271 => 126,  269 => 125,  264 => 123,  260 => 122,  253 => 117,  247 => 115,  241 => 113,  239 => 112,  234 => 111,  232 => 110,  227 => 108,  223 => 107,  217 => 103,  208 => 96,  206 => 94,  205 => 93,  204 => 91,  200 => 89,  198 => 86,  197 => 84,  196 => 83,  195 => 81,  191 => 79,  189 => 75,  188 => 74,  187 => 72,  180 => 67,  178 => 66,  175 => 65,  173 => 64,  172 => 63,  171 => 62,  165 => 58,  158 => 54,  154 => 53,  150 => 52,  143 => 48,  139 => 47,  135 => 46,  131 => 44,  129 => 43,  121 => 38,  117 => 37,  113 => 36,  104 => 30,  100 => 29,  96 => 28,  91 => 25,  83 => 20,  79 => 19,  75 => 18,  70 => 15,  68 => 14,  60 => 9,  56 => 8,  52 => 7,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set stats = account_stats(app.request.get('slug')) %}

<ul class=\"nav justify-content-center custom-active  flex-column flex-sm-row\">

    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow {{ active('web_profile','_route','active shadow') }}\"
           href=\"{{ path('web_profile',{slug: user.slug}) }}\">
            Published ({{ stats.published }})</a>

    </li>


    {% if  user_settings(user).is_portfolio_visible == true or app.user and app.user.id == user.id or is_granted('ROLE_CURATOR') %}

        <li class=\"nav-item flex-sm-fill text-center\">

            <a class=\"nav-link no-shadow {{ active('web_profile_portfolio','_route','active shadow') }}\"
               href=\"{{ path('web_profile_portfolio',{slug: user.slug}) }}\">
                Uploaded ({{ stats.portfolio }})</a>

        </li>

    {% endif %}


    <li class=\"nav-item flex-sm-fill text-center\">
        <a class=\"nav-link no-shadow {{ active('web_profile_products','_route','active shadow') }}\"
           href=\"{{ path('web_profile_products',{slug:user.slug}) }}\">
            Prints for Sale ({{ stats.products }})</a>
    </li>


    <li class=\"nav-item flex-sm-fill text-center\">

        <a class=\"nav-link no-shadow {{ active('web_albums','_route','active shadow') }}\"
           href=\"{{ path('web_albums',{slug: user.slug}) }}\">
            Albums ({{ stats.albums }})</a>

    </li>


    {% if app.user and app.user.id == user.id %}

        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow {{ active('web_profile_favorites','_route','active shadow') }}\"
               href=\"{{ path('web_profile_favorites',{slug:user.slug}) }}\">
                Favorites ({{ stats.favorites }})</a>
        </li>

        <li class=\"nav-item flex-sm-fill text-center\">
            <a class=\"nav-link no-shadow {{ active('web_profile_followed','_route','active shadow') }}\"
               href=\"{{ path('web_profile_followed',{slug:user.slug}) }}\">
                Followed ({{ stats.followed }})</a>
        </li>

    {% endif %}


</ul>

{% if app.request.get('_route') == 'web_profile'
    or app.request.get('_route') == 'web_profile_portfolio'
    or app.request.get('_route') == 'web_profile_products' %}
    <hr>
    {% if app.request.get('_route') == 'web_profile_products' %}

        <div class=\"text-center\">

            <strong>Share your Print Store!&nbsp;

                {{ include('_partials/buttons/_facebook_share.html.twig', {
                    'share_url': url('web_profile_products', {
                        'slug': user.slug,
                        'type': app.request.get('type')
                    }),
                    'classes': 'pr-2',
                    'icon_classes': ''
                }) }}

                {{ include('_partials/buttons/_pinterest_share.html.twig', {
                    'share_url': url('web_profile_products', {
                        'slug': user.slug,
                        'type': app.request.get('type')
                    }),
                    'media': user.profile.cover ?? '',
                    'description': 'Best Quality Art Prints',
                    'classes': 'pr-2'
                }) }}

                {{ include('_partials/buttons/_twitter_share.html.twig', {
                    'share_url': url('web_profile_products', {
                        'slug': user.slug,
                        'type': app.request.get('type')
                    }),
                }) }}

            </strong>

        </div>

    {% endif %}

    <div class=\"d-flex justify-content-center\">
        <ul class=\"nav custom-active w-50\">
            <li class=\"nav-item flex-sm-fill text-center\">
                <a class=\"nav-link no-shadow {{ active('single','type','active shadow') }}\"
                   href=\"{{ path(app.request.get('_route'),{slug: user.slug}) }}\">
                    Single
                    {% if app.request.get('_route') == 'web_profile' %}
                        ({{ stats.single_published }})
                    {% elseif app.request.get('_route') == 'web_profile_products' %}
                        ({{ stats.products_single }})
                    {% else %}
                        ({{ stats.single_portfolio }})
                    {% endif %}
                </a>

            </li>
            <li class=\"nav-item flex-sm-fill text-center\">

                <a class=\"nav-link no-shadow {{ active('series','type','active shadow') }}\"
                   href=\"{{ path(app.request.get('_route'),{slug: user.slug,type:'series'}) }}\">
                    Series
                    {% if app.request.get('_route') == 'web_profile' %}
                        ({{ stats.series_published }})
                    {% elseif app.request.get('_route') == 'web_profile_products' %}
                        ({{ stats.products_series }})
                    {% else %}
                        ({{ stats.series_portfolio }})
                    {% endif %}
                </a>

            </li>
        </ul>
    </div>

{% endif %}
", "user/profile/navigation.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/navigation.html.twig");
    }
}
