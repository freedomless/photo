<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-votes.html.twig */
class __TwigTemplate_aa09cbb00da2d540d8aa8a072dce92e2ccf4053aaa6f2ac8f00cffe56e579a56 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-votes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-votes.html.twig"));

        // line 1
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["votes"]) || array_key_exists("votes", $context) ? $context["votes"] : (function () { throw new RuntimeError('Variable "votes" does not exist.', 1, $this->source); })())), 0))) {
            // line 2
            echo "    <div class=\"card mt-3 mb-3\">
        <div class=\"card-header\">
            <strong>Curator votes</strong>
        </div>
        <div class=\"card-body\">
            <ul class=\"list-group p-0\">
                ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["votes"]) || array_key_exists("votes", $context) ? $context["votes"] : (function () { throw new RuntimeError('Variable "votes" does not exist.', 8, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["vote"]) {
                // line 9
                echo "                    <li class=\"list-group-item d-flex\">
                        <strong class=\"flex-grow-1 bold\"
                                data-toggle=\"tooltip\"
                                data-placement=\"top\" title=\"";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vote"], "voter", [], "any", false, false, false, 12), "username", [], "any", false, false, false, 12), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["vote"], "voter", [], "any", false, false, false, 12), "profile", [], "any", false, false, false, 12), "fullName", [], "any", false, false, false, 12), "html", null, true);
                echo "</strong>

                        <span class=\"badge badge-";
                // line 14
                echo ((twig_get_attribute($this->env, $this->source, $context["vote"], "vote", [], "any", false, false, false, 14)) ? ("success") : ("danger"));
                echo " mb-1\">";
                echo ((twig_get_attribute($this->env, $this->source, $context["vote"], "vote", [], "any", false, false, false, 14)) ? ("Yes") : ("No"));
                echo "</span>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vote'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "            </ul>
        </div>
    </div>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-votes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 17,  69 => 14,  62 => 12,  57 => 9,  53 => 8,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if votes | length > 0 %}
    <div class=\"card mt-3 mb-3\">
        <div class=\"card-header\">
            <strong>Curator votes</strong>
        </div>
        <div class=\"card-body\">
            <ul class=\"list-group p-0\">
                {% for vote in votes %}
                    <li class=\"list-group-item d-flex\">
                        <strong class=\"flex-grow-1 bold\"
                                data-toggle=\"tooltip\"
                                data-placement=\"top\" title=\"{{ vote.voter.username }}\">{{ vote.voter.profile.fullName }}</strong>

                        <span class=\"badge badge-{{ vote.vote ? 'success' : 'danger' }} mb-1\">{{ vote.vote ? 'Yes' : 'No' }}</span>
                    </li>
                {% endfor %}
            </ul>
        </div>
    </div>
{% endif %}
", "admin/curator/_partials/curator-votes.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-votes.html.twig");
    }
}
