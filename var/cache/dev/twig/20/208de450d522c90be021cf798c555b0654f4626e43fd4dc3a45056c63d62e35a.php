<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/register/index.html.twig */
class __TwigTemplate_ff952a009fc5cf0f0c7d93200d08737eb6dc115d970113f55c55bf8da84e0543 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "security/template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/register/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/register/index.html.twig"));

        $this->parent = $this->loadTemplate("security/template.html.twig", "security/register/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Registration";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_title"));

        echo "Registration";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_body"));

        // line 8
        echo "
    <div class=\"basic-layout\">

        <div class=\"default-form-layout\">

            ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), 'form_start');
        echo "

            ";
        // line 16
        echo "            ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "vars", [], "any", false, false, false, 16), "errors", [], "any", false, false, false, 16))) {
            // line 17
            echo "
                <div class=\"alert alert-error\">
                    ";
            // line 19
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), 'errors');
            echo "
                </div>

            ";
        }
        // line 23
        echo "
            <div class=\"form-group mb-3\">

                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 26, $this->source); })()), "email", [], "any", false, false, false, 26), 'label');
        echo "
                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "email", [], "any", false, false, false, 27), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "password", [], "any", false, false, false, 33), "first", [], "any", false, false, false, 33), 'label');
        echo "
                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), "password", [], "any", false, false, false, 34), "first", [], "any", false, false, false, 34), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), "password", [], "any", false, false, false, 40), "second", [], "any", false, false, false, 40), 'label');
        echo "
                ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 41, $this->source); })()), "password", [], "any", false, false, false, 41), "second", [], "any", false, false, false, 41), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <hr>

            <div class=\"form-group mb-3\">

                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 49, $this->source); })()), "username", [], "any", false, false, false, 49), 'label');
        echo "
                ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "username", [], "any", false, false, false, 50), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                <small id=\"emailHelp\" class=\"form-text text-muted\">
                    Note: Once set, it cannot be changed later.
                </small>

            </div>

            <hr>

            <div class=\"form-group mb-1\">
                <small id=\"emailHelp\" class=\"form-text text-warning\">
                    Note: It is recommended to use latin characters for your First and Last name. Otherwise your photos
                    won't be watermarked.
                </small>
            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 68
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 68, $this->source); })()), "profile", [], "any", false, false, false, 68), "firstName", [], "any", false, false, false, 68), 'label');
        echo "
                ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 69, $this->source); })()), "profile", [], "any", false, false, false, 69), "firstName", [], "any", false, false, false, 69), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <div class=\"form-group mb-3\">

                ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 75, $this->source); })()), "profile", [], "any", false, false, false, 75), "lastName", [], "any", false, false, false, 75), 'label');
        echo "
                ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 76, $this->source); })()), "profile", [], "any", false, false, false, 76), "lastName", [], "any", false, false, false, 76), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

            </div>

            <hr>

            <div class=\"registration-agreement rounded-corner\">
                By clicking <strong>Agree and Continue</strong>, I hereby: <br>
                Agree and consent to the
                <br><br>
                <input type=\"checkbox\" id=\"js-terms-agreement\"> <label for=\"js-terms-agreement\"><b>Agree
                                                                                                   and
                                                                                                   Continue</b></label>
            </div>

            <hr>

            <div class=\"text-center ml-5 mr-5 mt-3\">
                <button class=\"btn btn-info disabled\" id=\"js-registraion-btn\" type=\"submit\">Register</button>
            </div>

            <div class=\"text-center mt-3 mb-3\">
                Already have an account? <strong><a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
        echo "\">Login</a></strong>
            </div>

            ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 101, $this->source); })()), "_token", [], "any", false, false, false, 101), 'widget');
        echo "

            ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 103, $this->source); })()), "recaptcha", [], "any", false, false, false, 103), 'widget');
        echo "

            ";
        // line 105
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 105, $this->source); })()), 'form_end');
        echo "

        </div>

        <div
                class=\"g-recaptcha\"
                data-sitekey=\"";
        // line 111
        echo twig_escape_filter($this->env, (isset($context["google_recaptcha_site_key"]) || array_key_exists("google_recaptcha_site_key", $context) ? $context["google_recaptcha_site_key"] : (function () { throw new RuntimeError('Variable "google_recaptcha_site_key" does not exist.', 111, $this->source); })()), "html", null, true);
        echo "\"
                data-callback=\"recaptchaOkay\"
                data-size=\"invisible\"
                id=\"cadastro-captcha\">
        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 121
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_footer"));

        // line 122
        echo "    <h6 class=\"h6\">Register with Your social media account:</h6>

    <a href=\"";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "facebook"]);
        echo "\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-facebook fa-2x\"></i>
    </a>

    <a href=\"";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "google"]);
        echo "\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-google fa-2x\"></i>
    </a>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 134
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 135
        echo "
    <script src=\"https://www.google.com/recaptcha/api.js?render=";
        // line 136
        echo twig_escape_filter($this->env, (isset($context["google_recaptcha_site_key"]) || array_key_exists("google_recaptcha_site_key", $context) ? $context["google_recaptcha_site_key"] : (function () { throw new RuntimeError('Variable "google_recaptcha_site_key" does not exist.', 136, $this->source); })()), "html", null, true);
        echo "\"></script>

    <script>

            grecaptcha.ready(function () {
                grecaptcha.execute('";
        // line 141
        echo twig_escape_filter($this->env, (isset($context["google_recaptcha_site_key"]) || array_key_exists("google_recaptcha_site_key", $context) ? $context["google_recaptcha_site_key"] : (function () { throw new RuntimeError('Variable "google_recaptcha_site_key" does not exist.', 141, $this->source); })()), "html", null, true);
        echo "', {action: 'submit'}).then(function (token) {
                    \$('#registration_recaptcha').val(token)
                });
            });


    </script>

    <script>

        \$(document).ready(function () {

            var selector = '#js-registraion-btn, .js-social-reg';

            toggleBind(selector, true);

            \$('#js-terms-agreement').click(function () {
                if (\$(this).is(':checked')) {
                    toggleBind(selector, false);
                } else {
                    toggleBind(selector, true);
                }
            });

            function toggleBind(selector, set) {
                if (set) {
                    \$(selector).addClass('disabled');
                    \$(selector).bind('click', function (e) {
                        e.preventDefault();
                    });
                } else {
                    \$(selector).removeClass('disabled');
                    \$(selector).unbind('click')
                }
            }
        })

    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/register/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  350 => 141,  342 => 136,  339 => 135,  329 => 134,  314 => 128,  307 => 124,  303 => 122,  293 => 121,  274 => 111,  265 => 105,  260 => 103,  255 => 101,  249 => 98,  224 => 76,  220 => 75,  211 => 69,  207 => 68,  186 => 50,  182 => 49,  171 => 41,  167 => 40,  158 => 34,  154 => 33,  145 => 27,  141 => 26,  136 => 23,  129 => 19,  125 => 17,  122 => 16,  117 => 13,  110 => 8,  100 => 7,  81 => 5,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'security/template.html.twig' %}

{% block title %}Registration{% endblock %}

{% block form_title %}Registration{% endblock %}

{% block form_body %}

    <div class=\"basic-layout\">

        <div class=\"default-form-layout\">

            {{ form_start(form) }}

            {# Errors #}
            {% if form.vars.errors|length %}

                <div class=\"alert alert-error\">
                    {{ form_errors(form) }}
                </div>

            {% endif %}

            <div class=\"form-group mb-3\">

                {{ form_label(form.email) }}
                {{ form_widget(form.email,{attr:{class:'form-control'}}) }}

            </div>

            <div class=\"form-group mb-3\">

                {{ form_label(form.password.first) }}
                {{ form_widget(form.password.first,{attr:{class:'form-control'}}) }}

            </div>

            <div class=\"form-group mb-3\">

                {{ form_label(form.password.second) }}
                {{ form_widget(form.password.second,{attr:{class:'form-control'}}) }}

            </div>

            <hr>

            <div class=\"form-group mb-3\">

                {{ form_label(form.username) }}
                {{ form_widget(form.username,{attr:{class:'form-control'}}) }}
                <small id=\"emailHelp\" class=\"form-text text-muted\">
                    Note: Once set, it cannot be changed later.
                </small>

            </div>

            <hr>

            <div class=\"form-group mb-1\">
                <small id=\"emailHelp\" class=\"form-text text-warning\">
                    Note: It is recommended to use latin characters for your First and Last name. Otherwise your photos
                    won't be watermarked.
                </small>
            </div>

            <div class=\"form-group mb-3\">

                {{ form_label(form.profile.firstName) }}
                {{ form_widget(form.profile.firstName,{attr:{class:'form-control'}}) }}

            </div>

            <div class=\"form-group mb-3\">

                {{ form_label(form.profile.lastName) }}
                {{ form_widget(form.profile.lastName,{attr:{class:'form-control'}}) }}

            </div>

            <hr>

            <div class=\"registration-agreement rounded-corner\">
                By clicking <strong>Agree and Continue</strong>, I hereby: <br>
                Agree and consent to the
                <br><br>
                <input type=\"checkbox\" id=\"js-terms-agreement\"> <label for=\"js-terms-agreement\"><b>Agree
                                                                                                   and
                                                                                                   Continue</b></label>
            </div>

            <hr>

            <div class=\"text-center ml-5 mr-5 mt-3\">
                <button class=\"btn btn-info disabled\" id=\"js-registraion-btn\" type=\"submit\">Register</button>
            </div>

            <div class=\"text-center mt-3 mb-3\">
                Already have an account? <strong><a href=\"{{ path('web_login') }}\">Login</a></strong>
            </div>

            {{ form_widget(form._token) }}

            {{ form_widget(form.recaptcha) }}

            {{ form_end(form) }}

        </div>

        <div
                class=\"g-recaptcha\"
                data-sitekey=\"{{ google_recaptcha_site_key }}\"
                data-callback=\"recaptchaOkay\"
                data-size=\"invisible\"
                id=\"cadastro-captcha\">
        </div>

    </div>

{% endblock %}

{% block form_footer %}
    <h6 class=\"h6\">Register with Your social media account:</h6>

    <a href=\"{{ path('web_social_login',{ network: 'facebook'}) }}\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-facebook fa-2x\"></i>
    </a>

    <a href=\"{{ path('web_social_login',{ network: 'google'}) }}\" class=\"btn js-social-reg disabled\">
        <i class=\"fab fa-google fa-2x\"></i>
    </a>

{% endblock %}

{% block js %}

    <script src=\"https://www.google.com/recaptcha/api.js?render={{ google_recaptcha_site_key }}\"></script>

    <script>

            grecaptcha.ready(function () {
                grecaptcha.execute('{{ google_recaptcha_site_key }}', {action: 'submit'}).then(function (token) {
                    \$('#registration_recaptcha').val(token)
                });
            });


    </script>

    <script>

        \$(document).ready(function () {

            var selector = '#js-registraion-btn, .js-social-reg';

            toggleBind(selector, true);

            \$('#js-terms-agreement').click(function () {
                if (\$(this).is(':checked')) {
                    toggleBind(selector, false);
                } else {
                    toggleBind(selector, true);
                }
            });

            function toggleBind(selector, set) {
                if (set) {
                    \$(selector).addClass('disabled');
                    \$(selector).bind('click', function (e) {
                        e.preventDefault();
                    });
                } else {
                    \$(selector).removeClass('disabled');
                    \$(selector).unbind('click')
                }
            }
        })

    </script>

{% endblock %}
", "security/register/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/register/index.html.twig");
    }
}
