<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/user.html.twig */
class __TwigTemplate_80e202a81d71252957a800b8720545182c35fe84221eca3a87d3ac5aafafd5b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/user.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/user.html.twig"));

        // line 1
        echo "<li class=\"nav-item dropdown\">

    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"account\" role=\"button\"
       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">My Profile</a>

    <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
         aria-labelledby=\"account\">

        <a class=\"dropdown-item\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "user", [], "any", false, false, false, 9), "slug", [], "any", false, false, false, 9)]), "html", null, true);
        echo "\">View</a>

        <a class=\"dropdown-item\" href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages");
        echo "\">Messages</a>

        <a class=\"dropdown-item\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_edit");
        echo "\">Settings</a>

        ";
        // line 15
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "user", [], "any", false, false, false, 15), "orders", [], "any", false, false, false, 15)), 0))) {
            // line 16
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_orders");
            echo "\">Orders</a>
        ";
        }
        // line 18
        echo "
        ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 19)) {
            // line 20
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_manage_sold");
            echo "\">Sold</a>
        ";
        }
        // line 22
        echo "
        <div class=\"dropdown-divider\"></div>
        <span class=\"ml-4 text-muted\" style=\"font-weight: 300\">Manage:</span>

        <a class=\"dropdown-item\" href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage");
        echo "\">Photos</a>
        <a class=\"dropdown-item\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photos_manage", ["type" => "series"]);
        echo "\">Series</a>

        ";
        // line 29
        if ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", true, true, false, 29) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 29), true)))) {
            // line 30
            echo "            <a class=\"dropdown-item\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_manage_products");
            echo "\">Products</a>
        ";
        }
        // line 32
        echo "
        <div class=\"dropdown-divider\"></div>

        <a class=\"dropdown-item\" href=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_logout");
        echo "\"><i class=\"fa fa-sign-out-alt\"></i> Sign-out</a>

    </div>

</li>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/navigation/user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 35,  110 => 32,  104 => 30,  102 => 29,  97 => 27,  93 => 26,  87 => 22,  81 => 20,  79 => 19,  76 => 18,  70 => 16,  68 => 15,  63 => 13,  58 => 11,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<li class=\"nav-item dropdown\">

    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"account\" role=\"button\"
       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">My Profile</a>

    <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
         aria-labelledby=\"account\">

        <a class=\"dropdown-item\" href=\"{{ path('web_profile', {'slug': app.user.slug}) }}\">View</a>

        <a class=\"dropdown-item\" href=\"{{ path('web_messages') }}\">Messages</a>

        <a class=\"dropdown-item\" href=\"{{ path('web_profile_edit') }}\">Settings</a>

        {% if app.user.orders | length > 0 %}
            <a class=\"dropdown-item\" href=\"{{ path('web_orders') }}\">Orders</a>
        {% endif %}

        {% if config().allowShopping %}
            <a class=\"dropdown-item\" href=\"{{ path('web_manage_sold') }}\">Sold</a>
        {% endif %}

        <div class=\"dropdown-divider\"></div>
        <span class=\"ml-4 text-muted\" style=\"font-weight: 300\">Manage:</span>

        <a class=\"dropdown-item\" href=\"{{ path('web_photos_manage') }}\">Photos</a>
        <a class=\"dropdown-item\" href=\"{{ path('web_photos_manage', {type:'series'}) }}\">Series</a>

        {% if config().allowShopping is defined and config().allowShopping == true %}
            <a class=\"dropdown-item\" href=\"{{ path('web_manage_products') }}\">Products</a>
        {% endif %}

        <div class=\"dropdown-divider\"></div>

        <a class=\"dropdown-item\" href=\"{{ path('web_logout') }}\"><i class=\"fa fa-sign-out-alt\"></i> Sign-out</a>

    </div>

</li>
", "_partials/navigation/user.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/user.html.twig");
    }
}
