<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/index.html.twig */
class __TwigTemplate_468baeb55abe52c82d4e4a7a9ef63ca647215cc96a1e6a5f1522583dd5a1f07f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/index.html.twig"));

        // line 1
        echo "<nav class=\"navbar navbar-expand-xl navbar-dark bg-primary shadow fixed-top\">

    ";
        // line 4
        echo "    <a class=\"navbar-brand\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_popular");
        echo "\">
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo_new.png"), "html", null, true);
        echo "\" alt=\"Logo\" height=\"40px\">
    </a>

    ";
        // line 9
        echo "    <button class=\"navbar-toggler border-0\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar\"
            aria-controls=\"navbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

        <span class=\"navbar-toggler-icon\"></span>

    </button>

    ";
        // line 17
        echo "    <div class=\"collapse navbar-collapse\" id=\"navbar\">

        <form class=\"form-inline nav-search\">
            ";
        // line 20
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("SearchBox", ["rendering" => "client_side"]);
        echo "
        </form>

        ";
        // line 23
        $this->loadTemplate("_partials/navigation/left.html.twig", "_partials/navigation/index.html.twig", 23)->display($context);
        // line 24
        echo "
        ";
        // line 25
        $this->loadTemplate("_partials/navigation/right.html.twig", "_partials/navigation/index.html.twig", 25)->display($context);
        // line 26
        echo "
    </div>

</nav>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/navigation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 26,  83 => 25,  80 => 24,  78 => 23,  72 => 20,  67 => 17,  58 => 9,  52 => 5,  47 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<nav class=\"navbar navbar-expand-xl navbar-dark bg-primary shadow fixed-top\">

    {# Logo #}
    <a class=\"navbar-brand\" href=\"{{ path('web_popular') }}\">
        <img src=\"{{ asset('images/logo_new.png') }}\" alt=\"Logo\" height=\"40px\">
    </a>

    {# Mobile Menu Button #}
    <button class=\"navbar-toggler border-0\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar\"
            aria-controls=\"navbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

        <span class=\"navbar-toggler-icon\"></span>

    </button>

    {# Mobile Dropdown Menu #}
    <div class=\"collapse navbar-collapse\" id=\"navbar\">

        <form class=\"form-inline nav-search\">
            {{ react_component('SearchBox',{'rendering':'client_side'}) }}
        </form>

        {% include '_partials/navigation/left.html.twig' %}

        {% include'_partials/navigation/right.html.twig' %}

    </div>

</nav>
", "_partials/navigation/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/index.html.twig");
    }
}
