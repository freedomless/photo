<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_button.html.twig */
class __TwigTemplate_8e91280fc5528cf14f2e2d26d8cd10865d2cf90efcdce37513a26d2049a4f841 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_button.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_button.html.twig"));

        // line 1
        echo "<p style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><a href=\"";
        (((array_key_exists("button_url", $context) &&  !(null === (isset($context["button_url"]) || array_key_exists("button_url", $context) ? $context["button_url"] : (function () { throw new RuntimeError('Variable "button_url" does not exist.', 1, $this->source); })())))) ? (print (twig_escape_filter($this->env, (isset($context["button_url"]) || array_key_exists("button_url", $context) ? $context["button_url"] : (function () { throw new RuntimeError('Variable "button_url" does not exist.', 1, $this->source); })()), "html", null, true))) : (print ("#")));
        echo "\"
                                                                         class=\"btn btn-primary\"
                                                                         style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #ffffff;padding: 10px 15px;border-radius: 10px;background: #375a7f;\">";
        // line 3
        (((array_key_exists("button_title", $context) &&  !(null === (isset($context["button_title"]) || array_key_exists("button_title", $context) ? $context["button_title"] : (function () { throw new RuntimeError('Variable "button_title" does not exist.', 3, $this->source); })())))) ? (print (twig_escape_filter($this->env, (isset($context["button_title"]) || array_key_exists("button_title", $context) ? $context["button_title"] : (function () { throw new RuntimeError('Variable "button_title" does not exist.', 3, $this->source); })()), "html", null, true))) : (print ("Click Here")));
        echo "</a>
</p>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "emails/_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;\"><a href=\"{{ button_url ?? '#' }}\"
                                                                         class=\"btn btn-primary\"
                                                                         style=\"-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;text-decoration: none;color: #ffffff;padding: 10px 15px;border-radius: 10px;background: #375a7f;\">{{ button_title ?? 'Click Here' }}</a>
</p>", "emails/_button.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_button.html.twig");
    }
}
