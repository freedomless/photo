<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/selection/_form.html.twig */
class __TwigTemplate_7eb04132c170891a26a33cb94cefc052ca63068c60c3ce52c41843070f6f0e4c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/selection/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/selection/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "

<div class=\"form-group\">
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "title", [], "any", false, false, false, 4), 'label', ["label" => "Series Decritption:"]);
        echo "
    ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "title", [], "any", false, false, false, 5), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Series Decritption"]]);
        echo "

    ";
        // line 7
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "title", [], "any", false, false, false, 7), "vars", [], "any", false, false, false, 7), "errors", [], "any", false, false, false, 7)), 0))) {
            // line 8
            echo "        <div class=\"form-error-list alert alert-danger small mt-2\">
            ";
            // line 9
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "title", [], "any", false, false, false, 9), 'errors');
            echo "
        </div>
    ";
        }
        // line 12
        echo "</div>

<div class=\"form-group\">
    ";
        // line 15
        if ((((array_key_exists("series", $context)) ? (_twig_default_filter((isset($context["series"]) || array_key_exists("series", $context) ? $context["series"] : (function () { throw new RuntimeError('Variable "series" does not exist.', 15, $this->source); })()))) : ("")) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["series"]) || array_key_exists("series", $context) ? $context["series"] : (function () { throw new RuntimeError('Variable "series" does not exist.', 15, $this->source); })()), "cover", [], "any", false, false, false, 15)))) {
            // line 16
            echo "        <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["series"]) || array_key_exists("series", $context) ? $context["series"] : (function () { throw new RuntimeError('Variable "series" does not exist.', 16, $this->source); })()), "cover", [], "any", false, false, false, 16), "html", null, true);
            echo "\" alt=\"Cover\" style=\"height: 150px\">

        <hr>
    ";
        }
        // line 20
        echo "
    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "imageCover", [], "any", false, false, false, 21), 'widget', ["attr" => ["class" => "form-control-file", "placeholder" => "Cover", "data-label" => "Cover"]]);
        echo "

    ";
        // line 23
        if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "imageCover", [], "any", false, false, false, 23), "vars", [], "any", false, false, false, 23), "errors", [], "any", false, false, false, 23)), 0))) {
            // line 24
            echo "        <div class=\"form-error-list alert alert-danger small mt-2\">
            ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "imageCover", [], "any", false, false, false, 25), 'errors');
            echo "
        </div>
    ";
        }
        // line 28
        echo "</div>

<div class=\"form-group\">
    <button type=\"submit\" class=\"btn btn-primary\">";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["button_title"]) || array_key_exists("button_title", $context) ? $context["button_title"] : (function () { throw new RuntimeError('Variable "button_title" does not exist.', 31, $this->source); })()), "html", null, true);
        echo "</button>
</div>

";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/selection/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 34,  108 => 31,  103 => 28,  97 => 25,  94 => 24,  92 => 23,  87 => 21,  84 => 20,  76 => 16,  74 => 15,  69 => 12,  63 => 9,  60 => 8,  58 => 7,  53 => 5,  49 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}

<div class=\"form-group\">
    {{ form_label(form.title, 'Series Decritption:') }}
    {{ form_widget(form.title, {attr:{class:'form-control', 'placeholder':'Series Decritption'}}) }}

    {% if form.title.vars.errors|length > 0 %}
        <div class=\"form-error-list alert alert-danger small mt-2\">
            {{ form_errors(form.title) }}
        </div>
    {% endif %}
</div>

<div class=\"form-group\">
    {% if series|default and series.cover is not null %}
        <img src=\"{{ series.cover }}\" alt=\"Cover\" style=\"height: 150px\">

        <hr>
    {% endif %}

    {{ form_widget(form.imageCover, {attr:{class:'form-control-file', 'placeholder':'Cover','data-label':'Cover'}}) }}

    {% if form.imageCover.vars.errors|length > 0 %}
        <div class=\"form-error-list alert alert-danger small mt-2\">
            {{ form_errors(form.imageCover) }}
        </div>
    {% endif %}
</div>

<div class=\"form-group\">
    <button type=\"submit\" class=\"btn btn-primary\">{{ button_title }}</button>
</div>

{{ form_end(form) }}
", "admin/selection/_form.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/selection/_form.html.twig");
    }
}
