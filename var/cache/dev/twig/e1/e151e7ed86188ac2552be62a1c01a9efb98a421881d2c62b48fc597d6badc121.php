<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/series-from-published.html.twig */
class __TwigTemplate_02dcd90eaddcedfde312ef23421a59cd9a929512cfa4b4757ab2b7e217d7e195 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/series-from-published.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/series-from-published.html.twig"));

        // line 3
        $context["order"] = [];
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_sort_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "vars", [], "any", false, false, false, 5), "value", [], "any", false, false, false, 5), "children", [], "any", false, false, false, 5), function ($__a__, $__b__) use ($context, $macros) { $context["a"] = $__a__; $context["b"] = $__b__; return (twig_get_attribute($this->env, $this->source, (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new RuntimeError('Variable "a" does not exist.', 5, $this->source); })()), "position", [], "any", false, false, false, 5) <=> twig_get_attribute($this->env, $this->source, (isset($context["b"]) || array_key_exists("b", $context) ? $context["b"] : (function () { throw new RuntimeError('Variable "b" does not exist.', 5, $this->source); })()), "position", [], "any", false, false, false, 5)); }));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 6
            $context["order"] = twig_array_merge((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 6, $this->source); })()), [0 => twig_get_attribute($this->env, $this->source, $context["child"], "id", [], "any", false, false, false, 6)]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "photo/series-from-published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "vars", [], "any", false, false, false, 9), "method", [], "any", false, false, false, 9), "POST"))) ? ("Create") : ("Update"));
        echo " Series";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 12
        echo "    <style>
        .series-option {
            text-align: center;
        }

        .series-option label img {
            max-width: 100%;
            max-height: 300px;
            border: 5px solid transparent;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            cursor: pointer;

        }

        .series-option span {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translateY(-50%) translateX(-50%);
            font-weight: bold;
            font-size: 40px;
            -webkit-text-stroke: 2px black;
        }

        .series-option input {
            display: none;
        }

        .series-option input:checked + img {
            border: 5px dashed #aaa;
        }

        .series-option input:checked + img.cover {
            border: 5px dashed #375a7f !important;
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 53
        echo "
    ";
        // line 54
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 54, $this->source); })()), [0 => "form_div_layout.html.twig"], true);
        // line 55
        echo "
    ";
        // line 56
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 56, $this->source); })()), "user", [], "any", false, false, false, 56), "isVerified", [], "any", false, false, false, 56), false))) {
            // line 57
            echo "        ";
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/series-from-published.html.twig", 57)->display($context);
            // line 58
            echo "    ";
        } else {
            // line 59
            echo "        ";
            if ((-1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 59, $this->source); })()), "children", [], "any", false, false, false, 59)), twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 59, $this->source); })()), "min", [], "any", false, false, false, 59)))) {
                // line 60
                echo "
            <div class=\"container center\">

                <div class=\"jumbotron d-flex justify-content-center align-items-center flex-column \">

                    <h3 class=\"text-center text-muted\">You don not have enough published photos!</h3>

                    <small class=\"text-muted mb-3\">Send images to curator or </small>

                    <a href=\"";
                // line 69
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create");
                echo "\" class=\"btn btn-blue\">Upload</a>

                </div>

            </div>

        ";
            } else {
                // line 76
                echo "
            <div class=\"container basic-layout\">

                <hr>

                <h3>";
                // line 81
                echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 81, $this->source); })()), "vars", [], "any", false, false, false, 81), "method", [], "any", false, false, false, 81), "POST"))) ? ("Create") : ("Update"));
                echo " Series</h3>

                <hr>

                <div class=\"upload-single\">

                    <div class=\"upload\">

                        ";
                // line 89
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 89, $this->source); })()), 'form_start');
                echo "

                        <div class=\"row\">

                            <div class=\"col-12\">

                                <div class=\"form-group row  pl-3 pr-3\">

                                    ";
                // line 97
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 97, $this->source); })()), "category", [], "any", false, false, false, 97), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 98
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 98, $this->source); })()), "category", [], "any", false, false, false, 98), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 104
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 104, $this->source); })()), "title", [], "any", false, false, false, 104), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 105
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 105, $this->source); })()), "title", [], "any", false, false, false, 105), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 111
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 111, $this->source); })()), "tags", [], "any", false, false, false, 111), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 112
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 112, $this->source); })()), "tags", [], "any", false, false, false, 112), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    ";
                // line 118
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 118, $this->source); })()), "description", [], "any", false, false, false, 118), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
                echo "
                                    ";
                // line 119
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 119, $this->source); })()), "description", [], "any", false, false, false, 119), 'widget', ["attr" => ["class" => "col-md-9 form-control"]]);
                echo "

                                </div>

                                <div class=\"row pl-3\">

                                    <div class=\"col-md-3\"></div>

                                    <div class=\"col-md-9\">

                                        <div class=\"form-check\">
                                            ";
                // line 130
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 130, $this->source); })()), "isNude", [], "any", false, false, false, 130), 'widget', ["attr" => ["class" => "form-check-input"]]);
                echo "
                                            <label for=\"";
                // line 131
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 131, $this->source); })()), "isNude", [], "any", false, false, false, 131), "vars", [], "any", false, false, false, 131), "id", [], "any", false, false, false, 131), "html", null, true);
                echo "\">
                                                ";
                // line 132
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 132, $this->source); })()), "isNude", [], "any", false, false, false, 132), "vars", [], "any", false, false, false, 132), "label", [], "any", false, false, false, 132), "html", null, true);
                echo "
                                            </label>
                                        </div>

                                        <div class=\"form-check\">
                                            ";
                // line 137
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 137, $this->source); })()), "isGreyscale", [], "any", false, false, false, 137), 'widget', ["attr" => ["class" => "form-check-input"]]);
                echo "
                                            <label for=\"";
                // line 138
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 138, $this->source); })()), "isGreyscale", [], "any", false, false, false, 138), "vars", [], "any", false, false, false, 138), "id", [], "any", false, false, false, 138), "html", null, true);
                echo "\">
                                                ";
                // line 139
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 139, $this->source); })()), "isGreyscale", [], "any", false, false, false, 139), "vars", [], "any", false, false, false, 139), "label", [], "any", false, false, false, 139), "html", null, true);
                echo "
                                            </label>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr>

                        ";
                // line 153
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 153), "children", [], "any", true, true, false, 153)) {
                    // line 154
                    echo "
                            <div class=\"row\">

                                <div class=\"col-md-12\">

                                    <div class=\"row\">

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-muted\" id=\"selected-images\">Select photos</h5>

                                            <div class=\"text-danger\">";
                    // line 165
                    echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 165, $this->source); })()), "children", [], "any", false, false, false, 165), 'errors')), "html", null, true);
                    echo "</div>

                                        </div>

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-right text-muted\">

                                                <span class=\"selected-images ";
                    // line 173
                    echo (((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 173, $this->source); })()), "vars", [], "any", false, false, false, 173), "value", [], "any", false, false, false, 173), "children", [], "any", false, false, false, 173)), twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 173, $this->source); })()), "max", [], "any", false, false, false, 173)))) ? ("text-danger") : (""));
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 173, $this->source); })()), "vars", [], "any", false, false, false, 173), "value", [], "any", false, false, false, 173), "children", [], "any", false, false, false, 173)), "html", null, true);
                    echo "</span>/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 173, $this->source); })()), "max", [], "any", false, false, false, 173), "html", null, true);
                    echo "

                                            </h5>

                                        </div>

                                    </div>

                                    <hr class=\"no-gutter\">

                                </div>

                            </div>

                            <div class=\"row available-photos  d-flex justify-content-center align-items-center\"
                                 style=\"height: 600px;overflow: auto\">

                                ";
                    // line 190
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 190, $this->source); })()), "children", [], "any", false, false, false, 190));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 191
                        echo "
                                    <div class=\"series-option col-lg-3 col-md-4 col-sm-6 col-12\">

                                        <div class=\"thumb-container\">

                                            <span class=\"order\"></span>

                                            <label for=\"post_children_";
                        // line 198
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 198), "value", [], "any", false, false, false, 198), "html", null, true);
                        echo "\">

                                                ";
                        // line 200
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                        echo "

                                                <img src=\"";
                        // line 202
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 202), "label", [], "any", false, false, false, 202), "html", null, true);
                        echo "\" alt=\"\">

                                            </label>

                                        </div>

                                    </div>

                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 211
                    echo "
                            </div>

                        ";
                } else {
                    // line 215
                    echo "
                            <div class=\"row\">

                                ";
                    // line 218
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 218, $this->source); })()), "children", [], "any", false, false, false, 218));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 219
                        echo "
                                    <div class=\"col-md-3 mb-3\">

                                        <img src=\"";
                        // line 222
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 222), "thumbnail", [], "any", false, false, false, 222), "html", null, true);
                        echo "\" alt=\"\" height=\"200px\" width=\"100%\" style=\"object-fit: cover\">

                                    </div>

                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 227
                    echo "
                            </div>

                        ";
                }
                // line 231
                echo "
                        <div class=\"text-center mt-5\">

                            ";
                // line 234
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 234, $this->source); })()), "create", [], "any", false, false, false, 234), 'widget', ["attr" => ["class" => "btn btn-blue"], "label" => (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 234, $this->source); })()), "vars", [], "any", false, false, false, 234), "method", [], "any", false, false, false, 234), "POST"))) ? ("Create") : ("Update"))]);
                echo "

                        </div>

                        ";
                // line 238
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 238, $this->source); })()), 'form_end');
                echo "

                    </div>

                </div>

            </div>

        ";
            }
            // line 247
            echo "    ";
        }
        // line 248
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 253
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 254
        echo "
    <script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/infinite.js"), "html", null, true);
        echo "\"></script>

    <script>infinite('.available-photos', '.available-photos');</script>

    <script>

        \$(function () {

            let hiddenOrder = \$('#post_position');

            hiddenOrder.val('";
        // line 265
        echo twig_escape_filter($this->env, twig_join_filter((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 265, $this->source); })()), ","), "html", null, true);
        echo "')

            let order = hiddenOrder.val() ? hiddenOrder.val().split(',') : [];

            let selected = \$('.selected-images');

            function updateNumbers() {
                \$('.series-option input[type=\"checkbox\"]').each(function (index, item) {

                    item = \$(item);

                    let i = order.indexOf(item.val());

                    if (i > -1) {

                        item.siblings('img').removeClass('cover');

                        if (i === 0) {
                            item.siblings('img').addClass('cover');
                        }

                        item.parent().siblings('.order').text(i + 1)
                        item.prop('checked', true)
                    } else {
                        item.parent().siblings('.order').html('')
                    }

                });
            }

            updateNumbers();

            function updateCheckbox(e) {

                e.preventDefault();

                if (e.target.checked) {

                    if (order.length + 1 > ";
        // line 303
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 303, $this->source); })()), "max", [], "any", false, false, false, 303), "html", null, true);
        echo ") {
                        \$(e.target).prop('checked', false)
                        return false;
                    }

                    order.push(e.target.value);

                    \$(e.target).parent().siblings('.order').text(order.length)

                } else {

                    order = order.filter(function (v) {
                        return v != e.target.value;
                    })

                }

                updateNumbers();

                hiddenOrder.val(order);

                selected.text(order.length);

            }

            \$(document).on('change', '.series-option input[type=\"checkbox\"]', updateCheckbox)

            \$('#reset-selection').on('click', function (e) {
                e.preventDefault();
                \$('.series-option input[type=\"checkbox\"]').prop('checked', false);
                \$('.selected-images').text(0);
                order = [];

                updateNumbers()
            })
        });

        \$(function () {
            let h = \$('#selected-images');

            \$(document).scroll(\$.throttle(250, function (e) {

                if (\$(window).scrollTop() > 700) {

                    h.css({
                        'position': 'fixed',
                        'left': '50%',
                        'top': '100px',
                        'background': '#fff',
                        'zIndex': 100,
                        'transform': 'translateX(-50%)'
                    });
                    h.addClass('shadow rounded p-2')

                } else {

                    h.removeAttr('style');
                    h.removeClass('shadow rounded p-2')

                }
            }))
        });

    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "photo/series-from-published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  561 => 303,  520 => 265,  507 => 255,  504 => 254,  494 => 253,  481 => 248,  478 => 247,  466 => 238,  459 => 234,  454 => 231,  448 => 227,  437 => 222,  432 => 219,  428 => 218,  423 => 215,  417 => 211,  402 => 202,  397 => 200,  392 => 198,  383 => 191,  379 => 190,  355 => 173,  344 => 165,  331 => 154,  329 => 153,  312 => 139,  308 => 138,  304 => 137,  296 => 132,  292 => 131,  288 => 130,  274 => 119,  270 => 118,  261 => 112,  257 => 111,  248 => 105,  244 => 104,  235 => 98,  231 => 97,  220 => 89,  209 => 81,  202 => 76,  192 => 69,  181 => 60,  178 => 59,  175 => 58,  172 => 57,  170 => 56,  167 => 55,  165 => 54,  162 => 53,  152 => 52,  104 => 12,  94 => 11,  74 => 9,  63 => 1,  57 => 6,  53 => 5,  51 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% set order = {} %}

{% for child  in form.vars.value.children | sort((a, b) => a.position <=> b.position) %}
    {% set order =  order | merge([child.id]) %}
{% endfor %}

{% block title %}{{ form.vars.method == 'POST' ? 'Create' : 'Update' }} Series{% endblock %}

{% block css %}
    <style>
        .series-option {
            text-align: center;
        }

        .series-option label img {
            max-width: 100%;
            max-height: 300px;
            border: 5px solid transparent;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            cursor: pointer;

        }

        .series-option span {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translateY(-50%) translateX(-50%);
            font-weight: bold;
            font-size: 40px;
            -webkit-text-stroke: 2px black;
        }

        .series-option input {
            display: none;
        }

        .series-option input:checked + img {
            border: 5px dashed #aaa;
        }

        .series-option input:checked + img.cover {
            border: 5px dashed #375a7f !important;
        }
    </style>
{% endblock %}

{% block body %}

    {% form_theme form 'form_div_layout.html.twig' %}

    {% if app.user.isVerified == false %}
        {% include '_partials/resent-confirmation.html.twig' %}
    {% else %}
        {% if form.children | length < settings.min %}

            <div class=\"container center\">

                <div class=\"jumbotron d-flex justify-content-center align-items-center flex-column \">

                    <h3 class=\"text-center text-muted\">You don not have enough published photos!</h3>

                    <small class=\"text-muted mb-3\">Send images to curator or </small>

                    <a href=\"{{ path('web_photo_create') }}\" class=\"btn btn-blue\">Upload</a>

                </div>

            </div>

        {% else %}

            <div class=\"container basic-layout\">

                <hr>

                <h3>{{ form.vars.method == 'POST' ? 'Create' : 'Update' }} Series</h3>

                <hr>

                <div class=\"upload-single\">

                    <div class=\"upload\">

                        {{ form_start(form) }}

                        <div class=\"row\">

                            <div class=\"col-12\">

                                <div class=\"form-group row  pl-3 pr-3\">

                                    {{ form_label(form.category, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                    {{ form_widget(form.category,{attr:{class:'col-md-9 form-control'}}) }}

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    {{ form_label(form.title, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                    {{ form_widget(form.title,{attr:{class:'col-md-9 form-control'}}) }}

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    {{ form_label(form.tags, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                    {{ form_widget(form.tags,{attr:{class:'col-md-9 form-control'}}) }}

                                </div>

                                <div class=\"form-group row pl-3 pr-3\">

                                    {{ form_label(form.description, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                    {{ form_widget(form.description,{attr:{class:'col-md-9 form-control'}}) }}

                                </div>

                                <div class=\"row pl-3\">

                                    <div class=\"col-md-3\"></div>

                                    <div class=\"col-md-9\">

                                        <div class=\"form-check\">
                                            {{ form_widget(form.isNude,{attr:{class:'form-check-input'}}) }}
                                            <label for=\"{{ form.isNude.vars.id }}\">
                                                {{ form.isNude.vars.label }}
                                            </label>
                                        </div>

                                        <div class=\"form-check\">
                                            {{ form_widget(form.isGreyscale,{attr:{class:'form-check-input'}}) }}
                                            <label for=\"{{ form.isGreyscale.vars.id }}\">
                                                {{ form.isGreyscale.vars.label }}
                                            </label>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr>

                        {% if form.children.children is defined %}

                            <div class=\"row\">

                                <div class=\"col-md-12\">

                                    <div class=\"row\">

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-muted\" id=\"selected-images\">Select photos</h5>

                                            <div class=\"text-danger\">{{ form_errors(form.children) | striptags }}</div>

                                        </div>

                                        <div class=\"col-md-6\">

                                            <h5 class=\"text-right text-muted\">

                                                <span class=\"selected-images {{ form.vars.value.children | length  > settings.max ? 'text-danger' : '' }}\">{{ form.vars.value.children | length }}</span>/{{ settings.max }}

                                            </h5>

                                        </div>

                                    </div>

                                    <hr class=\"no-gutter\">

                                </div>

                            </div>

                            <div class=\"row available-photos  d-flex justify-content-center align-items-center\"
                                 style=\"height: 600px;overflow: auto\">

                                {% for child in form.children %}

                                    <div class=\"series-option col-lg-3 col-md-4 col-sm-6 col-12\">

                                        <div class=\"thumb-container\">

                                            <span class=\"order\"></span>

                                            <label for=\"post_children_{{ child.vars.value }}\">

                                                {{ form_widget(child) }}

                                                <img src=\"{{ child.vars.label }}\" alt=\"\">

                                            </label>

                                        </div>

                                    </div>

                                {% endfor %}

                            </div>

                        {% else %}

                            <div class=\"row\">

                                {% for child in post.children %}

                                    <div class=\"col-md-3 mb-3\">

                                        <img src=\"{{ child.image.thumbnail }}\" alt=\"\" height=\"200px\" width=\"100%\" style=\"object-fit: cover\">

                                    </div>

                                {% endfor %}

                            </div>

                        {% endif %}

                        <div class=\"text-center mt-5\">

                            {{ form_widget(form.create,{attr:{class:\"btn btn-blue\"},label: form.vars.method == 'POST' ? 'Create' : 'Update' }) }}

                        </div>

                        {{ form_end(form) }}

                    </div>

                </div>

            </div>

        {% endif %}
    {% endif %}



{% endblock %}

{% block js %}

    <script src=\"{{ asset('js/infinite.js') }}\"></script>

    <script>infinite('.available-photos', '.available-photos');</script>

    <script>

        \$(function () {

            let hiddenOrder = \$('#post_position');

            hiddenOrder.val('{{ order | join(',') }}')

            let order = hiddenOrder.val() ? hiddenOrder.val().split(',') : [];

            let selected = \$('.selected-images');

            function updateNumbers() {
                \$('.series-option input[type=\"checkbox\"]').each(function (index, item) {

                    item = \$(item);

                    let i = order.indexOf(item.val());

                    if (i > -1) {

                        item.siblings('img').removeClass('cover');

                        if (i === 0) {
                            item.siblings('img').addClass('cover');
                        }

                        item.parent().siblings('.order').text(i + 1)
                        item.prop('checked', true)
                    } else {
                        item.parent().siblings('.order').html('')
                    }

                });
            }

            updateNumbers();

            function updateCheckbox(e) {

                e.preventDefault();

                if (e.target.checked) {

                    if (order.length + 1 > {{ settings.max }}) {
                        \$(e.target).prop('checked', false)
                        return false;
                    }

                    order.push(e.target.value);

                    \$(e.target).parent().siblings('.order').text(order.length)

                } else {

                    order = order.filter(function (v) {
                        return v != e.target.value;
                    })

                }

                updateNumbers();

                hiddenOrder.val(order);

                selected.text(order.length);

            }

            \$(document).on('change', '.series-option input[type=\"checkbox\"]', updateCheckbox)

            \$('#reset-selection').on('click', function (e) {
                e.preventDefault();
                \$('.series-option input[type=\"checkbox\"]').prop('checked', false);
                \$('.selected-images').text(0);
                order = [];

                updateNumbers()
            })
        });

        \$(function () {
            let h = \$('#selected-images');

            \$(document).scroll(\$.throttle(250, function (e) {

                if (\$(window).scrollTop() > 700) {

                    h.css({
                        'position': 'fixed',
                        'left': '50%',
                        'top': '100px',
                        'background': '#fff',
                        'zIndex': 100,
                        'transform': 'translateX(-50%)'
                    });
                    h.addClass('shadow rounded p-2')

                } else {

                    h.removeAttr('style');
                    h.removeClass('shadow rounded p-2')

                }
            }))
        });

    </script>
{% endblock %}

", "photo/series-from-published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/series-from-published.html.twig");
    }
}
