<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/template.html.twig */
class __TwigTemplate_c76a48c2909f64a0c975f30d83cdda9d8f3627fca082a0d1162fbf20438f6532 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/template.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/template.html.twig"));

        // line 1
        echo "<p></p>
<div class=\"media pt-3 pl-3 pb-3 border-left mt-5\">
    <div class=\"media-body\">
        <h6 class=\"mb-3\">
            <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("account", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 5, $this->source); })()), "user", [], "any", false, false, false, 5), "slug", [], "any", false, false, false, 5)]), "html", null, true);
        echo "\">
                ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 6, $this->source); })()), "user", [], "any", false, false, false, 6), "profile", [], "any", false, false, false, 6), "firstName", [], "any", false, false, false, 6), "html", null, true);
        echo "
                ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7), "profile", [], "any", false, false, false, 7), "lastName", [], "any", false, false, false, 7), "html", null, true);
        echo "
            </a>

        </h6>
        <div class=\"text-muted float-right\" style=\"float: right;margin: -30px 0 0 \">
            ";
        // line 12
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 12, $this->source); })()), "createdAt", [], "any", false, false, false, 12), "Y-m-d H:i"), "html", null, true);
        echo "
        </div>
        <div>
            ";
        // line 15
        echo twig_get_attribute($this->env, $this->source, (isset($context["opinion"]) || array_key_exists("opinion", $context) ? $context["opinion"] : (function () { throw new RuntimeError('Variable "opinion" does not exist.', 15, $this->source); })()), "text", [], "any", false, false, false, 15);
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "forum/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 15,  65 => 12,  57 => 7,  53 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p></p>
<div class=\"media pt-3 pl-3 pb-3 border-left mt-5\">
    <div class=\"media-body\">
        <h6 class=\"mb-3\">
            <a href=\"{{ path('account',{slug:opinion.user.slug}) }}\">
                {{ opinion.user.profile.firstName }}
                {{ opinion.user.profile.lastName }}
            </a>

        </h6>
        <div class=\"text-muted float-right\" style=\"float: right;margin: -30px 0 0 \">
            {{ opinion.createdAt | date('Y-m-d H:i') }}
        </div>
        <div>
            {{ opinion.text | raw }}
        </div>
    </div>
</div>
", "forum/template.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/forum/template.html.twig");
    }
}
