<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/header.html.twig */
class __TwigTemplate_fa876d1e8caf0fbee7f2bad75e8238a302118941606f7c91635e54ec524a53f3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/header.html.twig"));

        // line 1
        echo "<div class=\"container-fluid profile\">

    <div class=\"row\">

        <div class=\"cover\">

            <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", true, true, false, 7) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", false, false, false, 7)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 7), "cover", [], "any", false, false, false, 7)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/cover.jpg"))), "html", null, true);
        echo "\" alt=\"Profile Cover\">

        </div>

    </div>

</div>

<div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

    <div class=\"row pb-4\">

        <div class=\"col-lg-2\">

            <div class=\"profile-avatar text-center\">

                <div class=\"circle\">

                    <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", true, true, false, 25) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", false, false, false, 25)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 25), "picture", [], "any", false, false, false, 25)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
        echo "\" alt=\"Profile Picture\">
                </div>

            </div>

        </div>

        <div class=\"col-lg-10\">

            ";
        // line 35
        echo "            <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                <h1>
                    <strong>
                        ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 39, $this->source); })()), "profile", [], "any", false, false, false, 39), "firstName", [], "any", false, false, false, 39), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 39, $this->source); })()), "profile", [], "any", false, false, false, 39), "lastName", [], "any", false, false, false, 39), "html", null, true);
        echo "
                    </strong>
                </h1>

                <div class=\"mt-3\">

                    ";
        // line 45
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 45, $this->source); })()), "profile", [], "any", false, false, false, 45), "facebook", [], "any", false, false, false, 45))) {
            // line 46
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 46, $this->source); })()), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 46, $this->source); })()), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 46, $this->source); })()), "profile", [], "any", false, false, false, 46), "facebook", [], "any", false, false, false, 46))));
            // line 47
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 47, $this->source); })()), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-facebook-f\" style=\"color: #3b5998;\"></i></a>
                    ";
        }
        // line 50
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 50, $this->source); })()), "profile", [], "any", false, false, false, 50), "instagram", [], "any", false, false, false, 50))) {
            // line 51
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "profile", [], "any", false, false, false, 51), "instagram", [], "any", false, false, false, 51))));
            // line 52
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 52, $this->source); })()), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-instagram\"></i></a>
                    ";
        }
        // line 55
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 55, $this->source); })()), "profile", [], "any", false, false, false, 55), "twitter", [], "any", false, false, false, 55))) {
            // line 56
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 56, $this->source); })()), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 56, $this->source); })()), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 56, $this->source); })()), "profile", [], "any", false, false, false, 56), "twitter", [], "any", false, false, false, 56))));
            // line 57
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 57, $this->source); })()), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-twitter\" style=\"color: #38A1F3;\"></i></a>
                    ";
        }
        // line 60
        echo "                    ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 60, $this->source); })()), "profile", [], "any", false, false, false, 60), "website", [], "any", false, false, false, 60))) {
            // line 61
            echo "                        ";
            $context["url"] = (((0 === twig_compare(twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 61, $this->source); })()), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61), 0, 4), "http"))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 61, $this->source); })()), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61)) : (("http://" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 61, $this->source); })()), "profile", [], "any", false, false, false, 61), "website", [], "any", false, false, false, 61))));
            // line 62
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 62, $this->source); })()), "html", null, true);
            echo "\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fas fa-globe\"></i></a>
                    ";
        }
        // line 65
        echo "
                    ";
        // line 66
        if (((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 66, $this->source); })()), "user", [], "any", false, false, false, 66) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 66, $this->source); })()), "user", [], "any", false, false, false, 66), "id", [], "any", false, false, false, 66), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 66, $this->source); })()), "id", [], "any", false, false, false, 66)))) && twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 66))) {
            // line 67
            echo "                        <span class=\"text-muted\">
                            <strong>Balance:</strong> ";
            // line 68
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 68, $this->source); })()), "user", [], "any", false, false, false, 68)), "total", [], "any", true, true, false, 68)) ? (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 68, $this->source); })()), "user", [], "any", false, false, false, 68)), "total", [], "any", false, false, false, 68)) : (0)), 2), "html", null, true);
            echo " &euro;
                        </span>
                        ";
            // line 70
            if ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 70, $this->source); })()), "user", [], "any", false, false, false, 70)), "blocked", [], "any", true, true, false, 70) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 70, $this->source); })()), "user", [], "any", false, false, false, 70)), "blocked", [], "any", false, false, false, 70), 0)))) {
                // line 71
                echo "                            |
                            <span class=\"text-muted\">
                            <strong>Awaiting payout:</strong> ";
                // line 73
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\UserBalanceExtension']->getBalance(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 73, $this->source); })()), "user", [], "any", false, false, false, 73)), "blocked", [], "any", false, false, false, 73), 2), "html", null, true);
                echo " &euro;
                        </span>
                        ";
            }
            // line 76
            echo "                    ";
        }
        // line 77
        echo "
                </div>
            </div>

            ";
        // line 82
        echo "            <div class=\"profile-actions mt-3 text-center text-lg-left\">

                ";
        // line 85
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 85, $this->source); })()), "user", [], "any", false, false, false, 85) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 85, $this->source); })()), "user", [], "any", false, false, false, 85), "id", [], "any", false, false, false, 85), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 85, $this->source); })()), "id", [], "any", false, false, false, 85))))) {
            // line 86
            echo "                    <div class=\"circle mr-3\">
                        ";
            // line 87
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Follow", ["rendering" => "client_side", "props" => ["following" => $this->extensions['App\Twig\IsFollowingExtension']->isFollowing(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 87, $this->source); })()), "user", [], "any", false, false, false, 87), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 87, $this->source); })())), "user" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 87, $this->source); })()), "id", [], "any", false, false, false, 87)]]);
            echo "
                    </div>
                ";
        }
        // line 90
        echo "
                ";
        // line 92
        echo "
                ";
        // line 93
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN") && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 93, $this->source); })()), "user", [], "any", false, false, false, 93), "id", [], "any", false, false, false, 93), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 93, $this->source); })()), "id", [], "any", false, false, false, 93))))) {
            // line 94
            echo "
                    <div class=\"circle mr-3\">
                        ";
            // line 96
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
                // line 97
                echo "
                            <a href=\"";
                // line 98
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["_switch_user" => "_exit", "slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 98, $this->source); })()), "slug", [], "any", false, false, false, 98)]), "html", null, true);
                echo "\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Exit from switch\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        ";
            } else {
                // line 107
                echo "
                            <a href=\"";
                // line 108
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["_switch_user" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 108, $this->source); })()), "email", [], "any", false, false, false, 108), "slug" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 108, $this->source); })()), "slug", [], "any", false, false, false, 108)]), "html", null, true);
                echo "\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Switch to user\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        ";
            }
            // line 117
            echo "                    </div>

                ";
        }
        // line 120
        echo "
                ";
        // line 122
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 122, $this->source); })()), "user", [], "any", false, false, false, 122)) {
            // line 123
            echo "                    <div class=\"circle mr-3\">

                        <a href=\"";
            // line 125
            (((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "user", [], "any", false, false, false, 125) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "user", [], "any", false, false, false, 125), "id", [], "any", false, false, false, 125), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 125, $this->source); })()), "id", [], "any", false, false, false, 125))))) ? (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages", ["user" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 125, $this->source); })()), "id", [], "any", false, false, false, 125)]), "html", null, true))) : (print ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_messages"))));
            echo "\"
                           class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Messages\">
                            <i class=\"far fa-envelope-open mr-1 ml-1\"></i>
                        </a>

                    </div>
                ";
        }
        // line 134
        echo "
                ";
        // line 135
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 135, $this->source); })()), "user", [], "any", false, false, false, 135) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 135, $this->source); })()), "user", [], "any", false, false, false, 135), "id", [], "any", false, false, false, 135), twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 135, $this->source); })()), "id", [], "any", false, false, false, 135))))) {
            // line 136
            echo "
                    ";
            // line 138
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 138)) {
                // line 139
                echo "
                        <div class=\"circle mr-3\">

                            <a href=\"";
                // line 142
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_payout");
                echo "\" class=\"profile-icon\" data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Request Payout\">
                                <i class=\"fas fa-wallet mr-1 ml-1\"></i>
                            </a>

                        </div>

                    ";
            }
            // line 151
            echo "
                    ";
            // line 153
            echo "                    <div class=\"circle mr-3\">

                        <a href=\"";
            // line 155
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile_edit");
            echo "\" class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Settings\">
                            <i class=\"fas fa-user-edit ml-1\"></i>
                        </a>

                    </div>

                ";
        }
        // line 164
        echo "
            </div>

        </div>

    </div>

    <div class=\"row pb-4\">

        <div class=\"col-12\">

            ";
        // line 175
        echo twig_include($this->env, $context, "user/profile/navigation.html.twig");
        echo "

        </div>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "user/profile/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  333 => 175,  320 => 164,  308 => 155,  304 => 153,  301 => 151,  289 => 142,  284 => 139,  281 => 138,  278 => 136,  276 => 135,  273 => 134,  261 => 125,  257 => 123,  254 => 122,  251 => 120,  246 => 117,  234 => 108,  231 => 107,  219 => 98,  216 => 97,  214 => 96,  210 => 94,  208 => 93,  205 => 92,  202 => 90,  196 => 87,  193 => 86,  190 => 85,  186 => 82,  180 => 77,  177 => 76,  171 => 73,  167 => 71,  165 => 70,  160 => 68,  157 => 67,  155 => 66,  152 => 65,  145 => 62,  142 => 61,  139 => 60,  132 => 57,  129 => 56,  126 => 55,  119 => 52,  116 => 51,  113 => 50,  106 => 47,  103 => 46,  101 => 45,  90 => 39,  84 => 35,  72 => 25,  51 => 7,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid profile\">

    <div class=\"row\">

        <div class=\"cover\">

            <img src=\"{{ user.profile.cover ?? asset('images/cover.jpg') }}\" alt=\"Profile Cover\">

        </div>

    </div>

</div>

<div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

    <div class=\"row pb-4\">

        <div class=\"col-lg-2\">

            <div class=\"profile-avatar text-center\">

                <div class=\"circle\">

                    <img src=\"{{ user.profile.picture ?? asset('images/avatar.jpg') }}\" alt=\"Profile Picture\">
                </div>

            </div>

        </div>

        <div class=\"col-lg-10\">

            {# Info #}
            <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                <h1>
                    <strong>
                        {{ user.profile.firstName }} {{ user.profile.lastName }}
                    </strong>
                </h1>

                <div class=\"mt-3\">

                    {% if  user.profile.facebook is not empty %}
                        {% set url = user.profile.facebook | slice(0,4) == 'http' ? user.profile.facebook : 'http://' ~ user.profile.facebook %}
                        <a href=\"{{ url }}\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-facebook-f\" style=\"color: #3b5998;\"></i></a>
                    {% endif %}
                    {% if  user.profile.instagram is not empty %}
                        {% set url = user.profile.instagram | slice(0,4) == 'http' ? user.profile.instagram : 'http://' ~ user.profile.instagram %}
                        <a href=\"{{ url }}\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-instagram\"></i></a>
                    {% endif %}
                    {% if  user.profile.twitter is not empty %}
                        {% set url = user.profile.twitter | slice(0,4) == 'http' ? user.profile.twitter : 'http://' ~ user.profile.twitter %}
                        <a href=\"{{ url }}\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fab fa-twitter\" style=\"color: #38A1F3;\"></i></a>
                    {% endif %}
                    {% if  user.profile.website is not empty %}
                        {% set url = user.profile.website | slice(0,4) == 'http' ? user.profile.website : 'http://' ~ user.profile.website %}
                        <a href=\"{{ url }}\" target=\"_blank\" class=\"mr-3\">
                            <i class=\"fas fa-globe\"></i></a>
                    {% endif %}

                    {% if app.user and app.user.id == user.id and config().allowShopping %}
                        <span class=\"text-muted\">
                            <strong>Balance:</strong> {{ (balance(app.user).total is defined ? balance(app.user).total : 0) | number_format(2)  }} &euro;
                        </span>
                        {% if balance(app.user).blocked is defined and  balance(app.user).blocked > 0 %}
                            |
                            <span class=\"text-muted\">
                            <strong>Awaiting payout:</strong> {{ balance(app.user).blocked | number_format(2) }} &euro;
                        </span>
                        {% endif %}
                    {% endif %}

                </div>
            </div>

            {# Actions #}
            <div class=\"profile-actions mt-3 text-center text-lg-left\">

                {# Follow #}
                {% if app.user and app.user.id != user.id %}
                    <div class=\"circle mr-3\">
                        {{ react_component('Follow',{'rendering':'client_side',props:{following: is_following(app.user,user),user: user.id}}) }}
                    </div>
                {% endif %}

                {# Switch #}

                {% if is_granted('ROLE_SUPER_ADMIN') and app.user.id != user.id %}

                    <div class=\"circle mr-3\">
                        {% if is_granted('ROLE_PREVIOUS_ADMIN') %}

                            <a href=\"{{ path('web_profile', {'_switch_user': '_exit',slug: user.slug}) }}\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Exit from switch\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        {% else %}

                            <a href=\"{{ path('web_profile', {'_switch_user': user.email,slug: user.slug}) }}\"
                               class=\"profile-icon\"
                               data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Switch to user\">
                                <i class=\"fa fa-random mr-1 ml-1\"></i>
                            </a>

                        {% endif %}
                    </div>

                {% endif %}

                {# Messages #}
                {% if app.user %}
                    <div class=\"circle mr-3\">

                        <a href=\"{{ app.user and app.user.id != user.id ? path('web_messages',{'user': user.id }) : path('web_messages') }}\"
                           class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Messages\">
                            <i class=\"far fa-envelope-open mr-1 ml-1\"></i>
                        </a>

                    </div>
                {% endif %}

                {% if app.user and app.user.id == user.id %}

                    {# Payout #}
                    {% if config().allowShopping %}

                        <div class=\"circle mr-3\">

                            <a href=\"{{ path('web_profile_payout') }}\" class=\"profile-icon\" data-toggle=\"tooltip\"
                               data-placement=\"bottom\"
                               title=\"Request Payout\">
                                <i class=\"fas fa-wallet mr-1 ml-1\"></i>
                            </a>

                        </div>

                    {% endif %}

                    {# Settiings #}
                    <div class=\"circle mr-3\">

                        <a href=\"{{ path('web_profile_edit') }}\" class=\"profile-icon\" data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           title=\"Settings\">
                            <i class=\"fas fa-user-edit ml-1\"></i>
                        </a>

                    </div>

                {% endif %}

            </div>

        </div>

    </div>

    <div class=\"row pb-4\">

        <div class=\"col-12\">

            {{ include('user/profile/navigation.html.twig') }}

        </div>

    </div>

</div>
", "user/profile/header.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/header.html.twig");
    }
}
