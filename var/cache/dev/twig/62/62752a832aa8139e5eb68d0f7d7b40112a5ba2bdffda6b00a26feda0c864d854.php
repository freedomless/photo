<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/published.html.twig */
class __TwigTemplate_48938f327308441df16b88264a7fc1d9da4129e168a1ec46f17078883a810733 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/published.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/published.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/published.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Published";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"curate-posts basic-layout\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/published.html.twig", 9)->display($context);
        // line 10
        echo "
        <hr>

        ";
        // line 13
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/published.html.twig", 13)->display($context);
        // line 14
        echo "
        <hr>

        <div class=\"d-flex justify-content-center pagination\">
            ";
        // line 18
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 18, $this->source); })()));
        echo "
        </div>

        <div class=\"curate-photos\">

            <div class=\"container photos\">

                <div class=\"row d-flex\">

                    ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 27, $this->source); })()));
        foreach ($context['_seq'] as $context["index"] => $context["post"]) {
            // line 28
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-3 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 35
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "isGreyscale", [], "any", false, false, false, 35), true))) {
                // line 36
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 40
            echo "
                                    ";
            // line 42
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 42))) {
                // line 43
                echo "                                        ";
                $context["comments"] = 0;
                // line 44
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 44));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 45
                    echo "                                            ";
                    $context["comments"] = ((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 45, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 45)));
                    // line 46
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 47
                echo "
                                        ";
                // line 48
                if ((1 === twig_compare((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 48, $this->source); })()), 0))) {
                    // line 49
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 51
                    echo twig_escape_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 51, $this->source); })()), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 55
                echo "
                                    ";
            } else {
                // line 57
                echo "
                                        ";
                // line 58
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 58)), 0))) {
                    // line 59
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 61
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 61)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 65
                echo "
                                    ";
            }
            // line 67
            echo "                                    ";
            // line 68
            echo "
                                    <a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 69)]), "html", null, true);
            echo "\">

                                        ";
            // line 71
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 71), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 71), "thumbnail", [], "any", false, false, false, 71)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 71), "image", [], "any", false, false, false, 71), "thumbnail", [], "any", false, false, false, 71)));
            // line 72
            echo "                                        <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 72, $this->source); })()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 72), "html", null, true);
            echo "\" width=\"auto\"
                                             style=\"max-width:100%;max-height:300px\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">Sent</span>
                                    ";
            // line 81
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "sentForCurateDate", [], "any", false, false, false, 81), "d/M/Y H:i"), "html", null, true);
            echo "
                                </div>

                                <div class=\"card-footer\">

                                    ";
            // line 86
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 86), twig_date_converter($this->env)))) {
                // line 87
                echo "
                                        <div class=\"d-flex text-success\">
                                        <span class=\"flex-grow-1  text-success\">
                                            Published
                                        ";
                // line 91
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 91), (isset($context["site_publish_visibility_delay"]) || array_key_exists("site_publish_visibility_delay", $context) ? $context["site_publish_visibility_delay"] : (function () { throw new RuntimeError('Variable "site_publish_visibility_delay" does not exist.', 91, $this->source); })())), twig_date_converter($this->env)))) {
                    // line 92
                    echo "                                            <div class=\"small text-light muted\">User not notified</div>
                                        ";
                }
                // line 94
                echo "                                        </span>
                                            ";
                // line 95
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 95), "d/M/Y H:i"), "html", null, true);
                echo "
                                        </div>

                                    ";
            }
            // line 99
            echo "
                                    ";
            // line 100
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 100), twig_date_converter($this->env)))) {
                // line 101
                echo "
                                        <div class=\"d-flex text-warning\">
                                        <span class=\"flex-grow-1  text-warning\">
                                            Scheduled
                                           ";
                // line 105
                if ((1 === twig_compare(twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 105), (isset($context["site_publish_visibility_delay"]) || array_key_exists("site_publish_visibility_delay", $context) ? $context["site_publish_visibility_delay"] : (function () { throw new RuntimeError('Variable "site_publish_visibility_delay" does not exist.', 105, $this->source); })())), twig_date_converter($this->env)))) {
                    // line 106
                    echo "                                               <div class=\"small text-light muted\">User not notified</div>
                                           ";
                }
                // line 108
                echo "                                        </span>
                                            ";
                // line 109
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 109), "d/M/Y H:i"), "html", null, true);
                echo "
                                        </div>

                                    ";
            }
            // line 113
            echo "

                                </div>

                                ";
            // line 117
            if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 117)), 0))) {
                // line 118
                echo "
                                    <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">
                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>
                                    </span>
                                        Series
                                    </div>

                                ";
            }
            // line 128
            echo "
                                ";
            // line 129
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "showDate", [], "any", false, false, false, 129), twig_date_converter($this->env)))) {
                // line 130
                echo "
                                    <div class=\"card-footer\">

                                        <div class=\"d-flex justify-content-center\">

                                            <form method=\"post\" class=\"d-flex\">

                                                ";
                // line 137
                if (((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 137, $this->source); })()), "request", [], "any", false, false, false, 137), "attributes", [], "any", false, false, false, 137), "get", [0 => "page"], "method", false, false, false, 137), 1)) || (1 === twig_compare($context["index"], 0)))) {
                    // line 138
                    echo "                                                    <button class=\"btn btn-blue\"
                                                            formaction=\"";
                    // line 139
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published_move", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 139), "up" => 1, "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 139, $this->source); })()), "request", [], "any", false, false, false, 139), "get", [0 => "page"], "method", false, false, false, 139)]), "html", null, true);
                    echo "\">
                                                        <i class=\"fas fa-chevron-left\"></i></button>
                                                ";
                }
                // line 142
                echo "
                                                <input type=\"text\" class=\"form-control\" name=\"places\" id=\"places\"
                                                       placeholder=\"Places\" value=\"1\">

                                                <button class=\"btn btn-blue\"
                                                        formaction=\"";
                // line 147
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published_move", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 147), "up" => 0, "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 147, $this->source); })()), "request", [], "any", false, false, false, 147), "get", [0 => "page"], "method", false, false, false, 147)]), "html", null, true);
                echo "\">
                                                    <i class=\"fas fa-chevron-right\"></i></button>

                                            </form>

                                        </div>

                                    </div>

                                ";
            }
            // line 157
            echo "
                                <div class=\"card-text small text-center\">
                                    <span class=\"text-success\">Publisher:</span>
                                    &nbsp;
                                    ";
            // line 161
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "publisher", [], "any", false, false, false, 161), "profile", [], "any", false, false, false, 161), "fullName", [], "any", false, false, false, 161), "html", null, true);
            echo "
                                </div>

                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center pagination\">
            ";
        // line 175
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 175, $this->source); })()));
        echo "
        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/published.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  383 => 175,  375 => 169,  361 => 161,  355 => 157,  342 => 147,  335 => 142,  329 => 139,  326 => 138,  324 => 137,  315 => 130,  313 => 129,  310 => 128,  298 => 118,  296 => 117,  290 => 113,  283 => 109,  280 => 108,  276 => 106,  274 => 105,  268 => 101,  266 => 100,  263 => 99,  256 => 95,  253 => 94,  249 => 92,  247 => 91,  241 => 87,  239 => 86,  231 => 81,  216 => 72,  214 => 71,  209 => 69,  206 => 68,  204 => 67,  200 => 65,  193 => 61,  189 => 59,  187 => 58,  184 => 57,  180 => 55,  173 => 51,  169 => 49,  167 => 48,  164 => 47,  158 => 46,  155 => 45,  150 => 44,  147 => 43,  144 => 42,  141 => 40,  135 => 36,  133 => 35,  124 => 28,  120 => 27,  108 => 18,  102 => 14,  100 => 13,  95 => 10,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Published{% endblock %}

{% block body %}

    <div class=\"curate-posts basic-layout\">

        {% include 'admin/curator/_partials/curator_nav.html.twig' %}

        <hr>

        {% include 'admin/curator/_partials/filter.html.twig' %}

        <hr>

        <div class=\"d-flex justify-content-center pagination\">
            {{ knp_pagination_render(posts) }}
        </div>

        <div class=\"curate-photos\">

            <div class=\"container photos\">

                <div class=\"row d-flex\">

                    {% for index,post in posts %}

                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-3 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    {% if post.isGreyscale == true %}
                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    {% endif %}

                                    {# Comments count START #}
                                    {% if post.cover is not null %}
                                        {% set comments = 0 %}
                                        {% for child in post.children %}
                                            {% set comments = comments + child.curateComments|length %}
                                        {% endfor %}

                                        {% if comments > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ comments }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% else %}

                                        {% if post.curateComments|length > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ post.curateComments|length }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% endif %}
                                    {# Comments count END #}

                                    <a href=\"{{ path('admin_photo_show', {id: post.id}) }}\">

                                        {% set image = post.type == 1 ? post.image.thumbnail : post.cover.image.thumbnail %}
                                        <img src=\"{{ image }}\" alt=\"{{ post.title }}\" width=\"auto\"
                                             style=\"max-width:100%;max-height:300px\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">Sent</span>
                                    {{ post.sentForCurateDate | date('d/M/Y H:i') }}
                                </div>

                                <div class=\"card-footer\">

                                    {% if post.showDate < date() %}

                                        <div class=\"d-flex text-success\">
                                        <span class=\"flex-grow-1  text-success\">
                                            Published
                                        {% if post.curatedAt|date_modify(site_publish_visibility_delay) > date() %}
                                            <div class=\"small text-light muted\">User not notified</div>
                                        {% endif %}
                                        </span>
                                            {{ post.showDate | date('d/M/Y H:i') }}
                                        </div>

                                    {% endif %}

                                    {% if post.showDate > date() %}

                                        <div class=\"d-flex text-warning\">
                                        <span class=\"flex-grow-1  text-warning\">
                                            Scheduled
                                           {% if post.curatedAt|date_modify(site_publish_visibility_delay) > date() %}
                                               <div class=\"small text-light muted\">User not notified</div>
                                           {% endif %}
                                        </span>
                                            {{ post.showDate | date('d/M/Y H:i') }}
                                        </div>

                                    {% endif %}


                                </div>

                                {% if post.children | length > 0 %}

                                    <div class=\"card-footer d-flex\">
                                    <span class=\"flex-grow-1\">
                                        <i class=\"fas fa-images\" data-toggle=\"tooltip\" title=\"\"
                                           data-original-title=\"Series\"></i>
                                    </span>
                                        Series
                                    </div>

                                {% endif %}

                                {% if post.showDate > date() %}

                                    <div class=\"card-footer\">

                                        <div class=\"d-flex justify-content-center\">

                                            <form method=\"post\" class=\"d-flex\">

                                                {% if app.request.attributes.get('page') != 1 or index > 0 %}
                                                    <button class=\"btn btn-blue\"
                                                            formaction=\"{{ path('admin_photo_published_move',{id:post.id,up:1,page:app.request.get('page')}) }}\">
                                                        <i class=\"fas fa-chevron-left\"></i></button>
                                                {% endif %}

                                                <input type=\"text\" class=\"form-control\" name=\"places\" id=\"places\"
                                                       placeholder=\"Places\" value=\"1\">

                                                <button class=\"btn btn-blue\"
                                                        formaction=\"{{ path('admin_photo_published_move',{id:post.id,up:0,page:app.request.get('page')}) }}\">
                                                    <i class=\"fas fa-chevron-right\"></i></button>

                                            </form>

                                        </div>

                                    </div>

                                {% endif %}

                                <div class=\"card-text small text-center\">
                                    <span class=\"text-success\">Publisher:</span>
                                    &nbsp;
                                    {{ post.publisher.profile.fullName }}
                                </div>

                            </div>

                        </div>

                    {% endfor %}
                </div>

            </div>
        </div>

        <div class=\"d-flex justify-content-center pagination\">
            {{ knp_pagination_render(posts) }}
        </div>

    </div>

{% endblock %}

", "admin/curator/published.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/published.html.twig");
    }
}
