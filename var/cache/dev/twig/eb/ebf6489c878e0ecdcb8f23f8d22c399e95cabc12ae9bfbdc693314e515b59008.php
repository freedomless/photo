<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/author-details.html.twig */
class __TwigTemplate_3ee502a6ae074b3993c4db80f5287dc10778c50bad6213ed0f55878dc1d86a6c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/author-details.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/author-details.html.twig"));

        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header text-info\">

        <strong class=\"h4\">";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 5, $this->source); })()), "user", [], "any", false, false, false, 5), "profile", [], "any", false, false, false, 5), "firstName", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 5, $this->source); })()), "user", [], "any", false, false, false, 5), "profile", [], "any", false, false, false, 5), "lastName", [], "any", false, false, false, 5), "html", null, true);
        echo "</strong>

        <div class=\"float-right\">
            ";
        // line 8
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 8, $this->source); })()), "type", [], "any", false, false, false, 8), 1))) {
            // line 9
            echo "
                <div class=\"text-center\">

                    ";
            // line 12
            if ( !(isset($context["hasBeenPhotoOfTheDay"]) || array_key_exists("hasBeenPhotoOfTheDay", $context) ? $context["hasBeenPhotoOfTheDay"] : (function () { throw new RuntimeError('Variable "hasBeenPhotoOfTheDay" does not exist.', 12, $this->source); })())) {
                // line 13
                echo "
                        <form method=\"POST\">
                            <button formaction=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_create_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15)]), "html", null, true);
                echo "\"
                                    class=\"btn btn-sm btn-success\">
                                Make Photo of the day
                            </button>
                        </form>

                    ";
            } else {
                // line 22
                echo "
                        <span class=\"text-muted\">This photo was already <strong>Photo of the Day</strong></span>

                    ";
            }
            // line 26
            echo "
                </div>

            ";
        }
        // line 30
        echo "        </div>

    </div>

    <div class=\"card-body p-0\">

        <div class=\"m-3\">
            ";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 37, $this->source); })()), "user", [], "any", false, false, false, 37), "email", [], "any", false, false, false, 37), "html", null, true);
        echo "
        </div>

        <div class=\"m-3\">

            <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_for_curate", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 42, $this->source); })()), "user", [], "any", false, false, false, 42), "username", [], "any", false, false, false, 42)]), "html", null, true);
        echo "\" class=\"btn btn-outline-warning\">Pending</a>

            <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_published", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 44, $this->source); })()), "user", [], "any", false, false, false, 44), "username", [], "any", false, false, false, 44)]), "html", null, true);
        echo "\" class=\"btn btn-outline-success\">Published</a>

            <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_rejected", ["filters[username]" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 46, $this->source); })()), "user", [], "any", false, false, false, 46), "username", [], "any", false, false, false, 46)]), "html", null, true);
        echo "\" class=\"btn btn-outline-danger\">Rejected</a>

        </div>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/author-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 46,  114 => 44,  109 => 42,  101 => 37,  92 => 30,  86 => 26,  80 => 22,  70 => 15,  66 => 13,  64 => 12,  59 => 9,  57 => 8,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card mt-3\">

    <div class=\"card-header text-info\">

        <strong class=\"h4\">{{ post.user.profile.firstName }} {{ post.user.profile.lastName }}</strong>

        <div class=\"float-right\">
            {% if post.type == 1 %}

                <div class=\"text-center\">

                    {% if not hasBeenPhotoOfTheDay %}

                        <form method=\"POST\">
                            <button formaction=\"{{ path('admin_create_photo_of_the_day',{id:post.id}) }}\"
                                    class=\"btn btn-sm btn-success\">
                                Make Photo of the day
                            </button>
                        </form>

                    {% else %}

                        <span class=\"text-muted\">This photo was already <strong>Photo of the Day</strong></span>

                    {% endif %}

                </div>

            {% endif %}
        </div>

    </div>

    <div class=\"card-body p-0\">

        <div class=\"m-3\">
            {{ post.user.email }}
        </div>

        <div class=\"m-3\">

            <a href=\"{{ path('admin_photo_for_curate',{'filters[username]': post.user.username}) }}\" class=\"btn btn-outline-warning\">Pending</a>

            <a href=\"{{ path('admin_photo_published', {'filters[username]': post.user.username}) }}\" class=\"btn btn-outline-success\">Published</a>

            <a href=\"{{ path('admin_photo_rejected', {'filters[username]': post.user.username}) }}\" class=\"btn btn-outline-danger\">Rejected</a>

        </div>

    </div>

</div>
", "admin/curator/_partials/author-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/author-details.html.twig");
    }
}
