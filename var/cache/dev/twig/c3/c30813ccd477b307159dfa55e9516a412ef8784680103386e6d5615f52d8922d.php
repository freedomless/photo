<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/product-details.html.twig */
class __TwigTemplate_7bd60bfe51fdce3a217a841a7543762fa929bd40e49245ad1949954585433b89 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/product-details.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/product-details.html.twig"));

        $this->parent = $this->loadTemplate("template.html.twig", "pages/product-details.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Product Details";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        echo "Product Details";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        // line 10
        echo "
    <div class=\"col-md-12\">

        <p>
            Each artwork you choose will be printed on the latest professional Epson printer model.
            Print will be on the media of your choice with the original Epson inks.
            The printed media will be fixed on Alu-Dibond. (see the pictures bellow for detailed view)
        </p>

        <div class=\"row mt-3\">
            <div class=\"col-md-4\" style=\"min-height: 200px\">
                <div class=\"video-container\">
                    <iframe src=\"https://www.youtube.com/embed/upA_HjZUuyA\" frameborder=\"0\" class=\"video\"
                            allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\"
                            allowfullscreen></iframe>
                </div>
            </div>

            <div class=\"col-md-4\">
                <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_000.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                <img src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_000.jpg"), "html", null, true);
        echo "\"
                     alt=\"Product\"
                     class=\"product-details-image\">
                </a>
            </div>

            <div class=\"col-md-4\">
                <a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_002.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_002.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>
        <hr>
        <div class=\"row mb-3\">
            <div class=\"col-md-4\">
                <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_003.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_003.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_004.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_004.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_005.jpg"), "html", null, true);
        echo "\" target=\"_blank\">
                    <img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/product/product_005.jpg"), "html", null, true);
        echo "\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>

        <p>
            Alu-Dibond is a photo medium made from aluminum.
        </p>
        <p>
            Is a high-quality, dimensionally stable image medium which is ideal for showing off professional photography
            exquisitely and durably.
        </p>


        <p>
            On the back of the board there will be an additional aluminum frame with a wall mounting element.
            <br>
            You can choose from 5 media that offer the best gallery quality:
        </p>


        <p>
        <ul>

            <li>
                <strong>Matte</strong> - Epson Enhanced Matte Photo Paper 189g/m². No light glare and reflections.
                <a href=\"https://www.epson.eu/products/consumables/paper/enhanced-matte-paper-24-x-305-m-189gm-c13s041595\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Luster</strong> - Epson Premium Luster Photo Paper 260g/m². Semi gloss surface. Favoured by
                professional
                photographers.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-luster-photo-paper-24-x-305-m-260gm-c13s042081\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Glossy</strong> - Epson Premium Glossy Photo Paper 260g/m². Glossy surface. Very close to real
                photo paper.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-glossy-photo-paper-roll-24-x-305-m-260gm-c13s041638#products\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Smooth</strong> - Epson Fine Art Cotton Smooth 300 g/m². Exceptional colour and
                detail. Archival
                quality.
                Suitable for works with big smooth areas.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-smooth-bright-24-x-15m-c13s450271\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Textured</strong> - Epson Fine Art Cotton Textured 300 g/m². Exceptional colour
                and detail.
                Archival
                quality. Тextured surface for fine art reproduction.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-textured-bright-24-x-15m-c13s450285\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
        </ul>
        </p>


        Each product will have free delivery in Europe with big discounts for buying more than one product!

        </p>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "pages/product-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 59,  201 => 58,  193 => 53,  189 => 52,  181 => 47,  177 => 46,  166 => 38,  162 => 37,  152 => 30,  148 => 29,  127 => 10,  117 => 9,  99 => 7,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'template.html.twig' %}

{% block title %}Product Details{% endblock %}

{% block header_left %}Product Details{% endblock %}

{% block header_right %}{% endblock %}

{% block main_content %}

    <div class=\"col-md-12\">

        <p>
            Each artwork you choose will be printed on the latest professional Epson printer model.
            Print will be on the media of your choice with the original Epson inks.
            The printed media will be fixed on Alu-Dibond. (see the pictures bellow for detailed view)
        </p>

        <div class=\"row mt-3\">
            <div class=\"col-md-4\" style=\"min-height: 200px\">
                <div class=\"video-container\">
                    <iframe src=\"https://www.youtube.com/embed/upA_HjZUuyA\" frameborder=\"0\" class=\"video\"
                            allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\"
                            allowfullscreen></iframe>
                </div>
            </div>

            <div class=\"col-md-4\">
                <a href=\"{{ asset('images/product/product_000.jpg') }}\" target=\"_blank\">
                <img src=\"{{ asset('images/product/product_000.jpg') }}\"
                     alt=\"Product\"
                     class=\"product-details-image\">
                </a>
            </div>

            <div class=\"col-md-4\">
                <a href=\"{{ asset('images/product/product_002.jpg') }}\" target=\"_blank\">
                    <img src=\"{{ asset('images/product/product_002.jpg') }}\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>
        <hr>
        <div class=\"row mb-3\">
            <div class=\"col-md-4\">
                <a href=\"{{ asset('images/product/product_003.jpg') }}\" target=\"_blank\">
                    <img src=\"{{ asset('images/product/product_003.jpg') }}\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"{{ asset('images/product/product_004.jpg') }}\" target=\"_blank\">
                    <img src=\"{{ asset('images/product/product_004.jpg') }}\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
            <div class=\"col-md-4\">
                <a href=\"{{ asset('images/product/product_005.jpg') }}\" target=\"_blank\">
                    <img src=\"{{ asset('images/product/product_005.jpg') }}\" alt=\"Product\"
                         class=\"product-details-image\">
                </a>
            </div>
        </div>

        <p>
            Alu-Dibond is a photo medium made from aluminum.
        </p>
        <p>
            Is a high-quality, dimensionally stable image medium which is ideal for showing off professional photography
            exquisitely and durably.
        </p>


        <p>
            On the back of the board there will be an additional aluminum frame with a wall mounting element.
            <br>
            You can choose from 5 media that offer the best gallery quality:
        </p>


        <p>
        <ul>

            <li>
                <strong>Matte</strong> - Epson Enhanced Matte Photo Paper 189g/m². No light glare and reflections.
                <a href=\"https://www.epson.eu/products/consumables/paper/enhanced-matte-paper-24-x-305-m-189gm-c13s041595\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Luster</strong> - Epson Premium Luster Photo Paper 260g/m². Semi gloss surface. Favoured by
                professional
                photographers.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-luster-photo-paper-24-x-305-m-260gm-c13s042081\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Glossy</strong> - Epson Premium Glossy Photo Paper 260g/m². Glossy surface. Very close to real
                photo paper.
                <a href=\"https://www.epson.eu/products/consumables/paper/premium-glossy-photo-paper-roll-24-x-305-m-260gm-c13s041638#products\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Smooth</strong> - Epson Fine Art Cotton Smooth 300 g/m². Exceptional colour and
                detail. Archival
                quality.
                Suitable for works with big smooth areas.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-smooth-bright-24-x-15m-c13s450271\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
            <li>
                <strong>Fine Art Cotton Textured</strong> - Epson Fine Art Cotton Textured 300 g/m². Exceptional colour
                and detail.
                Archival
                quality. Тextured surface for fine art reproduction.
                <a href=\"https://www.epson.eu/products/consumables/paper/fine-art-cotton-textured-bright-24-x-15m-c13s450285\"
                   class=\"text-info\" target=\"_blank\">
                    Read More
                </a>
            </li>
        </ul>
        </p>


        Each product will have free delivery in Europe with big discounts for buying more than one product!

        </p>

    </div>

{% endblock %}
", "pages/product-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/pages/product-details.html.twig");
    }
}
