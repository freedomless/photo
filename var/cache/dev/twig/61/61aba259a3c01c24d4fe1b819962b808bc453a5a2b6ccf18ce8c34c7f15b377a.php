<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/_partials/rejected.html.twig */
class __TwigTemplate_e59a04f15ece1b5641f0a99f46ca73a0531404f4efcca640d22ee234a5c3c264 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/_partials/rejected.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/_partials/rejected.html.twig"));

        // line 1
        echo "<p>You photo was rejected for store</p>

<div class=\"row\">
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Actual</h4>
        <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "post", [], "any", false, false, false, 6), "image", [], "any", false, false, false, 6), "thumbnail", [], "any", false, false, false, 6), "html", null, true);
        echo "\" alt=\"\" width=\"200px\">
    </div>
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Uploaded</h4>
        <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", true, true, false, 10) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", false, false, false, 10)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "post", [], "any", false, true, false, 10), "image", [], "any", false, true, false, 10), "printThumbnail", [], "any", false, false, false, 10)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "post", [], "any", false, false, false, 10), "image", [], "any", false, false, false, 10), "thumbnail", [], "any", false, false, false, 10))), "html", null, true);
        echo "\" alt=\"\" width=\"200px\">
    </div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "product/_partials/rejected.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 10,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p>You photo was rejected for store</p>

<div class=\"row\">
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Actual</h4>
        <img src=\"{{ product.post.image.thumbnail }}\" alt=\"\" width=\"200px\">
    </div>
    <div class=\"col-6\">
        <h4 class=\"text-muted\">Uploaded</h4>
        <img src=\"{{ product.post.image.printThumbnail ??  product.post.image.thumbnail }}\" alt=\"\" width=\"200px\">
    </div>
</div>
", "product/_partials/rejected.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/_partials/rejected.html.twig");
    }
}
