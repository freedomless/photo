<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maintenance.html */
class __TwigTemplate_6a4061916a86b4fc93e583f0bf811ee8032e073b1527f5313f141d2ba23d9e4a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maintenance.html"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maintenance.html"));

        // line 1
        echo "<!DOCTYPE html>
<head>
    <meta charset=\"utf-8\">
    <title>Site temporarily under maintenance</title>
    <link rel=\"stylesheet\" href=\"/build/app.css\">
</head>
<body class=\"vh-100\">
    <div class=\"center flex-column\">
        <div class=\"bg-primary circle p-3\">
            <img src=\"/images/logo.png\" alt=\"Logo\">
        </div>
        <h1>Site temporarily under maintenance</h1>
        <p>Sorry for the inconvenience, we will get back soon !</p>
    </div>
</body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maintenance.html";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<head>
    <meta charset=\"utf-8\">
    <title>Site temporarily under maintenance</title>
    <link rel=\"stylesheet\" href=\"/build/app.css\">
</head>
<body class=\"vh-100\">
    <div class=\"center flex-column\">
        <div class=\"bg-primary circle p-3\">
            <img src=\"/images/logo.png\" alt=\"Logo\">
        </div>
        <h1>Site temporarily under maintenance</h1>
        <p>Sorry for the inconvenience, we will get back soon !</p>
    </div>
</body>
</html>

", "maintenance.html", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/maintenance.html");
    }
}
