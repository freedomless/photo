<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/award.html.twig */
class __TwigTemplate_13fcdbc2c8a0d001410e6ff72a9e1b4a2e2223c59931f9ff69a79b975f3e83b3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/award.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/award.html.twig"));

        // line 1
        if ((array_key_exists("photo_of_the_day", $context) &&  !(null === (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 1, $this->source); })())))) {
            // line 2
            echo "
    ";
            // line 4
            echo "    ";
            // line 5
            echo "    ";
            // line 6
            echo "    ";
            // line 7
            echo "    ";
            // line 8
            echo "
    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_show", ["page" => "awarded", "id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 15, $this->source); })()), "post", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)]), "html", null, true);
            echo "\">

                    ";
            // line 18
            echo "                    ";
            // line 19
            echo "                    ";
            // line 20
            echo "                    ";
            // line 21
            echo "                    ";
            // line 22
            echo "
                    <img src=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 23, $this->source); })()), "post", [], "any", false, false, false, 23), "image", [], "any", false, false, false, 23), "small", [], "any", false, false, false, 23), "html", null, true);
            echo "\"
                         alt=\"Photo of the day!\">

                    <div class=\"author\">

                        <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 29
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 29, $this->source); })()), "post", [], "any", false, false, false, 29), "user", [], "any", false, false, false, 29), "slug", [], "any", false, false, false, 29)]), "html", null, true);
            // line 30
            echo "\">
                            &copy;
                            ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 32, $this->source); })()), "post", [], "any", false, false, false, 32), "user", [], "any", false, false, false, 32), "profile", [], "any", false, false, false, 32), "first_name", [], "any", false, false, false, 32), "html", null, true);
            echo "
                            ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 33, $this->source); })()), "post", [], "any", false, false, false, 33), "user", [], "any", false, false, false, 33), "profile", [], "any", false, false, false, 33), "last_name", [], "any", false, false, false, 33), "html", null, true);
            echo "
                        </a>

                    </div>

                </a>

            </div>

            <div class=\"social\">

                <span class=\"photo-of-the-day-title\">PHOTO OF THE DAY&nbsp;&nbsp;<span class=\"date\">
                        ";
            // line 45
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 45, $this->source); })()), "created_at", [], "any", false, false, false, 45), "d M Y"), "html", null, true);
            echo "</span>
                </span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-drop-right-share\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share It!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-drop-right-share\">
                        ";
            // line 56
            echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 58
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 58, $this->source); })()), "id", [], "any", false, false, false, 58), "date" => twig_get_attribute($this->env, $this->source,             // line 59
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 59, $this->source); })()), "urlized_date", [], "any", false, false, false, 59)]), "classes" => "pr-2"]);
            // line 62
            echo "

                        ";
            // line 64
            echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 66
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 66, $this->source); })()), "id", [], "any", false, false, false, 66), "date" => twig_get_attribute($this->env, $this->source,             // line 67
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 67, $this->source); })()), "urlized_date", [], "any", false, false, false, 67)]), "media" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 69
($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", true, true, false, 69) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", false, false, false, 69)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["photo_of_the_day"] ?? null), "post", [], "any", false, true, false, 69), "image", [], "any", false, true, false, 69), "social", [], "any", false, false, false, 69)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 69, $this->source); })()), "post", [], "any", false, false, false, 69), "image", [], "any", false, false, false, 69), "small", [], "any", false, false, false, 69))), "description" => "Photo of the Day!", "classes" => "pr-2"]);
            // line 72
            echo "

                        ";
            // line 74
            echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_photo_of_the_day", ["id" => twig_get_attribute($this->env, $this->source,             // line 76
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 76, $this->source); })()), "id", [], "any", false, false, false, 76), "date" => twig_get_attribute($this->env, $this->source,             // line 77
(isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 77, $this->source); })()), "urlized_date", [], "any", false, false, false, 77)])]);
            // line 79
            echo "
                    </div>

                </span>

            </div>

            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

            <!--
            <div class=\"photo-of-the-day-title\" style=\"text-transform: none\">Our best wishes to all PiART
                photographers.<br>
                Merry Christmas!
            </div>
            -->

        </div>

    </div>

    ";
            // line 100
            echo "    ";
            // line 101
            echo "    ";
            // line 102
            echo "    ";
            // line 103
            echo "    ";
            // line 104
            echo "
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/award.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 104,  178 => 103,  176 => 102,  174 => 101,  172 => 100,  150 => 79,  148 => 77,  147 => 76,  146 => 74,  142 => 72,  140 => 69,  139 => 67,  138 => 66,  137 => 64,  133 => 62,  131 => 59,  130 => 58,  129 => 56,  115 => 45,  100 => 33,  96 => 32,  92 => 30,  90 => 29,  89 => 28,  81 => 23,  78 => 22,  76 => 21,  74 => 20,  72 => 19,  70 => 18,  65 => 15,  56 => 8,  54 => 7,  52 => 6,  50 => 5,  48 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if photo_of_the_day is defined and photo_of_the_day is not null %}

    {#    <div class=\"row\" style=\"margin-bottom: 3rem\">#}
    {#        <div class=\"col-12\">#}
    {#            <div class=\"horizontal-divider\"></div>#}
    {#        </div>#}
    {#    </div>#}

    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"{{ path('web_photo_show', {page: 'awarded', id: photo_of_the_day.post.id}) }}\">

                    {#                    <div class=\"label\">#}
                    {#                        Photo of the day!#}
                    {#                        <span style=\"opacity: .65; font-size: .9rem\">#}
                    {#                            <span class=\"d-none d-sm-inline\">{{ photo_of_the_day.createdAt|date('d M Y') }}</span></span>#}
                    {#                    </div>#}

                    <img src=\"{{ photo_of_the_day.post.image.small }}\"
                         alt=\"Photo of the day!\">

                    <div class=\"author\">

                        <a href=\"{{ path('web_profile', {
                            'slug': photo_of_the_day.post.user.slug
                        }) }}\">
                            &copy;
                            {{ photo_of_the_day.post.user.profile.first_name }}
                            {{ photo_of_the_day.post.user.profile.last_name }}
                        </a>

                    </div>

                </a>

            </div>

            <div class=\"social\">

                <span class=\"photo-of-the-day-title\">PHOTO OF THE DAY&nbsp;&nbsp;<span class=\"date\">
                        {{ photo_of_the_day.created_at|date('d M Y') }}</span>
                </span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-drop-right-share\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share It!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-drop-right-share\">
                        {{ include('_partials/buttons/_facebook_share.html.twig', {
                            'share_url': url('web_photo_of_the_day', {
                                'id': photo_of_the_day.id,
                                'date': photo_of_the_day.urlized_date,
                            }),
                            'classes': 'pr-2'
                        }) }}

                        {{ include('_partials/buttons/_pinterest_share.html.twig', {
                            'share_url': url('web_photo_of_the_day', {
                                'id': photo_of_the_day.id,
                                'date': photo_of_the_day.urlized_date,
                            }),
                            'media': photo_of_the_day.post.image.social ?? photo_of_the_day.post.image.small,
                            'description': 'Photo of the Day!',
                            'classes': 'pr-2'
                        }) }}

                        {{ include('_partials/buttons/_twitter_share.html.twig', {
                            'share_url': url('web_photo_of_the_day', {
                                'id': photo_of_the_day.id,
                                'date': photo_of_the_day.urlized_date,
                            }),
                        }) }}
                    </div>

                </span>

            </div>

            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

            <!--
            <div class=\"photo-of-the-day-title\" style=\"text-transform: none\">Our best wishes to all PiART
                photographers.<br>
                Merry Christmas!
            </div>
            -->

        </div>

    </div>

    {#    <div class=\"row mt-5\">#}
    {#        <div class=\"col-12\">#}
    {#            <div class=\"horizontal-divider\"></div>#}
    {#        </div>#}
    {#    </div>#}

{% endif %}
", "home/award.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/award.html.twig");
    }
}
