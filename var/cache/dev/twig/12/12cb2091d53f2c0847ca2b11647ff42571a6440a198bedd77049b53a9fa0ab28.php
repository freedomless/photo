<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/show.html.twig */
class __TwigTemplate_693807fd67ad7ce1a191b6cbc04c668ab0964154d9eb7d001bc55890071eb259 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "forum/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["section"]) || array_key_exists("section", $context) ? $context["section"] : (function () { throw new RuntimeError('Variable "section" does not exist.', 4, $this->source); })()), "name", [], "any", false, false, false, 4), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
    <div class=\"container basic-layout\">

        <div class=\"row mt-3\">
            <div class=\"col-10\">

                ";
        // line 14
        $this->loadTemplate("forum/nav.html.twig", "forum/show.html.twig", 14)->display(twig_array_merge($context, ["links" => [0 => ["title" => twig_get_attribute($this->env, $this->source,         // line 17
(isset($context["section"]) || array_key_exists("section", $context) ? $context["section"] : (function () { throw new RuntimeError('Variable "section" does not exist.', 17, $this->source); })()), "name", [], "any", false, false, false, 17), "url" => ""]]]));
        // line 20
        echo "
            </div>

            <div class=\"col-2 text-right\">
                <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_topic_create", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["section"]) || array_key_exists("section", $context) ? $context["section"] : (function () { throw new RuntimeError('Variable "section" does not exist.', 24, $this->source); })()), "slug", [], "any", false, false, false, 24)]), "html", null, true);
        echo "\"
                   class=\"btn btn-blue text-white\">
                    Add New Topic
                </a>
            </div>

            <hr>

        </div>

        ";
        // line 34
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 34, $this->source); })())), 0))) {
            // line 35
            echo "
            <div class=\"table-responsive forum-topics\">

                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Topic</th>
                        <th>Replies</th>
                        <th>Author</th>
                        ";
            // line 44
            if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 44, $this->source); })()), "user", [], "any", false, false, false, 44) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
                // line 45
                echo "                            <th></th>
                        ";
            }
            // line 47
            echo "                    </tr>
                    </thead>

                    <tbody>

                    ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 52, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["topic"]) {
                // line 53
                echo "
                        <tr>

                            <td>

                                <a href=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_section_opinion", ["slug" => twig_get_attribute($this->env, $this->source, $context["topic"], "slug", [], "any", false, false, false, 58)]), "html", null, true);
                echo "\">
                                    <h5>";
                // line 59
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "title", [], "any", false, false, false, 59), "html", null, true);
                echo "</h5>
                                </a>

                                <div class=\"small text-muted\">
                                    Created on: ";
                // line 63
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "createdAt", [], "any", false, false, false, 63), "Y-m-d H:i"), "html", null, true);
                echo "
                                </div>

                                <div class=\"small\">
                                    ";
                // line 67
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 67), "last", [], "any", false, false, false, 67)) {
                    // line 68
                    echo "                                        Last reply on
                                        ";
                    // line 69
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 69), "last", [], "any", false, false, false, 69), "createdAt", [], "any", false, false, false, 69), "Y-m-d H:i"), "html", null, true);
                    echo "
                                        by
                                        <a href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 71), "last", [], "any", false, false, false, 71), "user", [], "any", false, false, false, 71), "slug", [], "any", false, false, false, 71)]), "html", null, true);
                    echo "\">
                                            ";
                    // line 72
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 72), "last", [], "any", false, false, false, 72), "user", [], "any", false, false, false, 72), "profile", [], "any", false, false, false, 72), "firstName", [], "any", false, false, false, 72), "html", null, true);
                    echo "
                                            ";
                    // line 73
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 73), "last", [], "any", false, false, false, 73), "user", [], "any", false, false, false, 73), "profile", [], "any", false, false, false, 73), "lastName", [], "any", false, false, false, 73), "html", null, true);
                    echo "
                                        </a>
                                    ";
                }
                // line 76
                echo "                                </div>
                            </td>

                            <td>";
                // line 79
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "opinions", [], "any", false, false, false, 79)), "html", null, true);
                echo "</td>

                            <td>
                                <a href=\"";
                // line 82
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, false, false, 82), "slug", [], "any", false, false, false, 82)]), "html", null, true);
                echo "\">
                                    <img src=\"";
                // line 83
                echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, true, false, 83), "profile", [], "any", false, true, false, 83), "picture", [], "any", true, true, false, 83) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, true, false, 83), "profile", [], "any", false, true, false, 83), "picture", [], "any", false, false, false, 83)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, true, false, 83), "profile", [], "any", false, true, false, 83), "picture", [], "any", false, false, false, 83)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
                echo "\"
                                         alt=\"";
                // line 84
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, false, false, 84), "profile", [], "any", false, false, false, 84), "firstName", [], "any", false, false, false, 84), "html", null, true);
                echo "\" width=\"50px\" height=\"50px\"
                                         class=\"rounded-circle mb-2 shadow-sm\">
                                    ";
                // line 86
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, false, false, 86), "profile", [], "any", false, false, false, 86), "firstName", [], "any", false, false, false, 86), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["topic"], "owner", [], "any", false, false, false, 86), "profile", [], "any", false, false, false, 86), "lastName", [], "any", false, false, false, 86), "html", null, true);
                echo "
                                </a>
                            </td>

                            ";
                // line 90
                if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 90, $this->source); })()), "user", [], "any", false, false, false, 90) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
                    // line 91
                    echo "
                                <td>
                                    <a href=\"";
                    // line 93
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_topic_update", ["slug" => twig_get_attribute($this->env, $this->source, $context["topic"], "slug", [], "any", false, false, false, 93)]), "html", null, true);
                    echo "\"
                                       class=\"text-white btn btn-success\">
                                        <i class=\"fa fa-edit\"></i>
                                    </a>
                                    <button class=\"btn btn-danger delete-opinion\"
                                            data-url=\"";
                    // line 98
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_topic_delete", ["slug" => twig_get_attribute($this->env, $this->source, $context["topic"], "slug", [], "any", false, false, false, 98)]), "html", null, true);
                    echo "\"
                                            data-toggle=\"modal\"
                                            data-target=\"#delete-opinion\">
                                        <i class=\"fa fa-times\"></i>
                                    </button>
                                </td>

                            ";
                }
                // line 106
                echo "
                        </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['topic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "
                    </tbody>

                </table>

            </div>

        ";
        } else {
            // line 118
            echo "            <h3 class=\"text-muted text-center mt-5 pt-5\">
                No topics
            </h3>
        ";
        }
        // line 122
        echo "        <div class=\"d-flex justify-content-center\">
            ";
        // line 123
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 123, $this->source); })()));
        echo "
        </div>
        ";
        // line 125
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "user", [], "any", false, false, false, 125) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN"))) {
            // line 126
            echo "            <div class=\"modal fade\" id=\"delete-opinion\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"delete-opinion\"
                 aria-hidden=\"true\">
                <div class=\"modal-dialog  modal-dialog-centered\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-body\">
                            Are you sure you want to delete this topic?
                        </div>
                        <div class=\"modal-footer\">
                            <form class=\"d-inline-block delete-opinion-form\" method=\"POST\">
                                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                <button type=\"submit\" class=\"btn btn-danger\">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        ";
        }
        // line 143
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 145
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 146
        echo "    <script>
        \$('.delete-opinion').on('click', function () {
            let url = \$(this).data('url');
            \$('.delete-opinion-form').prop('action', url)
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "forum/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 146,  325 => 145,  314 => 143,  295 => 126,  293 => 125,  288 => 123,  285 => 122,  279 => 118,  269 => 110,  260 => 106,  249 => 98,  241 => 93,  237 => 91,  235 => 90,  226 => 86,  221 => 84,  217 => 83,  213 => 82,  207 => 79,  202 => 76,  196 => 73,  192 => 72,  188 => 71,  183 => 69,  180 => 68,  178 => 67,  171 => 63,  164 => 59,  160 => 58,  153 => 53,  149 => 52,  142 => 47,  138 => 45,  136 => 44,  125 => 35,  123 => 34,  110 => 24,  104 => 20,  102 => 17,  101 => 14,  93 => 8,  83 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    {{ section.name }}
{% endblock %}

{% block body %}

    <div class=\"container basic-layout\">

        <div class=\"row mt-3\">
            <div class=\"col-10\">

                {% include 'forum/nav.html.twig'
                    with {'links':
                    [
                        {'title':section.name, 'url': ''},
                    ]}
                %}

            </div>

            <div class=\"col-2 text-right\">
                <a href=\"{{ path('web_forum_topic_create',{slug:section.slug}) }}\"
                   class=\"btn btn-blue text-white\">
                    Add New Topic
                </a>
            </div>

            <hr>

        </div>

        {% if topics | length > 0 %}

            <div class=\"table-responsive forum-topics\">

                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Topic</th>
                        <th>Replies</th>
                        <th>Author</th>
                        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
                            <th></th>
                        {% endif %}
                    </tr>
                    </thead>

                    <tbody>

                    {% for topic in topics %}

                        <tr>

                            <td>

                                <a href=\"{{ path('web_forum_section_opinion',{slug:topic.slug}) }}\">
                                    <h5>{{ topic.title }}</h5>
                                </a>

                                <div class=\"small text-muted\">
                                    Created on: {{ topic.createdAt | date('Y-m-d H:i') }}
                                </div>

                                <div class=\"small\">
                                    {% if topic.opinions.last %}
                                        Last reply on
                                        {{ topic.opinions.last.createdAt  | date('Y-m-d H:i') }}
                                        by
                                        <a href=\"{{ path('web_profile',{slug:topic.opinions.last.user.slug}) }}\">
                                            {{ topic.opinions.last.user.profile.firstName }}
                                            {{ topic.opinions.last.user.profile.lastName }}
                                        </a>
                                    {% endif %}
                                </div>
                            </td>

                            <td>{{ topic.opinions | length }}</td>

                            <td>
                                <a href=\"{{ path('web_profile',{slug:topic.owner.slug}) }}\">
                                    <img src=\"{{ topic.owner.profile.picture ?? asset('images/avatar.jpg') }}\"
                                         alt=\"{{ topic.owner.profile.firstName }}\" width=\"50px\" height=\"50px\"
                                         class=\"rounded-circle mb-2 shadow-sm\">
                                    {{ topic.owner.profile.firstName }} {{ topic.owner.profile.lastName }}
                                </a>
                            </td>

                            {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}

                                <td>
                                    <a href=\"{{ path('web_forum_topic_update',{slug:topic.slug}) }}\"
                                       class=\"text-white btn btn-success\">
                                        <i class=\"fa fa-edit\"></i>
                                    </a>
                                    <button class=\"btn btn-danger delete-opinion\"
                                            data-url=\"{{ path('web_forum_topic_delete',{slug:topic.slug}) }}\"
                                            data-toggle=\"modal\"
                                            data-target=\"#delete-opinion\">
                                        <i class=\"fa fa-times\"></i>
                                    </button>
                                </td>

                            {% endif %}

                        </tr>

                    {% endfor %}

                    </tbody>

                </table>

            </div>

        {% else %}
            <h3 class=\"text-muted text-center mt-5 pt-5\">
                No topics
            </h3>
        {% endif %}
        <div class=\"d-flex justify-content-center\">
            {{ knp_pagination_render(topics) }}
        </div>
        {% if app.user and is_granted('ROLE_SUPER_ADMIN') %}
            <div class=\"modal fade\" id=\"delete-opinion\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"delete-opinion\"
                 aria-hidden=\"true\">
                <div class=\"modal-dialog  modal-dialog-centered\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-body\">
                            Are you sure you want to delete this topic?
                        </div>
                        <div class=\"modal-footer\">
                            <form class=\"d-inline-block delete-opinion-form\" method=\"POST\">
                                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                <button type=\"submit\" class=\"btn btn-danger\">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
{% endblock %}
{% block js %}
    <script>
        \$('.delete-opinion').on('click', function () {
            let url = \$(this).data('url');
            \$('.delete-opinion-form').prop('action', url)
        })
    </script>
{% endblock %}
", "forum/show.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/forum/show.html.twig");
    }
}
