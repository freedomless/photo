<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-comment.html.twig */
class __TwigTemplate_279647c3e00f1b28f150d1af3e7fc821dfbf5a0ec2c3af8c9769536b57e4b6ad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-comment.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-comment.html.twig"));

        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header\">

        <strong>Curator Comments</strong>

    </div>

    <div class=\"card-body p-0\">

        <ul class=\"list-group list-group-flush\">
            ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 12, $this->source); })()), "curateComments", [], "any", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 13
            echo "                <li class=\"list-group-item\">
                    <p class=\"text-muted\"
                       data-toggle=\"tooltip\"
                       data-placement=\"top\" title=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["comment"], "user", [], "any", false, false, false, 16), "username", [], "any", false, false, false, 16), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["comment"], "user", [], "any", false, false, false, 16), "profile", [], "any", false, false, false, 16), "fullName", [], "any", false, false, false, 16), "html", null, true);
            echo "</p>
                    <p>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "text", [], "any", false, false, false, 17), "html", null, true);
            echo "</p>
                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </ul>

        <form action=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_comment", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 22, $this->source); })()), "id", [], "any", false, false, false, 22)]), "html", null, true);
        echo "\" method=\"post\">

            <textarea name=\"comment\" id=\"curator-comment\" rows=\"5\" class=\"form-control\"
                      placeholder=\"Comment\"></textarea>

            <button class=\"btn btn-blue btn-block\">Add</button>

        </form>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 22,  80 => 20,  71 => 17,  65 => 16,  60 => 13,  56 => 12,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card mt-3\">

    <div class=\"card-header\">

        <strong>Curator Comments</strong>

    </div>

    <div class=\"card-body p-0\">

        <ul class=\"list-group list-group-flush\">
            {% for comment in post.curateComments %}
                <li class=\"list-group-item\">
                    <p class=\"text-muted\"
                       data-toggle=\"tooltip\"
                       data-placement=\"top\" title=\"{{ comment.user.username }}\">{{ comment.user.profile.fullName }}</p>
                    <p>{{ comment.text }}</p>
                </li>
            {% endfor %}
        </ul>

        <form action=\"{{ path('admin_curator_comment',{id:post.id}) }}\" method=\"post\">

            <textarea name=\"comment\" id=\"curator-comment\" rows=\"5\" class=\"form-control\"
                      placeholder=\"Comment\"></textarea>

            <button class=\"btn btn-blue btn-block\">Add</button>

        </form>

    </div>

</div>
", "admin/curator/_partials/curator-comment.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-comment.html.twig");
    }
}
