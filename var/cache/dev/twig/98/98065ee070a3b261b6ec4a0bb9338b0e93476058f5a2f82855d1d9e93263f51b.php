<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/easyadmin/_product_compare_images.html.twig */
class __TwigTemplate_2bddc7edd4e6a6c13d0c777bd4659b7b603ad74087a3e9926de978980630e571 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/easyadmin/_product_compare_images.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/easyadmin/_product_compare_images.html.twig"));

        // line 1
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 1, $this->source); })()), "post", [], "any", false, false, false, 1), "type", [], "any", false, false, false, 1), 1))) {
            // line 2
            echo "    ";
            // line 3
            echo "
    <div class=\"card mb-3\">

        <div class=\"card-header\">

            <span class=\"h6 card-title mb-0\">Photo ID #";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 8, $this->source); })()), "post", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8), "html", null, true);
            echo "</span>

            ";
            // line 10
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 10, $this->source); })()), "post", [], "any", false, false, false, 10), "image", [], "any", false, false, false, 10), "printThumbnail", [], "any", false, false, false, 10))) {
                // line 11
                echo "
                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 13, $this->source); })()), "post", [], "any", false, false, false, 13), "image", [], "any", false, false, false, 13), "print", [], "any", false, false, false, 13)), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> Download Print File
                </a>

                <a class=\"btn btn-outline-primary ml-2 float-right\" target=\"_blank\"
                   href=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 18, $this->source); })()), "post", [], "any", false, false, false, 18), "image", [], "any", false, false, false, 18), "print", [], "any", false, false, false, 18)), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> View in browser
                </a>

            ";
            } else {
                // line 23
                echo "
                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 25, $this->source); })()), "post", [], "any", false, false, false, 25), "image", [], "any", false, false, false, 25), "url", [], "any", false, false, false, 25), "html", null, true);
                echo "\">
                    <i class=\"fa fa-cloud-download\"></i> Download Original File
                </a>

            ";
            }
            // line 30
            echo "
            ";
            // line 31
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 31, $this->source); })()), "post", [], "any", false, false, false, 31), "product", [], "any", false, false, false, 31), "status", [], "any", false, false, false, 31), 3))) {
                // line 32
                echo "
                <a href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 33, $this->source); })()), "post", [], "any", false, false, false, 33), "product", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\"
                   class=\"btn btn-success float-right\">
                    <i class=\"fa fa-thumbs-up\"></i> Approve
                </a>

            ";
            }
            // line 39
            echo "
        </div>

        <div class=\"card-body\">

            <div class=\"compare\" onclick=\"toggle('compare-print')\">

                <div class=\"compare-original\">

                    <img src=\"";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 48, $this->source); })()), "post", [], "any", false, false, false, 48), "image", [], "any", false, false, false, 48), "small", [], "any", false, false, false, 48), "html", null, true);
            echo "\"
                         alt=\"\"
                         style=\"";
            // line 50
            echo $this->env->getRuntime('App\Twig\ComparePrintRuntime')->getStyle(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 50, $this->source); })()), "post", [], "any", false, false, false, 50), "image", [], "any", false, false, false, 50), "width", [], "any", false, false, false, 50), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 50, $this->source); })()), "post", [], "any", false, false, false, 50), "image", [], "any", false, false, false, 50), "height", [], "any", false, false, false, 50));
            echo "\"
                    >

                    <span class=\"small text-muted\">original</span>

                    <div class=\"compare-print\" id=\"compare-print\">

                        ";
            // line 57
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 57, $this->source); })()), "post", [], "any", false, false, false, 57), "image", [], "any", false, false, false, 57), "printThumbnail", [], "any", false, false, false, 57))) {
                // line 58
                echo "
                            <img src=\"";
                // line 59
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 59, $this->source); })()), "post", [], "any", false, false, false, 59), "image", [], "any", false, false, false, 59), "printThumbnail", [], "any", false, false, false, 59), "html", null, true);
                echo "\" alt=\"\">

                            <span class=\"small text-muted\">print</span>

                        ";
            } else {
                // line 64
                echo "
                            <div class=\"no-print small text-muted\">No print file available</div>

                        ";
            }
            // line 68
            echo "
                    </div>

                </div>

            </div>

        </div>

    </div>

";
        } else {
            // line 80
            echo "    ";
            // line 81
            echo "
    ";
            // line 82
            if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 82, $this->source); })()), "post", [], "any", false, false, false, 82), "product", [], "any", false, false, false, 82), "status", [], "any", false, false, false, 82), 3))) {
                // line 83
                echo "
        <a href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 84, $this->source); })()), "post", [], "any", false, false, false, 84), "product", [], "any", false, false, false, 84), "id", [], "any", false, false, false, 84)]), "html", null, true);
                echo "\"
           class=\"btn btn-outline-success mb-3\">
            <i class=\"fa fa-thumbs-up\"></i> Approve Complete Series
        </a>

        <br>

    ";
            }
            // line 92
            echo "
    ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 93, $this->source); })()), "children", [], "any", false, false, false, 93));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 94
                echo "
        <div class=\"card mb-3\">

            <div class=\"card-header\">

                <span class=\"h6 card-title mb-0\">Photo ID #";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 99), "id", [], "any", false, false, false, 99), "html", null, true);
                echo "</span>

                ";
                // line 101
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 101), "image", [], "any", false, false, false, 101), "printThumbnail", [], "any", false, false, false, 101))) {
                    // line 102
                    echo "
                    <a class=\"btn btn-outline-primary float-right\" href=\"";
                    // line 103
                    echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 103), "image", [], "any", false, false, false, 103), "print", [], "any", false, false, false, 103)), "html", null, true);
                    echo "\">
                        <i class=\"fa fa-cloud-download\"></i> Download Print File
                    </a>

                    <a class=\"btn btn-outline-primary mr-2 float-right\" target=\"_blank\"
                       href=\"";
                    // line 108
                    echo twig_escape_filter($this->env, $this->extensions['App\Twig\SignedLinkExtension']->generate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 108), "image", [], "any", false, false, false, 108), "print", [], "any", false, false, false, 108)), "html", null, true);
                    echo "\">
                        <i class=\"fa fa-cloud-download\"></i> View in browser
                    </a>

                ";
                }
                // line 113
                echo "
                ";
                // line 114
                if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 114), "product", [], "any", false, false, false, 114), "status", [], "any", false, false, false, 114), 3))) {
                    // line 115
                    echo "
                    <a href=\"";
                    // line 116
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_product", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 116), "product", [], "any", false, false, false, 116), "id", [], "any", false, false, false, 116)]), "html", null, true);
                    echo "\"
                       class=\"btn btn-success float-right\">
                        <i class=\"fa fa-thumbs-up\"></i> Approve Photo
                    </a>

                ";
                }
                // line 122
                echo "
            </div>

            <div class=\"card-body\">

                <div class=\"compare mb-3\" onclick=\"toggle('compare-print')\">

                    <div class=\"compare-original\">

                        <img src=\"";
                // line 131
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 131), "image", [], "any", false, false, false, 131), "small", [], "any", false, false, false, 131), "html", null, true);
                echo "\"
                             class=\"shadow-sm\"
                             style=\"";
                // line 133
                echo $this->env->getRuntime('App\Twig\ComparePrintRuntime')->getStyle(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 133), "image", [], "any", false, false, false, 133), "width", [], "any", false, false, false, 133), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 133), "image", [], "any", false, false, false, 133), "height", [], "any", false, false, false, 133));
                echo "\"
                             alt=\"\"
                        >

                        <span class=\"small text-muted\">original</span>

                        <div class=\"compare-print\" id=\"compare-print\">

                            ";
                // line 141
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 141), "image", [], "any", false, false, false, 141), "printThumbnail", [], "any", false, false, false, 141))) {
                    // line 142
                    echo "
                                <img src=\"";
                    // line 143
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "post", [], "any", false, false, false, 143), "image", [], "any", false, false, false, 143), "printThumbnail", [], "any", false, false, false, 143), "html", null, true);
                    echo "\" alt=\"\">

                                <span class=\"small text-muted\">print</span>

                            ";
                } else {
                    // line 148
                    echo "
                                <div class=\"no-print small text-muted\">No print file available</div>

                            ";
                }
                // line 152
                echo "
                        </div>

                    </div>

                </div>

            </div>

        </div>

    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 164
            echo "
";
        }
        // line 166
        echo "
<style>
    .compare-original {
        position: relative;
        cursor: pointer;
    }

    .compare-original span {
        position: absolute;
        left: 20px;
        top: 10px;
        color: white;
        mix-blend-mode: difference;
    }

    .compare-print {
        position: absolute;
        top: 0;
    }

    .no-print {
        position: absolute;
        left: 20px;
        top: 30px;
        background: darkgoldenrod;
        color: #000 !important;
        width: 130px;
        padding: .5rem;
    }
</style>

<script>
    function toggle(target) {
        \$(\".\" + target).toggle();
    }
</script>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/easyadmin/_product_compare_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 166,  310 => 164,  293 => 152,  287 => 148,  279 => 143,  276 => 142,  274 => 141,  263 => 133,  258 => 131,  247 => 122,  238 => 116,  235 => 115,  233 => 114,  230 => 113,  222 => 108,  214 => 103,  211 => 102,  209 => 101,  204 => 99,  197 => 94,  193 => 93,  190 => 92,  179 => 84,  176 => 83,  174 => 82,  171 => 81,  169 => 80,  155 => 68,  149 => 64,  141 => 59,  138 => 58,  136 => 57,  126 => 50,  121 => 48,  110 => 39,  101 => 33,  98 => 32,  96 => 31,  93 => 30,  85 => 25,  81 => 23,  73 => 18,  65 => 13,  61 => 11,  59 => 10,  54 => 8,  47 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if item.post.type == 1 %}
    {# SINGLE PHOTO #}

    <div class=\"card mb-3\">

        <div class=\"card-header\">

            <span class=\"h6 card-title mb-0\">Photo ID #{{ item.post.id }}</span>

            {% if item.post.image.printThumbnail is not null %}

                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"{{ item.post.image.print | signed_link }}\">
                    <i class=\"fa fa-cloud-download\"></i> Download Print File
                </a>

                <a class=\"btn btn-outline-primary ml-2 float-right\" target=\"_blank\"
                   href=\"{{ item.post.image.print | signed_link }}\">
                    <i class=\"fa fa-cloud-download\"></i> View in browser
                </a>

            {% else %}

                <a class=\"btn btn-outline-primary ml-2 float-right\" download=\"print\"
                   href=\"{{ item.post.image.url }}\">
                    <i class=\"fa fa-cloud-download\"></i> Download Original File
                </a>

            {% endif %}

            {% if item.post.product.status < 3 %}

                <a href=\"{{ path('admin_approve_product', {'id': item.post.product.id}) }}\"
                   class=\"btn btn-success float-right\">
                    <i class=\"fa fa-thumbs-up\"></i> Approve
                </a>

            {% endif %}

        </div>

        <div class=\"card-body\">

            <div class=\"compare\" onclick=\"toggle('compare-print')\">

                <div class=\"compare-original\">

                    <img src=\"{{ item.post.image.small }}\"
                         alt=\"\"
                         style=\"{{ get_compare_style(item.post.image.width, item.post.image.height) }}\"
                    >

                    <span class=\"small text-muted\">original</span>

                    <div class=\"compare-print\" id=\"compare-print\">

                        {% if item.post.image.printThumbnail is not null %}

                            <img src=\"{{ item.post.image.printThumbnail }}\" alt=\"\">

                            <span class=\"small text-muted\">print</span>

                        {% else %}

                            <div class=\"no-print small text-muted\">No print file available</div>

                        {% endif %}

                    </div>

                </div>

            </div>

        </div>

    </div>

{% else %}
    {# SERIES #}

    {% if item.post.product.status < 3 %}

        <a href=\"{{ path('admin_approve_product', {'id': item.post.product.id}) }}\"
           class=\"btn btn-outline-success mb-3\">
            <i class=\"fa fa-thumbs-up\"></i> Approve Complete Series
        </a>

        <br>

    {% endif %}

    {% for product  in item.children %}

        <div class=\"card mb-3\">

            <div class=\"card-header\">

                <span class=\"h6 card-title mb-0\">Photo ID #{{ product.post.id }}</span>

                {% if product.post.image.printThumbnail is not null %}

                    <a class=\"btn btn-outline-primary float-right\" href=\"{{ product.post.image.print | signed_link }}\">
                        <i class=\"fa fa-cloud-download\"></i> Download Print File
                    </a>

                    <a class=\"btn btn-outline-primary mr-2 float-right\" target=\"_blank\"
                       href=\"{{ product.post.image.print | signed_link }}\">
                        <i class=\"fa fa-cloud-download\"></i> View in browser
                    </a>

                {% endif %}

                {% if product.post.product.status < 3 %}

                    <a href=\"{{ path('admin_approve_product', {'id': product.post.product.id}) }}\"
                       class=\"btn btn-success float-right\">
                        <i class=\"fa fa-thumbs-up\"></i> Approve Photo
                    </a>

                {% endif %}

            </div>

            <div class=\"card-body\">

                <div class=\"compare mb-3\" onclick=\"toggle('compare-print')\">

                    <div class=\"compare-original\">

                        <img src=\"{{ product.post.image.small }}\"
                             class=\"shadow-sm\"
                             style=\"{{ get_compare_style(product.post.image.width, product.post.image.height) }}\"
                             alt=\"\"
                        >

                        <span class=\"small text-muted\">original</span>

                        <div class=\"compare-print\" id=\"compare-print\">

                            {% if product.post.image.printThumbnail is not null %}

                                <img src=\"{{ product.post.image.printThumbnail }}\" alt=\"\">

                                <span class=\"small text-muted\">print</span>

                            {% else %}

                                <div class=\"no-print small text-muted\">No print file available</div>

                            {% endif %}

                        </div>

                    </div>

                </div>

            </div>

        </div>

    {% endfor %}

{% endif %}

<style>
    .compare-original {
        position: relative;
        cursor: pointer;
    }

    .compare-original span {
        position: absolute;
        left: 20px;
        top: 10px;
        color: white;
        mix-blend-mode: difference;
    }

    .compare-print {
        position: absolute;
        top: 0;
    }

    .no-print {
        position: absolute;
        left: 20px;
        top: 30px;
        background: darkgoldenrod;
        color: #000 !important;
        width: 130px;
        padding: .5rem;
    }
</style>

<script>
    function toggle(target) {
        \$(\".\" + target).toggle();
    }
</script>
", "admin/easyadmin/_product_compare_images.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/easyadmin/_product_compare_images.html.twig");
    }
}
