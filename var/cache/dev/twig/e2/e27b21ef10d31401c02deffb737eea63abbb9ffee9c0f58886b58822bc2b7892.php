<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/photo-details.html.twig */
class __TwigTemplate_9eee448c729f1d48a900f71b3d3f6fc0318d38edee665d89e316e7b243f25eef extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/photo-details.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/photo-details.html.twig"));

        // line 1
        echo "<div class=\"card mt-3\">
    <div class=\"card-header\">
        <strong>Photo Details:</strong>
    </div>
    <div class=\"card-body p-0\">
        <ul class=\"list-group-flush p-0\">
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Date sent:</span>
                ";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 9, $this->source); })()), "sentForCurateDate", [], "any", false, false, false, 9)), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Title:</span>
                ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 13, $this->source); })()), "title", [], "any", false, false, false, 13), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Category:</span>
                ";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 17, $this->source); })()), "category", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Views:</span>
                ";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 21, $this->source); })()), "views", [], "any", false, false, false, 21), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Contains nudity:</span>
                ";
        // line 25
        echo ((twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 25, $this->source); })()), "isNude", [], "any", false, false, false, 25)) ? ("Yes") : ("No"));
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Resolution:</span>
                ";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 29, $this->source); })()), "image", [], "any", false, false, false, 29), "width", [], "any", false, false, false, 29), "html", null, true);
        echo "px x ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 29, $this->source); })()), "image", [], "any", false, false, false, 29), "height", [], "any", false, false, false, 29), "html", null, true);
        echo "px
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Orientation:</span>
                ";
        // line 33
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 33), "orientation", [], "any", false, true, false, 33), "name", [], "any", true, true, false, 33)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image", [], "any", false, true, false, 33), "orientation", [], "any", false, true, false, 33), "name", [], "any", false, false, false, 33), "")) : (""))), "html", null, true);
        echo "
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Greyscale:</span>
                ";
        // line 37
        echo ((twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 37, $this->source); })()), "isGreyscale", [], "any", false, false, false, 37)) ? ("Yes") : ("No"));
        echo "
            </li>
            <li class=\"list-group-item d-flex flex-wrap w-100 text-center\">
                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 40, $this->source); })()), "tags", [], "any", false, false, false, 40));
        foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
            // line 41
            echo "                    <span class=\"badge badge-info ml-2 mr-2 mb-2\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 41), "html", null, true);
            echo "</span>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "            </li>


        </ul>
    </div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/photo-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 43,  114 => 41,  110 => 40,  104 => 37,  97 => 33,  88 => 29,  81 => 25,  74 => 21,  67 => 17,  60 => 13,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card mt-3\">
    <div class=\"card-header\">
        <strong>Photo Details:</strong>
    </div>
    <div class=\"card-body p-0\">
        <ul class=\"list-group-flush p-0\">
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Date sent:</span>
                {{ post.sentForCurateDate | date }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Title:</span>
                {{ post.title }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Category:</span>
                {{ post.category.name }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Views:</span>
                {{ post.views }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Contains nudity:</span>
                {{ post.isNude ? 'Yes' : 'No' }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Resolution:</span>
                {{ post.image.width }}px x {{ post.image.height }}px
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Orientation:</span>
                {{ post.image.orientation.name | default('') | capitalize }}
            </li>
            <li class=\"list-group-item d-flex\">
                <span class=\"flex-grow-1\">Greyscale:</span>
                {{ post.isGreyscale ? 'Yes' : 'No' }}
            </li>
            <li class=\"list-group-item d-flex flex-wrap w-100 text-center\">
                {% for tag in post.tags  %}
                    <span class=\"badge badge-info ml-2 mr-2 mb-2\">{{ tag.name }}</span>
                {% endfor %}
            </li>


        </ul>
    </div>
</div>
", "admin/curator/_partials/photo-details.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/photo-details.html.twig");
    }
}
