<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/warning-modal.html.twig */
class __TwigTemplate_2b5ab08f275a8f0c470a5898fdafa7959028e5fc3053d23844a026f2f8dd2891 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/warning-modal.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/warning-modal.html.twig"));

        // line 1
        $context["modal_id"] = (($context["modal_id"]) ?? ("warning-modal"));
        // line 2
        $context["form_id"] = (($context["form_id"]) ?? ("warning-form"));
        // line 3
        $context["btn_class"] = (($context["btn_id"]) ?? ("warning-action"));
        // line 4
        $context["header"] = (($context["header"]) ?? ("Confirm!"));
        // line 5
        $context["message"] = (($context["message"]) ?? ("Are you sure you want to take this action?"));
        // line 6
        $context["yes"] = (($context["yes"]) ?? ("Yes"));
        // line 7
        $context["no"] = (($context["no"]) ?? ("No"));
        // line 8
        echo "
<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["modal_id"]) || array_key_exists("modal_id", $context) ? $context["modal_id"] : (function () { throw new RuntimeError('Variable "modal_id" does not exist.', 9, $this->source); })()), "html", null, true);
        echo "\">

    <div class=\"modal-dialog\" role=\"document\">

        <form method=\"post\" id=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["form_id"]) || array_key_exists("form_id", $context) ? $context["form_id"] : (function () { throw new RuntimeError('Variable "form_id" does not exist.', 13, $this->source); })()), "html", null, true);
        echo "\">

            <div class=\"modal-content\">

                <div class=\"modal-header\">

                    <h5 class=\"modal-title\">";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "</h5>

                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                        <span aria-hidden=\"true\">&times;</span>

                    </button>

                </div>

                <div class=\"modal-body\">

                    <p>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 31, $this->source); })()), "html", null, true);
        echo "</p>

                </div>

                <div class=\"modal-footer\">

                    <button class=\"btn btn-primary\">";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["yes"]) || array_key_exists("yes", $context) ? $context["yes"] : (function () { throw new RuntimeError('Variable "yes" does not exist.', 37, $this->source); })()), "html", null, true);
        echo "</button>

                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["no"]) || array_key_exists("no", $context) ? $context["no"] : (function () { throw new RuntimeError('Variable "no" does not exist.', 39, $this->source); })()), "html", null, true);
        echo "</button>

                </div>

            </div>

        </form>

    </div>

</div>

";
        // line 51
        $this->displayBlock('js', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 52
        echo "    <script>

        \$(function(){
            \$(document).on('click', '.";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["btn_class"]) || array_key_exists("btn_class", $context) ? $context["btn_class"] : (function () { throw new RuntimeError('Variable "btn_class" does not exist.', 55, $this->source); })()), "html", null, true);
        echo "',function(){
                \$('#";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["modal_id"]) || array_key_exists("modal_id", $context) ? $context["modal_id"] : (function () { throw new RuntimeError('Variable "modal_id" does not exist.', 56, $this->source); })()), "html", null, true);
        echo "').modal('show');
                \$('#";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["form_id"]) || array_key_exists("form_id", $context) ? $context["form_id"] : (function () { throw new RuntimeError('Variable "form_id" does not exist.', 57, $this->source); })()), "html", null, true);
        echo "').prop('action', \$(this).data('url'));
            })
        })

    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "_partials/warning-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 57,  149 => 56,  145 => 55,  140 => 52,  121 => 51,  106 => 39,  101 => 37,  92 => 31,  77 => 19,  68 => 13,  61 => 9,  58 => 8,  56 => 7,  54 => 6,  52 => 5,  50 => 4,  48 => 3,  46 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set modal_id =  modal_id ?? 'warning-modal' %}
{% set form_id =  form_id ?? 'warning-form' %}
{% set btn_class =  btn_id ?? 'warning-action' %}
{% set header = header ?? 'Confirm!' %}
{% set message = message ?? 'Are you sure you want to take this action?' %}
{% set yes = yes ?? 'Yes' %}
{% set no = no ?? 'No' %}

<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"{{ modal_id }}\">

    <div class=\"modal-dialog\" role=\"document\">

        <form method=\"post\" id=\"{{ form_id  }}\">

            <div class=\"modal-content\">

                <div class=\"modal-header\">

                    <h5 class=\"modal-title\">{{ header }}</h5>

                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                        <span aria-hidden=\"true\">&times;</span>

                    </button>

                </div>

                <div class=\"modal-body\">

                    <p>{{ message }}</p>

                </div>

                <div class=\"modal-footer\">

                    <button class=\"btn btn-primary\">{{ yes }}</button>

                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{ no }}</button>

                </div>

            </div>

        </form>

    </div>

</div>

{% block js %}
    <script>

        \$(function(){
            \$(document).on('click', '.{{ btn_class }}',function(){
                \$('#{{ modal_id }}').modal('show');
                \$('#{{ form_id }}').prop('action', \$(this).data('url'));
            })
        })

    </script>

{% endblock %}
", "_partials/warning-modal.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/warning-modal.html.twig");
    }
}
