<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/posters.html.twig */
class __TwigTemplate_32528c82dae9bcb705f4fcd5c4168c821956133af6db9c4dc0dcc10d93c3c226 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/posters.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/posters.html.twig"));

        // line 1
        if ((((array_key_exists("selection_series", $context) &&  !(null === (isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 1, $this->source); })()))) && array_key_exists("photo_of_the_day", $context)) &&  !(null === (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 1, $this->source); })())))) {
            // line 2
            echo "
    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                ";
            // line 7
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 7)->display($context);
            // line 8
            echo "            </div>

            <div class=\"col-lg-4\">
                ";
            // line 11
            $this->loadTemplate("home/selection_series.html.twig", "home/posters.html.twig", 11)->display($context);
            // line 12
            echo "            </div>
        </div>
    </div>

";
        } elseif (((        // line 16
array_key_exists("photo_of_the_day", $context) &&  !(null === (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 16, $this->source); })()))) && array_key_exists("video", $context))) {
            // line 17
            echo "
    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                ";
            // line 22
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 22)->display($context);
            // line 23
            echo "            </div>

            <div class=\"col-lg-4\">
                ";
            // line 26
            $this->loadTemplate("home/video.html.twig", "home/posters.html.twig", 26)->display($context);
            // line 27
            echo "            </div>
        </div>
    </div>

";
        } elseif ((        // line 31
array_key_exists("photo_of_the_day", $context) &&  !(null === (isset($context["photo_of_the_day"]) || array_key_exists("photo_of_the_day", $context) ? $context["photo_of_the_day"] : (function () { throw new RuntimeError('Variable "photo_of_the_day" does not exist.', 31, $this->source); })())))) {
            // line 32
            echo "
    ";
            // line 33
            $this->loadTemplate("home/award.html.twig", "home/posters.html.twig", 33)->display($context);
            // line 34
            echo "
";
        } else {
            // line 36
            echo "
    ";
            // line 37
            $this->loadTemplate("home/selection_series.html.twig", "home/posters.html.twig", 37)->display($context);
            // line 38
            echo "
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/posters.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 38,  105 => 37,  102 => 36,  98 => 34,  96 => 33,  93 => 32,  91 => 31,  85 => 27,  83 => 26,  78 => 23,  76 => 22,  69 => 17,  67 => 16,  61 => 12,  59 => 11,  54 => 8,  52 => 7,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if selection_series is defined and selection_series is not null and photo_of_the_day is defined and photo_of_the_day is not null %}

    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                {% include 'home/award.html.twig' %}
            </div>

            <div class=\"col-lg-4\">
                {% include 'home/selection_series.html.twig' %}
            </div>
        </div>
    </div>

{% elseif photo_of_the_day is defined and photo_of_the_day is not null and video is defined %}

    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-lg-8\">
                {% include 'home/award.html.twig' %}
            </div>

            <div class=\"col-lg-4\">
                {% include 'home/video.html.twig' %}
            </div>
        </div>
    </div>

{% elseif photo_of_the_day is defined and photo_of_the_day is not null %}

    {% include 'home/award.html.twig' %}

{% else %}

    {% include 'home/selection_series.html.twig' %}

{% endif %}
", "home/posters.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/posters.html.twig");
    }
}
