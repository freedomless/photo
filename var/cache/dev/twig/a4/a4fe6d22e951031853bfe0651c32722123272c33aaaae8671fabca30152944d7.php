<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/selection_series.html.twig */
class __TwigTemplate_afd21d30dbe6f7f20c93eb14d973366593bb15d1b64be59323c5f4ca956b19fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/selection_series.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/selection_series.html.twig"));

        // line 1
        if ((array_key_exists("selection_series", $context) &&  !(null === (isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 1, $this->source); })())))) {
            // line 2
            echo "
    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_series_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 9, $this->source); })()), "post", [], "any", false, false, false, 9), "id", [], "any", false, false, false, 9)]), "html", null, true);
            echo "\">

                    <img src=\"";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 11, $this->source); })()), "cover", [], "any", false, false, false, 11), "html", null, true);
            echo "\"
                         alt=\"Selection Series\">

                    <div class=\"author\"></div>
                </a>

            </div>

            <div class=\"social\">
                <span class=\"photo-of-the-day-title\">PiART Selection Series</span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-selection-series-dropdown\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share it!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-selection-series-dropdown\">

                        ";
            // line 31
            echo twig_include($this->env, $context, "_partials/buttons/_facebook_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 33
(isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 33, $this->source); })()), "id", [], "any", false, false, false, 33)]), "classes" => "pr-3 pl-3"]);
            // line 36
            echo "

                        ";
            // line 38
            echo twig_include($this->env, $context, "_partials/buttons/_pinterest_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 40
(isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 40, $this->source); })()), "id", [], "any", false, false, false, 40)]), "media" => twig_get_attribute($this->env, $this->source,             // line 42
(isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 42, $this->source); })()), "cover", [], "any", false, false, false, 42), "description" => "Curators Series", "classes" => "pr-2"]);
            // line 45
            echo "

                        ";
            // line 47
            echo twig_include($this->env, $context, "_partials/buttons/_twitter_share.html.twig", ["share_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("series_selection_history", ["id" => twig_get_attribute($this->env, $this->source,             // line 49
(isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 49, $this->source); })()), "id", [], "any", false, false, false, 49)])]);
            // line 51
            echo "
                    </div>

                </span>

            </div>

            ";
            // line 58
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["selection_series"] ?? null), "post", [], "any", false, true, false, 58), "description", [], "any", true, true, false, 58)) {
                // line 59
                echo "                <p class=\"quote text-muted mt-2\">
                    ";
                // line 60
                echo twig_get_attribute($this->env, $this->source, (isset($context["selection_series"]) || array_key_exists("selection_series", $context) ? $context["selection_series"] : (function () { throw new RuntimeError('Variable "selection_series" does not exist.', 60, $this->source); })()), "title", [], "any", false, false, false, 60);
                echo "
                </p>
            ";
            }
            // line 63
            echo "
            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

        </div>

    </div>

";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/selection_series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 63,  114 => 60,  111 => 59,  109 => 58,  100 => 51,  98 => 49,  97 => 47,  93 => 45,  91 => 42,  90 => 40,  89 => 38,  85 => 36,  83 => 33,  82 => 31,  59 => 11,  54 => 9,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if selection_series is defined and selection_series is not null %}

    <div class=\"row\">

        <div class=\"col-12 text-center\">

            <div class=\"photo-of-the-day\">

                <a href=\"{{ path('web_series_show', {'id': selection_series.post.id}) }}\">

                    <img src=\"{{ selection_series.cover }}\"
                         alt=\"Selection Series\">

                    <div class=\"author\"></div>
                </a>

            </div>

            <div class=\"social\">
                <span class=\"photo-of-the-day-title\">PiART Selection Series</span>

                <span class=\"dropright\">

                    <button class=\"btn btn-link share-btn-main dropdown-toggle js-tooltip-item\" type=\"button\" id=\"js-selection-series-dropdown\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" title=\"Share it!\">
                        <i class=\"fa fa-share-alt\"></i>
                    </button>

                    <div class=\"dropdown-menu ml-3 share-btn-container\" aria-labelledby=\"js-selection-series-dropdown\">

                        {{ include('_partials/buttons/_facebook_share.html.twig', {
                            'share_url': url('series_selection_history', {
                                'id': selection_series.id
                            }),
                            'classes': 'pr-3 pl-3'
                        }) }}

                        {{ include('_partials/buttons/_pinterest_share.html.twig', {
                            'share_url': url('series_selection_history', {
                                'id': selection_series.id
                            }),
                            'media': selection_series.cover,
                            'description': 'Curators Series',
                            'classes': 'pr-2'
                        }) }}

                        {{ include('_partials/buttons/_twitter_share.html.twig', {
                            'share_url': url('series_selection_history', {
                                'id': selection_series.id
                            }),
                        }) }}
                    </div>

                </span>

            </div>

            {% if selection_series.post.description is defined %}
                <p class=\"quote text-muted mt-2\">
                    {{ selection_series.title|raw }}
                </p>
            {% endif %}

            <hr style=\"opacity: .2; margin-top: 0\" class=\"no-gutter\">

        </div>

    </div>

{% endif %}
", "home/selection_series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/home/selection_series.html.twig");
    }
}
