<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/_sharing.html.twig */
class __TwigTemplate_c9f137b10beee62042baa0141e4897591207a763ea3170479f12047b2621e5eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_sharing.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "emails/_sharing.html.twig"));

        // line 1
        echo "<span style=\"line-height: 24px; height: 24px\">
    Share it with the world:&nbsp;

    <a href=\"https://www.facebook.com/dialog/share?app_id=2360993147521648&display=popup&href=";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["share_url"]) || array_key_exists("share_url", $context) ? $context["share_url"] : (function () { throw new RuntimeError('Variable "share_url" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\"><img
                src=\"https://";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["site_url"]) || array_key_exists("site_url", $context) ? $context["site_url"] : (function () { throw new RuntimeError('Variable "site_url" does not exist.', 5, $this->source); })()), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_facebook.png"), "html", null, true);
        echo "\"
                alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://pinterest.com/pin/create/button/?url=";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["share_url"]) || array_key_exists("share_url", $context) ? $context["share_url"] : (function () { throw new RuntimeError('Variable "share_url" does not exist.', 8, $this->source); })()), "html", null, true);
        echo "&media=";
        echo twig_escape_filter($this->env, (isset($context["media"]) || array_key_exists("media", $context) ? $context["media"] : (function () { throw new RuntimeError('Variable "media" does not exist.', 8, $this->source); })()), "html", null, true);
        echo "&description=\">
        <img src=\"https://";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["site_url"]) || array_key_exists("site_url", $context) ? $context["site_url"] : (function () { throw new RuntimeError('Variable "site_url" does not exist.', 9, $this->source); })()), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_pinterest.png"), "html", null, true);
        echo "\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://twitter.com/intent/tweet?url=";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["share_url"]) || array_key_exists("share_url", $context) ? $context["share_url"] : (function () { throw new RuntimeError('Variable "share_url" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "\">
        <img src=\"https://";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["site_url"]) || array_key_exists("site_url", $context) ? $context["site_url"] : (function () { throw new RuntimeError('Variable "site_url" does not exist.', 13, $this->source); })()), "html", null, true);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/share_twitter.png"), "html", null, true);
        echo "\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

</span>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "emails/_sharing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 13,  72 => 12,  65 => 9,  59 => 8,  52 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<span style=\"line-height: 24px; height: 24px\">
    Share it with the world:&nbsp;

    <a href=\"https://www.facebook.com/dialog/share?app_id=2360993147521648&display=popup&href={{ share_url }}\"><img
                src=\"https://{{ site_url }}{{ asset('images/share_facebook.png') }}\"
                alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://pinterest.com/pin/create/button/?url={{ share_url }}&media={{ media }}&description=\">
        <img src=\"https://{{ site_url }}{{ asset('images/share_pinterest.png') }}\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

    <a href=\"https://twitter.com/intent/tweet?url={{ share_url }}\">
        <img src=\"https://{{ site_url }}{{ asset('images/share_twitter.png') }}\"
             alt=\"\" style=\"width: 24px; height: 24px\"></a>

</span>
", "emails/_sharing.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/emails/_sharing.html.twig");
    }
}
