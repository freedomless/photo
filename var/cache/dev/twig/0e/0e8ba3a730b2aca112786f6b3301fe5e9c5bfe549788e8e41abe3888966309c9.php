<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/settings/index.html.twig */
class __TwigTemplate_d956cd9430e79565e2fc0723221882146a610e82e1cf4d2be4a47db436fa6649 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/settings/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/settings/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/settings/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Account Settings";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 6
        echo "    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "
    <div class=\"profile-settings-page\">

        <div class=\"container-fluid profile\" style=\"max-width: 1200px\">

            <div class=\"row\">

                <div class=\"cover\">

                    <label for=\"cover\" class=\"btn btn-warning\">
                        <input id=\"cover\" type=\"file\" accept=\"image/jpeg\"/>
                        Upload
                    </label>

                    <div class=\"options\">

                        <button class=\"btn btn-primary\" id=\"save\" type=\"button\">
                            <i class=\"fas fa-check\"></i>
                        </button>

                        <button class=\"btn btn-danger shadow\" id=\"cancel\" type=\"button\">
                            <i class=\"fas fa-times\"></i>
                        </button>

                    </div>

                    <small class=\"text-muted bg-white p-2 rounded  position-absolute\" style=\"right:10px;top:10px;\">
                        Recommended size: 1200x350 px
                    </small>

                    <img src=\"";
        // line 40
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", true, true, false, 40) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", false, false, false, 40)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "profile", [], "any", false, true, false, 40), "cover", [], "any", false, false, false, 40)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/cover.jpg"))), "html", null, true);
        echo "\" id=\"cover-image\"
                         alt=\"Cover\"/>

                    <img src=\"\" id=\"cover-image-cropper\" alt=\"Cover\"/>

                    <div class=\"dots-loader\"></div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

            <div class=\"row\" style=\"padding-bottom: 3.5rem\">

                <div class=\"col-lg-2\">

                    <div class=\"profile-avatar profile-avatar-settings circle text-center\">

                        <div class=\"avatar-loader\"></div>

                        <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", true, true, false, 63) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", false, false, false, 63)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "profile", [], "any", false, true, false, 63), "vars", [], "any", false, true, false, 63), "value", [], "any", false, true, false, 63), "picture", [], "any", false, false, false, 63)) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/avatar.jpg"))), "html", null, true);
        echo "\"
                             alt=\"Profile Picture\"
                             class=\"shadow circle\">

                        <input type=\"file\" id=\"avatar\" accept=\"image/jpeg,image/png\" class=\"circle\"/>

                    </div>

                </div>

                <div class=\"col-lg-10\">

                    <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                        <h1>
                            <strong>
                                ";
        // line 79
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 79, $this->source); })()), "profile", [], "any", false, false, false, 79), "firstName", [], "any", false, false, false, 79), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 79, $this->source); })()), "profile", [], "any", false, false, false, 79), "lastName", [], "any", false, false, false, 79), "html", null, true);
        echo "
                            </strong>
                        </h1>


                        <div class=\"mt-3\">

                            <i class=\"fa fa-lock\"></i>
                            <a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_change_password");
        echo "\">
                                Change Password
                            </a>

                            <i class=\"fas fa-wrench ml-4\"></i>
                            <a href=\"";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_settings");
        echo "\">
                                Site Settings
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout mt-5\">

            ";
        // line 108
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 108, $this->source); })()), 'form_start');
        echo "

            <div class=\"row default-form-layout\">

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Personal Details:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 119, $this->source); })()), "profile", [], "any", false, false, false, 119), "firstName", [], "any", false, false, false, 119), 'label');
        echo "
                        ";
        // line 120
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 120, $this->source); })()), "profile", [], "any", false, false, false, 120), "firstName", [], "any", false, false, false, 120), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 124, $this->source); })()), "profile", [], "any", false, false, false, 124), "lastName", [], "any", false, false, false, 124), 'label');
        echo "
                        ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 125, $this->source); })()), "profile", [], "any", false, false, false, 125), "lastName", [], "any", false, false, false, 125), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    ";
        // line 128
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "balance", [], "any", true, true, false, 128)) {
            // line 129
            echo "                        <div class=\"form-group\">
                            ";
            // line 130
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 130, $this->source); })()), "balance", [], "any", false, false, false, 130), "payout", [], "any", false, false, false, 130), 'label', ["label" => "Paypal Email"]);
            echo "
                            ";
            // line 131
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 131, $this->source); })()), "balance", [], "any", false, false, false, 131), "payout", [], "any", false, false, false, 131), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "
                        </div>
                    ";
        }
        // line 134
        echo "
                    <h3 class=\"text-muted\">Social:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 140
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 140, $this->source); })()), "profile", [], "any", false, false, false, 140), "facebook", [], "any", false, false, false, 140), 'label');
        echo "
                        ";
        // line 141
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 141, $this->source); })()), "profile", [], "any", false, false, false, 141), "facebook", [], "any", false, false, false, 141), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 145
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 145, $this->source); })()), "profile", [], "any", false, false, false, 145), "twitter", [], "any", false, false, false, 145), 'label');
        echo "
                        ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 146, $this->source); })()), "profile", [], "any", false, false, false, 146), "twitter", [], "any", false, false, false, 146), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 150, $this->source); })()), "profile", [], "any", false, false, false, 150), "instagram", [], "any", false, false, false, 150), 'label');
        echo "
                        ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 151, $this->source); })()), "profile", [], "any", false, false, false, 151), "instagram", [], "any", false, false, false, 151), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 155
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 155, $this->source); })()), "profile", [], "any", false, false, false, 155), "website", [], "any", false, false, false, 155), 'label');
        echo "
                        ";
        // line 156
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 156, $this->source); })()), "profile", [], "any", false, false, false, 156), "website", [], "any", false, false, false, 156), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                </div>

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Address:</h3>

                    <hr>

                    <div class=\"form-group\">
                        ";
        // line 168
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 168, $this->source); })()), "profile", [], "any", false, false, false, 168), "nationality", [], "any", false, false, false, 168), 'label');
        echo "
                        ";
        // line 169
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 169, $this->source); })()), "profile", [], "any", false, false, false, 169), "nationality", [], "any", false, false, false, 169), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 173
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 173, $this->source); })()), "profile", [], "any", false, false, false, 173), "address", [], "any", false, false, false, 173), "country", [], "any", false, false, false, 173), 'label');
        echo "
                        ";
        // line 174
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 174, $this->source); })()), "profile", [], "any", false, false, false, 174), "address", [], "any", false, false, false, 174), "country", [], "any", false, false, false, 174), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 178
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 178, $this->source); })()), "profile", [], "any", false, false, false, 178), "address", [], "any", false, false, false, 178), "county", [], "any", false, false, false, 178), 'label');
        echo "
                        ";
        // line 179
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 179, $this->source); })()), "profile", [], "any", false, false, false, 179), "address", [], "any", false, false, false, 179), "county", [], "any", false, false, false, 179), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 183
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 183, $this->source); })()), "profile", [], "any", false, false, false, 183), "address", [], "any", false, false, false, 183), "address1", [], "any", false, false, false, 183), 'label');
        echo "
                        ";
        // line 184
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 184, $this->source); })()), "profile", [], "any", false, false, false, 184), "address", [], "any", false, false, false, 184), "address1", [], "any", false, false, false, 184), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 188
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 188, $this->source); })()), "profile", [], "any", false, false, false, 188), "address", [], "any", false, false, false, 188), "address2", [], "any", false, false, false, 188), 'label');
        echo "
                        ";
        // line 189
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 189, $this->source); })()), "profile", [], "any", false, false, false, 189), "address", [], "any", false, false, false, 189), "address2", [], "any", false, false, false, 189), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 193
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 193, $this->source); })()), "profile", [], "any", false, false, false, 193), "address", [], "any", false, false, false, 193), "zip", [], "any", false, false, false, 193), 'label');
        echo "
                        ";
        // line 194
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 194, $this->source); })()), "profile", [], "any", false, false, false, 194), "address", [], "any", false, false, false, 194), "zip", [], "any", false, false, false, 194), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 198
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 198, $this->source); })()), "profile", [], "any", false, false, false, 198), "address", [], "any", false, false, false, 198), "city", [], "any", false, false, false, 198), 'label');
        echo "
                        ";
        // line 199
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 199, $this->source); })()), "profile", [], "any", false, false, false, 199), "address", [], "any", false, false, false, 199), "city", [], "any", false, false, false, 199), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 203
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 203, $this->source); })()), "profile", [], "any", false, false, false, 203), "address", [], "any", false, false, false, 203), "phone", [], "any", false, false, false, 203), 'label');
        echo "
                        ";
        // line 204
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 204, $this->source); })()), "profile", [], "any", false, false, false, 204), "address", [], "any", false, false, false, 204), "phone", [], "any", false, false, false, 204), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>

                </div>

            </div>

            <div class=\"row\">

                <div class=\"col-12 text-center\">

                    <hr>

                    <button class=\"btn btn-info\">
                        Save
                    </button>

                </div>

            </div>

            ";
        // line 225
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 225, $this->source); })()), 'form_end');
        echo "

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 233
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 234
        echo "    <script src=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js\"></script>
    <script src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/cover-cropper.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/settings/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  473 => 235,  470 => 234,  460 => 233,  443 => 225,  419 => 204,  415 => 203,  408 => 199,  404 => 198,  397 => 194,  393 => 193,  386 => 189,  382 => 188,  375 => 184,  371 => 183,  364 => 179,  360 => 178,  353 => 174,  349 => 173,  342 => 169,  338 => 168,  323 => 156,  319 => 155,  312 => 151,  308 => 150,  301 => 146,  297 => 145,  290 => 141,  286 => 140,  278 => 134,  272 => 131,  268 => 130,  265 => 129,  263 => 128,  257 => 125,  253 => 124,  246 => 120,  242 => 119,  228 => 108,  209 => 92,  201 => 87,  188 => 79,  169 => 63,  143 => 40,  111 => 10,  101 => 9,  90 => 6,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Account Settings{% endblock %}

{% block css %}
    <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css\">
{% endblock %}

{% block body %}

    <div class=\"profile-settings-page\">

        <div class=\"container-fluid profile\" style=\"max-width: 1200px\">

            <div class=\"row\">

                <div class=\"cover\">

                    <label for=\"cover\" class=\"btn btn-warning\">
                        <input id=\"cover\" type=\"file\" accept=\"image/jpeg\"/>
                        Upload
                    </label>

                    <div class=\"options\">

                        <button class=\"btn btn-primary\" id=\"save\" type=\"button\">
                            <i class=\"fas fa-check\"></i>
                        </button>

                        <button class=\"btn btn-danger shadow\" id=\"cancel\" type=\"button\">
                            <i class=\"fas fa-times\"></i>
                        </button>

                    </div>

                    <small class=\"text-muted bg-white p-2 rounded  position-absolute\" style=\"right:10px;top:10px;\">
                        Recommended size: 1200x350 px
                    </small>

                    <img src=\"{{ user.profile.cover ?? asset('images/cover.jpg') }}\" id=\"cover-image\"
                         alt=\"Cover\"/>

                    <img src=\"\" id=\"cover-image-cropper\" alt=\"Cover\"/>

                    <div class=\"dots-loader\"></div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout profile-meta\" style=\"max-width: 1200px\">

            <div class=\"row\" style=\"padding-bottom: 3.5rem\">

                <div class=\"col-lg-2\">

                    <div class=\"profile-avatar profile-avatar-settings circle text-center\">

                        <div class=\"avatar-loader\"></div>

                        <img src=\"{{ form.profile.vars.value.picture ?? asset('images/avatar.jpg') }}\"
                             alt=\"Profile Picture\"
                             class=\"shadow circle\">

                        <input type=\"file\" id=\"avatar\" accept=\"image/jpeg,image/png\" class=\"circle\"/>

                    </div>

                </div>

                <div class=\"col-lg-10\">

                    <div class=\"profile-info mt-lg-n5 mt-0 text-center text-lg-left\">

                        <h1>
                            <strong>
                                {{ user.profile.firstName }} {{ user.profile.lastName }}
                            </strong>
                        </h1>


                        <div class=\"mt-3\">

                            <i class=\"fa fa-lock\"></i>
                            <a href=\"{{ path('web_change_password') }}\">
                                Change Password
                            </a>

                            <i class=\"fas fa-wrench ml-4\"></i>
                            <a href=\"{{ path('web_settings') }}\">
                                Site Settings
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class=\"container basic-layout mt-5\">

            {{ form_start(form) }}

            <div class=\"row default-form-layout\">

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Personal Details:</h3>

                    <hr>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.firstName) }}
                        {{ form_widget(form.profile.firstName,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.lastName) }}
                        {{ form_widget(form.profile.lastName,{attr:{class:'form-control'}}) }}
                    </div>

                    {% if form.balance is defined %}
                        <div class=\"form-group\">
                            {{ form_label(form.balance.payout,'Paypal Email') }}
                            {{ form_widget(form.balance.payout,{attr:{class:'form-control'}}) }}
                        </div>
                    {% endif %}

                    <h3 class=\"text-muted\">Social:</h3>

                    <hr>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.facebook) }}
                        {{ form_widget(form.profile.facebook,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.twitter) }}
                        {{ form_widget(form.profile.twitter,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.instagram) }}
                        {{ form_widget(form.profile.instagram,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.website) }}
                        {{ form_widget(form.profile.website,{attr:{class:'form-control'}}) }}
                    </div>

                </div>

                <div class=\"col-md-6 col-sm-6\">

                    <h3 class=\"text-muted\">Address:</h3>

                    <hr>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.nationality) }}
                        {{ form_widget(form.profile.nationality,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.country) }}
                        {{ form_widget(form.profile.address.country,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.county) }}
                        {{ form_widget(form.profile.address.county,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.address1) }}
                        {{ form_widget(form.profile.address.address1,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.address2) }}
                        {{ form_widget(form.profile.address.address2,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.zip) }}
                        {{ form_widget(form.profile.address.zip,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.city) }}
                        {{ form_widget(form.profile.address.city,{attr:{class:'form-control'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {{ form_label(form.profile.address.phone) }}
                        {{ form_widget(form.profile.address.phone,{attr:{class:'form-control'}}) }}
                    </div>

                </div>

            </div>

            <div class=\"row\">

                <div class=\"col-12 text-center\">

                    <hr>

                    <button class=\"btn btn-info\">
                        Save
                    </button>

                </div>

            </div>

            {{ form_end(form) }}

        </div>

    </div>

{% endblock %}

{% block js %}
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js\"></script>
    <script src=\"{{ asset('js/cover-cropper.js') }}\"></script>
{% endblock %}
", "user/settings/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/settings/index.html.twig");
    }
}
