<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/faq.html.twig */
class __TwigTemplate_9ca9de059ac0e846901ffb07cd2948ef3e2e2776abce465e2537db0a40d03edd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'main_content' => [$this, 'block_main_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/faq.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/faq.html.twig"));

        $this->parent = $this->loadTemplate("template.html.twig", "pages/faq.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "F.A.Q";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        echo "F.A.Q";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        // line 8
        echo "    <span class=\"text-muted\">Last Update: 28 Aug, 2019</span>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_main_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_content"));

        // line 12
        echo "
    <div class=\"col-md-12\">

        <div>
            <h2><strong>Q: What is ";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 16, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A: </strong> Do you want to know whether your photos would be posted among photos carefully
                selected by experienced
                professional curators with the highest criteria? All photos posted at the ";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 20, $this->source); })()), "html", null, true);
        echo "'s gallery have
                been carefully selected by a professional
                curatorial team. The curators in the team have many years of experience and world-class standards.
                ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 23, $this->source); })()), "html", null, true);
        echo " is a curated gallery, which means that the posted photos are only at the highest level,
                which meets the great number of different criteria of the curators.
            </p>
        </div>

        <div>
            <h2><strong>Q: How are my images protected?</strong></h2>

            <p><strong>A:</strong> We took safety precautions because we consider the illegal use of protected images as
                a very serious issue.
            <ul>
                <li>We have prevented the image from being downloaded by dragging or right-clicking.</li>
                <li>All images have a watermark with the author's name.</li>
                <li>The metadata of all images has been removed.</li>
            </ul>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I add photos to my favourites?</strong></h2>

            <p><strong>A:</strong> Use the heart icon in the toolbar below the photos to add photos to your favorites.
                You can access your favourite photos from:
                <br><code>Click on: \"My Profile\" in the top toolbar <b>>></b> View <b>>></b> Favourites.</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I follow my favourite photographers?</strong></h2>

            <p><strong>A:</strong> Log in to the author's profile and click on the log image icon, or use the same icon
                in the bottom toolbar of any of the author's photos when open in large size.</p>
        </div>

        <div>
            <h2><strong>Q: How do albums work?</strong></h2>

            <p><strong>A:</strong> With albums, you can organize your photos quickly and easily.
                Just create a new album and click on all the photos you want to add in it.
                You can choose a cover photo for your album.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I upload photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: “UPLOAD” button in the upper right corner <b>>></b> select
                    “PHOTO”</code><br>
                You can drag the photo into the blue square or press „Browse“ to select a photo from
                your device. Your photos should be in 8-bit standard sRGB and JPG format.</p>
        </div>

        <div>
            <h2><strong>Q: What are series?</strong></h2>

            <p><strong>A:</strong> The series is a set of 3 to 5 photos that have a common theme and are united by the
                idea the author wants to represent, through concept, composition, or editing, but always on the basis of
                a consistent and common idea.</p>
        </div>

        <div>
            <h2><strong>Q: How do I upload a series of photos?</strong></h2>

            <p><strong>A:</strong> Open the UPLOAD button in the upper right corner and select SERIES. You can drag a
                photo into the blue square or click Browse, to select a photo from your device. The photo uploaded at
                the first position will be the cover of the series. Press plus if you want to add more photos. The
                minimum number of series upload photos is 3 and the maximum is 5.
                The series should be created by photos that are related by theme, idea or story.
            </p>
        </div>

        <div>
            <h2><strong>Q: Can I create a series of photos already posted in ";
        // line 95
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 95, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Yes, you can. <br>
                <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos</code><br>
                There is a ‘Create a Series’ button in the upper right corner.
                This button opens the menu for editing a series of already posted photos. If the series is not approved
                by the curatorial team, this does not affect the photos already posted, they remain posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: What are the recommended photo upload sizes?</strong></h2>

            <p><strong>A:</strong> We recommend that you upload your photos in their original size if you want to use
                them to sell prints through ";
        // line 109
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 109, $this->source); })()), "html", null, true);
        echo ".
                The photos will automatically be resized to a smaller optimal size when displayed in ";
        // line 110
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 110, $this->source); })()), "html", null, true);
        echo " and
                the file with high resolution will not be posted. The photos sent to curators must be at least 1000
                pixels along the long side, but for better viewing, we recommend that you always upload photos of at
                least 1600 pixels on the long side or more.
            </p>
        </div>

        <div>
            <h2><strong>Q: Why don't the colours of the photo in ";
        // line 118
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 118, $this->source); })()), "html", null, true);
        echo " match the colours of the photo I
                    uploaded?</strong></h2>

            <p><strong>A:</strong> Remember to always convert the photos to 8-bit standard sRGB saved in JPG format
                before uploading them.</p>
        </div>

        <div>
            <h2><strong>Q: How do I send photos to curators for review in the gallery?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos
                    <b>>></b>
                    Send to Curators </code><br>
                By clicking the \"Send to Curators\" button you send the photo to the curatorial team. Users can send 10
                photos per week for curating.
                If your photo has been approved for publication by the curatorial team, it will be posted in the
                ";
        // line 134
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 134, $this->source); })()), "html", null, true);
        echo " gallery within a few days. When a photo is posted, the author will be notified.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does consumer voting work?</strong></h2>

            <p><strong>A:</strong> Clicking on the \"VOTE\" button in the upper right corner gives you the opportunity to
                vote for the photos that have been sent to the official curatorial team. You can give a 5 point rating
                to each photo, enabling the authors and curators to understand the user rating of the sent photos.
                This vote helps a lot the site development and performance, but it is not decisive whether or not a
                photo will be posted. The final decision is made by the official curatorial team and is not subject to
                discussion.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does curation work?</strong></h2>

            <p><strong>A:</strong> After you send a photo to the curators, it will be assessed by them in terms of idea,
                mood, aesthetics and technical quality.
                The originality and variety of the gallery are very important. The curatorial team will always keep in
                mind the rating of the consumer curation,
                but sometimes it is possible for a high-rated photo to be rejected, because too many photos with a
                similar motive were previously posted on the site; on the contrary, a photo with a very low rating can
                be posted because the curators saw something special in it.
            </p>
        </div>

        ";
        // line 164
        echo "        ";
        // line 165
        echo "
        ";
        // line 167
        echo "        ";
        // line 168
        echo "        ";
        // line 169
        echo "        ";
        // line 170
        echo "
        <div>
            <h2><strong>Q: </strong>How do I know whether my photo has been posted or rejected?</h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                Each photo has information in the upper right corner concerning its status. The user receives an
                additional notification when his photo is posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I edit information in photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                By clicking on the selected photo or on the edit icon, the edit menu opens.</p>
        </div>

        <div>
            <h2><strong>Q: How do I delete a photo?</strong></h2>

            <p><strong>A:</strong> From the \"My Profile\" in the top toolbar> Manage Your: Photos from the menu that
                opens by
                clicking on the selected photo or on the edit icon. You can only delete photos that have not been posted
                or have not been sent for curation. Posted photos and the ones that are in the process of curation have
                no deletion option.</p>
        </div>

        <div>
            <h2><strong>Q: Do you allow watermark, logo or other tagging on the photos?</strong></h2>

            <p><strong>A:</strong> All photos in ";
        // line 202
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 202, $this->source); })()), "html", null, true);
        echo " are automatically tagged by us with your name.
                Uploading
                photos with watermarks or other additional markings is not allowed.</p>
        </div>

        <div>
            <h2><strong>Q: Are photo frames and borders allowed?</strong></h2>

            <p><strong>A:</strong> The photos sent to the curators should not contain any frames or borders because we
                want to keep elegant style in the gallery and the frames sometimes distract. If your photo is diptych or
                triptych, borders are allowed, but please make them as discreet as possible.</p>
        </div>

        <div>
            <h2><strong>Q: Am I allowed to use stock photos from other photographs in my works?</strong></h2>

            <p><strong>A:</strong> All the parts in the photos you send to the curators should consist of photos taken
                by you. You are not allowed to use stock photos in photomontages.</p>
        </div>

        <div>
            <h2><strong>Q: What are the rules for writing comments in ";
        // line 223
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 223, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Offensive comments or personal attacks can cause your account to be blocked or
                deleted. Remember to always use friendly and polite language when communicating in ";
        // line 226
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 226, $this->source); })()), "html", null, true);
        echo ".
                Never write when you are angry. If you have a problem or conflict with another member of the site,
                contact ";
        // line 228
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 228, $this->source); })()), "html", null, true);
        echo " Support and we will assist you.</p>
        </div>

        <div>
            <h2><strong>Q: Can I sale my photos at ";
        // line 232
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 232, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Yes. You can list for sale on our Print store any of your Published photos/series.</p>
        </div>

        <div>
            <h2><strong>Q: How to list my photo(s)/series for sale at ";
        // line 238
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 238, $this->source); })()), "html", null, true);
        echo "'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Photos/Series <b>>></b> Click on \"SALE\" button next to each published photo/series.</code>
                You can then manage each of your listed for sale items from <code> \"My Profile\" <b>>></b> Manage <b>>></b> Products</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my photo(s)/series that are listed for sale at ";
        // line 247
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 247, $this->source); })()), "html", null, true);
        echo "'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Products <b>>></b> Click on \"Remove\" button next to each photo/series.</code>
                Important note: Each photo/series that is deleted from the Print Store Gallery will remain visible and available for purchase for \"30\" days after the deletion date. When the time expires the photo will be permanently removed from the store.
                In the meantime, you will still be paid for each sale that your photo/series generates.
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my account?</strong></h2>

            <p><strong>A:</strong> If you wish no longer to be a member of ";
        // line 259
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 259, $this->source); })()), "html", null, true);
        echo " you have the opportunity to
                delete your account. To delete your account, please contact us using this <a
                        href=\"";
        // line 261
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
        echo "\">form</a> and will delete your account for you.
                Deleting your account is a final decision and we cannot get it back, so give yourself a few days before
                you decide to do such an action. Be sure to contact the site support first if you have any problems that
                made you proceed with deleting your account. The site team will help you solve the problems.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I filter nude contents?</strong></h2>

            <p><strong>A:</strong> Enable or disable the nude content filter in your account settings.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact ";
        // line 275
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 275, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> Use the contact form.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact a member of ";
        // line 281
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 281, $this->source); })()), "html", null, true);
        echo "?</strong></h2>

            <p><strong>A:</strong> To contact a member of ";
        // line 283
        echo twig_escape_filter($this->env, (isset($context["site_name"]) || array_key_exists("site_name", $context) ? $context["site_name"] : (function () { throw new RuntimeError('Variable "site_name" does not exist.', 283, $this->source); })()), "html", null, true);
        echo ", log in to their profile and click on the
                personal message icon.</p>
        </div>

        <div>
            <h2><strong>Q: How do I contact the support?</strong></h2>

            <p><strong>A:</strong> Before contacting the support, please read the FAQ carefully to make sure that your
                question has not been answered.
                You can contact us for any questions or suggestions.
                <br>The contact email is: <strong>";
        // line 293
        echo twig_escape_filter($this->env, (isset($context["site_email"]) || array_key_exists("site_email", $context) ? $context["site_email"] : (function () { throw new RuntimeError('Variable "site_email" does not exist.', 293, $this->source); })()), "html", null, true);
        echo "</strong>
            </p>
        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "pages/faq.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  479 => 293,  466 => 283,  461 => 281,  452 => 275,  435 => 261,  430 => 259,  415 => 247,  403 => 238,  394 => 232,  387 => 228,  382 => 226,  376 => 223,  352 => 202,  318 => 170,  316 => 169,  314 => 168,  312 => 167,  309 => 165,  307 => 164,  275 => 134,  256 => 118,  245 => 110,  241 => 109,  224 => 95,  149 => 23,  143 => 20,  136 => 16,  130 => 12,  120 => 11,  109 => 8,  99 => 7,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'template.html.twig' %}

{% block title %}F.A.Q{% endblock %}

{% block header_left %}F.A.Q{% endblock %}

{% block header_right %}
    <span class=\"text-muted\">Last Update: 28 Aug, 2019</span>
{% endblock %}

{% block main_content %}

    <div class=\"col-md-12\">

        <div>
            <h2><strong>Q: What is {{ site_name }}?</strong></h2>

            <p><strong>A: </strong> Do you want to know whether your photos would be posted among photos carefully
                selected by experienced
                professional curators with the highest criteria? All photos posted at the {{ site_name }}'s gallery have
                been carefully selected by a professional
                curatorial team. The curators in the team have many years of experience and world-class standards.
                {{ site_name }} is a curated gallery, which means that the posted photos are only at the highest level,
                which meets the great number of different criteria of the curators.
            </p>
        </div>

        <div>
            <h2><strong>Q: How are my images protected?</strong></h2>

            <p><strong>A:</strong> We took safety precautions because we consider the illegal use of protected images as
                a very serious issue.
            <ul>
                <li>We have prevented the image from being downloaded by dragging or right-clicking.</li>
                <li>All images have a watermark with the author's name.</li>
                <li>The metadata of all images has been removed.</li>
            </ul>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I add photos to my favourites?</strong></h2>

            <p><strong>A:</strong> Use the heart icon in the toolbar below the photos to add photos to your favorites.
                You can access your favourite photos from:
                <br><code>Click on: \"My Profile\" in the top toolbar <b>>></b> View <b>>></b> Favourites.</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I follow my favourite photographers?</strong></h2>

            <p><strong>A:</strong> Log in to the author's profile and click on the log image icon, or use the same icon
                in the bottom toolbar of any of the author's photos when open in large size.</p>
        </div>

        <div>
            <h2><strong>Q: How do albums work?</strong></h2>

            <p><strong>A:</strong> With albums, you can organize your photos quickly and easily.
                Just create a new album and click on all the photos you want to add in it.
                You can choose a cover photo for your album.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I upload photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: “UPLOAD” button in the upper right corner <b>>></b> select
                    “PHOTO”</code><br>
                You can drag the photo into the blue square or press „Browse“ to select a photo from
                your device. Your photos should be in 8-bit standard sRGB and JPG format.</p>
        </div>

        <div>
            <h2><strong>Q: What are series?</strong></h2>

            <p><strong>A:</strong> The series is a set of 3 to 5 photos that have a common theme and are united by the
                idea the author wants to represent, through concept, composition, or editing, but always on the basis of
                a consistent and common idea.</p>
        </div>

        <div>
            <h2><strong>Q: How do I upload a series of photos?</strong></h2>

            <p><strong>A:</strong> Open the UPLOAD button in the upper right corner and select SERIES. You can drag a
                photo into the blue square or click Browse, to select a photo from your device. The photo uploaded at
                the first position will be the cover of the series. Press plus if you want to add more photos. The
                minimum number of series upload photos is 3 and the maximum is 5.
                The series should be created by photos that are related by theme, idea or story.
            </p>
        </div>

        <div>
            <h2><strong>Q: Can I create a series of photos already posted in {{ site_name }}?</strong></h2>

            <p><strong>A:</strong> Yes, you can. <br>
                <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos</code><br>
                There is a ‘Create a Series’ button in the upper right corner.
                This button opens the menu for editing a series of already posted photos. If the series is not approved
                by the curatorial team, this does not affect the photos already posted, they remain posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: What are the recommended photo upload sizes?</strong></h2>

            <p><strong>A:</strong> We recommend that you upload your photos in their original size if you want to use
                them to sell prints through {{ site_name }}.
                The photos will automatically be resized to a smaller optimal size when displayed in {{ site_name }} and
                the file with high resolution will not be posted. The photos sent to curators must be at least 1000
                pixels along the long side, but for better viewing, we recommend that you always upload photos of at
                least 1600 pixels on the long side or more.
            </p>
        </div>

        <div>
            <h2><strong>Q: Why don't the colours of the photo in {{ site_name }} match the colours of the photo I
                    uploaded?</strong></h2>

            <p><strong>A:</strong> Remember to always convert the photos to 8-bit standard sRGB saved in JPG format
                before uploading them.</p>
        </div>

        <div>
            <h2><strong>Q: How do I send photos to curators for review in the gallery?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your: Photos
                    <b>>></b>
                    Send to Curators </code><br>
                By clicking the \"Send to Curators\" button you send the photo to the curatorial team. Users can send 10
                photos per week for curating.
                If your photo has been approved for publication by the curatorial team, it will be posted in the
                {{ site_name }} gallery within a few days. When a photo is posted, the author will be notified.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does consumer voting work?</strong></h2>

            <p><strong>A:</strong> Clicking on the \"VOTE\" button in the upper right corner gives you the opportunity to
                vote for the photos that have been sent to the official curatorial team. You can give a 5 point rating
                to each photo, enabling the authors and curators to understand the user rating of the sent photos.
                This vote helps a lot the site development and performance, but it is not decisive whether or not a
                photo will be posted. The final decision is made by the official curatorial team and is not subject to
                discussion.
            </p>
        </div>

        <div>
            <h2><strong>Q: How does curation work?</strong></h2>

            <p><strong>A:</strong> After you send a photo to the curators, it will be assessed by them in terms of idea,
                mood, aesthetics and technical quality.
                The originality and variety of the gallery are very important. The curatorial team will always keep in
                mind the rating of the consumer curation,
                but sometimes it is possible for a high-rated photo to be rejected, because too many photos with a
                similar motive were previously posted on the site; on the contrary, a photo with a very low rating can
                be posted because the curators saw something special in it.
            </p>
        </div>

        {#        <div>#}
        {#            <h2><strong>Q: How does the popularity and users rating system work?</strong></h2>#}

        {#            <p><strong>A:</strong> In order to encourage activity and contribution to {{ site_name }}, we have#}
        {#                introduced a rating system. You get points for posting photos, commenting on other photos, participating#}
        {#                in curation, etc. In the profile against your name you can see the points you have accumulated.</p>#}
        {#        </div>#}

        <div>
            <h2><strong>Q: </strong>How do I know whether my photo has been posted or rejected?</h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                Each photo has information in the upper right corner concerning its status. The user receives an
                additional notification when his photo is posted.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I edit information in photos?</strong></h2>

            <p><strong>A:</strong> <code>Click on: \"My Profile\" in the upper toolbar <b>>></b> Manage Your:
                    Photos</code><br>
                By clicking on the selected photo or on the edit icon, the edit menu opens.</p>
        </div>

        <div>
            <h2><strong>Q: How do I delete a photo?</strong></h2>

            <p><strong>A:</strong> From the \"My Profile\" in the top toolbar> Manage Your: Photos from the menu that
                opens by
                clicking on the selected photo or on the edit icon. You can only delete photos that have not been posted
                or have not been sent for curation. Posted photos and the ones that are in the process of curation have
                no deletion option.</p>
        </div>

        <div>
            <h2><strong>Q: Do you allow watermark, logo or other tagging on the photos?</strong></h2>

            <p><strong>A:</strong> All photos in {{ site_name }} are automatically tagged by us with your name.
                Uploading
                photos with watermarks or other additional markings is not allowed.</p>
        </div>

        <div>
            <h2><strong>Q: Are photo frames and borders allowed?</strong></h2>

            <p><strong>A:</strong> The photos sent to the curators should not contain any frames or borders because we
                want to keep elegant style in the gallery and the frames sometimes distract. If your photo is diptych or
                triptych, borders are allowed, but please make them as discreet as possible.</p>
        </div>

        <div>
            <h2><strong>Q: Am I allowed to use stock photos from other photographs in my works?</strong></h2>

            <p><strong>A:</strong> All the parts in the photos you send to the curators should consist of photos taken
                by you. You are not allowed to use stock photos in photomontages.</p>
        </div>

        <div>
            <h2><strong>Q: What are the rules for writing comments in {{ site_name }}?</strong></h2>

            <p><strong>A:</strong> Offensive comments or personal attacks can cause your account to be blocked or
                deleted. Remember to always use friendly and polite language when communicating in {{ site_name }}.
                Never write when you are angry. If you have a problem or conflict with another member of the site,
                contact {{ site_name }} Support and we will assist you.</p>
        </div>

        <div>
            <h2><strong>Q: Can I sale my photos at {{ site_name }}?</strong></h2>

            <p><strong>A:</strong> Yes. You can list for sale on our Print store any of your Published photos/series.</p>
        </div>

        <div>
            <h2><strong>Q: How to list my photo(s)/series for sale at {{ site_name }}'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Photos/Series <b>>></b> Click on \"SALE\" button next to each published photo/series.</code>
                You can then manage each of your listed for sale items from <code> \"My Profile\" <b>>></b> Manage <b>>></b> Products</code>
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my photo(s)/series that are listed for sale at {{ site_name }}'s Print Store?</strong></h2>

            <p><strong>A:</strong>
                <code> Click on: \"My Profile\" in the top toolbar <b>>></b> Manage <b>>></b> Products <b>>></b> Click on \"Remove\" button next to each photo/series.</code>
                Important note: Each photo/series that is deleted from the Print Store Gallery will remain visible and available for purchase for \"30\" days after the deletion date. When the time expires the photo will be permanently removed from the store.
                In the meantime, you will still be paid for each sale that your photo/series generates.
            </p>
        </div>

        <div>
            <h2><strong>Q: How can I delete my account?</strong></h2>

            <p><strong>A:</strong> If you wish no longer to be a member of {{ site_name }} you have the opportunity to
                delete your account. To delete your account, please contact us using this <a
                        href=\"{{ path('web_contacts') }}\">form</a> and will delete your account for you.
                Deleting your account is a final decision and we cannot get it back, so give yourself a few days before
                you decide to do such an action. Be sure to contact the site support first if you have any problems that
                made you proceed with deleting your account. The site team will help you solve the problems.
            </p>
        </div>

        <div>
            <h2><strong>Q: How do I filter nude contents?</strong></h2>

            <p><strong>A:</strong> Enable or disable the nude content filter in your account settings.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact {{ site_name }}?</strong></h2>

            <p><strong>A:</strong> Use the contact form.</p>
        </div>

        <div>
            <h2><strong>Q: How to contact a member of {{ site_name }}?</strong></h2>

            <p><strong>A:</strong> To contact a member of {{ site_name }}, log in to their profile and click on the
                personal message icon.</p>
        </div>

        <div>
            <h2><strong>Q: How do I contact the support?</strong></h2>

            <p><strong>A:</strong> Before contacting the support, please read the FAQ carefully to make sure that your
                question has not been answered.
                You can contact us for any questions or suggestions.
                <br>The contact email is: <strong>{{ site_email }}</strong>
            </p>
        </div>

    </div>

{% endblock %}
", "pages/faq.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/pages/faq.html.twig");
    }
}
