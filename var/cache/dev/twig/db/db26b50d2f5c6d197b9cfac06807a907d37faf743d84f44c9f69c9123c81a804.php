<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/for-sale.html.twig */
class __TwigTemplate_12f29795e8b2bc425f8fe2dc3c35c4928210422855f469b116bc3290836ec196 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/for-sale.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/for-sale.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "post/for-sale.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "    <div class=\"container mt-5\">


        <h3 class=\"text-center\">Add photo to store</h3>

        <div class=\"row\">

            <div class=\"col-12 d-flex justify-content-center align-content-center align-items-center flex-column\">

                <div>

                    <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })()), "image", [], "any", false, false, false, 19), "thumbnail", [], "any", false, false, false, 19), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })()), "title", [], "any", false, false, false, 19), "html", null, true);
        echo "\">

                </div>

                ";
        // line 23
        if ((0 === twig_compare((isset($context["canEdit"]) || array_key_exists("canEdit", $context) ? $context["canEdit"] : (function () { throw new RuntimeError('Variable "canEdit" does not exist.', 23, $this->source); })()), false))) {
            // line 24
            echo "
                    <div class=\"row\">


                        <div class=\"col-12\">
                            <p class=\"bold p-3 text-center\">

                                You will be able to edit you product
                                on ";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 32, $this->source); })()), "priceChangeAt", [], "any", false, false, false, 32), "modify", [0 => (("+ " . twig_get_attribute($this->env, $this->source, (isset($context["config"]) || array_key_exists("config", $context) ? $context["config"] : (function () { throw new RuntimeError('Variable "config" does not exist.', 32, $this->source); })()), "priceChangeTimespan", [], "any", false, false, false, 32)) . "days")], "method", false, false, false, 32), "d-m-Y H:i"), "html", null, true);
            echo "

                                <br>

                                <strong>Price: <small>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 36, $this->source); })()), "commission", [], "any", false, false, false, 36), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["config"]) || array_key_exists("config", $context) ? $context["config"] : (function () { throw new RuntimeError('Variable "config" does not exist.', 36, $this->source); })()), "currency", [], "any", false, false, false, 36), "html", null, true);
            echo "</small></strong>

                            </p>


                        </div>

                    </div>

                ";
        }
        // line 46
        echo "
                ";
        // line 47
        if (((isset($context["canEdit"]) || array_key_exists("canEdit", $context) ? $context["canEdit"] : (function () { throw new RuntimeError('Variable "canEdit" does not exist.', 47, $this->source); })()) || (isset($context["print"]) || array_key_exists("print", $context) ? $context["print"] : (function () { throw new RuntimeError('Variable "print" does not exist.', 47, $this->source); })()))) {
            // line 48
            echo "
                    ";
            // line 49
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 49, $this->source); })()), 'form_start');
            echo "

                    <div class=\"form-row mt-3\">

                        ";
            // line 53
            if ((isset($context["canEdit"]) || array_key_exists("canEdit", $context) ? $context["canEdit"] : (function () { throw new RuntimeError('Variable "canEdit" does not exist.', 53, $this->source); })())) {
                // line 54
                echo "
                            <div class=\"form-group col-12 p-0\">
                                ";
                // line 56
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 56, $this->source); })()), "commission", [], "any", false, false, false, 56), 'label');
                echo "
                                ";
                // line 57
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 57, $this->source); })()), "commission", [], "any", false, false, false, 57), 'widget', ["attr" => ["class" => "form-control"]]);
                echo "
                            </div>


                        ";
            }
            // line 62
            echo "
                        ";
            // line 63
            if ((isset($context["print"]) || array_key_exists("print", $context) ? $context["print"] : (function () { throw new RuntimeError('Variable "print" does not exist.', 63, $this->source); })())) {
                // line 64
                echo "
                            <div class=\"form-group col-12 p-0\">
                                ";
                // line 66
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 66, $this->source); })()), "print", [], "any", false, false, false, 66), 'label');
                echo "
                                ";
                // line 67
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 67, $this->source); })()), "print", [], "any", false, false, false, 67), 'widget', ["attr" => ["class" => "form-control"]]);
                echo "
                            </div>


                        ";
            }
            // line 72
            echo "

                        <button class=\"btn btn-blue btn-block\">Save</button>


                    </div>
                    ";
            // line 78
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 78, $this->source); })()), 'form_end');
            echo "


                ";
        }
        // line 82
        echo "
            </div>

        </div>


    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "post/for-sale.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 82,  208 => 78,  200 => 72,  192 => 67,  188 => 66,  184 => 64,  182 => 63,  179 => 62,  171 => 57,  167 => 56,  163 => 54,  161 => 53,  154 => 49,  151 => 48,  149 => 47,  146 => 46,  131 => 36,  124 => 32,  114 => 24,  112 => 23,  103 => 19,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}

{% endblock %}

{% block content %}
    <div class=\"container mt-5\">


        <h3 class=\"text-center\">Add photo to store</h3>

        <div class=\"row\">

            <div class=\"col-12 d-flex justify-content-center align-content-center align-items-center flex-column\">

                <div>

                    <img src=\"{{ post.image.thumbnail }}\" alt=\"{{ post.title }}\">

                </div>

                {% if canEdit == false %}

                    <div class=\"row\">


                        <div class=\"col-12\">
                            <p class=\"bold p-3 text-center\">

                                You will be able to edit you product
                                on {{ product.priceChangeAt.modify('+ '  ~ config.priceChangeTimespan  ~ 'days') | date('d-m-Y H:i') }}

                                <br>

                                <strong>Price: <small>{{ product.commission }} {{ config.currency }}</small></strong>

                            </p>


                        </div>

                    </div>

                {% endif %}

                {% if canEdit or print %}

                    {{ form_start(form) }}

                    <div class=\"form-row mt-3\">

                        {% if canEdit %}

                            <div class=\"form-group col-12 p-0\">
                                {{ form_label(form.commission) }}
                                {{ form_widget(form.commission, {attr:{class:'form-control'}}) }}
                            </div>


                        {% endif %}

                        {% if print %}

                            <div class=\"form-group col-12 p-0\">
                                {{ form_label(form.print) }}
                                {{ form_widget(form.print, {attr:{class:'form-control'}}) }}
                            </div>


                        {% endif %}


                        <button class=\"btn btn-blue btn-block\">Save</button>


                    </div>
                    {{ form_end(form) }}


                {% endif %}

            </div>

        </div>


    </div>
{% endblock %}

", "post/for-sale.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/post/for-sale.html.twig");
    }
}
