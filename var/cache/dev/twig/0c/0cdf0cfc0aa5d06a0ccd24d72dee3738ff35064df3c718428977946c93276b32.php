<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site_notifications/_base.html.twig */
class __TwigTemplate_3bc63bba5ef251c625fb0449306a0d4d6f9618f9c68ae2f589fa0d1c3c990742 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'message_header' => [$this, 'block_message_header'],
            'message_title' => [$this, 'block_message_title'],
            'message_content' => [$this, 'block_message_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site_notifications/_base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site_notifications/_base.html.twig"));

        // line 1
        echo "<div class=\"notification-container\">

    <div class=\"container\">

        <div class=\"row\">

            <div class=\"notification-wrapper\">

                <div class=\"poll-vote p-3 p-md-4 rounded\">

                    <div class=\"d-none d-md-inline\">
                        <h1 class=\"h5\">
                            ";
        // line 13
        $this->displayBlock('message_header', $context, $blocks);
        // line 14
        echo "                        </h1>

                        <a href=\"#\" class=\"js-hide-notification close-poll\"
                           data-api-url=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_global_notification_hide", ["notificationName" =>         // line 18
(isset($context["templateName"]) || array_key_exists("templateName", $context) ? $context["templateName"] : (function () { throw new RuntimeError('Variable "templateName" does not exist.', 18, $this->source); })())]), "html", null, true);
        // line 19
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close</a>
                        <hr>
                    </div>

                    <h2 class=\"h4\">
                        ";
        // line 25
        $this->displayBlock('message_title', $context, $blocks);
        // line 26
        echo "                    </h2>

                    <hr class=\"d-block d-md-none\">

                    <div class=\"poll-choices \">
                        ";
        // line 31
        $this->displayBlock('message_content', $context, $blocks);
        // line 32
        echo "                    </div>

                    <div class=\"text-center d-block d-md-none mt-2\">
                        <a href=\"#\" class=\"js-hide-notification\"
                           data-api-url=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_global_notification_hide", ["notificationName" =>         // line 37
(isset($context["templateName"]) || array_key_exists("templateName", $context) ? $context["templateName"] : (function () { throw new RuntimeError('Variable "templateName" does not exist.', 37, $this->source); })())]), "html", null, true);
        // line 38
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 13
    public function block_message_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_header"));

        echo "🔔 Notification";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_message_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 31
    public function block_message_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "message_content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site_notifications/_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 31,  140 => 25,  121 => 13,  98 => 38,  96 => 37,  95 => 36,  89 => 32,  87 => 31,  80 => 26,  78 => 25,  70 => 19,  68 => 18,  67 => 17,  62 => 14,  60 => 13,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"notification-container\">

    <div class=\"container\">

        <div class=\"row\">

            <div class=\"notification-wrapper\">

                <div class=\"poll-vote p-3 p-md-4 rounded\">

                    <div class=\"d-none d-md-inline\">
                        <h1 class=\"h5\">
                            {% block message_header %}🔔 Notification{% endblock %}
                        </h1>

                        <a href=\"#\" class=\"js-hide-notification close-poll\"
                           data-api-url=\"{{ path('api_global_notification_hide', {
                               'notificationName': templateName
                           }) }}\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close</a>
                        <hr>
                    </div>

                    <h2 class=\"h4\">
                        {% block message_title %}{% endblock %}
                    </h2>

                    <hr class=\"d-block d-md-none\">

                    <div class=\"poll-choices \">
                        {% block message_content %}{% endblock %}
                    </div>

                    <div class=\"text-center d-block d-md-none mt-2\">
                        <a href=\"#\" class=\"js-hide-notification\"
                           data-api-url=\"{{ path('api_global_notification_hide', {
                               'notificationName': templateName
                           }) }}\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>", "site_notifications/_base.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/site_notifications/_base.html.twig");
    }
}
