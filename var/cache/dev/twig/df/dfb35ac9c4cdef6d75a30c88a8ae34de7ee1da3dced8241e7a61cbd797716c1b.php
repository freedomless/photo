<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curated-by.html.twig */
class __TwigTemplate_03905f37fbace7b1004427c80cc5a8cd22c063080424813f2fe7313bfc378e05 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curated-by.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curated-by.html.twig"));

        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-text\">

        <ul class=\"list-group list-group-flush\">

            <li class=\"list-group-item\">

                ";
        // line 9
        if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 9, $this->source); })()), 2)) {
            // line 10
            echo "                    <h3 class=\"h6 font-weight-bold\">
                        ";
            // line 11
            if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 11, $this->source); })()), "parent", [], "any", false, false, false, 11))) {
                // line 12
                echo "                            <span class=\"text-danger\">Rejected</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 12, $this->source); })()), "parent", [], "any", false, false, false, 12), "publisher", [], "any", false, false, false, 12), "profile", [], "any", false, false, false, 12), "fullName", [], "any", false, false, false, 12), "html", null, true);
                echo "
                        ";
            } else {
                // line 14
                echo "                            <span class=\"text-danger\">Rejected</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 14, $this->source); })()), "publisher", [], "any", false, false, false, 14), "profile", [], "any", false, false, false, 14), "fullName", [], "any", false, false, false, 14), "html", null, true);
                echo "
                        ";
            }
            // line 16
            echo "                    </h3>
                ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 17
(isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 17, $this->source); })()), 3)) {
            // line 18
            echo "                    <h3 class=\"h6 font-weight-bold\">
                        ";
            // line 19
            if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })()), "parent", [], "any", false, false, false, 19))) {
                // line 20
                echo "                            <span class=\"text-success\">Published</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 20, $this->source); })()), "parent", [], "any", false, false, false, 20), "publisher", [], "any", false, false, false, 20), "profile", [], "any", false, false, false, 20), "fullName", [], "any", false, false, false, 20), "html", null, true);
                echo "
                        ";
            } else {
                // line 22
                echo "                            <span class=\"text-success\">Published</span> by: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 22, $this->source); })()), "publisher", [], "any", false, false, false, 22), "profile", [], "any", false, false, false, 22), "fullName", [], "any", false, false, false, 22), "html", null, true);
                echo "
                        ";
            }
            // line 24
            echo "                    </h3>
                ";
        }
        // line 26
        echo "
            </li>

        </ul>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curated-by.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 26,  94 => 24,  88 => 22,  82 => 20,  80 => 19,  77 => 18,  75 => 17,  72 => 16,  66 => 14,  60 => 12,  58 => 11,  55 => 10,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card mt-3\">

    <div class=\"card-text\">

        <ul class=\"list-group list-group-flush\">

            <li class=\"list-group-item\">

                {% if workflow_has_marked_place(post,2) %}
                    <h3 class=\"h6 font-weight-bold\">
                        {% if post.parent is not null %}
                            <span class=\"text-danger\">Rejected</span> by: {{ post.parent.publisher.profile.fullName }}
                        {% else %}
                            <span class=\"text-danger\">Rejected</span> by: {{ post.publisher.profile.fullName }}
                        {% endif %}
                    </h3>
                {% elseif workflow_has_marked_place(post,3) %}
                    <h3 class=\"h6 font-weight-bold\">
                        {% if post.parent is not null %}
                            <span class=\"text-success\">Published</span> by: {{ post.parent.publisher.profile.fullName }}
                        {% else %}
                            <span class=\"text-success\">Published</span> by: {{ post.publisher.profile.fullName }}
                        {% endif %}
                    </h3>
                {% endif %}

            </li>

        </ul>

    </div>

</div>
", "admin/curator/_partials/curated-by.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curated-by.html.twig");
    }
}
