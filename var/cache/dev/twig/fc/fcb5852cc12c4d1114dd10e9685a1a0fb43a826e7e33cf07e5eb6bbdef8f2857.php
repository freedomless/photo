<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* member/nav.html.twig */
class __TwigTemplate_3e77e652858326902a4284679b12e73e89a4d97205b2913541ebb317a6a614b4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "member/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "member/nav.html.twig"));

        // line 1
        echo "<ul class=\"nav justify-content-center custom-active basic-nav\">

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("alphabetical", "page", "active shadow"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members");
        echo "\">
            All Members</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("founders", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "founders"]);
        echo "\">
            PiART Founders</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("following", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "following"]);
        echo "\">Followed</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link ";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("followers", "page", "active shadow"), "html", null, true);
        echo "\"
           href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members", ["page" => "followers"]);
        echo "\">Followers</a>
    </li>

</ul>

<hr>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "member/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  81 => 20,  74 => 16,  70 => 15,  62 => 10,  58 => 9,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"nav justify-content-center custom-active basic-nav\">

    <li class=\"nav-item\">
        <a class=\"nav-link {{ active('alphabetical','page','active shadow') }}\" href=\"{{ path('web_members') }}\">
            All Members</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link {{ active('founders','page','active shadow') }}\"
           href=\"{{ path('web_members', {page: 'founders'}) }}\">
            PiART Founders</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link {{ active('following','page','active shadow') }}\"
           href=\"{{ path('web_members',{page:'following'}) }}\">Followed</a>
    </li>

    <li class=\"nav-item\">
        <a class=\"nav-link {{ active('followers','page','active shadow') }}\"
           href=\"{{ path('web_members',{page:'followers'}) }}\">Followers</a>
    </li>

</ul>

<hr>
", "member/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/member/nav.html.twig");
    }
}
