<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* store/index.html.twig */
class __TwigTemplate_3d86688cc9c139b17a8457fafe4066ebb1d0047f5dba365c236fb0891b312824 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "store/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "store/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "store/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Art Prints";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"basic-layout store\">

        <ul class=\"nav justify-content-center custom-active store-nav\">

            <li class=\"nav-item\">
                <a class=\"nav-link ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("single", "type"), "html", null, true);
        echo "\"
                   href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store");
        echo "\"><strong>SINGLE</strong></a>
            </li>

            <li class=\"nav-item \">
                <a class=\"nav-link  ";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("series", "type"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store", ["type" => "series"]);
        echo "\">
                    <strong>SERIES</strong>
                </a>
            </li>

            <li class=\"nav-item \">
                <a class=\"text-info nav-link\" target=\"_blank\" href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_product_details");
        echo "\">
                    <strong>PRODUCT DETAILS</strong>
                </a>
            </li>

        </ul>

        <div class=\"container photos mt-5\">

            <hr>

            ";
        // line 34
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "request", [], "any", false, false, false, 34), "query", [], "any", false, false, false, 34), "all", [], "method", false, false, false, 34);
        // line 35
        echo "
            <i class=\"fas fa-lightbulb d-inline d-md-none js-wall-switch\" id=\"js-bulb\"></i>
            <button class=\"btn btn-black-txt btn-light d-none d-md-inline js-wall-switch\" id=\"js-swich\">LITE WALL</button>

            ";
        // line 39
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "request", [], "any", false, false, false, 39), "attributes", [], "any", false, false, false, 39), "get", [0 => "type"], "method", false, false, false, 39), "single"))) {
            // line 40
            echo "                ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>             // line 42
(isset($context["nude"]) || array_key_exists("nude", $context) ? $context["nude"] : (function () { throw new RuntimeError('Variable "nude" does not exist.', 42, $this->source); })()), "auth" =>             // line 43
(isset($context["auth"]) || array_key_exists("auth", $context) ? $context["auth"] : (function () { throw new RuntimeError('Variable "auth" does not exist.', 43, $this->source); })()), "sorting" => true, "more" => twig_get_attribute($this->env, $this->source,             // line 45
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 45, $this->source); })()), "has_more", [], "any", false, false, false, 45), "page" => "store", "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "filter" => true, "filters" =>             // line 50
(isset($context["filters"]) || array_key_exists("filters", $context) ? $context["filters"] : (function () { throw new RuntimeError('Variable "filters" does not exist.', 50, $this->source); })()), "categories" =>             // line 51
(isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 51, $this->source); })()), "posts" => twig_get_attribute($this->env, $this->source,             // line 52
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 52, $this->source); })()), "items", [], "any", false, false, false, 52), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,             // line 53
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 53, $this->source); })()), "total", [], "any", false, false, false, 53), 0)), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")]]);
            // line 54
            echo "
            ";
        } else {
            // line 56
            echo "                ";
            echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["filter" => 1, "sorting" => true, "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "categories" =>             // line 61
(isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 61, $this->source); })()), "series" => twig_get_attribute($this->env, $this->source,             // line 62
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 62, $this->source); })()), "items", [], "any", false, false, false, 62), "page" => "store"]]);
            // line 63
            echo "
            ";
        }
        // line 65
        echo "
        </div>

    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 73
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 74
        echo "<script>
    \$(document).ready(function() {
        \$('.js-wall-switch').click(function(e){
            \$('.main-body').toggleClass('main-store');

            let bulb = \$('#js-bulb');
            bulb.toggleClass('fas')
            bulb.toggleClass('far')

            let bg_switch = \$('#js-swich');
            bg_switch.toggleClass('btn-light')
            bg_switch.toggleClass('btn-dark')
            bg_switch.toggleClass('btn-black-txt')
            bg_switch.text(function(i, text){
                return text === \"LITE WALL\" ? \"DARK WALL\" : \"LITE WALL\";
            })
        });
    });
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "store/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 74,  182 => 73,  166 => 65,  162 => 63,  160 => 62,  159 => 61,  157 => 56,  153 => 54,  151 => 53,  150 => 52,  149 => 51,  148 => 50,  147 => 45,  146 => 43,  145 => 42,  143 => 40,  141 => 39,  135 => 35,  133 => 34,  119 => 23,  108 => 17,  101 => 13,  97 => 12,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Art Prints{% endblock %}

{% block body %}

    <div class=\"basic-layout store\">

        <ul class=\"nav justify-content-center custom-active store-nav\">

            <li class=\"nav-item\">
                <a class=\"nav-link {{ active('single','type') }}\"
                   href=\"{{ path('web_store') }}\"><strong>SINGLE</strong></a>
            </li>

            <li class=\"nav-item \">
                <a class=\"nav-link  {{ active('series','type') }}\" href=\"{{ path('web_store',{type: 'series'}) }}\">
                    <strong>SERIES</strong>
                </a>
            </li>

            <li class=\"nav-item \">
                <a class=\"text-info nav-link\" target=\"_blank\" href=\"{{ path('web_product_details') }}\">
                    <strong>PRODUCT DETAILS</strong>
                </a>
            </li>

        </ul>

        <div class=\"container photos mt-5\">

            <hr>

            {% set filters = app.request.query.all() %}

            <i class=\"fas fa-lightbulb d-inline d-md-none js-wall-switch\" id=\"js-bulb\"></i>
            <button class=\"btn btn-black-txt btn-light d-none d-md-inline js-wall-switch\" id=\"js-swich\">LITE WALL</button>

            {% if app.request.attributes.get('type') == 'single' %}
                {{ react_component('InfinitePost',{rendering:'client_side',props:{
                    mobile:is_mobile(),
                    nude:nude,
                    auth:auth,
                    sorting: true,
                    more: posts.has_more,
                    page: 'store',
                    sizes: sizes(),
                    materials: materials(),
                    filter: true,
                    filters: filters,
                    categories: categories,
                    posts: posts.items,
                    hidden: posts.total != 0,
                    curator: is_granted('ROLE_CURATOR') }}) }}
            {% else %}
                {{ react_component('InfiniteSeries',{rendering:'client_side',props:{
                    filter:1,
                    sorting: true,
                    sizes: sizes(),
                    materials: materials(),
                    categories:categories,
                    series:posts.items,
                    page: 'store'}}) }}
            {% endif %}

        </div>

    </div>


{% endblock %}

{% block js %}
<script>
    \$(document).ready(function() {
        \$('.js-wall-switch').click(function(e){
            \$('.main-body').toggleClass('main-store');

            let bulb = \$('#js-bulb');
            bulb.toggleClass('fas')
            bulb.toggleClass('far')

            let bg_switch = \$('#js-swich');
            bg_switch.toggleClass('btn-light')
            bg_switch.toggleClass('btn-dark')
            bg_switch.toggleClass('btn-black-txt')
            bg_switch.text(function(i, text){
                return text === \"LITE WALL\" ? \"DARK WALL\" : \"LITE WALL\";
            })
        });
    });
</script>
{% endblock %}
", "store/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/store/index.html.twig");
    }
}
