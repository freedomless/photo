<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/admin.html.twig */
class __TwigTemplate_a0e5d69ad1ace1b2a76f0e4745201b41a90953599c3b1f3b3856554f86a3141e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/admin.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/admin.html.twig"));

        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
            // line 2
            echo "
    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"admin\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">

            <i class=\"fa fa-tools nav-admin-icon text-white d-none d-xl-inline-block\"></i>

            <span class=\"d-xl-none ml-xl-3\">ADMIN</span>

        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
             aria-labelledby=\"admin\">

            ";
            // line 17
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 18
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_for_curate");
                echo "\">Curator Panel</a>
            ";
            }
            // line 20
            echo "
            <div class=\"dropdown-divider\"></div>

            <span class=\"ml-4 text-muted\">Manage:</span>

            ";
            // line 25
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN")) {
                // line 26
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_list");
                echo "\">Polls</a>
            ";
            }
            // line 28
            echo "
            ";
            // line 29
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 30
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_manage_photo_of_the_day");
                echo "\">Photo of the day</a>
            ";
            }
            // line 32
            echo "
            ";
            // line 33
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 34
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series");
                echo "\">Curator Series</a>
            ";
            }
            // line 36
            echo "
            ";
            // line 37
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")) {
                // line 38
                echo "                <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_list");
                echo "\">Series Selection</a>
            ";
            }
            // line 40
            echo "
            ";
            // line 41
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_SUPER_ADMIN")) {
                // line 42
                echo "
                <div class=\"dropdown-divider\"></div>
                
                <a class=\"dropdown-item\" href=\"";
                // line 45
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("easyadmin");
                echo "\">Admin Panel</a>
            ";
            }
            // line 47
            echo "
        </div>

    </li>

";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/navigation/admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 47,  128 => 45,  123 => 42,  121 => 41,  118 => 40,  112 => 38,  110 => 37,  107 => 36,  101 => 34,  99 => 33,  96 => 32,  90 => 30,  88 => 29,  85 => 28,  79 => 26,  77 => 25,  70 => 20,  64 => 18,  62 => 17,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if is_granted('ROLE_CURATOR') %}

    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"admin\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">

            <i class=\"fa fa-tools nav-admin-icon text-white d-none d-xl-inline-block\"></i>

            <span class=\"d-xl-none ml-xl-3\">ADMIN</span>

        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-right\"
             aria-labelledby=\"admin\">

            {% if is_granted('ROLE_CURATOR') %}
                <a class=\"dropdown-item\" href=\"{{ path('admin_photo_for_curate') }}\">Curator Panel</a>
            {% endif %}

            <div class=\"dropdown-divider\"></div>

            <span class=\"ml-4 text-muted\">Manage:</span>

            {% if is_granted('ROLE_SUPER_ADMIN') %}
                <a class=\"dropdown-item\" href=\"{{ path('poll_admin_list') }}\">Polls</a>
            {% endif %}

            {% if is_granted('ROLE_CURATOR') %}
                <a class=\"dropdown-item\" href=\"{{ path('admin_manage_photo_of_the_day') }}\">Photo of the day</a>
            {% endif %}

            {% if is_granted('ROLE_CURATOR') %}
                <a class=\"dropdown-item\" href=\"{{ path('admin_series') }}\">Curator Series</a>
            {% endif %}

            {% if is_granted('ROLE_CURATOR') %}
                <a class=\"dropdown-item\" href=\"{{ path('admin_series_selection_list') }}\">Series Selection</a>
            {% endif %}

            {% if is_granted('ROLE_SUPER_ADMIN') %}

                <div class=\"dropdown-divider\"></div>
                
                <a class=\"dropdown-item\" href=\"{{ path('easyadmin') }}\">Admin Panel</a>
            {% endif %}

        </div>

    </li>

{% endif %}
", "_partials/navigation/admin.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/admin.html.twig");
    }
}
