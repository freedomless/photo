<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/filter.html.twig */
class __TwigTemplate_d36a22e057d16d7df17ee9f98793fdd1240c8b09dc5cc27574d2ee9e931f6a3e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/filter.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/filter.html.twig"));

        // line 1
        $context["clearLink"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1));
        // line 2
        echo "
<div class=\"container\">

    <div class=\"default-form-layout\">

        ";
        // line 7
        $context["filters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", [], "any", false, false, false, 7), "query", [], "any", false, false, false, 7), "get", [0 => "filters", 1 => []], "method", false, false, false, 7);
        // line 8
        echo "
        <form>

            <div class=\"form row\">

                <div class=\"col-sm-12 col-md-3\">
                    <div class=\"form-group\">
                        <label for=\"username\">Filter by username</label>
                        <input class=\"form-control\" type=\"text\" id=\"username\"
                               placeholder=\"Filter by Username\"
                               value=\"";
        // line 18
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "username", [], "any", true, true, false, 18)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "username", [], "any", false, false, false, 18))) : ("")), "html", null, true);
        echo "\" name=\"filters[username]\">
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-3\">

                    <div class=\"form-group\">
                        <label for=\"name\">Filter by name</label>
                        <input class=\"form-control\" type=\"text\" id=\"name\"
                               placeholder=\"Filter by Name\"
                               value=\"";
        // line 28
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "name", [], "any", true, true, false, 28)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["filters"] ?? null), "name", [], "any", false, false, false, 28))) : ("")), "html", null, true);
        echo "\"
                               name=\"filters[name]\">
                    </div>

                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <button class=\"form-control btn btn-primary\">Filter</button>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["clearLink"]) || array_key_exists("clearLink", $context) ? $context["clearLink"] : (function () { throw new RuntimeError('Variable "clearLink" does not exist.', 46, $this->source); })()), "html", null, true);
        echo "\" class=\"btn btn-warning form-control text-white\">Clear</a>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_create");
        echo "\" class=\"btn btn-secondary form-control\">Create Series</a>
                    </div>
                </div>

            </div>

        </form>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/filter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 54,  100 => 46,  79 => 28,  66 => 18,  54 => 8,  52 => 7,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set clearLink = path(app.request.get('_route')) %}

<div class=\"container\">

    <div class=\"default-form-layout\">

        {% set filters = app.request.query.get('filters', []) %}

        <form>

            <div class=\"form row\">

                <div class=\"col-sm-12 col-md-3\">
                    <div class=\"form-group\">
                        <label for=\"username\">Filter by username</label>
                        <input class=\"form-control\" type=\"text\" id=\"username\"
                               placeholder=\"Filter by Username\"
                               value=\"{{ filters.username | default }}\" name=\"filters[username]\">
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-3\">

                    <div class=\"form-group\">
                        <label for=\"name\">Filter by name</label>
                        <input class=\"form-control\" type=\"text\" id=\"name\"
                               placeholder=\"Filter by Name\"
                               value=\"{{ filters.name | default }}\"
                               name=\"filters[name]\">
                    </div>

                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <button class=\"form-control btn btn-primary\">Filter</button>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"{{ clearLink }}\" class=\"btn btn-warning form-control text-white\">Clear</a>
                    </div>
                </div>

                <div class=\"col-sm-12 col-md-2\">
                    <div class=\"form-group\">
                        <label for=\"name\">&nbsp;</label>

                        <a href=\"{{ path('admin_series_create') }}\" class=\"btn btn-secondary form-control\">Create Series</a>
                    </div>
                </div>

            </div>

        </form>

    </div>

</div>
", "admin/curator/_partials/filter.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/filter.html.twig");
    }
}
