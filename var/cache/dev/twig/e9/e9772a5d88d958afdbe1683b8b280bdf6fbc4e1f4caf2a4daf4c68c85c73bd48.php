<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/buttons/_pinterest_share.html.twig */
class __TwigTemplate_6a17cca3df0ef514a1a16957bde8e748a5d8dd8b2a86e0987bfa625edcb90f69 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/buttons/_pinterest_share.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/buttons/_pinterest_share.html.twig"));

        // line 1
        echo "<a
        target=\"_blank\"
        class=\"btn share-btn ";
        // line 3
        (((array_key_exists("classes", $context) &&  !(null === (isset($context["classes"]) || array_key_exists("classes", $context) ? $context["classes"] : (function () { throw new RuntimeError('Variable "classes" does not exist.', 3, $this->source); })())))) ? (print (twig_escape_filter($this->env, (isset($context["classes"]) || array_key_exists("classes", $context) ? $context["classes"] : (function () { throw new RuntimeError('Variable "classes" does not exist.', 3, $this->source); })()), "html", null, true))) : (print ("")));
        echo "\"
        href=\"//pinterest.com/pin/create/button/?url=";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["share_url"]) || array_key_exists("share_url", $context) ? $context["share_url"] : (function () { throw new RuntimeError('Variable "share_url" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "&media=";
        echo twig_escape_filter($this->env, (isset($context["media"]) || array_key_exists("media", $context) ? $context["media"] : (function () { throw new RuntimeError('Variable "media" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "&description=";
        echo twig_escape_filter($this->env, (isset($context["description"]) || array_key_exists("description", $context) ? $context["description"] : (function () { throw new RuntimeError('Variable "description" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\">
    <i class=\"fab fa-pinterest-p btn-share-pinterest ";
        // line 5
        (((array_key_exists("icon_classes", $context) &&  !(null === (isset($context["icon_classes"]) || array_key_exists("icon_classes", $context) ? $context["icon_classes"] : (function () { throw new RuntimeError('Variable "icon_classes" does not exist.', 5, $this->source); })())))) ? (print (twig_escape_filter($this->env, (isset($context["icon_classes"]) || array_key_exists("icon_classes", $context) ? $context["icon_classes"] : (function () { throw new RuntimeError('Variable "icon_classes" does not exist.', 5, $this->source); })()), "html", null, true))) : (print ("")));
        echo "\"></i>
</a>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/buttons/_pinterest_share.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 5,  51 => 4,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<a
        target=\"_blank\"
        class=\"btn share-btn {{ classes ?? '' }}\"
        href=\"//pinterest.com/pin/create/button/?url={{ share_url }}&media={{ media }}&description={{ description }}\">
    <i class=\"fab fa-pinterest-p btn-share-pinterest {{ icon_classes ?? '' }}\"></i>
</a>", "_partials/buttons/_pinterest_share.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/buttons/_pinterest_share.html.twig");
    }
}
