<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile/products.html.twig */
class __TwigTemplate_7a877c5ebd4e121a0ffcc2b77a8e149d2e53fbcef7ae69a5fa69814a608a05ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "user/profile/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/products.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile/products.html.twig"));

        $this->parent = $this->loadTemplate("user/profile/index.html.twig", "user/profile/products.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "



    <div class=\"col-12\">

        ";
        // line 10
        if ((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 10, $this->source); })()), "posts", [], "any", false, false, false, 10)), 0))) {
            // line 11
            echo "
            <h3 class=\"mt-5 mb-5 text-center text-muted\">No Products.</h3>

        ";
        } else {
            // line 15
            echo "
            ";
            // line 16
            $context["stats"] = $this->extensions['App\Twig\AccountStatsExtension']->getAccountStats(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "request", [], "any", false, false, false, 16), "get", [0 => "slug"], "method", false, false, false, 16));
            // line 17
            echo "
            ";
            // line 18
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "request", [], "any", false, false, false, 18), "get", [0 => "type"], "method", false, false, false, 18), "single"))) {
                // line 19
                echo "
                ";
                // line 20
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfinitePost", ["rendering" => "client_side", "props" => ["mobile" => $this->extensions['App\Twig\IsMobileExtension']->isMobile(), "nude" =>                 // line 22
(isset($context["nude"]) || array_key_exists("nude", $context) ? $context["nude"] : (function () { throw new RuntimeError('Variable "nude" does not exist.', 22, $this->source); })()), "auth" =>                 // line 23
(isset($context["auth"]) || array_key_exists("auth", $context) ? $context["auth"] : (function () { throw new RuntimeError('Variable "auth" does not exist.', 23, $this->source); })()), "more" => twig_get_attribute($this->env, $this->source,                 // line 24
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 24, $this->source); })()), "has_more", [], "any", false, false, false, 24), "page" => "user-store", "user" => twig_get_attribute($this->env, $this->source,                 // line 26
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 26, $this->source); })()), "id", [], "any", false, false, false, 26), "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "posts" => twig_get_attribute($this->env, $this->source,                 // line 29
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 29, $this->source); })()), "items", [], "any", false, false, false, 29), "hidden" => (0 !== twig_compare(twig_get_attribute($this->env, $this->source,                 // line 30
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 30, $this->source); })()), "total", [], "any", false, false, false, 30), 0)), "curator" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_CURATOR")]]);
                // line 31
                echo "

            ";
            } else {
                // line 34
                echo "                ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("InfiniteSeries", ["rendering" => "client_side", "props" => ["sorting" => true, "sizes" => $this->extensions['App\Twig\SizeExtension']->sizes(), "materials" => $this->extensions['App\Twig\MaterialExtension']->materials(), "series" => twig_get_attribute($this->env, $this->source,                 // line 38
(isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 38, $this->source); })()), "items", [], "any", false, false, false, 38), "page" => "store"]]);
                // line 39
                echo "

            ";
            }
            // line 42
            echo "
        ";
        }
        // line 44
        echo "
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/profile/products.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 44,  119 => 42,  114 => 39,  112 => 38,  110 => 34,  105 => 31,  103 => 30,  102 => 29,  101 => 26,  100 => 24,  99 => 23,  98 => 22,  97 => 20,  94 => 19,  92 => 18,  89 => 17,  87 => 16,  84 => 15,  78 => 11,  76 => 10,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'user/profile/index.html.twig' %}

{% block content %}




    <div class=\"col-12\">

        {% if user.posts | length == 0 %}

            <h3 class=\"mt-5 mb-5 text-center text-muted\">No Products.</h3>

        {% else %}

            {% set stats = account_stats(app.request.get('slug')) %}

            {% if app.request.get('type') == 'single' %}

                {{ react_component('InfinitePost',{rendering:'client_side',props:{
                    mobile:is_mobile(),
                    nude:nude,
                    auth:auth,
                    more: posts.has_more,
                    page: 'user-store',
                    user: user.id,
                    sizes: sizes(),
                    materials: materials(),
                    posts: posts.items,
                    hidden: posts.total != 0,
                    curator: is_granted('ROLE_CURATOR') }}) }}

            {% else %}
                {{ react_component('InfiniteSeries',{rendering:'client_side',props:{
                    sorting: true,
                    sizes: sizes(),
                    materials: materials(),
                    series:posts.items,
                    page: 'store'}}) }}

            {% endif %}

        {% endif %}

    </div>


{% endblock %}
", "user/profile/products.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/user/profile/products.html.twig");
    }
}
