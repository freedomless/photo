<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photo/upload-series.html.twig */
class __TwigTemplate_d348eb14e6d14aff4d8d127c3166bf0aa3e5de195b6f4a768ac714623f38226a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'js' => [$this, 'block_js'],
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload-series.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photo/upload-series.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "photo/upload-series.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Upload Series";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7), "isVerified", [], "any", false, false, false, 7), false))) {
            // line 8
            echo "        ";
            $this->loadTemplate("_partials/resent-confirmation.html.twig", "photo/upload-series.html.twig", 8)->display($context);
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    ";
            // line 16
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), 'form_start');
            echo "

                    <hr>

                    <h3 class=\"d-flex\">
                        <span class=\"flex-grow-1\">";
            // line 21
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "vars", [], "any", false, false, false, 21), "method", [], "any", false, false, false, 21), "POST"))) ? ("Create") : ("Update"));
            echo " Series</span>

                        ";
            // line 23
            if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "vars", [], "any", false, false, false, 23), "method", [], "any", false, false, false, 23), "PUT")) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 23), "vars", [], "any", true, true, false, 23))) {
                // line 24
                echo "                            <button class=\"btn btn-blue curator-warning-btn\"
                                    data-toggle=\"tooltip\"
                                    data-placement=\"bottom\"
                                    data-title=\"Send to Curators\"
                                    type=\"button\"
                                    data-url=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_curate", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 29, $this->source); })()), "id", [], "any", false, false, false, 29), "page" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 29, $this->source); })()), "request", [], "any", false, false, false, 29), "get", [0 => "page"], "method", false, false, false, 29)]), "html", null, true);
                echo "\">
                                Send For Curate
                            </button>

                        ";
            }
            // line 34
            echo "
                    </h3>

                    <hr>

                    ";
            // line 39
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), 'errors');
            echo "

                    <div class=\"row mb-5\">

                        <div class=\"col-12\">

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 47
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 47, $this->source); })()), "category", [], "any", false, false, false, 47), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 48
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 48, $this->source); })()), "category", [], "any", false, false, false, 48), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 54
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 54, $this->source); })()), "title", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 55
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 55, $this->source); })()), "title", [], "any", false, false, false, 55), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 61, $this->source); })()), "tags", [], "any", false, false, false, 61), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 62, $this->source); })()), "tags", [], "any", false, false, false, 62), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                ";
            // line 68
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 68, $this->source); })()), "description", [], "any", false, false, false, 68), 'label', ["label_attr" => ["class" => "col-md-3 col-form-label"]]);
            echo "
                                ";
            // line 69
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 69, $this->source); })()), "description", [], "any", false, false, false, 69), 'widget', ["attr" => ["class" => "col-md-9"]]);
            echo "

                            </div>

                            <div class=\"row pl-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-md-9\">

                                    ";
            // line 79
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 79, $this->source); })()), "isNude", [], "any", false, false, false, 79), 'label');
            echo "
                                    ";
            // line 80
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 80, $this->source); })()), "isNude", [], "any", false, false, false, 80), 'widget');
            echo "

                                    ";
            // line 82
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 82, $this->source); })()), "isGreyscale", [], "any", false, false, false, 82), 'label');
            echo "
                                    ";
            // line 83
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 83, $this->source); })()), "isGreyscale", [], "any", false, false, false, 83), 'widget');
            echo "

                                </div>

                            </div>

                        </div>

                    </div>

                    ";
            // line 93
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 93), "vars", [], "any", true, true, false, 93)) {
                // line 94
                echo "
                        <h3 class=\"text-muted\">
                            Photos: (<span class=\"counter\">";
                // line 96
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 96, $this->source); })()), "children", [], "any", false, false, false, 96)), "html", null, true);
                echo "</span>/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 96, $this->source); })()), "max", [], "any", false, false, false, 96), "html", null, true);
                echo ")
                            <small class=\"float-right text-danger ml-5 errors-min d-none\">You must add at least <span class=\"counter\">";
                // line 97
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 97, $this->source); })()), "children", [], "any", false, false, false, 97)), "html", null, true);
                echo "</span> photos!</small>
                        </h3>

                        <hr>

                        <div class=\"row series-list mb-5\"
                             data-prototype=\"";
                // line 103
                echo twig_call_macro($macros["_self"], "macro_image", [], 103, $context, $this->getSourceContext());
                echo "\"
                             data-widget-counter=\"";
                // line 104
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 104, $this->source); })()), "children", [], "any", false, false, false, 104)), "html", null, true);
                echo "\">

                            ";
                // line 106
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 106, $this->source); })()), "children", [], "any", false, false, false, 106));
                foreach ($context['_seq'] as $context["index"] => $context["child"]) {
                    // line 107
                    echo "
                                <div class=\"col-md-3 mb-3\">

                                    ";
                    // line 110
                    if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 110, $this->source); })()), "vars", [], "any", false, false, false, 110), "method", [], "any", false, false, false, 110), "POST")) || (null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 110), "value", [], "any", false, false, false, 110)))) {
                        // line 111
                        echo "
                                        <div class=\"d-flex justify-content-center align-items-center\">

                                            ";
                        // line 114
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 114), "file", [], "any", false, false, false, 114), 'widget', ["attr" => ["class" => "auto", "data-label" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 114), "file", [], "any", false, false, false, 114), "vars", [], "any", false, false, false, 114), "label", [], "any", false, false, false, 114)]]);
                        echo "

                                        </div>


                                        ";
                        // line 119
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 119), 'widget', ["attr" => ["placeholder" => "Title"]]);
                        echo "

                                    ";
                    } else {
                        // line 122
                        echo "
                                        <div class=\"text-center mb-3 d-flex justify-content-center align-items-center\"
                                             style=\"height: 200px\">

                                            <a href=\"";
                        // line 126
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_update", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 126), "value", [], "any", false, false, false, 126), "id", [], "any", false, false, false, 126)]), "html", null, true);
                        echo "\">
                                                <img src=\"";
                        // line 127
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 127), "value", [], "any", false, false, false, 127), "image", [], "any", false, false, false, 127), "thumbnail", [], "any", false, false, false, 127), "html", null, true);
                        echo "\"
                                                     alt=\"";
                        // line 128
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 128), "value", [], "any", false, false, false, 128), "title", [], "any", false, false, false, 128), "html", null, true);
                        echo "\"
                                                     style=\"max-width: 100%;max-height: 200px\"
                                                />
                                            </a>

                                        </div>

                                        <div class=\"d-flex\">

                                            ";
                        // line 137
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 137), 'widget', ["attr" => ["placeholder" => "Title"]]);
                        echo "

                                            ";
                        // line 139
                        if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 139, $this->source); })()), 0) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 139, $this->source); })()), "cover", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 139), "value", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139))))) {
                            // line 140
                            echo "
                                                <button class=\"btn ml-2\"
                                                        data-toggle=\"tooltip\"
                                                        title=\"Set as cover\"
                                                        formaction=\"";
                            // line 144
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_set_cover", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 144), "value", [], "any", false, false, false, 144), "id", [], "any", false, false, false, 144)]), "html", null, true);
                            echo "\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"far fa-file-image\"></i>

                                                </button>

                                            ";
                        }
                        // line 152
                        echo "
                                            ";
                        // line 153
                        if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 153, $this->source); })()), 0) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 153, $this->source); })()), "cover", [], "any", false, false, false, 153), "id", [], "any", false, false, false, 153), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 153), "value", [], "any", false, false, false, 153), "id", [], "any", false, false, false, 153)))) && (1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 153, $this->source); })()), "children", [], "any", false, false, false, 153)), twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 153, $this->source); })()), "min", [], "any", false, false, false, 153))))) {
                            // line 154
                            echo "                                                <button class=\"btn text-danger ml-2 delete-warning-btn\"
                                                        data-toggle=\"tooltip\"
                                                        type=\"button\"
                                                        title=\"Remove from series\"
                                                        data-url=\"";
                            // line 158
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 158), "value", [], "any", false, false, false, 158), "id", [], "any", false, false, false, 158)]), "html", null, true);
                            echo "\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"fa fa-trash-alt\"></i>

                                                </button>
                                            ";
                        }
                        // line 165
                        echo "                                        </div>

                                    ";
                    }
                    // line 168
                    echo "
                                </div>

                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['index'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 172
                echo "
                            ";
                // line 173
                if ((-1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 173, $this->source); })()), "children", [], "any", false, false, false, 173)), twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 173, $this->source); })()), "max", [], "any", false, false, false, 173)))) {
                    // line 174
                    echo "                                <div class=\"col-md-3 mb-3 series-add-button-wrapper\">

                                    <button type=\"button\" class=\"series-add-button shadow\">
                                        <i class=\"fas fa-plus\"></i>
                                    </button>

                                </div>

                            ";
                }
                // line 183
                echo "
                        </div>

                    ";
            } else {
                // line 187
                echo "
                        <div class=\"row\">

                            ";
                // line 190
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 190, $this->source); })()), "children", [], "any", false, false, false, 190));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 191
                    echo "
                                <div class=\"col-md-3 mb-3\">

                                    <img src=\"";
                    // line 194
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "image", [], "any", false, false, false, 194), "thumbnail", [], "any", false, false, false, 194), "html", null, true);
                    echo "\" alt=\"\" height=\"200px\" width=\"100%\"
                                         style=\"object-fit: cover\">

                                </div>

                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 200
                echo "
                        </div>

                    ";
            }
            // line 204
            echo "
                    <div class=\"text-center\">

                        ";
            // line 207
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 207, $this->source); })()), "upload", [], "any", false, false, false, 207), 'widget', ["label" => (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 207, $this->source); })()), "vars", [], "any", false, false, false, 207), "method", [], "any", false, false, false, 207), "POST"))) ? ("Create") : ("Update"))]);
            echo "

                    </div>

                    ";
            // line 211
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 211, $this->source); })()), 'form_end');
            echo "
                </div>

            </div>

        </div>
    ";
        }
        // line 218
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 222
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 223
        echo "
    <script>

        \$.min = '";
        // line 226
        (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 226, $this->source); })()), "vars", [], "any", false, false, false, 226), "method", [], "any", false, false, false, 226), "POST"))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 226, $this->source); })()), "min", [], "any", false, false, false, 226), "html", null, true))) : (print (0)));
        echo "'

        jQuery(document).ready(function () {

            \$('.custom-file').dropzone({form: \$('form')})

            let list = jQuery('.series-list');
            // Try to find the counter of the list or use the length of the list
            let counter = list.data('widget-counter') || list.children().length;

            let \$series = \$('.series-add-button-wrapper');
            let \$counter = \$('.counter');

            jQuery('.series-add-button').click(function (e) {
                // grab the prototype template
                var newWidget = list.attr('data-prototype');
                // replace the \"__name__\" used in the id and name of the prototype
                // with a number that's unique to your emails
                // end name attribute looks like name=\"contact[emails][2]\"
                newWidget = newWidget.replace(/__name__/g, counter);
                // Increase the counter
                counter++;
                // And store it, the length cannot be used if deleting widgets is allowed
                list.data('widget-counter', counter);

                // create a new list element and add it to the list
                var newElem = jQuery(newWidget);

                var remove = \$('<button class=\"remove-series\"><i class=\"fas fa-times\"></i></button>')

                newElem.prepend(remove)

                \$counter.text(counter);

                remove.on('click', function () {
                    newElem.remove()
                    counter--;
                    \$counter.text(counter);
                    list.data('widget-counter', counter);

                    let event = new CustomEvent('series-child-removed')
                    document.dispatchEvent(event);

                    --\$.min

                    if (counter < '";
        // line 271
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 271, $this->source); })()), "max", [], "any", false, false, false, 271), "html", null, true);
        echo "') {
                        \$series.show()
                    }
                })

                \$series.before(newElem)

                ++\$.min

                newElem.find('.custom-file').dropzone({form: \$('form')});

                if (counter == '";
        // line 282
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 282, $this->source); })()), "max", [], "any", false, false, false, 282), "html", null, true);
        echo "') {
                    \$series.hide()
                }

            });
        });

    </script>

    ";
        // line 291
        $macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"] = $this->loadTemplate("_partials/warning.html.twig", "photo/upload-series.html.twig", 291)->unwrap();
        // line 292
        echo "
    ";
        // line 293
        echo twig_call_macro($macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"], "macro_warning", ["delete-warning-modal", "delete-warning-form", "delete-warning-btn", "Confirm!", "Are you sure you want delete this photo?"], 293, $context, $this->getSourceContext());
        echo "

    ";
        // line 295
        echo twig_call_macro($macros["__internal_bbf5b86128b8c42f1f15696ed1b86f4b100a60d4453795f1da4927478b78331c"], "macro_warning", ["curator-warning-modal", "curator-warning-form", "curator-warning-btn", "Confirm!", "Are you sure you want to send this photo to curator?"], 295, $context, $this->getSourceContext());
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 299
    public function macro_image(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "image"));

            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "image"));

            // line 300
            echo "    <div class='col-md-3 mb-3'>
        <div class='d-flex justify-content-center align-items-center'>
            <div class='custom-file'>
                <input type='file'
                       id='post_children___name___image_file'
                       name='post[children][__name__][image][file]'
                       class='auto custom-file-input'
                       data-label='Click or Drag & Drop File'
                       accept='image/jpeg,image/png'>
                <label for='post_children___name___image_file' class='custom-file-label'></label>
            </div>
        </div>
        <input type='text' id='post_children___name___title' name='post[children][__name__][title]' placeholder='Title'
               class='form-control'></div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

            
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "photo/upload-series.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  602 => 300,  584 => 299,  571 => 295,  566 => 293,  563 => 292,  561 => 291,  549 => 282,  535 => 271,  487 => 226,  482 => 223,  472 => 222,  460 => 218,  450 => 211,  443 => 207,  438 => 204,  432 => 200,  420 => 194,  415 => 191,  411 => 190,  406 => 187,  400 => 183,  389 => 174,  387 => 173,  384 => 172,  375 => 168,  370 => 165,  360 => 158,  354 => 154,  352 => 153,  349 => 152,  338 => 144,  332 => 140,  330 => 139,  325 => 137,  313 => 128,  309 => 127,  305 => 126,  299 => 122,  293 => 119,  285 => 114,  280 => 111,  278 => 110,  273 => 107,  269 => 106,  264 => 104,  260 => 103,  251 => 97,  245 => 96,  241 => 94,  239 => 93,  226 => 83,  222 => 82,  217 => 80,  213 => 79,  200 => 69,  196 => 68,  187 => 62,  183 => 61,  174 => 55,  170 => 54,  161 => 48,  157 => 47,  146 => 39,  139 => 34,  131 => 29,  124 => 24,  122 => 23,  117 => 21,  109 => 16,  101 => 10,  98 => 9,  95 => 8,  93 => 7,  90 => 6,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Upload Series{% endblock %}

{% block body %}

    {% if app.user.isVerified == false %}
        {% include '_partials/resent-confirmation.html.twig' %}
    {% else %}
        <div class=\"container basic-layout\">

            <div class=\"upload-single\">

                <div class=\"upload\">

                    {{ form_start(form) }}

                    <hr>

                    <h3 class=\"d-flex\">
                        <span class=\"flex-grow-1\">{{ form.vars.method == 'POST' ? 'Create' : 'Update' }} Series</span>

                        {% if form.vars.method == 'PUT' and form.children.vars is defined %}
                            <button class=\"btn btn-blue curator-warning-btn\"
                                    data-toggle=\"tooltip\"
                                    data-placement=\"bottom\"
                                    data-title=\"Send to Curators\"
                                    type=\"button\"
                                    data-url=\"{{ path('web_photo_for_curate',{id: post.id, page:app.request.get('page')}) }}\">
                                Send For Curate
                            </button>

                        {% endif %}

                    </h3>

                    <hr>

                    {{ form_errors(form) }}

                    <div class=\"row mb-5\">

                        <div class=\"col-12\">

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.category, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.category,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.title, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.title,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.tags, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.tags,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"form-group row pl-3 pr-3\">

                                {{ form_label(form.description, null, {label_attr:{class:'col-md-3 col-form-label'}}) }}
                                {{ form_widget(form.description,{attr:{class:'col-md-9'}}) }}

                            </div>

                            <div class=\"row pl-3\">

                                <div class=\"col-md-3\"></div>

                                <div class=\"col-md-9\">

                                    {{ form_label(form.isNude) }}
                                    {{ form_widget(form.isNude) }}

                                    {{ form_label(form.isGreyscale) }}
                                    {{ form_widget(form.isGreyscale) }}

                                </div>

                            </div>

                        </div>

                    </div>

                    {% if form.children.vars is defined %}

                        <h3 class=\"text-muted\">
                            Photos: (<span class=\"counter\">{{ form.children | length }}</span>/{{ settings.max }})
                            <small class=\"float-right text-danger ml-5 errors-min d-none\">You must add at least <span class=\"counter\">{{ form.children | length }}</span> photos!</small>
                        </h3>

                        <hr>

                        <div class=\"row series-list mb-5\"
                             data-prototype=\"{{ _self.image() }}\"
                             data-widget-counter=\"{{ form.children|length }}\">

                            {% for index,child in form.children %}

                                <div class=\"col-md-3 mb-3\">

                                    {% if form.vars.method == 'POST' or child.vars.value is null %}

                                        <div class=\"d-flex justify-content-center align-items-center\">

                                            {{ form_widget(child.image.file,{attr:{class:'auto','data-label':child.image.file.vars.label}}) }}

                                        </div>


                                        {{ form_widget(child.title,{attr:{placeholder:'Title'}}) }}

                                    {% else %}

                                        <div class=\"text-center mb-3 d-flex justify-content-center align-items-center\"
                                             style=\"height: 200px\">

                                            <a href=\"{{ path('web_photo_update', {id: child.vars.value.id}) }}\">
                                                <img src=\"{{ child.vars.value.image.thumbnail }}\"
                                                     alt=\"{{ child.vars.value.title }}\"
                                                     style=\"max-width: 100%;max-height: 200px\"
                                                />
                                            </a>

                                        </div>

                                        <div class=\"d-flex\">

                                            {{ form_widget(child.title,{attr:{placeholder: 'Title'}}) }}

                                            {% if workflow_has_marked_place(post,0) and post.cover.id != child.vars.value.id %}

                                                <button class=\"btn ml-2\"
                                                        data-toggle=\"tooltip\"
                                                        title=\"Set as cover\"
                                                        formaction=\"{{ path('web_photo_set_cover',{id:child.vars.value.id}) }}\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"far fa-file-image\"></i>

                                                </button>

                                            {% endif %}

                                            {% if workflow_has_marked_place(post,0) and post.cover.id != child.vars.value.id and post.children | length > settings.min %}
                                                <button class=\"btn text-danger ml-2 delete-warning-btn\"
                                                        data-toggle=\"tooltip\"
                                                        type=\"button\"
                                                        title=\"Remove from series\"
                                                        data-url=\"{{ path('web_photo_delete',{id:child.vars.value.id}) }}\"
                                                        data-original-title=\"Set as cover\">

                                                    <i class=\"fa fa-trash-alt\"></i>

                                                </button>
                                            {% endif %}
                                        </div>

                                    {% endif %}

                                </div>

                            {% endfor %}

                            {% if form.children | length < settings.max %}
                                <div class=\"col-md-3 mb-3 series-add-button-wrapper\">

                                    <button type=\"button\" class=\"series-add-button shadow\">
                                        <i class=\"fas fa-plus\"></i>
                                    </button>

                                </div>

                            {% endif %}

                        </div>

                    {% else %}

                        <div class=\"row\">

                            {% for child in post.children %}

                                <div class=\"col-md-3 mb-3\">

                                    <img src=\"{{ child.image.thumbnail }}\" alt=\"\" height=\"200px\" width=\"100%\"
                                         style=\"object-fit: cover\">

                                </div>

                            {% endfor %}

                        </div>

                    {% endif %}

                    <div class=\"text-center\">

                        {{ form_widget(form.upload,{label: form.vars.method == 'POST' ? 'Create' : 'Update' }) }}

                    </div>

                    {{ form_end(form) }}
                </div>

            </div>

        </div>
    {% endif %}


{% endblock %}

{% block js %}

    <script>

        \$.min = '{{ form.vars.method == 'POST' ? settings.min : 0 }}'

        jQuery(document).ready(function () {

            \$('.custom-file').dropzone({form: \$('form')})

            let list = jQuery('.series-list');
            // Try to find the counter of the list or use the length of the list
            let counter = list.data('widget-counter') || list.children().length;

            let \$series = \$('.series-add-button-wrapper');
            let \$counter = \$('.counter');

            jQuery('.series-add-button').click(function (e) {
                // grab the prototype template
                var newWidget = list.attr('data-prototype');
                // replace the \"__name__\" used in the id and name of the prototype
                // with a number that's unique to your emails
                // end name attribute looks like name=\"contact[emails][2]\"
                newWidget = newWidget.replace(/__name__/g, counter);
                // Increase the counter
                counter++;
                // And store it, the length cannot be used if deleting widgets is allowed
                list.data('widget-counter', counter);

                // create a new list element and add it to the list
                var newElem = jQuery(newWidget);

                var remove = \$('<button class=\"remove-series\"><i class=\"fas fa-times\"></i></button>')

                newElem.prepend(remove)

                \$counter.text(counter);

                remove.on('click', function () {
                    newElem.remove()
                    counter--;
                    \$counter.text(counter);
                    list.data('widget-counter', counter);

                    let event = new CustomEvent('series-child-removed')
                    document.dispatchEvent(event);

                    --\$.min

                    if (counter < '{{ settings.max }}') {
                        \$series.show()
                    }
                })

                \$series.before(newElem)

                ++\$.min

                newElem.find('.custom-file').dropzone({form: \$('form')});

                if (counter == '{{ settings.max }}') {
                    \$series.hide()
                }

            });
        });

    </script>

    {% from \"_partials/warning.html.twig\" import warning %}

    {{ warning('delete-warning-modal','delete-warning-form','delete-warning-btn','Confirm!', 'Are you sure you want delete this photo?') }}

    {{ warning('curator-warning-modal','curator-warning-form','curator-warning-btn','Confirm!', 'Are you sure you want to send this photo to curator?') }}

{% endblock %}

{% macro image() %}
    <div class='col-md-3 mb-3'>
        <div class='d-flex justify-content-center align-items-center'>
            <div class='custom-file'>
                <input type='file'
                       id='post_children___name___image_file'
                       name='post[children][__name__][image][file]'
                       class='auto custom-file-input'
                       data-label='Click or Drag & Drop File'
                       accept='image/jpeg,image/png'>
                <label for='post_children___name___image_file' class='custom-file-label'></label>
            </div>
        </div>
        <input type='text' id='post_children___name___title' name='post[children][__name__][title]' placeholder='Title'
               class='form-control'></div>
{% endmacro %}
", "photo/upload-series.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/photo/upload-series.html.twig");
    }
}
