<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* poll/_vote.html.twig */
class __TwigTemplate_3d9ab6e4f0e64d413fe6125279152a0b352aa792492a7aea931dfc1273d20d04 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/_vote.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "poll/_vote.html.twig"));

        // line 1
        echo "<div class=\"container-fluid poll-container\">

    <div class=\"row\">

        <div class=\"container\">

            <div class=\"row\">

                <div class=\"col-lg-8 offset-lg-2 col-md-10 offset-md-1 mt-5 mt-sm-2 pt-2 poll-wrapper\">

                    <div class=\"poll-vote p-3 p-md-4 rounded\">

                        <div class=\"d-none d-md-inline\">
                            <h1 class=\"h5\">
                                🙋‍♂️ Hey! We have a new poll. Please vote.
                            </h1>

                            <a href=\"#\" class=\"js-hide-poll close-poll\"
                               data-api-url=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 20
(isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 20, $this->source); })()), "id", [], "any", false, false, false, 20)]), "html", null, true);
        // line 21
        echo "\">
                                <i class=\"fa fa-times\" title=\"Close\"></i></a>

                            <hr>
                        </div>

                        <div class=\"alert alert-danger\" style=\"display: none\" id=\"js-vote-error\">
                            <strong>Error:</strong> <span class=\"msg\"></span>
                        </div>

                        <h2 class=\"h4\">
                            ";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 32, $this->source); })()), "title", [], "any", false, false, false, 32), "html", null, true);
        echo "
                        </h2>

                        <a href=\"#\" class=\"js-hide-poll d-md-none d-inline\"
                           data-api-url=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 37
(isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 37, $this->source); })()), "id", [], "any", false, false, false, 37)]), "html", null, true);
        // line 38
        echo "\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>

                        <hr class=\"d-block d-md-none\">

                        <p class=\"text-muted poll-desc\">";
        // line 44
        echo twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 44, $this->source); })()), "description", [], "any", false, false, false, 44);
        echo "</p>

                        <div class=\"mb-3\">
                            <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 47, $this->source); })()), "link", [], "any", false, false, false, 47), "html", null, true);
        echo "\" target=\"_blank\">
                                <i class=\"fas fa-comment-dots\"></i> Read more (discussion thread)</a>
                        </div>

                        <div class=\"poll-choices\">
                            ";
        // line 52
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 52, $this->source); })()), 'form_start', ["attr" => ["id" => "vote-form", "data-api-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_vote")]]);
        // line 56
        echo "

                            ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 58, $this->source); })()), "choices", [], "any", false, false, false, 58));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 59
            echo "
                                <div class=\"radio\">
                                    ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["choice"], 'widget');
            echo "
                                    ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["choice"], 'label');
            echo "
                                </div>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "

                            <button class=\"btn btn-primary\" type=\"submit\">Vote!</button>
                            <button class=\"btn btn-link gray-text js-hide-poll\"
                                    data-api-url=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_poll_hide_for_session", ["id" => twig_get_attribute($this->env, $this->source,         // line 71
(isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 71, $this->source); })()), "id", [], "any", false, false, false, 71)]), "html", null, true);
        // line 72
        echo "\">Not now (I will vote later)
                            </button>

                            ";
        // line 75
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 75, $this->source); })()), 'form_end');
        echo "
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "poll/_vote.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 75,  148 => 72,  146 => 71,  145 => 70,  139 => 66,  129 => 62,  125 => 61,  121 => 59,  117 => 58,  113 => 56,  111 => 52,  103 => 47,  97 => 44,  89 => 38,  87 => 37,  86 => 36,  79 => 32,  66 => 21,  64 => 20,  63 => 19,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid poll-container\">

    <div class=\"row\">

        <div class=\"container\">

            <div class=\"row\">

                <div class=\"col-lg-8 offset-lg-2 col-md-10 offset-md-1 mt-5 mt-sm-2 pt-2 poll-wrapper\">

                    <div class=\"poll-vote p-3 p-md-4 rounded\">

                        <div class=\"d-none d-md-inline\">
                            <h1 class=\"h5\">
                                🙋‍♂️ Hey! We have a new poll. Please vote.
                            </h1>

                            <a href=\"#\" class=\"js-hide-poll close-poll\"
                               data-api-url=\"{{ path('api_poll_hide_for_session', {
                                   'id': poll.id
                               }) }}\">
                                <i class=\"fa fa-times\" title=\"Close\"></i></a>

                            <hr>
                        </div>

                        <div class=\"alert alert-danger\" style=\"display: none\" id=\"js-vote-error\">
                            <strong>Error:</strong> <span class=\"msg\"></span>
                        </div>

                        <h2 class=\"h4\">
                            {{ poll.title }}
                        </h2>

                        <a href=\"#\" class=\"js-hide-poll d-md-none d-inline\"
                           data-api-url=\"{{ path('api_poll_hide_for_session', {
                               'id': poll.id
                           }) }}\">
                            <i class=\"fa fa-times\" title=\"Close\"></i> Close
                        </a>

                        <hr class=\"d-block d-md-none\">

                        <p class=\"text-muted poll-desc\">{{ poll.description|raw }}</p>

                        <div class=\"mb-3\">
                            <a href=\"{{ poll.link }}\" target=\"_blank\">
                                <i class=\"fas fa-comment-dots\"></i> Read more (discussion thread)</a>
                        </div>

                        <div class=\"poll-choices\">
                            {{ form_start(form,
                                {'attr':
                                    {'id': 'vote-form', 'data-api-url': path('api_poll_vote') }
                                }
                            ) }}

                            {% for choice in form.choices %}

                                <div class=\"radio\">
                                    {{ form_widget(choice) }}
                                    {{ form_label(choice) }}
                                </div>

                            {% endfor %}


                            <button class=\"btn btn-primary\" type=\"submit\">Vote!</button>
                            <button class=\"btn btn-link gray-text js-hide-poll\"
                                    data-api-url=\"{{ path('api_poll_hide_for_session', {
                                        'id': poll.id
                                    }) }}\">Not now (I will vote later)
                            </button>

                            {{ form_end(form) }}
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>", "poll/_vote.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/poll/_vote.html.twig");
    }
}
