<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product/posts.html.twig */
class __TwigTemplate_7a930e1d5e41a4eceac843af00cea4c45e490c22250f6b33ebfb225cb8d9a765 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/posts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product/posts.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">
                ";
            // line 9
            if ((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 9), 1))) {
                // line 10
                echo "                    <i class=\"fas fa-images position-absolute\" style=\"left: 5px;top:5px\" data-toggle=\"tooltip\" title=\"\"
                       data-original-title=\"Series\"></i>
                ";
            }
            // line 13
            echo "                ";
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 13), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 13), "thumbnail", [], "any", false, false, false, 13)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 13), "image", [], "any", false, false, false, 13), "thumbnail", [], "any", false, false, false, 13)));
            // line 14
            echo "
                <img src=\"";
            // line 15
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 15, $this->source); })()), "html", null, true);
            echo "\"
                     alt=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 16), "html", null, true);
            echo "\">

                ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 18), 1)) {
                // line 19
                echo "
                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                ";
            }
            // line 25
            echo "
                ";
            // line 26
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 26), 2)) {
                // line 27
                echo "
                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                ";
            }
            // line 33
            echo "
                ";
            // line 34
            if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 34), 3)) {
                // line 35
                echo "
                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                ";
            }
            // line 41
            echo "
            </div>

            <div class=\"image-thumb-metadata\">

                ";
            // line 46
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "deletionDate", [], "any", false, false, false, 46))) {
                // line 47
                echo "                   <p class=\"text-center\">
                       This product will be removed from store on ";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "deletionDate", [], "any", false, false, false, 48), "Y-m-d"), "html", null, true);
                echo "
                   </p>
                ";
            } else {
                // line 51
                echo "                    <div class=\"actions mb-1 text-center\">

                        <a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_for_sale", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 53), "type" => (((1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 53), 1))) ? ("series") : (null))]), "html", null, true);
                echo "\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"";
                // line 57
                echo (( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 57))) ? ("Update Products") : ("Add to store"));
                echo "\">

                            <i class=\"fas fa-store\"></i>
                            <br>
                            <small class=\"text-muted\">Update Product</small>

                        </a>

                        ";
                // line 65
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "product", [], "any", false, false, false, 65), "status", [], "any", false, false, false, 65), 3))) {
                    // line 66
                    echo "
                            <button type=\"button\"
                                    class=\"btn delete-warning-btn\"
                                    data-url=\"";
                    // line 69
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_product_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 69)]), "html", null, true);
                    echo "\">
                                <i class=\"fas fa-trash text-danger\"></i>
                                <br>
                                <small class=\"text-muted\">Remove</small>
                            </button>

                        ";
                }
                // line 76
                echo "
                    </div>
                ";
            }
            // line 79
            echo "
            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "product/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 79,  171 => 76,  161 => 69,  156 => 66,  154 => 65,  143 => 57,  136 => 53,  132 => 51,  126 => 48,  123 => 47,  121 => 46,  114 => 41,  106 => 35,  104 => 34,  101 => 33,  93 => 27,  91 => 26,  88 => 25,  80 => 19,  78 => 18,  73 => 16,  69 => 15,  66 => 14,  63 => 13,  58 => 10,  56 => 9,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for post in posts %}

    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6\">

        <div class=\"image-thumb-container\">

            <div class=\"image-thumb\">
                {% if post.type > 1 %}
                    <i class=\"fas fa-images position-absolute\" style=\"left: 5px;top:5px\" data-toggle=\"tooltip\" title=\"\"
                       data-original-title=\"Series\"></i>
                {% endif %}
                {% set image = post.type == 1 ?  post.image.thumbnail  : post.cover.image.thumbnail %}

                <img src=\"{{ image }}\"
                     alt=\"{{ post.title }}\">

                {% if workflow_has_marked_place(post.product, 1) %}

                    <div class=\"sticker pending semi-transparent\">
                        Pending Review
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(post.product, 2) %}

                    <div class=\"sticker canceled semi-transparent\">
                        Not approved!
                    </div>

                {% endif %}

                {% if workflow_has_marked_place(post.product, 3) %}

                    <div class=\"sticker approved semi-transparent\">
                        In Store!
                    </div>

                {% endif %}

            </div>

            <div class=\"image-thumb-metadata\">

                {% if post.deletionDate is not null %}
                   <p class=\"text-center\">
                       This product will be removed from store on {{ post.deletionDate|date('Y-m-d') }}
                   </p>
                {% else %}
                    <div class=\"actions mb-1 text-center\">

                        <a href=\"{{ path('web_photo_for_sale', {id:post.id,type: post.type > 1 ? 'series' : null}) }}\"
                           data-toggle=\"tooltip\"
                           data-placement=\"bottom\"
                           class=\"btn btn-default\"
                           title=\"{{ post.product is not null ? 'Update Products' : 'Add to store' }}\">

                            <i class=\"fas fa-store\"></i>
                            <br>
                            <small class=\"text-muted\">Update Product</small>

                        </a>

                        {% if post.product.status == 3 %}

                            <button type=\"button\"
                                    class=\"btn delete-warning-btn\"
                                    data-url=\"{{ path('web_product_delete',{id:post.id}) }}\">
                                <i class=\"fas fa-trash text-danger\"></i>
                                <br>
                                <small class=\"text-muted\">Remove</small>
                            </button>

                        {% endif %}

                    </div>
                {% endif %}

            </div>

        </div>

    </div>

{% endfor %}
", "product/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/product/posts.html.twig");
    }
}
