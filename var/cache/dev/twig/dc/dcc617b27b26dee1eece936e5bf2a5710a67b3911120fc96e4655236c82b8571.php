<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/curator-actions.html.twig */
class __TwigTemplate_f8e3cdceb05d05dc2a8631e1ace9ebf650e9bf7bd6b19bd66324ab8ab27e27a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-actions.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/curator-actions.html.twig"));

        // line 1
        if ((($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 1, $this->source); })()), 1) || $this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 2
(isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 2, $this->source); })()), 2)) || ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace(        // line 3
(isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 3, $this->source); })()), 3) && (1 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 3, $this->source); })()), "showDate", [], "any", false, false, false, 3), twig_date_converter($this->env)))))) {
            // line 4
            echo "
    <div class=\"card-header text-center\">

        <div>

            <form method=\"post\">

                ";
            // line 11
            if (((isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 11, $this->source); })()) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 11, $this->source); })()), "blocker", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "user", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11))))) {
                // line 12
                echo "
                    <p>This post was blocked by ";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 13, $this->source); })()), "blocker", [], "any", false, false, false, 13), "profile", [], "any", false, false, false, 13), "fullName", [], "any", false, false, false, 13), "html", null, true);
                echo "</p>

                ";
            } else {
                // line 16
                echo "
                    ";
                // line 17
                if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 17, $this->source); })()), "to_published")) {
                    // line 18
                    echo "
                        <button class=\"btn btn-outline-success\"
                                formaction=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 20, $this->source); })()), "id", [], "any", false, false, false, 20), "action" => 1]), "html", null, true);
                    echo "\">

                            Publish

                        </button>

                    ";
                }
                // line 27
                echo "
                    ";
                // line 28
                if ($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->canTransition((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "to_rejected")) {
                    // line 29
                    echo "
                        <button class=\"btn btn-outline-danger\" ";
                    // line 30
                    (((1 === twig_compare(twig_length_filter($this->env, (isset($context["reasons"]) || array_key_exists("reasons", $context) ? $context["reasons"] : (function () { throw new RuntimeError('Variable "reasons" does not exist.', 30, $this->source); })())), 0))) ? (print (" type=\"button\" data-toggle=\"modal\" data-target=\"#reject\"")) : (print (twig_escape_filter($this->env, ("formaction=" . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 30, $this->source); })()), "id", [], "any", false, false, false, 30), "action" => 0])), "html", null, true))));
                    echo ">

                            Reject

                        </button>

                    ";
                }
                // line 37
                echo "
                    ";
                // line 38
                if ((null === (isset($context["alreadyBlockedByCurator"]) || array_key_exists("alreadyBlockedByCurator", $context) ? $context["alreadyBlockedByCurator"] : (function () { throw new RuntimeError('Variable "alreadyBlockedByCurator" does not exist.', 38, $this->source); })()))) {
                    // line 39
                    echo "                        <button class=\"btn btn-outline-info\"
                                formaction=\"";
                    // line 40
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_block_post", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 40, $this->source); })()), "id", [], "any", false, false, false, 40)]), "html", null, true);
                    echo "\">
                            Block
                        </button>
                    ";
                }
                // line 44
                echo "
                    ";
                // line 45
                if ((isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 45, $this->source); })())) {
                    // line 46
                    echo "                        <button class=\"btn btn-outline-warning\"
                                formaction=\"";
                    // line 47
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_curator_unblock_post", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 47, $this->source); })()), "id", [], "any", false, false, false, 47)]), "html", null, true);
                    echo "\">
                            Unblock
                        </button>
                    ";
                }
                // line 51
                echo "
                ";
            }
            // line 53
            echo "
                ";
            // line 54
            if ((isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 54, $this->source); })())) {
                // line 55
                echo "                    <div>
                        Blocked until ";
                // line 56
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["blocked"]) || array_key_exists("blocked", $context) ? $context["blocked"] : (function () { throw new RuntimeError('Variable "blocked" does not exist.', 56, $this->source); })()), "endDate", [], "any", false, false, false, 56), "Y-m-d H:i"), "html", null, true);
                echo "
                    </div>
                ";
            }
            // line 59
            echo "
                <hr>

                ";
            // line 62
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 62, $this->source); })()), 1) && (0 === twig_compare((isset($context["voted"]) || array_key_exists("voted", $context) ? $context["voted"] : (function () { throw new RuntimeError('Variable "voted" does not exist.', 62, $this->source); })()), false)))) {
                // line 63
                echo "
                    <button class=\"btn btn-link\" formaction=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_vote", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 64, $this->source); })()), "id", [], "any", false, false, false, 64), "vote" => 1]), "html", null, true);
                echo "\">
                        <i class=\"fa fa-thumbs-up\"></i> Yes
                    </button>

                    <button class=\"btn btn-link text-danger\"
                            formaction=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_vote", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 69, $this->source); })()), "id", [], "any", false, false, false, 69), "vote" => 0]), "html", null, true);
                echo "\">
                        <i class=\"fa fa-thumbs-down\"></i> No
                    </button>

                ";
            }
            // line 74
            echo "
                ";
            // line 75
            if (($this->extensions['Symfony\Bridge\Twig\Extension\WorkflowExtension']->hasMarkedPlace((isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 75, $this->source); })()), 3) && (-1 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 75, $this->source); })()), "showDate", [], "any", false, false, false, 75), twig_date_converter($this->env))))) {
                // line 76
                echo "
                    <strong>Published
                            by: </strong> ";
                // line 78
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 78, $this->source); })()), "publisher", [], "any", false, false, false, 78), "profile", [], "any", false, false, false, 78), "firstName", [], "any", false, false, false, 78), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 78, $this->source); })()), "publisher", [], "any", false, false, false, 78), "profile", [], "any", false, false, false, 78), "lastName", [], "any", false, false, false, 78), "html", null, true);
                echo "

                ";
            }
            // line 81
            echo "
            </form>

        </div>

    </div>


    <div class=\"modal fade\" id=\"reject\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"reject\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <form action=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_curate", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 91, $this->source); })()), "id", [], "any", false, false, false, 91), "action" => 0]), "html", null, true);
            echo "\" method=\"post\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Reject Reasons</h5>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>
                    </div>
                    <div class=\"modal-body\">
                        ";
            // line 100
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reasons"]) || array_key_exists("reasons", $context) ? $context["reasons"] : (function () { throw new RuntimeError('Variable "reasons" does not exist.', 100, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["reason"]) {
                // line 101
                echo "
                            <div class=\"form-check\">
                                <input class=\"form-check-input\" type=\"checkbox\" name=\"reasons[]\" value=\"";
                // line 103
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 103), "html", null, true);
                echo "\"
                                       id=\"reason-";
                // line 104
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 104), "html", null, true);
                echo "\">
                                <label class=\"form-check-label\" for=\"reason-";
                // line 105
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "id", [], "any", false, false, false, 105), "html", null, true);
                echo "\">
                                    ";
                // line 106
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reason"], "reason", [], "any", false, false, false, 106), "html", null, true);
                echo "
                                </label>
                            </div>

                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reason'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                        <button class=\"btn btn-primary\">Reject</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


";
        }
        // line 123
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/curator-actions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 123,  250 => 111,  239 => 106,  235 => 105,  231 => 104,  227 => 103,  223 => 101,  219 => 100,  207 => 91,  195 => 81,  187 => 78,  183 => 76,  181 => 75,  178 => 74,  170 => 69,  162 => 64,  159 => 63,  157 => 62,  152 => 59,  146 => 56,  143 => 55,  141 => 54,  138 => 53,  134 => 51,  127 => 47,  124 => 46,  122 => 45,  119 => 44,  112 => 40,  109 => 39,  107 => 38,  104 => 37,  94 => 30,  91 => 29,  89 => 28,  86 => 27,  76 => 20,  72 => 18,  70 => 17,  67 => 16,  61 => 13,  58 => 12,  56 => 11,  47 => 4,  45 => 3,  44 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if workflow_has_marked_place(post,1) or
    workflow_has_marked_place(post,2) or
    (workflow_has_marked_place(post,3) and post.showDate > date()) %}

    <div class=\"card-header text-center\">

        <div>

            <form method=\"post\">

                {% if blocked and blocked.blocker.id != app.user.id %}

                    <p>This post was blocked by {{ blocked.blocker.profile.fullName }}</p>

                {% else %}

                    {% if workflow_can(post, 'to_published') %}

                        <button class=\"btn btn-outline-success\"
                                formaction=\"{{ path('admin_photo_curate',{id:post.id,action: 1}) }}\">

                            Publish

                        </button>

                    {% endif %}

                    {% if workflow_can(post, 'to_rejected') %}

                        <button class=\"btn btn-outline-danger\" {{ reasons | length > 0 ? ' type=\"button\" data-toggle=\"modal\" data-target=\"#reject\"' :  'formaction='  ~ path('admin_photo_curate',{id:post.id,action: 0}) }}>

                            Reject

                        </button>

                    {% endif %}

                    {% if alreadyBlockedByCurator is null %}
                        <button class=\"btn btn-outline-info\"
                                formaction=\"{{ path('admin_curator_block_post', {id: post.id}) }}\">
                            Block
                        </button>
                    {% endif %}

                    {% if blocked %}
                        <button class=\"btn btn-outline-warning\"
                                formaction=\"{{ path('admin_curator_unblock_post', {id: post.id}) }}\">
                            Unblock
                        </button>
                    {% endif %}

                {% endif %}

                {% if blocked %}
                    <div>
                        Blocked until {{ blocked.endDate | date('Y-m-d H:i') }}
                    </div>
                {% endif %}

                <hr>

                {% if workflow_has_marked_place(post,1) and voted == false %}

                    <button class=\"btn btn-link\" formaction=\"{{ path('admin_photo_vote',{id:post.id,vote:1}) }}\">
                        <i class=\"fa fa-thumbs-up\"></i> Yes
                    </button>

                    <button class=\"btn btn-link text-danger\"
                            formaction=\"{{ path('admin_photo_vote',{id:post.id,vote:0}) }}\">
                        <i class=\"fa fa-thumbs-down\"></i> No
                    </button>

                {% endif %}

                {% if workflow_has_marked_place(post,3) and post.showDate < date() %}

                    <strong>Published
                            by: </strong> {{ post.publisher.profile.firstName }} {{ post.publisher.profile.lastName }}

                {% endif %}

            </form>

        </div>

    </div>


    <div class=\"modal fade\" id=\"reject\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"reject\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <form action=\"{{ path('admin_photo_curate',{id:post.id,action: 0}) }}\" method=\"post\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Reject Reasons</h5>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>
                    </div>
                    <div class=\"modal-body\">
                        {% for reason in reasons %}

                            <div class=\"form-check\">
                                <input class=\"form-check-input\" type=\"checkbox\" name=\"reasons[]\" value=\"{{ reason.id }}\"
                                       id=\"reason-{{ reason.id }}\">
                                <label class=\"form-check-label\" for=\"reason-{{ reason.id }}\">
                                    {{ reason.reason }}
                                </label>
                            </div>

                        {% endfor %}
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                        <button class=\"btn btn-primary\">Reject</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


{% endif %}

", "admin/curator/_partials/curator-actions.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/curator-actions.html.twig");
    }
}
