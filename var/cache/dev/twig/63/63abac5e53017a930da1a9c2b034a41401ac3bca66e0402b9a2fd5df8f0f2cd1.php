<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/_partials/photo-update.html.twig */
class __TwigTemplate_d2af9ff19a25effc5dffff03fe9c4661308903dc7b96669a5fa945370044cde6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/photo-update.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/_partials/photo-update.html.twig"));

        // line 1
        echo "<div class=\"card mt-3\">

    <div class=\"card-header d-flex\">

        <strong class=\"flex-grow-1\">";
        // line 5
        echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 5, $this->source); })()), "type", [], "any", false, false, false, 5), 1))) ? ("Photo") : ("Series"));
        echo " Update</strong>

    </div>
    <div class=\"card-body\">

        <div class=\"card-text\">

            <form action=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_update", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 12, $this->source); })()), "id", [], "any", false, false, false, 12)]), "html", null, true);
        echo "\" method=\"post\">

                <div class=\"form-group\">
                    <label for=\"category_";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15), "html", null, true);
        echo "\">Category</label>
                    <select name=\"category\" class=\"form-control\" id=\"category_";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 16, $this->source); })()), "id", [], "any", false, false, false, 16), "html", null, true);
        echo "\">
                        <option value=\"\" disabled>Select Category</option>
                        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 19
            echo "                            <option ";
            echo (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 19, $this->source); })()), "category", [], "any", false, false, false, 19), "id", [], "any", false, false, false, 19), twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 19)))) ? ("selected") : (""));
            echo "
                                    value=\"";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 20), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 20), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                    </select>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_greyscale_";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 26, $this->source); })()), "id", [], "any", false, false, false, 26), "html", null, true);
        echo "\">
                        <input type=\"hidden\" value=\"0\" name=\"is_greyscale\">
                        <input type=\"checkbox\" id=\"is_greyscale_";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "id", [], "any", false, false, false, 28), "html", null, true);
        echo "\" name=\"is_greyscale\"
                               value=\"1\" ";
        // line 29
        echo ((twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 29, $this->source); })()), "isGreyscale", [], "any", false, false, false, 29)) ? ("checked") : (""));
        echo ">
                        Black & White
                    </label>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_nude_";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 35, $this->source); })()), "id", [], "any", false, false, false, 35), "html", null, true);
        echo "\">
                        <input type=\"hidden\" value=\"0\" name=\"is_nude\">
                        <input type=\"checkbox\" id=\"is_nude_";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 37, $this->source); })()), "id", [], "any", false, false, false, 37), "html", null, true);
        echo "\" name=\"is_nude\"
                               value=\"1\" ";
        // line 38
        echo ((twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 38, $this->source); })()), "isNude", [], "any", false, false, false, 38)) ? ("checked") : (""));
        echo ">
                        Nude Content
                    </label>
                </div>

                <div class=\"text-center m-2\">
                    <button class=\"btn btn-primary\">
                        Save
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/_partials/photo-update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 38,  122 => 37,  117 => 35,  108 => 29,  104 => 28,  99 => 26,  93 => 22,  83 => 20,  78 => 19,  74 => 18,  69 => 16,  65 => 15,  59 => 12,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card mt-3\">

    <div class=\"card-header d-flex\">

        <strong class=\"flex-grow-1\">{{ post.type == 1 ? 'Photo' : 'Series' }} Update</strong>

    </div>
    <div class=\"card-body\">

        <div class=\"card-text\">

            <form action=\"{{ path('admin_photo_update',{id: post.id}) }}\" method=\"post\">

                <div class=\"form-group\">
                    <label for=\"category_{{ post.id }}\">Category</label>
                    <select name=\"category\" class=\"form-control\" id=\"category_{{ post.id }}\">
                        <option value=\"\" disabled>Select Category</option>
                        {% for category in categories %}
                            <option {{ post.category.id == category.id ? 'selected' : '' }}
                                    value=\"{{ category.id }}\">{{ category.name }}</option>
                        {% endfor %}
                    </select>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_greyscale_{{ post.id }}\">
                        <input type=\"hidden\" value=\"0\" name=\"is_greyscale\">
                        <input type=\"checkbox\" id=\"is_greyscale_{{ post.id }}\" name=\"is_greyscale\"
                               value=\"1\" {{ post.isGreyscale ? 'checked' : '' }}>
                        Black & White
                    </label>
                </div>

                <div class=\"form-group\">
                    <label for=\"is_nude_{{ post.id }}\">
                        <input type=\"hidden\" value=\"0\" name=\"is_nude\">
                        <input type=\"checkbox\" id=\"is_nude_{{ post.id }}\" name=\"is_nude\"
                               value=\"1\" {{ post.isNude ? 'checked' : '' }}>
                        Nude Content
                    </label>
                </div>

                <div class=\"text-center m-2\">
                    <button class=\"btn btn-primary\">
                        Save
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>
", "admin/curator/_partials/photo-update.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/_partials/photo-update.html.twig");
    }
}
