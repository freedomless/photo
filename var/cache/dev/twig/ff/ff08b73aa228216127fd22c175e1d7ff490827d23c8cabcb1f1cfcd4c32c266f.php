<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/poll/choice_manage.html.twig */
class __TwigTemplate_36217b338d26f6320b7ae3781ad417bae200877d06f935ec65e863e3cc8cc486 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header_left' => [$this, 'block_header_left'],
            'header_right' => [$this, 'block_header_right'],
            'row' => [$this, 'block_row'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/poll/choice_manage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/poll/choice_manage.html.twig"));

        $this->parent = $this->loadTemplate("template.html.twig", "admin/poll/choice_manage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 3, $this->source); })()), "title", [], "any", false, false, false, 3), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_header_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_left"));

        echo "Add choices for: <strong>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 5, $this->source); })()), "title", [], "any", false, false, false, 5), "html", null, true);
        echo "</strong>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_header_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header_right"));

        // line 7
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("poll_admin_list");
        echo "\">
        <i class=\"fa fa-times\"></i>
    </a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "row"));

        // line 13
        echo "    <div class=\"row default-form-layout\">

        <div class=\"col-12\">

            <div style=\"display: none\" id=\"js-prototype\">

                <div class=\"form-group row\">
                    <div class=\"col-2 col-sm-1\">
                <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                    <i class=\"fa fa-arrows-alt text-white\"></i>
                </span>
                    </div>

                    <div class=\"col-8 col-sm-10\">
                        <input type=\"text\"
                               data-entity-id=\"\" id=\"choice-2\"
                               class=\"form-control js-choice-input\"
                               placeholder=\"Choice text\"
                        >
                    </div>
                    <div class=\"col-2 col-sm-1\">
                        <button class=\"btn btn-danger float-right js-remove-input\">
                            <i class=\"fa fa-minus\"></i>
                        </button>
                    </div>
                </div>

            </div>

            <form action=\"\" method=\"post\">

                <div id=\"js-form-inputs\">

                    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["poll"]) || array_key_exists("poll", $context) ? $context["poll"] : (function () { throw new RuntimeError('Variable "poll" does not exist.', 46, $this->source); })()), "choices", [], "any", false, false, false, 46));
        foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
            // line 47
            echo "
                        <div class=\"form-group row\">
                            <div class=\"col-2 col-sm-1\">

                            <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                                <i class=\"fa fa-arrows-alt text-white\"></i>
                            </span>

                            </div>
                            <div class=\"col-8 col-sm-10\">
                                <input type=\"text\"
                                       data-entity-id=\"\"
                                       class=\"form-control js-choice-input\"
                                       placeholder=\"Choice text\"
                                       value=\"";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["choice"], "title", [], "any", false, false, false, 61), "html", null, true);
            echo "\"
                                >
                            </div>
                            <div class=\"col-2 col-sm-1\">
                                <button class=\"btn btn-danger float-right js-remove-input\">
                                    <i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "
                </div>

                ";
        // line 76
        echo "                <div class=\"form-group row\">
                    <div class=\"col-12\">
                        <button
                                id=\"js-add-more-btn\"
                                class=\"btn btn-success float-right\">
                            <i class=\"fa fa-plus\"></i>
                        </button>
                    </div>
                </div>
                <div class=\"form-group\">

                    <button type=\"submit\" class=\"btn btn-primary\" id=\"js-submit-it\">Save Choices</button>
                </div>

                <input type=\"hidden\" name=\"token\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("submit_choices"), "html", null, true);
        echo "\"/>

            </form>

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 100
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 101
        echo "    <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

    <script src=\"//code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
    <script>
        \$(function () {
            \$(\"#js-form-inputs\").sortable();
            \$(\"#js-form-inputs\").disableSelection();
        });
    </script>
    <script>

        \$(document).ready(function () {

            \$(document).on('click', '.js-remove-input', function (e) {
                e.preventDefault();

                \$(this).parent().parent().remove();

                return false;
            });

            \$('#js-add-more-btn').on('click', function (e) {
                e.preventDefault();

                let row = \$('#js-prototype').clone();
                \$('#js-form-inputs').append(\$(row).html());
            });

            \$('#js-submit-it').on('click', function (e) {

                e.preventDefault();

                let choices = [];
                let inputs = \$('.js-choice-input');

                inputs.each(function (i, item) {
                    let val = \$(item).val();
                    if (val !== '') {
                        choices.push(item);
                    }
                });

                if (choices.length === 0) {
                    return;
                }

                let formSelector = 'form';

                \$(choices).each(function (i, item) {
                    addInputToForm(formSelector, i, item)
                });

                \$(formSelector).submit();
            });
        });

        function addInputToForm(formSelector, index, value) {
            let form = \$(formSelector);
            form.append(
                \$('<input type=\"hidden\" name=\"choice[' + index + ']\" value=\"' + \$(value).val() + '\">')
            );
        }

    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/poll/choice_manage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 101,  249 => 100,  230 => 90,  214 => 76,  209 => 72,  192 => 61,  176 => 47,  172 => 46,  137 => 13,  127 => 12,  112 => 7,  102 => 6,  81 => 5,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'template.html.twig' %}

{% block title %}{{ poll.title }}{% endblock %}

{% block header_left %}Add choices for: <strong>{{ poll.title }}</strong>{% endblock %}
{% block header_right %}
    <a href=\"{{ path('poll_admin_list') }}\">
        <i class=\"fa fa-times\"></i>
    </a>
{% endblock %}

{% block row %}
    <div class=\"row default-form-layout\">

        <div class=\"col-12\">

            <div style=\"display: none\" id=\"js-prototype\">

                <div class=\"form-group row\">
                    <div class=\"col-2 col-sm-1\">
                <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                    <i class=\"fa fa-arrows-alt text-white\"></i>
                </span>
                    </div>

                    <div class=\"col-8 col-sm-10\">
                        <input type=\"text\"
                               data-entity-id=\"\" id=\"choice-2\"
                               class=\"form-control js-choice-input\"
                               placeholder=\"Choice text\"
                        >
                    </div>
                    <div class=\"col-2 col-sm-1\">
                        <button class=\"btn btn-danger float-right js-remove-input\">
                            <i class=\"fa fa-minus\"></i>
                        </button>
                    </div>
                </div>

            </div>

            <form action=\"\" method=\"post\">

                <div id=\"js-form-inputs\">

                    {% for choice in poll.choices %}

                        <div class=\"form-group row\">
                            <div class=\"col-2 col-sm-1\">

                            <span class=\"btn btn-default btn-block\" style=\"cursor: move\">
                                <i class=\"fa fa-arrows-alt text-white\"></i>
                            </span>

                            </div>
                            <div class=\"col-8 col-sm-10\">
                                <input type=\"text\"
                                       data-entity-id=\"\"
                                       class=\"form-control js-choice-input\"
                                       placeholder=\"Choice text\"
                                       value=\"{{ choice.title }}\"
                                >
                            </div>
                            <div class=\"col-2 col-sm-1\">
                                <button class=\"btn btn-danger float-right js-remove-input\">
                                    <i class=\"fa fa-minus\"></i>
                                </button>
                            </div>
                        </div>

                    {% endfor %}

                </div>

                {# Add more inputs button #}
                <div class=\"form-group row\">
                    <div class=\"col-12\">
                        <button
                                id=\"js-add-more-btn\"
                                class=\"btn btn-success float-right\">
                            <i class=\"fa fa-plus\"></i>
                        </button>
                    </div>
                </div>
                <div class=\"form-group\">

                    <button type=\"submit\" class=\"btn btn-primary\" id=\"js-submit-it\">Save Choices</button>
                </div>

                <input type=\"hidden\" name=\"token\" value=\"{{ csrf_token('submit_choices') }}\"/>

            </form>

        </div>

    </div>

{% endblock %}

{% block js %}
    <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

    <script src=\"//code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
    <script>
        \$(function () {
            \$(\"#js-form-inputs\").sortable();
            \$(\"#js-form-inputs\").disableSelection();
        });
    </script>
    <script>

        \$(document).ready(function () {

            \$(document).on('click', '.js-remove-input', function (e) {
                e.preventDefault();

                \$(this).parent().parent().remove();

                return false;
            });

            \$('#js-add-more-btn').on('click', function (e) {
                e.preventDefault();

                let row = \$('#js-prototype').clone();
                \$('#js-form-inputs').append(\$(row).html());
            });

            \$('#js-submit-it').on('click', function (e) {

                e.preventDefault();

                let choices = [];
                let inputs = \$('.js-choice-input');

                inputs.each(function (i, item) {
                    let val = \$(item).val();
                    if (val !== '') {
                        choices.push(item);
                    }
                });

                if (choices.length === 0) {
                    return;
                }

                let formSelector = 'form';

                \$(choices).each(function (i, item) {
                    addInputToForm(formSelector, i, item)
                });

                \$(formSelector).submit();
            });
        });

        function addInputToForm(formSelector, index, value) {
            let form = \$(formSelector);
            form.append(
                \$('<input type=\"hidden\" name=\"choice[' + index + ']\" value=\"' + \$(value).val() + '\">')
            );
        }

    </script>
{% endblock %}", "admin/poll/choice_manage.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/poll/choice_manage.html.twig");
    }
}
