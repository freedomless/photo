<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/navigation/right.html.twig */
class __TwigTemplate_9e00d210e7e6222c4e49494956d40530030489f4730b3ea3cd8607971dc5135b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/right.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/navigation/right.html.twig"));

        // line 1
        echo "<ul class=\"main-nav-links navbar-nav mr-lg-3 pr-lg-1 right-nav\">

    <li class=\"nav-item ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery", "_route"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_series", "_route"), "html", null, true);
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 4
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery", ["page" => "latest"]);
        echo "\">Gallery</a>
    </li>

    ";
        // line 8
        echo "    <li class=\"nav-item dropdown ";
        if ((is_string($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "attributes", [], "any", false, false, false, 8), "get", [0 => "_route"], "method", false, false, false, 8)) && is_string($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = "web_photo_create") && ('' === $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 || 0 === strpos($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4, $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144)))) {
            echo "active";
        }
        echo "\">

        <a class=\"nav-link dropdown-toggle ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_photo_create", "_route"), "html", null, true);
        echo "\"
           href=\"#\" id=\"account\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            Upload
        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-left\"
             aria-labelledby=\"account\">
            <a class=\"dropdown-item\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create");
        echo "\">Photo</a>
            <a class=\"dropdown-item\" href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_photo_create", ["type" => "series"]);
        echo "\">Series</a>
        </div>

    </li>

    <li class=\"nav-item ";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_vote", "_route"), "html", null, true);
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote");
        echo "\">Vote</a>
    </li>

    <li class=\"nav-item ";
        // line 28
        if ((is_string($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 28, $this->source); })()), "request", [], "any", false, false, false, 28), "attributes", [], "any", false, false, false, 28), "get", [0 => "_route"], "method", false, false, false, 28)) && is_string($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = "web_members") && ('' === $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 || 0 === strpos($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b, $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002)))) {
            echo "active";
        }
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_members");
        echo "\">Members</a>
    </li>

    <li class=\"nav-item ";
        // line 32
        if ((is_string($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "request", [], "any", false, false, false, 32), "attributes", [], "any", false, false, false, 32), "get", [0 => "_route"], "method", false, false, false, 32)) && is_string($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = "web_forum") && ('' === $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 || 0 === strpos($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4, $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666)))) {
            echo "active";
        }
        echo "\">
        <a class=\"nav-link\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum");
        echo "\">Forums</a>
    </li>

    ";
        // line 36
        if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 36)) {
            // line 37
            echo "        <li class=\"nav-item ";
            if ((is_string($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 37, $this->source); })()), "request", [], "any", false, false, false, 37), "attributes", [], "any", false, false, false, 37), "get", [0 => "_route"], "method", false, false, false, 37)) && is_string($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = "web_store") && ('' === $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 || 0 === strpos($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e, $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52)))) {
                echo "active";
            }
            echo "\">
            <a class=\"nav-link print-store-label\" href=\"";
            // line 38
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_store");
            echo "\">Art Prints</a>
        </li>
    ";
        }
        // line 41
        echo "
    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\"
           href=\"#\" id=\"more\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <span class=\"d-none d-md-block\"><span class=\"navbar-toggler-icon\"></span></span>
            <span class=\"d-md-none\">More</span>
        </a>

        ";
        // line 52
        echo "        <div class=\"dropdown-menu shadow border-0 dropdown-menu-more\">

            <a class=\"dropdown-item\" href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_tos");
        echo "\">Terms of Service</a>

            <a class=\"dropdown-item\" href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_privacy_policy");
        echo "\">Privacy Policy</a>

            <a class=\"dropdown-item\" href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_user_agreement");
        echo "\">User Agreement</a>

            <a class=\"dropdown-item\" href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_faq");
        echo "\">F.A.Q</a>

            <a class=\"dropdown-item\" href=\"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_contacts");
        echo "\">Contact Us</a>

        </div>

    </li>

    <li class=\"divider-vertical\"></li>


    ";
        // line 72
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 73
            echo "
        ";
            // line 74
            $this->loadTemplate("_partials/navigation/user.html.twig", "_partials/navigation/right.html.twig", 74)->display($context);
            // line 75
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\PostSettingsExtension']->getUserSettings(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 75, $this->source); })()), "user", [], "any", false, false, false, 75)), "is_web_notification_allowed", [], "any", false, false, false, 75)) {
                // line 76
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Notifications", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\NotificationsCountExtension']->getUnseenNotifications(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 76, $this->source); })()), "user", [], "any", false, false, false, 76))]]);
                echo "
        ";
            }
            // line 78
            echo "
        ";
            // line 79
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 79)) {
                // line 80
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("CartButton", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\CartCountExtension']->getCartCount(), "href" => "/cart"]]);
                echo "
        ";
            }
            // line 82
            echo "
        ";
            // line 84
            echo "        ";
            $this->loadTemplate("_partials/navigation/admin.html.twig", "_partials/navigation/right.html.twig", 84)->display($context);
            // line 85
            echo "
    ";
        } else {
            // line 87
            echo "
        ";
            // line 88
            if (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\ConfigExtension']->getConfig(), "allowShopping", [], "any", false, false, false, 88)) {
                // line 89
                echo "            ";
                echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("CartButton", ["rendering" => "client_side", "props" => ["count" => $this->extensions['App\Twig\CartCountExtension']->getCartCount(), "href" => "/cart"]]);
                echo "
        ";
            }
            // line 91
            echo "
        ";
            // line 93
            echo "        <li class=\"nav-item\">
            <a href=\"";
            // line 94
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_login");
            echo "\" class=\"nav-link text-white\">Log in</a>
        </li>

        <li class=\"nav-item\">
            <a href=\"";
            // line 98
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_register");
            echo "\" class=\"nav-link text-white\">Register</a>
        </li>

    ";
        }
        // line 102
        echo "
    ";
        // line 104
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 105
            echo "
        <li class=\"nav-item\">

            ";
            // line 109
            echo "            ";
            // line 110
            echo "            ";
            // line 111
            echo "
            ";
            // line 113
            echo "
            ";
            // line 115
            echo "
            <a href=\"";
            // line 116
            echo twig_escape_filter($this->env, ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 116, $this->source); })()), "request", [], "any", false, false, false, 116), "attributes", [], "any", false, false, false, 116), "get", [0 => "_route"], "method", false, false, false, 116), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 117
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 117, $this->source); })()), "request", [], "any", false, false, false, 117), "attributes", [], "any", false, false, false, 117), "get", [0 => "_route_params"], "method", false, false, false, 117)) . "?_switch_user=_exit"), "html", null, true);
            echo "\" class=\"nav-link\"
               data-toggle=\"tooltip\"
               data-placement=\"bottom\"
               title=\"Exit from switch\">

                <i class=\"fas fa-user-minus\"></i>

            </a>

        </li>

    ";
        }
        // line 129
        echo "
</ul>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/navigation/right.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  296 => 129,  281 => 117,  280 => 116,  277 => 115,  274 => 113,  271 => 111,  269 => 110,  267 => 109,  262 => 105,  259 => 104,  256 => 102,  249 => 98,  242 => 94,  239 => 93,  236 => 91,  230 => 89,  228 => 88,  225 => 87,  221 => 85,  218 => 84,  215 => 82,  209 => 80,  207 => 79,  204 => 78,  198 => 76,  195 => 75,  193 => 74,  190 => 73,  187 => 72,  175 => 62,  170 => 60,  165 => 58,  160 => 56,  155 => 54,  151 => 52,  139 => 41,  133 => 38,  126 => 37,  124 => 36,  118 => 33,  112 => 32,  106 => 29,  100 => 28,  94 => 25,  90 => 24,  82 => 19,  78 => 18,  67 => 10,  59 => 8,  53 => 4,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"main-nav-links navbar-nav mr-lg-3 pr-lg-1 right-nav\">

    <li class=\"nav-item {{ active('web_gallery','_route') }} {{ active('web_gallery_series','_route') }}\">
        <a class=\"nav-link\" href=\"{{ path('web_gallery',{page:'latest'}) }}\">Gallery</a>
    </li>

    {# \"Upload\" Dropdown menu #}
    <li class=\"nav-item dropdown {% if app.request.attributes.get('_route') starts with 'web_photo_create' %}active{% endif %}\">

        <a class=\"nav-link dropdown-toggle {{ active('web_photo_create','_route') }}\"
           href=\"#\" id=\"account\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            Upload
        </a>

        <div class=\"dropdown-menu shadow border-0 dropdown-menu-left\"
             aria-labelledby=\"account\">
            <a class=\"dropdown-item\" href=\"{{ path('web_photo_create') }}\">Photo</a>
            <a class=\"dropdown-item\" href=\"{{ path('web_photo_create', {type: 'series'}) }}\">Series</a>
        </div>

    </li>

    <li class=\"nav-item {{ active('web_vote','_route') }}\">
        <a class=\"nav-link\" href=\"{{ path('web_vote') }}\">Vote</a>
    </li>

    <li class=\"nav-item {% if app.request.attributes.get('_route') starts with 'web_members' %}active{% endif %}\">
        <a class=\"nav-link\" href=\"{{ path('web_members') }}\">Members</a>
    </li>

    <li class=\"nav-item {% if app.request.attributes.get('_route') starts with 'web_forum' %}active{% endif %}\">
        <a class=\"nav-link\" href=\"{{ path('web_forum') }}\">Forums</a>
    </li>

    {% if config().allowShopping %}
        <li class=\"nav-item {% if app.request.attributes.get('_route') starts with 'web_store' %}active{% endif %}\">
            <a class=\"nav-link print-store-label\" href=\"{{ path('web_store') }}\">Art Prints</a>
        </li>
    {% endif %}

    <li class=\"nav-item dropdown\">

        <a class=\"nav-link dropdown-toggle\"
           href=\"#\" id=\"more\" role=\"button\"
           data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <span class=\"d-none d-md-block\"><span class=\"navbar-toggler-icon\"></span></span>
            <span class=\"d-md-none\">More</span>
        </a>

        {# \"More\" Dropdown Menu #}
        <div class=\"dropdown-menu shadow border-0 dropdown-menu-more\">

            <a class=\"dropdown-item\" href=\"{{ path('web_tos') }}\">Terms of Service</a>

            <a class=\"dropdown-item\" href=\"{{ path('web_privacy_policy') }}\">Privacy Policy</a>

            <a class=\"dropdown-item\" href=\"{{ path('web_user_agreement') }}\">User Agreement</a>

            <a class=\"dropdown-item\" href=\"{{ path('web_faq') }}\">F.A.Q</a>

            <a class=\"dropdown-item\" href=\"{{ path('web_contacts') }}\">Contact Us</a>

        </div>

    </li>

    <li class=\"divider-vertical\"></li>


    {# User Menu #}
    {% if is_granted('ROLE_USER') %}

        {% include '_partials/navigation/user.html.twig' %}
        {% if user_settings(app.user).is_web_notification_allowed %}
            {{ react_component('Notifications',{'rendering':'client_side','props': {'count':notification_count(app.user)},}) }}
        {% endif %}

        {% if config().allowShopping %}
            {{ react_component('CartButton',{'rendering':'client_side','props': {count :cart_count(), href: '/cart'}}) }}
        {% endif %}

        {# Admin Menu #}
        {% include '_partials/navigation/admin.html.twig' %}

    {% else %}

        {% if config().allowShopping %}
            {{ react_component('CartButton',{'rendering':'client_side','props': {count :cart_count(), href: '/cart'}}) }}
        {% endif %}

        {# Guests Menu #}
        <li class=\"nav-item\">
            <a href=\"{{ path('web_login') }}\" class=\"nav-link text-white\">Log in</a>
        </li>

        <li class=\"nav-item\">
            <a href=\"{{ path('web_register') }}\" class=\"nav-link text-white\">Register</a>
        </li>

    {% endif %}

    {# Switch button #}
    {% if  is_granted('ROLE_PREVIOUS_ADMIN') %}

        <li class=\"nav-item\">

            {# <a href=\"{{ path('web_profile', {'slug': app.user.slug,'_switch_user': '_exit'}) }}\" class=\"nav-link\" data-toggle=\"tooltip\" #}
            {# data-placement=\"bottom\" #}
            {# title=\"Exit from switch\"> #}

            {# <i class=\"fas fa-user-minus\"></i> #}

            {# </a> #}

            <a href=\"{{ path(app.request.attributes.get('_route'),
                app.request.attributes.get('_route_params')) ~ '?_switch_user=_exit' }}\" class=\"nav-link\"
               data-toggle=\"tooltip\"
               data-placement=\"bottom\"
               title=\"Exit from switch\">

                <i class=\"fas fa-user-minus\"></i>

            </a>

        </li>

    {% endif %}

</ul>
", "_partials/navigation/right.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/navigation/right.html.twig");
    }
}
