<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/index.html.twig */
class __TwigTemplate_f7cac3f244dc53e46608ddbf18d2b1027060f6004f53e4c06e474a0f5b38662c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "forum/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Forum
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
    <div class=\"container basic-layout forums\">

        <h3>Forums</h3>

        <hr>

        <div class=\"row\">

            <div class=\"col-12\">

                <div class=\"list-group\">

                    ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sections"]) || array_key_exists("sections", $context) ? $context["sections"] : (function () { throw new RuntimeError('Variable "sections" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 22
            echo "

                        <div class=\"list-group-item list-group-item-action\">

                            <div class=\"row\">

                                <div class=\"col-1 category-image\">
                                    <img src=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "image", [], "any", false, false, false, 29), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "name", [], "any", false, false, false, 29), "html", null, true);
            echo "\">
                                </div>

                                <div class=\"col-11\">

                                    <div class=\"d-flex w-100 justify-content-between\">

                                        <h5 class=\"mb-1 mt-0\">
                                            <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_section", ["slug" => twig_get_attribute($this->env, $this->source, $context["section"], "slug", [], "any", false, false, false, 37)]), "html", null, true);
            echo "\">
                                                ";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "name", [], "any", false, false, false, 38), "html", null, true);
            echo "
                                            </a>
                                        </h5>

                                        <div>
                                            Number of Topics:
                                            <span class=\"badge badge-primary badge-pill\">
                                                ";
            // line 45
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "topics", [], "any", false, false, false, 45)), "html", null, true);
            echo "
                                            </span>
                                        </div>

                                    </div>

                                    ";
            // line 51
            $context["last"] = twig_get_attribute($this->env, $this->source, (isset($context["opinions"]) || array_key_exists("opinions", $context) ? $context["opinions"] : (function () { throw new RuntimeError('Variable "opinions" does not exist.', 51, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["section"], "id", [], "any", false, false, false, 51), [], "array", false, false, false, 51);
            // line 52
            echo "                                    ";
            if ((array_key_exists("last", $context) &&  !(null === (isset($context["last"]) || array_key_exists("last", $context) ? $context["last"] : (function () { throw new RuntimeError('Variable "last" does not exist.', 52, $this->source); })())))) {
                // line 53
                echo "                                        ";
                $context["topic"] = twig_get_attribute($this->env, $this->source, (isset($context["last"]) || array_key_exists("last", $context) ? $context["last"] : (function () { throw new RuntimeError('Variable "last" does not exist.', 53, $this->source); })()), "topic", [], "any", false, false, false, 53);
                // line 54
                echo "                                        <small>Last post in <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forum_section_opinion", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 54, $this->source); })()), "slug", [], "any", false, false, false, 54)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["topic"]) || array_key_exists("topic", $context) ? $context["topic"] : (function () { throw new RuntimeError('Variable "topic" does not exist.', 54, $this->source); })()), "title", [], "any", false, false, false, 54), "html", null, true);
                echo "</a> by <a
                                                    href=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_profile", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["last"]) || array_key_exists("last", $context) ? $context["last"] : (function () { throw new RuntimeError('Variable "last" does not exist.', 55, $this->source); })()), "user", [], "any", false, false, false, 55), "slug", [], "any", false, false, false, 55)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["last"]) || array_key_exists("last", $context) ? $context["last"] : (function () { throw new RuntimeError('Variable "last" does not exist.', 55, $this->source); })()), "user", [], "any", false, false, false, 55), "username", [], "any", false, false, false, 55), "html", null, true);
                echo "</a> (";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["last"]) || array_key_exists("last", $context) ? $context["last"] : (function () { throw new RuntimeError('Variable "last" does not exist.', 55, $this->source); })()), "createdAt", [], "any", false, false, false, 55)), "html", null, true);
                echo ")</small>
                                        ";
            } else {
                // line 57
                echo "                                        <small>Be the first to give opinion </small>
                                    ";
            }
            // line 59
            echo "
                                </div>

                            </div>

                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "
                </div>

                ";
        // line 69
        if ((0 === twig_compare(twig_length_filter($this->env, (isset($context["sections"]) || array_key_exists("sections", $context) ? $context["sections"] : (function () { throw new RuntimeError('Variable "sections" does not exist.', 69, $this->source); })())), 0))) {
            // line 70
            echo "                    <div class=\"col-12 mt-5 pt-5\">
                        <h3 class=\"text-muted text-center mt-5 pt-5\">
                            No categories
                        </h3>
                    </div>
                ";
        }
        // line 76
        echo "
            </div>

            <div class=\"d-flex justify-content-center\">
                ";
        // line 80
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["sections"]) || array_key_exists("sections", $context) ? $context["sections"] : (function () { throw new RuntimeError('Variable "sections" does not exist.', 80, $this->source); })()));
        echo "
            </div>


        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "forum/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 80,  209 => 76,  201 => 70,  199 => 69,  194 => 66,  182 => 59,  178 => 57,  169 => 55,  162 => 54,  159 => 53,  156 => 52,  154 => 51,  145 => 45,  135 => 38,  131 => 37,  118 => 29,  109 => 22,  105 => 21,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    Forum
{% endblock %}

{% block body %}

    <div class=\"container basic-layout forums\">

        <h3>Forums</h3>

        <hr>

        <div class=\"row\">

            <div class=\"col-12\">

                <div class=\"list-group\">

                    {% for section in sections  %}


                        <div class=\"list-group-item list-group-item-action\">

                            <div class=\"row\">

                                <div class=\"col-1 category-image\">
                                    <img src=\"{{ section.image }}\" alt=\"{{ section.name }}\">
                                </div>

                                <div class=\"col-11\">

                                    <div class=\"d-flex w-100 justify-content-between\">

                                        <h5 class=\"mb-1 mt-0\">
                                            <a href=\"{{ path('web_forum_section',{slug:section.slug }) }}\">
                                                {{ section.name }}
                                            </a>
                                        </h5>

                                        <div>
                                            Number of Topics:
                                            <span class=\"badge badge-primary badge-pill\">
                                                {{ section.topics | length }}
                                            </span>
                                        </div>

                                    </div>

                                    {% set last = opinions[section.id] %}
                                    {% if last is defined and last is not null %}
                                        {% set topic =  last.topic  %}
                                        <small>Last post in <a href=\"{{ path('web_forum_section_opinion',{slug:topic.slug}) }}\">{{topic.title }}</a> by <a
                                                    href=\"{{ path('web_profile',{slug:last.user.slug}) }}\">{{last.user.username}}</a> ({{ last.createdAt | date  }})</small>
                                        {% else %}
                                        <small>Be the first to give opinion </small>
                                    {% endif %}

                                </div>

                            </div>

                        </div>
                    {% endfor %}

                </div>

                {% if sections | length == 0 %}
                    <div class=\"col-12 mt-5 pt-5\">
                        <h3 class=\"text-muted text-center mt-5 pt-5\">
                            No categories
                        </h3>
                    </div>
                {% endif %}

            </div>

            <div class=\"d-flex justify-content-center\">
                {{ knp_pagination_render(sections) }}
            </div>


        </div>
    </div>
{% endblock %}
", "forum/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/forum/index.html.twig");
    }
}
