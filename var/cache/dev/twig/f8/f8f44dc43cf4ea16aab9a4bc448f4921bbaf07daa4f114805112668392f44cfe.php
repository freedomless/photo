<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login/index.html.twig */
class __TwigTemplate_30f907a4f737de4662bc8475813a8aeee487cdac424de092c1fe979212245e9f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'metadata' => [$this, 'block_metadata'],
            'form_title' => [$this, 'block_form_title'],
            'form_body' => [$this, 'block_form_body'],
            'form_footer' => [$this, 'block_form_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "security/template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login/index.html.twig"));

        $this->parent = $this->loadTemplate("security/template.html.twig", "security/login/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Login";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_metadata($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metadata"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metadata"));

        // line 6
        echo "    ";
        $context["social"] = ["title" => "Login", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("web_login")];
        // line 7
        echo "    ";
        $this->loadTemplate("_partials/meta.html.twig", "security/login/index.html.twig", 7)->display(twig_array_merge($context, (isset($context["social"]) || array_key_exists("social", $context) ? $context["social"] : (function () { throw new RuntimeError('Variable "social" does not exist.', 7, $this->source); })())));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_form_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_title"));

        echo "Login";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_form_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_body"));

        // line 13
        echo "
    <form method=\"post\" class=\"default-form-layout\">

        ";
        // line 16
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 16, $this->source); })())) {
            // line 17
            echo "            <div class=\"alert alert-danger text-center\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 17, $this->source); })()), "messageKey", [], "any", false, false, false, 17), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 17, $this->source); })()), "messageData", [], "any", false, false, false, 17), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 19
        echo "
        ";
        // line 20
        $context["error"] = false;
        // line 21
        echo "
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "flashes", [0 => "error"], "method", false, false, false, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 23
            echo "
            ";
            // line 24
            $context["error"] = true;
            // line 25
            echo "
            <div class=\"alert alert-danger text-center\">";
            // line 26
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
        <div class=\"form-group\">

            <label for=\"email\">Email</label>

            <input type=\"email\"
                   id=\"email\"
                   class=\"form-control\"
                   name=\"email\"
                   required
                   value=\"";
        // line 39
        (((array_key_exists("last_username", $context) &&  !(null === (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 39, $this->source); })())))) ? (print (twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 39, $this->source); })()), "html", null, true))) : (print ("")));
        echo "\">

        </div>

        <div class=\"form-group password\">

            <label for=\"password\">Password</label>

            <input type=\"password\"
                   class=\"form-control\"
                   id=\"password\"
                   required
                   name=\"password\">

            <small class=\"text-right d-block pull-right mr-4 mt-1\">
                <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_forgotten_password");
        echo "\">Forgotten Password?</a>
            </small>

        </div>

        <div class=\"form-group text-center\">

            <label class=\"custom-control custom-checkbox\">
                <input type=\"checkbox\" checked name=\"_remember_me\" class=\"custom-control-input\"/>
                <span class=\"custom-control-indicator\"></span>
                Remember Me
            </label>

        </div>

        <div class=\"text-center mt-3 mb-3\">
            New here?<a href=\"";
        // line 70
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_register");
        echo "\" class=\"btn\">Register</a>
        </div>

        <hr>

        <div class=\"text-center ml-5 mr-5 mt-3\">
            <button class=\"btn btn-info\" type=\"submit\">Login</button>
        </div>

        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">

    </form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 85
    public function block_form_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form_footer"));

        // line 86
        echo "
    <h6 class=\"h6\">Login with Your social media account:</h6>

    <a href=\"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "facebook"]);
        echo "\" class=\"btn facebook-login\">
        <i class=\"fab fa-facebook\"></i>
    </a>

    <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_social_login", ["network" => "google"]);
        echo "\" class=\"btn google-login\">
        <i class=\"fab fa-google\"></i>
    </a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/login/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  273 => 93,  266 => 89,  261 => 86,  251 => 85,  237 => 79,  225 => 70,  206 => 54,  188 => 39,  176 => 29,  167 => 26,  164 => 25,  162 => 24,  159 => 23,  155 => 22,  152 => 21,  150 => 20,  147 => 19,  141 => 17,  139 => 16,  134 => 13,  124 => 12,  105 => 10,  94 => 7,  91 => 6,  81 => 5,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'security/template.html.twig' %}

{% block title %}Login{% endblock %}

{% block metadata %}
    {% set social = {title: 'Login', url: url('web_login')} %}
    {% include '_partials/meta.html.twig' with social %}
{% endblock %}

{% block form_title %}Login{% endblock %}

{% block form_body %}

    <form method=\"post\" class=\"default-form-layout\">

        {% if error %}
            <div class=\"alert alert-danger text-center\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
        {% endif %}

        {% set error = false %}

        {% for message in app.flashes('error') %}

            {% set error = true %}

            <div class=\"alert alert-danger text-center\">{{ message }}</div>

        {% endfor %}

        <div class=\"form-group\">

            <label for=\"email\">Email</label>

            <input type=\"email\"
                   id=\"email\"
                   class=\"form-control\"
                   name=\"email\"
                   required
                   value=\"{{ last_username ?? '' }}\">

        </div>

        <div class=\"form-group password\">

            <label for=\"password\">Password</label>

            <input type=\"password\"
                   class=\"form-control\"
                   id=\"password\"
                   required
                   name=\"password\">

            <small class=\"text-right d-block pull-right mr-4 mt-1\">
                <a href=\"{{ path('web_forgotten_password') }}\">Forgotten Password?</a>
            </small>

        </div>

        <div class=\"form-group text-center\">

            <label class=\"custom-control custom-checkbox\">
                <input type=\"checkbox\" checked name=\"_remember_me\" class=\"custom-control-input\"/>
                <span class=\"custom-control-indicator\"></span>
                Remember Me
            </label>

        </div>

        <div class=\"text-center mt-3 mb-3\">
            New here?<a href=\"{{ path('web_register') }}\" class=\"btn\">Register</a>
        </div>

        <hr>

        <div class=\"text-center ml-5 mr-5 mt-3\">
            <button class=\"btn btn-info\" type=\"submit\">Login</button>
        </div>

        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\">

    </form>
{% endblock %}


{% block form_footer %}

    <h6 class=\"h6\">Login with Your social media account:</h6>

    <a href=\"{{ path('web_social_login',{ network: 'facebook'}) }}\" class=\"btn facebook-login\">
        <i class=\"fab fa-facebook\"></i>
    </a>

    <a href=\"{{ path('web_social_login',{ network: 'google'}) }}\" class=\"btn google-login\">
        <i class=\"fab fa-google\"></i>
    </a>
{% endblock %}
", "security/login/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/security/login/index.html.twig");
    }
}
