<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/notification.html.twig */
class __TwigTemplate_334188dc7900c49badbe506db4c2318220c0e11801a4aecf66bde99b753537e4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/notification.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/notification.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "flashes", [0 => "toast"], "method", false, false, false, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "
    ";
            // line 3
            $context["data"] = $this->extensions['App\Twig\JsonDecodeExtension']->decodeJson($context["message"]);
            // line 4
            echo "

    ";
            // line 6
            if ( !(null === (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 6, $this->source); })()))) {
                // line 7
                echo "        <script>
            iziToast.show({
                theme: '',
                color: '";
                // line 10
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", true, true, false, 10) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", false, false, false, 10)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "color", [], "any", false, false, false, 10), "html", null, true))) : (print ("")));
                echo "',
                icon: '";
                // line 11
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", true, true, false, 11) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", false, false, false, 11)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "icon", [], "any", false, false, false, 11), "html", null, true))) : (print ("far fa-image")));
                echo "',
                image: '";
                // line 12
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", true, true, false, 12) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", false, false, false, 12)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "image", [], "any", false, false, false, 12), "html", null, true))) : (print ("")));
                echo "',
                message: '";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 13, $this->source); })()), "message", [], "any", false, false, false, 13), "html", null, true);
                echo "',
                position: '";
                // line 14
                (((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", true, true, false, 14) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", false, false, false, 14)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "position", [], "any", false, false, false, 14), "html", null, true))) : (print ("bottomLeft")));
                echo "', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });
        </script>
    ";
            }
            // line 19
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "_partials/notification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 19,  79 => 14,  75 => 13,  71 => 12,  67 => 11,  63 => 10,  58 => 7,  56 => 6,  52 => 4,  50 => 3,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for message in app.flashes('toast') %}

    {% set data = message | json_decode %}


    {% if data is not null %}
        <script>
            iziToast.show({
                theme: '',
                color: '{{ data.color ?? '' }}',
                icon: '{{ data.icon ?? 'far fa-image' }}',
                image: '{{ data.image ?? '' }}',
                message: '{{ data.message }}',
                position: '{{ data.position ?? 'bottomLeft' }}', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                progressBarColor: '#548fc9'
            });
        </script>
    {% endif %}

{% endfor %}
", "_partials/notification.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/notification.html.twig");
    }
}
