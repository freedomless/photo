<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/rejected.html.twig */
class __TwigTemplate_fb14795b7db487bb9162a38cd99d15f545a08ff3e9549f0fbba00146a9157ebc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/rejected.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/rejected.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/curator/rejected.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Rejected";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"curate-posts basic-layout\">

        ";
        // line 9
        $this->loadTemplate("admin/curator/_partials/curator_nav.html.twig", "admin/curator/rejected.html.twig", 9)->display($context);
        // line 10
        echo "
        <hr>

        ";
        // line 13
        $this->loadTemplate("admin/curator/_partials/filter.html.twig", "admin/curator/rejected.html.twig", 13)->display($context);
        // line 14
        echo "
        <hr>

        <div class=\"d-flex justify-content-center\">
            ";
        // line 18
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 18, $this->source); })()));
        echo "
        </div>

        <div class=\"curate-posts\">
            <div class=\"container photos\">

                <div class=\"row\">

                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 26, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 27
            echo "
                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    ";
            // line 34
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "isGreyscale", [], "any", false, false, false, 34), true))) {
                // line 35
                echo "                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    ";
            }
            // line 39
            echo "
                                    ";
            // line 41
            echo "                                    ";
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 41))) {
                // line 42
                echo "                                        ";
                $context["comments"] = 0;
                // line 43
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 43));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 44
                    echo "                                            ";
                    $context["comments"] = ((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 44, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "curateComments", [], "any", false, false, false, 44)));
                    // line 45
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "
                                        ";
                // line 47
                if ((1 === twig_compare((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 47, $this->source); })()), 0))) {
                    // line 48
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 50
                    echo twig_escape_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 50, $this->source); })()), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 54
                echo "
                                    ";
            } else {
                // line 56
                echo "
                                        ";
                // line 57
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 57)), 0))) {
                    // line 58
                    echo "                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    ";
                    // line 60
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curateComments", [], "any", false, false, false, 60)), "html", null, true);
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 64
                echo "
                                    ";
            }
            // line 66
            echo "                                    ";
            // line 67
            echo "
                                    ";
            // line 68
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 68), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 68), "image", [], "any", false, false, false, 68), "thumbnail", [], "any", false, false, false, 68)));
            // line 69
            echo "
                                    <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_photo_show", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 70)]), "html", null, true);
            echo "\">

                                        <img src=\"";
            // line 72
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 72, $this->source); })()), "html", null, true);
            echo "\" alt=\"\" width=\"auto\"
                                             style=\"max-height: 300px;max-width: 100%;\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    ";
            // line 83
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "sentForCurateDate", [], "any", false, false, false, 83), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-footer d-flex text-danger\">

                                    <strong class=\"flex-grow-1 text-danger\">Rejected:</strong>

                                    ";
            // line 91
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "curatedAt", [], "any", false, false, false, 91), "d/M/Y H:i"), "html", null, true);
            echo "

                                </div>

                                <div class=\"card-text small text-center\">
                                    <span class=\"text-danger\">Rejected by:</span>
                                    &nbsp;
                                    ";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "publisher", [], "any", false, false, false, 98), "profile", [], "any", false, false, false, 98), "fullName", [], "any", false, false, false, 98), "html", null, true);
            echo "
                                </div>

                            </div>

                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "
                </div>

            </div>
        </div>


        <div class=\"d-flex justify-content-center\">

            ";
        // line 115
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 115, $this->source); })()));
        echo "

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/rejected.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 115,  267 => 106,  253 => 98,  243 => 91,  232 => 83,  218 => 72,  213 => 70,  210 => 69,  208 => 68,  205 => 67,  203 => 66,  199 => 64,  192 => 60,  188 => 58,  186 => 57,  183 => 56,  179 => 54,  172 => 50,  168 => 48,  166 => 47,  163 => 46,  157 => 45,  154 => 44,  149 => 43,  146 => 42,  143 => 41,  140 => 39,  134 => 35,  132 => 34,  123 => 27,  119 => 26,  108 => 18,  102 => 14,  100 => 13,  95 => 10,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Rejected{% endblock %}

{% block body %}

    <div class=\"curate-posts basic-layout\">

        {% include 'admin/curator/_partials/curator_nav.html.twig' %}

        <hr>

        {% include 'admin/curator/_partials/filter.html.twig' %}

        <hr>

        <div class=\"d-flex justify-content-center\">
            {{ knp_pagination_render(posts) }}
        </div>

        <div class=\"curate-posts\">
            <div class=\"container photos\">

                <div class=\"row\">

                    {% for post in posts %}

                        <div class=\"col-sm-6 col-lg-3 col-md-4  mb-5 d-flex\">

                            <div class=\"card p-0 w-100\">

                                <div class=\"card-body p-0 w-100 d-flex justify-content-center align-items-center\">

                                    {% if post.isGreyscale == true %}
                                        <div style=\"position: absolute;top:0; right: 0; padding: 0 1rem; background: #000000\">
                                            <i class=\"fas fa-adjust\"></i>
                                        </div>
                                    {% endif %}

                                    {# Comments count START #}
                                    {% if post.cover is not null %}
                                        {% set comments = 0 %}
                                        {% for child in post.children %}
                                            {% set comments = comments + child.curateComments|length %}
                                        {% endfor %}

                                        {% if comments > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ comments }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% else %}

                                        {% if post.curateComments|length > 0 %}
                                            <div style=\"position: absolute;top:0; left: 0;margin: 0 0.3rem;font-size: 20px\">
                                                <div class=\"badge badge-dark text-info\">
                                                    {{ post.curateComments|length }}
                                                </div>
                                            </div>
                                        {% endif %}

                                    {% endif %}
                                    {# Comments count END #}

                                    {% set image = post.type == 1 ? post.image.thumbnail : post.cover.image.thumbnail %}

                                    <a href=\"{{ path('admin_photo_show',{id: post.id}) }}\">

                                        <img src=\"{{ image }}\" alt=\"\" width=\"auto\"
                                             style=\"max-height: 300px;max-width: 100%;\">

                                    </a>

                                </div>

                                <div class=\"card-footer d-flex\">

                                    <strong class=\"flex-grow-1\">Sent:</strong>

                                    {{ post.sentForCurateDate | date('d/M/Y H:i') }}

                                </div>

                                <div class=\"card-footer d-flex text-danger\">

                                    <strong class=\"flex-grow-1 text-danger\">Rejected:</strong>

                                    {{ post.curatedAt | date('d/M/Y H:i') }}

                                </div>

                                <div class=\"card-text small text-center\">
                                    <span class=\"text-danger\">Rejected by:</span>
                                    &nbsp;
                                    {{ post.publisher.profile.fullName }}
                                </div>

                            </div>

                        </div>

                    {% endfor %}

                </div>

            </div>
        </div>


        <div class=\"d-flex justify-content-center\">

            {{ knp_pagination_render(posts) }}

        </div>

    </div>

{% endblock %}
", "admin/curator/rejected.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/rejected.html.twig");
    }
}
