<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vote/rating/index.html.twig */
class __TwigTemplate_392c33a73444814d69a76ec3c17f835d79866cdc61709aa637348b072ea65131 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "vote/rating/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "vote/rating/index.html.twig"));

        // line 1
        echo "<div class=\"d-flex\">

    <form method=\"post\">

        <div class=\"rating\">";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(twig_get_attribute($this->env, $this->source, (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new RuntimeError('Variable "settings" does not exist.', 6, $this->source); })()), "max", [], "any", false, false, false, 6), 1));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "<button
                data-toggle=\"tooltip\"
                        formaction=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_vote_action", ["vote" => $context["i"]]), "html", null, true);
            echo "\"
                        title=\"Rating ";
            // line 9
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">

                    <i class=\"fas fa-star\"></i>
                </button>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "</div>

    </form>

    <div>
        ";
        // line 17
        echo $this->extensions['Limenius\ReactRenderer\Twig\ReactRenderExtension']->reactRenderComponent("Favorite", ["rendering" => "client_side", "props" => ["post" => twig_get_attribute($this->env, $this->source,         // line 18
(isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 18, $this->source); })()), "id", [], "any", false, false, false, 18), "favorite" => (isset($context["favorite"]) || array_key_exists("favorite", $context) ? $context["favorite"] : (function () { throw new RuntimeError('Variable "favorite" does not exist.', 18, $this->source); })())]]);
        echo "
    </div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "vote/rating/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 18,  77 => 17,  70 => 12,  60 => 9,  56 => 8,  49 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"d-flex\">

    <form method=\"post\">

        <div class=\"rating\">{%
                for i in settings.max..1 %}<button
                data-toggle=\"tooltip\"
                        formaction=\"{{ path('web_vote_action',{vote:i}) }}\"
                        title=\"Rating {{ i }}\">

                    <i class=\"fas fa-star\"></i>
                </button>{% endfor %}</div>

    </form>

    <div>
        {{ react_component('Favorite',
            {rendering:'client_side',props:{post:post.id,favorite:favorite}}) }}
    </div>

</div>
", "vote/rating/index.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/vote/rating/index.html.twig");
    }
}
