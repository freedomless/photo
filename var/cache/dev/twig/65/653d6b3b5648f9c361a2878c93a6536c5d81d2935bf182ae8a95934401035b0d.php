<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/curator/series/posts.html.twig */
class __TwigTemplate_8ab9dd18c85191cecc4018890bd50304988c82bb76e871073374623227ba29fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/series/posts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/curator/series/posts.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new RuntimeError('Variable "posts" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "
    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6 d-flex mb-5\">

        <div class=\"card\">

            <div class=\"card-body p-0\">

                <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_update", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 10)]), "html", null, true);
            echo "\">

                    ";
            // line 12
            $context["image"] = (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 12), 1))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "cover", [], "any", false, false, false, 12), "image", [], "any", false, false, false, 12), "thumbnail", [], "any", false, false, false, 12)));
            // line 13
            echo "
                    <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 14), "html", null, true);
            echo "\" width=\"100%\" height=\"200px\"
                         style=\"object-fit: cover\"/>
                </a>

            </div>

            ";
            // line 20
            if (twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 20)) {
                // line 21
                echo "                <div class=\"card-footer p-2\">
                    <small class=\"text-muted\">Title:</small>
                    <h5 class=\"p-0\"> ";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 23), "html", null, true);
                echo "</h5>
                </div>
            ";
            }
            // line 26
            echo "
            ";
            // line 27
            if (twig_get_attribute($this->env, $this->source, $context["post"], "description", [], "any", false, false, false, 27)) {
                // line 28
                echo "
                <div class=\"card-footer\">
                    <small class=\"text-muted\">Description:</small>
                    <p class=\"text-muted\">
                        ";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "description", [], "any", false, false, false, 32), "html", null, true);
                echo "
                    </p>
                </div>

            ";
            }
            // line 37
            echo "
            <div class=\"card-footer\">

                <div class=\"text-muted\">
                    <small>Actions:</small>
                </div>

                <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_update", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 44)]), "html", null, true);
            echo "\"
                   data-toggle=\"tooltip\" data-placement=\"bottom\"
                   title=\"Edit\"
                   class=\"btn btn-default\">
                    <i class=\"fa fa-edit\"></i>
                </a>

                <button type=\"button\"
                        class=\"btn delete-warning-btn\"
                        data-toggle=\"tooltip\"
                        data-placement=\"bottom\"
                        data-title=\"Delete\"
                        data-url=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 56)]), "html", null, true);
            echo "\">
                    <i class=\"fas fa-trash-alt text-danger\"></i>
                </button>

                ";
            // line 61
            echo "                ";
            if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["post"], "type", [], "any", false, false, false, 61), 4)) && (null === twig_get_attribute($this->env, $this->source, $context["post"], "selectionSeries", [], "any", false, false, false, 61)))) {
                // line 62
                echo "                    <a class=\"btn btn-primary btn-sm\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_series_selection_new", ["id" => twig_get_attribute($this->env, $this->source,                 // line 63
$context["post"], "id", [], "any", false, false, false, 63)]), "html", null, true);
                // line 64
                echo "\">Add to Selected</a>
                ";
            }
            // line 66
            echo "

            </div>

        </div>

    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/curator/series/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 66,  147 => 64,  145 => 63,  143 => 62,  140 => 61,  133 => 56,  118 => 44,  109 => 37,  101 => 32,  95 => 28,  93 => 27,  90 => 26,  84 => 23,  80 => 21,  78 => 20,  67 => 14,  64 => 13,  62 => 12,  57 => 10,  47 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for post in posts %}

    <!--suppress ALL -->
    <div class=\"col-lg-4 col-md-6 d-flex mb-5\">

        <div class=\"card\">

            <div class=\"card-body p-0\">

                <a href=\"{{ path('admin_series_update',{id: post.id}) }}\">

                    {% set image = post.type == 1 ?  post.image.thumbnail  : post.cover.image.thumbnail %}

                    <img src=\"{{ image }}\" alt=\"{{ post.title }}\" width=\"100%\" height=\"200px\"
                         style=\"object-fit: cover\"/>
                </a>

            </div>

            {% if post.title %}
                <div class=\"card-footer p-2\">
                    <small class=\"text-muted\">Title:</small>
                    <h5 class=\"p-0\"> {{ post.title }}</h5>
                </div>
            {% endif %}

            {% if post.description %}

                <div class=\"card-footer\">
                    <small class=\"text-muted\">Description:</small>
                    <p class=\"text-muted\">
                        {{ post.description }}
                    </p>
                </div>

            {% endif %}

            <div class=\"card-footer\">

                <div class=\"text-muted\">
                    <small>Actions:</small>
                </div>

                <a href=\"{{ path('admin_series_update',{id:post.id}) }}\"
                   data-toggle=\"tooltip\" data-placement=\"bottom\"
                   title=\"Edit\"
                   class=\"btn btn-default\">
                    <i class=\"fa fa-edit\"></i>
                </a>

                <button type=\"button\"
                        class=\"btn delete-warning-btn\"
                        data-toggle=\"tooltip\"
                        data-placement=\"bottom\"
                        data-title=\"Delete\"
                        data-url=\"{{ path('admin_series_delete',{id:post.id}) }}\">
                    <i class=\"fas fa-trash-alt text-danger\"></i>
                </button>

                {# Font Page Series #}
                {% if post.type == 4 and post.selectionSeries is null %}
                    <a class=\"btn btn-primary btn-sm\" href=\"{{ path('admin_series_selection_new', {
                        id: post.id
                    }) }}\">Add to Selected</a>
                {% endif %}


            </div>

        </div>

    </div>

{% endfor %}
", "admin/curator/series/posts.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/admin/curator/series/posts.html.twig");
    }
}
