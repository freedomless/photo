<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/warning.html.twig */
class __TwigTemplate_6de100789165db8ddb3ea2c28048ffd7d7617926d6011bbac5e958514b60f9d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/warning.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/warning.html.twig"));

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 51
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "js"));

        // line 52
        echo "        <script>

            \$(function () {
                \$(document).on('click', '.";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["btn_class"]) || array_key_exists("btn_class", $context) ? $context["btn_class"] : (function () { throw new RuntimeError('Variable "btn_class" does not exist.', 55, $this->source); })()), "html", null, true);
        echo "', function () {
                    \$('#";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["modal_id"]) || array_key_exists("modal_id", $context) ? $context["modal_id"] : (function () { throw new RuntimeError('Variable "modal_id" does not exist.', 56, $this->source); })()), "html", null, true);
        echo "').modal('show');
                    \$('#";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["form_id"]) || array_key_exists("form_id", $context) ? $context["form_id"] : (function () { throw new RuntimeError('Variable "form_id" does not exist.', 57, $this->source); })()), "html", null, true);
        echo "').prop('action', \$(this).data('url'));
                })
            })

        </script>

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1
    public function macro_warning($__modal_id__ = "warning-modal", $__form_id__ = "warning-form", $__btn_class__ = "warning-action", $__header__ = "Confirm!", $__message__ = "Are you sure you want to take this action?", $__yes__ = "Yes", $__no__ = "No", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "modal_id" => $__modal_id__,
            "form_id" => $__form_id__,
            "btn_class" => $__btn_class__,
            "header" => $__header__,
            "message" => $__message__,
            "yes" => $__yes__,
            "no" => $__no__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "warning"));

            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "warning"));

            // line 8
            echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"";
            // line 9
            echo twig_escape_filter($this->env, (isset($context["modal_id"]) || array_key_exists("modal_id", $context) ? $context["modal_id"] : (function () { throw new RuntimeError('Variable "modal_id" does not exist.', 9, $this->source); })()), "html", null, true);
            echo "\">

        <div class=\"modal-dialog\" role=\"document\">

            <form method=\"post\" id=\"";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["form_id"]) || array_key_exists("form_id", $context) ? $context["form_id"] : (function () { throw new RuntimeError('Variable "form_id" does not exist.', 13, $this->source); })()), "html", null, true);
            echo "\">

                <div class=\"modal-content\">

                    <div class=\"modal-header\">

                        <h5 class=\"modal-title\">";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 19, $this->source); })()), "html", null, true);
            echo "</h5>

                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                            <span aria-hidden=\"true\">&times;</span>

                        </button>

                    </div>

                    <div class=\"modal-body\">

                        <p>";
            // line 31
            echo twig_escape_filter($this->env, (isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 31, $this->source); })()), "html", null, true);
            echo "</p>

                    </div>

                    <div class=\"modal-footer\">

                        <button class=\"btn btn-primary\">";
            // line 37
            echo twig_escape_filter($this->env, (isset($context["yes"]) || array_key_exists("yes", $context) ? $context["yes"] : (function () { throw new RuntimeError('Variable "yes" does not exist.', 37, $this->source); })()), "html", null, true);
            echo "</button>

                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["no"]) || array_key_exists("no", $context) ? $context["no"] : (function () { throw new RuntimeError('Variable "no" does not exist.', 39, $this->source); })()), "html", null, true);
            echo "</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

    ";
            // line 51
            $this->displayBlock('js', $context, $blocks);
            // line 64
            echo "
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

            
            $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "_partials/warning.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 64,  180 => 51,  165 => 39,  160 => 37,  151 => 31,  136 => 19,  127 => 13,  120 => 9,  117 => 8,  92 => 1,  75 => 57,  71 => 56,  67 => 55,  62 => 52,  52 => 51,);
    }

    public function getSourceContext()
    {
        return new Source("{% macro warning(modal_id = 'warning-modal',
                        form_id = 'warning-form',
                        btn_class = 'warning-action',
                        header = 'Confirm!',
                        message = 'Are you sure you want to take this action?',
                        yes = 'Yes',
                        no = 'No') %}

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"{{ modal_id }}\">

        <div class=\"modal-dialog\" role=\"document\">

            <form method=\"post\" id=\"{{ form_id }}\">

                <div class=\"modal-content\">

                    <div class=\"modal-header\">

                        <h5 class=\"modal-title\">{{ header }}</h5>

                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">

                            <span aria-hidden=\"true\">&times;</span>

                        </button>

                    </div>

                    <div class=\"modal-body\">

                        <p>{{ message }}</p>

                    </div>

                    <div class=\"modal-footer\">

                        <button class=\"btn btn-primary\">{{ yes }}</button>

                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{ no }}</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

    {% block js %}
        <script>

            \$(function () {
                \$(document).on('click', '.{{ btn_class }}', function () {
                    \$('#{{ modal_id }}').modal('show');
                    \$('#{{ form_id }}').prop('action', \$(this).data('url'));
                })
            })

        </script>

    {% endblock %}

{% endmacro %}
", "_partials/warning.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/_partials/warning.html.twig");
    }
}
