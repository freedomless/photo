<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gallery/nav.html.twig */
class __TwigTemplate_799bf23a5b5902c2541bbfe4e0d72261fd18546d5beebbff6ffe83c08c2afa78 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gallery/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gallery/nav.html.twig"));

        // line 1
        echo "<div>
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link ";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("latest", "page", "active "), "html", null, true);
        echo "\"
               href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery", ["page" => "latest"]);
        echo "\"><strong>Single</strong></a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link ";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_series", "_route", "active"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery_series");
        echo "\">
                <strong>Series</strong>
            </a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\ActiveNavBarExtension']->isActive("web_gallery_awarded", "_route", "active"), "html", null, true);
        echo "\"
               href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("web_gallery_awarded");
        echo "\">
                <strong>Photo of the Day</strong>
            </a>
        </li>

    </ul>

    <hr>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "gallery/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 16,  70 => 15,  59 => 9,  52 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div>
    <ul class=\"nav justify-content-center custom-active\">
        <li class=\"nav-item\">
            <a class=\"nav-link {{ active('latest','page','active ') }}\"
               href=\"{{ path('web_gallery',{page: 'latest'}) }}\"><strong>Single</strong></a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link {{ active('web_gallery_series','_route','active') }}\" href=\"{{ path('web_gallery_series') }}\">
                <strong>Series</strong>
            </a>
        </li>

        <li class=\"nav-item \">
            <a class=\"nav-link {{ active('web_gallery_awarded','_route','active') }}\"
               href=\"{{ path('web_gallery_awarded') }}\">
                <strong>Photo of the Day</strong>
            </a>
        </li>

    </ul>

    <hr>

</div>
", "gallery/nav.html.twig", "/home/freedomless/Work/symfony/photoimaginart-v2/templates/gallery/nav.html.twig");
    }
}
