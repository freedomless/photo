<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/curator/photo-of-the-day' => [[['_route' => 'admin_manage_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::index'], null, null, null, false, false, null]],
        '/curator/photo-of-the-day/switch' => [[['_route' => 'admin_switch_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::switch'], null, null, null, false, false, null]],
        '/curator/series' => [[['_route' => 'admin_series_create', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::create'], null, null, null, false, false, null]],
        '/admin/dashboard' => [[['_route' => 'admin_dashboard', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\DashboardController::dashboardAction'], null, null, null, false, false, null]],
        '/admin/email-tester' => [[['_route' => 'admin_email_tester', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::index'], null, null, null, false, false, null]],
        '/admin/email-tester/photo' => [[['_route' => 'admin_email_tester_photo', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testPhoto'], null, null, null, false, false, null]],
        '/admin/email-tester/series' => [[['_route' => 'admin_email_tester_series', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testSeries'], null, null, null, false, false, null]],
        '/admin/email-tester/selection-series' => [[['_route' => 'admin_email_tester_selection_series', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testSelectionSeries'], null, null, null, false, false, null]],
        '/admin/email-tester/potd' => [[['_route' => 'admin_email_tester_potd', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\EmailTesterController::testPhotoOfTheDay'], null, null, null, false, false, null]],
        '/admin/poll' => [[['_route' => 'poll_admin_list', '_controller' => 'App\\Controller\\Admin\\PollController::list'], null, null, null, false, false, null]],
        '/admin/poll/new' => [[['_route' => 'poll_admin_new', '_controller' => 'App\\Controller\\Admin\\PollController::new'], null, null, null, false, false, null]],
        '/curator/selection' => [[['_route' => 'admin_series_selection_list', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::list'], null, ['GET' => 0], null, false, false, null]],
        '/api/avatar' => [[['_route' => 'api_update_avatar', '_controller' => 'App\\Controller\\Api\\AccountController::avatar'], null, null, null, false, false, null]],
        '/api/cover' => [[['_route' => 'api_update_cover', '_controller' => 'App\\Controller\\Api\\AccountController::cover'], null, null, null, false, false, null]],
        '/api/accept-cookies' => [[['_route' => 'api_accept_cookies', '_controller' => 'App\\Controller\\Api\\AccountController::acceptCookies'], null, null, null, false, false, null]],
        '/api/cart' => [[['_route' => 'api_cart_create', '_controller' => 'App\\Controller\\Api\\CartController::create'], null, ['POST' => 0], null, false, false, null]],
        '/api/comments' => [[['_route' => 'app_api_comment_create', '_controller' => 'App\\Controller\\Api\\CommentController::create'], null, ['POST' => 0], null, false, false, null]],
        '/api/notifiations/total' => [[['_route' => 'api_notifications_total', '_controller' => 'App\\Controller\\Api\\NotificationController::getCount'], null, null, null, false, false, null]],
        '/api/notifiations/makr-as-seen' => [[['_route' => 'api_notifications_seen', '_controller' => 'App\\Controller\\Api\\NotificationController::markAllAsSeen'], null, ['POST' => 0], null, false, false, null]],
        '/api/order' => [[['_route' => 'api_order_create', '_controller' => 'App\\Controller\\Api\\PaymentController::order'], null, null, null, false, false, null]],
        '/api/poll/vote' => [[['_route' => 'api_poll_vote', '_controller' => 'App\\Controller\\Api\\PollController::vote'], null, null, null, false, false, null]],
        '/api/posts/follow-favorite' => [[['_route' => 'api_photos_follow_favorite', '_controller' => 'App\\Controller\\Api\\PostController::getFollowFavorite'], null, null, null, false, false, null]],
        '/api/replies' => [[['_route' => 'api_replies_create', '_controller' => 'App\\Controller\\Api\\ReplyController::create'], null, ['POST' => 0], null, false, false, null]],
        '/profile/edit' => [[['_route' => 'web_profile_edit', '_controller' => 'App\\Controller\\Web\\AccountController::update'], null, null, null, false, false, null]],
        '/payout-request' => [[['_route' => 'web_profile_payout', '_controller' => 'App\\Controller\\Web\\AccountController::payoutRequest'], null, null, null, false, false, null]],
        '/settings' => [[['_route' => 'web_settings', '_controller' => 'App\\Controller\\Web\\AccountController::settings'], null, null, null, false, false, null]],
        '/change-password' => [[['_route' => 'web_change_password', '_controller' => 'App\\Controller\\Web\\AccountController::changePassword'], null, null, null, false, false, null]],
        '/album' => [[['_route' => 'web_album_create', '_controller' => 'App\\Controller\\Web\\AlbumController::create'], null, null, null, false, false, null]],
        '/cart' => [[['_route' => 'web_cart_index', '_controller' => 'App\\Controller\\Web\\CartController::index'], null, null, null, false, false, null]],
        '/contacts' => [[['_route' => 'web_contacts', '_controller' => 'App\\Controller\\Web\\ContactController::index'], null, null, null, false, false, null]],
        '/forgotten-password' => [[['_route' => 'web_forgotten_password', '_controller' => 'App\\Controller\\Web\\ForgottenPasswordController::create'], null, null, null, false, false, null]],
        '/photos/awarded' => [[['_route' => 'web_gallery_awarded', 'page' => 'awarded', '_controller' => 'App\\Controller\\Web\\GalleryController::awarded'], null, null, null, false, false, null]],
        '/photos/series' => [[['_route' => 'web_gallery_series', '_controller' => 'App\\Controller\\Web\\GalleryController::series'], null, null, null, false, false, null]],
        '/latest' => [[['_route' => 'web_latest', 'page' => 'latest', '_controller' => 'App\\Controller\\Web\\HomeController::latest'], null, null, null, false, false, null]],
        '/' => [
            [['_route' => 'web_popular', 'page' => 'popular', '_controller' => 'App\\Controller\\Web\\HomeController::popular'], null, null, null, false, false, null],
            [['_route' => 'web_members_by_country', '_controller' => 'App\\Controller\\Web\\MemberController::byCountry'], null, null, null, false, false, null],
        ],
        '/series' => [[['_route' => 'web_series', '_controller' => 'App\\Controller\\Web\\HomeController::series'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'web_login', '_controller' => 'App\\Controller\\Web\\LoginController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'web_logout', '_controller' => 'App\\Controller\\Web\\LoginController::logout'], null, null, null, false, false, null]],
        '/messages' => [[['_route' => 'web_messages', '_controller' => 'App\\Controller\\Web\\MessageController::index'], null, null, null, false, false, null]],
        '/product-details' => [[['_route' => 'web_product_details', '_controller' => 'App\\Controller\\Web\\PagesController::media'], null, null, null, false, false, null]],
        '/privacy-policy' => [[['_route' => 'web_privacy_policy', '_controller' => 'App\\Controller\\Web\\PagesController::privacy'], null, null, null, false, false, null]],
        '/terms-of-service' => [[['_route' => 'web_tos', '_controller' => 'App\\Controller\\Web\\PagesController::tos'], null, null, null, false, false, null]],
        '/faq' => [[['_route' => 'web_faq', '_controller' => 'App\\Controller\\Web\\PagesController::faq'], null, null, null, false, false, null]],
        '/user-agreement' => [[['_route' => 'web_user_agreement', '_controller' => 'App\\Controller\\Web\\PagesController::userAgreement'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'web_register', '_controller' => 'App\\Controller\\Web\\RegisterController::register'], null, null, null, false, false, null]],
        '/resend-confirmation' => [[['_route' => 'web_resend_confirmation', '_controller' => 'App\\Controller\\Web\\RegisterController::resendConfirmation'], null, null, null, false, false, null]],
        '/search' => [[['_route' => 'web_search', '_controller' => 'App\\Controller\\Web\\SearchController::index'], null, null, null, false, false, null]],
        '/vote' => [[['_route' => 'web_vote', '_controller' => 'App\\Controller\\Web\\VoteController::index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], null, null, null, true, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/for(?'
                    .'|\\-sale/([^/]++)(?:/(single|series))?(*:212)'
                    .'|um/(?'
                        .'|topic/([^/]++)(?:/(\\d+))?(*:251)'
                        .'|opinion/([^/]++)/(?'
                            .'|delete(*:285)'
                            .'|update(*:299)'
                        .')'
                    .')'
                .')'
                .'|/curator/(?'
                    .'|p(?'
                        .'|hoto(?'
                            .'|\\-of\\-the\\-day/(?'
                                .'|add/([^/]++)(*:363)'
                                .'|delete/([^/]++)(*:386)'
                            .')'
                            .'|/([^/]++)(?'
                                .'|(*:407)'
                                .'|/(?'
                                    .'|next(*:423)'
                                    .'|p(?'
                                        .'|rev(*:438)'
                                        .'|ublished/move/([^/]++)/([^/]++)(*:477)'
                                    .')'
                                    .'|u(?'
                                        .'|pdate(*:495)'
                                        .'|nblock(*:509)'
                                    .')'
                                    .'|c(?'
                                        .'|urate/(0|1)(*:533)'
                                        .'|omment(*:547)'
                                    .')'
                                    .'|vote/([^/]++)(*:569)'
                                    .'|block(*:582)'
                                .')'
                            .')'
                        .')'
                        .'|ending(?:/([^/]++)(?:/([^/]++))?)?(*:627)'
                        .'|ublished(?:/([^/]++)(?:/([^/]++))?)?(*:671)'
                    .')'
                    .'|rejected(?:/([^/]++)(?:/([^/]++))?)?(*:716)'
                    .'|manage/series(?:/([^/]++)(?:/([^/]++))?)?(*:765)'
                    .'|se(?'
                        .'|ries/([^/]++)/(?'
                            .'|edit(*:799)'
                            .'|delete(*:813)'
                        .')'
                        .'|lection/(?'
                            .'|new/([^/]++)(*:845)'
                            .'|([^/]++)/(?'
                                .'|edit(*:869)'
                                .'|delete(*:883)'
                                .'|show(*:895)'
                                .'|hide(*:907)'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/a(?'
                    .'|dmin/p(?'
                        .'|roduct/approve/([^/]++)(*:956)'
                        .'|oll/([^/]++)/(?'
                            .'|choice/manage(*:993)'
                            .'|edit(*:1005)'
                            .'|de(?'
                                .'|lete(*:1023)'
                                .'|activate(*:1040)'
                            .')'
                            .'|activate(*:1058)'
                        .')'
                    .')'
                    .'|pi/(?'
                        .'|c(?'
                            .'|art/([^/]++)(?'
                                .'|(*:1094)'
                            .')'
                            .'|omments/([^/]++)(?'
                                .'|(?:/([^/]++))?(*:1137)'
                                .'|(*:1146)'
                            .')'
                        .')'
                        .'|f(?'
                            .'|avorites/([^/]++)(?'
                                .'|(*:1181)'
                            .')'
                            .'|ollow/([^/]++)(?'
                                .'|(*:1208)'
                            .')'
                        .')'
                        .'|notification(?'
                            .'|/([^/]++)/hide(*:1248)'
                            .'|s(?'
                                .'|(?:/([^/]++))?(*:1275)'
                                .'|/([^/]++)(*:1293)'
                            .')'
                        .')'
                        .'|me(?'
                            .'|mbers(?'
                                .'|/fo(?'
                                    .'|unders(?:/([^/]++))?(*:1343)'
                                    .'|llow(?'
                                        .'|ing(?:/([^/]++))?(*:1376)'
                                        .'|ers(?:/([^/]++))?(*:1402)'
                                    .')'
                                .')'
                                .'|(?:/([^/]++))?(*:1427)'
                            .')'
                            .'|ssage(?'
                                .'|s(?'
                                    .'|(?:/([^/]++)(?:/([^/]++))?)?(*:1477)'
                                    .'|(?:/([^/]++))?(*:1500)'
                                    .'|/([^/]++)(*:1518)'
                                .')'
                                .'|/([^/]++)(?:/([^/]++))?(*:1551)'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/members/top(?:/([^/]++))?(*:1590)'
                .'|/([^/]++)/(?'
                    .'|answer(*:1618)'
                    .'|cancel(*:1633)'
                .')'
                .'|/a(?'
                    .'|pi/(?'
                        .'|p(?'
                            .'|oll/([^/]++)/hide(*:1675)'
                            .'|hoto(?'
                                .'|s/(?'
                                    .'|([^/]++)(*:1704)'
                                    .'|p(?'
                                        .'|ublished(?:/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?)?(*:1767)'
                                        .'|o(?'
                                            .'|rtfolio(?:/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?)?(*:1829)'
                                            .'|pular(?:/([^/]++)(?:/([^/]++))?)?(*:1871)'
                                        .')'
                                    .')'
                                    .'|latest(?:/([^/]++)(?:/([^/]++))?)?(*:1916)'
                                    .'|f(?'
                                        .'|ollowing(?:/([^/]++)(?:/([^/]++))?)?(*:1965)'
                                        .'|avorites(?:/([^/]++)(?:/([^/]++))?)?(*:2010)'
                                    .')'
                                    .'|category/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:2065)'
                                    .'|tag/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:2114)'
                                    .'|a(?'
                                        .'|warded(?:/([^/]++)(?:/([^/]++))?)?(*:2161)'
                                        .'|lbum/([^/]++)(?:/([^/]++)(?:/([^/]++))?)?(*:2211)'
                                    .')'
                                    .'|series(?:/([^/]++)(?:/([^/]++))?)?(*:2255)'
                                    .'|([^/]++)/info(*:2277)'
                                    .'|store(?:/([^/]++))?(*:2305)'
                                .')'
                                .'|/([^/]++)/add\\-view(*:2334)'
                            .')'
                            .'|rice/([^/]++)/([^/]++)/([^/]++)/([^/]++)(*:2384)'
                        .')'
                        .'|s(?'
                            .'|e(?'
                                .'|ries/([^/]++)(*:2415)'
                                .'|arch/(?'
                                    .'|([^/]++)(*:2440)'
                                    .'|users/([^/]++)(*:2463)'
                                .')'
                            .')'
                            .'|tore(?:/([^/]++)(?:/([^/]++))?)?(*:2506)'
                        .')'
                        .'|replies/([^/]++)(*:2532)'
                    .')'
                    .'|lbum/([^/]++)(?'
                        .'|(*:2558)'
                        .'|/(?'
                            .'|update(*:2577)'
                            .'|delete(*:2592)'
                        .')'
                    .')'
                .')'
                .'|/p(?'
                    .'|ro(?'
                        .'|file/([^/]++)(?'
                            .'|(?:/(single|series))?(*:2651)'
                            .'|/(?'
                                .'|p(?'
                                    .'|ortfolio(?:/(single|series))?(*:2697)'
                                    .'|rints\\-for\\-sale(?:/(single|series))?(*:2743)'
                                .')'
                                .'|series(*:2759)'
                                .'|f(?'
                                    .'|ollowe(?'
                                        .'|rs(*:2783)'
                                        .'|d(*:2793)'
                                    .')'
                                    .'|avorites(*:2811)'
                                .')'
                                .'|albums(?:/([^/]++))?(*:2841)'
                            .')'
                        .')'
                        .'|duct/([^/]++)/delete(*:2872)'
                    .')'
                    .'|hoto(?'
                        .'|s(?:/([^/]++))?(*:2904)'
                        .'|\\-of\\-the\\-day/([^/]++)(?:/([^/]++))?(*:2950)'
                        .'|/([^/]++)/(?'
                            .'|edit(*:2976)'
                            .'|series/cover(*:2997)'
                            .'|curate(*:3012)'
                            .'|delete(*:3027)'
                            .'|([^/]++)(?:/([^/]++))?(*:3058)'
                        .')'
                    .')'
                .')'
                .'|/orders(?:/([^/]++)(?:/([^/]++))?)?(*:3105)'
                .'|/for(?'
                    .'|gotten\\-password/([^/]++)/([^/]++)(*:3155)'
                    .'|um(?'
                        .'|(?:/([^/]++))?(*:3183)'
                        .'|/(?'
                            .'|([^/]++)/section(?:/([^/]++))?(*:3226)'
                            .'|topic/([^/]++)/update(*:3256)'
                        .')'
                    .')'
                .')'
                .'|/s(?'
                    .'|e(?'
                        .'|lection\\-series/([^/]++)(?:/([^/]++))?(*:3315)'
                        .'|ries/([^/]++)(?'
                            .'|(*:3340)'
                            .'|/view(*:3354)'
                        .')'
                    .')'
                    .'|old(?:/([^/]++))?(*:3382)'
                    .'|tore(?:/(single|series)(?:/([^/]++)(?:/([^/]++))?)?)?(*:3444)'
                .')'
                .'|/login/(?'
                    .'|([^/]++)(*:3472)'
                    .'|check/([^/]++)(*:3495)'
                .')'
                .'|/m(?'
                    .'|embers(?:/([^/]++)(?:/([^/]++))?)?(*:3544)'
                    .'|anage/p(?'
                        .'|hotos(?:/(single|series))?(*:3589)'
                        .'|roducts(?:/([^/]++))?(*:3619)'
                    .')'
                .')'
                .'|/upload(?:/(single|series|series-from-published))?(*:3680)'
                .'|/confirmation/([^/]++)(*:3711)'
                .'|/invoice/([^/]++)(*:3737)'
                .'|/t(?'
                    .'|ag/([^/]++)(*:3762)'
                    .'|opic/([^/]++)/(?'
                        .'|create(*:3794)'
                        .'|delete(*:3809)'
                    .')'
                .')'
                .'|/vote/([^/]++)(*:3834)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        212 => [[['_route' => 'web_photo_for_sale', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\ProductController::addToStore'], ['id', 'type'], null, null, false, true, null]],
        251 => [[['_route' => 'web_forum_section_opinion', 'page' => '1', '_controller' => 'App\\Controller\\Web\\OpinionController::index'], ['slug', 'page'], null, null, false, true, null]],
        285 => [[['_route' => 'web_forum_opinion_destroy', '_controller' => 'App\\Controller\\Web\\OpinionController::delete'], ['id'], null, null, false, false, null]],
        299 => [[['_route' => 'web_forum_opinion_update', '_controller' => 'App\\Controller\\Web\\OpinionController::update'], ['id'], null, null, false, false, null]],
        363 => [[['_route' => 'admin_create_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::create'], ['id'], null, null, false, true, null]],
        386 => [[['_route' => 'admin_delete_photo_of_the_day', '_controller' => 'App\\Controller\\Admin\\AwardController::delete'], ['id'], null, null, false, true, null]],
        407 => [[['_route' => 'admin_photo_show', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::show'], ['id'], null, null, false, true, null]],
        423 => [[['_route' => 'admin_photo_show_next', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::next'], ['id'], null, null, false, false, null]],
        438 => [[['_route' => 'admin_photo_show_prev', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::previous'], ['id'], null, null, false, false, null]],
        477 => [[['_route' => 'admin_photo_published_move', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::move'], ['id', 'up', 'page'], ['POST' => 0], null, false, true, null]],
        495 => [[['_route' => 'admin_photo_update', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::update'], ['id'], null, null, false, false, null]],
        509 => [[['_route' => 'admin_curator_unblock_post', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::unblockPost'], ['id'], null, null, false, false, null]],
        533 => [[['_route' => 'admin_photo_curate', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::action'], ['id', 'action'], null, null, false, true, null]],
        547 => [[['_route' => 'admin_curator_comment', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::comment'], ['id'], ['POST' => 0], null, false, false, null]],
        569 => [[['_route' => 'admin_photo_vote', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::vote'], ['id', 'vote'], ['POST' => 0], null, false, true, null]],
        582 => [[['_route' => 'admin_curator_block_post', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::blockPost'], ['id'], null, null, false, false, null]],
        627 => [[['_route' => 'admin_photo_for_curate', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::pending'], ['page', 'limit'], null, null, false, true, null]],
        671 => [[['_route' => 'admin_photo_published', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::published'], ['page', 'limit'], null, null, false, true, null]],
        716 => [[['_route' => 'admin_photo_rejected', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorPanelController::rejected'], ['page', 'limit'], null, null, false, true, null]],
        765 => [[['_route' => 'admin_series', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::index'], ['page', 'limit'], null, null, false, true, null]],
        799 => [[['_route' => 'admin_series_update', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::update'], ['id'], null, null, false, false, null]],
        813 => [[['_route' => 'admin_series_delete', '_controller' => 'App\\Controller\\Admin\\CuratorSeriesController::delete'], ['id'], null, null, false, false, null]],
        845 => [[['_route' => 'admin_series_selection_new', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::add'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        869 => [[['_route' => 'admin_series_selection_edit', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        883 => [[['_route' => 'admin_series_selection_delete', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::delete'], ['id'], ['POST' => 0], null, false, false, null]],
        895 => [[['_route' => 'admin_series_selection_show', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::show'], ['id'], ['POST' => 0], null, false, false, null]],
        907 => [[['_route' => 'admin_series_selection_hide', '_controller' => 'App\\Controller\\Admin\\SelectionSeriesController::hide'], ['id'], ['POST' => 0], null, false, false, null]],
        956 => [[['_route' => 'admin_approve_product', '_controller' => 'App\\Controller\\Admin\\Easyadmin\\ProductController::approveProduct'], ['id'], null, null, false, true, null]],
        993 => [[['_route' => 'poll_admin_manage_choices', '_controller' => 'App\\Controller\\Admin\\PollController::manageChoices'], ['id'], null, null, false, false, null]],
        1005 => [[['_route' => 'poll_admin_edit', '_controller' => 'App\\Controller\\Admin\\PollController::edit'], ['id'], null, null, false, false, null]],
        1023 => [[['_route' => 'poll_admin_delete', '_controller' => 'App\\Controller\\Admin\\PollController::delete'], ['id'], null, null, false, false, null]],
        1040 => [[['_route' => 'poll_admin_deactivate', '_controller' => 'App\\Controller\\Admin\\PollController::deactivate'], ['id'], null, null, false, false, null]],
        1058 => [[['_route' => 'poll_admin_activate', '_controller' => 'App\\Controller\\Admin\\PollController::activate'], ['id'], null, null, false, false, null]],
        1094 => [
            [['_route' => 'api_cart_update', '_controller' => 'App\\Controller\\Api\\CartController::update'], ['id'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_cart_delete', '_controller' => 'App\\Controller\\Api\\CartController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        1137 => [[['_route' => 'api_comments_list', 'page' => '1', '_controller' => 'App\\Controller\\Api\\CommentController::index'], ['id', 'page'], ['GET' => 0], null, false, true, null]],
        1146 => [[['_route' => 'app_api_comment_destroy', '_controller' => 'App\\Controller\\Api\\CommentController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1181 => [
            [['_route' => 'api_favorties_create', '_controller' => 'App\\Controller\\Api\\FavoriteController::create'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null],
            [['_route' => 'api_favroties_delete', '_controller' => 'App\\Controller\\Api\\FavoriteController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        1208 => [
            [['_route' => 'app_api_follow_create', '_controller' => 'App\\Controller\\Api\\FollowController::create'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null],
            [['_route' => 'app_api_follow_delete', '_controller' => 'App\\Controller\\Api\\FollowController::delete'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        1248 => [[['_route' => 'api_global_notification_hide', '_controller' => 'App\\Controller\\Api\\GlobalNotificationController::hideGlobalNotificationForSession'], ['notificationName'], null, null, false, false, null]],
        1275 => [[['_route' => 'api_notifications_index', 'page' => '1', '_controller' => 'App\\Controller\\Api\\NotificationController::index'], ['page'], ['GET' => 0], null, false, true, null]],
        1293 => [[['_route' => 'api_notifications_delete', '_controller' => 'App\\Controller\\Api\\NotificationController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1343 => [[['_route' => 'api_founders', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::founders'], ['page'], null, null, false, true, null]],
        1376 => [[['_route' => 'api_members_following', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::following'], ['page'], null, null, false, true, null]],
        1402 => [[['_route' => 'api_members_followed', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::followers'], ['page'], null, null, false, true, null]],
        1427 => [[['_route' => 'api_members_alphabetical', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::alphabetical'], ['page'], null, null, false, true, null]],
        1477 => [[['_route' => 'api_messages_index', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\MessageController::index'], ['page', 'limit'], ['GET' => 0], null, false, true, null]],
        1500 => [[['_route' => 'api_messages_create', 'id' => null, '_controller' => 'App\\Controller\\Api\\MessageController::create'], ['id'], ['POST' => 0], null, false, true, null]],
        1518 => [[['_route' => 'api_messages_delete', '_controller' => 'App\\Controller\\Api\\MessageController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1551 => [[['_route' => 'api_messages_show', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MessageController::show'], ['id', 'page'], ['GET' => 0], null, false, true, null]],
        1590 => [[['_route' => 'api_members', 'page' => '1', '_controller' => 'App\\Controller\\Api\\MemberController::top'], ['page'], null, null, false, true, null]],
        1618 => [[['_route' => 'api_order_answer', 'gateway' => 'paypal', '_controller' => 'App\\Controller\\Api\\PaymentController::answer'], ['gateway'], null, null, false, false, null]],
        1633 => [[['_route' => 'api_order_cancel', 'gateway' => 'paypal', '_controller' => 'App\\Controller\\Api\\PaymentController::cancel'], ['gateway'], null, null, false, false, null]],
        1675 => [[['_route' => 'api_poll_hide_for_session', '_controller' => 'App\\Controller\\Api\\PollController::hidePollForSession'], ['id'], null, null, false, false, null]],
        1704 => [[['_route' => 'api_photos_show', '_controller' => 'App\\Controller\\Api\\PostController::show'], ['id'], null, null, false, true, null]],
        1767 => [[['_route' => 'api_photos_published', 'page' => '1', 'user' => null, 'series' => '0', '_controller' => 'App\\Controller\\Api\\PostController::published'], ['page', 'user', 'series'], null, null, false, true, null]],
        1829 => [[['_route' => 'api_photos_portfolio', 'page' => '1', 'series' => '0', 'user' => null, '_controller' => 'App\\Controller\\Api\\PostController::portfolio'], ['page', 'user', 'series'], null, null, false, true, null]],
        1871 => [[['_route' => 'api_photos_popular', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::popular'], ['page', 'limit'], null, null, false, true, null]],
        1916 => [[['_route' => 'api_photos_latest', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::latest'], ['page', 'limit'], null, null, false, true, null]],
        1965 => [[['_route' => 'api_photos_following', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::following'], ['page', 'limit'], null, null, false, true, null]],
        2010 => [[['_route' => 'api_photos_favorites', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::favorite'], ['page', 'limit'], null, null, false, true, null]],
        2065 => [[['_route' => 'api_photos_by_category', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::category'], ['id', 'page', 'limit'], null, null, false, true, null]],
        2114 => [[['_route' => 'api_photos_by_tag', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::tag'], ['tag', 'page', 'limit'], null, null, false, true, null]],
        2161 => [[['_route' => 'app_api_post_awarded', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::awarded'], ['page', 'limit'], null, null, false, true, null]],
        2211 => [[['_route' => 'api_photos_by_album', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::album'], ['id', 'page', 'limit'], null, null, false, true, null]],
        2255 => [[['_route' => 'api_photos_series_all', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Api\\PostController::series'], ['page', 'limit'], null, null, false, true, null]],
        2277 => [[['_route' => 'api_photos_info', '_controller' => 'App\\Controller\\Api\\PostController::info'], ['id'], null, null, false, false, null]],
        2305 => [[['_route' => 'app_api_post_store', 'page' => 1, '_controller' => 'App\\Controller\\Api\\PostController::store'], ['page'], null, null, false, true, null]],
        2334 => [[['_route' => 'api_add_view', '_controller' => 'App\\Controller\\Api\\PostController::addView'], ['id'], null, null, false, false, null]],
        2384 => [[['_route' => 'api_product_price', '_controller' => 'App\\Controller\\Api\\StoreController::calculateProductPrice'], ['product', 'quantity', 'size', 'material'], null, null, false, true, null]],
        2415 => [[['_route' => 'api_series_photos', '_controller' => 'App\\Controller\\Api\\PostController::seriesPhotos'], ['id'], null, null, false, true, null]],
        2440 => [[['_route' => 'api_search', '_controller' => 'App\\Controller\\Api\\SearchController::index'], ['query'], ['GET' => 0], null, false, true, null]],
        2463 => [[['_route' => 'api_search_users', '_controller' => 'App\\Controller\\Api\\SearchController::users'], ['query'], ['GET' => 0], null, false, true, null]],
        2506 => [[['_route' => 'app_api_store_single', 'type' => 'single', 'page' => 1, '_controller' => 'App\\Controller\\Api\\StoreController::single'], ['type', 'page'], null, null, false, true, null]],
        2532 => [[['_route' => 'api_replies_delete', '_controller' => 'App\\Controller\\Api\\ReplyController::destroy'], ['id'], ['DELETE' => 0], null, false, true, null]],
        2558 => [[['_route' => 'web_album_show', '_controller' => 'App\\Controller\\Web\\AlbumController::show'], ['id'], null, null, false, true, null]],
        2577 => [[['_route' => 'web_album_update', '_controller' => 'App\\Controller\\Web\\AlbumController::update'], ['id'], null, null, false, false, null]],
        2592 => [[['_route' => 'web_album_delete', '_controller' => 'App\\Controller\\Web\\AlbumController::destroy'], ['id'], null, null, false, false, null]],
        2651 => [[['_route' => 'web_profile', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::index'], ['slug', 'type'], null, null, false, true, null]],
        2697 => [[['_route' => 'web_profile_portfolio', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::portfolio'], ['slug', 'type'], null, null, false, true, null]],
        2743 => [[['_route' => 'web_profile_products', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\AccountController::products'], ['slug', 'type'], null, null, false, true, null]],
        2759 => [[['_route' => 'web_profile_series', '_controller' => 'App\\Controller\\Web\\AccountController::series'], ['slug'], null, null, false, false, null]],
        2783 => [[['_route' => 'web_profile_followers', '_controller' => 'App\\Controller\\Web\\AccountController::followers'], ['slug'], null, null, false, false, null]],
        2793 => [[['_route' => 'web_profile_followed', '_controller' => 'App\\Controller\\Web\\AccountController::followed'], ['slug'], null, null, false, false, null]],
        2811 => [[['_route' => 'web_profile_favorites', '_controller' => 'App\\Controller\\Web\\AccountController::favorites'], ['slug'], null, null, false, false, null]],
        2841 => [[['_route' => 'web_albums', 'page' => '1', '_controller' => 'App\\Controller\\Web\\AlbumController::index'], ['slug', 'page'], null, null, false, true, null]],
        2872 => [[['_route' => 'web_product_delete', '_controller' => 'App\\Controller\\Web\\ProductController::delete'], ['id'], null, null, false, false, null]],
        2904 => [[['_route' => 'web_gallery', 'page' => 'latest', '_controller' => 'App\\Controller\\Web\\GalleryController::index'], ['page'], null, null, false, true, null]],
        2950 => [[['_route' => 'web_photo_of_the_day', 'date' => null, '_controller' => 'App\\Controller\\Web\\HomeController::photoOfTheDayHistory'], ['id', 'date'], null, null, false, true, null]],
        2976 => [[['_route' => 'web_photo_update', '_controller' => 'App\\Controller\\Web\\PostController::update'], ['id'], null, null, false, false, null]],
        2997 => [[['_route' => 'web_photo_set_cover', '_controller' => 'App\\Controller\\Web\\PostController::setCover'], ['id'], null, null, false, false, null]],
        3012 => [[['_route' => 'web_photo_for_curate', '_controller' => 'App\\Controller\\Web\\PostController::sendForCurate'], ['id'], null, null, false, false, null]],
        3027 => [[['_route' => 'web_photo_delete', '_controller' => 'App\\Controller\\Web\\PostController::destroy'], ['id'], null, null, false, false, null]],
        3058 => [[['_route' => 'web_photo_show', 'payload' => null, '_controller' => 'App\\Controller\\Web\\PostController::show'], ['page', 'id', 'payload'], null, null, false, true, null]],
        3105 => [[['_route' => 'web_orders', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Web\\AccountController::orders'], ['page', 'limit'], null, null, false, true, null]],
        3155 => [[['_route' => 'web_forgotten_password_reset', '_controller' => 'App\\Controller\\Web\\ForgottenPasswordController::reset'], ['email', 'token'], null, null, false, true, null]],
        3183 => [[['_route' => 'web_forum', 'page' => '1', '_controller' => 'App\\Controller\\Web\\SectionController::index'], ['page'], null, null, false, true, null]],
        3226 => [[['_route' => 'web_forum_section', 'page' => '1', '_controller' => 'App\\Controller\\Web\\SectionController::show'], ['slug', 'page'], null, null, false, true, null]],
        3256 => [[['_route' => 'web_forum_topic_update', '_controller' => 'App\\Controller\\Web\\TopicController::update'], ['slug'], null, null, false, false, null]],
        3315 => [[['_route' => 'series_selection_history', 'slug' => null, '_controller' => 'App\\Controller\\Web\\HomeController::selectionSeriesHistory'], ['id', 'slug'], ['GET' => 0], null, false, true, null]],
        3340 => [[['_route' => 'web_series_show_old', '_controller' => 'App\\Controller\\Web\\PostController::series'], ['id'], null, null, false, true, null]],
        3354 => [[['_route' => 'web_series_show', '_controller' => 'App\\Controller\\Web\\PostController::series'], ['id'], null, null, false, false, null]],
        3382 => [[['_route' => 'web_manage_sold', 'page' => '1', '_controller' => 'App\\Controller\\Web\\ProductController::sold'], ['page'], null, null, false, true, null]],
        3444 => [[['_route' => 'web_store', 'type' => 'single', 'page' => '1', 'limit' => '20', '_controller' => 'App\\Controller\\Web\\StoreController::index'], ['type', 'page', 'limit'], null, null, false, true, null]],
        3472 => [[['_route' => 'web_social_login', '_controller' => 'App\\Controller\\Web\\LoginController::social'], ['network'], null, null, false, true, null]],
        3495 => [[['_route' => 'web_social_login_auth', '_controller' => 'App\\Controller\\Web\\LoginController::social'], ['network'], null, null, false, true, null]],
        3544 => [[['_route' => 'web_members', 'page' => 'alphabetical', 'iso' => null, '_controller' => 'App\\Controller\\Web\\MemberController::index'], ['page', 'iso'], null, null, false, true, null]],
        3589 => [[['_route' => 'web_photos_manage', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\PostController::index'], ['type'], null, null, false, true, null]],
        3619 => [[['_route' => 'web_manage_products', 'page' => '1', '_controller' => 'App\\Controller\\Web\\ProductController::index'], ['page'], null, null, false, true, null]],
        3680 => [[['_route' => 'web_photo_create', 'type' => 'single', '_controller' => 'App\\Controller\\Web\\PostController::create'], ['type'], null, null, false, true, null]],
        3711 => [[['_route' => 'web_confirmation', '_controller' => 'App\\Controller\\Web\\RegisterController::confirm'], ['token'], null, null, false, true, null]],
        3737 => [[['_route' => 'web_invoice', '_controller' => 'App\\Controller\\Web\\StoreController::invoice'], ['token'], null, null, false, true, null]],
        3762 => [[['_route' => 'web_photo_by_tag', '_controller' => 'App\\Controller\\Web\\TagController::show'], ['tag'], ['GET' => 0], null, false, true, null]],
        3794 => [[['_route' => 'web_forum_topic_create', '_controller' => 'App\\Controller\\Web\\TopicController::create'], ['slug'], null, null, false, false, null]],
        3809 => [[['_route' => 'web_forum_topic_delete', '_controller' => 'App\\Controller\\Web\\TopicController::destroy'], ['slug'], null, null, false, false, null]],
        3834 => [
            [['_route' => 'web_vote_action', '_controller' => 'App\\Controller\\Web\\VoteController::vote'], ['vote'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
