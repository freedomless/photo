<?php

namespace ContainerLA6i8e5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getProductControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\Admin\Easyadmin\ProductController' shared autowired service.
     *
     * @return \App\Controller\Admin\Easyadmin\ProductController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/vendor/easycorp/easyadmin-bundle/src/Controller/AdminControllerTrait.php';
        include_once \dirname(__DIR__, 4).'/vendor/easycorp/easyadmin-bundle/src/Controller/EasyAdminController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Admin/Easyadmin/ProductController.php';

        $container->services['App\\Controller\\Admin\\Easyadmin\\ProductController'] = $instance = new \App\Controller\Admin\Easyadmin\ProductController(($container->privates['App\\EntityManager\\MessageManager'] ?? $container->load('getMessageManagerService')));

        $instance->setContainer(($container->privates['.service_locator.J1b8VZy'] ?? $container->load('get_ServiceLocator_J1b8VZyService'))->withContext('App\\Controller\\Admin\\Easyadmin\\ProductController', $container));

        return $instance;
    }
}
