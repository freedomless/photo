<?php

namespace ContainerLA6i8e5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getProductService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.qnN8FiL.App\Entity\Store\Product' shared service.
     *
     * @return \App\Entity\Store\Product
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.qnN8FiL": it references class "App\\Entity\\Store\\Product" but no such service exists.');
    }
}
