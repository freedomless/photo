<?php

namespace ContainerLA6i8e5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCommentControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\Api\CommentController' shared autowired service.
     *
     * @return \App\Controller\Api\CommentController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Api/BaseController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Api/CommentController.php';

        $container->services['App\\Controller\\Api\\CommentController'] = $instance = new \App\Controller\Api\CommentController(($container->privates['app.manager.comment'] ?? $container->load('getApp_Manager_CommentService')));

        $a = ($container->services['serializer'] ?? $container->getSerializerService());

        $instance->setContainer(($container->privates['.service_locator.2Ca9hUK'] ?? $container->load('get_ServiceLocator_2Ca9hUKService'))->withContext('App\\Controller\\Api\\CommentController', $container));
        $instance->setSerializer($a);
        $instance->setNormalizer($a);
        $instance->setDenormalizer($a);
        $instance->setServiceContainer($container);
        $instance->setDispatcher(($container->services['event_dispatcher'] ?? $container->getEventDispatcherService()));

        return $instance;
    }
}
