<?php

namespace ContainerLA6i8e5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getSocialAuthenticationService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Authentication\SocialAuthentication' shared autowired service.
     *
     * @return \App\Authentication\SocialAuthentication
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Authentication/SocialAuthentication.php';
        include_once \dirname(__DIR__, 4).'/vendor/knpuniversity/oauth2-client-bundle/src/Client/ClientRegistry.php';

        return $container->privates['App\\Authentication\\SocialAuthentication'] = new \App\Authentication\SocialAuthentication(($container->services['knpu.oauth2.registry'] ?? ($container->services['knpu.oauth2.registry'] = new \KnpU\OAuth2ClientBundle\Client\ClientRegistry($container, ['facebook' => 'knpu.oauth2.client.facebook', 'google' => 'knpu.oauth2.client.google']))));
    }
}
