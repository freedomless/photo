<?php

namespace Proxies\__CG__\App\Entity\Photo;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Image extends \App\Entity\Photo\Image implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'file', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'print', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'printThumbnail', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'social', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'url', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'thumbnail', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'large', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'small', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'width', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'height', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'camera', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'size', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'lens', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'focalLength', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'shutterSpeed', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'ISO', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'aperture', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'exposure', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'flash', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'filter', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'tripod', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'post', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'orientation', 'id', 'createdAt', 'updatedOn', 'isDeleted'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'file', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'print', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'printThumbnail', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'social', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'url', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'thumbnail', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'large', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'small', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'width', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'height', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'camera', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'size', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'lens', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'focalLength', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'shutterSpeed', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'ISO', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'aperture', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'exposure', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'flash', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'filter', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'tripod', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'post', '' . "\0" . 'App\\Entity\\Photo\\Image' . "\0" . 'orientation', 'id', 'createdAt', 'updatedOn', 'isDeleted'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Image $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getPrint(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrint', []);

        return parent::getPrint();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrint($print): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrint', [$print]);

        return parent::setPrint($print);
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrl', []);

        return parent::getUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function setUrl($url): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUrl', [$url]);

        return parent::setUrl($url);
    }

    /**
     * {@inheritDoc}
     */
    public function getThumbnail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getThumbnail', []);

        return parent::getThumbnail();
    }

    /**
     * {@inheritDoc}
     */
    public function setThumbnail(?string $thumbnail): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setThumbnail', [$thumbnail]);

        return parent::setThumbnail($thumbnail);
    }

    /**
     * {@inheritDoc}
     */
    public function getLarge(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLarge', []);

        return parent::getLarge();
    }

    /**
     * {@inheritDoc}
     */
    public function setLarge(?string $large): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLarge', [$large]);

        return parent::setLarge($large);
    }

    /**
     * {@inheritDoc}
     */
    public function getSmall(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSmall', []);

        return parent::getSmall();
    }

    /**
     * {@inheritDoc}
     */
    public function setSmall(?string $small): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSmall', [$small]);

        return parent::setSmall($small);
    }

    /**
     * {@inheritDoc}
     */
    public function getWidth(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWidth', []);

        return parent::getWidth();
    }

    /**
     * {@inheritDoc}
     */
    public function setWidth(?int $width): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setWidth', [$width]);

        return parent::setWidth($width);
    }

    /**
     * {@inheritDoc}
     */
    public function getHeight(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHeight', []);

        return parent::getHeight();
    }

    /**
     * {@inheritDoc}
     */
    public function setHeight(?int $height): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHeight', [$height]);

        return parent::setHeight($height);
    }

    /**
     * {@inheritDoc}
     */
    public function getCamera(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCamera', []);

        return parent::getCamera();
    }

    /**
     * {@inheritDoc}
     */
    public function setCamera(?string $camera): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCamera', [$camera]);

        return parent::setCamera($camera);
    }

    /**
     * {@inheritDoc}
     */
    public function getSize(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSize', []);

        return parent::getSize();
    }

    /**
     * {@inheritDoc}
     */
    public function setSize(?int $size): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSize', [$size]);

        return parent::setSize($size);
    }

    /**
     * {@inheritDoc}
     */
    public function getLens(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLens', []);

        return parent::getLens();
    }

    /**
     * {@inheritDoc}
     */
    public function setLens(?string $lens): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLens', [$lens]);

        return parent::setLens($lens);
    }

    /**
     * {@inheritDoc}
     */
    public function getFocalLength(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFocalLength', []);

        return parent::getFocalLength();
    }

    /**
     * {@inheritDoc}
     */
    public function setFocalLength(?string $focalLength): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFocalLength', [$focalLength]);

        return parent::setFocalLength($focalLength);
    }

    /**
     * {@inheritDoc}
     */
    public function getShutterSpeed(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getShutterSpeed', []);

        return parent::getShutterSpeed();
    }

    /**
     * {@inheritDoc}
     */
    public function setShutterSpeed(?string $shutterSpeed): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setShutterSpeed', [$shutterSpeed]);

        return parent::setShutterSpeed($shutterSpeed);
    }

    /**
     * {@inheritDoc}
     */
    public function getISO(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getISO', []);

        return parent::getISO();
    }

    /**
     * {@inheritDoc}
     */
    public function setISO(?string $ISO): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setISO', [$ISO]);

        return parent::setISO($ISO);
    }

    /**
     * {@inheritDoc}
     */
    public function getAperture(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAperture', []);

        return parent::getAperture();
    }

    /**
     * {@inheritDoc}
     */
    public function setAperture(?string $aperture): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAperture', [$aperture]);

        return parent::setAperture($aperture);
    }

    /**
     * {@inheritDoc}
     */
    public function getExposure(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExposure', []);

        return parent::getExposure();
    }

    /**
     * {@inheritDoc}
     */
    public function setExposure(?string $exposure): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExposure', [$exposure]);

        return parent::setExposure($exposure);
    }

    /**
     * {@inheritDoc}
     */
    public function getFlash(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFlash', []);

        return parent::getFlash();
    }

    /**
     * {@inheritDoc}
     */
    public function setFlash(?string $flash): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFlash', [$flash]);

        return parent::setFlash($flash);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilter(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFilter', []);

        return parent::getFilter();
    }

    /**
     * {@inheritDoc}
     */
    public function setFilter(?string $filter): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFilter', [$filter]);

        return parent::setFilter($filter);
    }

    /**
     * {@inheritDoc}
     */
    public function getTripod(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTripod', []);

        return parent::getTripod();
    }

    /**
     * {@inheritDoc}
     */
    public function setTripod(?string $tripod): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTripod', [$tripod]);

        return parent::setTripod($tripod);
    }

    /**
     * {@inheritDoc}
     */
    public function getPost(): ?\App\Entity\Photo\Post
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPost', []);

        return parent::getPost();
    }

    /**
     * {@inheritDoc}
     */
    public function setPost(?\App\Entity\Photo\Post $post): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPost', [$post]);

        return parent::setPost($post);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrientation(): ?\App\Entity\Photo\Orientation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrientation', []);

        return parent::getOrientation();
    }

    /**
     * {@inheritDoc}
     */
    public function setOrientation(?\App\Entity\Photo\Orientation $orientation): \App\Entity\Photo\Image
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOrientation', [$orientation]);

        return parent::setOrientation($orientation);
    }

    /**
     * {@inheritDoc}
     */
    public function getFile(): ?\Symfony\Component\HttpFoundation\File\UploadedFile
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFile', []);

        return parent::getFile();
    }

    /**
     * {@inheritDoc}
     */
    public function setFile(?\Symfony\Component\HttpFoundation\File\UploadedFile $file): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFile', [$file]);

        parent::setFile($file);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrintThumbnail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrintThumbnail', []);

        return parent::getPrintThumbnail();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrintThumbnail($printThumbnail): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrintThumbnail', [$printThumbnail]);

        parent::setPrintThumbnail($printThumbnail);
    }

    /**
     * {@inheritDoc}
     */
    public function getSocial()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSocial', []);

        return parent::getSocial();
    }

    /**
     * {@inheritDoc}
     */
    public function setSocial($social): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSocial', [$social]);

        parent::setSocial($social);
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedOn()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedOn', []);

        return parent::getUpdatedOn();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedOn($updatedOn): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedOn', [$updatedOn]);

        parent::setUpdatedOn($updatedOn);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsDeleted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsDeleted', []);

        return parent::getIsDeleted();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsDeleted($isDeleted): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsDeleted', [$isDeleted]);

        parent::setIsDeleted($isDeleted);
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist(): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'prePersist', []);

        parent::prePersist();
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate(): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'preUpdate', []);

        parent::preUpdate();
    }

}
